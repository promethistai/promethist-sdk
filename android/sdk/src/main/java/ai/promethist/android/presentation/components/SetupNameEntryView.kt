package ai.promethist.android.presentation.components

import ai.promethist.android.ext.init
import ai.promethist.android.ext.painterResource
import ai.promethist.android.ext.regularMaterial
import ai.promethist.android.ext.stringResource
import ai.promethist.android.presentation.components.input.UserTextInputField
import ai.promethist.android.presentation.components.input.textFieldHeight
import ai.promethist.android.presentation.components.util.rememberKeyboardPaddingState
import ai.promethist.android.style.SpaceLarge
import ai.promethist.android.style.SpaceMedium
import ai.promethist.android.style.SpaceMediumLarge
import ai.promethist.android.style.SpaceMediumSmall
import ai.promethist.android.style.SpaceSmall
import ai.promethist.android.utils.AppEvent
import ai.promethist.android.utils.AppEventBus
import ai.promethist.client.AppPreset
import ai.promethist.client.Client
import ai.promethist.client.resources.Resources
import android.app.Activity
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import com.google.zxing.integration.android.IntentIntegrator
import dev.icerock.moko.resources.ImageResource

@Composable
fun SetupNameEntryView(
    onContinue: () -> Unit
) {
    val appConfig = remember { Client.appConfig() }
    val keyboardPaddingState by rememberKeyboardPaddingState()
    var invalidSetupNameAlert by remember { mutableStateOf(false) }
    var domain by remember { mutableStateOf(appConfig.engineDomain) }
    var setupName by remember { mutableStateOf(appConfig.engineSetupName) }
    val activity = LocalContext.current as Activity

    LaunchedEffect(Unit) {
        AppEventBus.subscribe(this){
            if (it is AppEvent.OnQRCodeRead) {
                domain = it.domain
                setupName = it.setupName
            }
        }
    }

    fun isValidSetupName(input: String): Boolean = try {
        input.matches(Regex("^[A-Za-z0-9:_\\-.]+$"))
    } catch (e: Exception) {
        false
    }

    Box(
        modifier = Modifier
            .fillMaxSize()
            .padding(bottom = keyboardPaddingState)
    ) {
        Image(
            painter = painterResource(
                res = Resources.images.appKeyEntryBackground
            ),
            modifier = Modifier.fillMaxSize(),
            contentDescription = null,
            contentScale = ContentScale.FillHeight
        )

        Card(
            modifier = Modifier.align(Alignment.BottomCenter),
            shape = RoundedCornerShape(topStart = sheetCornerRadius, topEnd = sheetCornerRadius),
            colors = CardDefaults.cardColors(containerColor = regularMaterial())
        ) {
            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(SpaceLarge)
                    .padding(bottom = bottomOffset),
                verticalArrangement = Arrangement.spacedBy(SpaceMedium),
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                Image(
                    modifier = Modifier.height(logoHeight),
                    painter = painterResource(res = Resources.images.appKeyEntryLogo),
                    contentDescription = null
                )

                EntryRow(
                    text = domain,
                    leadingIcon = Resources.images.icNetwork,
                    buttonIcon = Resources.images.icQrCode,
                    buttonColor = Color.Transparent,
                    buttonIconTint = Color.init(res = Resources.colors.accent),
                    onValueChange = { domain = it }
                ) {
                    val integrator = IntentIntegrator(activity)
                    integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE)
                    integrator.setOrientationLocked(false)
                    integrator.setBeepEnabled(false)
                    integrator.setPrompt("")
                    integrator.initiateScan()
                }

                EntryRow(
                    text = setupName,
                    leadingIcon = Resources.images.icKey,
                    buttonIcon = Resources.images.icArrowRight,
                    buttonColor = Color.init(res = Resources.colors.accent),
                    buttonIconTint = Color.White,
                    onValueChange = { setupName = it }
                ) {
                    if (!isValidSetupName(setupName)) {
                        invalidSetupNameAlert = true
                        return@EntryRow
                    }
                    appConfig.engineSetupName = setupName
                    appConfig.engineDomain = domain
                    appConfig.preset = AppPreset.EngineV3
                    onContinue()
                }
            }
        }
    }

    if (invalidSetupNameAlert) {
        InvalidSetupNameAlert {
            invalidSetupNameAlert = false
        }
    }
}

private val logoHeight = 50.dp
private val sheetCornerRadius = 36.dp
private val bottomOffset = 84.dp

@Composable
private fun InvalidSetupNameAlert(
    onDismiss: () -> Unit
) {
    AlertDialog(
        onDismissRequest = onDismiss,
        title = {
            Text(text = stringResource(res = Resources.strings.onboarding.invalidAppKey))
        },
        text = {
            Text(stringResource(res = Resources.strings.onboarding.invalidAppKey))
        },
        confirmButton = {
            Button(
                onClick = onDismiss) {
                Text("OK")
            }
        }
    )
}

@Composable
fun EntryRow(
    text: String,
    leadingIcon: ImageResource,
    buttonIcon: ImageResource,
    buttonIconTint: Color,
    buttonColor: Color,
    onValueChange: (String) -> Unit,
    onClick: () -> Unit
) {
    Row (
        modifier = Modifier
            .fillMaxWidth(),
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.spacedBy(SpaceSmall)
    ) {

        UserTextInputField(
            modifier = Modifier.weight(1F),
            enabled = true,
            currentText = text,
            leadingIcon = {
                Icon(
                    painter = painterResource(res = leadingIcon),
                    contentDescription = "Key icon",
                    modifier = Modifier
                        .size(SpaceMediumLarge)
                )
            },
            onValueChange = { onValueChange(it) }
        )

        Button(
            modifier = Modifier
                .size(textFieldHeight),
            onClick = { onClick() },
            enabled = true,
            contentPadding = PaddingValues(0.dp),
            shape = CircleShape,
            colors = ButtonDefaults.buttonColors(
                containerColor = buttonColor
            )
        ) {
            Icon(
                modifier = Modifier
                    .padding(SpaceMediumSmall),
                painter = painterResource(res = buttonIcon),
                contentDescription = null,
                tint = buttonIconTint
            )
        }

    }
}