package ai.promethist.android.presentation.components.screen

import ai.promethist.android.presentation.components.BottomVoiceInput
import ai.promethist.client.ClientModel
import ai.promethist.client.ClientUiState
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier

@Composable
fun VoiceView(
    modifier: Modifier,
    clientUiState: State<ClientUiState>,
    viewModel: ClientModel
) {
    Box(modifier = modifier.fillMaxSize()) {
        TranscriptView(
            modifier = Modifier.align(Alignment.BottomCenter),
            clientUiState = clientUiState
        )
        if (clientUiState.value.modal == null) {
            BottomVoiceInput(
                modifier = Modifier.align(Alignment.BottomCenter),
                clientUiState = clientUiState.value,
                viewModel = viewModel
            )
        } else {
            ModalView(
                modifier = Modifier.align(Alignment.BottomCenter),
                modal = clientUiState.value.modal,
                handleEvent = {
                    viewModel.handleEvent(it)
                }
            )
        }
    }
}



