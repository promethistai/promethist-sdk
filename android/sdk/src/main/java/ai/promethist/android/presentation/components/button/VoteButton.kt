package ai.promethist.android.presentation.components.button

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource

@Composable
fun VoteButton(imageId: Int, modifier: Modifier, onClick: () -> Unit) {
    Box(modifier = modifier.clickable { onClick() }) {
        Image(
            painterResource(id = imageId),
            contentDescription = "Vote button",
            contentScale = ContentScale.Inside
        )
    }
}