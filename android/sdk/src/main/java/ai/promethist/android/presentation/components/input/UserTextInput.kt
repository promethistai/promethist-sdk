package ai.promethist.android.presentation.components.input

import ai.promethist.android.style.SpaceMediumSmall
import ai.promethist.android.style.SpaceSmall
import ai.promethist.android.ext.init
import ai.promethist.android.ext.painterResource
import ai.promethist.android.ext.regularMaterial
import ai.promethist.android.ext.stringResource
import ai.promethist.android.style.regular
import ai.promethist.client.resources.Resources
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.unit.dp

// For #textInput, #requestText, text mode and app key entry
@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun UserTextInput(
    modifier: Modifier = Modifier,
    enabled: Boolean,
    initialText: String? = null,
    leadingIcon: @Composable (() -> Unit)? = null,
    onSubmit: (String) -> Unit
) {
    var currentText by remember { mutableStateOf(initialText ?: "") }
    val keyboardController = LocalSoftwareKeyboardController.current
    val focusManager = LocalFocusManager.current

    Row (
        modifier = modifier
            .fillMaxWidth(),
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.spacedBy(SpaceSmall)
    ) {

        UserTextInputField(
            modifier = Modifier.weight(1F),
            enabled = enabled,
            currentText = currentText,
            leadingIcon = leadingIcon,
            onValueChange = {
                currentText = it
            }
        )

        Button(
            modifier = Modifier
                .size(textFieldHeight),
            onClick = {
                onSubmit(currentText)
                currentText = ""
                focusManager.clearFocus()
                keyboardController?.hide()
            },
            enabled = currentText.isNotEmpty(),
            contentPadding = PaddingValues(0.dp),
            shape = CircleShape,
            colors = ButtonDefaults.buttonColors(
                containerColor = Color.init(res = Resources.colors.accent)
            )
        ) {
            Icon(
                modifier = Modifier
                    .padding(SpaceMediumSmall),
                painter = painterResource(res = Resources.images.icArrowRight),
                contentDescription = null,
                tint = Color.White
            )
        }
    }
}

@Composable
fun UserTextInputField(
    modifier: Modifier,
    enabled: Boolean,
    currentText: String,
    leadingIcon: @Composable (() -> Unit)? = null,
    onValueChange: (String) -> Unit
) {

    TextField(
        value = currentText,
        modifier = modifier
            .height(textFieldHeight),
        enabled = enabled,
        placeholder = {
            Text(
                text = stringResource(res = Resources.strings.persona.typeHere),
                style = regular.copy(color = Color.LightGray)
            )
        },
        onValueChange = { onValueChange(it) },
        shape = RoundedCornerShape(10.dp),
        textStyle = regular,
        singleLine = true,
        leadingIcon = leadingIcon,
        colors = TextFieldDefaults.colors(
            focusedContainerColor = regularMaterial(),
            unfocusedContainerColor = regularMaterial(),
            focusedIndicatorColor = Color.Transparent,
            unfocusedIndicatorColor = Color.Transparent,
            disabledIndicatorColor = Color.Transparent
        )
    )
}

val textFieldHeight = 52.dp