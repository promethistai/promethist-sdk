package ai.promethist.android.presentation.components.modal

import ai.promethist.android.ext.init
import ai.promethist.android.ext.stringResource
import ai.promethist.android.presentation.components.button.PromethistPrimaryButton
import ai.promethist.android.style.SpaceMedium
import ai.promethist.android.style.SpaceMediumLarge
import ai.promethist.android.style.SpaceSmall
import ai.promethist.android.style.SpaceXXSmall
import ai.promethist.channel.item.UserProfileItem
import ai.promethist.client.resources.Resources
import ai.promethist.model.Gender
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.onFocusChanged
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

@Composable
fun UserProfileModalView(
    item: UserProfileItem,
    onSubmit: () -> Unit
) {
    var name by remember { mutableStateOf("") }
    var gender by remember { mutableStateOf<Gender?>(null) }
    var isError by remember { mutableStateOf(false) }
    var isTextFieldFocused by remember { mutableStateOf(false) }

    val isValid = name.isNotEmpty() &&
            (if (item.content.showGenderSelection) gender != null else true) &&
            name.length > 2

    val borderColor = Color.init(res = if (isTextFieldFocused) Resources.colors.accent else Resources.colors.personaBorderGrey)
    val borderLineWidth = if (isTextFieldFocused) 2.dp else 1.dp

    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(top = SpaceSmall),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.spacedBy(SpaceMediumLarge)
    ) {
        Text(
            text = stringResource(res = Resources.strings.modals.profileTitle),
            textAlign = TextAlign.Center,
            modifier = Modifier.fillMaxWidth(),
            fontSize = 24.sp,
            fontWeight = FontWeight.Medium
        )

        // Name Section
        Section(title = stringResource(res = Resources.strings.modals.profileName)) {
            OutlinedTextField(
                value = name,
                onValueChange = { value ->
                    name = value
                    if (name.length <= 2) {
                        isError = false
                    } else {
                        // isError = !Validator.shared.isNameValid(entry = value)
                    }
                },
                placeholder = { Text(text = stringResource(res = Resources.strings.modals.profileNotFilled)) },
                textStyle = TextStyle(fontSize = 18.sp),
                singleLine = true,
                modifier = Modifier
                    .fillMaxWidth()
                    .border(
                        BorderStroke(borderLineWidth, borderColor),
                        shape = RoundedCornerShape(10.dp)
                    )
                    .onFocusChanged { focusState ->
                        isTextFieldFocused = focusState.isFocused
                    },
                shape = RoundedCornerShape(10.dp)
            )
        }

        if (item.content.showGenderSelection) {
            Section(title = stringResource(res = Resources.strings.modals.profileGender)) {
                Row(
                    modifier = Modifier.fillMaxWidth(),
                    horizontalArrangement = Arrangement.spacedBy(10.dp),
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    GenderItem(
                        modifier = Modifier.weight(1F),
                        text = stringResource(res = Resources.strings.modals.profileGenderMale),
                        isSelected = (gender == Gender.Male),
                        onClick = { gender = Gender.Male }
                    )
                    GenderItem(
                        modifier = Modifier.weight(1F),
                        text = stringResource(res = Resources.strings.modals.profileGenderFemale),
                        isSelected = (gender == Gender.Female),
                        onClick = { gender = Gender.Female }
                    )
                }
            }
        }

        // Error message (if any)
        if (isError) {
            Text(
                text = stringResource(res = Resources.strings.modals.profileInvalidNickname),
                color = Color.Red,
                textAlign = TextAlign.Center,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = SpaceXXSmall),
                fontSize = 18.sp
            )
        }

        // Submit Button
        PromethistPrimaryButton(
            text = stringResource(res = Resources.strings.modals.profileSubmit),
            isEnabled = isValid && !isError,
            onClick = {
//                if (!Validator.shared.isNameValid(entry = name)) {
//                    isError = true
//                    return@PromethistPrimaryButton
//                }
                onSubmit()
            },
            modifier = Modifier.fillMaxWidth()
        )
    }
}

/**
 * A helper composable that renders a section with a title and content.
 */
@Composable
fun Section(
    title: String,
    content: @Composable () -> Unit
) {
    Column(
        modifier = Modifier.fillMaxWidth(),
        verticalArrangement = Arrangement.spacedBy(SpaceSmall),
        horizontalAlignment = Alignment.Start
    ) {
        Text(
            text = title,
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = SpaceXXSmall),
            fontSize = 16.sp
        )
        content()
    }
}

/**
 * A composable representing a gender selection button.
 */
@Composable
fun GenderItem(
    modifier: Modifier,
    text: String,
    isSelected: Boolean,
    onClick: () -> Unit
) {
    Button(
        onClick = {
            onClick()
        },
        shape = RoundedCornerShape(10.dp),
        colors = ButtonDefaults.buttonColors(
            containerColor = if (isSelected) Color.init(Resources.colors.accent) else Color.Transparent,
            contentColor = if (isSelected) Color.White else Color.init(res = Resources.colors.personaTextPrimary)
        ),
        contentPadding = PaddingValues(0.dp),
        modifier = modifier
    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .border(
                    border = if (!isSelected) BorderStroke(
                        2.dp,
                        Color.init(res = Resources.colors.personaBorderGrey)
                    ) else BorderStroke(0.dp, Color.Transparent),
                    shape = RoundedCornerShape(10.dp)
                ),
            horizontalArrangement = Arrangement.Center,
            verticalAlignment = Alignment.CenterVertically
        ) {
            Text(
                text = text,
                modifier = Modifier.padding(SpaceMedium),
                fontSize = 18.sp,
                fontWeight = if (isSelected) FontWeight.SemiBold else FontWeight.Normal
            )
        }
    }
}