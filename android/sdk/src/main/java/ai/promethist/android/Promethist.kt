package ai.promethist.android

import ai.promethist.android.ext.addFlagLayoutNoLimit
import ai.promethist.android.ext.updateSystemThemeSettings
import ai.promethist.android.utils.AppEvent
import ai.promethist.android.utils.AppEventBus
import ai.promethist.client.model.CameraPermission
import ai.promethist.client.Client
import ai.promethist.client.ClientTarget
import ai.promethist.client.ClientTargetPreset
import ai.promethist.client.model.MicPermission
import ai.promethist.client.ClientEvent
import ai.promethist.client.ClientModel
import ai.promethist.telemetry.Telemetry
import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.media.AudioManager
import android.provider.Settings
import android.util.Log
import android.view.WindowManager
import androidx.activity.ComponentActivity
import androidx.activity.addCallback
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.app.ActivityCompat
import androidx.lifecycle.lifecycleScope
import com.unity3d.player.IUnityPlayerLifecycleEvents
import com.unity3d.player.UnityPlayer
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class Promethist(target: ClientTargetPreset) {

    private val scope = CoroutineScope(Job())

    lateinit var viewModel: ClientModel
    private lateinit var permissionsLauncher: ActivityResultLauncher<String>
    private var permissionsLauncherCallback: PermissionsLauncherCallback? = null

    private lateinit var audioManager: AudioManager
    lateinit var unityPlayer: UnityPlayer
        private set

    private var previousRingVolume: Int = 0
    private var previousNotificationVolume: Int = 0

    init {
        ClientTarget.apply {
            currentTarget = target
        }
    }

    fun onCreate(activity: ComponentActivity) {
        initOtl(activity)
        activity.addFlagLayoutNoLimit()
        addBackPressedDispatcherCallback(activity)

        viewModel = ClientModelFactory.create(activity, this)
        permissionsLauncher = activity.registerForActivityResult(
            ActivityResultContracts.RequestPermission()
        ) {
            permissionsLauncherCallback?.invoke(it)
        }
        audioManager = activity.getSystemService(Context.AUDIO_SERVICE) as AudioManager
        activity.window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)

        unityPlayer = UnityPlayer(activity, object: IUnityPlayerLifecycleEvents {
            override fun onUnityPlayerUnloaded() {}
            override fun onUnityPlayerQuitted() {}
        })
    }

    fun onPause(activity: ComponentActivity) {
        unityPlayer.onPause()
        changeRingVolume(activity, true)
        viewModel.handleEvent(ClientEvent.Pause)
    }

    fun onResume(activity: ComponentActivity) {
        unityPlayer.onResume()

        previousRingVolume = audioManager.getStreamVolume(AudioManager.STREAM_RING)
        previousNotificationVolume = audioManager.getStreamVolume(AudioManager.STREAM_NOTIFICATION)
        changeRingVolume(activity, false)

        // Check if user enabled or disabled permissions in settings
        val micPermissionDenied = ActivityCompat.checkSelfPermission(activity, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED
        val cameraPermissionDenied = ActivityCompat.checkSelfPermission(activity, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED

        with(Client.appConfig()) {
            if (micPermissionDenied && micPermission == MicPermission.Granted) {
                micPermission = MicPermission.Denied
            }
            if (micPermissionDenied && micPermission != MicPermission.Unknown) {
                scope.launch {
                    AppEventBus.emit(AppEvent.MicPermissionUpdated(true))
                }
            }
            if (!micPermissionDenied && micPermission != MicPermission.Granted) {
                micPermission = MicPermission.Granted
                scope.launch {
                    AppEventBus.emit(AppEvent.MicPermissionUpdated(false))
                }
            }
            if (!cameraPermissionDenied && cameraPermission != CameraPermission.Granted) {
                cameraPermission = CameraPermission.Granted
            } else if (cameraPermissionDenied && cameraPermission == CameraPermission.Granted) {
                cameraPermission = CameraPermission.Denied
            }
        }

        activity.updateSystemThemeSettings()
        viewModel.handleEvent(ClientEvent.Resume)
    }

    fun onStart() {
        unityPlayer.onStart()
    }

    fun onStop(activity: ComponentActivity) {
        unityPlayer.onStop()
        changeRingVolume(activity, true)
    }

    fun onDestroy() {
        unityPlayer.destroy()
    }

    fun onLowMemory() {
        unityPlayer.lowMemory()
    }

    fun onWindowFocusChanged(hasFocus: Boolean) {
        unityPlayer.windowFocusChanged(hasFocus)
    }

    fun requestPermission(permission: String, callback: PermissionsLauncherCallback) {
        permissionsLauncherCallback = callback
        permissionsLauncher.launch(permission)
    }

    private fun addBackPressedDispatcherCallback(activity: ComponentActivity) {
        activity.onBackPressedDispatcher.addCallback {
            activity.lifecycleScope.launch {
                viewModel.handleEvent(ClientEvent.Reset(true))
            }
        }
    }

    private fun initOtl(activity: ComponentActivity) = try {
        Telemetry.register(
            "promethist-android:" + activity.packageManager.getApplicationLabel(
                activity.applicationInfo
            ),
            Client.appConfig().otelUrl
        )
    } catch (e: Exception) {
        e.printStackTrace()
    }

    private fun changeRingVolume(activity: ComponentActivity, enable: Boolean) {
        try {
            if (Settings.Global.getInt(
                    activity.contentResolver,
                    "zen_mode"
                ) == 0 && audioManager.ringerMode != AudioManager.RINGER_MODE_SILENT
            ) {
                val newRingVolume = if (enable) previousRingVolume else AudioManager.ADJUST_MUTE
                val newNotificationVolume = if (enable) previousNotificationVolume else AudioManager.ADJUST_MUTE
                audioManager.setStreamVolume(AudioManager.STREAM_RING, newRingVolume, 0)
                audioManager.setStreamVolume(AudioManager.STREAM_NOTIFICATION, newNotificationVolume, 0)
            }
        } catch (re: RuntimeException) {
            Log.e("MainActivity", "Attempted to turn off audio in Do Not Disturb mode!")
        }
    }
}

typealias PermissionsLauncherCallback = (Boolean) -> Unit