package ai.promethist.android.presentation.components.graphics

import ai.promethist.android.R
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp

@Composable
fun ElysaiSplashscreen() {
    Box(modifier = Modifier.wrapContentSize()) {
        Image(
            painter = painterResource(id = R.drawable.room_background),
            contentDescription = "Splashscreen",
            modifier = Modifier.align(Alignment.Center).height(800.dp).width(700.dp),
            contentScale = ContentScale.Crop
        )
        Image(
            painter = painterResource(id = R.drawable.splash_logo),
            contentDescription = "Splashscreen",
            modifier = Modifier.align(Alignment.Center).height(95.dp).width(248.dp),
        )
    }
}