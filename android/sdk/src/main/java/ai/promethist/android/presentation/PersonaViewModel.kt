package ai.promethist.android.presentation

import ai.promethist.android.utils.AppEvent
import ai.promethist.android.utils.AppEventBus
import ai.promethist.client.Client
import ai.promethist.client.ClientDefaults
import ai.promethist.client.auth.AuthPolicy
import ai.promethist.client.auth.OAuth2Client
import ai.promethist.client.common.Result
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.SheetState
import androidx.compose.material3.SheetValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch

data class PersonaUiState(
    val authSheetVisible: Boolean = false
)

@OptIn(ExperimentalMaterial3Api::class)
class PersonaViewModel(
    private val authClient: OAuth2Client = Client.authClient()
) : ViewModel() {

    private val _uiState = MutableStateFlow(PersonaUiState())
    val uiState: StateFlow<PersonaUiState> = _uiState.asStateFlow()

    private var compositionScope: CoroutineScope? = null

    private val isAuthRequired: Boolean get() =
        ClientDefaults.authPolicy == AuthPolicy.Required

    val authSheetState = SheetState(
        skipPartiallyExpanded = true,
        confirmValueChange = { !isAuthRequired || (it != SheetValue.Hidden) }
    )

    init {
        AppEventBus.subscribe(viewModelScope) {
            when (it) {
                AppEvent.OnAuthSessionUpdated -> onAuthSessionUpdated()
                else -> Unit
            }
        }
    }

    fun onAppear(compositionScope: CoroutineScope, userInitiated: Boolean = false) {
        this.compositionScope = compositionScope
        if ((isAuthRequired || userInitiated) && !authClient.isLoggedIn()) {
            showAuthSheet()
        }
    }

    fun finishLogin(code: Result<Unit>, onFinished: () -> Unit) = viewModelScope.launch {
        val result = code is Result.Success
        if (!result) {
            // TODO: Show error message?
            hideAuthSheet(false)
            delay(1000)
            showAuthSheet()
            return@launch
        }

        hideAuthSheet(false)
        onFinished()
    }

    private fun onAuthSessionUpdated() = viewModelScope.launch {
        delay(500)
        if (isAuthRequired && !authClient.isLoggedIn()) {
            showAuthSheet()
        }
    }

    fun forceOpenAuthWebView() = viewModelScope.launch {
        delay(500)
        showAuthSheet()
    }

    fun hideAuthSheet(
        userInitiated: Boolean = true,
        onFinished: () -> Unit = {}
    ) = compositionScope?.launch {
        authSheetState.hide()
        delay(500)
        if (
            ClientDefaults.authPolicy == AuthPolicy.Optional
            && authClient.isLoggedIn()
            && userInitiated
        ) {
            authClient.logout()
            onFinished()
        }
        _uiState.update { it.copy(authSheetVisible = false) }
    }

    private fun showAuthSheet() = compositionScope?.launch {
        _uiState.update { it.copy(authSheetVisible = true) }
        authSheetState.show()
    }
}