package ai.promethist.android.presentation.components.text

import ai.promethist.android.ext.textShadow
import ai.promethist.android.style.heading2
import ai.promethist.client.ClientDefaults
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.core.LinearEasing
import androidx.compose.animation.core.tween
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.State
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.Hyphens
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.sp

@Composable
fun AutoScrollText(modifier: Modifier, itemVisible: Boolean, text: String?, visible: Boolean, textSize: TextUnit = 24.sp) {
    val state = rememberScrollState()

    LaunchedEffect(itemVisible, text) {
        if (itemVisible && text != null) {
            val maxValue = if (state.maxValue == Int.MAX_VALUE || state.maxValue < 0) 0 else state.maxValue
            val delay = if ((text.length ?: 0) != 0) 1000 else 3500
            state.animateScrollTo(
                maxValue,
                tween(maxValue * 30, delayMillis = delay, easing = LinearEasing)
            )
        }
    }

    AnimatedVisibility(
        visible = visible,
        enter = fadeIn(animationSpec = tween(durationMillis = 400)),
        exit = fadeOut(animationSpec = tween(durationMillis = 400))
    ) {
        Text(
            text = text ?: "",
            modifier = modifier
                .fillMaxWidth()
                .verticalScroll(state),
            textAlign = TextAlign.Center,
            fontSize = textSize,
            style = heading2.copy(
                fontSize = 25.sp,
                shadow = textShadow,
                hyphens = Hyphens.Auto
            ),
            color = Color.White
        )
    }
}