package ai.promethist.android.presentation.components

import ai.promethist.android.style.SpaceMedium
import ai.promethist.android.style.SpaceMediumSmall
import ai.promethist.channel.model.PersonaModule
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp

@Composable
fun ModulesView(
    modifier: Modifier = Modifier,
    items: List<PersonaModule>,
    onItemClick: (PersonaModule) -> Unit
) {
    Box(
        modifier = modifier
            .fillMaxSize()
    ) {
        LazyRow(
            modifier = Modifier
                .align(Alignment.BottomCenter)
                .padding(bottom = SpaceMedium)
                .height(ModulesHeight),
            horizontalArrangement = Arrangement.spacedBy(SpaceMediumSmall),
            verticalAlignment = Alignment.Top
        ) {
            items(items) {
                ModuleItemView(
                    item = it,
                    onClick = {
                        onItemClick(it)
                    }
                )
            }
        }
    }
}

private val ModulesHeight = 160.dp