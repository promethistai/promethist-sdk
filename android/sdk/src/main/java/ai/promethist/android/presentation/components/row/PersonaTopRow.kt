package ai.promethist.android.presentation.components.row

import ai.promethist.android.ext.painterResource
import ai.promethist.android.ext.textShadow
import ai.promethist.android.presentation.ProfileActivity
import ai.promethist.android.presentation.SettingsActivity
import ai.promethist.android.presentation.components.button.PersonaMenuButton
import ai.promethist.android.style.SpaceMediumLarge
import ai.promethist.android.style.SpaceMediumSmall
import ai.promethist.android.style.SpaceSmall
import ai.promethist.android.style.SpaceXLarge
import ai.promethist.android.style.SpaceXXSmall
import ai.promethist.android.style.heading1
import ai.promethist.android.style.heading3
import ai.promethist.channel.model.Action
import ai.promethist.client.Client
import ai.promethist.client.resources.Resources
import android.content.Intent
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

@Composable
fun PersonaTopRow(
    modifier: Modifier,
    title: String,
    subTitle: String,
    avatarUrl: String?,
    titlesVisible: Boolean,
    toolbarVisible: Boolean,
    tutorialVisible: Boolean,
    onTutorialClick: () -> Unit,
    onDebugClick: () -> Unit,
    onShouldLogin: () -> Unit
) {
    val context = LocalContext.current
    val startForResult = rememberLauncherForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
//        onClientEvent(ClientEvent.UpdateAvatarQuality)
//        when (result.resultCode) {
//            SettingsActivity.ChangePersonaResultCode -> onClientEvent(ClientEvent.ChangePersona)
//            SettingsActivity.StartFeedbackConversationResultCode -> onClientEvent(ClientEvent.UserInput (Action.Feedback.text))
//            SettingsActivity.StartSOSConversationResultCode -> onClientEvent(ClientEvent.UserInput (Action.Sos.text))
//            SettingsActivity.ShouldResetResultCode -> onClientEvent(ClientEvent.Reset)
//            SettingsActivity.ShouldSendReportResultCode -> onClientEvent(ClientEvent.SendTechnicalReport)
//            ProfileActivity.LogInResultCode -> onShouldLogin()
//        }
    }

    val titleFontSize = when (title.count()) {
        in Int.MIN_VALUE..7 -> 22.5.sp
        in Int.MIN_VALUE..25 -> 17.5.sp
        else -> 16.sp
    }

    Column {
        AnimatedVisibility(
            visible = toolbarVisible,
            enter = fadeIn(),
            exit = fadeOut()
        ) {
            Row (
                modifier = modifier
                    .padding(
                        top = SpaceXLarge,
                        start = SpaceMediumLarge,
                        end = SpaceMediumLarge,
                        bottom = SpaceSmall,
                    )
                    .height(64.dp),
                verticalAlignment = Alignment.Top
            ) {
                PersonaMenuButton(
                    painter = painterResource(Resources.images.iconOptions),
                    onClick = {
                        val intent = Intent(context, SettingsActivity::class.java)
                        startForResult.launch(intent)
                    }
                )

                Spacer(modifier = Modifier.width(SpaceMediumSmall))

                PersonaMenuButton(
                    modifier = Modifier
                        .alpha(if (Client.appConfig().onScreenDebug) 1f else 0f),
                    painter = painterResource(Resources.images.icDebug),
                    onClick = {
                        onDebugClick()
                    }
                )

                Spacer(modifier = Modifier.weight(1f))

                Column(
                    modifier = Modifier
                        .alpha(if (titlesVisible) 1f else 0f),
                    verticalArrangement = Arrangement.spacedBy(SpaceXXSmall),
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    Text(
                        text = title,
                        color = Color.White,
                        style = heading1.copy(
                            fontSize = titleFontSize,
                            shadow = textShadow
                        )
                    )

                    Text(
                        text = subTitle,
                        color = Color.White,
                        style = heading3.copy(shadow = textShadow)
                    )
                }

                Spacer(modifier = Modifier.weight(1f))

                PersonaMenuButton(
                    painter = painterResource(Resources.images.iconProfile),
                    avatarUrl = avatarUrl,
                    onClick = {
                        val intent = Intent(context, ProfileActivity::class.java)
                        intent.putExtra(ProfileActivity.USERNAME, "")
                        startForResult.launch(intent)
                    }
                )
            }
        }

        AnimatedVisibility(
            visible = tutorialVisible,
            enter = fadeIn(),
            exit = fadeOut()
        ) {
            Row(
                horizontalArrangement = Arrangement.End,
                modifier = modifier
                    .padding(
                        start = SpaceMediumLarge,
                        end = SpaceMediumLarge,
                        top = if (toolbarVisible) 0.dp else SpaceXLarge
                    )
                    .height(64.dp)
                    .fillMaxWidth()
            ) {
                PersonaMenuButton(
                    painter = painterResource(Resources.images.iconTutorial),
                    onClick = { onTutorialClick() },
                )
            }
        }
    }
}
