package ai.promethist.android.presentation.components.auth

import ai.promethist.client.Client
import androidx.compose.foundation.layout.*
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import io.ktor.http.Url

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun AuthSheetView(
    state: SheetState,
    shouldHandleCallback: (Url) -> Boolean,
    handleCallback: (Url) -> Unit,
    onDismissRequest: () -> Unit
) {
    var isLoading by remember { mutableStateOf(false) }
    val url = Client.authClient().getAuthorizationUrl()

    ModalBottomSheet(
        sheetState = state,
        onDismissRequest = onDismissRequest,
    ) {
        Box(modifier = Modifier.fillMaxSize()) {
            AuthWebView(
                url = url.toString(),
                shouldHandleCallback = shouldHandleCallback,
                handleCallback = handleCallback
            )

            if (isLoading) {
                Box(
                    contentAlignment = Alignment.Center,
                    modifier = Modifier.fillMaxSize()
                ) {
                    CircularProgressIndicator()
                }
            }
        }
    }
}