package ai.promethist.android.services.speechRecognizer.duplex

import kotlin.math.log10

/**
 * A utility class for measuring the loudness of audio data.
 *
 * This class provides a method to calculate the loudness of an audio signal
 * represented as 16-bit PCM data in a `ByteArray`.
 */
class LoudnessMeter {

    /**
     * Calculates the loudness of the given audio data in decibels (dB).
     *
     * The loudness is computed as the logarithmic value of the mean squared amplitude
     * of the audio samples. The audio data is interpreted as 16-bit PCM (Pulse Code Modulation),
     * where each sample is represented by two bytes (little-endian format).
     *
     * @param audioData A `ByteArray` containing the audio data in 16-bit PCM format.
     * @return The calculated loudness in decibels (dB).
     */
    fun calculateLoudness(audioData: ByteArray): Double {
        var sum = 0.0
        // Interpret audio as 16-bit PCM
        for (i in audioData.indices step 2) {
            val sample = (audioData[i].toInt() or (audioData[i + 1].toInt() shl 8)).toShort()
            sum += sample * sample
        }
        val mean = sum / (audioData.size / 2) // Divide by number of samples
        return 10 * log10(mean + 1e-10)
    }
}