package ai.promethist.android.presentation.components.picker

import ai.promethist.android.presentation.components.text.ShadedText
import ai.promethist.channel.command.PersonaModule
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.wear.compose.material.Picker
import androidx.wear.compose.material.PickerDefaults
import androidx.wear.compose.material.rememberPickerState

@Composable
fun ModuleSpinnerPicker(
    modifier: Modifier,
    modules: List<PersonaModule>,
    onClick: (String) -> Unit
) {
    val pickerState = rememberPickerState(initialNumberOfOptions = modules.size)
    val interactionSource = remember { MutableInteractionSource() }
    Picker(
        state = pickerState,
        contentDescription = "Module picker",
        modifier = modifier
            .fillMaxHeight(0.3F)
            .clickable(
                interactionSource = interactionSource,
                indication = null
            ) {

                val module = modules[pickerState.selectedOption]
                onClick(module.action)
            },
        onSelected = {},
        scalingParams = PickerDefaults.scalingParams(
            edgeAlpha = 0.3F
        ),
        gradientColor = Color.Transparent
    ) {
        if (modules.isNotEmpty()) {
            ShadedText(
                modifier = Modifier
                    .padding(vertical = 20.dp),
                personaText = modules[it].name
            )
        }
    }
}