package ai.promethist.android.presentation.components

import ai.promethist.android.ext.init
import ai.promethist.android.ext.painterResource
import ai.promethist.android.style.SpaceMedium
import ai.promethist.android.style.SpaceSmall
import ai.promethist.client.resources.Resources
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

@Composable
fun AppToolbar(
    modifier: Modifier = Modifier,
    title: String? = null,
    onBackClick: () -> Unit
) {
    Box(
        modifier = modifier
            .background(MaterialTheme.colorScheme.background)
            .fillMaxWidth()
            .height(ToolbarHeight.dp)
            .padding(horizontal = SpaceMedium)
    ) {
        IconButton(onClick = onBackClick) {
            Icon(
                modifier = Modifier
                    .align(Alignment.CenterStart)
                    .size(ToolbarHeight.dp)
                    .padding(SpaceSmall),
                painter = painterResource(res = Resources.images.backArrow),
                tint = Color.init(Resources.colors.primaryLabel),
                contentDescription = null
            )
        }

        title?.let {
            Text(
                modifier = Modifier.align(Alignment.Center),
                text = it.take(TitleMaxLength),
                fontSize = 16.sp,
                color = Color.init(Resources.colors.primaryLabel),
                maxLines = 1,
                overflow = TextOverflow.Ellipsis
            )
        }
    }
}

private const val ToolbarHeight = 50
private const val TitleMaxLength = 20