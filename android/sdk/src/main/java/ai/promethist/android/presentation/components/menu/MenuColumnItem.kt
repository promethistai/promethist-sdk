package ai.promethist.android.presentation.components.menu

import ai.promethist.android.ext.init
import ai.promethist.android.style.RadiusLarge
import ai.promethist.android.style.RadiusXLarge
import ai.promethist.android.style.SpaceMedium
import ai.promethist.android.style.SpaceMediumSmall
import ai.promethist.android.style.SpaceSmall
import ai.promethist.android.style.SpaceXSmall
import ai.promethist.android.style.heading2
import ai.promethist.channel.command.GrowthModule
import ai.promethist.channel.command.GrowthState
import ai.promethist.channel.command.GrowthSubmodule
import ai.promethist.client.resources.Resources
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.animateContentSize
import androidx.compose.animation.core.animateIntAsState
import androidx.compose.animation.expandVertically
import androidx.compose.animation.shrinkVertically
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.heightIn
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment.Companion.CenterVertically
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.Hyphens
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.wear.compose.material.Text
import coil.compose.AsyncImage

@Composable
fun MenuColumnItem(
    module: GrowthModule,
    onSubmoduleStart: (String) -> Unit
) {
    var isExpanded by remember { mutableStateOf(false) }

    val minImageHeight: Int by animateIntAsState(if (isExpanded) 25 else 60, label = "")
    val fontSize: Int by animateIntAsState(if (isExpanded) 13 else if (module.title.count() > 22) 16 else 18, label = "")
    val imageHeight: Int by animateIntAsState(if (isExpanded) 30 else 92, label = "")
    val imageWidth: Int by animateIntAsState(if (isExpanded) 70 else 158, label = "")
    val rowPadding: Int by animateIntAsState(if (isExpanded) 7 else 0, label = "")

    val containerBackground = when (module.state) {
        GrowthState.New -> Resources.colors.growthModuleNew
        GrowthState.Done -> Resources.colors.growthModuleDone
        GrowthState.InProgress -> Resources.colors.growthModuleInProgress
        GrowthState.Locked -> Resources.colors.growthModuleLocked
        GrowthState.Unlocked -> Resources.colors.growthModuleInProgress
    }
    val moduleBorder = when(module.state) {
        GrowthState.Done -> Color.init(res = Resources.colors.growthModuleInProgress)
        else -> Color.Transparent
    }

    val shape = RoundedCornerShape(RadiusXLarge)

    Column(
        modifier = Modifier
            .clip(shape)
            .border(width = 1.dp, color = moduleBorder, shape = shape)
            .clickable {
                isExpanded = !isExpanded
            }
            .fillMaxWidth()
            .background(Color.init(res = containerBackground))
            .padding(horizontal = SpaceMedium)
    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(vertical = rowPadding.dp),
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = CenterVertically
        ) {
            Column(
                modifier = Modifier
                    .fillMaxHeight()
                    .weight(1f)
                    .padding(vertical = SpaceMedium)
                    .animateContentSize(),
                verticalArrangement = Arrangement.spacedBy(SpaceXSmall)
            ) {
                Text(
                    text = module.title,
                    style = heading2.copy(
                        hyphens = Hyphens.Auto,
                        fontSize = fontSize.sp,
                        fontWeight = FontWeight.SemiBold,
                        fontFamily = null
                    ),
                    color = Color.Black,
                    maxLines = 3,
                    overflow = TextOverflow.Ellipsis,
                )

                module.description?.let {
                    if (it.isNotEmpty() && isExpanded) {
                        Text(
                            text = it,
                            color = Color.Black,
                            maxLines = 1,
                            fontSize = 13.sp,
                            overflow = TextOverflow.Ellipsis
                        )
                    }
                }
            }

            module.imageURL?.let {
                Box(
                    modifier = Modifier
                        .widthIn(min = 20.dp)
                        .heightIn(min = minImageHeight.dp)
                        .align(CenterVertically)
                ) {
                    if (it.isNotBlank()) {
                        AsyncImage(
                            modifier = Modifier
                                .height(imageHeight.dp)
                                .width(imageWidth.dp)
                            ,
                            model = it,
                            contentDescription = null,
                            contentScale = ContentScale.Fit
                        )
                    }
                }
            }
        }

        AnimatedVisibility(
            visible = isExpanded,
            enter = expandVertically(),
            exit = shrinkVertically()
        ) {
            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(bottom = SpaceMedium),
                verticalArrangement = Arrangement.spacedBy(6.dp)
            ) {
                module.submoduleGroups.forEach { group ->
                    group.submodules.forEach {
                        GrowthSubmoduleView(
                            item = it,
                            moduleState = it.state,
                            onClick = {
                                if (it.state != GrowthState.Locked && module.state != GrowthState.Locked)
                                    onSubmoduleStart(it.action)
                            }
                        )
                    }
                }

                module.submodules.forEach { submodule ->
                    submodule.forEach {
                        GrowthSubmoduleView(
                            item = it,
                            moduleState = it.state,
                            onClick = {
                                if (it.state != GrowthState.Locked && module.state != GrowthState.Locked)
                                    onSubmoduleStart(it.action)
                            }
                        )
                    }
                }
            }
        }
    }
}

@Composable
private fun GrowthSubmoduleView(
    item: GrowthSubmodule,
    moduleState: GrowthState,
    onClick: () -> Unit
) {

    val background = when (moduleState) {
        GrowthState.New -> Color.init(res = Resources.colors.growthSubmoduleNew)
        GrowthState.Done -> Color.init(res = Resources.colors.growthSubmoduleDone)
        GrowthState.InProgress -> Color.init(res = Resources.colors.growthSubmoduleInProgress)
        GrowthState.Locked -> Color.init(res = Resources.colors.growthSubmoduleLocked)
        GrowthState.Unlocked -> Color.init(res = Resources.colors.growthSubmoduleInProgress)
    }
    val progressBarTint = if (moduleState == GrowthState.Locked)
        Color.init(res = Resources.colors.growthProgressLocked)
    else
        Color.init(res = Resources.colors.growthProgress)

    Row(
        modifier = Modifier
            .clip(RoundedCornerShape(RadiusLarge))
            .clickable(onClick = onClick)
            .fillMaxWidth()
            .background(background)
            .padding(horizontal = SpaceMedium, vertical = SpaceSmall),
        horizontalArrangement = Arrangement.spacedBy(SpaceMediumSmall),
        verticalAlignment = CenterVertically
    ) {
        Column(
            modifier = Modifier.weight(1f)
        ) {
            Text(
                text = item.title,
                fontSize = 15.sp,
                fontWeight = FontWeight.Medium,
                color = Color.Black,
                textAlign = TextAlign.Start
            )

            item.description?.let {
                Text(
                    modifier = Modifier.padding(top = SpaceXSmall),
                    text = it,
                    fontSize = 13.sp,
                    color = Color.Black,
                    textAlign = TextAlign.Start
                )
            }

            item.progress?.let {
                MenuProgressTileV2(
                    modifier = Modifier.padding(top = 3.dp),
                    progress = it,
                    height = 5.dp,
                    fontSize = 12.sp,
                    progressBarTint = progressBarTint
                )
            }
        }

        item.imageURL?.let {
            AsyncImage(
                modifier = Modifier.size(100.dp),
                model = it,
                contentDescription = null
            )
        }
    }
}