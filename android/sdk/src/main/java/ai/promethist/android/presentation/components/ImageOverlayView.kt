package ai.promethist.android.presentation.components

import ai.promethist.android.ext.init
import ai.promethist.android.style.SpaceLarge
import ai.promethist.client.resources.Resources
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import coil.compose.AsyncImage

@Composable
fun ImageOverlayView(
    url: String,
    alpha: Float,
    onImageSuccess: () -> Unit,
    onImageError: () -> Unit,
) {
    Box(
        modifier = Modifier
            .alpha(alpha)
            .fillMaxSize()
            .padding(top = 120.dp, bottom = 110.dp)
            .padding(horizontal = SpaceLarge),
        contentAlignment = Alignment.BottomCenter
    ) {
        AsyncImage(
            modifier = Modifier
                .height(236.dp)
                .widthIn(max = 236.dp)
                .border(
                    width = 3.dp,
                    color = Color.init(Resources.colors.accentSecondary),
                    shape = RoundedCornerShape(15.dp)
                )
                .padding(5.dp)
                .clip(RoundedCornerShape(10.dp)),
            model = url,
            contentDescription = null,
            onSuccess = { onImageSuccess() },
            onError = { onImageError() }
        )
    }
}