package ai.promethist.android.presentation.components.graphics

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color

@Composable
fun VoiceGradient(
    modifier: Modifier,
    showVoiceIndicator: Boolean,
    isRadial: Boolean = false
) {
    AnimatedVisibility(
        modifier = modifier,
        visible = showVoiceIndicator,
        enter = fadeIn(),
        exit = fadeOut()
    ) {
        val gradient: (List<Color>) -> Brush =
            if (isRadial) {
                { Brush.radialGradient(it.reversed()) }
            }
            else {
                { Brush.verticalGradient(it) }
            }
        Box(
            modifier = Modifier
                .background(
                    brush = gradient(
                        listOf(
                            Color.Transparent,
                            Color(0x8034C759),
                            Color(0xFF34C759)
                        )
                    )
                )
        ) {
        }
    }
}