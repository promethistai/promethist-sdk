package ai.promethist.android.services.speechRecognizer

import ai.promethist.android.ext.runOnMainLooper
import ai.promethist.client.speech.SpeechRecognizerController
import ai.promethist.client.speech.SpeechRecognizerControllerCallback
import android.app.Application
import android.util.Log
import net.gotev.speech.GoogleVoiceTypingDisabledException
import net.gotev.speech.Speech
import net.gotev.speech.SpeechDelegate
import net.gotev.speech.SpeechRecognitionNotAvailable
import java.util.Locale

class AndroidSpeechRecognizerV2(val context: Application): SpeechRecognizerController {

    private var callback_: SpeechRecognizerControllerCallback? = null

    override fun setCallback(callback: SpeechRecognizerControllerCallback) {
        callback_ = callback
    }

    override suspend fun startTranscribe() {
        Log.i(TAG, "Speech - startTranscribe")
        transcribe()
    }

    override fun stopTranscribe() {
        runOnMainLooper {
            Speech.getInstance().stopListening()
        }
    }

    override fun pause() {
        // TODO
    }

    override fun resume() {
        // TODO
    }

    override fun setSilenceLength(length: Int) {
        TODO("Not yet implemented")
    }

    override fun onVisualItemReceived() {

    }

    override fun restartTranscribe() {
        TODO("Not yet implemented")
    }

    override fun setPrivileged(value: Boolean) {
        TODO("Not yet implemented")
    }

    private fun transcribe() {
        runOnMainLooper {
            try {
                Speech.getInstance().setLocale(Locale.US)
                Speech.getInstance().startListening(object : SpeechDelegate {

                    override fun onStartOfSpeech() {
                        Log.i(TAG, "Speech recognition is now active")
                    }

                    override fun onSpeechPartialResults(results: List<String>) {
                        results.firstOrNull()?.let {
                            if (it.isNotEmpty())
                                callback_?.onTranscribeResult(it, false)
                        }
                        Log.i(TAG, "partial result: " + results.map { it })
                    }

                    override fun onSpeechResult(result: String) {
                        Log.i(TAG, "result: $result")
                        if (result.isBlank()) {
                            transcribe()
                            return
                        }
                        callback_?.onTranscribeResult(result, true)
                    }

                    override fun onSpeechRmsChanged(value: Float) {}
                })
            } catch (exc: SpeechRecognitionNotAvailable) {
                Log.e(TAG, "Speech recognition is not available on this device!")
                // TODO Speech recognition is not available on this device!
                // You can prompt the user if he wants to install Google App to have
                // speech recognition, and then you can simply call:
                //
                // SpeechUtil.redirectUserToGoogleAppOnPlayStore(this)
            } catch (exc: GoogleVoiceTypingDisabledException) {
                Log.e(TAG, "Google voice typing must be enabled!")
                // TODO Google voice typing must be enabled!
            }
        }
    }

    companion object {
        private const val TAG = "SpeechRecognizerV2"
    }
}