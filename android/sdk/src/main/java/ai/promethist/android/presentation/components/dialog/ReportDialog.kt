package ai.promethist.android.presentation.components.dialog

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

@Composable
fun ReportDialog(
    onReportClick: (String, Boolean) -> Unit
) {
    var eventDescription by remember { mutableStateOf("") }
    AlertDialog(modifier = Modifier
        .padding(8.dp)
        .fillMaxWidth(0.92F),
        onDismissRequest = { onReportClick("", false) },
        confirmButton = {
            TextButton(onClick = {
                onReportClick(eventDescription, true)
                eventDescription = ""
            }) {
                Text("Send report")
            }
        },
        dismissButton = {},
        text = {
            Column(
                modifier = Modifier.verticalScroll(rememberScrollState())
            ) {
                Text(
                    text = "Describe what went wrong:",
                    modifier = Modifier.padding(top = 8.dp, bottom = 4.dp),
                    style = MaterialTheme.typography.titleLarge,
                    fontWeight = FontWeight.Bold,
                    fontSize = 18.sp
                )
                TextField(
                    value = eventDescription,
                    onValueChange = { eventDescription = it },
                    modifier = Modifier.padding(top = 8.dp, bottom = 4.dp).fillMaxSize(),
                    readOnly = false,
                    minLines = 10
                )
            }
        })
}