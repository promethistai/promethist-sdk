package ai.promethist.android

import ai.promethist.client.ClientUiState
import ai.promethist.client.ClientUiStateHandler
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update

class ClientUiStateHandlerImpl: ClientUiStateHandler {

    private val _uiState = MutableStateFlow(ClientUiState.Initial)
    val uiState: StateFlow<ClientUiState> = _uiState.asStateFlow()

    override suspend fun setState(state: ClientUiState) {
        _uiState.update { state }
    }

    override suspend fun getState(): ClientUiState = uiState.value
}