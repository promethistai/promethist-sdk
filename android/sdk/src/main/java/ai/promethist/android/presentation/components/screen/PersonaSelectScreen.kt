package ai.promethist.android.presentation.components.screen

import ai.promethist.android.ext.init
import ai.promethist.android.ext.painterResource
import ai.promethist.android.ext.stringResource
import ai.promethist.android.presentation.SettingsActivity
import ai.promethist.android.presentation.components.PersonaCard
import ai.promethist.android.presentation.components.button.CircularButton
import ai.promethist.android.presentation.components.row.PersonaRowV2
import ai.promethist.android.style.SpaceLarge
import ai.promethist.android.style.SpaceMediumLarge
import ai.promethist.android.style.SpaceSmall
import ai.promethist.android.style.SpaceXLarge
import ai.promethist.channel.item.PersonaItem
import ai.promethist.client.Client
import ai.promethist.client.model.ApplicationInfo
import ai.promethist.client.model.Message
import ai.promethist.client.resources.Resources
import android.content.Intent
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import kotlinx.coroutines.launch

@Composable
fun PersonaSelectScreen(
    personas: List<ApplicationInfo.Persona>,
    onHistoryOpened: (List<Message>) -> Unit,
    onLogout: () -> Unit,
    onStartButtonClick: (String, String) -> Unit
) {

    val startForResult = rememberLauncherForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
        when (result.resultCode) {
            SettingsActivity.ShouldLogoutCode -> onLogout()
        }
    }

    val context = LocalContext.current
    val scope = rememberCoroutineScope()

    val withHistory = Client.appConfig().lastSelectedAvatarId != null
    val suffix = if (withHistory) "" else "" // TODO change

    var currentIndex by remember {
        mutableStateOf<Int?>(null)
    }
    var currentPersonaRef by remember {
        mutableStateOf<String?>(null)
    }
    var currentAvatarId by remember {
        mutableStateOf<String?>(null)
    }
    var avatarUrl by remember {
        mutableStateOf<String?>(null)
    }

    LaunchedEffect(Client.authClient().isLoggedIn()) {
        scope.launch {
            avatarUrl = if (!Client.authClient().isLoggedIn())
                null
            else {
                val userInfo = Client.authClient().getUserInfo()
                userInfo["avatarUrl"]
            }
        }
    }

    Box(
        modifier = Modifier
            .fillMaxSize()
    ) {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .background(Color.White)
                .align(Alignment.TopCenter)

        ) {
            Row (
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(
                        top = SpaceLarge,
                        bottom = SpaceSmall,
                        end = SpaceMediumLarge
                    ),
                horizontalArrangement = Arrangement.End
            ) {
                CircularButton(
                    Modifier,
                    true,
                    painterResource(res = Resources.images.personaSettings),
                    avatarUrl = avatarUrl
                )  {
                    val intent = Intent(context, SettingsActivity::class.java)
                    startForResult.launch(intent)
                }
            }
            Text(
                text = stringResource(res = Resources.strings.home.title) + suffix,
                fontSize = 40.sp,
                fontWeight = FontWeight.Bold,
                modifier = Modifier
                    .padding(vertical = SpaceSmall, horizontal = SpaceMediumLarge)
            )
            Text(
                text = stringResource(res = Resources.strings.home.subtitle),
                fontSize = startButtonLabelFontSize,
                modifier = Modifier
                    .padding(vertical = SpaceSmall, horizontal = SpaceMediumLarge)
            )
            if (withHistory) {
                PersonaRowV2(
                    personas = personas,
                    onHistoryOpened = {
                        onHistoryOpened(it)
                    },
                    onPageSelected = {ref, avatarId, idx ->
                        currentPersonaRef = ref
                        currentAvatarId = avatarId
                        currentIndex = idx
                    }
                )
            } else {
                PersonaColumn(
                    modifier = Modifier
                        .weight(1F)
                        .padding(horizontal = SpaceMediumLarge),
                    personas = personas,
                    currentIndex = currentIndex,
                    onClick = { ref, avatarId, idx ->
                        currentPersonaRef = ref
                        currentAvatarId = avatarId
                        currentIndex = idx
                    }
                )
            }
        }
        Button(
            onClick = {
                currentPersonaRef?.let { ref ->
                    currentAvatarId?.let { avatarId ->
                        Client.appConfig().lastSelectedAvatarId = avatarId
                        onStartButtonClick(ref, avatarId)
                    }
                }
            },
            enabled = currentIndex != null,
            modifier = Modifier
                .height(startButtonHeight)
                .fillMaxWidth()
                .padding(start = SpaceMediumLarge, end = SpaceMediumLarge, bottom = SpaceXLarge)
                .align(Alignment.BottomCenter),
            colors = ButtonDefaults.buttonColors(
                containerColor = Color.init(res = Resources.colors.accent),
                contentColor = Color.White,
                disabledContainerColor = Color(0xFFB5B5B5),
                disabledContentColor = Color(0xFF474747)
            )
        ) {
            Text(
                text = stringResource(res = Resources.strings.home.startConversation),
                fontSize = startButtonLabelFontSize
            )
        }
    }
}


@Composable
fun PersonaColumn(
    modifier: Modifier,
    personas: List<ApplicationInfo.Persona>,
    currentIndex: Int?,
    onClick: (String, String, Int) -> Unit
) {
    Column(
        modifier = modifier
            .verticalScroll(rememberScrollState())
    ) {
        personas.mapIndexed { index, personaItem ->
            PersonaCard(
                currentIndex = currentIndex,
                item = personaItem,
                index = index
            ) { ref, avatarId, personaIndex ->
                onClick(ref, avatarId, personaIndex)
            }
        }
        // Space for the button
        Spacer(modifier = Modifier.height(spacerHeight))
    }
}
private val spacerHeight = 96.dp
private val startButtonHeight = 96.dp
private val startButtonLabelFontSize = 18.sp
