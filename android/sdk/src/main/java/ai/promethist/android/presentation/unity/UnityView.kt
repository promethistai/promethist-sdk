package ai.promethist.android.presentation.unity

import android.view.ViewGroup
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.viewinterop.AndroidView
import com.unity3d.player.UnityPlayer

@Composable fun UnityView(
    modifier: Modifier = Modifier,
    unityPlayer: UnityPlayer
) {
    AndroidView(
        modifier = modifier.fillMaxSize(),
        factory = {
            unityPlayer.apply {
                layoutParams = ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT
                )
            }
        }
    )
}