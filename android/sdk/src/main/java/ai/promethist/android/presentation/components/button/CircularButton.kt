package ai.promethist.android.presentation.components.button

import ai.promethist.android.ext.init
import ai.promethist.android.presentation.components.ShadowIcon
import ai.promethist.client.resources.Resources
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.size
import androidx.compose.material3.IconButton
import androidx.compose.material3.IconButtonDefaults
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import coil.compose.AsyncImage

@Composable
fun CircularButton(
    modifier: Modifier,
    isActive: Boolean,
    painter: Painter,
    avatarUrl: String? = null,
    onClick: () -> Unit
    ) {
    val backgroundColor = if (isActive) Color.White else Color(0x33F5F5F7) // TODO resource
    val contentColor = if (isActive) Color.init(res = Resources.colors.accent) else Color.White
    IconButton(
        modifier = modifier
            .size(size),
        onClick = onClick,
        colors = IconButtonDefaults.iconButtonColors(containerColor = backgroundColor)
    ) {
        if (avatarUrl == null) {
            ShadowIcon(
                modifier = Modifier
                    .size(iconSize),
                painter = painter,
                contentDescription = null,
                tint = contentColor
            )
        } else {
            AsyncImage(
                modifier = Modifier.fillMaxSize(),
                contentScale = ContentScale.Crop,
                model = avatarUrl,
                contentDescription = null
            )
        }
    }
}
private val size: Dp = 60.dp
private val iconSize: Dp = 26.dp