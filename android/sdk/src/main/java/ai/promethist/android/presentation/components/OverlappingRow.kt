package ai.promethist.android.presentation.components

import androidx.annotation.FloatRange
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.Layout
import androidx.compose.ui.layout.MeasurePolicy

// Taken from https://proandroiddev.com/custom-layouts-with-jetpack-compose-bc1bdf10f5fd
@Composable
fun OverlappingRow(
    modifier: Modifier = Modifier,
    @FloatRange(from = 0.1,to = 1.0) overlapFactor:Float = 0.75f,
    content: @Composable () -> Unit,
){
    val measurePolicy = overlappingRowMeasurePolicy(overlapFactor)
    Layout(
        modifier = modifier,
        measurePolicy = measurePolicy,
        content = content
    )
}

private fun overlappingRowMeasurePolicy(overlapFactor: Float) = MeasurePolicy { measurables, constraints ->
    val placeables = measurables.map { measurable -> measurable.measure(constraints)}
    val height = placeables.maxOf { it.height }
    val width = (placeables.subList(1,placeables.size).sumOf { it.width  } * overlapFactor + placeables[0].width).toInt()
    layout(width,height) {
        var xPosition = 0
        placeables.forEach { placeable ->
            placeable.placeRelative(xPosition, 0, 0f)
            xPosition += (placeable.width * overlapFactor).toInt()
        }
    }
}
