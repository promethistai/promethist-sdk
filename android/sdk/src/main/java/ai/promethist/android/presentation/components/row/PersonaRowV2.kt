package ai.promethist.android.presentation.components.row

import ai.promethist.android.presentation.components.HistoryRange
import ai.promethist.android.presentation.components.HistoryView
import ai.promethist.android.presentation.components.pager.PersonaPagerV2
import ai.promethist.android.style.SpaceMediumLarge
import ai.promethist.client.model.ApplicationInfo
import ai.promethist.client.model.Message
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier

@Composable
fun PersonaRowV2(
    personas: List<ApplicationInfo.Persona>,
    onHistoryOpened: (List<Message>) -> Unit,
    onPageSelected: (String, String, Int) -> Unit
) {
    val histories = listOf(listOf<HistoryRange>()) // TODO real data

    var history by remember {
        mutableStateOf<List<HistoryRange>>(listOf())
    }

    Column {
        if (personas.isNotEmpty()) {
            PersonaPagerV2(personas = personas) { ref, avatarId, idx ->
                onPageSelected(ref, avatarId, idx)
                history = histories.getOrNull(idx) ?: listOf()
            }
        }
        HistoryView(
            modifier = Modifier.padding(horizontal = SpaceMediumLarge),
            items = history
        ) {
            onHistoryOpened(it)
        }
    }
}