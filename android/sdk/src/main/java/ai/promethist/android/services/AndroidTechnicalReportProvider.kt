package ai.promethist.android.services

import ai.promethist.client.model.CameraPermission
import ai.promethist.client.Client
import ai.promethist.client.model.MicPermission
import ai.promethist.client.utils.TechnicalReportProvider
import ai.promethist.client.model.TechnicalReport
import ai.promethist.util.getBuildConfigValue
import android.app.ActivityManager
import android.content.Context
import androidx.core.content.ContextCompat.getSystemService
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader

class AndroidTechnicalReportProvider(val context: Context): TechnicalReportProvider {
    override fun technicalReport(parameters: Map<String, String>): TechnicalReport = with(Client.appConfig()) {
        val memoryInfo = ActivityManager.MemoryInfo()
        (getSystemService(context, ActivityManager::class.java) as ActivityManager).getMemoryInfo(memoryInfo)
        val nativeHeapSize = memoryInfo.totalMem
        val nativeHeapFreeSize = memoryInfo.availMem
        val usedMemInBytes = nativeHeapSize - nativeHeapFreeSize
        val usedMemInPercentage = usedMemInBytes * 100.0 / nativeHeapSize
        val cpuUsage = sampleCPU()
        TechnicalReport(
            deviceId = deviceId,
            memoryUsageBytes = usedMemInBytes,
            memoryUsagePercent = usedMemInPercentage,
            cpuUsagePercent = cpuUsage,
            micPermission = this.micPermission == MicPermission.Granted,
            cameraPermission = this.cameraPermission == CameraPermission.Granted,
            videoMode = this.videoMode,
            avatarQualityLevel = this.avatarQualityLevel,
            arcFps = (parameters["fps"] ?: "0.0").replace(",", ".").toFloat(),
            arcMemory = (parameters["memory"] ?: "0.0").replace(",", ".").toFloat()
        )
    }

    private fun sampleCPU(): Double {
        val rate = 0
        try {
            var Result: String
            val p = Runtime.getRuntime().exec("top -n 1")
            val br = BufferedReader(InputStreamReader(p.inputStream))
            while (br.readLine().also { Result = it } != null) {
                if (Result.contains(getBuildConfigValue("APPLICATION_ID") as String? ?: return 0.0)) {
                    val info = Result.trim { it <= ' ' }
                        .replace(" +".toRegex(), " ").split(" ".toRegex())
                        .dropLastWhile { it.isEmpty() }
                        .toTypedArray()
                    return java.lang.Double.valueOf(info[9])
                }
            }
        } catch (e: IOException) {
            e.printStackTrace()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        }
        return rate.toDouble()
    }
}