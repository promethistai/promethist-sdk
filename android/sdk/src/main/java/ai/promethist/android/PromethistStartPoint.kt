package ai.promethist.android

enum class PromethistStartPoint {
    Onboarding, Main, AppKeyEntry, IntroVideo, AuthScreen, PersonaSelect, Permissions
}