package ai.promethist.android.presentation.components

import ai.promethist.android.R
import ai.promethist.android.presentation.components.graphics.VoiceGradient
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp

@Composable
fun VoiceRecorder(onClick: (Boolean) -> Unit) {
    var isRecording by remember { mutableStateOf(false) }
    Column(
        modifier = Modifier.fillMaxWidth(),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(
            text = if (isRecording) "Stop recording and send" else "Start recording",
            modifier = Modifier.padding(bottom = 16.dp),
            style = MaterialTheme.typography.titleLarge,
            color = Color.Black
        )
        Box(
            modifier = Modifier.size(96.dp),
            contentAlignment = Alignment.Center
        ) {
            VoiceGradient(modifier = Modifier.fillMaxSize(), showVoiceIndicator = isRecording, isRadial = true)
            Button(
                onClick = {
                    isRecording = !isRecording
                    onClick(isRecording)
                },
                modifier = Modifier.size(64.dp),
                contentPadding = PaddingValues(0.dp)
            ) {
                val resId = if (isRecording) R.drawable.baseline_stop_24 else R.drawable.baseline_fiber_manual_record_24
                val color = if (isRecording) Color.Black else Color.Red
                Image(
                    painter = painterResource(id = resId),
                    contentDescription = "Recording button icon",
                    colorFilter = ColorFilter.tint(color),
                    modifier = Modifier.size(48.dp)
                )
            }
        }

    }
}