package ai.promethist.android.presentation.components

import ai.promethist.SharedRes
import ai.promethist.android.ext.init
import ai.promethist.android.ext.onboardingTermsAccepted
import ai.promethist.android.ext.painterResource
import ai.promethist.android.ext.regularMaterial
import ai.promethist.android.ext.stringResource
import ai.promethist.android.presentation.components.text.URLContainer
import ai.promethist.android.presentation.components.text.URLText
import ai.promethist.android.style.SpaceLarge
import ai.promethist.android.style.SpaceMedium
import ai.promethist.android.style.SpaceMediumSmall
import ai.promethist.android.style.heading1
import ai.promethist.android.style.modal
import ai.promethist.client.common.AppConfig
import ai.promethist.client.ClientTarget
import ai.promethist.client.model.PersonaId
import ai.promethist.client.utils.PersonaResourcesManager
import ai.promethist.client.resources.Resources
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.core.Animatable
import androidx.compose.animation.core.tween
import androidx.compose.animation.slideInVertically
import androidx.compose.animation.slideOutVertically
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

@Composable
fun OnboardingView(
    appConfig: AppConfig,
    onOnboardingFinished: (Boolean) -> Unit
) {
    // val targetConfig = ClientTarget.currentTarget.config
    val coroutinesScope = rememberCoroutineScope()
    val personaId = PersonaId.fromAvatarId(appConfig.primaryPersonaId ?: "")
    val termsAndConditionsVisible = remember { mutableStateOf(false) }
    val dataProtectionVisible = remember { mutableStateOf(false) }
    val logoOffsetY = remember { Animatable(0f) }

    val context = LocalContext.current
    val termsURL = SharedRes.strings.onboarding_terms_and_conditions_url.getString(context)
    val termsLabel = SharedRes.strings.onboarding_terms_and_conditions_name.getString(context)
    val privacyPolicyURL = SharedRes.strings.onboarding_data_protection_url.getString(context)
    val privacyPolicyLabel = SharedRes.strings.onboarding_data_protection_name.getString(context)

    LaunchedEffect(appConfig) {
        if (appConfig.onboardingTermsAccepted) return@LaunchedEffect
        coroutinesScope.launch {
            delay(200)

            logoOffsetY.animateTo(
                targetValue = -250f,
                animationSpec = tween(durationMillis = logoAnimationDuration)
            )

            if (!appConfig.termsAndConditionsAccepted) {
                termsAndConditionsVisible.value = true
            } else if (!appConfig.dataProtectionAccepted) {
                dataProtectionVisible.value = true
            }
        }
    }

    Box(
        modifier = Modifier
            .fillMaxSize()
    ) {
        Image(
            painter = painterResource(
                res = PersonaResourcesManager.getBackground(personaId)
            ),
            contentDescription = null,
            contentScale = ContentScale.Crop
        )

//        if (targetConfig.backgroundColorTillPersonaSelection) {
//            Box(
//                modifier = Modifier
//                    .fillMaxSize()
//                    .background(Color.init(res = Resources.colors.backgroundOverlay))
//            )
//        }

        Image(
            modifier = Modifier
                .size(logoSize)
                .align(Alignment.Center)
                .offset(y = logoOffsetY.value.dp),
            painter = painterResource(Resources.images.splashLogo),
            contentDescription = null
        )

        AnimatedVisibility(
            modifier = Modifier
                .align(Alignment.BottomCenter),
            visible = termsAndConditionsVisible.value,
            enter = slideInVertically(initialOffsetY = { it / 2 }),
            exit = slideOutVertically(targetOffsetY = { it / 2 })
        ) {
            TermsSheet(
                title = stringResource(res = Resources.strings.onboarding.termsAndConditionsTitle),
                text = stringResource(res = Resources.strings.onboarding.termsAndConditionsText),
                onAccept = {
                    appConfig.termsAndConditionsAccepted = true
                    termsAndConditionsVisible.value = false

                    if (!appConfig.dataProtectionAccepted) {
                        dataProtectionVisible.value = true
                    } else {
                        onOnboardingFinished(true)
                    }
                },
                urls = listOf(
                    URLContainer(termsLabel, termsURL),
                    URLContainer(privacyPolicyLabel, privacyPolicyURL)
                ),
                onDecline = {
                    onOnboardingFinished(false)
                }
            )
        }

        AnimatedVisibility(
            modifier = Modifier
                .align(Alignment.BottomCenter),
            visible = dataProtectionVisible.value,
            enter = slideInVertically(initialOffsetY = { it / 2 }),
            exit = slideOutVertically(targetOffsetY = { it / 2 })
        ) {
            TermsSheet(
                title = stringResource(res = Resources.strings.onboarding.dataProtectionTitle),
                text = stringResource(res = Resources.strings.onboarding.dataProtectionText),
                onAccept = {
                    appConfig.dataProtectionAccepted = true
                    dataProtectionVisible.value = false

                    coroutinesScope.launch {
                        logoOffsetY.animateTo(
                            targetValue = 0f,
                            animationSpec = tween(durationMillis = logoAnimationDuration)
                        )
                        onOnboardingFinished(true)
                    }
                },
                urls = listOf(),
                onDecline = {
                    onOnboardingFinished(false)
                }
            )
        }
    }
}

@Composable
private fun TermsSheet(
    modifier: Modifier = Modifier,
    title: String,
    text: String,
    urls: List<URLContainer>,
    onAccept: () -> Unit,
    onDecline: () -> Unit
) {
    Card(
        modifier = modifier,
        shape = RoundedCornerShape(topStart = sheetCornerRadius, topEnd = sheetCornerRadius),
        colors = CardDefaults.cardColors(containerColor = regularMaterial())
    ) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .padding(SpaceLarge)
                .padding(bottom = SpaceLarge),
            verticalArrangement = Arrangement.spacedBy(SpaceMedium),
            horizontalAlignment = Alignment.Start
        ) {
            Text(
                text = title,
                style = heading1
            )

            URLText(
                text = text,
                urls = urls,
                style = modal.copy(fontWeight = FontWeight.Normal)
            )

            Row(
                modifier = Modifier
                    .fillMaxWidth(),
                horizontalArrangement = Arrangement.spacedBy(SpaceMediumSmall)
            ) {
                Spacer(modifier = Modifier.weight(1f))

                Button(
                    onClick = onDecline,
                    shape = RoundedCornerShape(10.dp),
                    colors = ButtonDefaults.buttonColors(
                        containerColor = Color.Gray.copy(alpha = .2f)
                    ),
                    contentPadding = PaddingValues(horizontal = SpaceMediumSmall)
                ) {
                    Text(
                        text = stringResource(res = Resources.strings.onboarding.decline),
                        style = modal.copy(fontWeight = FontWeight.SemiBold),
                        color = Color.init(res = Resources.colors.primaryLabel)
                    )
                }

                Button(
                    onClick = onAccept,
                    shape = RoundedCornerShape(10.dp),
                    colors = ButtonDefaults.buttonColors(
                        containerColor = Color.Gray.copy(alpha = .2f)
                    ),
                    contentPadding = PaddingValues(horizontal = SpaceMediumSmall)
                ) {
                    Text(
                        text = stringResource(res = Resources.strings.onboarding.accept),
                        style = modal.copy(fontWeight = FontWeight.SemiBold),
                        color = Color.init(res = Resources.colors.primaryLabel)
                    )
                }

                Spacer(modifier = Modifier.weight(1f))
            }
        }
    }
}

private val logoSize = 288.dp
private val logoAnimationDuration = 500
private val sheetCornerRadius = 36.dp

