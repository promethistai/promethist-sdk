package ai.promethist.android.presentation.components.screen

import ai.promethist.android.presentation.components.input.SearchBar
import ai.promethist.android.presentation.components.row.MessageRow
import ai.promethist.android.style.SpaceLarge
import ai.promethist.android.style.SpaceMedium
import ai.promethist.android.style.SpaceMediumLarge
import ai.promethist.client.model.Message
import androidx.compose.animation.animateContentSize
import androidx.compose.animation.core.*
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.*
import androidx.compose.runtime.*
import androidx.compose.ui.*
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.platform.LocalView
import androidx.compose.ui.unit.dp
import kotlinx.coroutines.launch

@Composable
fun TextView(
    messages: List<Message>
) {
    var query by remember { mutableStateOf("") }
    var currentIndex by remember { mutableStateOf<Int?>(null) }
    var currentOccurrences by remember { mutableStateOf(0) }
    val listState = rememberLazyListState()
    val scope = rememberCoroutineScope()

    LaunchedEffect(currentIndex) {
        val message = messages.firstOrNull { it.searchedIndexes?.contains(currentIndex) ?: false }
        scope.launch {
            message?.let {
                listState.scrollToItem(messages.indexOf(message), -300)
            }
        }
    }

    Box(
        modifier = Modifier
            .fillMaxSize()
    ) {
        MessageColumn(
            messages = messages,
            listState = listState,
            query = query,
            currentIndex = currentIndex
        )
        GradientBox(modifier = Modifier.align(Alignment.TopCenter), true)
        GradientBox(modifier = Modifier.align(Alignment.BottomCenter))
        Box(
            modifier = Modifier.fillMaxSize(),
            contentAlignment = Alignment.BottomCenter
        ) {
            Column(
                modifier = Modifier.fillMaxWidth(),
                horizontalAlignment = Alignment.Start
            ) {
                Box(
                    modifier = Modifier
                        .animateContentSize(tween(300)) // animate the change if the keyboard height changes
                ) {
                    SearchBar(
                        query = query,
                        index = currentIndex?.inc() ?: 0, // TODO fix
                        total = currentOccurrences,
                        onValueChange = {
                            query = it
                            if (it.isNotBlank()) {
                                currentOccurrences = messages.search(query)
                                currentIndex = if (currentOccurrences != 0) 0 else null
                            } else {
                                currentOccurrences = 0
                                currentIndex = null
                            }
                        },
                        onNext = {
                            currentIndex = currentIndex?.plus(1)?.rem(currentOccurrences)
                            val message = messages.firstOrNull { it.searchedIndexes?.contains(currentIndex) ?: false }
                            scope.launch {
                                message?.let {
                                    listState.scrollToItem(messages.indexOf(message))
                                }
                            }
                        },
                        onPrevious = {
                            val updated = currentIndex?.minus(1)
                            if (updated != null) {
                                currentIndex = if (updated < 0) currentOccurrences - 1 else updated.rem(currentOccurrences)
                            }
                        }
                    )
                }
            }
        }
    }
    // Swift: .onAppear { UITableView.appearance().contentInset.top = -35 }
    // Not needed in Compose, but if you want to replicate some offset logic, you can place it here
}

@Composable
fun MessageColumn(
    messages: List<Message>,
    listState: LazyListState,
    query: String,
    currentIndex: Int?
) {
    val view = LocalView.current
    val bottomInset = view.rootWindowInsets?.systemWindowInsetBottom ?: 0
    val topInset = view.rootWindowInsets?.systemWindowInsetTop ?: 0
    val density = LocalDensity.current
    val topOffset = 80.dp + with (density) { topInset.toDp() } + SpaceMediumLarge
    val bottomOffset = 60.dp + with (density) { bottomInset.toDp() } + SpaceMediumLarge
    LazyColumn(
        state = listState,
        modifier = Modifier
            .fillMaxSize()
            .padding(vertical = SpaceLarge)
    ) {
        items(messages) {   message ->
            val isFirst = message.id == messages.firstOrNull()?.id
            val isLast = message.id == messages.lastOrNull()?.id

            // Swift used ZStack, so we just place the TranscriptionRow if text is non-empty
            if (message.text.isNotEmpty()) {
                Box(
                    modifier = Modifier
                        .padding(
                            start = SpaceMediumLarge,
                            end = SpaceMediumLarge,
                            bottom = SpaceMedium
                        )
                        .padding(top = if (isFirst) topOffset else 0.dp)
                        .padding(bottom = if (isLast) bottomOffset else 0.dp)
                ) {
                    MessageRow(
                        message = message,
                        currentIndex = currentIndex,
                        query = query
                    )
                }
            }
        }
    }
}

@Composable
fun GradientBox(
    modifier: Modifier,
    reversed: Boolean = false
) {
    var colorList = listOf(
        Color.Transparent,
        Color.Black
    )
    if (reversed)
        colorList = colorList.reversed()
    Box(
        modifier = modifier
            .fillMaxWidth()
            .height(200.dp)
            .background(
                brush = Brush.verticalGradient(
                    colorList
                )
            )
    )
}

fun List<Message>.search(searched: String): Int {
    val regex = Regex(searched, RegexOption.IGNORE_CASE)
    var i = 0
    this.forEach { message ->
        val occurrences = regex.findAll(message.text).toList()
        if (occurrences.isNotEmpty()) {
            message.searchedIndexes = mutableListOf()
        }
        occurrences.forEach {
            message.searchedIndexes?.add(i)
            i++
        }
    }
    return i
}
