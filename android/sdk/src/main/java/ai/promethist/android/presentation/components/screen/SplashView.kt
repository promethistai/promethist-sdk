package ai.promethist.android.presentation.components.screen

import ai.promethist.android.R
import ai.promethist.android.ext.painterResource
import ai.promethist.android.style.SpaceXXXLarge
import ai.promethist.client.resources.Resources
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.LinearProgressIndicator
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.StrokeCap
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.unit.dp
import com.airbnb.lottie.compose.LottieAnimation
import com.airbnb.lottie.compose.LottieCompositionSpec
import com.airbnb.lottie.compose.LottieConstants
import com.airbnb.lottie.compose.animateLottieCompositionAsState
import com.airbnb.lottie.compose.rememberLottieComposition

@Composable
fun SplashView(
    progress: Float?
) {
    val logoHeight = 50.dp
    val indicatorHeight = 15.dp
    val roundedCornerRadius = (indicatorHeight + 1.dp) / 2
    Box (
        modifier = Modifier
            .fillMaxSize()
            .clickable(false) {}
            .background(color = colorResource(id = Resources.colors.accent.resourceId))
    ) {
        Image(
            modifier = Modifier
                .height(logoHeight)
                .align(Alignment.Center),
            painter = painterResource(res = Resources.images.splashLogo),
            contentDescription = null
        )

        progress?.let {
            LinearProgressIndicator(
                progress = { progress },
                modifier = Modifier
                    .widthIn(max = 500.dp)
                    .offset(y = 200.dp)
                    .padding(horizontal = SpaceXXXLarge)
                    .height(indicatorHeight)
                    .border(
                        2.dp,
                        color = Color.White,
                        shape = RoundedCornerShape(roundedCornerRadius)
                    )
                    .align(Alignment.Center),
                color = Color.White,
                strokeCap = StrokeCap.Round,
                trackColor = Color.Transparent
            )
        }
        if (progress == null) {
            Box(modifier = Modifier
                .align(Alignment.Center)
                .offset(y = 130.dp)
                .size(100.dp)
            ) {
                Loader()
            }
        }
    }
}

@Composable
fun Loader() {
    val composition by rememberLottieComposition(LottieCompositionSpec.RawRes(R.raw.loading_alt))
    val progress by animateLottieCompositionAsState(
        composition,
        iterations = LottieConstants.IterateForever
    )
    LottieAnimation(
        composition = composition,
        progress = { progress },
    )
}