package ai.promethist.android.presentation.components.menu

import ai.promethist.android.ext.init
import ai.promethist.android.style.SpaceSmall
import ai.promethist.android.style.SpaceXSmall
import ai.promethist.client.resources.Resources
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.LinearProgressIndicator
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

@Composable
fun MenuProgressTile(
    modifier: Modifier = Modifier,
    progress: Float?,
    goal: String? = null,
    text: String? = null,
    height: Dp = 8.dp,
    fontSize: TextUnit = 16.sp
) {
    val alignment: Arrangement.Horizontal =
        if ((goal ?: "").isBlank()) Arrangement.End else Arrangement.Start

    Column(
        modifier = modifier.fillMaxWidth(),
        verticalArrangement = Arrangement.spacedBy(SpaceXSmall)
    ) {
        Row(
            modifier = Modifier.fillMaxWidth(),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = alignment
        ) {
            Text(
                modifier = Modifier,
                text = getProgressText(progress, goal, text),
                fontSize = fontSize,
                style = MaterialTheme.typography.labelLarge.copy(fontWeight = FontWeight.Normal),
                color = MaterialTheme.colorScheme.onBackground
            )
        }
        progress?.let {
            LinearProgressIndicator(
                modifier = Modifier
                    .clip(CircleShape)
                    .fillMaxWidth()
                    .height(height),
                progress = it,
                color = Color.init(res = Resources.colors.growthTint)
            )
        }
    }
}

@Composable
fun MenuProgressTileV2(
    modifier: Modifier = Modifier,
    progress: Float,
    height: Dp = 8.dp,
    fontSize: TextUnit = 16.sp,
    progressBarTint: Color
) {

    Row(
        modifier = modifier.fillMaxWidth(),
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.spacedBy(SpaceSmall)
    ) {
        LinearProgressIndicator(
            modifier = Modifier
                .weight(1f)
                .clip(CircleShape)
                .fillMaxWidth()
                .height(height),
            progress = progress,
            color = progressBarTint,
            trackColor = Color.init(res = Resources.colors.growthProgressBackground)
        )

        Text(
            modifier = Modifier,
            text = getProgressText(progress, null, null),
            fontSize = fontSize,
            style = MaterialTheme.typography.labelLarge.copy(fontWeight = FontWeight.Normal),
            color = MaterialTheme.colorScheme.onBackground
        )
    }
}

fun getProgressText(progress: Float?, goal: String?, text: String?): String {
    var finalText = if (progress != null) {
        "${(progress * 100).toInt()}%"
    } else {
        ""
    }
    text?.let { finalText += " $it" }
    goal?.let { finalText += " $it" }
    return finalText
}