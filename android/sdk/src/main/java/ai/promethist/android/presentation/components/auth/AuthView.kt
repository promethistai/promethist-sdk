package ai.promethist.android.presentation.components.auth

import ai.promethist.client.Client
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import io.ktor.http.Url

@Composable
fun AuthView(
    shouldHandleCallback: (Url) -> Boolean,
    handleCallback: (Url) -> Unit
) {
    var isLoading by remember { mutableStateOf(false) }
    val url = Client.authClient().getAuthorizationUrl()
    Box(
        modifier = Modifier.fillMaxSize()
    ) {
        AuthWebView(
            url = url.toString(),
            shouldHandleCallback = shouldHandleCallback,
            handleCallback = handleCallback
        )

        if (isLoading) {
            Box(
                contentAlignment = Alignment.Center,
                modifier = Modifier.fillMaxSize()
            ) {
                CircularProgressIndicator()
            }
        }
    }
}