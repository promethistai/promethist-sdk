package ai.promethist.android.services.speechRecognizer.duplex

import ai.promethist.android.R
import ai.promethist.android.services.speechRecognizer.duplex.DuplexAndroidSpeechRecognizer.Companion.LANGUAGE_CODE
import ai.promethist.android.services.speechRecognizer.duplex.DuplexAndroidSpeechRecognizer.Companion.SAMPLE_RATE
import android.content.Context
import com.google.api.gax.core.CredentialsProvider
import com.google.auth.oauth2.ServiceAccountCredentials
import com.google.cloud.speech.v1.RecognitionConfig
import com.google.cloud.speech.v1.SpeechClient
import com.google.cloud.speech.v1.SpeechContext
import com.google.cloud.speech.v1.StreamingRecognitionConfig
import com.google.cloud.speech.v1.StreamingRecognizeRequest
import com.google.cloud.speech.v1.stub.GrpcSpeechStub
import com.google.cloud.speech.v1.stub.SpeechStubSettings

/**
 * A utility class for interfacing with the Google Speech-to-Text API.
 *
 * This class provides methods for creating a `SpeechClient`, building streaming recognition requests,
 * and managing credentials for Google Cloud Speech-to-Text API integration.
 */
class GoogleSpeechClient {

    companion object {
        private const val HOSTNAME = "speech.googleapis.com"
        private const val PORT = 443
    }

    /**
     * Creates a `SpeechClient` instance configured for streaming recognition.
     *
     * This method initializes the client with appropriate credentials and endpoint configuration.
     *
     * @param context The application context used to access credentials.
     * @return A configured `SpeechClient` instance.
     */
    fun create(context: Context): SpeechClient {
        var grpcStub: GrpcSpeechStub? = null
        SpeechStubSettings.newBuilder()?.apply {
            credentialsProvider = getCredentialsProvider(context)
            endpoint = "$HOSTNAME:$PORT"
            grpcStub = GrpcSpeechStub.create(build())
        }
        return SpeechClient.create(grpcStub)
    }

    /**
     * Builds a `StreamingRecognizeRequest` configured for real-time audio recognition.
     *
     * @return A `StreamingRecognizeRequest` ready for use with a `SpeechClient`.
     */
    fun buildStreamingRecognizeRequest(): StreamingRecognizeRequest {
        val builder = StreamingRecognizeRequest.newBuilder()
            .setStreamingConfig(
                StreamingRecognitionConfig.newBuilder()
                    .setConfig(
                        RecognitionConfig.newBuilder()
                            .addSpeechContexts(
                                SpeechContext.newBuilder() // TODO add expected speech phrases?
                                    .addPhrases("datový balíček")
                                    .addPhrases("roaming")
                                    .addPhrases("ano")
                                    .addPhrases("ne")
                                    .addPhrases("špatně")
                                    .addPhrases("to stačí")
                                    .addPhrases("ne chci")
                                    .addPhrases("ne, chci")
                                    .build()
                            )
                            .setLanguageCode(LANGUAGE_CODE)
                            .setEncoding(RecognitionConfig.AudioEncoding.LINEAR16)
                            .setSampleRateHertz(SAMPLE_RATE)
                            .setUseEnhanced(true)
                            .setProfanityFilter(false)
                            .setEnableAutomaticPunctuation(true)
                            .setMaxAlternatives(3)
                            //.setModel("command_and_search") // TODO find the best model
                            .build()
                    )
                    .setInterimResults(true)
                    .setSingleUtterance(false)
                    .build()
            )
        return builder.build()
    }

    /**
     * Retrieves a `CredentialsProvider` for authenticating with Google Cloud APIs.
     *
     * @return A configured `CredentialsProvider` instance.
     */
    private fun getCredentialsProvider(context: Context): CredentialsProvider {
        return CredentialsProvider {
            context.resources.openRawResource(R.raw.creds).let {
                ServiceAccountCredentials.fromStream(it)
            }
        }
    }
}