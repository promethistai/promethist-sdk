package ai.promethist.android.presentation.components.input

import ai.promethist.channel.command.RequestText
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier

@Composable
fun UserTextField(
    payload: RequestText,
    minLines: Int = 20,
    maxLines: Int = 20,
    onSubmit: (String) -> Unit
) {
    var currentText by remember { mutableStateOf(payload.text ?: "") }
    Column(
        modifier = Modifier.fillMaxWidth(),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        payload.title?.let {
            Text(
                text = it,
                style = MaterialTheme.typography.titleLarge
            )
        }
        TextField(
            value = currentText,
            onValueChange = { currentText = it },
            modifier = Modifier.fillMaxWidth(),
            minLines = minLines,
            maxLines = maxLines
        )
        Button(
            onClick = { onSubmit(currentText) },
            enabled = currentText.isNotEmpty()
        ) {
            Text(text = "Send")
        }
    }
}