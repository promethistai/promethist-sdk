package ai.promethist.android.services.speechRecognizer.duplex

import ai.promethist.android.Promethist
import ai.promethist.android.services.AndroidAvatarPlayerController
import ai.promethist.android.utils.AppEvent
import ai.promethist.android.utils.AppEventBus
import ai.promethist.client.Client
import ai.promethist.client.speech.DuplexMode
import ai.promethist.client.speech.SpeechRecognizerController
import ai.promethist.client.speech.SpeechRecognizerControllerCallback
import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.media.AudioFormat
import android.media.AudioManager
import android.media.AudioRecord
import android.media.MediaRecorder
import android.os.Process
import android.util.Log
import android.widget.Toast
import com.google.api.gax.rpc.ClientStream
import com.google.api.gax.rpc.ResponseObserver
import com.google.api.gax.rpc.StreamController
import com.google.cloud.speech.v1.SpeechClient
import com.google.cloud.speech.v1.StreamingRecognizeRequest
import com.google.cloud.speech.v1.StreamingRecognizeResponse
import com.google.protobuf.ByteString
import com.konovalov.vad.silero.Vad
import com.konovalov.vad.silero.VadSilero
import com.konovalov.vad.silero.config.FrameSize
import com.konovalov.vad.silero.config.Mode
import com.konovalov.vad.silero.config.SampleRate
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.suspendCancellableCoroutine
import kotlinx.datetime.Clock
import kotlinx.datetime.Instant
import kotlin.coroutines.resume
import kotlin.time.Duration.Companion.seconds

/**
 * Class that manages duplex speech recognition allowing barge-in and voice activity detection (VAD)
 * for the application.
 *
 * This class is responsible for interacting with Google's Speech-to-Text API, handling voice recording,
 * processing audio data, and providing real-time transcriptions during speech recognition. It also incorporates
 * voice activity detection (VAD) to detect when speech begins and ends, and manages duplex communication.
 */
class DuplexAndroidSpeechRecognizer(
    private val context: Context,
    private val promethist: Promethist
): SpeechRecognizerController {

    private var _callback: SpeechRecognizerControllerCallback? = null

    // Dependencies
    private val audioRecorderFx = AudioRecorderFx()
    private val loudnessMeter = LoudnessMeter()
    private val sampleRateConverter = SampleRateConverter()
    private val externalScope = CoroutineScope(SupervisorJob())
    private val googleSpeechClient = GoogleSpeechClient()
    private var speechClient: SpeechClient? = null
    private var clientStream: ClientStream<StreamingRecognizeRequest>? = null
    private lateinit var vad: VadSilero
    var voiceRecorder: AudioRecord? = null

    // State properties
    private var currentTranscript: String = ""
    private var lastTranscriptUpdateTime: Instant? = null
    private var isListening = false
    private var isActive = false
    private var isProcessing = false
    private var isSpeaking: Boolean = false
    private var isVadAvailable: Boolean = true
    private var isPrivileged: Boolean = false
    var isRecording = false

    // Constants
    val isStorageEnabled: Boolean get() = false
    private val duplexMode: DuplexMode get() = Client.appConfig().duplexMode
    private val isSpeechRecognitionActive: Boolean get() =
        speechClient != null && clientStream != null

    companion object {
        val START_UP_DELAY = 1.seconds
        val END_OF_UTTERANCE_TIMEOUT = (1.8).seconds
        const val SAMPLE_RATE = 24000
        const val LANGUAGE_CODE = "cs-CZ"
        const val CHANNEL_CONFIG: Int = AudioFormat.CHANNEL_IN_MONO
        const val AUDIO_FORMAT: Int = AudioFormat.ENCODING_PCM_16BIT
        const val BUFFER_SIZE: Int = 1024
        const val LOUDNESS_THRESHOLD_DB = 42
        const val SILENCE_DURATION_MS = 900
        const val SPEECH_DURATION_MS = 5
        const val TAG = "DuplexAndroidSpeechRecognizer"
    }

    // == Lifecycle ==================================================================

    init {
        externalScope.launch {
            delay(START_UP_DELAY)
            start()
        }
    }

    /**
     * Starts the speech recognizer by setting up required resources, permissions,
     * and initializing the audio recording and speech client.
     */
    suspend fun start() {
        // Configure audio settings
        setupAudioManager(context)

        // Initialize Voice Activity Detection if available
        setupVad()

        // Request necessary permissions
        val isAudioPermissionGranted = askPermissionsAsync()
        if (!isAudioPermissionGranted) {
            Toast.makeText(context, "Record permission is required, please allow it", Toast.LENGTH_LONG).show()
            return
        }

        // Create speech client
        speechClient = googleSpeechClient.create(context)
        initAudioRecordWithPlugins()
        observeAppEventBus()

        // Start recording either to storage or directly to audio stream
        if (isStorageEnabled) {
            startRecordingToStorage(context)
        } else {
            // Start audio streaming with/without VAD
            if (isVadAvailable) {
                startAudioStreamWithVAD()
            } else {
                startAudioStream()
            }
        }
    }

    private fun observeAppEventBus() {
        externalScope.launch {
            AppEventBus.subscribe(this) {
                when (it) {
                    AppEvent.OnAvatarAudioStarted -> onAudioPlaybackStarted()
                    else -> Unit
                }
            }
        }
    }

    private fun onAudioPlaybackStarted() {
        isListening = true
        // Start speech recognition when playback starts
        if (!isSpeechRecognitionActive) {
            Log.i(TAG, "onAudioPlaybackStarted")
            stopSpeechRecognition()
            initClientStream()
        }
    }

    private fun askPermissions(callback: (Boolean) -> Unit) {
        promethist.requestPermission(Manifest.permission.RECORD_AUDIO, callback)
    }

    private suspend fun askPermissionsAsync(): Boolean = suspendCancellableCoroutine { continuation ->
        askPermissions { granted ->
            continuation.resume(granted)
        }
    }

    // == Recorder ==================================================================

    /**
     * Initializes Voice Activity Detection (VAD) settings and the necessary configuration.
     */
    private fun setupVad() {
        try {
            vad = Vad.builder()
                .setContext(context)
                .setSampleRate(SampleRate.SAMPLE_RATE_16K)
                .setFrameSize(FrameSize.FRAME_SIZE_512)
                .setMode(Mode.NORMAL)
                .setSilenceDurationMs(SILENCE_DURATION_MS)
                .setSpeechDurationMs(SPEECH_DURATION_MS)
                .build()
        } catch (e: Exception) {
            isVadAvailable = false
            Log.e(TAG, "VAD is not available")
        }
    }

    /**
     * Configures the audio manager for optimal voice communication during speech recognition.
     */
    private fun setupAudioManager(context: Context) {
        val audioManager = context.getSystemService(Context.AUDIO_SERVICE) as AudioManager
        // Set MODE_IN_COMMUNICATION to require ANC
        audioManager.mode = AudioManager.MODE_IN_COMMUNICATION
        // Force audio output to loud speaker
        @Suppress("DEPRECATION")
        audioManager.isSpeakerphoneOn = true
        Log.i(TAG, "AudioManager mode set to MODE_IN_COMMUNICATION")
    }

    /**
     * Configures the audio recorder.
     */
    @SuppressLint("MissingPermission")
    private fun initAudioRecordWithPlugins() {
        if (voiceRecorder != null) return

        try {
            // Init voice recorder
            val voiceRecorder = AudioRecord(
                MediaRecorder.AudioSource.VOICE_COMMUNICATION,
                SAMPLE_RATE,
                CHANNEL_CONFIG,
                AUDIO_FORMAT,
                BUFFER_SIZE * 10
            )
            this.voiceRecorder = voiceRecorder

            // Apply FX
            val sessionId = voiceRecorder.audioSessionId
            audioRecorderFx.setAcousticEchoCancelerEnabled(sessionId)
            audioRecorderFx.setNoiseSuppressionEnabled(sessionId)
            audioRecorderFx.setAutomaticGainControlEnabled(sessionId, isEnabled = false)

            Log.i(TAG, "AudioRecord created")
        } catch (e: Exception) {
            Log.e(TAG, "Error while initializing", e)
        }
    }

    /**
     * Initializes the client stream for Google Cloud's Speech-to-Text API.
     */
    private fun initClientStream() {
        try {
            if (clientStream != null) return

            // Init connection to Google STT
            clientStream = speechClient?.streamingRecognizeCallable()?.splitCall(callback)
            val request = googleSpeechClient.buildStreamingRecognizeRequest()
            clientStream?.send(request)

            // Send empty content to ensure correct connection
            clientStream?.send(
                StreamingRecognizeRequest.newBuilder()
                    .setAudioContent(ByteString.EMPTY).build()
            )
        } catch (e: Exception) {
            Log.e(TAG, "Could not initialize ClientStream", e)
        }
    }

    /**
     * Starts the continuous microphone audio stream for speech recognition.
     *
     * This function initializes and starts the recording of audio data using the device's microphone.
     * It continuously reads audio data into a buffer and sends the buffered audio data for transcription.
     *
     * This mechanism is used for devices not supporting VAD, possibly lower quality.
     */
    private fun startAudioStream() {
        if (voiceRecorder == null) {
            Log.e(TAG, "voiceRecorder is not initialized")
            return
        }
        Log.d(TAG, "Audio stream started without VAD")

        var buffer = ByteArray(BUFFER_SIZE)
        val audioThread = Thread {
            Process.setThreadPriority(Process.THREAD_PRIORITY_AUDIO)
            try {
                voiceRecorder?.apply {
                    startRecording()

                    while (true) {
                        // Read microphone data
                        val bytesRead = read(buffer, 0, buffer.size)

                        // Send downsampled data for loudness check
                        val downSampledBytes = sampleRateConverter.downSampleTo16kHz(
                            input = buffer,
                            inputSampleRate = 24000,
                            outputSampleRate = 16000
                        )
                        val isLoudSpeech = checkLoudness(downSampledBytes)

                        // Send data to Google STT if active
                        if (isActive && bytesRead > 0) {
                            if ((isLoudSpeech || isPrivileged || currentTranscript.isNotBlank())) {
                                recognize(buffer.copyOf(bytesRead), bytesRead)
                            }
                        }

                        // Empty buffer
                        if (!isActive) {
                            buffer = ByteArray(BUFFER_SIZE)
                        }
                    }
                }
            } catch (e: Exception) {
                Log.e(TAG, "Error in audio streaming: ${e.message}")
                e.printStackTrace()
            } finally {
                voiceRecorder?.stop()
                voiceRecorder?.release()
                Log.d(TAG, "Audio stream stopped")
            }
        }
        audioThread.start()
    }

    /**
     * Starts the audio stream with Voice Activity Detection (VAD) for speech recognition.
     *
     * This function is similar to `startAudioStream`, but it incorporates Voice Activity Detection (VAD)
     * to detect the presence of speech in the audio data before sending it for recognition. It reads audio data
     * from the microphone, down samples the audio to a lower sample rate (16 kHz), and processes it through VAD.
     * Only when speech is detected (using the VAD and loudness checks), the audio data is sent to the recognition service.
     */
    private fun startAudioStreamWithVAD() {
        if (voiceRecorder == null) {
            Log.e(TAG, "voiceRecorder is not initialized")
            return
        }
        Log.d(TAG, "Audio stream started with VAD")

        var buffer = ByteArray(BUFFER_SIZE)
        val audioBuffer = mutableListOf<ByteArray>()
        val bufferLock = Object()
        val audioThread = Thread {
            Process.setThreadPriority(Process.THREAD_PRIORITY_AUDIO)
            try {
                voiceRecorder?.apply {
                    startRecording()

                    while (true) {
                        // Read microphone data
                        val bytesRead = read(buffer, 0, buffer.size)

                        // Send downsampled data to VAD engine for speech detection
                        val downSampledBytes = sampleRateConverter.downSampleTo16kHz(
                            input = buffer,
                            inputSampleRate = 24000,
                            outputSampleRate = 16000
                        )
                        if (isActive) {
                            checkVad(downSampledBytes)
                        }

                        if (bytesRead > 0) {
                            // Buffer non-speech audio data for using in advance when speech detected
                            if (!isSpeaking) {
                                synchronized(bufferLock) {
                                    audioBuffer.add(buffer.copyOf(bytesRead))
                                    if (audioBuffer.size > SAMPLE_RATE / BUFFER_SIZE * 0.95) {
                                        audioBuffer.removeAt(0)
                                    }
                                }
                            }

                            if (isSpeaking || isPrivileged || currentTranscript.isNotBlank()) {
                                synchronized(bufferLock) {
                                    // Send the buffered data (few ms of audio before speech detection)
                                    audioBuffer.forEach { bufferedData ->
                                        recognize(bufferedData, bufferedData.size)
                                    }
                                    audioBuffer.clear()

                                    // Followed by realtime audio
                                    recognize(buffer.copyOf(bytesRead), bytesRead)
                                }
                            }
                        }

                        // Empty buffers
                        if (!isActive) {
                            buffer = ByteArray(BUFFER_SIZE)
                            audioBuffer.clear()
                        }
                    }
                }
            } catch (e: Exception) {
                Log.e(TAG, "Error in audio streaming: ${e.message}")
                e.printStackTrace()
            } finally {
                voiceRecorder?.stop()
                voiceRecorder?.release()
                Log.d(TAG, "Audio stream stopped")
            }
        }
        audioThread.start()
    }

    /**
     * Checks if the recorded audio data contains speech using Voice Activity Detection (VAD).
     *
     * The function works as follows:
     * 2. It calculates the loudness of the audio using a loudness meter and compares it with a predefined loudness threshold.
     * 3. If speech is detected (using the VAD system) and the loudness is above the threshold, or if the speech is privileged,
     *    it marks the user as speaking and mutes system output audio.
     * 4. If speech is not detected or the loudness is not sufficient, and the user was previously marked as speaking,
     *    it un-mutes the system music audio and updates the speech state to reflect the user is no longer speaking.
     *
     * @param audioData The audio data with 16000Hz sample rate.
     */
    private fun checkVad(audioData: ByteArray) {
        if (!isVadAvailable) {
            return
        }

        val audioManager = context.getSystemService(Context.AUDIO_SERVICE) as AudioManager
        val isLoudSpeech = checkLoudness(audioData)

        if (vad.isSpeech(audioData) && (isLoudSpeech || isPrivileged || currentTranscript.isNotBlank())) {
            if (isSpeaking) return
            Log.i(TAG, "Speech detected")
            isSpeaking = true
            @Suppress("DEPRECATION")
            audioManager.setStreamMute(AudioManager.STREAM_MUSIC, true)
        } else {
            if (!isSpeaking) return
            Log.i(TAG, "Speech ended")
            isSpeaking = false
            @Suppress("DEPRECATION")
            audioManager.setStreamMute(AudioManager.STREAM_MUSIC, false)
        }
    }

    /**
     * Determines whether the given audio data represents loud speech.
     */
    private fun checkLoudness(audioData: ByteArray): Boolean {
        val loudness = loudnessMeter.calculateLoudness(audioData)
        //Log.d(TAG, "Loudness: $loudness")
        val isLoudSpeech = loudness > LOUDNESS_THRESHOLD_DB
        return isLoudSpeech
    }

    /**
     * Sends the given audio data to the streaming recognize client for further processing.
     */
    private fun recognize(data: ByteArray?, size: Int) {
        if (!isListening) return

        clientStream?.send(
            StreamingRecognizeRequest.newBuilder()
                .setAudioContent(ByteString.copyFrom(data, 0, size)).build()
        )
    }

    /**
     * Google STT engine callback. Receives STT results.
     */
    private val callback = object : ResponseObserver<StreamingRecognizeResponse> {

        // Connection to Google STT is established
        override fun onStart(controller: StreamController?) {
            Log.d(TAG, "ClientStream started")
            isActive = true
        }

        // Received STT results
        override fun onResponse(streamingResponse: StreamingRecognizeResponse?) {
            if (!isListening || !isActive) {
                return
            }

            val response = streamingResponse ?: return
            var text: String? = null
            var confidence: Float? = null
            var isFinal = false

            // Process partial results
            if (response.resultsCount > 0) {
                val result = response.getResults(0)
                isFinal = result.isFinal

                if (result.alternativesCount > 0) {
                    // Find best alternative
                    val bestAlternative = result.alternativesList.maxByOrNull { it.confidence }
                        ?: result.getAlternatives(0)
                    text = bestAlternative.transcript
                    confidence = bestAlternative.confidence

                    // Cut speech output audio
                    AndroidAvatarPlayerController.setBlocked(true)
                    _callback?.onTranscribeResult(transcript = text, isFinal = false)

                    // Enqueue EOU processing
                    lastTranscriptUpdateTime = Clock.System.now()
                    enqueueTranscriptProcessing()
                }
            }

            currentTranscript = text ?: ""

            // If transcript marked final, send result immediately
            if (isFinal) {
                text?.let {
                    externalScope.launch {
                        Log.d(TAG, "Final transcript: $it (confidence: $confidence)")
                        endSpeechRecognizer()
                        AndroidAvatarPlayerController.setBlocked(true)
                        _callback?.onTranscribeResult(transcript = it, isFinal = false)
                        delay(1200)
                        _callback?.onTranscribeResult(transcript = it, isFinal = true)
                        AndroidAvatarPlayerController.setBlocked(false)
                    }
                }
            }
        }

        // Error from Google STT
        override fun onError(t: Throwable?) {
            Log.e(TAG, "onError[${t?.message}]")

            Log.e(TAG, "Google STT threw an error, restarting client stream..")
            restartTranscribe()
        }

        // STT finished and will no longer fire results
        override fun onComplete() {
            Log.d(TAG, "onComplete $clientStream $voiceRecorder")
        }
    }

    /**
     * Ends the speech recognition process by resetting relevant states and stopping the recognition.
     *
     * This method is generally used to terminate speech recognition when it is no longer needed or when the session ends.
     */
    private fun endSpeechRecognizer() {
        if (!isListening && !isProcessing && !isActive && currentTranscript.isEmpty()) {
            return
        }

        Log.d(TAG, "Ending speech recognizer")

        isListening = false
        isProcessing = false
        isActive = false
        currentTranscript = ""
        stopSpeechRecognition()

        AndroidAvatarPlayerController.setBlocked(false)
    }

    /**
     * Stops the speech recognition process by closing the Google STT client stream.
     *
     * This method is typically called when speech recognition needs to be terminated or when the session ends.
     */
    private fun stopSpeechRecognition() {
        if (clientStream == null) {
            return
        }

        Log.d(TAG, "Stopping recognition..")
        clientStream?.closeSend()
        clientStream = null
    }

    /**
     * Enqueues the processing of the transcript to determine EOU.
     */
    private fun enqueueTranscriptProcessing() {
        if (isProcessing) {
            return
        }
        isProcessing = true
        Log.i(TAG, "Enqueue transcript processing..")

        externalScope.launch {
            while (isProcessing) {
                val lastTranscriptUpdateTime = lastTranscriptUpdateTime ?: continue
                val endOfUtteranceEstimation = (lastTranscriptUpdateTime + END_OF_UTTERANCE_TIMEOUT)
                if (Clock.System.now() > endOfUtteranceEstimation && currentTranscript.isNotBlank()) {
                    Log.d(TAG, "Final transcript (EOU): $currentTranscript")
                    _callback?.onTranscribeResult(transcript = currentTranscript, isFinal = true)
                    endSpeechRecognizer()
                }
            }
        }
    }

    // == Overrides ==================================================================

    /**
     * Restarts the speech recognition process if certain conditions are met.
     *
     * Ensures correct recognition when ClientViewModel transcript() called and it is users turn
     * to speak.
     */
    override fun restartTranscribe() {
        if (duplexMode != DuplexMode.OnDevice) return
        if (currentTranscript.isNotBlank()) return
        if (isSpeaking) return

        Log.d(TAG, "Restarting speech recognizer..")
        endSpeechRecognizer()
        initClientStream()
        isListening = true
    }

    /**
     * Sets the privileged status for the speech recognition process.
     *
     * The privileged status skips the VAD in case user is expected to speak.
     */
    override fun setPrivileged(value: Boolean) {
        Log.d(TAG, "isPrivileged: $value")
        isPrivileged = value
    }

    override fun setCallback(callback: SpeechRecognizerControllerCallback) {
        this._callback = callback
    }

    override fun stopTranscribe() {
        endSpeechRecognizer()
    }

    override fun pause() {
        // End the audio file (for debug purposes)
        isRecording = false
    }

    override suspend fun startTranscribe() { /* Ignoring events in duplex mode */ }

    override fun resume() { /* Ignoring events in duplex mode */ }

    override fun onVisualItemReceived() { /* Ignoring events in duplex mode */ }

    override fun setSilenceLength(length: Int) { /* Ignoring events in duplex mode */ }
}