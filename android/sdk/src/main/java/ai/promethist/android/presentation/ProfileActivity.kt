package ai.promethist.android.presentation

import ai.promethist.android.presentation.components.screen.ProfileView
import ai.promethist.android.style.ElysaiAndroidTheme
import ai.promethist.client.Client
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.os.Bundle
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent

class ProfileActivity: ComponentActivity() {

    companion object {
        const val USERNAME = "userName"
            val LogInResultCode = 907
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val config = Client.appConfig()
        setContent {
            ElysaiAndroidTheme(config.appearance) {
                ProfileView(
                    config.profileData,
                    authClient = Client.authClient(),
                    {
//                    val sp = PreferenceManager.getDefaultSharedPreferences(this)
//                    sp.edit {
//                        remove("deviceId")
//                        putString(Preferences.keyPrimaryPersona, "0")
//                        commit()
//                    }
//                    Toast.makeText(this, "Profile successfully reset", Toast.LENGTH_LONG).show()
                },{
                    finish()
                },{
                    val clipboard =
                        this.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
                    val clip: ClipData = ClipData.newPlainText("ID", config.deviceId)
                    clipboard.setPrimaryClip(clip)
                    Toast.makeText(this, "ID copied to clipboard", Toast.LENGTH_LONG).show()
                }) {
                    this.setResult(it)
                    this.finish()
                }
            }
       }
    }
}