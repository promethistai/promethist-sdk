package ai.promethist.android.presentation.components.pager

import ai.promethist.SharedRes
import ai.promethist.channel.command.PersonaSelection
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.pager.rememberPagerState
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.sp

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun PersonaPager(
    modifier: Modifier,
    payload: PersonaSelection,
    onPageSelected: (Int) -> Unit,
    onPageClick: (String) -> Unit
) {
    val personas = payload.personas
    val pagerState = rememberPagerState(
        initialPage = personas.size / 2,
        pageCount = { personas.size }
    )
    ElysaiHorizontalPager(
        elementList = personas,
        pagerState = pagerState,
        onPageSelected = {  },
    ) { pageIndex ->
        val choice = personas[pageIndex]
        val image = SharedRes.images.anthony_portrait_l.drawableResId
        Box(
            modifier = Modifier.clickable {
                val persona = choice.name
                onPageClick(persona)
            }
        ) {
            Column(
                modifier = Modifier
                    .wrapContentHeight()
                    .fillMaxWidth(),
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                Text(
                    text = choice.name,
                    modifier = Modifier,
                    textAlign = TextAlign.Center,
                    style = MaterialTheme.typography.labelLarge,
                    fontSize = 36.sp
                )
                Image(
                    painter = painterResource(id = image),
                    contentDescription = "Portrait",
                    modifier = Modifier.weight(1F)
                )
                choice.description?.let {
                    Text(
                        text = it,
                        textAlign = TextAlign.Left,
                        lineHeight = 18.sp
                    )
                }
            }
        }
    }
}