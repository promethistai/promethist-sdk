package ai.promethist.android.presentation.components.button

import ai.promethist.android.ext.init
import ai.promethist.android.style.SpaceMedium
import ai.promethist.android.style.SpaceSmall
import ai.promethist.client.resources.Resources
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

@Composable
fun PromethistPrimaryButton(
    modifier: Modifier = Modifier,
    text: String,
    backgroundColor: Color = Color.init(res = Resources.colors.accent),
    foregroundColor: Color = Color.White,
    isEnabled: Boolean = true,
    onClick: (() -> Unit)? = null
) {
    val height = 59.dp
    val radius = height / 2

    val horizontalPadding = SpaceMedium
    val verticalPadding = SpaceSmall

    val content = @Composable {
        Box(
            modifier = Modifier
                .background(
                    color = backgroundColor.copy(alpha = if (isEnabled) 1f else 0.3f),
                    shape = RoundedCornerShape(radius)
                )
                .height(height)
                .fillMaxWidth(),
            contentAlignment = Alignment.Center
        ) {
            Text(
                text = text,
                color = foregroundColor,
                fontSize = 18.sp,
                fontWeight = FontWeight.SemiBold,
                maxLines = 1,
                overflow = TextOverflow.Ellipsis,
                modifier = Modifier.padding(horizontal = horizontalPadding, vertical = verticalPadding)
            )
        }
    }

    if (onClick != null) {
        Button(
            onClick = onClick,
            enabled = isEnabled,
            colors = ButtonDefaults.buttonColors(containerColor = Color.Transparent),
            contentPadding = PaddingValues(0.dp),
            shape = RoundedCornerShape(radius),
            modifier = modifier.fillMaxWidth(),
            border = BorderStroke(1.dp, foregroundColor)
        ) {
            content()
        }
    } else {
        content()
    }
}