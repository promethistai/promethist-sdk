package ai.promethist.android.presentation.components.picker

import ai.promethist.android.presentation.components.text.ShadedText
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Send
import androidx.compose.material3.Button
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.wear.compose.material.Picker
import androidx.wear.compose.material.PickerDefaults
import androidx.wear.compose.material.rememberPickerState

@Composable
fun YearPicker(
    modifier: Modifier = Modifier,
    onClick: (Int) -> Unit
) {
    val years = (1900..2020).toList()
    val pickerState = rememberPickerState(
        initialNumberOfOptions = years.size,
        initiallySelectedOption = 100,
        repeatItems = false
    )
    Column(
        modifier = Modifier.padding(vertical = 8.dp),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Picker(
            state = pickerState,
            contentDescription = "Module picker",
            modifier = modifier
                .fillMaxHeight(0.25F),
            onSelected = {},
            scalingParams = PickerDefaults.scalingParams(edgeAlpha = 0.3F),
            gradientColor = Color.Transparent
        ) {
            ShadedText(
                modifier = Modifier
                    .padding(vertical = 12.dp),
                personaText = years[it].toString()
            )
        }
        Button(
            modifier = Modifier.size(48.dp),
            contentPadding = PaddingValues(0.dp),
            onClick = { onClick(years[pickerState.selectedOption]) }
        ) {
            Image(
                imageVector = Icons.Filled.Send,
                contentDescription = "Year submit button"
            )
        }
    }
}