package ai.promethist.android.presentation.components.row

import ai.promethist.android.ext.init
import ai.promethist.android.ext.painterResource
import ai.promethist.android.style.SpaceMedium
import ai.promethist.android.style.SpaceSmall
import ai.promethist.channel.model.ResourceSize
import ai.promethist.client.model.Message
import ai.promethist.client.model.PersonaId
import ai.promethist.client.utils.PersonaResourcesManager
import ai.promethist.client.resources.Resources
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.GenericShape
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.rememberAsyncImagePainter

/**
 * A Jetpack Compose approximation of the SwiftUI `TranscriptionRow`.
 * Displays a message bubble and an optional avatar if it's not from the user.
 *
 * @param message The [Message] to display.
 */
@Composable
fun MessageRow(
    message: Message,
    currentIndex: Int?,
    query: String
) {
    val textColor: Color =
        if (message.isUser) Color.White else Color.init(res = Resources.colors.personaChatBubbleForeground)

    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(start = if (message.isUser) 76.dp else 0.dp),
        verticalAlignment = Alignment.Top
    ) {
        if (message.isUser) {
            Spacer(modifier = Modifier.weight(1f))
        }

        if (!message.isUser && message.personaId != null) {
            val portrait = PersonaResourcesManager.getPortrait(
                personaId = PersonaId.fromAvatarId(message.personaId),
                size = ResourceSize.M
            )
            Spacer(modifier = Modifier.width(SpaceSmall))
            Box(
                modifier = Modifier
                    .size(avatarSize.dp)
                    .clip(CircleShape)
                    .background(
                        Color
                            .init(res = Resources.colors.personaSurface)
                            .copy(alpha = 0.2f)
                    )
            ) {
                Image(
                    painter = painterResource(res = portrait),
                    contentDescription = null,
                    contentScale = ContentScale.Crop,
                    modifier = Modifier.fillMaxSize()
                )
            }
            Spacer(modifier = Modifier.width(SpaceSmall))
        }

        MessageBubble(message = message, textColor = textColor, query = query, currentIndex = currentIndex)

        if (!message.isUser) {
            Spacer(modifier = Modifier.weight(1f))
        }
    }
}

/**
 * The "bubble" portion that displays message info (time, text, optional image).
 */
@Composable
private fun MessageBubble(
    message: Message,
    textColor: Color,
    currentIndex: Int?,
    query: String
) {
    val bubbleShape = rememberBubbleShape(message.isUser)

    Column(
        modifier = Modifier
            .background(
                color = if (message.isUser)
                    Color.init(res = Resources.colors.accent)
                else
                    Color.init(res = Resources.colors.personaChatBubbleBackground),
                shape = bubbleShape
            )
            .padding(SpaceMedium)
    ) {
        Text(
            text = message.createdFormatted,
            fontSize = 12.sp,
            color = textColor.copy(alpha = 0.7f)
        )

        Text(
            text = annotate(
                query = query,
                text = message.text.replace("\n", ""),
                indexes = message.searchedIndexes,
                currentIndex = currentIndex
            ),
            fontSize = 18.sp,
            color = textColor
        )

        // Optional image
        message.image?.let { urlString ->
            Spacer(modifier = Modifier.height(8.dp))
            Image(
                painter = rememberAsyncImagePainter(urlString),
                contentDescription = null,
                modifier = Modifier.fillMaxWidth(),
                contentScale = ContentScale.Fit
            )
        }
    }
}

@Composable
private fun rememberBubbleShape(isUser: Boolean): Shape {
    val density = LocalDensity.current
    return GenericShape { size, _ ->
        val cornerRadiusPx = with(density) { 26.dp.toPx() }

        val bottomLeftRadius = if (isUser) cornerRadiusPx else 0f
        val bottomRightRadius = if (isUser) 0f else cornerRadiusPx

        moveTo(cornerRadiusPx, 0f)
        lineTo(size.width - cornerRadiusPx, 0f)
        quadraticBezierTo(size.width, 0f, size.width, cornerRadiusPx)
        lineTo(size.width, size.height - bottomRightRadius)
        quadraticBezierTo(size.width, size.height, size.width - bottomRightRadius, size.height)
        lineTo(bottomLeftRadius, size.height)
        quadraticBezierTo(0f, size.height, 0f, size.height - bottomLeftRadius)
        lineTo(0f, cornerRadiusPx)
        quadraticBezierTo(0f, 0f, cornerRadiusPx, 0f)
        close()
    }
}

@Composable
fun annotate (query: String, text: String, indexes: List<Int>?, currentIndex: Int?): AnnotatedString {
    val highlightedCurrent = SpanStyle(
        color = MaterialTheme.colorScheme.onPrimaryContainer,
        fontWeight = FontWeight.SemiBold,
        background = Color(0XFFFFBB00)
    )
    val highlightedOther = SpanStyle(
        color = MaterialTheme.colorScheme.onPrimaryContainer,
        fontWeight = FontWeight.SemiBold,
        background = Color.Red
    )
    return buildAnnotatedString {
        var start = 0
        var i = indexes?.get(0)
        while (text.indexOf(query, start, ignoreCase = true) != -1 && query.isNotBlank()) {
            val firstIndex = text.indexOf(query, start, true)
            val end = firstIndex + query.length
            append(text.substring(start, firstIndex))
            withStyle(
                style = if (i != null && i == currentIndex) highlightedCurrent else highlightedOther
            ) {
                append(text.substring(firstIndex, end))
            }
            start = end
            i = i?.inc()
        }
        append(text.substring(start, text.length))
        toAnnotatedString()
    }
}

private val avatarSize = 40