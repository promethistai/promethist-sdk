package ai.promethist.android.presentation.components

import ai.promethist.android.style.SpaceXSmall
import ai.promethist.android.ext.textShadow
import ai.promethist.android.style.heading3
import ai.promethist.channel.model.PersonaModule
import ai.promethist.client.utils.PersonaResourcesManager
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import coil.compose.AsyncImage
import coil.request.ImageRequest

@Composable
fun ModuleItemView(
    item: PersonaModule,
    onClick: () -> Unit
) {
    Column(
        modifier = Modifier
            .width(ModuleWidth)
            .clickable(onClick = onClick),
        verticalArrangement = Arrangement.spacedBy(SpaceXSmall),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        AsyncImage(
            modifier = Modifier
                .size(ModuleIconSize),
            model = ImageRequest.Builder(LocalContext.current)
                .data("")
                .crossfade(true)
                .build(),
            contentDescription = null,
            contentScale = ContentScale.Fit
        )

        Text(
            text = item.name,
            textAlign = TextAlign.Center,
            style = heading3.copy(shadow = textShadow),
            color = Color.White
        )
    }
}

private val ModuleWidth = 88.dp
private val ModuleIconSize = 78.dp