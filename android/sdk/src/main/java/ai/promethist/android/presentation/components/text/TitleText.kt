package ai.promethist.android.presentation.components.text

import ai.promethist.android.ext.init
import ai.promethist.android.style.SpaceMedium
import ai.promethist.android.style.SpaceMediumSmall
import ai.promethist.client.resources.Resources
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.sp

@Composable
fun TitleText(
    modifier: Modifier,
    title: String?,
    text: String? = null,
    textAlign: TextAlign = TextAlign.Start,
    textStyle: TextStyle = MaterialTheme.typography.titleLarge
) {
    val titleUnwrapped = title ?: ""
    val fontSize = when {
        titleUnwrapped.count() < 5 -> 30.sp
        titleUnwrapped.count() < 15 -> 25.sp
        titleUnwrapped.count() < 35 -> 20.sp
        else -> 20.sp
    }

    Box(
        modifier = modifier
            .fillMaxWidth()
            .padding(horizontal = SpaceMedium)
            .padding(bottom = SpaceMedium)
    ) {
        Column(
            modifier = Modifier
                .verticalScroll(rememberScrollState())
                .fillMaxWidth(),
            verticalArrangement = Arrangement.spacedBy(SpaceMediumSmall)
        ) {
            title?.let {
                Text(
                    modifier = Modifier.fillMaxWidth(),
                    text = it,
                    style = textStyle.copy(
                        color = Color.init(res = Resources.colors.primaryLabel),
                        fontSize = fontSize
                    ),
                    fontWeight = FontWeight.Bold,
                    textAlign = textAlign
                )
            }

            text?.let {
                Text(
                    modifier = Modifier.fillMaxWidth(),
                    text = it,
                    style = textStyle.copy(
                        color = Color.init(res = Resources.colors.primaryLabel),
                        fontSize = 15.sp,
                        fontWeight = FontWeight.Medium
                    ),
                    textAlign = TextAlign.Start
                )
            }
        }
    }
}