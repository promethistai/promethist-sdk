package ai.promethist.android.presentation.components.screen

import ai.promethist.android.ext.fillWidthOfParent
import ai.promethist.android.presentation.components.ButtonConfig
import ai.promethist.android.presentation.components.CameraControls
import ai.promethist.android.presentation.components.OptionMatrix
import ai.promethist.android.presentation.components.SessionEndMenu
import ai.promethist.android.presentation.components.TileList
import ai.promethist.android.presentation.components.VoiceRecorder
import ai.promethist.android.presentation.components.input.UserTextField
import ai.promethist.android.presentation.components.input.UserTextInput
import ai.promethist.android.presentation.components.menu.MenuColumn
import ai.promethist.android.presentation.components.modal.HomeDisclaimerModalView
import ai.promethist.android.presentation.components.picker.YearPicker
import ai.promethist.android.presentation.components.row.SetPersonaRow
import ai.promethist.android.presentation.components.text.TitleText
import ai.promethist.android.style.SpaceMedium
import ai.promethist.android.style.SpaceMediumLarge
import ai.promethist.android.style.SpaceSmall
import ai.promethist.android.style.heading1
import ai.promethist.channel.command.RequestText
import ai.promethist.channel.item.ActionsItem
import ai.promethist.channel.item.CameraItem
import ai.promethist.channel.item.CorrectInputItem
import ai.promethist.channel.item.GrowthContentItem
import ai.promethist.channel.item.InputItem
import ai.promethist.channel.item.Modal
import ai.promethist.channel.item.QuestionnaireItem
import ai.promethist.channel.item.RequestRecordingItem
import ai.promethist.channel.item.RequestTextItem
import ai.promethist.channel.item.SessionEndItem
import ai.promethist.channel.item.SetPersonaItem
import ai.promethist.channel.item.TextInputItem
import ai.promethist.channel.item.VoiceInputItem
import ai.promethist.client.ClientEvent
import ai.promethist.client.ClientModel
import android.graphics.Bitmap
import android.util.Base64
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.animateContentSize
import androidx.compose.animation.core.tween
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.heightIn
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.io.ByteArrayOutputStream

@Composable
fun ModalView(
    modifier: Modifier,
    modal: Modal?,
    handleEvent: (ClientEvent) -> Unit
) {
    Card(
        modifier = modifier
            .wrapContentHeight()
            .padding(SpaceMediumLarge)
            .animateContentSize(),
        shape = RoundedCornerShape(30.dp),
        colors = CardDefaults.cardColors(containerColor = Color.White)
    ) {

        AnimatedVisibility(
            visible = modal != null,
            enter = fadeIn(tween(600)),
            exit = fadeOut(tween(700))
        ) {

            Box(
                modifier = Modifier
                    .padding(30.dp)
            ) {
                when (modal) {
                    null, is VoiceInputItem, is TextInputItem -> {

                    }

                    is ActionsItem -> {
                        TileList(actions = modal.content) { text, label ->
                            handleEvent(ClientEvent.UserInput(text))
                        }
                    }

                    is QuestionnaireItem -> {
                        val content = modal.content
                        OptionMatrix(
                            matrixContent = content.createMatrixContent(),
                            title = content.title ?: "",
                            labels = content.subLabels,
                            buttonConfig = ButtonConfig(
                                Modifier.aspectRatio(1F),
                                RoundedCornerShape(8.dp)
                            )
                        ) { text, label ->
                            handleEvent(ClientEvent.UserInput(text))
                        }
                    }

                    is InputItem -> {
                        val content = modal.input
                        Column {
                            TitleText(
                                modifier = Modifier
                                    .fillWidthOfParent(SpaceMedium),
                                title = content.title,
                                text = content.text,
                                textStyle = heading1
                            )

                            OptionMatrix(
                                matrixContent = content.createMatrixContent()
                            ) { text, label ->
                                handleEvent(ClientEvent.UserInput(text))
                            }

                            Spacer(modifier = Modifier.height(SpaceMedium))

                            if (content.birthYear) {
                                YearPicker(
                                    modifier = Modifier
                                        .padding(start = 16.dp, end = 16.dp, bottom = 16.dp)
                                ) {
                                    val pickedYear = it.toString()
                                    handleEvent(ClientEvent.UserInput(pickedYear))
                                }
                            }
                            if (modal.textInput) {
                                UserTextInput(
                                    enabled = true
                                ) {
                                    handleEvent(ClientEvent.UserInput(it))
                                }
                            }
                        }
                    }

                    is SessionEndItem -> {
                        HomeDisclaimerModalView(
                            onConfirm = {
                                handleEvent(ClientEvent.Reset(true))
                            },
                            onCancel = {}
                        )
                    }

                    is GrowthContentItem -> {
                        Box(
                            modifier = Modifier
                                .fillMaxWidth()
                                .fillMaxHeight(fraction = 0.78f)
                        ) {
                            MenuColumn(
                                modal.content
                            ) { action ->
                                handleEvent(ClientEvent.UserInput("#$action"))
                            }
                        }
                    }

                    is RequestRecordingItem -> {
                        VoiceRecorder {
                            // onRecording(it)
                        }
                    }

                    is RequestTextItem -> {
                        UserTextField(payload = modal.content) {
                            val shortened = if (it.length > 30) it.substring(0, 30) + "..." else it
                            handleEvent(ClientEvent.UserInput(it))
                        }
                    }

                    is SetPersonaItem -> {
                        PersonaSelectV1(modal = modal, handleEvent = handleEvent)
                    }

                    is CorrectInputItem -> {
                        UserTextField(
                            payload = RequestText(
                                text = modal.correctInput.input
                            ),
                            minLines = 3,
                            maxLines = 3,
                            onSubmit = {
                                handleEvent(ClientEvent.UserInput(it))
                            })
                    }

                    is CameraItem -> {
                        CameraControls(
                            onImage = { bitmap ->
                                if (bitmap == null) {
                                    handleEvent(ClientEvent.Resume)
                                }
                                bitmap?.let {
                                    val scaled =
                                        Bitmap.createScaledBitmap(it, imageWidth, imageHeight, true)
                                    val stream = ByteArrayOutputStream()
                                    scaled.compress(Bitmap.CompressFormat.JPEG, 100, stream)
                                    val byteArray: ByteArray = stream.toByteArray()
                                    it.recycle()
                                    val encoded = Base64.encodeToString(byteArray, Base64.DEFAULT)
                                    CoroutineScope(Dispatchers.Main).launch {
                                        handleEvent(ClientEvent.Resume)
                                    }
                                }
                            },
                            onUserInput = {

                            },
                            onCameraActivated = {
                                handleEvent(ClientEvent.Pause)
                            }
                        )
                    }
                }
            }
        }
    }
}

@Composable
fun PersonaSelectV1(
    modal: SetPersonaItem,
    handleEvent: (ClientEvent) -> Unit
) {
    val content = modal.content
    Column(
        modifier = Modifier
            .fillWidthOfParent(6.dp),
    ) {
        content.title?.let {
            if (it.isNotEmpty())
                TitleText(
                    modifier = Modifier
                        .weight(weight = 1f, fill = false)
                        .padding(horizontal = SpaceSmall),
                    title = it,
                    textAlign = TextAlign.Center,
                    textStyle = heading1
                )
        }

        SetPersonaRow(
            payload = content,
            onPageClick = { personaItem, personaType ->
                handleEvent(
                    ClientEvent.SetPersona(
                        avatarRef = personaItem.getAvatar(),
                        ref = personaItem.id,
                        sendAsInput = true
                    ))
            }
        )
    }
}

val imageHeight = 800
val imageWidth = 600