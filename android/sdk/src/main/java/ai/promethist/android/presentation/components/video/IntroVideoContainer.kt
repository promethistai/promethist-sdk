package ai.promethist.android.presentation.components.video

import ai.promethist.android.ext.painterResource
import ai.promethist.android.ext.stringResource
import ai.promethist.android.presentation.components.button.CircularButton
import ai.promethist.android.style.SpaceLarge
import ai.promethist.android.style.SpaceMediumLarge
import ai.promethist.android.style.SpaceSmall
import ai.promethist.client.ClientDefaults
import ai.promethist.client.resources.Resources
import androidx.annotation.OptIn
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.media3.common.MediaItem
import androidx.media3.common.Player
import androidx.media3.common.util.UnstableApi
import androidx.media3.datasource.RawResourceDataSource
import androidx.media3.exoplayer.ExoPlayer

@OptIn(UnstableApi::class)
@Composable
fun IntroVideoContainer(
    onVideoEnd: () -> Unit
) {
    val context = LocalContext.current
    val exoPlayer by remember {
        mutableStateOf(ExoPlayer.Builder(context).build().apply {
            addListener(object: Player.Listener {
                override fun onPlaybackStateChanged(playbackState: Int) {
                    super.onPlaybackStateChanged(playbackState)
                    when (playbackState) {
                        Player.STATE_ENDED -> onVideoEnd()
                        else -> Unit
                    }
                }
            })
            addMediaItem(MediaItem.fromUri(RawResourceDataSource.buildRawResourceUri(ClientDefaults.introVideo.rawResId)))
            prepare()
            play()
        })
    }

    Box(modifier = Modifier.fillMaxSize()) {
        ExoPlayerContainer(
            modifier = Modifier.fillMaxSize().align(Alignment.BottomCenter),
            exoPlayer = exoPlayer,
            colorBackgroundOverlay = false,
            opacity = 1.0,
            isPaused = false
        ) {

        }
        Row (
            modifier = Modifier
                .height(140.dp)
                .fillMaxWidth()
                .padding(horizontal = SpaceMediumLarge, vertical = SpaceLarge)
                .align(Alignment.BottomCenter),
            horizontalArrangement = Arrangement.End,
            verticalAlignment = Alignment.CenterVertically
        ) {
            CircularButton(
                modifier = Modifier
                    .padding(end = SpaceSmall)
                    .alpha(0F),
                isActive = false,
                painter = painterResource(res = Resources.images.personaSkip)
            ) { }
            Button(
                modifier = Modifier
                    .padding(horizontal = SpaceSmall)
                    .height(60.dp)
                    .weight(1F),
                onClick = {},
                enabled = false,
                colors = ButtonDefaults.buttonColors(disabledContainerColor = Color(0x33F5F5F7))
            ) {
                Text(
                    text = stringResource(res = Resources.strings.persona.introduction),
                    color = Color.White,
                    fontSize = 17.sp,
                    fontWeight = FontWeight.Medium
                )
            }
            CircularButton(
                modifier = Modifier
                    .padding(start = SpaceSmall),
                isActive = false,
                painter = painterResource(res = Resources.images.personaSkip)
            ) {
                exoPlayer.stop()
                exoPlayer.release()
                onVideoEnd()
            }
        }
    }
}