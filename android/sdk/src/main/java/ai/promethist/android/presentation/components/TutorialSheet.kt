package ai.promethist.android.presentation.components

import ai.promethist.android.ext.init
import ai.promethist.android.presentation.components.button.OptionButton
import ai.promethist.android.presentation.components.text.TitleText
import ai.promethist.android.style.SpaceLarge
import ai.promethist.android.style.SpaceMediumLarge
import ai.promethist.android.style.SpaceXSmall
import ai.promethist.android.style.SpaceXXLarge
import ai.promethist.channel.command.TutorialData
import ai.promethist.client.resources.Resources
import androidx.compose.foundation.layout.Arrangement.spacedBy
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.ModalBottomSheet
import androidx.compose.material3.SheetState
import androidx.compose.material3.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.AsyncImage

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun TutorialBottomSheet(
    tutorialData: TutorialData,
    onDismissRequest: () -> Unit,
    state: SheetState
) {

    var topicItem by remember { mutableStateOf<TutorialData.TopicItem?>(null) }

    ModalBottomSheet(
        sheetState = state,
        onDismissRequest = { onDismissRequest() },
    ) {
        Box(
            modifier = Modifier
                .fillMaxSize()
                .padding(horizontal = SpaceMediumLarge, vertical = SpaceLarge),
        ) {
            if (topicItem == null) {
                InfoItems(
                    tutorialData.title,
                    topics = tutorialData.topics
                ) { topicItem = it }
            } else {
                topicItem?.let {
                    InfoItemDetail(
                        topicItem = it,
                        returnText = tutorialData.returnText
                    ) { topicItem = null }
                }
            }
        }
    }
}

@Composable
fun InfoItems(
    title: String,
    topics: List<TutorialData.TopicItem>,
    onClick: (TutorialData.TopicItem) -> Unit
) {
    Column (
        modifier = Modifier,
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = spacedBy(SpaceMediumLarge)
    ) {
        TitleText(
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = SpaceXSmall, bottom = SpaceXSmall)
                .padding(bottom = SpaceXSmall),
            title = title,
            textAlign = TextAlign.Center
        )
        Column(
            modifier = Modifier.verticalScroll(rememberScrollState())
        ) {
            topics.forEach {
                OptionButton(
                    text = it.title,
                    onClick = { onClick(it) },
                    modifier = Modifier.fillMaxWidth(),
                    color = Color.init(res = Resources.colors.accentSecondary),
                    cornerRadius = SpaceXXLarge.value.toInt()
                )
            }
        }
    }
}

@Composable
fun InfoItemDetail(
    topicItem: TutorialData.TopicItem,
    returnText: String,
    onClick: () -> Unit
) {
    Column (
        modifier = Modifier,
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = spacedBy(SpaceMediumLarge)
    ) {
        TitleText(
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = SpaceXSmall, bottom = SpaceXSmall)
                .padding(bottom = SpaceXSmall),
            title = topicItem.title,
            textAlign = TextAlign.Center
        )
        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = spacedBy(SpaceMediumLarge),
            modifier = Modifier
                .verticalScroll(rememberScrollState())
                .fillMaxSize()
                .weight(1f)
        ) {
            topicItem.image?.let {
                AsyncImage(
                    model = it,
                    contentDescription = "Info image",
                    modifier = Modifier
                        .height(150.dp),
                )
            }
            Text(
                text = topicItem.text,
                fontSize = 16.sp
            )
        }
        OptionButton(
            text = returnText,
            onClick = { onClick() },
            modifier = Modifier
                .fillMaxWidth(),
            color = Color.init(res = Resources.colors.accentSecondary),
            cornerRadius = SpaceXXLarge.value.toInt()
        )
    }
}

