package ai.promethist.android.services

import android.content.Intent
import com.google.zxing.integration.android.IntentIntegrator
import com.google.zxing.integration.android.IntentResult

object AndroidScannerController {
    fun onResult(
        requestCode: Int,
        resultCode: Int,
        data: Intent?,
        onReceived: (String?) -> Unit
    ) {
        val result: IntentResult? =
            IntentIntegrator.parseActivityResult(requestCode, resultCode, data)
        if (result?.contents == null) {
            onReceived("Scan cancelled")
        } else {
            onReceived(result.contents)
        }
    }
}