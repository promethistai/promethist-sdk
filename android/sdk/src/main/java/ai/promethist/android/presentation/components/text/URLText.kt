package ai.promethist.android.presentation.components.text

import ai.promethist.android.R
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.ClickableText
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalUriHandler
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.unit.dp


data class URLContainer(val label: String, val url: String)

@Composable
fun annotateHTML(text: String, style: SpanStyle, urls: List<URLContainer>, titles: List<String>): AnnotatedString = buildAnnotatedString {
    val tag = stringResource(R.string.tag_url)
    append(text)
    val linkStyle = SpanStyle(
        color = MaterialTheme.colorScheme.primary,
        textDecoration = TextDecoration.Underline
    )
    urls.forEach { urlContainer ->
        val startIndex = text.indexOf(urlContainer.label)
        val endIndex = startIndex + urlContainer.label.length
        addStyle(
            style = linkStyle, start = startIndex, end = endIndex
        )
        addStringAnnotation(
            tag = tag,
            annotation = urlContainer.url,
            start = startIndex,
            end = endIndex
        )
    }
    titles.forEach {
        val stripped = it.replace("**", "")
        val startIndex = text.indexOf(stripped)
        val endIndex = startIndex + stripped.length
        addStyle(style, startIndex, endIndex)
    }
}

@Composable
fun URLText(text: String, urls: List<URLContainer>, style: TextStyle? = null, center: Boolean = false){
    val uriHandler = LocalUriHandler.current
    val annotatedTitles = Regex("\\*\\*([a-zA-Z]*)\\*\\*").findAll(text)

    val titles = annotatedTitles.mapNotNull { title ->
        title.groups.first()?.value
    }
    var finalText = text
    titles.forEach { title ->
        val stripped = title.replace("**", "")
        finalText = finalText.replace(title, "\n$stripped\n")
    }
    val titleStyle = SpanStyle(fontWeight = FontWeight.Bold)
    val annotatedLinkString = annotateHTML(finalText.replace(Regex(" \\([a-z.:/-]*\\)"), ""), titleStyle, urls, titles.toList())
    val tag = stringResource(R.string.tag_url)
    val align = if (center) TextAlign.Center else TextAlign.Start
    val alignStyle = TextStyle(textAlign = align)
    ClickableText(
        modifier = Modifier
            .padding(4.dp)
            .fillMaxWidth(),
        text = annotatedLinkString,
        style = style ?: alignStyle,
        onClick = {
            annotatedLinkString
                .getStringAnnotations(tag, it, it)
                .firstOrNull()?.let { stringAnnotation ->
                    uriHandler.openUri(stringAnnotation.item)
                }
        },
    )
}