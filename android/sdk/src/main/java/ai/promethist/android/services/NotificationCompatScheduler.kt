package ai.promethist.android.services

import ai.promethist.android.R
import android.Manifest
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.pm.PackageManager
import android.util.TypedValue
import androidx.core.app.ActivityCompat
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.work.Constraints
import androidx.work.Data
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.WorkManager
import androidx.work.Worker
import androidx.work.WorkerParameters
import kotlinx.datetime.Clock
import java.time.Instant
import java.util.UUID
import java.util.concurrent.TimeUnit
import kotlin.time.DurationUnit
import kotlin.time.toTimeUnit

class NotificationCompatScheduler(val context: Context) {
    suspend fun scheduleNotification(
        tag: String?,
        name: String,
        text: String,
        timeDelay: Long?,
        timeUnit: DurationUnit?,
        scheduledTime: String?
    ) {
        val notificationData = Data.Builder()
            .putString("body", text)
            .putString("title", name)

        var timeDelay = timeDelay
        var timeUnit = timeUnit

        if (scheduledTime != null) {
            try {
                timeDelay = Instant
                    .parse(scheduledTime) // ISO String
                    .minusMillis(Clock.System.now().toEpochMilliseconds())
                    .toEpochMilli()
                timeUnit = DurationUnit.MILLISECONDS
            } catch (e: Exception) {
                // Log.w(TAG, "Failed to parse scheduled date, the string is probably not in the ISO 8601 format.")
            }
        }

        val workTask = OneTimeWorkRequestBuilder<NotificationWorker>()
            .setInitialDelay(timeDelay ?: 1, (timeUnit ?: DurationUnit.DAYS).toTimeUnit())
            .setConstraints(Constraints.Builder().setTriggerContentMaxDelay(1000, TimeUnit.MILLISECONDS).build()) // API Level 24
            .setInputData(notificationData.build())
            .addTag(tag ?: "default")
            .build()

        WorkManager.getInstance(context).enqueue(workTask)
    }

    suspend fun cancelNotification(tag: String) {
        WorkManager.getInstance(context).cancelAllWorkByTag(tag)
    }
}

class NotificationWorker(val context: Context, val params: WorkerParameters) :Worker(context, params)  {
    private fun createNotificationChannel(context: Context, name: String, description: String): String {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        val outValue = TypedValue()
        // context.theme.resolveAttribute(R.attr., outValue, true)

        val channelId = UUID.randomUUID().toString()
        val importance = NotificationManager.IMPORTANCE_HIGH
        val channel = NotificationChannel(channelId, name, importance)
        channel.enableLights(true)
        channel.enableVibration(true)
        channel.description = description
        channel.lightColor = outValue.data
        channel.lockscreenVisibility = Notification.VISIBILITY_PUBLIC

        val notificationManager = context.getSystemService(NotificationManager::class.java)
        notificationManager?.createNotificationChannel(channel)

        return channelId
    }
    private fun prepareNotification(context: Context, title: String, body: String, notificationChannelId: String): NotificationCompat.Builder {

        val notificationBuilder = NotificationCompat.Builder(context, notificationChannelId)

        val iconId = if (context.applicationInfo.icon > 0)
            context.applicationInfo.icon
        else
            R.drawable.icon_entertainment_poppy

        notificationBuilder.setDefaults(Notification.DEFAULT_ALL)
            .setContentTitle(title)
            .setContentText(body)
            .setSmallIcon(iconId)
            .setContentInfo("Content Info")
            .setAutoCancel(true)

        return notificationBuilder
    }

    private fun sendNotification(context: Context, notificationBuilder: NotificationCompat.Builder) {
        val notificationManager = NotificationManagerCompat.from(context)
        val notificationId = (System.currentTimeMillis() and 0xfffffff).toInt()

        val resultPendingIntent = PendingIntent.getActivity(
            context,
            0,
            null,
            PendingIntent.FLAG_IMMUTABLE or PendingIntent.FLAG_UPDATE_CURRENT
        )

        notificationBuilder.setContentIntent(resultPendingIntent)
        if (ActivityCompat.checkSelfPermission(
                context,
                Manifest.permission.POST_NOTIFICATIONS
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            return
        }
        notificationManager.notify(notificationId, notificationBuilder.build())
    }

    override fun doWork(): Result {
        val inputData = params.inputData
        val title = inputData.getString("title") ?: ""
        val body = inputData.getString("body") ?: ""

        val notificationChannelId = createNotificationChannel(context, title, body)
        val notification = prepareNotification(context, title, body, notificationChannelId)
        sendNotification(context, notification)

        return Result.success()
    }
}