package ai.promethist.android.services.speechRecognizer.duplex

import ai.promethist.android.services.speechRecognizer.duplex.DuplexAndroidSpeechRecognizer.Companion.BUFFER_SIZE
import ai.promethist.android.services.speechRecognizer.duplex.DuplexAndroidSpeechRecognizer.Companion.TAG
import android.content.Context
import android.util.Log
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.io.File
import java.io.FileOutputStream
import java.io.IOException

/**
 * Starts recording audio to storage using the `DuplexAndroidSpeechRecognizer`.
 *
 * This function records audio from the `voiceRecorder` and saves it as a PCM file
 * in the application's external files directory. Recording is automatically stopped
 * after 20 seconds or can be manually interrupted by setting `isRecording` to `false`.
 *
 * Used for debugging
 *
 * @param context The application context, used to access external storage.
 */
fun DuplexAndroidSpeechRecognizer.startRecordingToStorage(context: Context) {
    // Open file in external storage
    val outputFile = File(context.getExternalFilesDir(null), "recorded_audio.pcm")
    val outputStream = FileOutputStream(outputFile)

    // Start recording
    voiceRecorder?.startRecording()
    isRecording = true

    // Automatic stop after 20000 ms
    CoroutineScope(Job()).launch {
        delay(20000)
        isRecording = false
    }

    Thread {
        // Read audio stream
        val buffer = ByteArray(BUFFER_SIZE)
        while (isRecording) {
            val bytesRead = voiceRecorder?.read(buffer, 0, buffer.size) ?: 0
            if (bytesRead > 0) {
                Log.d(TAG, "bytes read: $bytesRead")

                // Write to file
                if (isStorageEnabled) {
                    try {
                        outputStream.write(buffer, 0, bytesRead)
                    } catch (e: IOException) {
                        Log.e(TAG, "Error writing to file", e)
                    }
                }
            }
        }

        // Close the output stream when done
        if (isStorageEnabled) {
            try {
                outputStream.close()
                Log.d(TAG, "Output stream closed successfully.")
            } catch (e: IOException) {
                Log.e(TAG, "Error closing output stream", e)
            }
        }
    }.start()
}