package ai.promethist.android.presentation.components.button

import ai.promethist.android.ext.stringResource
import ai.promethist.android.utils.AppEvent
import ai.promethist.android.utils.AppEventBus
import ai.promethist.client.model.AudioIndicatorState
import ai.promethist.client.resources.Resources
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.scale
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import java.lang.Float.max
import java.lang.Float.min

@Composable
fun AudioButton(
    modifier: Modifier,
    audioIndicatorState: AudioIndicatorState
) {
    
    var scale by remember {
        mutableStateOf(0F)
    }
    val backgroundColor =
        if (audioIndicatorState == AudioIndicatorState.Speaking || audioIndicatorState == AudioIndicatorState.StartSpeaking)
            Color(0x4D7DFF9E)
        else
            Color(0x33F5F5F7)
    
    LaunchedEffect(Unit) {
        AppEventBus.subscribe(this) {
            if (it is AppEvent.MicPowerUpdated) {
                scale = DecibelToPercentageConverter.convertToPercentage(it.rms)
            }
        }
    }
    
    Button(
        modifier = modifier
            .height(60.dp), // TODO extract
        onClick = {},
        enabled = false,
        colors = ButtonDefaults.buttonColors(disabledContainerColor = backgroundColor)
    ) {
        when (audioIndicatorState) {
            AudioIndicatorState.None, AudioIndicatorState.Playing -> {
                Image(
                    painter = painterResource(Resources.images.personaAudioIndicatorInactive.drawableResId),
                    contentDescription = null,
                    modifier = Modifier
                        .height(37.dp)
                        .fillMaxWidth()
                )
            }
            AudioIndicatorState.StartSpeaking -> {
                Text(
                    text = stringResource(res = Resources.strings.persona.startSpeaking),
                    color = Color.White,
                    fontSize = 17.sp,
                    fontWeight = FontWeight.Medium
                )
            }
            AudioIndicatorState.Speaking -> {
                Image(
                    painter = painterResource(Resources.images.personaAudioIndicatorActive.drawableResId),
                    contentDescription = null,
                    modifier = Modifier
                        .height(37.dp)
                        .scale(scaleY = scale, scaleX = 1F)
                )
            }
        }
    }
}

object DecibelToPercentageConverter {

    private val minDb: Float = -2.0F
    private val maxDb: Float = 10.0F

    private val minResult = 0.08F

    /// Converts a decibel value to a percentage (0 to 1) based on the loudness of a human voice.
    /// - Parameter decibel: The decibel value to convert.
    /// - Returns: A percentage value (0 to 100) representing the relative loudness.
    fun convertToPercentage( decibel: Float) : Float {
        // Ensure the value is clamped within the valid range
        val clampedDecibel = max(minDb, min(decibel, maxDb))
        // Normalize the value to a 0-100 scale
        val percentage = ((clampedDecibel - minDb) / (maxDb - minDb))

        if (percentage < minResult) {
            return minResult
        }

        return percentage
    }
}