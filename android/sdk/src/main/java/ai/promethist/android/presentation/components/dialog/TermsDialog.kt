package ai.promethist.android.presentation.components.dialog

import ai.promethist.android.presentation.components.text.URLContainer
import ai.promethist.android.presentation.components.text.URLText
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.window.DialogProperties


data class TermsParagraph(val title: String, val text: String, val urls: List<URLContainer>)
@Composable
fun TermsAlertDialog(
    confirmButtonTitle: String,
    dismissButtonTitle: String,
    paragraphs: List<TermsParagraph>,
    confirmAction: () -> Unit,
    dismissAction: () -> Unit,
) {
    val properties = DialogProperties(
        dismissOnBackPress = false,
        dismissOnClickOutside = false,
        usePlatformDefaultWidth = false
    )
    val buttonFontSize = 16.sp
    AlertDialog(
        modifier = Modifier
            .padding(8.dp)
            .fillMaxWidth(0.92F),
        properties = properties,
        onDismissRequest = {},
        confirmButton = {
            TextButton(onClick = { confirmAction() }) {
                Text(confirmButtonTitle, fontSize = buttonFontSize)
            }
        },
        dismissButton = {
            TextButton(onClick = { dismissAction() }) {
                Text(dismissButtonTitle, fontSize = buttonFontSize)
            }
        },
        text = {
            Column(
                modifier = Modifier.padding(top = 16.dp)
            ) {
                paragraphs.forEach {
                    Text(
                        text = it.title,
                        modifier = Modifier.padding(top = 8.dp, bottom = 4.dp),
                        style = MaterialTheme.typography.titleLarge,
                        fontWeight = FontWeight.Bold,
                        fontSize = 18.sp
                    )
                    URLText(
                        text = it.text,
                        urls = it.urls
                    )
                }
            }
        }
    )
}
