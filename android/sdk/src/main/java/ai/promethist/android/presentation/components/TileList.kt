package ai.promethist.android.presentation.components

import ai.promethist.android.style.SpaceXSmall
import ai.promethist.android.presentation.components.button.TileButton
import ai.promethist.android.presentation.components.text.TitleText
import ai.promethist.android.style.heading1
import ai.promethist.channel.command.Actions
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign

// For #actions
@Composable
fun TileList(
    actions: Actions,
    onTileClick: (String, String) -> Unit
) {
    Column(
        modifier = Modifier
            .fillMaxWidth(),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        if (actions.title.isNotBlank()) {
            TitleText(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(vertical = SpaceXSmall),
                title = actions.title,
                textStyle = heading1,
                textAlign = TextAlign.Center
            )
        }

        Column(
            modifier = Modifier
                .fillMaxWidth()
                .verticalScroll(rememberScrollState()),
            verticalArrangement = Arrangement.spacedBy(SpaceXSmall),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            actions.tiles.forEach {
                TileButton(
                    tile = it,
                    fullWidth = actions.tiles.size > 1,
                    onClickCallback = { text, label, preferredMode -> onTileClick(text, "") }
                )
            }
        }
    }
}