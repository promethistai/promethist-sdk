package ai.promethist.android.presentation.components

data class ControlConfig(
    val undoEnabled: Boolean,
    val homeEnabled: Boolean,
    val skipEnabled: Boolean
)
