package ai.promethist.android.presentation.components.pager

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.pager.HorizontalPager
import androidx.compose.foundation.pager.PagerState
import androidx.compose.foundation.pager.rememberPagerState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.snapshotFlow
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.unit.dp
import com.google.android.material.math.MathUtils.lerp
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlin.math.absoluteValue

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun ElysaiHorizontalPager(
    modifier: Modifier = Modifier,
    elementList: List<Any>,
    pagerState: PagerState = rememberPagerState(pageCount = { elementList.size }),
    onPageSelected: (Int) -> Unit,
    content: @Composable (Int) -> Unit
) {
    Box(
        modifier = modifier
            .background(Color.Transparent),
        contentAlignment = Alignment.Center
    ) {
        LaunchedEffect(pagerState) {
            snapshotFlow { pagerState.currentPage }.distinctUntilChanged().collect { page ->
                onPageSelected(page)
            }
        }

        HorizontalPager(
            contentPadding = PaddingValues(horizontal = 48.dp, vertical = 32.dp),
            state = pagerState
        ) { page ->
            Box(
                modifier = Modifier
                    .graphicsLayer {

                        val pageOffset =
                            ((pagerState.currentPage - page) + pagerState.currentPageOffsetFraction).absoluteValue
                        val scaleModifier = 1.15F

                        lerp(
                            0.85f,
                            1f,
                            1f - pageOffset.coerceIn(0f, 1f)
                        ).also { scale ->
                            scaleX = scale * scaleModifier
                            scaleY = scale * scaleModifier
                        }
                    }
                    .fillMaxWidth(),
                content = { content(page) }
            )
        }
    }

}