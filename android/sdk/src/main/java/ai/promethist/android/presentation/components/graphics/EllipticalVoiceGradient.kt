package ai.promethist.android.presentation.components.graphics

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.animation.scaleIn
import androidx.compose.animation.scaleOut
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.scale
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp

@OptIn(ExperimentalAnimationApi::class)
@Composable
fun EllipticalVoiceGradient(
    modifier: Modifier,
    visible: Boolean
) {
    AnimatedVisibility(
        modifier = modifier,
        visible = visible,
        enter = scaleIn(),
        exit = scaleOut()
    ) {
        BoxWithConstraints(
            Modifier
                .height(192.dp)
                .offset(y = 48.dp)
        ) {
            GradientBox(color = 0x80000000)
            GradientBox(color = 0xFF34C759)
        }
    }
}

@Composable
private fun GradientBox(color: Long) {
    Box(
        Modifier
            .fillMaxSize()
            .scale(3F, 2.5F)
            .background(
                brush = Brush.radialGradient(
                    colors = listOf(
                        Color(color),
                        Color.Transparent,
                    )
                ),
            )
    )
}