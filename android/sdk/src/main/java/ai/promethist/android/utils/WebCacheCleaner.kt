package ai.promethist.android.utils

import android.webkit.CookieManager
import android.webkit.WebStorage

object WebCacheCleaner {

    fun clean() {
        WebStorage.getInstance().deleteAllData()
        CookieManager.getInstance().removeAllCookies(null)
        CookieManager.getInstance().flush()
    }
}