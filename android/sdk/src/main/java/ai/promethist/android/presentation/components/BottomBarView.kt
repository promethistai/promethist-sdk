package ai.promethist.android.presentation.components

import ai.promethist.android.style.RadiusXXLarge
import ai.promethist.android.style.SpaceXSmall
import ai.promethist.android.ext.painterResource
import ai.promethist.channel.item.Modal
import ai.promethist.client.resources.Resources
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.unit.dp

@Composable
fun BottomBarView(
    modifier: Modifier = Modifier,
    modalItem: Modal?,
    onPersonaSelected: () -> Unit,
    onUndoClick: () -> Unit,
    onHomeClick: () -> Unit,
    onSkipClick: () -> Unit
) {
    Box(
        modifier = modifier
            .fillMaxSize(),
        contentAlignment = Alignment.BottomCenter
    ) {
        Column(
            modifier = Modifier
                .clip(RoundedCornerShape(RadiusXXLarge))
                .shadow(3.dp)
                .background(MaterialTheme.colors.surface.copy(alpha = 0.75f))
        ) {
            // TODO display modal items

            Row(
                modifier = Modifier
                    .padding(horizontal = SpaceXSmall),
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.spacedBy(SpaceXSmall)
            ) {
                IconButton(onClick = onUndoClick) {
                    Icon(
                        modifier = Modifier
                            .size(ControlsIconSize),
                        painter = painterResource(Resources.images.icArrowUturnBackward),
                        contentDescription = null
                    )
                }

                IconButton(onClick = onHomeClick) {
                    Icon(
                        modifier = Modifier
                            .size(ControlsIconSize),
                        painter = painterResource(Resources.images.icHouse),
                        contentDescription = null
                    )
                }

                IconButton(onClick = onSkipClick) {
                    Icon(
                        modifier = Modifier
                            .size(ControlsIconSize),
                        painter = painterResource(Resources.images.icForwardEnd),
                        contentDescription = null
                    )
                }
            }
        }
    }
}

private val ControlsIconSize = 24.dp