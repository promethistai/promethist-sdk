package ai.promethist.android.presentation.components

import ai.promethist.SharedRes
import ai.promethist.android.ext.runOnMainLooper
import ai.promethist.android.presentation.components.button.OptionButton
import ai.promethist.android.presentation.components.text.TitleText
import ai.promethist.android.style.SpaceXXSmall
import ai.promethist.client.model.CameraPermission
import ai.promethist.client.Client
import ai.promethist.util.Log
import android.Manifest
import android.content.Context
import android.graphics.Bitmap
import android.graphics.Matrix
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.OptIn
import androidx.camera.core.Camera
import androidx.camera.core.CameraSelector
import androidx.camera.core.ExperimentalGetImage
import androidx.camera.core.ImageAnalysis
import androidx.camera.core.ImageCapture
import androidx.camera.core.ImageCaptureException
import androidx.camera.core.ImageProxy
import androidx.camera.core.Preview
import androidx.camera.core.UseCase
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.camera.view.PreviewView
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clipToBounds
import androidx.compose.ui.draw.scale
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.compose.ui.viewinterop.AndroidView
import androidx.lifecycle.LifecycleOwner
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.concurrent.Executors

@Composable
@OptIn(ExperimentalGetImage::class)
fun CameraControls(
    onImage: (Bitmap?) -> Unit,
    onUserInput: (Boolean) -> Unit,
    onCameraActivated: () -> Unit
) {
    val config = Client.appConfig()
    val context = LocalContext.current

    var uiDisplayed by remember { mutableStateOf(config.cameraPermission == CameraPermission.Granted) }
    var errorState by remember { mutableStateOf(false) }


    val cameraProvider by remember {
        mutableStateOf(ProcessCameraProvider.getInstance(context).get())
    }

    val cameraState = remember { mutableStateOf<Camera?>(null) }

    val launcher = rememberLauncherForActivityResult(
        ActivityResultContracts.RequestPermission()
    ) { isGranted: Boolean ->
        if (isGranted) {
            config.cameraPermission = CameraPermission.Granted
            if (config.preset.shouldConfirmCamera)
                onUserInput(true)
            else
                uiDisplayed = true
        } else {
            config.cameraPermission = CameraPermission.Denied
            onUserInput(false)
        }
    }

    DisposableEffect(Unit) {
        onDispose {
            cameraProvider?.unbindAll()
        }
    }

    if (!uiDisplayed) {
        LaunchedEffect(Unit) {
            launcher.launch(Manifest.permission.CAMERA)
        }
    } else {
        onCameraActivated()
        val imageCapture = ImageCapture.Builder()
            .build()

        var photo by remember { mutableStateOf<Bitmap?>(null) }

        Column(
            modifier = Modifier.fillMaxWidth(),
            verticalArrangement = Arrangement.spacedBy(SpaceXXSmall)
        ) {
            if (!errorState) {
                if (photo == null)
                    Box(
                        modifier = Modifier.clipToBounds()
                    ) {
                        CameraPreview(
                            modifier = Modifier
                                .fillMaxWidth(),
                            imageCapture = imageCapture,
                            cameraProvider = cameraProvider,
                            cameraOutput = cameraState
                        )
                    }
                else
                    photo?.let {
                        BitmapImage(
                            bitmap = it,
                            modifier = Modifier
                                .fillMaxWidth()
                                .scale(scaleX = -1F, scaleY = 1F)
                        )
                    }
            } else {
                TitleText(
                    modifier = Modifier,
                    title = SharedRes.strings.camera_error.getString(context)
                )
            }


            Row(
                horizontalArrangement = Arrangement.SpaceBetween,
                modifier = Modifier.fillMaxWidth()
            ) {

                OptionButton(
                    text = SharedRes.strings.camera_cancel.getString(context),
                    onClick = {
                        errorState = false
                        onImage(null)
                    },
                )
                if (photo == null && !errorState)
                    OptionButton(
                        text = SharedRes.strings.camera_capture.getString(context),
                        onClick = {
                            imageCapture.takePicture(
                                Executors.newSingleThreadExecutor(),
                                object : ImageCapture.OnImageCapturedCallback() {
                                    override fun onCaptureSuccess(image: ImageProxy) {
                                        runOnMainLooper { cameraProvider?.unbindAll() }
                                        val matrix = Matrix()
                                        matrix.postRotate(image.imageInfo.rotationDegrees.toFloat())
                                        val bitmap = image.toBitmap()
                                        photo = Bitmap.createBitmap(
                                            bitmap,
                                            0,
                                            0,
                                            image.width,
                                            image.height,
                                            matrix,
                                            true
                                        )
                                        image.image?.close()
                                    }

                                    override fun onError(exception: ImageCaptureException) {
                                        exception.printStackTrace()
                                        errorState = true
                                    }
                                }
                            )
                        }
                    )
                else {
                    OptionButton(
                        text = SharedRes.strings.camera_retake.getString(context),
                        onClick = {
                            errorState = false
                            photo = null
                        },
                    )

                    if (!errorState) {
                        OptionButton(
                            text = SharedRes.strings.camera_send.getString(context),
                            onClick = {
                                onImage(photo)
                            },
                        )
                    }
                }
            }


        }
    }
}

@Composable
fun BitmapImage(bitmap: Bitmap, modifier: Modifier) {
    Image(
        bitmap = bitmap.asImageBitmap(),
        contentDescription = "User photo",
        modifier = modifier
    )
}

// Taken from https://stackoverflow.com/a/70302763
@Composable
fun CameraPreview(
    modifier: Modifier = Modifier,
    cameraProvider: ProcessCameraProvider?,
    cameraOutput: MutableState<in Camera>,
    cameraSelector: CameraSelector = CameraSelector.DEFAULT_FRONT_CAMERA,
    implementationMode: PreviewView.ImplementationMode = PreviewView.ImplementationMode.COMPATIBLE,
    scaleType: PreviewView.ScaleType = PreviewView.ScaleType.FILL_CENTER,
    imageAnalysis: ImageAnalysis? = null,
    imageCapture: ImageCapture? = null,
    preview: Preview = remember { Preview.Builder().build() }
) {
    val context = LocalContext.current
    val lifecycleOwner = LocalLifecycleOwner.current

    val scope = rememberCoroutineScope()
    val bindings = remember { PreviewViewBindings(context) }

    AndroidView(
        modifier = modifier,
        factory = { _ ->
            PreviewView(context).also {
                it.scaleType = scaleType
                it.implementationMode = implementationMode
                preview.setSurfaceProvider(it.surfaceProvider)
            }
        },
        update = { view ->
            if (view.scaleType != scaleType) {
                view.scaleType = scaleType
            }
            if (view.implementationMode != implementationMode) {
                view.implementationMode = implementationMode
            }

            val oldUseCases: List<UseCase>
            val newUseCases: List<UseCase>
            if (bindings.lifecycleOwner !== lifecycleOwner ||
                bindings.cameraSelector != cameraSelector
            ) {
                oldUseCases = bindings.getUseCases()
                newUseCases = listOfNotNull(preview, imageCapture, imageAnalysis)
            } else {
                oldUseCases = listOfNotNull(
                    bindings.preview?.takeIf { it !== preview },
                    bindings.imageCapture?.takeIf { it !== imageCapture },
                    bindings.imageAnalysis?.takeIf { it !== imageAnalysis }
                )
                newUseCases = listOfNotNull(
                    preview.takeIf { it !== bindings.preview },
                    imageCapture?.takeIf { it !== bindings.imageCapture },
                    imageAnalysis?.takeIf { it !== bindings.imageAnalysis }
                )
            }

            if (oldUseCases.isNotEmpty() || newUseCases.isNotEmpty()) {
                bindings.bindingJob?.cancel()
                bindings.bindingJob = scope.launch(Dispatchers.Main.immediate) {
                    if (oldUseCases.isNotEmpty()) {
                        cameraProvider?.unbind(*oldUseCases.toTypedArray())
                    }
                    if (newUseCases.isNotEmpty()) {
                        val camera =
                            cameraProvider?.bindToLifecycle(
                                lifecycleOwner,
                                cameraSelector,
                                *newUseCases.toTypedArray()
                            )

                        cameraOutput.value = camera!!
                    }

                    if (bindings.preview !== preview) {
                        bindings.preview?.setSurfaceProvider(null)
                        preview.setSurfaceProvider(view.surfaceProvider)
                    }

                    bindings.lifecycleOwner = lifecycleOwner
                    bindings.cameraSelector = cameraSelector
                    bindings.preview = preview
                    bindings.imageCapture = imageCapture
                    bindings.imageAnalysis = imageAnalysis
                }
            }
        }
    )
}


// Taken from https://github.com/skgmn/CameraXX/blob/master/library-composable/src/main/java/com/github/skgmn/cameraxx/CameraPreview.kt
private class PreviewViewBindings(
    private val appContext: Context
) : RememberObserver {
    var bindingJob: Job? = null
    var lifecycleOwner: LifecycleOwner? = null
    var cameraSelector: CameraSelector? = null
    var preview: Preview? = null
    var imageCapture: ImageCapture? = null
    var imageAnalysis: ImageAnalysis? = null

    fun getUseCases(): List<UseCase> {
        return listOfNotNull(preview, imageCapture, imageAnalysis)
    }

    override fun onAbandoned() {
        onForgotten()
    }

    @kotlin.OptIn(DelicateCoroutinesApi::class)
    override fun onForgotten() {
        val useCases = getUseCases()
        if (useCases.isNotEmpty()) {
            GlobalScope.launch(Dispatchers.Main.immediate) {
                val cameraProvider =
                    withContext(Dispatchers.IO) {
                        ProcessCameraProvider.getInstance(appContext).get()
                    }
                cameraProvider.unbind(*useCases.toTypedArray())
            }
        }
    }

    override fun onRemembered() {
    }
}
