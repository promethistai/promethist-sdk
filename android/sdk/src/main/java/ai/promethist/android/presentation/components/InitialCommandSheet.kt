package ai.promethist.android.presentation.components

import ai.promethist.android.ext.init
import ai.promethist.android.ext.stringResource
import ai.promethist.android.style.SpaceMedium
import ai.promethist.android.style.SpaceMediumLarge
import ai.promethist.client.resources.Resources
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.MaterialTheme
import androidx.compose.material3.Button
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.ModalBottomSheet
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.SheetState
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.unit.dp

@OptIn(ExperimentalMaterial3Api::class, ExperimentalComposeUiApi::class)
@Composable
fun InitialCommandSheet(
    value: String,
    onValueChanged: (String) -> Unit,
    onDismissRequest: () -> Unit,
    state: SheetState
) {
    val keyboardController = LocalSoftwareKeyboardController.current
    var textValue by remember { mutableStateOf(value.ifBlank { "#intro" }) }

    ModalBottomSheet(
        sheetState = state,
        onDismissRequest = { onDismissRequest() },
    ) {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(SpaceMedium)
        ) {
            OutlinedTextField(
                modifier = Modifier
                    .fillMaxWidth()
                    .height(120.dp)
                    .background(
                        color = Color.init(res = Resources.colors.growthSurface),
                        shape = RoundedCornerShape(5.dp)
                    ),
                shape = RoundedCornerShape(5.dp),
                value = textValue,
                onValueChange = {
                    textValue = it
                    onValueChanged(it)
                },
                keyboardOptions = KeyboardOptions(imeAction = ImeAction.Done),
                keyboardActions = KeyboardActions(
                    onDone = {
                        keyboardController?.hide()
                        onDismissRequest()
                    }
                ),
                maxLines = 3,
                textStyle = MaterialTheme.typography.caption
            )

            Spacer(modifier = Modifier.height(SpaceMediumLarge))

            Button(
                modifier = Modifier.fillMaxWidth(),
                onClick = onDismissRequest
            ) {
                Text(stringResource(res = Resources.strings.settings.done))
            }
        }
    }
}