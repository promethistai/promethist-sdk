package ai.promethist.android.presentation.components.dialog

import ai.promethist.android.ext.painterResource
import ai.promethist.android.ext.textShadow
import ai.promethist.android.style.SpaceLarge
import ai.promethist.android.style.SpaceMedium
import ai.promethist.android.style.SpaceMediumSmall
import ai.promethist.android.style.heading2
import ai.promethist.client.resources.Resources
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.Hyphens
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.wear.compose.material.Button
import androidx.wear.compose.material.ButtonDefaults
import androidx.wear.compose.material.Text

@Composable
fun AlertPopupOverlay(
    text: String,
    textAlign: TextAlign,
    backgroundColor: Color,
    onDismiss: (() -> Unit)? = null
) {
    Box(
        modifier = Modifier
            .clip(RoundedCornerShape(10.dp))
            .background(color = backgroundColor)
    ) {
        Row(
            modifier = Modifier.padding(SpaceMedium),
            verticalAlignment = Alignment.CenterVertically
        ) {
            Text(
                text = text,
                style = heading2.copy(
                    fontSize = 15.sp,
                    shadow = textShadow,
                    hyphens = Hyphens.Auto,
                    fontWeight = FontWeight.Medium,
                    textAlign = textAlign,
                    color = Color.White
                )
            )
            if (onDismiss != null) {
                Spacer(modifier = Modifier.width(SpaceMediumSmall))
                Button(
                    modifier = Modifier
                        .size(SpaceLarge),
                    onClick = { onDismiss() },
                    colors = ButtonDefaults.buttonColors(backgroundColor = Color.Transparent)
                ) {
                    Image(
                        painter = painterResource(res = Resources.images.icClose),
                        contentDescription = null,
                        colorFilter = ColorFilter.tint(Color.White)
                    )
                }
            }
        }
    }
}