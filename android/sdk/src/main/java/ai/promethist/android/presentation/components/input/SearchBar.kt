package ai.promethist.android.presentation.components.input

import ai.promethist.android.presentation.components.button.CircularButton
import ai.promethist.android.style.SpaceMediumLarge
import ai.promethist.android.style.SpaceMediumSmall
import ai.promethist.android.style.SpaceSmall
import ai.promethist.android.style.SpaceXSmall
import ai.promethist.android.style.SpaceXXXLarge
import ai.promethist.client.resources.Resources
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.expandHorizontally
import androidx.compose.animation.shrinkHorizontally
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.IconButtonDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Alignment.Companion.CenterVertically
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import ai.promethist.android.ext.painterResource as painterRes

@Composable
fun SearchBar(
    query: String,
    index: Int,
    total: Int,
    onValueChange: (String) -> Unit,
    onNext: () -> Unit,
    onPrevious: () -> Unit,
) {
    var searchFieldVisible by remember { mutableStateOf(false) }

    Box(
        modifier = Modifier
            .fillMaxWidth()
    ){
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .height(120.dp)
                .padding(horizontal = SpaceMediumLarge)
        ) {
            // CircButton + searchbar row
            Box(
                modifier = Modifier
                    .fillMaxWidth()
                    .height(60.dp)
            ) {
                androidx.compose.animation.AnimatedVisibility(
                    visible = searchFieldVisible,
                    enter = expandHorizontally { -it },
                    exit = shrinkHorizontally { -it },
                ) {
                    Row(
                        modifier = Modifier
                            .background(Color(0x33F5F5F7), shape = RoundedCornerShape(30.dp))
                            .fillMaxSize()
                            .padding(start = SpaceXXXLarge, end = SpaceXSmall),
                        verticalAlignment = CenterVertically
                    ) {
                        BasicTextField(
                            modifier = Modifier
                                .weight(1F)
                                .padding(horizontal = SpaceMediumSmall),
                            value = query,
                            onValueChange = {
                                onValueChange(it)
                            },
                            singleLine = true,
                            textStyle = TextStyle(
                                color = Color.White.copy(alpha = 0.8f),
                                fontSize = 18.sp
                            ),
                            keyboardOptions = KeyboardOptions.Default
                        )
                        Text(
                            text = "$index/$total",
                            color = Color.White,
                            fontSize = 18.sp,
                            modifier = Modifier
                                .padding(horizontal = SpaceSmall)
                        )
                        IconButton(
                            onClick = {
                                searchFieldVisible = false
                                onValueChange("")
                            },
                            modifier = Modifier
                                .size(48.dp)
                                .padding(SpaceXSmall),
                            colors = IconButtonDefaults.iconButtonColors(containerColor = Color.White)
                        ) {
                            Icon(
                                painter = painterRes(res = Resources.images.icClose),
                                contentDescription = null,
                                tint = Color.Gray
                            )
                        }
                    }
                }

                CircularButton(
                    modifier = Modifier.align(Alignment.CenterStart),
                    isActive = false,
                    painter = painterRes(res = Resources.images.personaSearch)
                ) {
                    searchFieldVisible = true
                }

            }
            // up down + done row

          AnimatedVisibility(visible = total > 0) {
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .height(48.dp)
                ) {
                    IconButton(
                        onClick = {
                            onPrevious()
                        },
                        modifier = Modifier
                            .size(48.dp)
                            .padding(SpaceXSmall)
                    ) {
                        Icon(
                            painter = painterResource(id = ai.promethist.android.R.drawable.baseline_keyboard_arrow_up_24),
                            contentDescription = null,
                            tint = Color.White.copy(alpha = 0.6f)
                        )
                    }
                    IconButton(
                        onClick = {
                            onNext()
                        },
                        modifier = Modifier
                            .size(48.dp)
                            .padding(SpaceXSmall)
                    ) {
                        Icon(
                            painter = painterResource(id = ai.promethist.android.R.drawable.baseline_keyboard_arrow_down_24),
                            contentDescription = null,
                            tint = Color.White.copy(alpha = 0.6f)
                        )
                    }
                }
          }

        }
    }
}