package ai.promethist.android

import ai.promethist.android.services.AndroidAvatarPlayerController
import ai.promethist.android.services.AndroidTechnicalReportProvider
import ai.promethist.android.services.speechRecognizer.AndroidSpeechRecognizer
import ai.promethist.android.services.speechRecognizer.duplex.DuplexAndroidSpeechRecognizer
import ai.promethist.client.Client
import ai.promethist.client.avatar.unity.AvatarEventsApiV1Impl
import ai.promethist.client.avatar.unity.AvatarPlayerImpl
import ai.promethist.client.speech.SpeechRecognizer
import ai.promethist.client.speech.SpeechRecognizerController
import ai.promethist.client.ClientModel
import ai.promethist.client.ClientModelConfig
import ai.promethist.client.ClientModelImpl
import ai.promethist.client.channel.WebSocketClientChannel
import android.content.Context

object ClientModelFactory {
    fun create(context: Context, promethist: Promethist): ClientModel {
        val config = ClientModelConfig(isHomeScreenEnabled = true)

        val unityController = AndroidAvatarPlayerController(promethist)
        val avatarEventsApi = AvatarEventsApiV1Impl(unityController)
        val avatarPlayer = AvatarPlayerImpl(unityController, avatarEventsApi)
        unityController.bridge = avatarPlayer

        val androidSpeechRecognizer: SpeechRecognizerController = when (Client.appConfig().duplexEnabled) {
            true -> DuplexAndroidSpeechRecognizer(context, promethist)
            else -> AndroidSpeechRecognizer(context, promethist)
        }
        val speechRecognizer = SpeechRecognizer(androidSpeechRecognizer)
        androidSpeechRecognizer.setCallback(speechRecognizer)

        val technicalReportProvider = AndroidTechnicalReportProvider(context)

        val uiStateHandler = ClientUiStateHandlerImpl()

        val clientChannel = WebSocketClientChannel()

        return ClientModelImpl(
            config = config,
            uiStateHandler = uiStateHandler,
            avatarPlayer = avatarPlayer,
            speechRecognizer = speechRecognizer,
            technicalReportProvider = technicalReportProvider,
            channel = clientChannel
        )
    }
}