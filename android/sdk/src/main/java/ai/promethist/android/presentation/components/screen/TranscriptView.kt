package ai.promethist.android.presentation.components.screen

import ai.promethist.android.presentation.components.text.AutoScrollTextV2
import ai.promethist.android.style.SpaceMediumLarge
import ai.promethist.android.style.SpaceSmall
import ai.promethist.android.style.SpaceXLarge
import ai.promethist.channel.item.VoiceInputItem
import ai.promethist.client.ClientUiState
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.runtime.Composable
import androidx.compose.runtime.State
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.max

@Composable
fun TranscriptView(
    modifier: Modifier,
    clientUiState: State<ClientUiState>
) {
    var subtitleOffset: Dp by remember { mutableStateOf(0.dp) }

    Column(
        modifier = modifier
            .fillMaxSize()
            .padding(horizontal = SpaceSmall)
            .padding(
                bottom = max(144.dp, subtitleOffset) + 24.dp,
                top = SpaceXLarge
            ),
        verticalArrangement = Arrangement.Bottom
    ) {
        AutoScrollTextV2(
            modifier = Modifier
                .height(120.dp)
                .alpha(if ((clientUiState.value.modal == null || clientUiState.value.modal is VoiceInputItem)) 1F else 0F)
                .wrapContentHeight(align = Alignment.Bottom)
                .padding(horizontal = SpaceMediumLarge),
            visible = clientUiState.value.visualItem?.text != null,
            clientUiState = clientUiState
        )
    }
}