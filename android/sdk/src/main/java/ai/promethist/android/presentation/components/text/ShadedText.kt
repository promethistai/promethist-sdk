package ai.promethist.android.presentation.components.text

import ai.promethist.android.ext.textShadow
import ai.promethist.android.style.heading2
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.Hyphens
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.sp

@Composable
fun ShadedText(
    modifier: Modifier,
    personaText: String,
    textSize: TextUnit = 24.sp
) {
    AutoResizeText(
        modifier = modifier.fillMaxWidth(),
        text = personaText,
        textAlign = TextAlign.Center,
        fontSizeRange = FontSizeRange(5.sp, textSize),
        style = heading2.copy(
            fontSize = textSize,
            shadow = textShadow,
            hyphens = Hyphens.Auto
        ),
        color = Color.White
    )
}