package ai.promethist.android.services

import ai.promethist.android.Promethist
import ai.promethist.android.utils.AppEvent
import ai.promethist.android.utils.AppEventBus
import ai.promethist.client.Client
import ai.promethist.client.avatar.AvatarPlayerBridge
import ai.promethist.client.avatar.AvatarPlayerController
import ai.promethist.client.avatar.unity.AvatarMessage
import com.unity3d.player.UnityPlayer
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class AndroidAvatarPlayerController(val promethist: Promethist) : AvatarPlayerController {

    override lateinit var bridge: AvatarPlayerBridge


    var scope: CoroutineScope = CoroutineScope(Job())
    init {
        if (androidUnityPlayerController == null)
            androidUnityPlayerController = this
    }

    override fun sendMessage(nameObject: String, functionName: String, message: String) {
        UnityPlayer.UnitySendMessage(nameObject, functionName, message)
    }

    override fun onSceneLoaded() = Unit

    override fun setAudioBlocked(isBlocked: Boolean) {
        AndroidAvatarPlayerController.isBlocked = isBlocked
    }

    override fun getIsAudioBlocked(): Boolean {
        return isBlocked
    }

    companion object {

        private var androidUnityPlayerController: AndroidAvatarPlayerController? = null

        // Must have return value, or the call from Unity fails
        @JvmStatic
        fun onMessageReceived(message: String): Any {
            androidUnityPlayerController?.let {
                it.scope.launch {
                    if (message.startsWith("download")) {
                        when(val parsedMessage = AvatarMessage.fromString(message)) {
                            AvatarMessage.DownloadFailed -> {
                                AppEventBus.emit(AppEvent.OnSplashScreenUpdate(true, 0F))
                            }
                            AvatarMessage.DownloadStarted -> {
                                AppEventBus.emit(AppEvent.OnSplashScreenUpdate(true, 0F))
                            }
                            AvatarMessage.DownloadEnded -> {
                                AppEventBus.emit(AppEvent.OnSplashScreenUpdate(true, 1F))
                            }
                            AvatarMessage.DownloadProgress -> {
                                parsedMessage.params["progress"]?.let { progress ->
                                    AppEventBus.emit(AppEvent.OnSplashScreenUpdate(
                                        true,
                                        progress.replace(",", ".").toFloat() / 100F)
                                    )
                                }
                            }
                            else -> Unit
                        }
                    } else if (message.startsWith("avatar")) {
                        when(AvatarMessage.fromString(message)) {
                            AvatarMessage.AvatarLoadStarted -> {
                                AppEventBus.emit(AppEvent.OnSplashScreenUpdate(true, null))
                            }
                            AvatarMessage.AvatarLoaded -> {
                                AppEventBus.emit(AppEvent.OnSplashScreenUpdate(false, null))
                                it.bridge.onMessageReceived(message)
                            }
                            AvatarMessage.AvatarLoadFailed -> {
                                AppEventBus.emit(AppEvent.OnSplashScreenUpdate(true, null))
                            }
                            else -> Unit
                        }
                    } else {
                        androidUnityPlayerController?.bridge?.onMessageReceived(message)
                    }
                }
            }

            // Duplex
            if (Client.appConfig().duplexEnabled) {
                when (message) {
                    "audioStarted" -> GlobalScope.launch {
                        AppEventBus.emit(AppEvent.OnAvatarAudioStarted)
                    }
                }
            }

            return Unit
        }

        // Duplex
        private var isBlocked = false

        /**
         * Sets the blocked state of the ARC and triggers an action based on the state.
         *
         * When the application is set to a blocked state (`isBlocked = true`), this method sends a
         * message to the Unity and blocks sending audio to Unity.
         */
        fun setBlocked(isBlocked: Boolean) {
            this.isBlocked = isBlocked

            if (isBlocked) {
                UnityPlayer.UnitySendMessage("AudioManager", "stopAudio", "")
            }
        }
    }
}