package ai.promethist.android.services.speechRecognizer

import ai.promethist.android.Promethist
import ai.promethist.android.ext.runOnMainLooper
import ai.promethist.android.utils.AppEvent
import ai.promethist.android.utils.AppEventBus
import ai.promethist.channel.model.Action
import ai.promethist.client.Client
import ai.promethist.client.model.MicPermission
import ai.promethist.client.speech.SpeechRecognizerController
import ai.promethist.client.speech.SpeechRecognizerControllerCallback
import ai.promethist.util.Log
import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.os.Bundle
import android.speech.RecognitionListener
import android.speech.SpeechRecognizer
import androidx.core.app.ActivityCompat
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class AndroidSpeechRecognizer(
    val context: Context,
    private val promethist: Promethist
): SpeechRecognizerController {

    private val appConfig = Client.appConfig()
    private var speechRecognizer: SpeechRecognizer? = null
    private var isActive = false
    var isPaused = false
    private lateinit var recognitionListener: SpeechRecognitionListener

    var partialResult: String = ""
    
    var _callback: SpeechRecognizerControllerCallback? = null

    override fun setCallback(callback: SpeechRecognizerControllerCallback) {
        _callback = callback
    }

    override suspend fun startTranscribe() {
        partialResult = ""
        if (appConfig.micPermission == MicPermission.Granted) {
            isActive = true
            startListening()
        } else if (ActivityCompat.checkSelfPermission(context, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
            askPermissions {
                if (it) {
                    appConfig.micPermission = MicPermission.Granted
                    if (appConfig.preset.shouldConfirmMic) {
                        _callback?.onTranscribeResult(Action.MicGranted.text, true)
                    } else {
                        isActive = true
                        startListening()
                    }
                } else {
                    appConfig.micPermission = MicPermission.Denied
                    _callback?.onTranscribeResult(Action.MicDenied.text, true)
                }
            }
        } else {
            _callback?.onTranscribeResult(Action.SpeechProcessingError.text, true)
        }
    }

    override fun stopTranscribe() {
        isActive = false
        runOnMainLooper {
            speechRecognizer?.cancel()
        }
    }

    private fun askPermissions(callback: (Boolean) -> Unit) {
        promethist.requestPermission(Manifest.permission.RECORD_AUDIO, callback)
    }

    private fun createSpeechRecognizer(): Boolean {
        if (speechRecognizer != null)
            return true
        return if (SpeechRecognizer.isRecognitionAvailable(context)) {
            recognitionListener = SpeechRecognitionListener(this)
            speechRecognizer = SpeechRecognizer.createSpeechRecognizer(context)
            speechRecognizer?.setRecognitionListener(recognitionListener)
            true
        } else {
            false
        }
    }

    private fun startListening() {
        runOnMainLooper {
            if (!createSpeechRecognizer()) {
                isActive = false
                _callback?.onTranscribeResult(Action.SpeechProcessingError.text, true)
                return@runOnMainLooper
            }

            val language = appConfig.locale.toLanguageTag()
            val speechRecognizerIntent = SpeechRecognizerIntentFactory.create(lang = language)

            try {
                speechRecognizer?.startListening(speechRecognizerIntent)
            } catch (se: SecurityException) {
                isActive = false
                _callback?.onTranscribeResult(Action.SpeechProcessingError.text, true)
            }
        }
    }

    override fun pause() {
        if (!isActive) return
        isPaused = true
        runOnMainLooper {
            speechRecognizer?.cancel()
        }
    }

    override fun resume() {
        if (!isActive) return
        isPaused = false
        runOnMainLooper {
            startListening()
        }
    }

    override fun setSilenceLength(length: Int) {

    }

    override fun onVisualItemReceived() {
        stopTranscribe()
    }

    override fun restartTranscribe() {}

    override fun setPrivileged(value: Boolean) {}

    class SpeechRecognitionListener(val recognizer: AndroidSpeechRecognizer): RecognitionListener {

        override fun onError(error: Int) = with(recognizer) {
            var errorMessage = "Error code $error occurred"
            when (error){
                SpeechRecognizer.ERROR_NO_MATCH -> {
                    errorMessage += ": Speech not matched"
                    if (isActive && partialResult.isEmpty()) {
                        startListening()
                    }
                }
                SpeechRecognizer.ERROR_SPEECH_TIMEOUT -> {
                    errorMessage += ": Speech recognition timed out"
                    isActive = false
                    _callback?.onTranscribeResult(Action.Silence.text, true)
                }
                SpeechRecognizer.ERROR_CLIENT -> {
                    errorMessage += ": Client error"
                    if (!isPaused)
                        isActive = false
                }
                SpeechRecognizer.ERROR_NETWORK, SpeechRecognizer.ERROR_NETWORK_TIMEOUT -> {
                    errorMessage += ": A problem with the network connection"
                    isActive = false
                    _callback?.onTranscribeResult(Action.SpeechProcessingError.text, true)
                }
                else -> {
                    isActive = false
                }
            }
            if (partialResult.isNotEmpty()) {
                _callback?.onTranscribeResult(partialResult, true)
            }
        }

        override fun onResults(results: Bundle?) {
            recognizer.isActive = false
            results?.let {
                val recognizedTexts =
                    it.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION)
                val confidenceScores =
                    it.getFloatArray(SpeechRecognizer.CONFIDENCE_SCORES)?.toList()

                if (recognizedTexts != null && confidenceScores != null) {
                    recognizer._callback?.onTranscribeResult(recognizedTexts[0] ?: "", true)
                } else {
                    recognizer._callback?.onTranscribeResult(Action.Silence.text, true)
                }
            }
        }

        override fun onPartialResults(partialResults: Bundle?) {
            partialResults?.let {
                val resultsList = it.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION)
                if (resultsList?.isNotEmpty() == true) {
                    recognizer.partialResult = resultsList[0] ?: ""
                    recognizer._callback?.onTranscribeResult(
                        resultsList[0] ?: "",
                        false
                    )
                }
            }
        }

        override fun onEvent(p0: Int, p1: Bundle?) {}
        override fun onReadyForSpeech(p0: Bundle?) {}
        override fun onBeginningOfSpeech() {}
        override fun onRmsChanged(rms: Float) {
            CoroutineScope(Dispatchers.Default).launch {
                AppEventBus.emit(AppEvent.MicPowerUpdated(rms))
            }
        }
        override fun onBufferReceived(p0: ByteArray?) {}
        override fun onEndOfSpeech() {}
    }
}