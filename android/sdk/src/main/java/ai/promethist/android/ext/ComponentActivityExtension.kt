package ai.promethist.android.ext

import ai.promethist.client.model.Appearance
import ai.promethist.client.Client
import android.content.res.Configuration
import android.view.WindowManager
import androidx.activity.ComponentActivity

fun ComponentActivity.addFlagLayoutNoLimit() {
    window.setFlags(
        WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
        WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
    )
}

fun ComponentActivity.updateSystemThemeSettings() {
    val appConfig = Client.appConfig()
    val resources = resources
    val resourcesConfig = resources.configuration

    resourcesConfig.uiMode = when (appConfig.appearance) {
        Appearance.System -> Configuration.UI_MODE_NIGHT_UNDEFINED
        Appearance.Light -> Configuration.UI_MODE_NIGHT_NO
        Appearance.Dark -> Configuration.UI_MODE_NIGHT_YES
    }
    // TODO replace with non-deprecated method
    resources.updateConfiguration(resourcesConfig, resources.displayMetrics)
}