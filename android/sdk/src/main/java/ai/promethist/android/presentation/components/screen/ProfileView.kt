package ai.promethist.android.presentation.components.screen

import ai.promethist.android.ext.init
import ai.promethist.android.ext.painterResource
import ai.promethist.android.ext.stringResource
import ai.promethist.android.presentation.ProfileActivity
import ai.promethist.android.presentation.SettingsActivity
import ai.promethist.android.presentation.SettingsActivity.Companion.dataStore
import ai.promethist.android.presentation.components.AppToolbar
import ai.promethist.android.style.SpaceLarge
import ai.promethist.android.style.SpaceMediumSmall
import ai.promethist.android.style.SpaceSmall
import ai.promethist.android.utils.AppEvent
import ai.promethist.android.utils.AppEventBus
import ai.promethist.android.utils.WebCacheCleaner
import ai.promethist.channel.command.ProfileData
import ai.promethist.client.Client
import ai.promethist.client.auth.OAuth2Client
import ai.promethist.client.resources.Resources
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.combinedClickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.AsyncImage
import com.jamal.composeprefs3.ui.PrefsScreen
import com.jamal.composeprefs3.ui.prefs.TextPref
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

@Composable
fun ProfileView(
    data: ProfileData?,
    authClient: OAuth2Client,
    onReset: () -> Unit,
    onBackClick: () -> Unit,
    onCopyClick: () -> Unit,
    setResultCode: (Int) -> Unit
) {

    Column(
        modifier = Modifier
            .background(MaterialTheme.colorScheme.background.copy(alpha = .9f))
            .fillMaxSize(),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        AppToolbar(
            title = stringResource(res = Resources.strings.profile.title),
            onBackClick = onBackClick
        )

        with(authClient.getStoredUserInfo()) {
            ProfileHeader(
                name = this["name"] ?: data?.name,
                email = this["email"],
                avatarUrl = this["avatarUrl"]
            )
        }

        ProfileLogin(
            authClient = authClient,
            onLogin = {
                setResultCode(ProfileActivity.LogInResultCode)
                onBackClick()
            },
            onLogout = {
                setResultCode(SettingsActivity.ShouldResetResultCode)
                onBackClick()
            },
            onCopyClick = { onCopyClick() }
        )
    }
}

@Composable
private fun ProfileHeader(
    name: String?,
    email: String?,
    avatarUrl: String? = null
) {
    val title: String = (name ?: "").ifBlank {
        stringResource(res = Resources.strings.profile.user)
    }

    Column(
        modifier = Modifier
            .background(MaterialTheme.colorScheme.background)
            .fillMaxWidth()
            .padding(vertical = HeaderPadding.dp),
        verticalArrangement = Arrangement.spacedBy(SpaceMediumSmall),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        if (avatarUrl == null) {
            Image(
                modifier = Modifier.size(AvatarSizeDp.dp),
                painter = painterResource(res = Resources.images.personCropCircle),
                contentDescription = null
            )
        } else {
            AsyncImage(
                modifier = Modifier
                    .size(AvatarSizeDp.dp)
                    .clip(RoundedCornerShape((AvatarSizeDp / 2).dp)),
                contentScale = ContentScale.Crop,
                model = avatarUrl,
                contentDescription = null
            )
        }

        Text(
            text =  title,
            fontSize = 18.sp,
            fontWeight = FontWeight.Medium,
            color = Color.init(Resources.colors.primaryLabel),
            maxLines = 1,
            overflow = TextOverflow.Ellipsis
        )

        email?.let {
            Text(
                text =  it,
                fontSize = 16.sp,
                fontWeight = FontWeight.Normal,
                color = Color.Gray,
                maxLines = 1,
                overflow = TextOverflow.Ellipsis
            )
        }
    }
}

@OptIn(DelicateCoroutinesApi::class, ExperimentalFoundationApi::class)
@Composable
fun ProfileLogin(
    authClient: OAuth2Client,
    onLogin: () -> Unit,
    onLogout: () -> Unit,
    onCopyClick: () -> Unit,
) {
    val coroutineScope = rememberCoroutineScope()
    PrefsScreen(
        dataStore = LocalContext.current.dataStore,
        modifier = Modifier
            .fillMaxSize()
            .padding(horizontal = SpaceLarge)
    ) {

        prefsGroup({ Text(text = "") }) {
            prefsItem {
                TextPref(
                    title = stringResource(
                        res = if (authClient.isLoggedIn())
                            Resources.strings.auth.logout
                        else
                            Resources.strings.auth.login
                    ),
                    modifier = Modifier
                        .background(Color.White),
                    textColor = MaterialTheme.colorScheme.primary,
                    enabled = true,
                    onClick = {
                        if (authClient.isLoggedIn()) {
                            coroutineScope.launch {
                                authClient.logout()
                                WebCacheCleaner.clean()
                                GlobalScope.launch {
                                    delay(1000)
                                    AppEventBus.emit(AppEvent.OnAuthSessionUpdated)
                                }
                                onLogout()
                            }
                        } else {
                            onLogin()
                        }
                    }
                )
            }
        }

        prefsGroup({
            Text(
                text = stringResource(res = Resources.strings.profile.deviceId),
                modifier = Modifier
                    .padding(bottom = SpaceSmall, start = SpaceSmall),
                color = MaterialTheme.colorScheme.onBackground
            )
        }) {
            prefsItem {
                TextPref(
                    title = Client.appConfig().deviceId,
                    modifier = Modifier
                        .background(Color.White)
                        .combinedClickable(
                            interactionSource = MutableInteractionSource(),
                            indication = null,
                            onClick = {},
                            onLongClick = {
                                onCopyClick()
                            }
                        )
                )
            }
        }
    }
}

private const val AvatarSizeDp = 90
private const val HeaderPadding = 40