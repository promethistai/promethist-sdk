package ai.promethist.android.presentation.components.video

import ai.promethist.android.utils.OnLifecycleEvent
import ai.promethist.android.R
import ai.promethist.android.ext.init
import ai.promethist.android.presentation.components.ShadowIcon
import ai.promethist.android.ext.noRippleClickable
import ai.promethist.android.ext.painterResource
import ai.promethist.client.resources.Resources
import android.util.Xml
import androidx.annotation.OptIn
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.core.LinearEasing
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.animation.core.tween
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.size
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.compose.ui.viewinterop.AndroidView
import androidx.lifecycle.Lifecycle
import androidx.media3.common.text.Cue
import androidx.media3.common.util.UnstableApi
import androidx.media3.exoplayer.ExoPlayer
import androidx.media3.ui.AspectRatioFrameLayout
import androidx.media3.ui.CaptionStyleCompat
import androidx.media3.ui.PlayerView
import dev.icerock.moko.resources.ImageResource

@OptIn(UnstableApi::class) @Composable
fun ExoPlayerContainer(
    modifier: Modifier,
    exoPlayer: ExoPlayer,
    colorBackgroundOverlay: Boolean,
    opacity: Double,
    isPaused: Boolean,
    onClick: () -> Unit
) {
    val context = LocalContext.current
    val imageAlpha: Float by animateFloatAsState(
        targetValue = if (opacity == 1.0) 1f else 0f,
        animationSpec = tween(
            durationMillis = 280,
            easing = LinearEasing,
        ), label = ""
    )

    OnLifecycleEvent { _, event ->
        when (event) {
            Lifecycle.Event.ON_RESUME -> exoPlayer.play()
            Lifecycle.Event.ON_PAUSE -> exoPlayer.pause()
            Lifecycle.Event.ON_STOP -> { /* exoPlayer.seekToNext() */ }
            else -> Unit
        }
    }

    Box {
//        Image(
//            modifier = Modifier
//                .fillMaxHeight(),
//            painter = painterResource(res = background),
//            contentDescription = "Background image",
//            contentScale = ContentScale.Crop
//        )

        DisposableEffect(
            AndroidView(
                modifier = modifier
                    .fillMaxWidth()
                    .alpha(imageAlpha)
                    .noRippleClickable(onClick = onClick),
                factory = {
                    val xmlAttributes = context.resources.getXml(R.xml.player_view).let {
                        try {
                            it.next()
                            it.nextTag()
                        } catch (e: Exception) {
                            // do something, log, whatever
                        }
                        Xml.asAttributeSet(it)
                    }

                    // exo player view for our video player
                    PlayerView(context, xmlAttributes).apply {
                        subtitleView?.let {
                            it.setApplyEmbeddedFontSizes(false)
                            it.setApplyEmbeddedStyles(false)
                            it.setFixedTextSize(Cue.TEXT_SIZE_TYPE_ABSOLUTE, 18F)
                            val style = CaptionStyleCompat(
                                android.graphics.Color.TRANSPARENT,
                                android.graphics.Color.TRANSPARENT,
                                android.graphics.Color.TRANSPARENT,
                                CaptionStyleCompat.EDGE_TYPE_NONE,
                                android.graphics.Color.TRANSPARENT,
                                null
                            )
                            it.setStyle(style)
                        }
                        player = exoPlayer
                        useController = false
                        resizeMode = AspectRatioFrameLayout.RESIZE_MODE_ZOOM
                    }
                }
            )
        ) {
            onDispose {
                // release player when no longer needed
                exoPlayer.release()
            }
        }

        AnimatedVisibility(
            visible = colorBackgroundOverlay,
            enter = fadeIn(),
            exit = fadeOut()
        ) {
            Box(
                modifier = Modifier
                    .fillMaxSize()
                    .background(Color.init(res = Resources.colors.backgroundOverlay))
            )
        }

        if (isPaused) PauseIndicator()
    }
}

@Composable
private fun PauseIndicator() {
    Box(
        modifier = Modifier
            .fillMaxSize(),
        contentAlignment = Alignment.Center
    ) {
        ShadowIcon(
            modifier = Modifier
                .size(50.dp)
                .alpha(.3f),
            painter = painterResource(res = Resources.images.icPlay),
            contentDescription = null,
            tint = Color.White
        )
    }
}