package ai.promethist.android.presentation.components

import ai.promethist.android.ext.init
import ai.promethist.android.style.SpaceMedium
import ai.promethist.android.style.SpaceMediumLarge
import ai.promethist.android.style.SpaceSmall
import ai.promethist.channel.item.PersonaItem
import ai.promethist.channel.model.ResourceSize
import ai.promethist.client.model.ApplicationInfo
import ai.promethist.client.model.PersonaId
import ai.promethist.client.utils.RemotePersonaResourceManager
import ai.promethist.client.resources.Resources
import androidx.compose.animation.animateColorAsState
import androidx.compose.animation.core.animateIntAsState
import androidx.compose.animation.core.tween
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxScope
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.AsyncImage

@Composable
fun PersonaCard(
    currentIndex: Int?,
    index: Int,
    item: ApplicationInfo.Persona,
    onClick: (String, String, Int) -> Unit
) {
    val isActive = currentIndex == index
    val personaId = PersonaId.fromAvatarId(item.avatarRef)
    val imageUrl = RemotePersonaResourceManager.getPortrait(
        personaId = personaId,
        size = ResourceSize.L
    )
    BoxCardBackground(
        modifier = Modifier
            .height(cardHeight)
            .fillMaxWidth()
            .padding(vertical = SpaceSmall),
        isActive = isActive,
        onClick = {
            onClick(item.ref, item.avatarRef, index)
        }
    ) {
        Row (
            modifier = Modifier.padding(SpaceMedium),
            verticalAlignment = Alignment.CenterVertically
        ) {
            AsyncImage(
                model = imageUrl,
                contentDescription = null,
                modifier = Modifier
                    .width(imageWidth)
                    .height(imageHeight)
                    .clip(RoundedCornerShape(SpaceMedium))
                    .background(Color(0xFFE7E7E7))
            )
            Column(
                Modifier.padding(start = SpaceMedium)
            ) {
                Text(
                    text = item.name,
                    fontSize = nameFontSize,
                    modifier = Modifier.padding(vertical = SpaceSmall)
                )
                Text(
                    text = item.description ?: "",
                    fontSize = descriptionFontSize,
                    modifier = Modifier.padding(bottom = SpaceSmall)
                )
            }
        }
    }
}

@Composable
fun BoxCardBackground(
    modifier: Modifier,
    isActive: Boolean,
    onClick: () -> Unit,
    content: @Composable BoxScope.() -> Unit
) {
    val borderWidth by animateIntAsState(
        if (isActive) 3 else 1,
        label = "width",
        animationSpec = tween(200)
    )
    val animatedColor by animateColorAsState(
        if (isActive) Color.init(res = Resources.colors.accent) else Color(0xFFE2E2E2),
        label = "color",
        animationSpec = tween(200)
    )
    Box(
        modifier = modifier
            .clip(RoundedCornerShape(SpaceMediumLarge))
            .border(
                width = borderWidth.dp,
                color = animatedColor,
                shape = RoundedCornerShape(SpaceMediumLarge)
            )
            .clickable {
                onClick()
            },
        content = content
    )
}
private val cardHeight = 142.dp

private val imageWidth = 92.dp
private val imageHeight = 110.dp

private val nameFontSize = 24.sp
private val descriptionFontSize = 14.sp