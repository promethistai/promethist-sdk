package ai.promethist.android.presentation.components.dialog

import ai.promethist.android.ext.init
import ai.promethist.android.ext.stringResource
import ai.promethist.android.style.SpaceSmall
import ai.promethist.android.style.SpaceXSmall
import ai.promethist.android.style.modal
import ai.promethist.client.resources.Resources
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Checkbox
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment.Companion.CenterVertically
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.sp
import androidx.compose.ui.window.DialogProperties

@Composable
fun BackToMenuDialog(
    text: String,
    displayCheckbox: Boolean = true,
    backToMenuCallback: (Boolean) -> Unit,
    resumeCallback: (Boolean) -> Unit,
    feedbackCallback: () -> Unit,
) {
    val properties = DialogProperties(
        dismissOnBackPress = false,
        dismissOnClickOutside = false,
        usePlatformDefaultWidth = false
    )

    var isChecked by remember { mutableStateOf(false) }

    AlertDialog(
        modifier = Modifier
            .padding(SpaceSmall)
            .fillMaxWidth(0.92F)
            .wrapContentHeight(),
        properties = properties,
        onDismissRequest = {},
        confirmButton = {},
        text = {
            Column {
                Text(
                    text = text,
                    modifier = Modifier.padding(vertical = SpaceSmall),
                    fontSize = 18.sp
                )

                Row(
                    modifier = Modifier
                        .height(IntrinsicSize.Max)
                        .padding(vertical = SpaceSmall)
                ) {
                    BackToMenuButton(
                        modifier = Modifier.weight(1f),
                        title = stringResource(res = Resources.strings.persona.backToMenu)
                    ) {
                        backToMenuCallback(isChecked)
                    }
                    BackToMenuButton(
                        modifier = Modifier.weight(1f),
                        title = stringResource(res = Resources.strings.persona.resume)
                    ) {
                        resumeCallback(isChecked)
                    }
                }

                if (displayCheckbox) {
                    Row(
                        modifier = Modifier
                            .padding(top = SpaceSmall)
                            .fillMaxWidth(),
                        horizontalArrangement = Arrangement.Center
                    ) {
                        Checkbox(
                            checked = isChecked,
                            onCheckedChange = {
                                isChecked = it
                            }
                        )

                        Text(
                            text = stringResource(res = Resources.strings.persona.dontShowThisMessage),
                            modifier = Modifier.align(CenterVertically)
                        )
                    }
                }
            }
        }
    )
}

@Composable
private fun BackToMenuButton(
    modifier: Modifier = Modifier,
    title: String,
    onClick: () -> Unit
) {
    Button(
        modifier = modifier
            .padding(horizontal = SpaceXSmall)
            .fillMaxHeight(),
        colors = ButtonDefaults.buttonColors(
            containerColor = Color.init(res = Resources.colors.backToMenuButtons)
        ),
        onClick = onClick
    ) {
        Text(
            text = title,
            style = modal.copy(
                textAlign = TextAlign.Center,
                color = Color.White
            )
        )
    }
}