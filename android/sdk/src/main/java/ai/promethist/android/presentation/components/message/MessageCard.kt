package ai.promethist.android.presentation.components.message

import ai.promethist.SharedRes
import ai.promethist.android.presentation.components.MessageConfig
import ai.promethist.android.presentation.components.row.VoteRow
import ai.promethist.client.model.Message
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import coil.compose.AsyncImage

@Composable
fun MessageCard(
    messageConfig: MessageConfig,
    message: Message,
    onClick: (String, String?, Int) -> Unit
) {
    Box(
        modifier = Modifier
            .fillMaxWidth()
            .padding(vertical = 8.dp),
        contentAlignment = messageConfig.alignment
    ) {
        val showRatingUI = message.showRatingUI
        Box {
            Row(verticalAlignment = Alignment.CenterVertically) {
                if (!message.isUser) {
                    Box(
                        modifier = Modifier
                            .size(64.dp)
                            .padding(
                                6.dp
                            )
                    ) {
                        Image(
                            painter = painterResource(
                                id = SharedRes.images.anthony_portrait_l.drawableResId
                            ),
                            contentDescription = "Avatar",
                            contentScale = ContentScale.Crop,
                            modifier = Modifier
                                .fillMaxSize()
                                .clip(CircleShape)
                                .background(Color.White)
                        )
                    }
                }
            Card(
                modifier = Modifier
                    .wrapContentHeight()
                    .padding(bottom = if (showRatingUI) 24.dp else 0.dp),
                colors = CardDefaults.cardColors(containerColor = Color.Transparent)
            ) {
                Box(
                    Modifier
                        .background(
                            brush = Brush.verticalGradient(
                                listOf(
                                    messageConfig.backgroundColorPrimary,
                                    messageConfig.backgroundColorSecondary,
                                )
                            )
                        )
                        .padding(top = 10.dp, bottom = 12.dp, end = 14.dp, start = 14.dp)
                ) {

                    if (message.image != null) {
                        AsyncImage(
                            model = message.image,
                            contentDescription = "Image"
                        )
                    } else {
                        TextMessage(
                            message = message,
                            textColor = messageConfig.textColor
                        )
                    }
                }
            }
            }
            if (showRatingUI) {
                VoteRow(modifier = Modifier.align(Alignment.BottomEnd), message, onClick)
            }
        }
    }
}