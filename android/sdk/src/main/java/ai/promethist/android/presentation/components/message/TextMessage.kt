package ai.promethist.android.presentation.components.message

import ai.promethist.client.model.Message
import androidx.compose.foundation.layout.*
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.sp

@Composable
fun TextMessage(
    message: Message,
    textColor: Color
) {
    val name = if (message.isUser) "You" else message.name
    Row(
        verticalAlignment = Alignment.Top
    ) {
        Column(
            modifier = Modifier
        ) {
            name?.let {
                Text(
                    text = "$it:",
                    fontWeight = FontWeight.Bold,
                    color = textColor,
                    style = MaterialTheme.typography.titleMedium,
                    fontSize = 16.sp
                )
            }
            Text(
                text = message.text ?: "",
                color = textColor,
                style = MaterialTheme.typography.bodyMedium,
                fontSize = 20.sp
            )
//            Text(
//                text = message.time,
//                color = textColor,
//                style = MaterialTheme.typography.bodyMedium,
//                fontSize = 14.sp
//            )
        }
    }
}