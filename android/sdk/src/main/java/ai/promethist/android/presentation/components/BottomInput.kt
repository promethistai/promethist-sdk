package ai.promethist.android.presentation.components

import ai.promethist.android.style.SpaceMedium
import ai.promethist.android.style.SpaceSmall
import ai.promethist.android.style.SpaceXSmall
import ai.promethist.android.presentation.components.input.UserTextField
import ai.promethist.android.presentation.components.input.UserTextInput
import ai.promethist.android.presentation.components.menu.MenuColumn
import ai.promethist.android.presentation.components.picker.YearPicker
import ai.promethist.android.presentation.components.row.SetPersonaRow
import ai.promethist.android.presentation.components.text.TitleText
import ai.promethist.android.ext.fillWidthOfParent
import ai.promethist.android.ext.init
import ai.promethist.android.ext.painterResource
import ai.promethist.android.ext.regularMaterial
import ai.promethist.android.style.heading1
import ai.promethist.channel.command.RequestText
import ai.promethist.channel.item.ActionsItem
import ai.promethist.channel.item.CameraItem
import ai.promethist.channel.item.CorrectInputItem
import ai.promethist.channel.item.GrowthContentItem
import ai.promethist.channel.item.InputItem
import ai.promethist.channel.item.Modal
import ai.promethist.channel.item.PersonaItem
import ai.promethist.channel.item.QuestionnaireItem
import ai.promethist.channel.item.RequestRecordingItem
import ai.promethist.channel.item.RequestTextItem
import ai.promethist.channel.item.SessionEndItem
import ai.promethist.channel.item.SetPersonaItem
import ai.promethist.channel.item.TextInputItem
import ai.promethist.channel.item.VoiceInputItem
import ai.promethist.channel.model.PersonaType
import ai.promethist.client.resources.Resources
import android.graphics.Bitmap
import android.util.Base64
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.animateContentSize
import androidx.compose.animation.core.tween
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxScope
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.io.ByteArrayOutputStream

@Composable
fun BottomInput(
    modifier: Modifier,
    modal: Modal?,
    textMode: Boolean = false,
    enableInput: Boolean = false,
    isHomeButtonEnabled: Boolean = true,
    roundedBottom: Boolean = false,
    onPersonaSelected: (PersonaItem, PersonaType) -> Unit,
    onUserInput: (String, String) -> Unit,
    onCameraGranted: (Boolean) -> Unit,
    onImagePicked: (String?) -> Unit,
    onCameraActivated: () -> Unit,
    onUndoClick: () -> Unit,
    onHomeClick: (Boolean) -> Unit,
    onSkipClick: () -> Unit
) {
    val imageHeight = 800
    val imageWidth = 600
    val isEmpty = (modal == null || modal is VoiceInputItem) && !textMode
    val modifiers = when(modal) {
        is GrowthContentItem -> growthContentModifiers
        is InputItem -> {
            if (modal.input.birthYear)
                birthYearModifiers
            else
                otherModifiers
        }
        else -> otherModifiers
    }

    ControlsCard(
        modifier = modifier,
        isEmpty = isEmpty,
        modifiers = modifiers,
        isHomeButtonEnabled = isHomeButtonEnabled,
        roundedBottom = roundedBottom,
        onUndoClick = onUndoClick,
        onHomeClick = onHomeClick,
        onSkipClick = onSkipClick
    ) {
        AnimatedVisibility(
            visible = modal != null || textMode,
            enter = fadeIn(tween(600)),
            exit = fadeOut(tween(700))
        ) {
            when(modal) {
                null, is VoiceInputItem, is TextInputItem -> {
                    if (textMode) {
                        UserTextInput(
                            modifier = Modifier,
                            enabled = enableInput
                        ) {
                            onUserInput(it, it)
                        }
                    }
                }
                is ActionsItem -> {
                    TileList(actions = modal.content) { text, label -> onUserInput(
                        text,
                        label
                    ) }
                }
                is QuestionnaireItem -> {
                    val content = modal.content
                    OptionMatrix(
                        matrixContent = content.createMatrixContent(),
                        title = content.title ?: "",
                        labels = content.subLabels,
                        buttonConfig = ButtonConfig(
                            Modifier.aspectRatio(1F),
                            RoundedCornerShape(8.dp)
                        )
                    ) { text, label ->
                        onUserInput(text, label)
                    }
                }
                is InputItem -> {
                    val content = modal.input
                    Column {
                        TitleText(
                            modifier = Modifier
                                .fillWidthOfParent(SpaceMedium),
                            title = content.title,
                            text = content.text,
                            textStyle = heading1
                        )

                        OptionMatrix(
                            matrixContent = content.createMatrixContent()
                        ) { text, label ->
                            onUserInput(text, label)
                        }

                        Spacer(modifier = Modifier.height(SpaceMedium))

                        if (content.birthYear) {
                            YearPicker(modifier = Modifier
                                .padding(start = 16.dp, end = 16.dp, bottom = 16.dp)
                            ) {
                                val pickedYear = it.toString()
                                onUserInput(pickedYear, pickedYear)
                            }
                        }
                        if (modal.textInput) {
                            UserTextInput(
                                enabled = true
                            ) {
                                onUserInput(it, it)
                            }
                        }
                    }
                }
                is SessionEndItem -> {
                    SessionEndMenu(
                        hasRating = modal.content.hasRating,
                        ratingTitle = modal.content.ratingTitle,
                        onRatingSubmitted = {},
                        onButtonClicked =  {
                            onHomeClick(true)
                        }
                    )
                }
                is GrowthContentItem -> {
                    Box(
                        modifier = Modifier
                            .fillMaxWidth()
                            .fillMaxHeight(fraction = 0.78f)
                    ) {
                        MenuColumn(
                            modal.content
                        ) { action ->
                            onUserInput("#$action", "")
                        }
                    }
                }
                is RequestRecordingItem -> {
                    VoiceRecorder {
                        // onRecording(it)
                    }
                }
                is RequestTextItem -> {
                    UserTextField(payload = modal.content) {
                        val shortened = if (it.length > 30) it.substring(0, 30) + "..." else it
                        onUserInput(it, shortened)
                    }
                }
                is SetPersonaItem -> {
                    val content = modal.content
                    Column(
                        modifier = Modifier
                            .fillWidthOfParent(24.dp),
                    ) {
                        content.title?.let {
                            if (it.isNotEmpty())
                                TitleText(
                                    modifier = Modifier
                                        .weight(weight = 1f, fill = false)
                                        .padding(horizontal = SpaceSmall),
                                    title = it,
                                    textAlign = TextAlign.Center,
                                    textStyle = heading1
                                )
                        }

                        SetPersonaRow(
                            payload = content,
                            onPageClick = { personaItem, personaType ->
                                onPersonaSelected(personaItem, personaType)
                            }
                        )
                    }
                }
                is CorrectInputItem -> {
                    UserTextField(
                        payload = RequestText(
                            text = modal.correctInput.input
                        ),
                        minLines = 3,
                        maxLines = 3,
                        onSubmit = {
                        onUserInput(it, it)
                    })
                }
                is CameraItem -> {
                    CameraControls(
                        onImage = { bitmap ->
                            if (bitmap == null)
                                onImagePicked(null)
                            bitmap?.let {
                                val scaled = Bitmap.createScaledBitmap(it, imageWidth, imageHeight, true)
                                val stream = ByteArrayOutputStream()
                                scaled.compress(Bitmap.CompressFormat.JPEG, 100, stream)
                                val byteArray: ByteArray = stream.toByteArray()
                                it.recycle()
                                val encoded = Base64.encodeToString(byteArray, Base64.DEFAULT)
                                CoroutineScope(Dispatchers.Main).launch {
                                    onImagePicked(encoded)
                                }
                            }
                        },
                        onUserInput = {
                            onCameraGranted(it)
                        },
                        onCameraActivated = onCameraActivated
                    )
                }
            }
        }
    }
}

@Composable
fun ControlsCard(
    modifier: Modifier,
    isEmpty: Boolean,
    modifiers: MultimodalModifiers,
    isHomeButtonEnabled: Boolean,
    roundedBottom: Boolean,
    onUndoClick: () -> Unit,
    onHomeClick: (Boolean) -> Unit,
    onSkipClick: () -> Unit,
    content: @Composable BoxScope.() -> Unit
) {
    val defaultBottomPadding = 44.dp
    val padding = if (isEmpty) 0 else 24
    val upperCornerSize = if (isEmpty) 120F else 44F
    val lowerCornerSize = if (isEmpty) 120F else (if (roundedBottom) 44F else 0F)
    val bottomPadding = if (isEmpty) defaultBottomPadding else 0.dp

    Card(
        modifier = modifier
            .wrapContentSize()
            .padding(bottom = bottomPadding),
        shape = RoundedCornerShape(
            topStart = upperCornerSize.dp,
            topEnd = upperCornerSize.dp,
            bottomStart = lowerCornerSize.dp,
            bottomEnd = lowerCornerSize.dp
        ),
        colors = CardDefaults.cardColors(
            containerColor = regularMaterial()
        )
    ) {
        // val columnModifier = if (isFullscreen) Modifier else Modifier.height(IntrinsicSize.Max)
        Column(
            modifier = modifiers
                .columnModifier
                .padding(bottom = defaultBottomPadding - bottomPadding)
                .animateContentSize(),
            verticalArrangement = Arrangement.Bottom,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            if (!isEmpty) {
                val boxModifier = if (modifiers.useWeight) Modifier.weight(1F) else Modifier
                Box(
                    modifier = boxModifier
                        .wrapContentSize()
                        .padding(padding.dp),
                    content = content
                )
            }
            BottomControls(
                isHomeButtonEnabled = isHomeButtonEnabled,
                onUndoClick = onUndoClick,
                onHomeClick = onHomeClick,
                onSkipClick = onSkipClick
            )
        }
    }
}

@Composable
private fun BottomControls(
    isHomeButtonEnabled: Boolean,
    onUndoClick: () -> Unit,
    onHomeClick: (Boolean) -> Unit,
    onSkipClick: () -> Unit
) {
    Row(
        modifier = Modifier.padding(horizontal = SpaceXSmall),
        horizontalArrangement = Arrangement.spacedBy(SpaceXSmall),
        verticalAlignment = Alignment.CenterVertically
    ) {
        ControlButton(painterResource(res = Resources.images.icArrowUturnBackward), "Undo", enabled = true, onClick = onUndoClick)
        ControlButton(painterResource(res = Resources.images.icHouse), "Home", padding = 10, enabled = isHomeButtonEnabled, onClick = { onHomeClick(false) })
        ControlButton(painterResource(res = Resources.images.icForwardEnd), "Skip", padding = 12,enabled = true, onClick = onSkipClick)

        if (false) {
            ControlButton(
                painterResource(res = Resources.images.icCopy),
                "Copy",
                enabled = true
            )
        }
    }
}

@Composable
private fun ControlButton(
    icon: Painter,
    type: String,
    enabled: Boolean = false,
    padding: Int = 13,
    onClick: () -> Unit = {}
) {
    val iconAlpha = if (enabled) 1f else 0.4f

    IconButton(
        modifier = Modifier
            .size(58.dp)
            .padding(horizontal = padding.dp),
        onClick = onClick,
        enabled = enabled
    ) {
        Icon(
            painter = icon,
            contentDescription = "$type button icon",
            tint = Color.init(Resources.colors.primaryLabel).copy(alpha = iconAlpha)
        )
    }
}

data class MultimodalModifiers(
    val columnModifier: Modifier,
    val useWeight: Boolean
)

private val growthContentModifiers = MultimodalModifiers(Modifier, false)
private val birthYearModifiers = MultimodalModifiers(Modifier, false)
private val otherModifiers = MultimodalModifiers(Modifier.height(IntrinsicSize.Max), true)