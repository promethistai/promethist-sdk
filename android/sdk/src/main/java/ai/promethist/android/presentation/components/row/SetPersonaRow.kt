package ai.promethist.android.presentation.components.row

import ai.promethist.android.presentation.components.OverlappingRow
import ai.promethist.android.style.heading2
import ai.promethist.android.style.heading3
import ai.promethist.channel.command.PersonaSelection
import ai.promethist.channel.item.PersonaItem
import ai.promethist.channel.model.PersonaType
import ai.promethist.channel.model.ResourceSize
import ai.promethist.client.model.PersonaId
import ai.promethist.client.utils.PersonaResourcesManager
import ai.promethist.client.utils.RemotePersonaResourceManager
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.horizontalScroll
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.rememberScrollState
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.AsyncImage

@Composable
fun SetPersonaRow(
    modifier: Modifier = Modifier,
    payload: PersonaSelection,
    onPageClick: (PersonaItem, PersonaType) -> Unit
) {
    val scrollState = rememberScrollState()
    val loaded = mutableListOf<Boolean>()
    var alpha by remember { mutableStateOf(0.0F) }

    Box(
        modifier = modifier.alpha(alpha),
    ) {
        val rowModifier = if (payload.personas.size == 1) Modifier.align(Alignment.Center) else Modifier
        OverlappingRow(
            modifier = rowModifier
                .horizontalScroll(scrollState)
        ) {
            payload.personas.forEachIndexed { index, personaItem ->
                loaded.add(false)
                PersonaItemView(
                    item = personaItem,
                    size = payload.size,
                    onClick = {
                        onPageClick(personaItem, payload.type)
                    },
                    onLoaded = {
                        loaded[index] = true
                        if (loaded.all { true }) {
                            alpha = 1.0F
                        }
                    }
                )
            }
        }
    }
}

@Composable
private fun PersonaItemView(
    item: PersonaItem,
    size: ResourceSize,
    onClick: () -> Unit,
    onLoaded: () -> Unit
) {
    val personaId = PersonaId.fromAvatarId(item.getAvatar())
    val imageUrl = RemotePersonaResourceManager.getPortrait(
        personaId = personaId,
        size = size
    )

    Box(
        modifier = Modifier
            .clickable(onClick = onClick)
            .fillMaxWidth()
    ) {
        var imageWidth by remember { mutableStateOf(0) }
        val imageModifier = Modifier
            .height(300.dp)
            .onGloballyPositioned {
                imageWidth = it.size.width
            }
        val contentDescription = "Portrait"
        Column(
            modifier = Modifier
                .wrapContentHeight()
                .fillMaxWidth(),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Text(
                text = item.name,
                modifier = Modifier,
                textAlign = TextAlign.Center,
                style = heading2.copy(fontSize = 20.sp)
            )
            if (imageUrl == null) {
                val image = PersonaResourcesManager.getBackground(personaId)
                Image(
                    painter = painterResource(id = image.drawableResId),
                    contentDescription = contentDescription,
                    modifier = imageModifier
                )
            } else {
                AsyncImage(
                    model = imageUrl,
                    contentDescription = contentDescription,
                    modifier = imageModifier,
                    onSuccess = {
                        onLoaded()
                    }
                )
            }
            item.description?.let {
                Text(
                    text = it,
                    maxLines = 30,
                    modifier = Modifier
                        .width(with(LocalDensity.current) { imageWidth.toDp() })
                        .padding(horizontal = 15.dp),
                    textAlign = TextAlign.Center,
                    style = heading3
                )
            }
        }
    }
}