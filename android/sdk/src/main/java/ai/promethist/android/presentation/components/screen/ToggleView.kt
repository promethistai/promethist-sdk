package ai.promethist.android.presentation.components.screen

import ai.promethist.android.ext.init
import ai.promethist.client.model.InteractionMode
import ai.promethist.client.resources.Resources
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.core.FiniteAnimationSpec
import androidx.compose.animation.core.spring
import androidx.compose.animation.slideInHorizontally
import androidx.compose.animation.slideOutHorizontally
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.IntOffset
import androidx.compose.ui.unit.dp

@Composable
fun ToggleView(
    selection: InteractionMode,
    onChange: (InteractionMode) -> Unit
) {
    Row(
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.spacedBy(0.dp),
        modifier = Modifier
            .border(
                width = 1.17.dp,
                shape = RoundedCornerShape(itemCornerRadius),
                color = Color.init(res = Resources.colors.personaGlassyBackground)
            )
            .background(Color(0x33F5F5F7), shape = RoundedCornerShape(itemCornerRadius))
    ) {
        InteractionMode.values().forEach { type ->
            val iconRes = if (type == InteractionMode.Text) Resources.images.personaChat else Resources.images.personaPerson
            val isSelected = selection == type
            val color = if (isSelected) Color.init(res = Resources.colors.accent) else Color.White
            Box(
                modifier = Modifier
                    .size(width = itemWidth, height = itemHeight)
                    .clickable {
                        onChange(type)
                    },
                contentAlignment = Alignment.Center
            ) {
                SelectionIndicator(isSelected = isSelected, type == InteractionMode.Voice)
                Box(
                    modifier = Modifier
                        .size(itemIconSize),
                    contentAlignment = Alignment.Center
                ) {
                    Image(
                        painter = painterResource(id = iconRes.drawableResId),
                        contentDescription = null,
                        contentScale = ContentScale.Fit,
                        modifier = Modifier.fillMaxSize(),
                        colorFilter = ColorFilter.tint(color)
                    )
                }
            }
        }
    }
}
@Composable
private fun SelectionIndicator(isSelected: Boolean, isLeft: Boolean) {
    val direction = if (isLeft) 1 else -1
    val offset: (Int) -> Int = { direction * it / 2 }
    val animationSpec: FiniteAnimationSpec<IntOffset> = spring(
        dampingRatio = 0.62f,
        stiffness = 300f
    )
    AnimatedVisibility(
        visible = isSelected,
        enter = slideInHorizontally (
            initialOffsetX = offset,
            animationSpec = animationSpec
        ),
        exit = slideOutHorizontally (
            targetOffsetX = offset,
            animationSpec = animationSpec
        )
    ) {
        if (isSelected) {
            Box(
                modifier = Modifier
                    .size(width = itemWidth, height = itemHeight)
                    .clip(RoundedCornerShape(itemCornerRadius))
                    .background(Color.init(res = Resources.colors.personaSurface))
            )
        }
    }
}


private val itemHeight = 60.dp
private val itemWidth = 88.dp
private val itemIconSize = 30.dp
private val itemCornerRadius = itemHeight / 2