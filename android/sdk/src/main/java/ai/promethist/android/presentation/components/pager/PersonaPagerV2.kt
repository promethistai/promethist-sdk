package ai.promethist.android.presentation.components.pager

import ai.promethist.android.presentation.components.BoxCardBackground
import ai.promethist.android.style.SpaceMedium
import ai.promethist.android.style.SpaceSmall
import ai.promethist.android.style.SpaceXLarge
import ai.promethist.android.style.SpaceXSmall
import ai.promethist.channel.model.ResourceSize
import ai.promethist.client.Client
import ai.promethist.client.model.ApplicationInfo
import ai.promethist.client.model.PersonaId
import ai.promethist.client.utils.RemotePersonaResourceManager
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.pager.HorizontalPager
import androidx.compose.foundation.pager.rememberPagerState
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.snapshotFlow
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.AsyncImage
import kotlinx.coroutines.flow.distinctUntilChanged

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun PersonaPagerV2(
    personas: List<ApplicationInfo.Persona>,
    onPageSelected: (String, String, Int) -> Unit
) {

    val initialPage = personas.indexOfFirst { it.avatarRef == Client.appConfig().lastSelectedAvatarId }
    val state = rememberPagerState(initialPage = Integer.max(initialPage, 0)) {
        personas.size
    }
    LaunchedEffect(state) {
        snapshotFlow { state.currentPage }.distinctUntilChanged().collect { page ->
            val persona = personas.getOrNull(page)
            persona?.let {
                onPageSelected(it.ref, it.avatarRef,  page)
            }
        }
    }

    HorizontalPager(
        state = state,
        contentPadding = PaddingValues(horizontal = SpaceXLarge),
        pageSpacing = SpaceXSmall
    ) {
        val item = personas[it]
        val personaId = PersonaId.fromAvatarId(item.avatarRef)

        val imageUrl = RemotePersonaResourceManager.getPortrait(
            personaId = personaId,
            size = ResourceSize.L
        )
        BoxCardBackground(
            isActive = state.currentPage == it,
            modifier = Modifier
                .padding(horizontal = SpaceMedium, vertical = SpaceSmall)
                .width(cardWidth)
                .height(cardHeight),
            onClick = {  }
        ) {
            Column(
                modifier  = Modifier.fillMaxSize(),
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                AsyncImage(
                    model = imageUrl,
                    contentDescription = null,
                    modifier = Modifier
                        .width(imageWidth)
                        .height(imageHeight)
                )
                Text(
                    text = item.name,
                    fontSize = nameFontSize,
                    modifier = Modifier.padding(vertical = SpaceSmall)
                )
                Text(
                    text = item.description,
                    fontSize = descriptionFontSize,
                    modifier = Modifier.padding(bottom = SpaceSmall)
                )
            }
        }
    }
}
private val cardWidth = 360.dp
private val cardHeight = 302.dp

private val imageWidth = 188.dp
private val imageHeight = 215.dp

private val nameFontSize = 24.sp
private val descriptionFontSize = 14.sp
