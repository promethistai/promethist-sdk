package ai.promethist.android

import ai.promethist.android.presentation.PersonaViewModel
import ai.promethist.android.presentation.components.OnboardingView
import ai.promethist.android.presentation.components.SetupNameEntryView
import ai.promethist.android.presentation.components.auth.AuthView
import ai.promethist.android.presentation.components.dialog.PermissionDialog
import ai.promethist.android.presentation.components.screen.PersonaSelectMain
import ai.promethist.android.presentation.components.screen.PersonaViewV2
import ai.promethist.android.presentation.components.screen.SplashView
import ai.promethist.android.presentation.components.video.IntroVideoContainer
import ai.promethist.android.presentation.unity.UnityView
import ai.promethist.android.utils.AppEvent
import ai.promethist.android.utils.AppEventBus
import ai.promethist.client.Client
import ai.promethist.client.ClientEvent
import ai.promethist.client.ClientModel
import ai.promethist.client.model.MicPermission
import ai.promethist.video.VideoMode
import android.app.Activity
import androidx.compose.animation.AnimatedContent
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.imePadding
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.lifecycle.viewmodel.compose.viewModel as composeViewModel
import kotlinx.coroutines.launch

@Composable
fun PromethistView(
    promethist: Promethist,
    promethistViewModel: PromethistViewModel
) {
    val viewModel: ClientModel = promethist.viewModel
    val activity = LocalContext.current as Activity

    val authClient = Client.authClient()
    val coroutineScope = rememberCoroutineScope()
    val personaViewModel: PersonaViewModel = composeViewModel()
    var downloadProgress by remember { mutableStateOf<Float?>(null) }
    var splashVisible by remember { mutableStateOf(true) }
    var permissionAlertVisible by remember { mutableStateOf(
        Client.appConfig().micPermission == MicPermission.Denied || Client.appConfig().micPermission == MicPermission.Unknown
    )
    }

    LaunchedEffect(Unit) {
        AppEventBus.subscribe(coroutineScope) {
            when(it) {
                is AppEvent.MicPermissionUpdated -> {
                    if (permissionAlertVisible && !it.denied){
                        permissionAlertVisible = false
                        promethistViewModel.updateStartPoint()
                    }
                }
                is AppEvent.OnSplashScreenUpdate -> {
                    downloadProgress = it.downloadProgress
                    splashVisible = it.splashVisible ?: splashVisible
                }
                else -> Unit
            }
        }
    }
    Box(
        modifier = Modifier.fillMaxSize().imePadding()
    ) {
        UnityView(
            modifier = Modifier.fillMaxSize(),
            unityPlayer = promethist.unityPlayer
        )
        AnimatedContent(
            targetState = promethistViewModel.startPoint,
            label = AnimatedContentLabel
        ) { startPoint ->
            when (startPoint) {
                PromethistStartPoint.Permissions -> Unit
                PromethistStartPoint.Onboarding -> {
                    OnboardingView(
                        appConfig = promethistViewModel.appConfig,
                        onOnboardingFinished = { termsAccepted ->
                            if (!termsAccepted) {
                                activity.finishAffinity()
                                return@OnboardingView
                            }
                            promethistViewModel.updateStartPoint()
                        }
                    )
                }

                PromethistStartPoint.Main -> {
                    PersonaViewV2(
                        viewModel = viewModel,
                        promethistViewModel = promethistViewModel,
                        personaViewModel = personaViewModel,
                        onLogout = {
                            promethistViewModel.updateStartPoint()
                        }
                    ) {
                        promethistViewModel.startPoint = PromethistStartPoint.PersonaSelect
                    }
                }

                PromethistStartPoint.AppKeyEntry -> {
                    splashVisible = false
                    SetupNameEntryView {
                        promethistViewModel.appKeyEntered()
                        if (Client.appConfig().videoMode != VideoMode.OnDeviceRendering)
                            viewModel.handleEvent(ClientEvent.Reset(true))
                    }
                }

                PromethistStartPoint.IntroVideo -> {
                    splashVisible = false

                    IntroVideoContainer {
                        promethistViewModel.appConfig.showIntroVideo = false
                        promethistViewModel.updateStartPoint()
                    }
                }

                PromethistStartPoint.AuthScreen -> {
                    splashVisible = false
                    AuthView(
                        shouldHandleCallback = {
                            return@AuthView authClient.shouldHandleCallback(it)
                        },
                        handleCallback = {
                            coroutineScope.launch {
                                personaViewModel.finishLogin(
                                    Client.authClient().handleCallback(it)
                                ) {
                                    promethistViewModel.updateStartPoint()
                                }
                            }
                        }
                    )
                }

                PromethistStartPoint.PersonaSelect -> {
                    PersonaSelectMain(
                        viewModel.channel,
                        onLoaded = {
                            splashVisible = false
                        },
                        onLogout = {
                            promethistViewModel.updateStartPoint()
                        }
                    ) { ref, avatarId ->
                        promethistViewModel.startPoint = PromethistStartPoint.Main
                        viewModel.start()
                        viewModel.handleEvent(ClientEvent.SetPersona(ref, avatarId, true))
                    }
                }
            }
            if (splashVisible) {
                SplashView(
                    progress = downloadProgress
                )
            }
            if (permissionAlertVisible) {
                PermissionDialog()
            }
        }
    }
}

private const val AnimatedContentLabel = ""