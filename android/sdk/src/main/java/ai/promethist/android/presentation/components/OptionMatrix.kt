package ai.promethist.android.presentation.components

import ai.promethist.android.ext.accentSecondary
import ai.promethist.android.style.SpaceMediumSmall
import ai.promethist.android.style.SpaceXSmall
import ai.promethist.android.presentation.components.text.TitleText
import ai.promethist.android.ext.init
import ai.promethist.android.style.modal
import ai.promethist.channel.command.MatrixContent
import ai.promethist.client.resources.Resources
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.unit.dp

// For #optionList (Nx1), #textInput (Nx1), #questionnaire(1xN) and matrix (MxN)
@Composable
fun OptionMatrix(
    matrixContent: MatrixContent,
    title: String = "",
    buttonConfig: ButtonConfig = ButtonConfig(Modifier, RoundedCornerShape(10.dp)),
    labels: List<String> = listOf(),
    onClick: (String, String) -> Unit
) {
    val columnAlignment: Alignment.Horizontal = Alignment.Start

    Column (
        modifier = Modifier
            .verticalScroll(rememberScrollState())
    ) {
        if (title.isNotEmpty()) {
            TitleText(modifier = Modifier
                .weight(weight = 1F, fill = false), title = title)
        }
        Column(
            modifier = Modifier.fillMaxWidth(),
            horizontalAlignment = columnAlignment,
            verticalArrangement = Arrangement.spacedBy(SpaceXSmall)
        ) {
            matrixContent.matrix.rows.forEach { row ->
                Row(
                    modifier = Modifier
                        .fillMaxWidth(),
                    horizontalArrangement = Arrangement.SpaceBetween,
                ) {
                    row.values.forEach {
                        Button(
                            onClick = { onClick(it.value, "") },
                            modifier = buttonConfig.modifier,
                            shape = buttonConfig.shape,
                            colors = ButtonDefaults.buttonColors(
                                containerColor = accentSecondary()
                            ),
                            contentPadding = PaddingValues(horizontal = SpaceMediumSmall)
                        ) {
                            Text(
                                text = it.label,
                                style = modal,
                                color = Color.init(res = Resources.colors.primaryLabel)
                            )
                        }
                    }
                }
            }
        }
        if (labels.size == 2) {
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 14.dp, vertical = 8.dp),
                horizontalArrangement = Arrangement.SpaceBetween
            ) {
                labels.forEach {
                    Text(
                        text = it,
                        style = MaterialTheme.typography.labelMedium,
                    )
                }
            }
        }
    }
}

data class ButtonConfig(
    val modifier: Modifier,
    val shape: Shape
)