package ai.promethist.android.presentation.components.row

import ai.promethist.android.ext.painterResource
import ai.promethist.android.presentation.ProfileActivity
import ai.promethist.android.presentation.SettingsActivity
import ai.promethist.android.presentation.components.button.CircularButton
import ai.promethist.android.presentation.components.screen.ToggleView
import ai.promethist.android.style.SpaceMediumLarge
import ai.promethist.android.style.SpaceSmall
import ai.promethist.client.Client
import ai.promethist.client.ClientDefaults
import ai.promethist.client.model.InteractionMode
import ai.promethist.client.resources.Resources
import ai.promethist.client.ClientEvent
import ai.promethist.client.auth.AuthPolicy
import android.content.Intent
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import kotlinx.coroutines.launch

@Composable
fun PersonaTopRowV2(
    modifier: Modifier,
    interactionMode: InteractionMode,
    debugButtonVisible: Boolean,
    handleEvent: (ClientEvent) -> Unit,
    onHomeClick: () -> Unit,
    onShouldLogin: () -> Unit,
    onDebugButtonClick: () -> Unit,
    onLogout: () -> Unit,
    onChange: (InteractionMode) -> Unit
) {
    val context = LocalContext.current
    val coroutineScope = rememberCoroutineScope()
    val startForResult = rememberLauncherForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
        handleEvent(ClientEvent.SyncSettings)
        when (result.resultCode) {
            ProfileActivity.LogInResultCode -> onShouldLogin()
            SettingsActivity.ShouldLogoutCode -> onLogout()
            SettingsActivity.ShouldResetResultCode -> {
                if (ClientDefaults.authPolicy == AuthPolicy.Required)
                    onLogout()
                else
                    onHomeClick()
            }
        }
    }
    var avatarUrl by remember { mutableStateOf<String?>(null) }



    LaunchedEffect(Client.authClient().isLoggedIn()) {
        coroutineScope.launch {
            avatarUrl = if (!Client.authClient().isLoggedIn())
                null
            else {
                val userInfo = Client.authClient().getUserInfo()
                userInfo["avatarUrl"]
            }
        }
    }
    Row(
        modifier
            .fillMaxWidth()
            .padding(horizontal = SpaceMediumLarge),
        horizontalArrangement = Arrangement.SpaceBetween
    ) {
        Column {
            CircularButton(
                Modifier,
                false,
                painterResource(res = Resources.images.personaHome)
            ) {
                onHomeClick()
            }
            if (debugButtonVisible) {
                CircularButton(
                    Modifier.padding(vertical = SpaceSmall),
                    false,
                    painterResource(res = Resources.images.icDebug)
                ) {
                    onDebugButtonClick()
                }
            }
        }
        ToggleView(selection = interactionMode) {
            handleEvent(
                if (it == InteractionMode.Voice)
                    ClientEvent.Resume
                else
                    ClientEvent.Pause
            )
            onChange(it)
        }
        CircularButton(
            Modifier,
            false,
            painterResource(res = Resources.images.personaSettings),
            avatarUrl = avatarUrl
        )  {
            val intent = Intent(context, SettingsActivity::class.java)
            intent.putExtra(ProfileActivity.USERNAME, "") // TODO new activity
            startForResult.launch(intent)
        }
    }
}