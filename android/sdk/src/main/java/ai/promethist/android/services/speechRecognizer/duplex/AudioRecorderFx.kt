package ai.promethist.android.services.speechRecognizer.duplex

import ai.promethist.android.services.speechRecognizer.duplex.DuplexAndroidSpeechRecognizer.Companion.TAG
import android.media.audiofx.AcousticEchoCanceler
import android.media.audiofx.AutomaticGainControl
import android.media.audiofx.NoiseSuppressor
import android.util.Log

/**
 * A utility class for managing audio effects during audio recording sessions.
 *
 * This class provides methods to enable or disable specific audio effects, including:
 * - Acoustic Echo Cancellation (AEC)
 * - Noise Suppression (NS)
 * - Automatic Gain Control (AGC)
 *
 * These effects help improve audio quality in different recording scenarios.
 */
class AudioRecorderFx {

    /**
     * Enables or disables the Acoustic Echo Canceler (AEC) for the specified audio session.
     *
     * Acoustic Echo Canceler reduces echo during audio recording, typically caused
     * by the speaker's output being picked up by the microphone.
     *
     * @param sessionId The audio session ID to which the effect should be applied.
     * @param isEnabled A boolean flag to enable (`true`) or disable (`false`) the effect. Default is `true`.
     */
    fun setAcousticEchoCancelerEnabled(sessionId: Int, isEnabled: Boolean = true) {
        try {
            if (AcousticEchoCanceler.isAvailable()) {
                val echoCanceler = AcousticEchoCanceler.create(sessionId)
                echoCanceler.enabled = isEnabled
                Log.i(TAG, "AcousticEchoCanceler enabled")
            } else {
                Log.e(TAG, "AcousticEchoCanceler not available")
            }
        } catch (e: Exception) {
            Log.e(TAG, "AcousticEchoCanceler not available", e)
        }
    }

    /**
     * Enables or disables the Noise Suppressor (NS) for the specified audio session.
     *
     * Noise Suppressor reduces background noise during audio recording, enhancing the clarity
     * of the recorded audio.
     *
     * IT IS NOT RECOMMENDED BY STT ENGINE TO USE THIS
     *
     * @param sessionId The audio session ID to which the effect should be applied.
     * @param isEnabled A boolean flag to enable (`true`) or disable (`false`) the effect. Default is `true`.
     */
    fun setNoiseSuppressionEnabled(sessionId: Int, isEnabled: Boolean = true) {
        try {
            if (NoiseSuppressor.isAvailable()) {
                val noiseSuppressor = NoiseSuppressor.create(sessionId)
                noiseSuppressor.enabled = isEnabled
                Log.i(TAG, "NoiseSuppressor enabled")
            } else {
                Log.e(TAG, "NoiseSuppressor is not available")
            }
        } catch (e: Exception) {
            Log.e(TAG, "NoiseSuppressor is not available", e)
        }
    }

    /**
     * Enables or disables the Automatic Gain Control (AGC) for the specified audio session.
     *
     * Automatic Gain Control adjusts the microphone gain automatically to maintain consistent
     * volume levels. Disabling it may be useful in cases like music recording or advanced audio processing.
     *
     * IT IS NOT RECOMMENDED BY STT ENGINE TO USE THIS
     *
     * @param sessionId The audio session ID to which the effect should be applied.
     * @param isEnabled A boolean flag to enable (`true`) or disable (`false`) the effect. Default is `true`.
     */
    fun setAutomaticGainControlEnabled(sessionId: Int, isEnabled: Boolean = true) {
        try {
            if (AutomaticGainControl.isAvailable()) {
                val automaticGainControl = AutomaticGainControl.create(sessionId)
                automaticGainControl.setEnabled(isEnabled)
                Log.i(TAG, "AutomaticGainControl disabled")
            } else {
                Log.e(TAG, "AutomaticGainControl is not available")
            }
        } catch (e: Exception) {
            Log.e(TAG, "AutomaticGainControl is not available", e)
        }
    }
}