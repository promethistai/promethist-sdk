package ai.promethist.android.services.speechRecognizer.duplex

/**
 * A utility class for converting audio sample rates.
 *
 * This class provides a method for downsampling audio data from a higher sample rate
 * to a target lower sample rate.
 */
class SampleRateConverter {

    /**
     * Downsamples the given audio data to a target sample rate.
     *
     * This method takes in audio data in a specified sample rate and converts it to a lower sample rate
     * by retaining every `n`-th sample, where `n` is the ratio of the input sample rate to the output sample rate.
     *
     * @param input The input audio data as a `ByteArray` (e.g., PCM data).
     * @param inputSampleRate The sample rate of the input audio data (e.g., 44100 Hz).
     * @param outputSampleRate The target sample rate for the output audio data (e.g., 16000 Hz).
     * @return A `ByteArray` containing the downsampled audio data.
     */
    fun downSampleTo16kHz(input: ByteArray, inputSampleRate: Int, outputSampleRate: Int): ByteArray {
        val ratio = inputSampleRate / outputSampleRate
        val output = ByteArray(input.size / ratio)
        for (i in output.indices) {
            output[i] = input[i * ratio]
        }
        return output
    }
}