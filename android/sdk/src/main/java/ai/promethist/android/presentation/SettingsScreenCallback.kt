package ai.promethist.android.presentation

import ai.promethist.client.model.Appearance

interface SettingsScreenCallback {
    fun openBrowser(url: String)
    fun setAppearance(newAppearance: Appearance)
    fun indicateShouldReset()
}
