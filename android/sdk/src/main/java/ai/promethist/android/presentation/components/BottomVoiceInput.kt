package ai.promethist.android.presentation.components

import ai.promethist.android.ext.painterResource
import ai.promethist.android.presentation.components.button.AudioButton
import ai.promethist.android.presentation.components.button.CircularButton
import ai.promethist.android.style.SpaceMediumSmall
import ai.promethist.android.style.SpaceSmall
import ai.promethist.client.resources.Resources
import ai.promethist.client.ClientEvent
import ai.promethist.client.ClientModel
import ai.promethist.client.ClientUiState
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier

@Composable
fun BottomVoiceInput(
    modifier: Modifier,
    clientUiState: ClientUiState,
    viewModel: ClientModel
) {
    Row(
        modifier = modifier
            .fillMaxWidth()
            .padding(horizontal = SpaceMediumSmall),
        verticalAlignment = Alignment.Bottom
    ) {
        val painterRes = if (clientUiState.isPaused) Resources.images.personaPlay else Resources.images.personaPause
        CircularButton(
            Modifier.padding(SpaceSmall),
            clientUiState.isPaused,
            painterResource(res = painterRes)
        ) {
            viewModel.handleEvent(
                if (clientUiState.isPaused)
                    ClientEvent.Resume
                else
                    ClientEvent.Pause
            )
        }
        AudioButton(
            modifier = Modifier
                .padding(SpaceSmall)
                .weight(1F),
            audioIndicatorState = clientUiState.audioIndicatorState
        )
        Column(
            modifier = Modifier
        ) {
//            CircularButton(
//                Modifier.padding(SpaceSmall),
//                false,
//                painterResource(res = Resources.images.personaHand)
//            ) {
//
//            }
            CircularButton(
                Modifier.padding(SpaceSmall),
                false,
                painterResource(res = Resources.images.personaHand)
            ) {
                viewModel.handleEvent(ClientEvent.BargeIn)
            }
        }
    }
}