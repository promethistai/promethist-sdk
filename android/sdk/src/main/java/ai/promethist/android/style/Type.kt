package ai.promethist.android.style

import ai.promethist.client.resources.Resources
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.sp

val AktivGrotesk = FontFamily(
    Font(Resources.fonts.primaryFont.regular.fontResourceId, FontWeight.Normal),
    Font(Resources.fonts.primaryFont.light.fontResourceId, FontWeight.Light),
    Font(Resources.fonts.primaryFont.thin.fontResourceId, FontWeight.Thin),
    Font(Resources.fonts.primaryFont.medium.fontResourceId, FontWeight.Medium),
    Font(Resources.fonts.primaryFont.bold.fontResourceId, FontWeight.Bold),
)

val heading1 = TextStyle(
    fontWeight = FontWeight.Bold,
    fontSize = 30.sp,
    fontFamily = AktivGrotesk
)

val heading2 = TextStyle(
    fontWeight = FontWeight.Bold,
    fontSize = 20.sp,
    fontFamily = AktivGrotesk
)

val heading3 = TextStyle(
    fontWeight = FontWeight.Medium,
    fontSize = 15.sp,
    fontFamily = AktivGrotesk
)

val modal = TextStyle(
    fontWeight = FontWeight.Medium,
    fontSize = 16.5.sp,
    fontFamily = AktivGrotesk
)

val regular = TextStyle(
    fontWeight = FontWeight.Normal,
    fontSize = 17.5.sp,
    fontFamily = AktivGrotesk
)

val minimal = TextStyle(
    fontWeight = FontWeight.Medium,
    fontSize = 12.5.sp,
    fontFamily = AktivGrotesk
)