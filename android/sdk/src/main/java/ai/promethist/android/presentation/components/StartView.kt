package ai.promethist.android.presentation.components

import ai.promethist.SharedRes
import ai.promethist.android.presentation.components.dialog.TermsAlertDialog
import ai.promethist.android.presentation.components.dialog.TermsParagraph
import ai.promethist.android.presentation.components.text.URLContainer
import ai.promethist.client.Client
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.platform.LocalContext

@Composable
fun StartView(
    successCallback: () -> Unit,
    finishCallback: () -> Unit,
) {
    val context = LocalContext.current
    val config = Client.appConfig()
    var termsShown by remember { mutableStateOf(!config.termsAndConditionsAccepted) }
    var consentShown by remember { mutableStateOf(!config.dataProtectionAccepted && !termsShown) }
    if (termsShown) {
        val termsURL = SharedRes.strings.onboarding_terms_and_conditions_url.getString(context)
        val privacyPolicyURL = SharedRes.strings.onboarding_data_protection_url.getString(context)
        val termsLabel = SharedRes.strings.onboarding_terms_and_conditions_name.getString(context)
        val privacyPolicyLabel = SharedRes.strings.onboarding_data_protection_name.getString(context)
        val text = SharedRes.strings.onboarding_terms_and_conditions_text.getString(context)
            .replace(Regex(" \\([a-z.:/-]*\\)"), "")
            .split("**")
        TermsAlertDialog(
            confirmButtonTitle = SharedRes.strings.onboarding_accept.getString(context),
            dismissButtonTitle = SharedRes.strings.onboarding_decline.getString(context),
            paragraphs = listOf(
                TermsParagraph(
                    SharedRes.strings.onboarding_terms_and_conditions_title.getString(context),
                    text[0],
                    listOf(
                        URLContainer(termsLabel, termsURL),
                        URLContainer(privacyPolicyLabel, privacyPolicyURL)
                    )
                ),
                TermsParagraph(
                    text[1],
                    text[2],
                    listOf(
                        URLContainer(privacyPolicyLabel, privacyPolicyURL)
                    )
                )
            ),
            confirmAction = {
                termsShown = false
                consentShown = true
                config.termsAndConditionsAccepted = true
            }) {
            finishCallback()
        }
    }
    if (consentShown) {
        TermsAlertDialog(
            confirmButtonTitle = SharedRes.strings.onboarding_consent.getString(context),
            dismissButtonTitle = SharedRes.strings.onboarding_dissent.getString(context),
            paragraphs = listOf(
                TermsParagraph(
                    SharedRes.strings.onboarding_data_protection_title.getString(context),
                    SharedRes.strings.onboarding_data_protection_text.getString(context),
                    listOf()
                )
            ),
            confirmAction = {
                consentShown = false
                config.dataProtectionAccepted = true
                successCallback()
            }
        ) {
            finishCallback()
        }
    }
}