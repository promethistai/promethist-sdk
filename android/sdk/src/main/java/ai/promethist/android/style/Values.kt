package ai.promethist.android.style

import androidx.compose.ui.unit.dp

/**
 * Radius of 3 dp
 */
val RadiusSmall = 3.dp

/**
 * Radius of 6 dp
 */
val RadiusMedium = 6.dp

/**
 * Radius of 10 dp
 */
val RadiusLarge = 10.dp

/**
 * Radius of 18 dp
 */
val RadiusXLarge = 18.dp

/**
 * Radius of 30 dp
 */
val RadiusXXLarge = 30.dp


/**
 * Spacing of 2 dp
 */
val SpaceXXSmall = 2.dp

/**
 * Spacing of 4 dp
 */
val SpaceXSmall = 4.dp

/**
 * Spacing of 8 dp
 */
val SpaceSmall = 8.dp

/**
 * Spacing of 12 dp
 */
val SpaceMediumSmall = 12.dp

/**
 * Spacing of 16 dp
 */
val SpaceMedium = 16.dp

/**
 * Spacing of 20 dp
 */
val SpaceMediumLarge = 20.dp

/**
 * Spacing of 24 dp
 */
val SpaceLarge = 24.dp

/**
 * Spacing of 32 dp
 */
val SpaceXLarge = 32.dp

/**
 * Spacing of 44 dp
 */
val SpaceXXLarge = 44.dp

/**
 * Spacing of 64 dp
 */
val SpaceXXXLarge = 64.dp