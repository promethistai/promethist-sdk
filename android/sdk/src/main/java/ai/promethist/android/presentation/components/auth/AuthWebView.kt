package ai.promethist.android.presentation.components.auth

import android.annotation.SuppressLint
import android.webkit.WebResourceRequest
import android.webkit.WebViewClient
import androidx.compose.foundation.layout.Box
import androidx.compose.runtime.*
import androidx.compose.ui.viewinterop.AndroidView
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import android.webkit.WebView
import android.net.Uri
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.ui.Alignment
import io.ktor.http.Url

@SuppressLint("SetJavaScriptEnabled")
@Composable
fun AuthWebView(
    url: String,
    shouldHandleCallback: (Url) -> Boolean,
    handleCallback: (Url) -> Unit
) {
    var isLoading by remember { mutableStateOf(true) }

    Box(
        modifier = Modifier.fillMaxSize(),
        contentAlignment = Alignment.Center
    ) {
        AndroidView(
            modifier = Modifier.fillMaxSize(),
            factory = { context ->
                WebView(context).apply {
                    webViewClient = object : WebViewClient() {

                        override fun shouldOverrideUrlLoading(
                            view: WebView?,
                            request: WebResourceRequest?
                        ): Boolean {
                            request?.url?.let { currentUri ->
                                val currentUrl = Url(currentUri.toString())
                                if (shouldHandleCallback(currentUrl)) {
                                    handleCallback(currentUrl)
                                    return true
                                } else {
                                   return false
                                }

                            }
                            return super.shouldOverrideUrlLoading(view, request)
                        }

                        override fun onPageFinished(view: WebView?, url: String?) {
                            isLoading = false
                        }
                    }
                    clearCache(true)
                    clearFormData()
                    clearHistory()
                    clearSslPreferences()
                    settings.userAgentString = System.getProperty("http.agent")
                    settings.javaScriptEnabled = true
                    loadUrl(url)
                }
            }
        )

        if (isLoading) {
            CircularProgressIndicator()
        }
    }
}

private fun handleUrl(uri: Uri, codeParamKey: String, onResult: (String) -> Unit) {
    val queryParams = uri.queryParameterNames
    if (queryParams.contains(codeParamKey)) {
        val code = uri.getQueryParameter(codeParamKey)
        if (code != null) {
            onResult(code)
        }
    }
}