package ai.promethist.android.presentation.components.screen

import ai.promethist.SharedRes
import ai.promethist.android.ext.openPermissionSettings
import ai.promethist.android.ext.stringResource
import ai.promethist.android.presentation.ProfileActivity
import ai.promethist.android.presentation.SettingsActivity
import ai.promethist.android.presentation.SettingsActivity.Companion.dataStore
import ai.promethist.android.presentation.SettingsScreenCallback
import ai.promethist.android.presentation.components.AppToolbar
import ai.promethist.android.style.SpaceSmall
import ai.promethist.android.style.SpaceXSmall
import ai.promethist.android.utils.AppEvent
import ai.promethist.android.utils.AppEventBus
import ai.promethist.android.utils.WebCacheCleaner
import ai.promethist.client.model.Appearance
import ai.promethist.client.Client
import ai.promethist.client.ClientDefaults
import ai.promethist.client.model.InteractionMode
import ai.promethist.client.avatar.AvatarQualityLevel
import ai.promethist.client.resources.Resources
import ai.promethist.client.speech.DuplexMode
import ai.promethist.runtime
import ai.promethist.util.Locale
import ai.promethist.video.VideoMode
import android.content.Context
import android.widget.Toast
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.combinedClickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import coil.compose.AsyncImage
import com.jamal.composeprefs3.ui.PrefsScreen
import com.jamal.composeprefs3.ui.prefs.EditTextPref
import com.jamal.composeprefs3.ui.prefs.ListPref
import com.jamal.composeprefs3.ui.prefs.SwitchPref
import com.jamal.composeprefs3.ui.prefs.TextPref
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

private inline fun <reified T: Enum<T>> enumSettingEntries(): Map<String, String> {
    return enumValues<T>().associate { t ->
        t.toString() to t.toString().split(regex = Regex("(?=\\p{Upper})"))
            .joinToString(" ")
            .removePrefix(" ")
    }
}
private fun showToast(context: Context, text: String, duration: Int = Toast.LENGTH_LONG) {
    Toast.makeText(context, text, duration).show()
}
@OptIn(ExperimentalFoundationApi::class, ExperimentalComposeUiApi::class)
@Composable
fun SettingsViewV2(
    callback: SettingsScreenCallback,
    onClose: (Int) -> Unit,
    onBackClick: () -> Unit,
) {
    val context = LocalContext.current
    val coroutineScope = rememberCoroutineScope()
    val config = Client.appConfig()
    val authClient = Client.authClient()
    val userName = authClient.getStoredUserInfo()["name"]
    val avatarUrl = authClient.getStoredUserInfo()["avatarUrl"]

    var preset by remember { mutableStateOf(config.preset) }
    var interactionMode by remember { mutableStateOf(config.interactionMode) }
    var videoMode by remember { mutableStateOf(config.videoMode) }
    var devSettings by remember { mutableStateOf(config.debugMode) }
    var duplexMode by remember { mutableStateOf(config.duplexMode) }

    val versionName = runtime.version
    val versionCode = runtime.buildNumber
    Box(
        modifier = Modifier.background(MaterialTheme.colorScheme.background)
    ) {
        AppToolbar(
            modifier = Modifier.align(Alignment.TopCenter),
            title = stringResource(res = Resources.strings.settings.title),
            onBackClick = onBackClick
        )

        PrefsScreen(
            dataStore = LocalContext.current.dataStore,
            modifier = Modifier
                .fillMaxSize()
                .padding(horizontal = SpaceSmall)
                .padding(top = 38.dp)
        ) {
            
            prefsGroup({
                Text(
                    text = stringResource(res = Resources.strings.profile.title),
                    color = MaterialTheme.colorScheme.onBackground
                )
            }) {
                //prefsGroup({ Text(text = "") }) {

                prefsItem {
                    TextPref(
                        title = userName ?: "Not logged in",
                        leadingIcon = if (avatarUrl != null) {
                            {
                                AsyncImage(
                                    modifier = Modifier
                                        .padding(bottom = SpaceXSmall, end = SpaceSmall, top = SpaceXSmall)
                                        .size(avatarSize.dp)
                                        .clip(RoundedCornerShape((avatarSize / 2).dp))
                                        .border(width = 1.dp, color = Color.LightGray, shape = RoundedCornerShape((avatarSize / 2).dp)),
                                    contentScale = ContentScale.Crop,
                                    model = avatarUrl,
                                    contentDescription = null
                                )
                            }
                        } else null,
                        enabled = true,
                        onClick = {
                            // TODO edit
                        },
                        trailingContent = {

                        }

                    )
                }
                    prefsItem {
                        TextPref(
                            title = stringResource(
                                res = if (authClient.isLoggedIn())
                                    Resources.strings.auth.logout
                                else
                                    Resources.strings.auth.login
                            ),
                            textColor = MaterialTheme.colorScheme.primary,
                            enabled = true,
                            onClick = {
                                if (authClient.isLoggedIn()) {
                                    coroutineScope.launch {
                                        authClient.logout()
                                        WebCacheCleaner.clean()
                                        GlobalScope.launch {
                                            delay(1000)
                                            AppEventBus.emit(AppEvent.OnAuthSessionUpdated)
                                        }
                                        onClose(SettingsActivity.ShouldLogoutCode)
                                    }
                                } else {
                                    onClose(ProfileActivity.LogInResultCode)
                                }
                            }
                        )
                    }
                // }

            }

            prefsGroup({
                Text(
                    text = stringResource(res = Resources.strings.settings.about),
                    color = MaterialTheme.colorScheme.onBackground
                )
            }) {
                prefsItem {
                    val alreadyActivatedText = stringResource(res = Resources.strings.settings.debugAlreadyActivated)
                    val activatedText = stringResource(res = Resources.strings.settings.debugActivated)
                    TextPref(
                        title = stringResource(res = Resources.strings.settings.buildNumber),
                        summary = versionCode,
                        modifier = Modifier.combinedClickable(
                            interactionSource = MutableInteractionSource(),
                            indication = null,
                            onClick = {},
                            onLongClick = {
                                if (config.debugMode) {
                                    showToast(context, alreadyActivatedText)
                                } else {
                                    showToast(context, activatedText)
                                    config.debugMode = true
                                    devSettings = true
                                }
                            }
                        )
                    )
                }
                prefsItem {
                    TextPref(
                        title = stringResource(res = Resources.strings.settings.privacyPolicy),
                        textColor = MaterialTheme.colorScheme.primary,
                        enabled = true,
                        onClick = { callback.openBrowser(SharedRes.strings.onboarding_terms_and_conditions_url.getString(context)) }
                    )
                }
                prefsItem {
                    TextPref(
                        title = stringResource(res = Resources.strings.settings.dataProtection),
                        textColor = MaterialTheme.colorScheme.primary,
                        enabled = true,
                        onClick = { callback.openBrowser(SharedRes.strings.onboarding_data_protection_url.getString(context)) }
                    )
                }
//                ClientTarget.currentTarget.config.aboutStringResource?.let {
//                    prefsItem {
//                        TextPref(title = "", summary = it.getString(context))
//                    }
//                }
            }

            if (devSettings) {
                prefsGroup({ Text(
                    text = stringResource(res = Resources.strings.settings.application),
                    color = MaterialTheme.colorScheme.onBackground
                ) }) {
                    prefsItem {
                        ListPref(
                            key = "preset",
                            title = stringResource(res = Resources.strings.settings.presets),
                            defaultValue = config.preset.id,
                            useSelectedAsSummary = true,
                            entries = ClientDefaults.presets.associateBy({ it.id }, { it.name }),
                            onValueChange = { id ->
                                callback.indicateShouldReset()
                                preset = ClientDefaults.presets.first { it.id == id }
                                config.preset = preset
                            }
                        )
                    }
                    prefsItem {
                        ListPref(
                            key = "videoMode",
                            title = stringResource(res = Resources.strings.settings.videoMode),
                            defaultValue = config.videoMode.name,
                            useSelectedAsSummary = true,
                            entries = enumSettingEntries<VideoMode>(),
                            onValueChange = {
                                callback.indicateShouldReset()
                                videoMode = VideoMode.valueOf(it)
                                config.videoMode = videoMode
                            }
                        )
                    }
                    prefsItem {
                        ListPref(
                            key = "arcQuality",
                            title = stringResource(res = Resources.strings.settings.arcQualityLevel),
                            defaultValue = config.avatarQualityLevel.name,
                            useSelectedAsSummary = true,
                            entries = enumSettingEntries<AvatarQualityLevel>(),
                            onValueChange = {
                                config.avatarQualityLevel = AvatarQualityLevel.valueOf(it)
                            }
                        )
                    }


                    prefsItem {
                        ListPref(
                            key = "locale",
                            title = stringResource(res = Resources.strings.settings.asrLocale),
                            defaultValue = config.locale.toLanguageTag(),
                            useSelectedAsSummary = true,
                            entries = Locale.supportedLocales.associateBy({ it.toLanguageTag() }, { it.toLanguageTag() }),
                            onValueChange = { id ->
                                config.engineLocale = Locale.supportedLocales.first { it.toLanguageTag() == id }
                            }
                        )
                    }

                    prefsItem {
                        EditTextPref(
                            key = "initialCommand",
                            title = stringResource(res = Resources.strings.settings.initialCommand),
                            dialogMessage = stringResource(res = Resources.strings.settings.initialCommand),
                            enabled = true,
                            defaultValue = config.initialCommand,
                            onValueChange = {
                                config.initialCommand = it
                                callback.indicateShouldReset()
                            }
                        )
                    }

                    prefsItem {
                        EditTextPref(
                            key = "otelUrl",
                            title = stringResource(res = Resources.strings.settings.changeOtelUrl),
                            dialogMessage = stringResource(res = Resources.strings.settings.changeOtelUrl),
                            enabled = true,
                            defaultValue = config.otelUrl,
                            onValueChange = {
                                config.otelUrl = it
                            }
                        )
                    }

                    prefsItem {
                        TextPref(
                            title = stringResource(res = Resources.strings.settings.report),
                            textColor = MaterialTheme.colorScheme.primary,
                            enabled = true,
                            onClick = {
                                onClose(SettingsActivity.ShouldSendReportResultCode)
                            }
                        )
                    }


                    prefsItem {
                        TextPref(
                            title = stringResource(res = Resources.strings.settings.openPermissionSettings),
                            textColor = MaterialTheme.colorScheme.primary,
                            enabled = true,
                            onClick = {
                                openPermissionSettings(context)
                            }
                        )
                    }
                }

                prefsGroup({
                    Text(
                        text = stringResource(res = Resources.strings.settings.userInterface),
                        color = MaterialTheme.colorScheme.onBackground
                    )
                }) {
                    prefsItem {
                        ListPref(
                            key = "prefDarkMode",
                            defaultValue = config.appearance.toString(),
                            title = stringResource(res = Resources.strings.settings.appearance),
                            useSelectedAsSummary = true,
                            entries = enumSettingEntries<Appearance>(),
                            onValueChange = {
                                val newAppearance = Appearance.valueOf(it)
                                config.appearance = newAppearance
                                callback.setAppearance(newAppearance)
                            }
                        )
                    }

                    prefsItem {
                        ListPref(
                            key = "prefInteractionMode",
                            defaultValue = config.interactionMode.toString(),
                            title = stringResource(res = Resources.strings.settings.defaultInteractionMode),
                            useSelectedAsSummary = true,
                            entries = enumSettingEntries<InteractionMode>(),
                            onValueChange = {
                                interactionMode =
                                    if (it == "Voice") InteractionMode.Voice else InteractionMode.Text
                                config.interactionMode = interactionMode
                            }
                        )
                    }
                    prefsItem {
                        SwitchPref(
                            key = "prefPreventSleep",
                            title = stringResource(res = Resources.strings.settings.preventDisplayTurningOff),
                            defaultChecked = config.preventDisplaySleep,
                            onCheckedChange = { config.preventDisplaySleep = it }
                        )
                    }
                    prefsItem {
                        SwitchPref(
                            key = "prefBackToMenu",
                            title = stringResource(res = Resources.strings.settings.showExitConversationAlert),
                            defaultChecked = config.showExitConversationAlert,
                            onCheckedChange = { config.showExitConversationAlert = it }
                        )
                    }
                    prefsItem {
                        SwitchPref(
                            key = "prefOnScreenDebug",
                            title = stringResource(res = Resources.strings.settings.onScreenDebug),
                            defaultChecked = config.onScreenDebug,
                            onCheckedChange = { config.onScreenDebug = it }
                        )
                    }
                    if (ClientDefaults.requiresAppKeyInput) {
                        prefsItem {
                            SwitchPref(
                                key = "prefShowAppKeyEntry",
                                title = stringResource(res = Resources.strings.settings.showAppKeyEntry),
                                defaultChecked = config.showAppKeyEntry,
                                onCheckedChange = { config.showAppKeyEntry = it }
                            )
                        }
                    }

                }

                prefsGroup({
                    Text(
                        text = stringResource(res = Resources.strings.settings.asr),
                        color = MaterialTheme.colorScheme.onBackground
                    )
                }) {
                    prefsItem {
                        SwitchPref(
                            key = "prefDuplexEnabled",
                            title = stringResource(res = Resources.strings.settings.duplexEnabled),
                            defaultChecked = config.duplexEnabled,
                            onCheckedChange = {
                                config.duplexEnabled = it
                            }
                        )
                    }
                    prefsItem {
                        ListPref(
                            key = "prefDuplexMode",
                            defaultValue = config.duplexMode.name,
                            title = stringResource(res = Resources.strings.settings.duplexMode),
                            useSelectedAsSummary = true,
                            entries = enumSettingEntries<DuplexMode>(),
                            onValueChange = {
                                duplexMode = DuplexMode.valueOf(it)
                                config.duplexMode = duplexMode
                            }
                        )
                    }
                }
            }
        }
    }
}

private val avatarSize = 44