package ai.promethist.android.presentation.components.screen

import ai.promethist.android.ClientUiStateHandlerImpl
import ai.promethist.android.PromethistViewModel
import ai.promethist.android.ext.stringResource
import ai.promethist.android.presentation.PersonaViewModel
import ai.promethist.android.presentation.components.DebugSheet
import ai.promethist.android.presentation.components.dialog.AlertPopupOverlay
import ai.promethist.android.presentation.components.dialog.ErrorDialog
import ai.promethist.android.presentation.components.row.PersonaTopRowV2
import ai.promethist.android.style.SpaceMedium
import ai.promethist.android.style.SpaceSmall
import ai.promethist.android.style.SpaceXXLarge
import ai.promethist.android.style.SpaceXXXLarge
import ai.promethist.channel.model.ElysaiError
import ai.promethist.client.Client
import ai.promethist.client.ClientEvent
import ai.promethist.client.ClientModel
import ai.promethist.client.channel.ClientChannel
import ai.promethist.client.common.LoggerV3
import ai.promethist.client.model.ClientConnectionState
import ai.promethist.client.model.InteractionMode
import ai.promethist.client.resources.Resources
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.scaleIn
import androidx.compose.animation.scaleOut
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.rememberModalBottomSheetState
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun PersonaViewV2(
    viewModel: ClientModel,
    promethistViewModel: PromethistViewModel,
    personaViewModel: PersonaViewModel = viewModel(),
    onLogout: () -> Unit,
    onHomeClick: () -> Unit
) {
    // Needs to be done:
    // New modals
    // Fix logout when login required
    // Fix splash appear/disappear + transfer to conversation
    // Blur
    val coroutineScope = rememberCoroutineScope()

    val uiStateHandler = viewModel.uiStateHandler as ClientUiStateHandlerImpl
    val clientUiState = uiStateHandler.uiState.collectAsState()

    var interactionMode by remember {
        mutableStateOf(InteractionMode.Voice)
    }
    var networkWarningVisible by remember { mutableStateOf(false) }
    var debugVisible by remember { mutableStateOf(false) }

    val debugSheetState = rememberModalBottomSheetState(skipPartiallyExpanded = true)

    LaunchedEffect(clientUiState.value.connectionState) {
        if (clientUiState.value.connectionState == ClientConnectionState.Unstable) {
            networkWarningVisible = true
        }
    }

    Box(
        modifier = Modifier
            .fillMaxSize()
    ) {
        if (interactionMode == InteractionMode.Voice) {
            VoiceView(
                modifier = Modifier.padding(bottom = 35.dp),
                clientUiState = clientUiState,
                viewModel = viewModel
            )
        } else {
            TextView(messages = clientUiState.value.messages)
        }
        PersonaTopRowV2(
            modifier = Modifier
                .padding(top = SpaceXXLarge)
                .align(Alignment.TopCenter),
            interactionMode = interactionMode,
            debugButtonVisible = Client.appConfig().onScreenDebug,
            onHomeClick = {
                if (viewModel.config.isHomeScreenEnabled) {
                    viewModel.handleEvent(ClientEvent.Reset(false))
                    onHomeClick()
                } else {
                    viewModel.handleEvent(ClientEvent.Reset(true))
                }
            },
            onShouldLogin = {
                coroutineScope.launch {
                    delay(200)
                    viewModel.handleEvent(ClientEvent.Pause)
                    personaViewModel.onAppear(coroutineScope, true)
                }
            },
            onLogout = {
                onLogout()
            },
            handleEvent = {
                viewModel.handleEvent(it)
            },
            onDebugButtonClick = {
                viewModel.handleEvent(ClientEvent.Pause)
                debugVisible = true
            }
        ) {
            interactionMode = it
        }

        AnimatedVisibility(
            modifier = Modifier
                .align(Alignment.TopCenter)
                .offset(y = SpaceXXXLarge + SpaceMedium)
                .padding(horizontal = SpaceSmall, vertical = SpaceMedium),
            visible = clientUiState.value.connectionState == ClientConnectionState.Disconnected,
            enter = scaleIn(),
            exit = scaleOut()
        ) {
            AlertPopupOverlay(
                text = stringResource(res = Resources.strings.network.networkError),
                textAlign = TextAlign.Center,
                backgroundColor = Color(0xCCE93F3F)
            )
        }

        AnimatedVisibility(
            modifier = Modifier
                .align(Alignment.TopCenter)
                .offset(y = SpaceXXXLarge + SpaceMedium)
                .padding(horizontal = SpaceSmall, vertical = SpaceMedium),
            visible = networkWarningVisible,
            enter = scaleIn(),
            exit = scaleOut()
        ) {
            AlertPopupOverlay(
                text = stringResource(res = Resources.strings.network.networkWarning),
                textAlign = TextAlign.Start,
                backgroundColor = Color(0xCCF17B44),
                onDismiss = {
                    networkWarningVisible = false
                }
            )
        }

        clientUiState.value.error?.let {
            if (it != ClientChannel.sessionExpiredError) {
                var errorText: String? = null
                it.localizedText?.let { localizedText ->
                    errorText = stringResource(res = localizedText)
                }
                ErrorDialog(
                    error = ElysaiError("Server error", errorText ?: it.text, true),
                    onSecondaryClick = {
                        promethistViewModel.resetKeyEntry()
                    },
                    onRetryClick = {
                        viewModel.handleEvent(ClientEvent.Reset(true))
                    }
                )
            }
        }

        if (debugVisible) {
            DebugSheet(
                log = LoggerV3.getLogs(),
                onDismissRequest = {
                    debugVisible = false
                    viewModel.handleEvent(ClientEvent.Resume)
                },
                state = debugSheetState
            )
        }
    }
}