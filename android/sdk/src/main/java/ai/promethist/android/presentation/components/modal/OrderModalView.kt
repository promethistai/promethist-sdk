package ai.promethist.android.presentation.components.modal

import ai.promethist.android.ext.init
import ai.promethist.android.ext.stringResource
import ai.promethist.android.presentation.components.button.PromethistPrimaryButton
import ai.promethist.android.style.SpaceMedium
import ai.promethist.android.style.SpaceMediumLarge
import ai.promethist.android.style.SpaceMediumSmall
import ai.promethist.android.style.SpaceSmall
import ai.promethist.channel.item.OrderSummaryItem
import ai.promethist.client.resources.Resources
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentWidth
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

@Composable
fun OrderModalView(
    orderItem: OrderSummaryItem,
    onCancel: () -> Unit,
    onSubmit: () -> Unit
) {
    Column(
        modifier = Modifier.fillMaxWidth(),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.spacedBy(0.dp)
    ) {
        OrderModalHeader()
        HorizontalDivider()
        OrderModalItems(orderItem)
        OrderModalFooter(orderItem, onCancel, onSubmit)
    }
}

@Composable
private fun OrderModalHeader() {
    Text(
        text = stringResource(res = Resources.strings.modals.orderSummaryTitle),
        fontSize = 22.sp,
        fontWeight = FontWeight.Medium,
        color = Color.init(Resources.colors.personaTextPrimary),
        maxLines = 1,
        overflow = TextOverflow.Ellipsis,
        textAlign = TextAlign.Center,
        modifier = Modifier.run {
            fillMaxWidth()
                .padding(
                    start = SpaceMediumLarge,
                    end = SpaceMediumLarge,
                    bottom = SpaceMediumLarge,
                    top = SpaceSmall
                )
        }
    )
}

@Composable
private fun OrderModalFooter(
    orderItem: OrderSummaryItem,
    onCancel: () -> Unit,
    onSubmit: () -> Unit
) {
    Column(
        modifier = Modifier.fillMaxWidth(),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.spacedBy(0.dp)
    ) {
        if (orderItem.content.totalAmount != null || orderItem.content.totalAmountWithoutVat != null) {
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = SpaceMediumLarge, vertical = 14.dp)
                    .background(Color.init(Resources.colors.personaGrey2).copy(alpha = 0.25f)),
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.spacedBy(SpaceMedium)
            ) {
                Text(
                    text = stringResource(res = Resources.strings.modals.orderTotal),
                    fontSize = 20.sp,
                    fontWeight = FontWeight.Medium,
                    color = Color.init(Resources.colors.personaTextPrimary),
                    modifier = Modifier
                        .weight(1f)
                        .fillMaxWidth(),
                    textAlign = TextAlign.Start
                )
                OrderPriceView(
                    price = orderItem.content.totalAmount,
                    priceWithoutVat = orderItem.content.totalAmountWithoutVat
                )
            }
            HorizontalDivider()
        }

        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = SpaceMediumLarge)
                .padding(top = 22.dp),
            horizontalArrangement = Arrangement.spacedBy(SpaceMediumSmall)
        ) {
            PromethistPrimaryButton(
                text = stringResource(res = Resources.strings.modals.orderCancel),
                backgroundColor = Color.White,
                foregroundColor = Color.Black,
                onClick = onCancel,
                modifier = Modifier.weight(1f)
            )
            PromethistPrimaryButton(
                text = stringResource(res = Resources.strings.modals.orderSubmit),
                onClick = onSubmit,
                modifier = Modifier.weight(1f)
            )
        }
    }
}

@Composable
private fun OrderModalItems(orderItem: OrderSummaryItem) {
    // Provide an empty list if items is null
    val itemsList = orderItem.content.items ?: emptyList()
    LazyColumn(
        modifier = Modifier.fillMaxWidth()
    ) {
        items(itemsList) { order ->
            OrderItem(
                title = order.name,
                price = order.price,
                priceWithoutVat = order.priceWithoutVat,
                onRemoveClick = {}
            )
        }
    }
}

@Composable
private fun OrderItem(
    title: String,
    price: String?,
    priceWithoutVat: String?,
    onRemoveClick: () -> Unit
) {
    Column(
        modifier = Modifier.fillMaxWidth(),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.spacedBy(0.dp)
    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = SpaceMediumLarge, vertical = SpaceMedium),
            verticalAlignment = Alignment.Top,
            horizontalArrangement = Arrangement.spacedBy(SpaceMedium)
        ) {
            Text(
                text = title,
                fontSize = 20.sp,
                fontWeight = FontWeight.Medium,
                color = Color.init(Resources.colors.personaTextPrimary),
                modifier = Modifier
                    .weight(1f)
                    .fillMaxWidth(),
                maxLines = 3,
                overflow = TextOverflow.Ellipsis,
                textAlign = TextAlign.Start
            )
            OrderPriceView(
                price = price,
                priceWithoutVat = priceWithoutVat
            )
        }
        HorizontalDivider()
    }
}

@Composable
private fun OrderPriceView(price: String?, priceWithoutVat: String?) {
    Column(
        modifier = Modifier.wrapContentWidth(Alignment.End),
        verticalArrangement = Arrangement.spacedBy(6.dp),
        horizontalAlignment = Alignment.End
    ) {
        price?.let {
            Text(
                text = it,
                fontSize = 20.sp,
                fontWeight = FontWeight.Medium,
                color = Color.init(Resources.colors.personaTextPrimary)
            )
        }
        priceWithoutVat?.let {
            Text(
                text = it,
                fontSize = 12.sp, 
                fontWeight = FontWeight.Normal,
                color = Color.init(Resources.colors.personaTextSecondary)
            )
        }
    }
}