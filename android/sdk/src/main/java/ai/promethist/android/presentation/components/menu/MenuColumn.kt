package ai.promethist.android.presentation.components.menu

import ai.promethist.android.style.SpaceSmall
import ai.promethist.channel.command.GrowthContent
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

@Composable
fun MenuColumn(
    growthContent: GrowthContent,
    onSubmoduleStart: (String) -> Unit
) {
    val scrollState = rememberScrollState()

    Column(
        modifier = Modifier
            .fillMaxSize(),
        verticalArrangement = Arrangement.Top,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(
            text = growthContent.title,
            textAlign = TextAlign.Center,
            fontSize = 20.sp,
            style = MaterialTheme.typography.titleLarge.copy(lineHeight = 24.sp),
            fontWeight = FontWeight.Bold,
            color = MaterialTheme.colorScheme.onBackground
        )

        MenuProgressTile(
            progress = growthContent.progress,
            goal = growthContent.goal,
            text = growthContent.text
        )

        Column (
            modifier = Modifier
                .verticalScroll(scrollState)
                .padding(top = SpaceSmall),
            verticalArrangement = Arrangement.spacedBy(10.dp)
        ) {
            growthContent.modules.forEach { module ->
                MenuColumnItem(
                    module = module,
                    onSubmoduleStart = onSubmoduleStart
                )
            }
        }
    }
}
