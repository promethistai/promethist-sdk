package ai.promethist.android.presentation.components.modal

import ai.promethist.android.ext.stringResource
import ai.promethist.android.presentation.components.button.PromethistPrimaryButton
import ai.promethist.android.presentation.components.screen.ModalView
import ai.promethist.android.style.SpaceMediumSmall
import ai.promethist.android.style.SpaceSmall
import ai.promethist.android.style.SpaceXXLarge
import ai.promethist.channel.command.SessionEnd
import ai.promethist.channel.item.SessionEndItem
import ai.promethist.client.resources.Resources
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

@Composable
fun HomeDisclaimerModalView(
    onConfirm: () -> Unit,
    onCancel: () -> Unit
) {
    Column(
        modifier = Modifier.fillMaxWidth(),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.spacedBy(0.dp)
    ) {
        Text(
            text = stringResource(res = Resources.strings.persona.homeDisclaimerText),
            fontSize = 24.sp,
            modifier = Modifier
                .fillMaxWidth()
                .padding(bottom = SpaceXXLarge)
                .padding(top = SpaceSmall)
        )
        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.spacedBy(SpaceMediumSmall)
        ) {
            PromethistPrimaryButton(
                text = stringResource(res = Resources.strings.persona.homeDisclaimerNo),
                backgroundColor = Color.White,
                foregroundColor = Color.Black,
                onClick = onCancel,
                modifier = Modifier.weight(1f)
            )
            PromethistPrimaryButton(
                text = stringResource(res = Resources.strings.persona.homeDisclaimerYes),
                onClick = onConfirm,
                modifier = Modifier.weight(1f)
            )
        }
    }
}

@Preview
@Composable
fun TestPrev() {
    ModalView(modifier = Modifier, modal = SessionEndItem(SessionEnd()), handleEvent = {})
}