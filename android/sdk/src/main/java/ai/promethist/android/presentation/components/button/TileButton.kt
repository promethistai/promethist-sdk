package ai.promethist.android.presentation.components.button

import ai.promethist.SharedRes
import ai.promethist.android.R
import ai.promethist.android.ext.accentSecondary
import ai.promethist.android.ext.conditional
import ai.promethist.android.ext.init
import ai.promethist.android.style.SpaceXXSmall
import ai.promethist.channel.command.ActionTile
import ai.promethist.client.resources.Resources
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

@Composable
fun TileButton(
    tile: ActionTile,
    fullWidth: Boolean,
    onClickCallback: (String, String, String?) -> Unit
) {
    Button(
        onClick = { onClickCallback("#${ tile.action }", tile.iosTitle ?: tile.title, tile.preferredInteractionMode)},
        enabled = tile.active,
        shape = RoundedCornerShape(50),
        colors = ButtonDefaults.buttonColors(containerColor = accentSecondary())
    ) {
        Box(
            modifier = Modifier
                .padding(SpaceXXSmall)
                .conditional(fullWidth) {
                    this.fillMaxWidth()
                }
                .wrapContentHeight(),
            contentAlignment = Alignment.CenterStart
        ) {
            Row (verticalAlignment = Alignment.CenterVertically) {
                if (fullWidth) Spacer(modifier = Modifier.weight(1f))

                Column(
                    verticalArrangement = Arrangement.spacedBy(SpaceXXSmall),
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    Text(
                        text = tile.iosTitle ?: tile.title,
                        style = MaterialTheme.typography.labelMedium.copy(
                            fontSize = 16.sp,
                            fontWeight = FontWeight.SemiBold,
                            color = Color.init(res = Resources.colors.primaryLabel)
                        )
                    )

                    tile.text?.let {
                        Text(
                            text = it,
                            style = MaterialTheme.typography.labelMedium.copy(
                                fontSize = 14.sp,
                                fontWeight = FontWeight.Normal,
                                color = Color.init(res = Resources.colors.primaryLabel)
                            )
                        )
                    }
                }

                tile.iosIcon?.let {
                    if (it.isNotEmpty()) {
                        val iconId = when (it) {
                            "questionmark.circle" -> SharedRes.images.ic_questionmark_circle.drawableResId
                            "hand.thumbsup.fill" -> SharedRes.images.ic_checkmark.drawableResId
                            else -> R.drawable.baseline_question_mark_24
                        }
                        Image(
                            painter = painterResource(id = iconId),
                            contentDescription = "Tile icon",
                            modifier = Modifier.size(22.dp),
                            colorFilter = ColorFilter.tint(MaterialTheme.colorScheme.onPrimary)
                        )
                    }
                }

                if (fullWidth) Spacer(modifier = Modifier.weight(1f))
            }
        }
    }
}