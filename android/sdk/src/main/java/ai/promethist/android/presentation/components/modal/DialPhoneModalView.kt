package ai.promethist.android.presentation.components.modal

import ai.promethist.android.ext.stringResource
import ai.promethist.android.presentation.components.button.PromethistPrimaryButton
import ai.promethist.android.style.SpaceSmall
import ai.promethist.android.style.SpaceXLarge
import ai.promethist.channel.item.DialPhoneItem
import ai.promethist.client.resources.Resources
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.sp

@Composable
fun DialPhoneModalView(
    item: DialPhoneItem,
    onCancel: () -> Unit,
    onCall: () -> Unit
) {
    Column(
        modifier = Modifier.fillMaxWidth(),
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {
        Text(
            text = "${stringResource(res = Resources.strings.modals.callText)} ${item.content.phoneNumber}",
            modifier = Modifier
                .fillMaxWidth()
                .padding(vertical = SpaceXLarge)
                .padding(bottom = SpaceSmall),
            fontWeight = FontWeight.Medium,
            fontSize = 24.sp,
            maxLines = 4,
            overflow = TextOverflow.Ellipsis,
            textAlign = TextAlign.Center
        )
        Row(
            modifier = Modifier
                .fillMaxWidth(),
            horizontalArrangement = Arrangement.spacedBy(SpaceSmall)
        ) {
            PromethistPrimaryButton(
                text = stringResource(res = Resources.strings.modals.callCancel),
                backgroundColor = Color.White,
                foregroundColor = Color.Black,
                onClick = onCancel,
                modifier = Modifier.weight(1f)
            )
            PromethistPrimaryButton(
                text = stringResource(res = Resources.strings.modals.callSubmit),
                onClick = onCall,
                modifier = Modifier.weight(1f)
            )
        }
    }
}