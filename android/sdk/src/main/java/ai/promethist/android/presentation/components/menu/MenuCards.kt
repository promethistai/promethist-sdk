package ai.promethist.android.presentation.components.menu

import ai.promethist.channel.command.GrowthState
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.layout.ColumnScope
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp

object MenuCards {
    fun getCard(state: GrowthState, isLockedClickable: Boolean = false):
            @Composable (() -> Unit, Modifier, @Composable() (ColumnScope.() -> Unit)) -> Unit {
        val card = @Composable {
                onClick: (() -> Unit),
                modifier: Modifier,
                content: @Composable ColumnScope.() -> Unit
            ->

            val lockedFunction: () -> Unit = if (isLockedClickable) onClick else { {} }
            when (state) {
                GrowthState.Locked ->  LockedCard(lockedFunction, modifier, content)
                GrowthState.Unlocked -> UnlockedCard(onClick, modifier, content)
                GrowthState.New -> NewCard(onClick, modifier, content)
                GrowthState.InProgress -> InProgressCard(onClick, modifier, content)
                GrowthState.Done -> DoneCard(onClick, modifier, content)
            }
        }
        return card
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
private fun LockedCard(onClick: () -> Unit, modifier: Modifier = Modifier, content: @Composable  ColumnScope.() -> Unit) {
    Card(
        onClick = {
          onClick()
        },
        modifier = modifier,
        elevation = CardDefaults.cardElevation(4.dp),
        shape = RoundedCornerShape(25.dp),
        content = content,
        colors = CardDefaults.cardColors(containerColor = Color.LightGray, contentColor = Color.Gray)
    )
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
private fun NewCard(onClick: () -> Unit, modifier: Modifier = Modifier, content: @Composable  ColumnScope.() -> Unit) {
    Card(
        onClick = {
          onClick()
        },
        modifier = modifier,
        elevation = CardDefaults.cardElevation(4.dp),
        shape = RoundedCornerShape(25.dp),
        content = content,
        border = BorderStroke(4.dp, Color.Yellow),
        colors = CardDefaults.cardColors(
            containerColor = MaterialTheme.colorScheme.primaryContainer
        )
    )
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
private fun UnlockedCard(onClick: () -> Unit, modifier: Modifier = Modifier, content: @Composable  ColumnScope.() -> Unit) {
    Card(
        onClick = {
          onClick()
        },
        modifier = modifier,
        elevation = CardDefaults.cardElevation(4.dp),
        shape = RoundedCornerShape(25.dp),
        content = content,
        border = BorderStroke(4.dp, MaterialTheme.colorScheme.primary),
        colors = CardDefaults.cardColors(
            containerColor = MaterialTheme.colorScheme.secondaryContainer,
            contentColor = MaterialTheme.colorScheme.primary
        )
    )
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
private fun InProgressCard(onClick: () -> Unit, modifier: Modifier = Modifier, content: @Composable  ColumnScope.() -> Unit) {
    Card(
        onClick = {
          onClick()
        },
        modifier = modifier,
        elevation = CardDefaults.cardElevation(4.dp),
        shape = RoundedCornerShape(25.dp),
        content = content
    )
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
private fun DoneCard(onClick: () -> Unit, modifier: Modifier = Modifier, content: @Composable  ColumnScope.() -> Unit) {
    Card(
        onClick = {
          onClick()
        },
        modifier = modifier,
        elevation = CardDefaults.cardElevation(4.dp),
        shape = RoundedCornerShape(25.dp),
        content = content,
        colors = CardDefaults.cardColors(
            containerColor = MaterialTheme.colorScheme.primary,
            contentColor = Color.White
        )
    )
}