package ai.promethist.android.utils

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

object AppEventBus {
    private val _eventFlow = MutableSharedFlow<AppEvent>()

    fun subscribe(scope: CoroutineScope, block: suspend (AppEvent) -> Unit) =
        _eventFlow.onEach(block).launchIn(scope)

    suspend fun emit(appEvent: AppEvent) = _eventFlow.emit(appEvent)
}

sealed interface AppEvent {
    data object OnAuthSessionUpdated : AppEvent
    data object OnAvatarAudioStarted : AppEvent
    data class OnSplashScreenUpdate(
        val splashVisible: Boolean?,
        val downloadProgress: Float?,
    ) : AppEvent
    data class OnQRCodeRead(
        val domain: String,
        val setupName: String
    ) : AppEvent
    data class MicPowerUpdated(val rms: Float) : AppEvent

    data class MicPermissionUpdated(val denied: Boolean) : AppEvent
}