package ai.promethist.android.style

import ai.promethist.android.ext.init
import ai.promethist.client.model.Appearance
import ai.promethist.client.resources.Resources
import android.os.Build
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.darkColorScheme
import androidx.compose.material3.dynamicDarkColorScheme
import androidx.compose.material3.dynamicLightColorScheme
import androidx.compose.material3.lightColorScheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext

@Composable
fun ElysaiAndroidTheme(
    // Dynamic color is available on Android 12+
    appearance: Appearance = Appearance.System,
    dynamicColor: Boolean = false,
    content: @Composable () -> Unit
) {
    val darkColorScheme = darkColorScheme(
        primary = Color.LightGray,
        onPrimary = Color.Black
    )

    val lightColorScheme = lightColorScheme(
        primary = Color.init(Resources.colors.accent)
    )
    val darkTheme: Boolean = isSystemInDarkTheme()
    val useDarkTheme = appearance == Appearance.Dark || (appearance == Appearance.System && darkTheme)

    val colorScheme = when {
        dynamicColor && Build.VERSION.SDK_INT >= Build.VERSION_CODES.S -> {
            val context = LocalContext.current
            if (useDarkTheme) dynamicDarkColorScheme(context) else dynamicLightColorScheme(context)
        }
        useDarkTheme -> darkColorScheme
        else -> lightColorScheme
    }

    MaterialTheme(
        colorScheme = colorScheme,
        content = content
    )
}