package ai.promethist.android.presentation.components.row

import ai.promethist.android.presentation.components.button.VoteButton
import ai.promethist.client.model.Message
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import ai.promethist.android.R

@Composable
fun VoteRow(modifier: Modifier, message: Message, onClick: (String, String?, Int) -> Unit) {
    var thumbsUpId by remember { mutableStateOf(R.drawable.thumbsup_emoji_50) }
    var thumbsDownId by remember { mutableStateOf(R.drawable.thumbsdown_emoji_50) }
    Row(
        modifier = modifier
            .fillMaxHeight(0.1F)
            .fillMaxWidth(0.22F)
    ) {
        thumbsUpId = if (message.thumbsUpActive) R.drawable.thumbsup_emoji else R.drawable.thumbsup_emoji_50
        thumbsDownId = if (message.thumbsDownActive) R.drawable.thumbsdown_emoji else R.drawable.thumbsdown_emoji_50
        val buttonModifier = Modifier.weight(1F).padding(horizontal = 4.dp)
        VoteButton(imageId = thumbsUpId, modifier = buttonModifier) {
            message.thumbsUpActive = true
            message.thumbsDownActive = false
            thumbsUpId = R.drawable.thumbsup_emoji
            thumbsDownId = R.drawable.thumbsdown_emoji_50
            onClick(message.turnId, message.nodeId, 1)
        }
        VoteButton(imageId = thumbsDownId, modifier = buttonModifier) {
            message.thumbsUpActive = false
            message.thumbsDownActive = true
            thumbsUpId = R.drawable.thumbsup_emoji_50
            thumbsDownId = R.drawable.thumbsdown_emoji
            onClick(message.turnId, message.nodeId, -1)
        }
    }
}