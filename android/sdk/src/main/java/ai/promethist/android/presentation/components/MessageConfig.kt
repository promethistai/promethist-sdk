package ai.promethist.android.presentation.components

import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.graphics.Color

data class MessageConfig(
    val backgroundColorPrimary: Color,
    val backgroundColorSecondary: Color,
    val textColor: Color,
    val alignment: Alignment
)

@Composable
fun personaMessageConfig() = MessageConfig(
    MaterialTheme.colorScheme.onPrimary,
    MaterialTheme.colorScheme.onPrimary,
    Color.DarkGray,
    Alignment.CenterStart
)

@Composable
fun userMessageConfig() = MessageConfig(
    MaterialTheme.colorScheme.primary,
    Color(0XFFFFC37D),
    MaterialTheme.colorScheme.onPrimary,
    Alignment.CenterEnd
)