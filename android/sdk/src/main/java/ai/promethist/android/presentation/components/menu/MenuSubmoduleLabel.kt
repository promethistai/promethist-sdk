package ai.promethist.android.presentation.components.menu

import ai.promethist.SharedRes
import ai.promethist.android.R
import ai.promethist.channel.command.GrowthState
import ai.promethist.channel.command.GrowthSubmodule
import androidx.compose.animation.*
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.PlayArrow
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

@Composable
fun MenuSubmoduleLabel(
    menuSubmodule: GrowthSubmodule,
    visible: Boolean,
    onButtonClick: (String) -> Unit
) {
    Box(
        contentAlignment = Alignment.CenterStart,
        modifier = Modifier
            .fillMaxWidth()
            .padding(start = 24.dp)
            .wrapContentHeight()
    ) {
        Column(
            modifier = Modifier
                .wrapContentHeight()
                .padding(vertical = 16.dp),
        ) {
            Text(
                text = menuSubmodule.title,
                style = MaterialTheme.typography.labelLarge,
                fontSize = 16.sp,
                textAlign = TextAlign.Start
            )

            AnimatedVisibility(
                visible = visible,
                enter = expandVertically { 0 },
                exit = shrinkVertically()
            ) {
                Row(
                    modifier = Modifier
                        .padding(top = 8.dp)
                        .fillMaxWidth()
                ) {
                    Text(
                        text = menuSubmodule.description ?: "",
                        modifier = Modifier.weight(4F),
                        textAlign = TextAlign.Start
                    )
                    if (menuSubmodule.state == GrowthState.Locked) {
                        Image(
                            painter = painterResource(id = SharedRes.images.ic_lock_fill.drawableResId),
                            contentDescription = stringResource(R.string.content_desc_icon_lock),
                            modifier = Modifier
                                .padding(end = 16.dp)
                                .weight(2F)
                                .align(Alignment.Bottom),
                            colorFilter = ColorFilter.tint(Color.Gray)
                        )
                    } else {
                        Button(
                            onClick = { onButtonClick(menuSubmodule.action) },
                            modifier = Modifier
                                .padding(end = 16.dp)
                                .weight(2F)
                                .align(Alignment.Bottom),
                            colors = ButtonDefaults.buttonColors(
                                containerColor = MaterialTheme.colorScheme.secondary,
                                contentColor = MaterialTheme.colorScheme.onSecondary
                            )
                        ) {
                            Image(
                                imageVector = Icons.Filled.PlayArrow,
                                contentDescription = stringResource(R.string.content_desc_icon_play)
                            )
                        }
                    }
                }
            }
        }
    }
}


