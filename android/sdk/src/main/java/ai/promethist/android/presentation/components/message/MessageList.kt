package ai.promethist.android.presentation.components.message

import ai.promethist.android.presentation.components.personaMessageConfig
import ai.promethist.android.presentation.components.userMessageConfig
import ai.promethist.client.model.Message
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp

@Composable
fun MessageList(modifier: Modifier, messageList: List<Message>, onClick: (String, String?, Int) -> Unit) {
    val state = rememberLazyListState()
    if (state.firstVisibleItemIndex <= 1) {
        //scroll to top to ensure latest added book gets visible
        LaunchedEffect(key1 = state) {
            state.scrollToItem(0)
        }
    }
    LazyColumn(
        modifier = modifier.fillMaxSize(),
        contentPadding = PaddingValues(16.dp),
        state = state,
        reverseLayout = true
    ) {
        items(messageList) {message ->
            val config = if (!message.isUser) personaMessageConfig() else userMessageConfig()
            MessageCard(messageConfig = config, message = message, onClick = onClick)
        }
    }
}