package ai.promethist.android.presentation.components

import ai.promethist.android.ext.stringResource
import ai.promethist.android.style.SpaceMediumSmall
import ai.promethist.android.style.SpaceSmall
import ai.promethist.client.model.Message
import ai.promethist.client.resources.Resources
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import java.util.Date


// TODO use actual history
data class HistoryRange(val date: Date, val items: List<HistoryItem>)
data class HistoryItem(val summary: String, val messages: List<Message>)
@Composable
fun HistoryView(
    modifier: Modifier,
    items: List<HistoryRange>,
    onClick: (List<Message>) -> Unit
) {
    val scrollState = rememberScrollState()

    LaunchedEffect(items) {
        scrollState.scrollTo(0)
    }

    Column(
        modifier = modifier
    ) {
        Text(
            text = stringResource(res = Resources.strings.home.historyOfConversations),
            fontSize = titleFontSize,
            modifier = Modifier.padding(top = SpaceMediumSmall, bottom = SpaceSmall)
        )
        Column(
            modifier = Modifier
                .verticalScroll(scrollState)
        ) {
            items.forEach {
                HistorySection(range = it) { list ->
                    onClick(list)
                }
            }
            // Space for the button
            Spacer(modifier = Modifier.height(spacerHeight))
        }
    }
}

@Composable
fun HistorySection(
    range: HistoryRange,
    onClick: (List<Message>) -> Unit
) {
    Column {
        Text(
            modifier = Modifier.padding(vertical = SpaceSmall),
            text = range.date.toString(),
            fontSize = dateFontSize,
            color = Color(0xFF989898)
        )
        range.items.forEach { 
            Button(
                modifier = Modifier
                    .height(openButtonHeight)
                    .padding(vertical = SpaceSmall)
                    .fillMaxWidth()
                    .clip(RoundedCornerShape(radius))
                    .border(
                        width = 1.dp,
                        color = Color(0xFFE2E2E2),
                        shape = RoundedCornerShape(radius)
                    ),
                onClick = { onClick(it.messages) },
                colors = ButtonDefaults.buttonColors(containerColor = Color.Transparent)) {
                Text(
                    text = it.summary,
                    fontSize = historyButtonLabelFontSize,
                    color = Color.Black
                )
            }
        }
    }
}
private val radius = 10.dp
private val openButtonHeight = 56.dp
private val spacerHeight = 96.dp
private val dateFontSize = 14.sp
private val historyButtonLabelFontSize = 18.sp
private val titleFontSize = 24.sp
