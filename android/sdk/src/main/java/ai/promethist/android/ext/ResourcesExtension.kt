package ai.promethist.android.ext

import ai.promethist.client.ClientTarget
import ai.promethist.client.ClientTargetPreset
import ai.promethist.client.resources.Resources
import androidx.compose.runtime.Composable
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Shadow
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.platform.LocalContext
import dev.icerock.moko.resources.ColorResource
import dev.icerock.moko.resources.ImageResource
import dev.icerock.moko.resources.desc.ResourceStringDesc

@Composable
fun painterResource(res: ImageResource): Painter =
    androidx.compose.ui.res.painterResource(id = res.drawableResId)

@Composable
fun stringResource(res: ResourceStringDesc): String =
    androidx.compose.ui.res.stringResource(id = res.stringRes.resourceId)

@Composable
fun Color.Companion.init(res: ColorResource): Color {
    val context = LocalContext.current
    return Color(res.getColor(context)).copy(alpha = 1f)
}

val textShadow = Shadow(
    color = Color.Black.copy(alpha = 0.6f),
    offset = Offset(2f, 2.0f),
    blurRadius = 1f
)

@Composable
fun regularMaterial(): Color = Color.init(Resources.colors.regularMaterial).copy(.88f)

@Composable
fun accentSecondary(): Color = Color.init(res = Resources.colors.accentSecondary)
    .copy(if (ClientTarget.currentTarget == ClientTargetPreset.DFHA) 1f else .22f)