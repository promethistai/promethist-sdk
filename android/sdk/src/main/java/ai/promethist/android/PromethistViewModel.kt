package ai.promethist.android

import ai.promethist.android.ext.onboardingTermsAccepted
import ai.promethist.client.common.AppConfig
import ai.promethist.client.Client
import ai.promethist.client.ClientDefaults
import ai.promethist.client.auth.AuthPolicy
import ai.promethist.client.model.MicPermission
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel

class PromethistViewModel(
    val appConfig: AppConfig = Client.appConfig(),
    val appLinkOpened: Boolean = false,
    val isHomeScreenEnabled: Boolean,
    val onReset: () -> Unit
) : ViewModel() {

    private var appKeyEntryPresented: Boolean by mutableStateOf(appConfig.showAppKeyEntry)
    var startPoint: PromethistStartPoint by mutableStateOf(determineStartPoint())

    fun updateStartPoint() {
        startPoint = determineStartPoint()
    }

    fun appKeyEntered() {
        appKeyEntryPresented = false
        updateStartPoint()
    }

    fun resetKeyEntry() {
        appKeyEntryPresented = true
        updateStartPoint()
    }

    private fun determineStartPoint(): PromethistStartPoint {
        if (appConfig.micPermission == MicPermission.Unknown || appConfig.micPermission == MicPermission.Denied)
            return PromethistStartPoint.Permissions
        if (!appConfig.onboardingTermsAccepted)
            return PromethistStartPoint.Onboarding
        if (appKeyEntryPresented && appConfig.showAppKeyEntry && !appLinkOpened)
            return PromethistStartPoint.AppKeyEntry
        if (ClientDefaults.authPolicy == AuthPolicy.Required && !Client.authClient().isLoggedIn())
            return PromethistStartPoint.AuthScreen
        if (appConfig.showIntroVideo)
            return PromethistStartPoint.IntroVideo
        if (isHomeScreenEnabled)
            return PromethistStartPoint.PersonaSelect


        onReset()
        return PromethistStartPoint.Main
    }
}