package ai.promethist.android.presentation.components.util

import android.view.ViewTreeObserver
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.platform.LocalView
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat


// Taken from https://github.com/lofcoding/ImeOverlappingIssue/blob/master/app/src/main/java/com/example/androidwindowinsets/ImeListener.kt
// and https://stackoverflow.com/a/72566595
@Composable
fun rememberKeyboardPaddingState(): MutableState<Dp> {
    val keyboardPaddingState = remember { mutableStateOf(0.dp) }
    val view = LocalView.current
    val density = LocalDensity.current
    DisposableEffect(view) {
        val listener = ViewTreeObserver.OnGlobalLayoutListener {
            val insets = ViewCompat.getRootWindowInsets(view)
            with (density) {
                val keyboardHeight = (insets?.getInsets(WindowInsetsCompat.Type.ime())?.bottom ?: 0).toDp()
                keyboardPaddingState.value = if (keyboardHeight == 0.dp) 0.dp else keyboardHeight - 48.dp
            }
        }
        view.viewTreeObserver.addOnGlobalLayoutListener(listener)
        onDispose {
            view.viewTreeObserver.removeOnGlobalLayoutListener(listener)
        }
    }
    return keyboardPaddingState
}