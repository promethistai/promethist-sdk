package ai.promethist.android.presentation.components.screen

import ai.promethist.android.presentation.components.BottomInput
import ai.promethist.android.presentation.components.message.MessageList
import ai.promethist.channel.item.Modal
import ai.promethist.client.model.Message
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment.Companion.CenterHorizontally
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp

@Composable
fun TextSubview(
    modifier: Modifier = Modifier,
    messageList: List<Message>,
    enableInput: Boolean,
    modal: Modal? = null
) {
    Column(
        modifier = modifier.fillMaxSize()
    ) {
        MessageList(
            modifier = Modifier
                .padding(top = 16.dp)
                .weight(1F),
            messageList = messageList.reversed(),
            onClick = {
                    turnId, nodeId, vote -> // callback.onVote(turnId, nodeId, vote)
            }
        )
        BottomInput(
            modifier = Modifier.align(CenterHorizontally),
            modal = modal,
            textMode = true,
            enableInput = enableInput,
            onHomeClick = {  },
            onSkipClick = {  },
            onUndoClick = {},
            onPersonaSelected = { personaItem, personaType ->

            },
            onUserInput = { text, label ->

            },
            onCameraGranted = {

            },
            onImagePicked = {


            },
            onCameraActivated = {

            }
        )
    }
}