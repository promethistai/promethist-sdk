package ai.promethist.android.presentation.components.dialog

import ai.promethist.SharedRes
import ai.promethist.channel.model.ElysaiError
import ai.promethist.client.ClientTarget
import ai.promethist.client.ClientTargetPreset
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.window.DialogProperties

@Composable
fun ErrorDialog(
    error: ElysaiError,
    onSecondaryClick: () -> Unit,
    onRetryClick: () -> Unit
) {
    val context = LocalContext.current
    val properties = DialogProperties(
        dismissOnBackPress = false,
        dismissOnClickOutside = false,
        usePlatformDefaultWidth = false
    )
    AlertDialog(modifier = Modifier
        .padding(8.dp)
        .fillMaxWidth(0.92F),
        properties = properties,
        onDismissRequest = {},
        confirmButton = {
            TextButton(onClick = { onRetryClick() }) {
                Text(SharedRes.strings.error_retry.getString(context))
            }
        },
        dismissButton = {
            if (ClientTarget.currentTarget == ClientTargetPreset.Promethist) {
                TextButton(onClick = { onSecondaryClick() }) {
                    Text("Back to initial screen")
               }
            }
        },
        text = {
            Column(
                modifier = Modifier.verticalScroll(rememberScrollState())
            ) {
                Text(
                    text = error.type,
                    modifier = Modifier.padding(top = 8.dp, bottom = 4.dp),
                    style = MaterialTheme.typography.titleLarge,
                    fontWeight = FontWeight.Bold,
                    fontSize = 18.sp
                )
                if (error.isUserFriendly){
                    Text(
                        text = error.message,
                        modifier = Modifier.padding(top = 8.dp, bottom = 4.dp),
                        fontSize = 14.sp
                    )
                } else {
                    TextField(
                        value = error.message,
                        onValueChange = {},
                        modifier = Modifier.padding(top = 8.dp, bottom = 4.dp),
                        readOnly = true
                    )
                }
            }
        })
}