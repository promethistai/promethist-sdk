package ai.promethist.android.services

import ai.promethist.android.ext.runOnMainLooper
import android.content.Context
import android.net.Uri
import android.os.Handler
import androidx.media3.common.MediaItem
import androidx.media3.common.PlaybackException
import androidx.media3.common.Player
import androidx.media3.common.util.UnstableApi
import androidx.media3.datasource.RawResourceDataSource
import androidx.media3.exoplayer.DefaultLoadControl
import androidx.media3.exoplayer.ExoPlayer
import androidx.media3.exoplayer.upstream.DefaultAllocator
import androidx.media3.exoplayer.upstream.DefaultBandwidthMeter
import dev.icerock.moko.resources.FileResource
import kotlinx.coroutines.delay
import java.lang.Thread.sleep

@UnstableApi
class ExoPlayerVideoController(context: Context) : Player.Listener {

    val exoPlayer = createVideoPlayer(context).apply { volume = 1.0F }
    // var callback: VideoPlayerControllerCallback? = null
    private var isPlayingStreamedMediaItem: Boolean = false
    private var currentStreamedMediaItem: MediaItem? = null
    private var videoQueue = mutableListOf<MediaItem>()
    private val ready = mutableMapOf<String, Boolean>()
    private val restarts = mutableMapOf<String, Int>()
    private var isBuffering: Boolean = false
    private var currentErrorCallback: (() -> Unit)? = null

    private fun createVideoPlayer(context: Context): ExoPlayer {
        val loadControl = DefaultLoadControl.Builder()
            .setAllocator(DefaultAllocator(true, AllocationSize))
            .setBufferDurationsMs(BufferDurationsMs, BufferDurationsMs, BufferDurationsMs, BufferDurationsMs)
            .setTargetBufferBytes(TargetBufferBytes)
            .setPrioritizeTimeOverSizeThresholds(true)
            .build()

        val bandwidthMeter = DefaultBandwidthMeter.Builder(context).build().apply {
            addEventListener(object: Handler() {}) { _, _, bitrateEstimate ->
                // callback?.onBandwidthSample(bitrateEstimate)
            }
        }
        return ExoPlayer.Builder(context)
            .setLoadControl(loadControl)
            .setBandwidthMeter(bandwidthMeter)
            .build().apply {
                addListener(this@ExoPlayerVideoController)
            }
    }

    val streamedVideoStartDelay: Long
        get() = StreamedVideoStartDelayMs

    fun append(videoFile: FileResource, name: String) {
        val uri = RawResourceDataSource.buildRawResourceUri(videoFile.rawResId)
        val mediaItem = MediaItem.Builder().setMediaId(name).setUri(uri).build()
        videoQueue.add(mediaItem)
        runOnMainLooper {
            if (exoPlayer.mediaItemCount == 0 && videoQueue.isNotEmpty()) {
                continuePlaying()
            }
        }
    }
    fun append(url: String, name: String, transcript: String?) {
        val flvURL =
            if (name == "first_contact") url.replace("mp4", "flv") else url
        val mediaItem = MediaItem.Builder().setMediaId(name).setUri(Uri.parse(flvURL)).build()
        videoQueue.add(mediaItem)
        runOnMainLooper {
            if (exoPlayer.mediaItemCount == 0 && videoQueue.isNotEmpty()) {
                continuePlaying()
            }
        }
    }

    // Append streamed video and play immediately
    suspend fun appendAndPlay(
        url: String,
        name: String,
        transcript: String?,
        onBuffered: () -> Unit,
        onError: () -> Unit
    ) {
        // Update callback
        currentErrorCallback = onError

        // Build media item
        val mediaItem = MediaItem.Builder().apply {
            setMediaId(name)
            setUri(Uri.parse(url))
        }.build()

        // Clear all video items except the current one
        videoQueue.clear()
        clearItemsInQueue()

        // Append media item & seek to it
        runOnMainLooper {
            exoPlayer.addMediaItem(mediaItem)
            exoPlayer.seekToNextMediaItem()
        }

        // Update internal state
        currentStreamedMediaItem = mediaItem
        isPlayingStreamedMediaItem = true

        // Block thread until playback finish & onBuffered() callback
        var onBufferedCalled = false
        while (isPlayingStreamedMediaItem) {
            delay(IsPlayingDelayMs)

            if (!isBuffering && !onBufferedCalled) {
                onBuffered()
                onBufferedCalled = true
            }
        }
        currentErrorCallback = null
    }

    fun appendNext(url: String, name: String, transcript: String?) {
        //TODO Implement..
    }

    fun replay() {
        runOnMainLooper {
            exoPlayer.seekTo(0)
        }
        play()
    }

    fun play() {
        runOnMainLooper {
            exoPlayer.prepare()
            exoPlayer.play()
        }
    }

    fun pause() {
        runOnMainLooper {
            exoPlayer.pause()
        }
    }

    fun skip() {
        if (isPlayingStreamedMediaItem) {
            isPlayingStreamedMediaItem = false
            onPlaybackEnded(true)
            return
        }

        runOnMainLooper {
            exoPlayer.seekToNextMediaItem()
        }
    }

    fun stop() {
        runOnMainLooper {
            exoPlayer.stop()
        }
    }

    fun clearQueue() {
        videoQueue.clear()
        runOnMainLooper {
            exoPlayer.clearMediaItems()
        }
    }

    fun removeItemsWhereNameContains(value: String) {
        videoQueue = videoQueue.filter { !it.mediaId.contains(value) }.toMutableList()
    }

    fun prefetch(url: String) {}

    override fun onPlaybackStateChanged(playbackState: Int) {
        when(playbackState) {
            Player.STATE_ENDED -> onPlaybackEnded()
            Player.STATE_READY -> {
                if (isPlayingStreamedMediaItem) {
                    isBuffering = false
                    // callback?.onVideoBuffering(false)
                }

                exoPlayer.currentMediaItem?.mediaId?.let {
                    ready[it] = true
                    if (exoPlayer.currentPosition == 0L)
                        restarts[it] = restarts.getOrDefault(it, 0) + 1
                    if ((restarts[it] ?: 0) > 2) {
                        skip()
                    }
                }
            }
            Player.STATE_BUFFERING -> {
                if (isPlayingStreamedMediaItem) {
                    isBuffering = true
                }
            }
            Player.STATE_IDLE -> Unit
        }
    }

    override fun onPlayerError(error: PlaybackException) {
        currentErrorCallback?.let { it() }
        // callback?.onCurrentPlayerItemChanged(currentVideoFileName)
    }

    override fun onMediaItemTransition(mediaItem: MediaItem?, reason: Int) {
        currentVideoFileName = mediaItem?.mediaId

        mediaItem?.mediaId?.let {
            ready[it] = false
            restarts[it] = 0
            Thread {
                sleep(3000)
                if (ready[it] == false && !(it.contains("idle") || it.contains("listening") || it.contains("firstcontact"))) {
                    onPlaybackEnded(true)
                }
            }.start()
        }
    }

    private fun onPlaybackEnded(mainThread: Boolean = false) {
        ready.clear()
        restarts.clear()
        if (videoQueue.isEmpty()) {
            if (currentStreamedMediaItem != null) {
                isPlayingStreamedMediaItem = false
                currentStreamedMediaItem = null
            }
            runOnMainLooper {
                exoPlayer.clearMediaItems()
            }
            // callback?.onCurrentPlayerItemChanged(currentVideoFileName)
            return
        }

        if (mainThread) {
            runOnMainLooper {
                continuePlaying()
            }
        } else {
            continuePlaying()
        }
    }

    private fun continuePlaying() {
        exoPlayer.clearMediaItems()
        val mediaItem = videoQueue.removeFirstOrNull()
        mediaItem?.let {
            exoPlayer.addMediaItem(it)
            exoPlayer.seekToDefaultPosition()
            exoPlayer.prepare()
            exoPlayer.play()
        }
    }

    private var currentVideoFileName: String? = null
        //exoPlayer.currentMediaItem?.localConfiguration?.uri?.toString()

    private fun clearItemsInQueue() {
        runOnMainLooper {
            val queueSize = exoPlayer.mediaItemCount
            val currentItemIndex = exoPlayer.currentMediaItemIndex

            for (index in queueSize downTo 0) {
                if (index != currentItemIndex) {
                    exoPlayer.removeMediaItem(index)
                }
            }
        }
    }

    companion object {
        const val TargetBufferBytes = -1
        const val BufferDurationsMs = 100
        const val AllocationSize = 2 * 1024 * 1024
        const val StreamedVideoStartDelayMs: Long = 450
        const val IsPlayingDelayMs: Long = 100
    }
}