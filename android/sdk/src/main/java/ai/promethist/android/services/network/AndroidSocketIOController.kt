package ai.promethist.android.services.network

import ai.promethist.client.Client
import ai.promethist.client.channel.ClientChannelSocketIOCallback
import ai.promethist.client.channel.ClientChannelSocketIOController
import ai.promethist.client.channel.ClientSessionConfig
import io.socket.client.IO
import io.socket.client.Socket
import kotlinx.coroutines.delay

class AndroidSocketIOController: ClientChannelSocketIOController {

    private var callback: ClientChannelSocketIOCallback? = null
    private lateinit var socket: Socket
    val config = Client.appConfig()
    val authClient = Client.authClient()

    override fun setup() {
        socket = IO.socket(config.preset.url + "/socket/io/pipeline")
        registerHandlers()
    }

    private fun registerHandlers() = socket.apply {
        on("text") { args ->
            val payload = args.getOrNull(0) as? String ?: return@on
            println("[SocketIO] text: $payload")
            callback?.onTextReceived(payload = payload)
        }
        on(Socket.EVENT_CONNECT) {
            println("[SocketIO] connected")
        }
        on(Socket.EVENT_DISCONNECT) {
            println("[SocketIO] disconnect")
        }
        on(Socket.EVENT_CONNECT_ERROR) { args ->
            val errorMessage = args.firstOrNull() as? String
            if (errorMessage != null) {
                println("[SocketIO] error: $errorMessage")
                callback?.onErrorReceived(message = errorMessage)
            } else {
                args.forEach {
                    println("[SocketIO] unknown error: $it")
                }
                callback?.onErrorReceived(message = null)
            }
        }
    }

    override suspend fun emit(input: String) {
        println("[SocketIO] emit: $input")
        socket.emit("text", input)
    }

    override suspend fun connect(config: ClientSessionConfig) {
        if (socket.connected()) {
            println("[SocketIO] skipping refresh")
            return
        }
        println("[SocketIO] refreshing session..")
        val authorizationTokenMap = if (authClient.isLoggedIn())
            mapOf("Authorization" to listOf("Bearer ${authClient.getAccessToken()}"))
        else
            mapOf()

        val options = IO.Options().apply {
            reconnection = true
            timeout = 10000
            query = "key=${this@AndroidSocketIOController.config.preset.key}"
            extraHeaders = config
                .headers
                .mapValues { listOf(it.value) }
                .plus(authorizationTokenMap)
        }
        socket.off()
        socket.disconnect()
        socket = IO.socket(this.config.preset.url + "/socket/io/pipeline", options)
        registerHandlers()

        socket.connect()
        var retryCount = 0
        while (!socket.connected() && retryCount < 200) {
            println("[SocketIO] connecting: ${socket.id()}")
            delay(50L)
            retryCount++
        }
    }

    override suspend fun disconnect() {
        if (!socket.connected()) {
            println("[SocketIO] skipping close")
            return
        }
        println("[SocketIO] close")
        socket.disconnect()
    }

    override fun registerCallback(callback: ClientChannelSocketIOCallback) {
        this.callback = callback
    }

    override fun unregisterCallback() {
        this.callback = null
    }
}