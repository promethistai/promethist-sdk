package ai.promethist.android.ext

import ai.promethist.client.common.AppConfig

val AppConfig.onboardingTermsAccepted: Boolean get() =
    this.termsAndConditionsAccepted && this.dataProtectionAccepted