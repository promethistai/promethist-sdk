package ai.promethist.android.presentation.components

import ai.promethist.android.R
import androidx.compose.foundation.layout.*
import androidx.compose.material3.Button
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp

@Composable
fun SessionEndMenu(
    hasRating: Boolean,
    ratingTitle: String?,
    onRatingSubmitted: (Int) -> Unit,
    onButtonClicked: () -> Unit
){
    var rating by remember { mutableStateOf( 0) }
    var ratingEnabled by remember { mutableStateOf( true) }
    Column {
        ratingTitle?.let {
            Text(
                text = it,
                modifier = Modifier
                    .padding(vertical = 16.dp, horizontal = 8.dp),
                style = MaterialTheme.typography.titleLarge
            )
        }
        if (hasRating) {
            RatingBar(
                rating = rating,
                enabled = ratingEnabled,
                onRatingChanged = {
                    rating = it
                    ratingEnabled = false
                    onRatingSubmitted(it)
                }
            )
        }
        Button(
            modifier = Modifier
                .fillMaxWidth()
                .padding(16.dp),
            onClick = { onButtonClicked() }
        ) {
            Text(
                text = "Back to menu",
                style = MaterialTheme.typography.labelLarge,
                color = Color.White
            )
        }
    }
}

@Composable
fun RatingBar(
    rating: Int,
    enabled: Boolean,
    maxRating: Int = 5,
    onRatingChanged: (Int) -> Unit
) {
    Row(
        modifier = Modifier.fillMaxWidth(),
        horizontalArrangement = Arrangement.Center
    ) {
        for (i in 1..maxRating) {
            IconButton(onClick = {
                if (enabled)
                    onRatingChanged(i)
            }) {
                Icon(
                    painter = painterResource(
                        if (i <= rating) R.drawable.baseline_star_24 else R.drawable.baseline_star_border_24
                    ),
                    contentDescription = "Star",
                    modifier = Modifier.size(48.dp),
                    tint = Color(0xFFFFD700)
                )
            }
        }
    }
}
