package ai.promethist.android.presentation.components.dialog

import ai.promethist.android.ext.openPermissionSettings
import ai.promethist.android.ext.stringResource
import ai.promethist.client.resources.Resources
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.window.DialogProperties

@Composable
fun PermissionDialog() {
    val context = LocalContext.current
    val properties = DialogProperties(
        dismissOnBackPress = false,
        dismissOnClickOutside = false,
        usePlatformDefaultWidth = false
    )
    AlertDialog(modifier = Modifier
        .padding(8.dp)
        .fillMaxWidth(0.92F),
        properties = properties,
        onDismissRequest = {},
        confirmButton = {
            TextButton(onClick = { openPermissionSettings(context) }) {
                Text(stringResource(res = Resources.strings.settings.openPermissionSettings))
            }
        },
        text = {
            Column(
                modifier = Modifier.verticalScroll(rememberScrollState())
            ) {
                Text(
                    text = stringResource(res = Resources.strings.settings.permissionsNeededTitle),
                    modifier = Modifier.padding(top = 8.dp, bottom = 4.dp),
                    style = MaterialTheme.typography.titleLarge,
                    fontWeight = FontWeight.Bold,
                    fontSize = 18.sp
                )
                Text(
                    text = stringResource(res = Resources.strings.settings.permissionsNeededText),
                    modifier = Modifier.padding(top = 8.dp, bottom = 4.dp),
                    fontSize = 14.sp
                )
            }
        })
}