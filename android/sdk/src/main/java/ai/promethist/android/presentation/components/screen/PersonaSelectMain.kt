package ai.promethist.android.presentation.components.screen

import ai.promethist.android.ext.painterResource
import ai.promethist.android.presentation.components.button.CircularButton
import ai.promethist.android.style.SpaceMediumLarge
import ai.promethist.android.style.SpaceXXLarge
import ai.promethist.client.Client
import ai.promethist.client.channel.ClientChannel
import ai.promethist.client.channel.getApplicationInfo
import ai.promethist.client.channel.syncAvatarManifest
import ai.promethist.client.model.ApplicationInfo
import ai.promethist.client.model.Message
import ai.promethist.client.resources.Resources
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import kotlinx.coroutines.launch

@Composable
fun PersonaSelectMain(
    channel: ClientChannel,
    onLoaded: () -> Unit,
    onLogout: () -> Unit,
    onClick: (String, String) -> Unit
) {
    val scope = rememberCoroutineScope()

    var applicationInfo by remember {
        mutableStateOf<ApplicationInfo?>(
            null
        )
    }
    LaunchedEffect(Unit) {
        scope.launch {
            if (Client.appConfig().avatarManifest.avatars.isEmpty())
                channel.syncAvatarManifest()
            applicationInfo = channel.getApplicationInfo()
            onLoaded()
        }
    }

    var messages by remember {
        mutableStateOf<List<Message>?>(null)
    }
    if (messages == null){
        PersonaSelectScreen(
            personas = applicationInfo?.personas ?: listOf(),
            onHistoryOpened = {
                messages = it
            },
            onLogout = onLogout,
            onStartButtonClick = { ref, avatarId ->
                onClick(ref, avatarId)
            }
        )
    } else {
        Box(
            modifier = Modifier.fillMaxSize()
        ) {
            messages?.let {
                TextView(messages = it)
            }
            CircularButton(
                modifier = Modifier
                    .align(Alignment.TopStart)
                    .padding(top = SpaceXXLarge, start = SpaceMediumLarge),
                isActive = false,
                painter = painterResource(res = Resources.images.icClose)
            ) {
                messages = null
            }
        }
    }
}