package ai.promethist.android.presentation.components.button

import ai.promethist.android.R
import ai.promethist.android.presentation.components.ShadowIcon
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.IconButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import coil.compose.AsyncImage

@Composable
fun PersonaMenuButton(
    modifier: Modifier = Modifier,
    painter: Painter,
    avatarUrl: String? = null,
    onClick: () -> Unit
) {
    IconButton(
        modifier = modifier
            .size(ButtonSizeDP.dp),
        onClick = onClick,
    ) {
        if (avatarUrl == null) {
            ShadowIcon(
                painter = painter,
                modifier = Modifier.fillMaxSize(),
                contentDescription = stringResource(R.string.content_desc_button_icon),
                tint = Color.White
            )
        } else {
            AsyncImage(
                modifier = Modifier
                    .fillMaxSize()
                    .clip(RoundedCornerShape((ButtonSizeDP / 2).dp)),
                contentScale = ContentScale.Crop,
                model = avatarUrl,
                contentDescription = null
            )
        }
    }
}

private const val ButtonSizeDP = 48