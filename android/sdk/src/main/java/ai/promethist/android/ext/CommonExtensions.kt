package ai.promethist.android.ext

import android.os.Handler
import android.os.Looper

private val mainLooperHandler = Handler(Looper.getMainLooper())

fun runOnMainLooper(transform: () -> Unit = {}) {
    mainLooperHandler.post(transform)
}