package ai.promethist.android.presentation

import ai.promethist.android.presentation.components.screen.SettingsViewV2
import ai.promethist.android.style.ElysaiAndroidTheme
import ai.promethist.client.model.Appearance
import ai.promethist.client.Client
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.runtime.*
import androidx.core.content.ContextCompat
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.booleanPreferencesKey
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.preferencesDataStore
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.firstOrNull
import kotlinx.coroutines.launch

class SettingsActivity : ComponentActivity(), SettingsScreenCallback {

    companion object {
        val ChangePersonaResultCode = 555
        val StartSOSConversationResultCode = 112
        val StartFeedbackConversationResultCode = 642
        val ShouldResetResultCode = 330
        val ShouldSendReportResultCode = 213
        val ShouldLogoutCode = 709
        val Context.dataStore: DataStore<Preferences> by preferencesDataStore(name = "settings")
        fun <T> Context.getDatastorePreference(key: Preferences.Key<T>, callback: (T?) -> Unit) {
            val scope = CoroutineScope(Dispatchers.Main)
            scope.launch {
                callback(dataStore.data.firstOrNull()!![key])
            }
        }
    }

    val screenMode = mutableStateOf(Appearance.System)

    override fun setAppearance(newAppearance: Appearance) {
        screenMode.value = newAppearance
    }

    override fun openBrowser(url: String) {
        val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
        ContextCompat.startActivity(this, browserIntent, null)
    }

    override fun indicateShouldReset() {
        this.setResult(ShouldResetResultCode)
    }

    private fun restartApp() = with(this) {
        // From https://stackoverflow.com/a/46848226
        val intent = packageManager.getLaunchIntentForPackage(packageName)
        val componentName = intent?.component
        componentName?.let {
            val mainIntent = Intent.makeRestartActivityTask(it)
            mainIntent.setPackage(packageName)
            startActivity(mainIntent)
        }
        Runtime.getRuntime().exit(0)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        lifecycleScope.launch {
            dataStore.edit { preferences ->
                preferences[booleanPreferencesKey("prefBackToMenu")] = Client.appConfig().showExitConversationAlert
            }
        }
        screenMode.value = Client.appConfig().appearance
        setContent {
            val darkMode by remember { screenMode }
            ElysaiAndroidTheme(darkMode) {
//                SettingsView(
//                    callback = this,
//                    config = Client.appConfig(),
//                    onClose = {
//                        this.setResult(it)
//                        this.finish()
//                    },
//                    onBackClick = ::onBackPressed
//                )

                SettingsViewV2(
                    callback = this,
                    onBackClick = ::onBackPressed,
                    onClose = {
                        this.setResult(it)
                        finish()
                    }
                )
            }
        }
    }
}