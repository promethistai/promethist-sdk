package ai.promethist.android.services.speechRecognizer

import android.content.Intent
import android.speech.RecognizerIntent

object SpeechRecognizerIntentFactory {

    private const val MaxResults = 5
    private const val DefaultLang = "en_US"
    private const val ENABLE_FORMATTING = "android.speech.extra.ENABLE_FORMATTING"
    private const val MASK_OFFENSIVE_WORDS = "android.speech.extra.MASK_OFFENSIVE_WORDS"

    fun create(lang: String = DefaultLang): Intent =
        Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH).apply {
            putExtra(
                RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM
            )
            putExtra(RecognizerIntent.EXTRA_PARTIAL_RESULTS, true)
            putExtra(ENABLE_FORMATTING, true)
            putExtra(MASK_OFFENSIVE_WORDS, true)
            putExtra(RecognizerIntent.EXTRA_LANGUAGE, lang)
            putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, MaxResults)
        }
}