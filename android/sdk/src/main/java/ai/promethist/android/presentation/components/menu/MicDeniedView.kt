package ai.promethist.android.presentation.components.menu

import ai.promethist.SharedRes
import ai.promethist.android.ext.accentSecondary
import ai.promethist.android.ext.openPermissionSettings
import ai.promethist.android.ext.painterResource
import ai.promethist.android.ext.regularMaterial
import ai.promethist.android.presentation.components.text.TitleText
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp

@Composable
fun MicDeniedView(
    onClick: () -> Unit
) {
    val context = LocalContext.current
    Box (
        modifier = Modifier
            .fillMaxSize()
            .background(regularMaterial())
            .clickable(false) {}
    ) {
        Column(
            modifier = Modifier
                .align(Alignment.Center),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            TitleText(
                modifier = Modifier,
                title = SharedRes.strings.mic_permission_denied.getString(context),
                textAlign = TextAlign.Center
            )
            Box (
                modifier = Modifier
                    .padding(top = 16.dp, bottom = 32.dp)
            ) {
                Image(
                    painter = painterResource(res = SharedRes.images.ic_mic_denied),
                    contentDescription = "Mic denied icon",
                    modifier = Modifier
                        .size(80.dp)
                )
            }
            Button(
                onClick = { openPermissionSettings(context) },
                colors = ButtonDefaults.buttonColors(containerColor = accentSecondary())
            ) {
                Text(
                    text = SharedRes.strings.settings_open_permissions.getString(context),
                    modifier = Modifier.padding(horizontal = 24.dp),
                    style = MaterialTheme.typography.titleMedium
                )
            }
            Button(
                onClick = { onClick() },
                colors = ButtonDefaults.buttonColors(containerColor = accentSecondary())
            ) {
                Text(
                    text = SharedRes.strings.error_retry.getString(context),
                    modifier = Modifier.padding(horizontal = 24.dp),
                    style = MaterialTheme.typography.titleMedium
                )
            }
        }
    }
}