package ai.promethist.android.presentation.components

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.offset
import androidx.compose.material3.Icon
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.blur
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.unit.dp

@Composable
fun ShadowIcon(
    modifier: Modifier = Modifier,
    painter: Painter,
    contentDescription: String?,
    tint: Color
) {
    Box(
        contentAlignment = Alignment.Center
    ) {
        Icon(
            modifier = modifier
                .offset(x = 1.dp, y = 1.dp)
                .blur(radius = 1.dp),
            painter = painter,
            contentDescription = null,
            tint = Color.Black.copy(alpha = 0.2f)
        )

        Icon(
            modifier = modifier,
            painter = painter,
            contentDescription = contentDescription,
            tint = tint
        )
    }
}