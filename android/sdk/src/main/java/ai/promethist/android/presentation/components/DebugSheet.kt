package ai.promethist.android.presentation.components

import ai.promethist.android.presentation.components.button.OptionButton
import ai.promethist.android.style.SpaceSmall
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.ModalBottomSheet
import androidx.compose.material3.SheetState
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext

@OptIn(ExperimentalMaterial3Api::class, ExperimentalFoundationApi::class)
@Composable
fun DebugSheet(
    log: String,
    onDismissRequest: () -> Unit,
    state: SheetState
) {
    ModalBottomSheet(
        sheetState = state,
        onDismissRequest = { onDismissRequest() },
    ) {
        val context = LocalContext.current
        Column(modifier = Modifier
                .padding(SpaceSmall)
        ) {
            OptionButton(text = "Copy", onClick = {
                val clipboard =
                    context.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
                val clip: ClipData = ClipData.newPlainText("Log", log)
                clipboard.setPrimaryClip(clip)
            })
        }
        TextField(
            value = log,
            onValueChange = {},
            readOnly = true,
        )
    }
}