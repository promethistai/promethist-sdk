package ai.promethist.android.presentation.components.picker

import ai.promethist.android.presentation.components.text.ShadedText
import ai.promethist.channel.model.PersonaModule
import ai.promethist.client.utils.PersonaResourcesManager
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.BoxWithConstraints
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.AsyncImage

@Composable
fun ModuleRowPicker(
    modifier: Modifier,
    modules: List<PersonaModule>,
    onClick: (PersonaModule) -> Unit
) {
    BoxWithConstraints(
        modifier = Modifier.fillMaxSize()
    ) {
        val screenWidth = maxWidth

        Row(
            modifier = modifier
                .wrapContentHeight()
                .fillMaxWidth()
                .align(Alignment.BottomCenter)
                .padding(horizontal = 56.dp),
            horizontalArrangement = Arrangement.Center
        ) {
            modules.forEach {
                Button(
                    contentPadding = PaddingValues(0.dp),
                    onClick = { onClick(it) },
                    modifier = Modifier
                        .wrapContentHeight()
                        .weight(1F)
                        .padding(0.dp),
                    colors = ButtonDefaults.buttonColors(containerColor = Color.Transparent),
                    shape = RoundedCornerShape(12.dp)
                ) {
                    Column (
                        horizontalAlignment = Alignment.CenterHorizontally
                    ) {
                        AsyncImage(
                            modifier = Modifier
                                .size(200.dp),
                            model = "",
                            contentDescription = "Module icon",
                            contentScale = ContentScale.Fit
                        )
                        /*Image(
                            painter = painterResource(id = R.drawable.icon_entertainment_poppy),
                            contentDescription = "Module icon"
                        )*/

                        ShadedText(
                            modifier = Modifier,
                            personaText = it.name,
                            textSize = 16.sp
                        )
                    }
                }
            }
        }
    }
}