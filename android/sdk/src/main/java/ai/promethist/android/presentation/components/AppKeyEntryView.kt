package ai.promethist.android.presentation.components

import ai.promethist.android.ext.painterResource
import ai.promethist.android.ext.regularMaterial
import ai.promethist.android.ext.stringResource
import ai.promethist.android.presentation.components.input.UserTextInput
import ai.promethist.android.presentation.components.util.rememberKeyboardPaddingState
import ai.promethist.android.style.SpaceLarge
import ai.promethist.android.style.SpaceMedium
import ai.promethist.client.AppPreset
import ai.promethist.client.Client
import ai.promethist.client.model.PersonaId
import ai.promethist.client.utils.PersonaResourcesManager
import ai.promethist.client.resources.Resources
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Button
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.unit.dp

@Deprecated("This component is for V2", ReplaceWith("SetupNameEntryView"))
@Composable
fun AppKeyEntryView(
    onContinue: () -> Unit
) {
    val appConfig = remember { Client.appConfig() }
    val personaId = PersonaId.fromAvatarId(appConfig.primaryPersonaId ?: "")
    val keyboardPaddingState by rememberKeyboardPaddingState()
    var invalidAppKeyAlert by remember { mutableStateOf(false) }

    fun isValidAppKey(input: String): Boolean = try {
        input.matches(Regex("^[A-Za-z0-9:]+$"))
    } catch (e: Exception) {
        false
    }

    Box(
        modifier = Modifier
            .fillMaxSize()
            .padding(bottom = keyboardPaddingState)
    ) {
        Image(
            painter = painterResource(
                res = PersonaResourcesManager.getBackground(personaId)
            ),
            contentDescription = null,
            contentScale = ContentScale.Crop
        )

        Card(
            modifier = Modifier.align(Alignment.BottomCenter),
            shape = RoundedCornerShape(topStart = sheetCornerRadius, topEnd = sheetCornerRadius),
            colors = CardDefaults.cardColors(containerColor = regularMaterial())
        ) {
            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(SpaceLarge)
                    .padding(bottom = bottomOffset),
                verticalArrangement = Arrangement.spacedBy(SpaceMedium),
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                Image(
                    modifier = Modifier.height(logoHeight),
                    painter = painterResource(res = Resources.images.splashLogo),
                    contentDescription = null
                )

                Text(text = stringResource(res = Resources.strings.onboarding.insertYourAppKey))

                UserTextInput(
                    enabled = true,
                    initialText = appConfig.engineAppKey,
                    onSubmit = {
                        if (!isValidAppKey(it)) {
                            invalidAppKeyAlert = true
                            return@UserTextInput
                        }
                        appConfig.engineAppKey = it
                        appConfig.preset = AppPreset.Engine
                        onContinue()
                    }
                )
            }
        }
    }

    if (invalidAppKeyAlert) {
        InvalidAppKeyAlert {
            invalidAppKeyAlert = false
        }
    }
}

private val logoHeight = 50.dp
private val sheetCornerRadius = 36.dp
private val bottomOffset = 84.dp

@Composable
private fun InvalidAppKeyAlert(
    onDismiss: () -> Unit
) {
    AlertDialog(
        onDismissRequest = onDismiss,
        title = {
            Text(text = stringResource(res = Resources.strings.onboarding.invalidAppKey))
        },
        text = {
            Text(stringResource(res = Resources.strings.onboarding.invalidAppKey))
        },
        confirmButton = {
            Button(
                onClick = onDismiss) {
                Text("OK")
            }
        }
    )
}