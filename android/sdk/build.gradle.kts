plugins {
    id("com.android.library")
    id("org.jetbrains.kotlin.android")
    id("maven-publish")
}

description = "Promethist Android SDK"

android {
    namespace = "ai.promethist.android"
    compileSdk = 34
    defaultConfig {
        minSdk = 29
    }
    buildFeatures {
        compose = true
    }
    composeOptions {
        kotlinCompilerExtensionVersion = "1.5.3"
    }

    buildTypes {
        release {
            isMinifyEnabled = false
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }
    repositories {
        google()
        mavenLocal()
        mavenCentral()
        maven {
            name = "Flowstorm"
            url = uri("https://gitlab.com/api/v4/projects/23512224/packages/maven")
            credentials {
                System.getenv("GITLAB_PRIVATE_TOKEN")?.let { token ->
                    username = "Private-Token"
                    password = token
                }
                System.getenv("CI_JOB_TOKEN")?.let { token ->
                    username = "gitlab-ci-token"
                    password = token
                }
            }
        }
        maven { url = uri("https://jitpack.io") }
    }

    packagingOptions(action = Action {
        exclude("META-INF/*")
    })
}

dependencies {
    api(project(":promethist-shared"))

    implementation("androidx.core:core-ktx:1.13.1")
    implementation("androidx.appcompat:appcompat:1.6.1")
    implementation("androidx.compose.ui:ui:1.4.3")
    implementation("androidx.compose.ui:ui-tooling:1.4.3")
    implementation("androidx.compose.ui:ui-tooling-preview:1.4.3")
    implementation("androidx.compose.foundation:foundation:1.4.3")
    implementation("androidx.compose.material:material:1.3.1")
    implementation("androidx.compose.material3:material3:1.1.0-rc01")
    implementation("androidx.activity:activity-compose:1.7.2")
    implementation("androidx.media3:media3-exoplayer:1.2.0")
    implementation("androidx.media3:media3-exoplayer-dash:1.2.0")
    implementation("androidx.media3:media3-ui:1.2.0")
    implementation("androidx.lifecycle:lifecycle-viewmodel-compose:2.6.1")

    implementation("io.coil-kt:coil-compose:2.2.2")

    implementation("androidx.paging:paging-compose:3.2.1")

    implementation("androidx.wear.compose:compose-foundation:1.1.2")

    // For Wear Material Design UX guidelines and specifications
    implementation("androidx.wear.compose:compose-material:1.1.2")
    implementation("com.google.android.material:material:1.6.0")
    implementation("androidx.datastore:datastore-preferences:1.0.0")
    implementation("com.github.JamalMulla:ComposePrefs3:1.0.4")
    implementation("androidx.work:work-runtime-ktx:2.8.1")
    implementation("androidx.camera:camera-core:1.3.3")
    implementation("androidx.camera:camera-view:1.3.3")
    implementation("androidx.camera:camera-lifecycle:1.3.3")
    implementation("androidx.camera:camera-camera2:1.3.3")
    implementation("org.jetbrains.kotlinx:kotlinx-datetime:0.4.0")
    implementation("androidx.core:core-splashscreen:1.0.1")
    implementation("net.gotev:speech:1.6.2")
    implementation("com.journeyapps:zxing-android-embedded:4.1.0")
    implementation("com.google.zxing:core:3.5.0")
    implementation("com.airbnb.android:lottie-compose:6.6.2")
    implementation("io.ktor:ktor-client-okhttp:2.3.10")

    // Unity
    // Use compileOnly while library publishing
    implementation(project(":unityLibrary"))
    implementation(fileTree(mapOf("dir" to "android/Unity/unityLibrary" + "\\libs", "include" to listOf("*.jar"))))
    implementation(files("../Unity/unityLibrary/libs/unity-classes.jar"))

    // Google STT
    implementation("com.google.auth:google-auth-library-oauth2-http:0.26.0")
    implementation("io.grpc:grpc-stub:1.39.0")
    implementation("com.google.cloud:google-cloud-speech:1.29.1")
    implementation("io.grpc:grpc-okhttp:1.38.1")

    // Google WebRTC
    //implementation("com.infobip:google-webrtc:1.0.0035529")
    implementation("com.github.gkonovalov.android-vad:silero:2.0.7")

    implementation("io.socket:socket.io-client:2.0.0") {
        exclude(group = "org.json", module = "json")
    }
}

project.afterEvaluate {
    publishing {
        publications {
            create<MavenPublication>("promethist-android-sdk") {
                groupId = "ai.promethist"
                artifactId = "promethist-android-sdk"
                from(components["release"])
            }
        }
        repositories {
            maven {
                url = uri(findProperty("maven.repository.uri") as String)
            }
        }
    }
}