package ai.promethist.android

import ai.promethist.client.ClientTargetPreset

val target: ClientTargetPreset get() {
    val packageName = MainApplication.instance.packageName
    return when (packageName) {
        "ai.flowstorm.poppy" -> ClientTargetPreset.Elysai
        "ai.promethist.dfha" -> ClientTargetPreset.DFHA
        "ai.promethist.tmobile" -> ClientTargetPreset.TMobile
        "ai.promethist.tmobilev3" -> ClientTargetPreset.TMobileV3
        "ai.promethist.gaia" -> ClientTargetPreset.Gaia
        else -> ClientTargetPreset.Promethist
    }
}