package ai.promethist.android

import ai.promethist.util.AndroidApplication
import android.app.Application

class MainApplication: Application() {

    override fun onCreate() {
        super.onCreate()
        instance = this
        AndroidApplication.application = this
    }

    companion object {
        lateinit var instance: MainApplication
            private set
    }
}