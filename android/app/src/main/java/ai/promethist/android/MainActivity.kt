package ai.promethist.android

import ai.promethist.android.ext.addFlagLayoutNoLimit
import ai.promethist.android.services.AndroidScannerController
import ai.promethist.android.services.speechRecognizer.duplex.DuplexAndroidSpeechRecognizer
import ai.promethist.android.utils.AppEvent
import ai.promethist.android.utils.AppEventBus
import ai.promethist.client.AppPreset
import ai.promethist.client.Client
import ai.promethist.client.ClientEvent
import ai.promethist.video.VideoMode
import android.Manifest
import android.animation.ObjectAnimator
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.media.AudioManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.view.animation.DecelerateInterpolator
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.runtime.remember
import androidx.core.animation.doOnEnd
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen
import androidx.core.view.WindowCompat
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MainActivity : ComponentActivity() {

    private val promethist = Promethist(
        target = target
    )

    private lateinit var promethistViewModel: PromethistViewModel

    private val permissionLauncher =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) {}

    override fun onCreate(savedInstanceState: Bundle?) {
        WindowCompat.setDecorFitsSystemWindows(window, true)
        super.onCreate(savedInstanceState)
        installSplashScreenWithAnimation()
        addFlagLayoutNoLimit()
        val appLinkOpened = handleAppLink(intent)
        promethist.onCreate(this)
        promethistViewModel = PromethistViewModel(
            appLinkOpened = appLinkOpened,
            isHomeScreenEnabled = promethist.viewModel.config.isHomeScreenEnabled,
            onReset = {
                if (Client.appConfig().videoMode != VideoMode.OnDeviceRendering || !promethist.viewModel.config.isHomeScreenEnabled)
                    promethist.viewModel.handleEvent(ClientEvent.Reset(true))
            }
        )
        requestPermissionsIfNeeded()
        setContent {
            PromethistView(
                promethist = promethist,
                remember { promethistViewModel }
            )
        }
    }

    // Workaround setting volume
    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if (!Client.appConfig().duplexEnabled) {
            return super.onKeyDown(keyCode, event)
        }

        if (keyCode == KeyEvent.KEYCODE_VOLUME_UP || keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) {
            android.util.Log.d(DuplexAndroidSpeechRecognizer.TAG, "Volume: $keyCode")
            val audioManager = getSystemService(Context.AUDIO_SERVICE) as AudioManager
            val direction = if (keyCode == KeyEvent.KEYCODE_VOLUME_UP) AudioManager.ADJUST_RAISE else AudioManager.ADJUST_LOWER
            audioManager.adjustStreamVolume(AudioManager.STREAM_MUSIC, direction, AudioManager.FLAG_SHOW_UI)
            return true
        }
        return super.onKeyDown(keyCode, event)
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        intent?.let { handleAppLink(it) }
    }

    override fun onPause() {
        super.onPause()
        promethist.onPause(this)
    }

    override fun onResume() {
        super.onResume()
        promethist.onResume(this)
    }

    override fun onStart() {
        super.onStart()
        promethist.onStart()
    }

    override fun onStop() {
        promethist.onStop(this)
        super.onStop()
    }

    override fun onDestroy() {
        super.onDestroy()
        promethist.onDestroy()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        promethist.onLowMemory()
    }

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)
        promethist.onWindowFocusChanged(hasFocus)
    }

    private fun handleAppLink(intent: Intent): Boolean {
        val data: Uri? = intent.data
        data?.let {
            Client.appConfig().engineSetupName = it.lastPathSegment ?: return false
            Client.appConfig().engineDomain = "https://" + it.host
            Client.appConfig().preset = AppPreset.EngineV3
            return true
        }
        return false
    }

    private fun requestPermissionsIfNeeded() {
        if (checkSelfPermission(Manifest.permission.POST_NOTIFICATIONS) != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU)
                permissionLauncher.launch(Manifest.permission.POST_NOTIFICATIONS)
        }
    }

    private fun installSplashScreenWithAnimation() {
        installSplashScreen().apply {
            setOnExitAnimationListener { splashScreen ->
                ObjectAnimator.ofFloat(
                    splashScreen.view,
                    View.ALPHA,
                    AnimationAlphaStart, AnimationAlphaEnd
                ).apply {
                    interpolator = DecelerateInterpolator()
                    duration = AnimationDuration
                    doOnEnd { splashScreen.remove() }
                    start()
                }
            }
        }
    }

    @Deprecated("Deprecated in Java")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        AndroidScannerController.onResult(requestCode, resultCode, data) {
            it?.let { text ->
                listOf("promethist.ai", "ngrok-free.app").forEach { appDomain ->
                    val promethistMatch = createRegexMatch(appDomain, text)
                    promethistMatch?.let { result ->
                        val scheme = result.groups[1]?.value ?: return@onResult
                        if (scheme != getString(R.string.link_scheme))
                            return@onResult
                        val domain = "${result.groups[2]?.value ?: return@onResult}.$appDomain"
                        val setupName = result.groups[4]?.value ?: return@onResult
                        CoroutineScope(Dispatchers.Default).launch {
                            AppEventBus.emit(AppEvent.OnQRCodeRead(domain, setupName))
                        }
                    }
                }
            }
        }

    }

    private fun createRegexMatch(domain: String, text: String): MatchResult? {
        val domainRegex = domain.replace(".", "\\.")
        val regex = Regex("([A-Za-z0-9\\-]*)://([A-Za-z0-9\\-]*)\\.$domainRegex(/[A-Za-z0-9\\-]*)*/([a-z\\-_:0-9]*(\\.[0-9]+)?)(\\?.*)?")
        return regex.matchEntire(text)
    }

    companion object {
        const val AnimationDuration = 500L
        const val AnimationAlphaStart = 1f
        const val AnimationAlphaEnd = 0f
    }
}