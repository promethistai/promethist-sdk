plugins {
    id("com.android.application")
    kotlin("android")
}

description = "Promethist Android"

android {
    namespace = "ai.promethist.android"
    compileSdk = 34
    defaultConfig {
        applicationId = "ai.promethist.android"
        minSdk = 29
        targetSdk = 33
        versionCode = 1
        versionName = "1.0"

        /*ndk {
            abiFilters.add("armeabi-v7a")
            abiFilters.add("x86")
        }*/
    }
    buildFeatures {
        compose = true
        buildConfig = true
    }
    composeOptions {
        kotlinCompilerExtensionVersion = "1.5.3"
    }
//    packaging {
//        resources {
//            excludes += "/META-INF/{AL2.0,LGPL2.1}"
//        }
//    }

    packagingOptions(action = Action {
        exclude("META-INF/*")
    })

    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
        }
    }
    
    flavorDimensions += "target"
    productFlavors {
        create("promethist") {
            applicationId = "ai.promethist.android"
            dimension = "target"
            manifestPlaceholders["appLabel"] = "Promethist"
        }

        create("elysai") {
            applicationId = "ai.flowstorm.poppy"
            dimension = "target"
            manifestPlaceholders["appLabel"] = "Elysai"
        }

        create("dfha") {
            applicationId = "ai.promethist.dfha"
            dimension = "target"
            manifestPlaceholders["appLabel"] = "Digital Financial Health Advisor BETA"
        }

        create("tmobile") {
            applicationId = "ai.promethist.tmobile"
            dimension = "target"
            manifestPlaceholders["appLabel"] = "T-Mobile BETA"
        }

        create("tmobilev3") {
            applicationId = "ai.promethist.tmobilev3"
            dimension = "target"
            manifestPlaceholders["appLabel"] = "T-Asistent BETA"
        }

        create("gaia") {
            applicationId = "ai.promethist.gaia"
            dimension = "target"
            manifestPlaceholders["appLabel"] = "Gaia BETA"
        }
    }

    sourceSets.getByName("promethist") {
        setRoot("src/promethist")
    }

    sourceSets.getByName("elysai") {
        setRoot("src/elysai")
    }

    sourceSets.getByName("dfha") {
        setRoot("src/dfha")
    }

    sourceSets.getByName("tmobile") {
        setRoot("src/tmobile")
    }

    sourceSets.getByName("tmobilev3") {
        setRoot("src/tmobilev3")
    }

    sourceSets.getByName("gaia") {
        setRoot("src/gaia")
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }
    repositories {
        google()
        mavenLocal()
        mavenCentral()
        maven {
            name = "Flowstorm"
            url = uri("https://gitlab.com/api/v4/projects/23512224/packages/maven")
            credentials {
                System.getenv("GITLAB_PRIVATE_TOKEN")?.let { token ->
                    username = "Private-Token"
                    password = token
                }
                System.getenv("CI_JOB_TOKEN")?.let { token ->
                    username = "gitlab-ci-token"
                    password = token
                }
            }
        }
        maven { url = uri("https://jitpack.io") }
    }
}

dependencies {
    implementation(project(":promethist-android-sdk"))

    implementation("androidx.compose.ui:ui:1.4.3")
    implementation("androidx.compose.ui:ui-tooling:1.4.3")
    implementation("androidx.compose.ui:ui-tooling-preview:1.4.3")
    implementation("androidx.compose.foundation:foundation:1.4.3")
    implementation("androidx.compose.material:material:1.3.1")
    implementation("androidx.compose.material3:material3:1.1.0-rc01")
    implementation("androidx.activity:activity-compose:1.7.2")

    // For Wear Material Design UX guidelines and specifications
    implementation("androidx.wear.compose:compose-material:1.1.2")
    implementation("com.google.android.material:material:1.6.0")
    implementation("androidx.core:core-splashscreen:1.0.1")
}