#!/bin/bash
cd "$(dirname "$0")"

if [[ $1 =~ ^[0-9]+\.[0-9]+(\.[0-9])?$ ]]; then
  VER="$1"
  TAG="latest"
fi

if [[ $1 =~ ^[0-9]+\.[0-9]+$ ]]; then
  VER="$VER.0"
fi

if [[ $1 =~ ^[0-9]+$ ]]; then
  RAND=`tr -dc A-Za-z0-9 </dev/urandom | head -c 6 ; echo ''`
  VER="1.0.$1-SNAPSHOT.$RAND"
  TAG="$1-SNAPSHOT"
fi

if [[ $1 == "snapshot" ]]; then
  VER="1.0.0-$CI_COMMIT_SHORT_SHA"
  TAG="1.0.0-SNAPSHOT"
fi

echo "Version: $VER Tag: $TAG"

echo "@promethist:registry=https://${CI_SERVER_HOST}/api/v4/projects/${CI_PROJECT_ID}/packages/npm/">.npmrc
echo "//${CI_SERVER_HOST}/api/v4/projects/${CI_PROJECT_ID}/packages/npm/:_authToken=${CI_JOB_TOKEN}">>.npmrc
echo ".npmrc:"
cat .npmrc

find build -type d -ls

#yarn version --new-version $VER --no-git-tag-version
yarn pack
yarn publish promethist-shared-*.tgz --new-version $VER --tag $TAG --no-git-tag-version
