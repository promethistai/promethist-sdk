import org.jetbrains.kotlin.gradle.plugin.mpp.apple.XCFramework

plugins {
    kotlin("multiplatform")
    kotlin("plugin.serialization")
    id("com.android.library")
    id("maven-publish")
    id("dev.icerock.mobile.multiplatform-resources")
}

group = "ai.promethist"

val springVersion: String by project
val kotlinVersion: String by project
val ktorVersion: String by project
val otelVersion: String by project
val kodeinVersion: String by project
val kotlinxCliVersion: String by project
val kotlinxDateTimeVersion: String by project
val kotlinxSerializationVersion: String by project
val kotlinxCoroutinesVersion: String by project
val iosBuild: String by project
val jsBuild: String by project
val mongoDbVersion: String by project

@OptIn(org.jetbrains.kotlin.gradle.ExperimentalKotlinGradlePluginApi::class)
kotlin {
    targetHierarchy.default()

    androidTarget {
        compilations.all {
            kotlinOptions {
                jvmTarget = "1.8"
                freeCompilerArgs = listOf("-Xoverride-konan-properties=minVersion.ios=15.0;minVersionSinceXcode15.ios=15.0")
            }
        }
        publishLibraryVariants("release", "debug")
        publishLibraryVariantsGroupedByFlavor = true
    }

    if (jsBuild.toBoolean()) {
        js(IR) {
            binaries.executable()
            browser {
            }
            generateTypeScriptDefinitions()
        }
    }

    jvm {

    }

    if (iosBuild.toBoolean()) {
        val iosTargets = listOf(iosX64(), iosArm64(), iosSimulatorArm64())
        val xcf = XCFramework(xcFrameworkName = "shared")

        iosTargets.forEach {
            it.binaries.framework {
                baseName = "shared"
                freeCompilerArgs = listOf("-Xoverride-konan-properties=minVersion.ios=15.0;minVersionSinceXcode15.ios=15.0")
                export("dev.icerock.moko:resources:0.23.0")
                export("dev.icerock.moko:graphics:0.9.0")
                xcf.add(this)
            }
        }
    }

    sourceSets {
        all {
            languageSettings.apply {
                optIn("kotlin.js.ExperimentalJsExport")
            }
        }
        val commonMain by getting {
            dependencies {
                implementation("io.ktor:ktor-serialization-kotlinx-json:$ktorVersion")
                implementation("io.ktor:ktor-client-core:$ktorVersion")
                implementation("io.ktor:ktor-client-logging:$ktorVersion")
                implementation("io.ktor:ktor-client-content-negotiation:$ktorVersion")
                implementation("io.ktor:ktor-client-websockets:$ktorVersion")
                implementation("org.kodein.di:kodein-di:$kodeinVersion")
                implementation("org.kodein.di:kodein-di-conf:$kodeinVersion")
                implementation("org.jetbrains.kotlinx:kotlinx-datetime:$kotlinxDateTimeVersion")
                implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:$kotlinxSerializationVersion")
                api("dev.icerock.moko:resources:0.23.0")
                implementation("com.russhwolf:multiplatform-settings-no-arg:1.0.0")
                implementation("com.russhwolf:multiplatform-settings-serialization:1.0.0")
                implementation("de.peilicke.sascha:kase64:1.2.0")
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:$kotlinxCoroutinesVersion")
            }
        }

        val androidMain by getting {
            dependsOn(commonMain)
            dependencies {
                implementation("io.ktor:ktor-client-okhttp:$ktorVersion")
                implementation("io.opentelemetry:opentelemetry-sdk:$otelVersion")
                implementation("io.opentelemetry.instrumentation:opentelemetry-resources:$otelVersion-alpha")
                implementation("io.opentelemetry:opentelemetry-exporter-otlp:$otelVersion")
                implementation("dev.tmapps:konnection:1.3.0") {
                    //exclude("org.jetbrains.kotlinx", "kotlinx-coroutines-core")
                }
                implementation("com.liftric:kvault:1.12.0")
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-android:$kotlinxCoroutinesVersion")
            }
        }

        if (iosBuild.toBoolean()) {
            val iosMain by getting {
                dependsOn(commonMain)
                dependencies {
                    implementation("io.ktor:ktor-client-darwin:$ktorVersion")
                    implementation("dev.tmapps:konnection:1.3.0")
                    implementation("com.liftric:kvault:1.12.0")
                }
            }
        }

        val commonTest by getting {
            dependencies {
                implementation(kotlin("test"))
            }
        }

        val jvmMain by getting {
            dependsOn(commonMain)
            dependencies {
                implementation("io.opentelemetry:opentelemetry-sdk:$otelVersion")
                implementation("io.opentelemetry.instrumentation:opentelemetry-resources:$otelVersion-alpha")
                implementation("io.opentelemetry:opentelemetry-exporter-otlp:$otelVersion")
                implementation("io.ktor:ktor-client-okhttp:$ktorVersion")
                implementation("org.springframework:spring-web:$springVersion")
                implementation("org.springframework:spring-context:$springVersion")
                implementation("org.jetbrains.kotlinx:kotlinx-cli:$kotlinxCliVersion")
                implementation("org.jetbrains.kotlin:kotlin-reflect:$kotlinVersion")
                implementation("org.mongodb:mongodb-driver-kotlin-sync:$mongoDbVersion")
                implementation("org.mongodb:bson-kotlinx:4.11.0")
                implementation("com.google.cloud:google-cloud-speech:4.1.0")
                implementation("dev.tmapps:konnection:1.3.0") {
                    exclude("org.jetbrains.kotlinx", "kotlinx-coroutines-core")
                }
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:$kotlinxCoroutinesVersion")
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-swing:$kotlinxCoroutinesVersion")
            }
        }

        if (jsBuild.toBoolean()) {
            val jsMain by getting {
                dependsOn(commonMain)
                dependencies {
                    implementation("io.ktor:ktor-client-js:$ktorVersion")
                    implementation("org.jetbrains.kotlin-wrappers:kotlin-react:18.2.0-pre.346")
                    implementation("org.jetbrains.kotlin-wrappers:kotlin-react-dom:18.2.0-pre.346")
                    implementation("org.jetbrains.kotlin-wrappers:kotlin-emotion:11.9.3-pre.346")
                }
            }
        }
    }
}

tasks.withType<Test>().configureEach {
    useJUnitPlatform()
}

tasks.named<Jar>("jvmJar") {
    exclude("files/**")
    exclude("images/**")
}

android {
    namespace = "ai.promethist"
    compileSdk = 33
    defaultConfig {
        minSdk = 29
    }
}

project.afterEvaluate {
    tasks.named("androidDebugSourcesJar") {
        dependsOn("generateMRandroidMain")
    }
    tasks.named("androidReleaseSourcesJar") {
        dependsOn("generateMRandroidMain")
    }
    publishing {
        repositories {
            maven {
                url = uri(findProperty("maven.repository.uri") as String)
            }
        }
    }
}

//if (iosBuild) {
//    val packForXcode by tasks.creating(Sync::class) {
//        group = "build"
//        val mode = System.getenv("CONFIGURATION") ?: "DEBUG"
//        val sdkName = System.getenv("SDK_NAME") ?: "iphonesimulator"
//        val targetName = "ios" + if (sdkName.startsWith("iphoneos")) "Arm64" else "X64"
//        val framework =
//            kotlin.targets.getByName<KotlinNativeTarget>(targetName).binaries.getFramework(mode)
//        inputs.property("mode", mode)
//        dependsOn(framework.linkTask)
//        val targetDir = File(buildDir, "xcode-frameworks")
//        from({ framework.outputDirectory })
//        into(targetDir)
//    }
//
//    tasks.getByName("build").dependsOn(packForXcode)
//}

multiplatformResources {
    multiplatformResourcesPackage = "ai.promethist"
    multiplatformResourcesClassName = "SharedRes"
}