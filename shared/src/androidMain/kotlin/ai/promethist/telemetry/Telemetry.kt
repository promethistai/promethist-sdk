package ai.promethist.telemetry

import ai.promethist.util.LoggerDelegate
import io.opentelemetry.api.OpenTelemetry
import io.opentelemetry.exporter.otlp.http.trace.OtlpHttpSpanExporter
import io.opentelemetry.sdk.OpenTelemetrySdk
import io.opentelemetry.sdk.resources.Resource
import io.opentelemetry.sdk.trace.SdkTracerProvider
import io.opentelemetry.sdk.trace.export.SimpleSpanProcessor
import io.opentelemetry.api.common.Attributes
import io.opentelemetry.semconv.ResourceAttributes

actual object Telemetry {

    private val logger by LoggerDelegate()
    private lateinit var telemetry: OpenTelemetry

    actual fun register(name: String, endpoint: String) {
        val exporter = OtlpHttpSpanExporter.builder()
            .setEndpoint("$endpoint/v1/traces")
            .build()
        logger.info("Registering OpenTelemetry using $exporter")

        telemetry = OpenTelemetrySdk.builder()
            .setTracerProvider(
                SdkTracerProvider.builder()
                    .setResource(Resource.create(
                        Attributes.of(
                            ResourceAttributes.SERVICE_NAME, name)
                        )
                    )
                    .addSpanProcessor(SimpleSpanProcessor.create(exporter))
                    .build()
            )
            .buildAndRegisterGlobal()
    }

    actual val trace: Trace by lazy {
        OpenTelemetryTrace(telemetry.getTracer("ai.promethist"))
    }

    actual val suspendableTrace: SuspendableTrace by lazy {
        OpenTelemetrySuspendableTrace(telemetry.getTracer("ai.promethist"))
    }
}