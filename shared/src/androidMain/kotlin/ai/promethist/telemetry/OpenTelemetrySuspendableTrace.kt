package ai.promethist.telemetry

import io.opentelemetry.api.trace.*
import io.opentelemetry.context.Context

class OpenTelemetrySuspendableTrace(private val tracer: Tracer) : SuspendableTrace {

    override val currentSpan: Span get() = Span.current()

    private fun createSpan(
        name: String? = null,
        isRoot: Boolean = false,
        links: Set<Span> = emptySet(),
    ): Span {
        val spanBuilder = tracer.spanBuilder(name ?: "Unnamed")
        if (isRoot) {
            spanBuilder.setNoParent()
        }
        for (link in links) {
            spanBuilder.addLink(link.spanContext)
        }
        return spanBuilder.startSpan()
    }

    override suspend fun <T> withSpan(name: String?, isRoot: Boolean, links: Set<Span>, block: SuspendableTrace.Block<T>): T =
        createSpan(name, isRoot, links).let { span ->
        try {
            span.makeCurrent().use { block.process(span) }
        } catch (e: Throwable) {
            span.recordException(e)
            throw e
        } finally {
            span.end()
        }
    }

    override suspend fun <T> withSpan(traceId: String, spanId: String, name: String, block: SuspendableTrace.Block<T>): T {
        val spanContext = SpanContext.createFromRemoteParent(traceId, spanId, TraceFlags.getSampled(), TraceState.getDefault())
        return Context.current().with(Span.wrap(spanContext)).makeCurrent().use {
            withSpan(name, false, emptySet(), block)
        }
    }

    override suspend fun <T> withSpan(span: Span, name: String, block: SuspendableTrace.Block<T>): T =
        with(span.spanContext) {
            withSpan(traceId, spanId, name, block)
        }

    override fun startSpan(span: Span, name: String): Span {
        with(span.spanContext) {
            val spanContext = SpanContext.createFromRemoteParent(traceId, spanId, TraceFlags.getSampled(), TraceState.getDefault())
            val context = Context.current().with(Span.wrap(spanContext))
            val spanBuilder = tracer
                .spanBuilder(name)
                .setParent(context)
            val newSpan = spanBuilder.startSpan()
            newSpan.makeCurrent()
            return newSpan
        }
    }

    override fun startSpan(name: String?, isRoot: Boolean, links: Set<Span>): Span =
        with(createSpan(name, isRoot, links)) {
            this.makeCurrent()
            return this
        }
}