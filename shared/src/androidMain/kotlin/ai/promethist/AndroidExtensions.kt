package ai.promethist

import ai.promethist.util.getBuildConfigValue
import android.os.Build

actual val platform = Platform.Android

actual val runtime = Runtime(
    (getBuildConfigValue("VERSION_NAME") ?: "1.0.0").toString(),
    (getBuildConfigValue("VERSION_CODE") ?: "1").toString(),
    "Android",
    Build.VERSION.SDK_INT.toString(),
    Build.CPU_ABI,
    Build.MODEL
)
