package ai.promethist.util

import java.lang.reflect.Field

/**
 * Gets a field from the project's BuildConfig. This is useful when, for example, flavors
 * are used at the project level to set custom fields.
 * @param fieldName     The name of the field-to-access
 * @return              The value of the field, or `null` if the field is not found.
 */
fun getBuildConfigValue(fieldName: String): Any? {
    try {
        val clazz = Class.forName("ai.promethist.android" + ".BuildConfig")
        val field: Field = clazz.getField(fieldName)
        return field.get(null)
    } catch (e: Exception) {
        e.printStackTrace()
    }
    return null
}