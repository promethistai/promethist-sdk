package ai.promethist.util

import android.util.Log

class AndroidLogger(private val tag: String) : Logger {

    override fun info(msg: String) { Log.i(tag, msg) }

    override fun warn(msg: String) { Log.w(tag, msg) }

    override fun debug(msg: String) { Log.d(tag, msg) }

    override fun error(msg: String) { Log.e(tag, msg) }

    override fun error(msg: String, t: Throwable) { Log.e(tag, msg, t) }
}