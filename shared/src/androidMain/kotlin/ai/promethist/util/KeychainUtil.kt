package ai.promethist.util

import com.liftric.kvault.KVault

actual object KeychainUtil {

    private val store by lazy { KVault(AndroidApplication.application, "promethist_ai") }

    actual fun saveValue(key: String, value: String): Boolean {
        return store.set(key = key, stringValue = value)
    }

    actual fun getValue(key: String): String? {
        return store.string(key)
    }

    actual fun removeValue(key: String) {
        store.deleteObject(key)
    }
}