package ai.promethist.type

import java.util.UUID

actual typealias ID = UUID

actual fun newId(): ID = UUID.randomUUID()

actual fun id(s: String): ID = UUID.fromString(s)
