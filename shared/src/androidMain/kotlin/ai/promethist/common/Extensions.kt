package ai.promethist.common

import android.util.Log
import io.ktor.client.HttpClient
import io.ktor.client.engine.okhttp.OkHttp
import io.ktor.client.plugins.logging.LogLevel
import io.ktor.client.plugins.logging.Logger
import io.ktor.client.plugins.logging.Logging
import io.ktor.client.plugins.websocket.WebSockets

actual val sharedHttpClient: HttpClient =
    HttpClient(OkHttp) {
        install(Logging) {
            logger = object: Logger {
                override fun log(message: String) {
                    Log.d("HTTP Client", message)
                }
            }
            level = LogLevel.ALL
        }
        install(WebSockets) {
            pingInterval = 15_000
        }
    }