package ai.promethist

import platform.UIKit.UIDevice
import platform.Foundation.NSBundle

actual val platform = Platform.Ios

actual val runtime = Runtime(
    getInfo("CFBundleShortVersionString"),
    getInfo("CFBundleVersion"),
    UIDevice.currentDevice.systemName(),
    UIDevice.currentDevice.systemVersion,
    UIDevice.currentDevice.model,
    UIDevice.currentDevice.name
)


fun getInfo(key: String) = NSBundle.mainBundle.infoDictionary?.get(key)?.toString() ?: "?"