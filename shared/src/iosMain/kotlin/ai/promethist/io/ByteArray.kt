package ai.promethist.io

import kotlinx.cinterop.BetaInteropApi
import kotlinx.cinterop.ExperimentalForeignApi
import kotlinx.cinterop.allocArrayOf
import kotlinx.cinterop.memScoped
import platform.Foundation.NSData
import platform.Foundation.create

/*
 * Converts Kotlin ByteArray to Swift NSData
 */
@OptIn(ExperimentalForeignApi::class, BetaInteropApi::class)
fun ByteArray.getNSData(): NSData = memScoped {
    NSData.create(bytes = allocArrayOf(this@getNSData), size.toULong())
}