package ai.promethist.io

actual abstract class InputStream : Closeable {
    actual abstract fun read(): Int
    actual fun read(buf: ByteArray, off: Int, len: Int): Int {
        var c = 0
        for (i in off until off + len) {
            val b = read()
            if (b <= 0)
                break
            buf[i] = b.toByte()
        }
        return c
    }
}