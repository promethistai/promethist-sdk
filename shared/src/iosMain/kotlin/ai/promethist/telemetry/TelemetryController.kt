package ai.promethist.telemetry

interface TelemetryController {
    fun register(name: String, endpoint: String)
    fun getTrace(): Trace
    fun getSuspendableTrace(): SuspendableTrace
}