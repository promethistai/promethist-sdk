package ai.promethist.telemetry

actual interface Span {

    actual fun getSpanContext(): SpanContext
    actual fun end()
    actual fun recordException(exception: Throwable): Span = this
    actual fun setAttribute(key: String, value: String): Span = this
    actual fun setAttribute(key: String, value: Long): Span = this
    actual fun setAttribute(key: String, value: Double): Span = this
    actual fun setAttribute(key: String, value: Boolean): Span = this
}