package ai.promethist.telemetry

import ai.promethist.util.LoggerDelegate

actual object Telemetry {

    private val blankSpan = object : Span {
        override fun getSpanContext(): SpanContext = object : SpanContext {
            override fun getTraceId(): String = ""
            override fun getSpanId(): String = ""
        }
        override fun end() {}
    }
    private val logger by LoggerDelegate()
    lateinit var telemetryController: TelemetryController

    actual fun register(name: String, endpoint: String) = telemetryController.register(name, endpoint)

    actual val trace: Trace by lazy {
        if (::telemetryController.isInitialized) {
            telemetryController.getTrace()
        } else {
            LoggingTrace(blankSpan)
        }
    }

    actual val suspendableTrace: SuspendableTrace by lazy {
        if (::telemetryController.isInitialized) {
            telemetryController.getSuspendableTrace()
        } else {
            LoggingSuspendableTrace(blankSpan)
        }
    }
}