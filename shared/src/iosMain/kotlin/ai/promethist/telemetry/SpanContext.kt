package ai.promethist.telemetry

actual interface SpanContext {
    actual fun getTraceId(): String
    actual fun getSpanId(): String
}