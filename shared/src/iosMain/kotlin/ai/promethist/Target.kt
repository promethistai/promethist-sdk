package ai.promethist

import ai.promethist.client.ClientTargetPreset
import platform.Foundation.NSBundle

val iosTarget: ClientTargetPreset get() =
    when(NSBundle.mainBundle.objectForInfoDictionaryKey("Target")) {
        "Promethist" -> ClientTargetPreset.Promethist
        "DFHA" -> ClientTargetPreset.DFHA
        "tmobile" -> ClientTargetPreset.TMobile
        "tmobilev3" -> ClientTargetPreset.TMobileV3
        "gaia" -> ClientTargetPreset.Gaia
        else -> ClientTargetPreset.Elysai
    }