package ai.promethist.type

import platform.Foundation.NSUUID

actual typealias ID = String

actual fun newId(): ID = NSUUID().UUIDString()

actual fun id(s: String): ID = TODO()