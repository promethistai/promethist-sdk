package ai.promethist.util

class IosLogger(val name: String) : Logger {

    override fun info(msg: String) = println("INFO $name - $msg")

    override fun warn(msg: String) = println("WARN $name - $msg")

    override fun debug(msg: String) = println("DEBUG $name - $msg")

    override fun error(msg: String) = println("ERROR $name - $msg")

    override fun error(msg: String, t: Throwable) = println("ERROR $name - $msg: ${t.stackTraceToString()}")
}