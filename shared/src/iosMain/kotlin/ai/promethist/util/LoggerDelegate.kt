package ai.promethist.util

import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.KProperty

actual class LoggerDelegate : ReadOnlyProperty<Any?, Logger> {

    private lateinit var logger: Logger

    actual override operator fun getValue(thisRef: Any?, property: KProperty<*>): Logger {
        require(thisRef != null) { "LoggerDelegate can be used only on class properties." }

        if (!::logger.isInitialized)
            logger = IosLogger(thisRef::class.qualifiedName ?: "Unknown")
        return logger
    }
}