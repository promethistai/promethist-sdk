package ai.promethist.util

import dev.tmapps.konnection.Konnection
import kotlinx.coroutines.flow.Flow

actual object NetworkUtil {

    actual fun observeHasConnection(): Flow<Boolean> {
        return Konnection.instance.observeHasConnection()
    }

    actual fun isConnected(): Boolean {
        return Konnection.instance.isConnected()
    }
}