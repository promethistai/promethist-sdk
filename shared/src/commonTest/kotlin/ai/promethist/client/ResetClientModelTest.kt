package ai.promethist.client

import ai.promethist.client.model.AudioIndicatorState
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withTimeout
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue
import kotlin.time.Duration.Companion.milliseconds

class ResetClientModelTest: BaseClientModelTest() {

    @Test
    fun testReset() = runBlocking {
        testResetWhileLoading()
        testResetWhilePlaying()
        testResetWhileListening()
    }

    /**
     * Simulates reset while input sent & client waits for response from server
     */
    private fun testResetWhileLoading() = runBlocking {
        var hasLoaded = false
        var hasPlayed = false

        // Wait till loading starts
        withTimeout(Timeout) {
            while (!uiState.isLoading) {
                println("Waiting for loading start..")
                delay(100.milliseconds)
            }
        }

        // Send Reset while loading
        delay(200.milliseconds)
        clientModel.handleEvent(ClientEvent.Reset(sendAsInput = true))
        delay(200.milliseconds)

        // Wait till loading ends & playback finish
        withTimeout(Timeout) {
            while (uiState.isLoading) {
                println("Waiting for loading to finish..")
                hasLoaded = true
                delay(50.milliseconds)
            }
        }
        assertTrue(!speechRecognizerController.isListening)
        assertEquals(AudioIndicatorState.Playing, uiState.audioIndicatorState)
        withTimeout(Timeout) {
            while (avatarPlayerController.isPlaying) {
                println("Waiting for playback to finish..")
                hasPlayed = true
                delay(50.milliseconds)
            }
        }

        // Check audio played & speech recognizer is listening
        delay(200.milliseconds)
        assertTrue(hasLoaded)
        assertTrue(hasPlayed)
        assertTrue(speechRecognizerController.isListening)
        assertEquals(AudioIndicatorState.StartSpeaking, uiState.audioIndicatorState)
        assertTrue(uiState.visualItem?.text.isNullOrBlank().not())
        println("<<<<< ${uiState.visualItem?.text}")
    }

    /**
     * Simulates reset while AvatarPlayer is playing
     */
    private fun testResetWhilePlaying() = runBlocking {
        var hasPlayed = false
        var hasLoaded = false

        clientModel.handleEvent(ClientEvent.Reset(sendAsInput = true))

        // Wait till loading starts
        withTimeout(Timeout) {
            while (!uiState.isLoading) {
                println("Waiting for loading start..")
                delay(100.milliseconds)
            }
        }

        // Wait till playback starts
        withTimeout(Timeout) {
            while (!avatarPlayerController.isPlaying) {
                println("Waiting for playback to start..")
                hasPlayed = true
                delay(50.milliseconds)
            }
        }
        assertTrue(hasPlayed)
        assertEquals(AudioIndicatorState.Playing, uiState.audioIndicatorState)
        assertTrue(avatarPlayerController.isPlaying)
        hasPlayed = false

        // Reset while playing
        delay(300.milliseconds)
        clientModel.handleEvent(ClientEvent.Reset(sendAsInput = true))

        // Wait for loading to start
        withTimeout(Timeout) {
            while (!uiState.isLoading) {
                println("Waiting for loading to start..")
                delay(50.milliseconds)
            }
        }

        // Wait till loading ends & playback finish
        withTimeout(Timeout) {
            while (uiState.isLoading) {
                println("Waiting for loading to finish..")
                hasLoaded = true
                delay(50.milliseconds)
            }
        }
        assertTrue(!speechRecognizerController.isListening)
        assertEquals(AudioIndicatorState.Playing, uiState.audioIndicatorState)
        withTimeout(Timeout) {
            while (avatarPlayerController.isPlaying) {
                println("Waiting for playback to finish..")
                hasPlayed = true
                delay(50.milliseconds)
            }
        }

        // Check audio played & speech recognizer is listening
        delay(200.milliseconds)
        assertTrue(hasLoaded)
        assertTrue(hasPlayed)
        assertTrue(speechRecognizerController.isListening)
        assertEquals(AudioIndicatorState.StartSpeaking, uiState.audioIndicatorState)
        assertTrue(uiState.visualItem?.text.isNullOrBlank().not())
        println("<<<<< ${uiState.visualItem?.text}")
    }

    /**
     * Simulates reset while SpeechRecognizer is listening
     */
    private fun testResetWhileListening() = runBlocking {
        var hasLoaded = false
        var hasPlayed = false

        clientModel.handleEvent(ClientEvent.Reset(sendAsInput = true))

        // Wait till loading starts & finishes
        withTimeout(Timeout) {
            while (!uiState.isLoading) {
                println("Waiting for loading start..")
                delay(100.milliseconds)
            }
        }
        withTimeout(Timeout) {
            while (uiState.isLoading) {
                println("Waiting for loading finish..")
                hasLoaded = true
                delay(100.milliseconds)
            }
        }
        withTimeout(Timeout) {
            while (avatarPlayerController.isPlaying) {
                println("Waiting for playback to finish..")
                hasPlayed = true
                delay(50.milliseconds)
            }
        }

        // Check listening
        delay(200.milliseconds)
        assertTrue(hasLoaded)
        assertTrue(hasPlayed)
        assertTrue(speechRecognizerController.isListening)
        assertEquals(AudioIndicatorState.StartSpeaking, uiState.audioIndicatorState)
        assertTrue(uiState.visualItem?.text.isNullOrBlank().not())
        println("<<<<< ${uiState.visualItem?.text}")

        // Reset while listening
        delay(300.milliseconds)
        clientModel.handleEvent(ClientEvent.Reset(sendAsInput = true))
        delay(200.milliseconds)

        // Wait till loading ends & playback finish
        withTimeout(Timeout) {
            while (uiState.isLoading) {
                println("Waiting for loading to finish..")
                hasLoaded = true
                delay(50.milliseconds)
            }
        }
        assertTrue(!speechRecognizerController.isListening)
        assertEquals(AudioIndicatorState.Playing, uiState.audioIndicatorState)
        withTimeout(Timeout) {
            while (avatarPlayerController.isPlaying) {
                println("Waiting for playback to finish..")
                hasPlayed = true
                delay(50.milliseconds)
            }
        }

        // Check audio played & speech recognizer is listening
        delay(200.milliseconds)
        assertTrue(hasLoaded)
        assertTrue(hasPlayed)
        assertTrue(speechRecognizerController.isListening)
        assertEquals(AudioIndicatorState.StartSpeaking, uiState.audioIndicatorState)
        assertTrue(uiState.visualItem?.text.isNullOrBlank().not())
        println("<<<<< ${uiState.visualItem?.text}")
    }
}