package ai.promethist.client

import ai.promethist.client.model.AudioIndicatorState
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withTimeout
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue
import kotlin.time.Duration.Companion.milliseconds

class SimpleConversationClientModelTest: BaseClientModelTest() {

    @Test
    fun testSimpleConversation() = runBlocking {
        testFirstTurn()
        testFirstTurnSpeechRecognizer()
        testSecondTurn()
    }

            /**
     * Simulates first turn after ClientModel is launched & sends #intro
     */
    private fun testFirstTurn() = runBlocking {
        var hasPlayed = false

        // SpeechRecognizer should not be listening at launch
        assertTrue(speechRecognizerController.isListening.not())
        assertTrue(uiState.audioIndicatorState != AudioIndicatorState.StartSpeaking)
        assertTrue(uiState.audioIndicatorState != AudioIndicatorState.Speaking)

        // Wait till loading starts
        withTimeout(Companion.Timeout) {
            while (!uiState.isLoading) {
                delay(50.milliseconds)
            }
        }

        // Wait till loading ends
        withTimeout(Companion.Timeout) {
            while (uiState.isLoading) {
                delay(50.milliseconds)
            }
        }

        // Wait for playback to finish
        withTimeout(Companion.Timeout) {
            while (avatarPlayerController.isPlaying) {
                hasPlayed = true
                delay(50.milliseconds)
            }
        }

        // Check for correct end turn (listening to user) state
        assertTrue(hasPlayed)
        assertTrue(avatarPlayerController.isPlaying.not())
        assertTrue(uiState.visualItem?.text.isNullOrBlank().not())
        println("<<<<< ${uiState.visualItem?.text}")
    }

    /**
     * Simulates user speech after first turn is finished
     */
    private fun testFirstTurnSpeechRecognizer() = runBlocking {
        delay(200.milliseconds)

        // Speech recognizer should be started & waiting for user speech
        assertTrue(speechRecognizerController.isListening)
        assertEquals(uiState.audioIndicatorState, AudioIndicatorState.StartSpeaking)

        // Check partial transcribe
        speechRecognizerController.simulatePartialTranscribe()
        delay(200.milliseconds)
        assertEquals(uiState.audioIndicatorState, AudioIndicatorState.Speaking)
        assertEquals(speechRecognizerController.currentTranscribe, uiState.transcript)

        // Send final user transcribe
        speechRecognizerController.simulateFullTranscribe()
        println(">>>>> ${uiState.transcript}")
        delay(100.milliseconds)

        // Check if user input has been sent & Speech recognizer closed
        assertTrue(uiState.isLoading)
        assertTrue(speechRecognizerController.isListening.not())
        assertEquals(uiState.audioIndicatorState, AudioIndicatorState.Playing)
    }

    /**
     * Simulates persona response after user speech is final
     */
    private fun testSecondTurn() = runBlocking {
        // Wait till loading ends
        withTimeout(Companion.Timeout) {
            while (uiState.isLoading) {
                delay(50.milliseconds)
            }
        }

        // Check response is received & audio is playing
        assertTrue(avatarPlayerController.isPlaying)
        assertEquals(AudioIndicatorState.Playing, uiState.audioIndicatorState)

        // Wait till playback finish
        withTimeout(Companion.Timeout) {
            while (avatarPlayerController.isPlaying) {
                delay(100.milliseconds)
            }
        }

        // Check for correct end turn (listening to user) state
        assertTrue(uiState.visualItem?.text.isNullOrBlank().not())
        println("<<<<< ${uiState.visualItem?.text}")

        // Check speech recognizer is listening
        delay(200.milliseconds)
        assertTrue(speechRecognizerController.isListening)
        assertEquals(AudioIndicatorState.StartSpeaking, uiState.audioIndicatorState)
    }
}