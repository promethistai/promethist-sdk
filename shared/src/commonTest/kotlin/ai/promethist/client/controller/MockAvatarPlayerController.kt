package ai.promethist.client.controller

import ai.promethist.client.avatar.AvatarPlayerBridge
import ai.promethist.client.avatar.AvatarPlayerController
import ai.promethist.client.avatar.unity.AvatarEventV1
import ai.promethist.client.avatar.unity.AvatarMessage
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class MockAvatarPlayerController : AvatarPlayerController {

    override lateinit var bridge: AvatarPlayerBridge

    private val scope = CoroutineScope(Job())
    private var audioBuffer: String = ""

    var isPlaying = false
        private set

    override fun sendMessage(nameObject: String, functionName: String, message: String) {
        when (functionName) {
            AvatarEventV1.SetPersona("", false).functionName -> {
                scope.launch {
                    // Simulate avatar loading for 0,5s
                    delay(500)
                    bridge.onMessageReceived(AvatarMessage.AvatarLoaded.name)
                }
            }
            AvatarEventV1.TurnEnd.functionName -> {
                scope.launch {
                    // Simulate audio playing for 1s & trigger TurnEnded
                    delay(1000)
                    bridge.onMessageReceived(AvatarMessage.TurnEnded.name)
                    audioBuffer = ""
                    isPlaying = false
                    println("Audio: Playback ended")
                }
            }
            AvatarEventV1.PlayAudio("").functionName -> {
                if (audioBuffer.isEmpty()) {
                    bridge.onMessageReceived(AvatarMessage.AudioStarted.name)
                }

                audioBuffer += message
                if (!isPlaying) println("Audio: Playback started")
                isPlaying = true
                println("Audio: ${audioBuffer.length}")
            }
            AvatarEventV1.ReceiveData("").functionName -> {
                if (audioBuffer.isEmpty()) {
                    bridge.onMessageReceived(AvatarMessage.AudioStarted.name)
                }

                audioBuffer += message
                if (!isPlaying) println("Audio: Playback started")
                isPlaying = true
                println("Audio: ${audioBuffer.length}")
            }
        }
    }

    override fun onSceneLoaded() {
        //TODO("Not yet implemented")
    }

    override fun setAudioBlocked(isBlocked: Boolean) {
        //TODO("Not yet implemented")
    }

    override fun getIsAudioBlocked(): Boolean {
        //TODO("Not yet implemented")
        return false
    }
}