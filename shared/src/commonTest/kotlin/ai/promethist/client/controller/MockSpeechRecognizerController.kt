package ai.promethist.client.controller

import ai.promethist.client.speech.SpeechRecognizerController
import ai.promethist.client.speech.SpeechRecognizerControllerCallback
import kotlinx.coroutines.delay

class MockSpeechRecognizerController: SpeechRecognizerController {

    private var callback: SpeechRecognizerControllerCallback? = null

    var isListening: Boolean = false
        private set

    var currentTranscribe: String = ""
        private set

    override fun setCallback(callback: SpeechRecognizerControllerCallback) {
        this.callback = callback
    }

    override suspend fun startTranscribe() {
        currentTranscribe = ""
        isListening = true
    }

    override fun stopTranscribe() {
        currentTranscribe = ""
        isListening = false
    }

    override fun pause() {}
    override fun resume() {}
    override fun setSilenceLength(length: Int) {}
    override fun onVisualItemReceived() {}
    override fun restartTranscribe() {}
    override fun setPrivileged(value: Boolean) {}

    suspend fun simulatePartialTranscribe() {
        delay(100)
        currentTranscribe = "Ahoj"
        callback?.onTranscribeResult(currentTranscribe, false)
        delay(200)
        currentTranscribe = "Ahoj jak se"
        callback?.onTranscribeResult(currentTranscribe, false)
    }

    suspend fun simulateFullTranscribe() {
        delay(100)
        currentTranscribe = "Ahoj"
        callback?.onTranscribeResult(currentTranscribe, false)
        delay(100)
        currentTranscribe = "Ahoj jak se"
        callback?.onTranscribeResult(currentTranscribe, false)
        delay(200)
        currentTranscribe = "Ahoj jak se máš?"

        callback?.onTranscribeResult(currentTranscribe, true)
        stopTranscribe()
    }
}