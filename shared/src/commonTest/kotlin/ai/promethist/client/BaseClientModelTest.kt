package ai.promethist.client

import ai.promethist.client.avatar.CoroutinesFlowUiStateHandler
import ai.promethist.client.avatar.unity.AvatarEventsApiV1Impl
import ai.promethist.client.avatar.unity.AvatarPlayerImpl
import ai.promethist.client.channel.WebSocketClientChannel
import ai.promethist.client.controller.MockAvatarPlayerController
import ai.promethist.client.controller.MockSpeechRecognizerController
import ai.promethist.client.speech.SpeechRecognizer
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import kotlin.test.BeforeTest

abstract class BaseClientModelTest {

    protected lateinit var avatarPlayerController: MockAvatarPlayerController
    protected lateinit var speechRecognizerController: MockSpeechRecognizerController
    protected lateinit var clientModel: ClientModel

    private lateinit var uiStateHandler: CoroutinesFlowUiStateHandler
    protected var uiState: ClientUiState = ClientUiState.Initial

    @OptIn(DelicateCoroutinesApi::class)
    @BeforeTest
    fun setup() {
        avatarPlayerController = MockAvatarPlayerController()
        val avatarEventsApi = AvatarEventsApiV1Impl(avatarPlayerController)
        val avatarPlayer = AvatarPlayerImpl(avatarPlayerController, avatarEventsApi)
        avatarPlayerController.bridge = avatarPlayer

        speechRecognizerController = MockSpeechRecognizerController()
        val speechRecognizer = SpeechRecognizer(speechRecognizerController)
        speechRecognizerController.setCallback(speechRecognizer)

        uiStateHandler = CoroutinesFlowUiStateHandler()
        clientModel = ClientModelImpl(
            config = ClientModelConfig.Default,
            uiStateHandler = uiStateHandler,
            avatarPlayer = avatarPlayer,
            speechRecognizer = speechRecognizer,
            technicalReportProvider = { TODO("Not implemented") },
            channel = WebSocketClientChannel()
        )
        clientModel.start()

        GlobalScope.launch {
            uiStateHandler.uiState.collectLatest {
                uiState = it
                //if (it.isLoading) println("------> LOADING")
            }
        }
    }

    companion object {
        const val Timeout: Long = 5000
    }
}