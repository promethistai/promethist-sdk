package ai.promethist.type

import java.util.UUID

actual typealias ID = UUID

actual fun newId(): ID = UUID.randomUUID()

val ZERO_ID = ID(0L, 0L)

actual fun id(s: String): ID = UUID.fromString(s)
