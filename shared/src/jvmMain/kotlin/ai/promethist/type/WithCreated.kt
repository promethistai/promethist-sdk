package ai.promethist.type

import java.time.Instant

interface WithCreated {
    val created: Instant
}