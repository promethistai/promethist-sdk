package ai.promethist.type

import java.time.Instant

interface WithLastModified {
    val lastModified: Instant
}