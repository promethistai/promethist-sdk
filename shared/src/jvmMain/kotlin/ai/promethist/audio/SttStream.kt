package ai.promethist.audio

import ai.promethist.io.WritableStream
import ai.promethist.util.Locale

interface SttStream: WritableStream {

    val config: SttStreamConfig
    val locale: Locale
    val isOpen: Boolean

    override val streamName get() = "${super.streamName}/$locale" + if (isOpen) "+" else "-"
}