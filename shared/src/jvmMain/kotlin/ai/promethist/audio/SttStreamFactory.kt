package ai.promethist.audio

interface SttStreamFactory {

    fun create(/*config: SttStreamConfig, callback: SttCallback, expectedPhrases: List<ExpectedPhrase>*/): SttStream

}