package ai.promethist.audio

import ai.promethist.util.LoggerDelegate
import com.google.api.gax.rpc.*
import com.google.cloud.speech.v1.*
import com.google.protobuf.ByteString
import java.io.Closeable

class GoogleSpeechRecognizer(
    private val microphone: Microphone,
) : Closeable {

    inner class Observer(
        private val callback: (String, Boolean) -> Unit
    ) : ResponseObserver<StreamingRecognizeResponse> {
        override fun onStart(controller: StreamController) = logger.info("Speech recognition started")
        override fun onComplete() = logger.info("Speech recognition completed")
        override fun onError(t: Throwable) = logger.error("Speech recognition failed", t)
        override fun onResponse(response: StreamingRecognizeResponse) {
            for (result in response.resultsList) {
                val transcript = result.alternativesList.first().transcript
                logger.info("Speech recognition transcript (isFinal = ${result.isFinal}): $transcript")
                callback(transcript, result.isFinal)
            }
        }
    }

    private val speechClient = SpeechClient.create()
    private val logger by LoggerDelegate()

    fun recognize(
        languageCode: String = "en-US",
        speechTimeout: Int = 10000,
        callback: (String, Boolean) -> Unit
    ) {
        var transcript: String? = null
        var speechDetected = false
        val clientStream = speechClient
            .streamingRecognizeCallable()
            .splitCall(Observer { it, isFinal ->
                speechDetected = true
                if (isFinal)
                    transcript = it
            })
        clientStream.send(
            StreamingRecognizeRequest
                .newBuilder()
                .setStreamingConfig(
                    StreamingRecognitionConfig
                        .newBuilder()
                        .setConfig(
                            RecognitionConfig
                                .newBuilder()
                                .setEncoding(RecognitionConfig.AudioEncoding.LINEAR16)
                                .setSampleRateHertz(microphone.audioFormat.sampleRate.toInt())
                                .setLanguageCode(languageCode)
                                .setEnableAutomaticPunctuation(true)
                                //.setEnableWordConfidence(true)
                                .build()
                        )
                        .setInterimResults(true)
                        .build()
                )
                .build()
        )
        val time = System.currentTimeMillis()
        microphone.process { buffer, bytesRead ->
            val audioBytes = ByteString.copyFrom(buffer.copyOf(bytesRead))
            clientStream.send(
                StreamingRecognizeRequest
                    .newBuilder()
                    .setAudioContent(audioBytes)
                    .build()
            )
            if (!speechDetected && time + speechTimeout < System.currentTimeMillis()) {
                transcript = "#silence"
            }
            transcript != null
        }
        clientStream.closeSend()
        callback(transcript!!, true)
    }

    override fun close() = speechClient.close()
}