package ai.promethist.audio

import java.io.Closeable
import javax.sound.sampled.AudioFormat
import javax.sound.sampled.AudioSystem
import javax.sound.sampled.DataLine
import javax.sound.sampled.TargetDataLine

class Microphone(
    val audioFormat: AudioFormat = AudioFormat(16000.0f, 16, 1, true, false)
) : Closeable {

    private val lineInfo = DataLine.Info(TargetDataLine::class.java, audioFormat)
    private val line: TargetDataLine
    private val buffer = ByteArray((audioFormat.sampleRate * audioFormat.frameSize).toInt())
    private var closed = false

    init {
        if (!AudioSystem.isLineSupported(lineInfo)) {
            error("Microphone is not supported")
        }
        line = AudioSystem.getLine(lineInfo) as TargetDataLine
    }

    fun process(onData: (ByteArray, Int) -> Boolean) {
        line.open(audioFormat)
        line.start()
        do {
            val bytesRead = line.read(buffer, 0, buffer.size)
        } while (!closed && !onData(buffer, bytesRead))
        line.close()
    }

    override fun close() {
        closed = true
    }
}