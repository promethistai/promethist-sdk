package ai.promethist.audio

interface SttHandler {
    fun onStart()
    fun onResult(transcript: String, isFinal: Boolean)
    fun onComplete()
    fun onError(e: Throwable)
}