package ai.promethist.audio

import ai.promethist.Defaults
import ai.promethist.toJava
import javax.sound.sampled.AudioSystem
import javax.sound.sampled.DataLine
import javax.sound.sampled.SourceDataLine

class RawAudioPlayer(
    audioFormat: AudioFormat = Defaults.audioFormat
) : AudioPlayer {

    private val format = audioFormat.toJava()
    private var line: SourceDataLine

    init {
        val info = DataLine.Info(SourceDataLine::class.java, format)
        line = AudioSystem.getLine(info) as SourceDataLine
        line.open(format, format.sampleSizeInBits / 8 * format.sampleRate.toInt() * 60)
        line.start()
    }


    override fun add(block: ByteArray) {
        line.write(block, 0, block.size)
    }

    override fun reset(drain: Boolean) {
        if (drain)
            line.drain()
    }
}