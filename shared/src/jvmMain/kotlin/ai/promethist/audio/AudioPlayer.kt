package ai.promethist.audio

interface AudioPlayer {
    fun add(block: ByteArray)
    fun reset(drain: Boolean = false)
}