package ai.promethist.audio

import java.io.ByteArrayInputStream
import java.nio.ByteBuffer
import javax.sound.sampled.AudioSystem
import javax.sound.sampled.DataLine
import javax.sound.sampled.SourceDataLine

class WavAudioPlayer : AudioPlayer {

    private var line: SourceDataLine? = null
    private val buffer = ByteBuffer.allocate(1024 * 1024)
    private var wavSize = 0
    private var dataReceived = 0
    private var headerParsed = false

    override fun add(block: ByteArray) {
        if (block.size == 4 && block.contentEquals(AUDIO_END)) {
            reset()
        } else {
            buffer.put(block)
            if (!headerParsed && buffer.position() >= 44) {
                parseHeader()
            }
            if (headerParsed) {
                playAudioData()
            }
        }
    }

    private fun parseHeader() {
        if (buffer.position() >= 44) {
            val headerBytes = ByteArray(44)
            buffer.flip()
            buffer.get(headerBytes)
            buffer.compact()

            val audioInputStream = AudioSystem.getAudioInputStream(ByteArrayInputStream(headerBytes))
            val audioFormat = audioInputStream.format

            val info = DataLine.Info(SourceDataLine::class.java, audioFormat)
            line = AudioSystem.getLine(info) as SourceDataLine
            line?.open(audioFormat)
            line?.start()

            wavSize = ByteBuffer.wrap(headerBytes, 4, 4).order(java.nio.ByteOrder.LITTLE_ENDIAN).int + 8
            headerParsed = true
            dataReceived = 44
        }
    }

    private fun playAudioData() {
        val available = buffer.position()
        if (available > 0) {
            buffer.flip()
            val audioData = ByteArray(available)
            buffer.get(audioData)
            buffer.compact()
            line?.write(audioData, 0, audioData.size)
            dataReceived += audioData.size
            // Check if we've received the entire WAV file
//            if (dataReceived >= wavSize) {
//                reset()
//            }
        }
    }

    override fun reset(drain: Boolean) {
        if (drain)
            line?.drain()
        line?.close()
        line = null
        headerParsed = false
        buffer.clear() // Reset buffer for next WAV file
        dataReceived = 0
    }
}