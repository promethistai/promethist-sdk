package ai.promethist.converter

import ai.promethist.type.Ref
import org.springframework.core.convert.converter.Converter

object RefWriteConverter : Converter<Ref, String> {
    override fun convert(ref: Ref) = ref.value
}