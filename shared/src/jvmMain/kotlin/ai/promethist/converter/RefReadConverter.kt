package ai.promethist.converter

import ai.promethist.type.Ref
import org.springframework.core.convert.converter.Converter

object RefReadConverter : Converter<String, Ref> {
    override fun convert(value: String) = Ref(value)
}