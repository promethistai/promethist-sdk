package ai.promethist.converter

import org.springframework.core.convert.converter.Converter
import java.time.ZonedDateTime
import java.util.Date

object ZonedDateTimeWriteConverter : Converter<ZonedDateTime, Date> {

    override fun convert(zonedDateTime: ZonedDateTime): Date = Date.from(zonedDateTime.toInstant())
}