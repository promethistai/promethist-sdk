package ai.promethist.telemetry

actual typealias SpanContext = io.opentelemetry.api.trace.SpanContext