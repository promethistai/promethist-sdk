package ai.promethist.telemetry

import io.opentelemetry.api.trace.*
import io.opentelemetry.context.Context
import java.time.Instant

class OpenTelemetrySuspendableTrace(tracer: Tracer) : OpenTelemetryTraceBase(tracer), SuspendableTrace {

    override val currentSpan: Span get() = Span.current()

    suspend fun <T> withSpan(
        name: String? = null,
        isRoot: Boolean = false,
        links: Set<Span> = emptySet(),
        startTimestamp: Instant? = null,
        endTimestamp: Instant? = null,
        block: SuspendableTrace.Block<T>
    ): T = createSpan(name, isRoot, links, startTimestamp).let { span ->
        try {
            span.makeCurrent().use { block.process(span) }
        } catch (e: Throwable) {
            span.recordException(e)
            throw e
        } finally {
            if (endTimestamp != null) {
                span.end(endTimestamp)
            } else {
                span.end()
            }
        }
    }

    override suspend fun <T> withSpan(name: String?, isRoot: Boolean, links: Set<Span>, block: SuspendableTrace.Block<T>): T
            = withSpan(name, isRoot, links, null, null, block)

    override suspend fun <T> withSpan(traceId: String, spanId: String, name: String, block: SuspendableTrace.Block<T>): T {
        val spanContext = SpanContext.createFromRemoteParent(traceId, spanId, TraceFlags.getSampled(), TraceState.getDefault())
        return Context.current().with(Span.wrap(spanContext)).makeCurrent().use {
            withSpan(name, false, emptySet(), null, null, block)
        }
    }

    override fun startSpan(span: Span, name: String) = createSpan(name)

    override fun startSpan(name: String?, isRoot: Boolean, links: Set<Span>) = createSpan(name, isRoot, links)

    override suspend fun <T> withSpan(span: Span, name: String, block: SuspendableTrace.Block<T>): T =
        withSpan(name, block = block)
}