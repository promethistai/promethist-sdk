package ai.promethist.telemetry

import io.opentelemetry.api.trace.*
import io.opentelemetry.context.Context
import java.time.Instant

class OpenTelemetryTrace(tracer: Tracer) : OpenTelemetryTraceBase(tracer), Trace {

    override val currentSpan: Span get() = Span.current()

    override fun startSpan(
        name: String?,
        isRoot: Boolean,
        links: Set<Span>,
    ): Span {
        val spanBuilder = tracer.spanBuilder(qualifiedName(name))
        if (isRoot) {
            spanBuilder.setNoParent()
        }
        for (link in links) {
            spanBuilder.addLink(link.spanContext)
        }
        return spanBuilder.startSpan()
    }

    fun <T> withSpan(
        name: String? = null,
        isRoot: Boolean = false,
        links: Set<Span> = emptySet(),
        endTimestamp: Instant? = null,
        block: Trace.Block<T>
    ): T = startSpan(name, isRoot, links).let { span ->
        try {
            span.makeCurrent().use { block.process(span) }
        } catch (e: Throwable) {
            span.recordException(e)
            throw e
        } finally {
            if (endTimestamp != null) {
                span.end(endTimestamp)
            } else {
                span.end()
            }
        }
    }

    override fun <T> withSpan(name: String?, isRoot: Boolean, links: Set<Span>, block: Trace.Block<T>): T
        = withSpan(name, isRoot, links, null, block)

    override fun <T> withSpan(traceId: String, spanId: String, name: String, block: Trace.Block<T>): T {
        val spanContext = SpanContext.createFromRemoteParent(traceId, spanId, TraceFlags.getSampled(), TraceState.getDefault())
        return Context.current().with(Span.wrap(spanContext)).makeCurrent().use {
            withSpan(name, false, emptySet(), null, block)
        }
    }
}