package ai.promethist.telemetry

import io.opentelemetry.api.metrics.*
import java.util.function.Consumer

class OpenTelemetryMetrics(private val meter: Meter) : Metrics {

    private fun qualifiedName(name: String) =
        Thread.currentThread().stackTrace
            .drop(1)
            .first { it.className != this@OpenTelemetryMetrics.javaClass.name }
            .let { "${it.className}.$name".lowercase().replace('.', '_') }

    override fun createLongCounter(name: String): LongCounter = meter.counterBuilder(qualifiedName(name)).build()

    override fun createDoubleCounter(name: String): DoubleCounter = meter.counterBuilder(qualifiedName(name)).ofDoubles().build()

    override fun createLongUpDownCounter(name: String): LongUpDownCounter = meter.upDownCounterBuilder(qualifiedName(name)).build()

    override fun createDoubleUpDownCounter(name: String): DoubleUpDownCounter = meter.upDownCounterBuilder(qualifiedName(name)).ofDoubles().build()

    override fun createLongHistogram(name: String): LongHistogram = meter.histogramBuilder(qualifiedName(name)).ofLongs().build()

    override fun createDoubleHistogram(name: String): DoubleHistogram = meter.histogramBuilder(qualifiedName(name)).build()

    override fun createLongGauge(name: String, callback: Consumer<ObservableLongMeasurement>) {
        meter.gaugeBuilder(qualifiedName(name)).ofLongs().buildWithCallback(callback)
    }

    override fun createDoubleGauge(name: String, callback: Consumer<ObservableDoubleMeasurement>) {
        meter.gaugeBuilder(qualifiedName(name)).buildWithCallback(callback)
    }
}