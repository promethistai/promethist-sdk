package ai.promethist.telemetry

import io.opentelemetry.api.metrics.*
import java.util.function.Consumer

//FIXME make multiplatform
interface Metrics {

    fun createLongCounter(name: String): LongCounter
    fun createDoubleCounter(name: String): DoubleCounter
    fun createLongUpDownCounter(name: String): LongUpDownCounter
    fun createDoubleUpDownCounter(name: String): DoubleUpDownCounter
    fun createLongHistogram(name: String): LongHistogram
    fun createDoubleHistogram(name: String): DoubleHistogram
    fun createLongGauge(name: String, callback: Consumer<ObservableLongMeasurement>)
    fun createDoubleGauge(name: String, callback: Consumer<ObservableDoubleMeasurement>)
}