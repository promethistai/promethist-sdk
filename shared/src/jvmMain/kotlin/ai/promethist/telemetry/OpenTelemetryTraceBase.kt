package ai.promethist.telemetry

import io.opentelemetry.api.trace.Tracer
import java.time.Instant

open class OpenTelemetryTraceBase(val tracer: Tracer) {

    protected fun qualifiedName(s: String?) =
        Thread.currentThread().stackTrace
            .drop(2)
            .first { !it.className.startsWith(OpenTelemetryTrace::class.java.packageName) }
            .let { "${it.className.substringAfterLast('.')}.${it.methodName}" + if (s != null) ":$s" else "" }

    protected fun createSpan(
        name: String? = null,
        isRoot: Boolean = false,
        links: Set<Span> = emptySet(),
        startTimestamp: Instant? = null
    ): Span {
        val spanBuilder = tracer.spanBuilder(qualifiedName(name))
        if (isRoot) {
            spanBuilder.setNoParent()
        }
        for (link in links) {
            spanBuilder.addLink(link.spanContext)
        }
        if (startTimestamp != null) {
            spanBuilder.setStartTimestamp(startTimestamp)
        }
        return spanBuilder.startSpan()
    }
}