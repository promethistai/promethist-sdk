package ai.promethist.common

import io.ktor.client.HttpClient
import io.ktor.client.engine.okhttp.OkHttp
import io.ktor.client.plugins.websocket.WebSockets

actual val sharedHttpClient: HttpClient =
    HttpClient(OkHttp) {
        install(WebSockets)
    }
