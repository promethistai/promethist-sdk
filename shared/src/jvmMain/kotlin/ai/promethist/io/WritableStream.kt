package ai.promethist.io

import java.io.InputStream

interface WritableStream : AutoCloseable {

    fun write(data: ByteArray, offset: Int, count: Int)

    fun write(input: InputStream, bufSize: Int = 16384) {
        val bytes = ByteArray(bufSize)
        var count = input.read(bytes)
        while (count != -1) {
            write(bytes, 0, count)
            count = input.read(bytes)
        }
    }

    val streamName get() = this::class.simpleName
}
