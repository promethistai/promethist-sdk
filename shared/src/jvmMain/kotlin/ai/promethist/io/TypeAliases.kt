package ai.promethist.io

actual typealias Serializable = java.io.Serializable
actual typealias Closeable = java.io.Closeable
actual typealias InputStream = java.io.InputStream
actual typealias OutputStream = java.io.OutputStream