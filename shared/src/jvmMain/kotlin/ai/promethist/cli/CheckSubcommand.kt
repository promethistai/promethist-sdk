package ai.promethist.cli

import ai.promethist.service.CheckService
import kotlinx.cli.ExperimentalCli
import kotlinx.cli.Subcommand
import org.springframework.stereotype.Component

@OptIn(ExperimentalCli::class)
@Component
class CheckSubcommand(
    private val checkService: CheckService,
    //private val securityManager: SecurityManager
): Subcommand("check", "Check application parameters") {

    override fun execute() = with(checkService.check("ready")) {
        println("[Status: $status]\n")
        println("[Runtime]\n${runtime.text()}")
    }
}