package ai.promethist.cli

import ai.promethist.security.SecurityManager
import kotlinx.cli.ExperimentalCli
import kotlinx.cli.Subcommand
import kotlinx.coroutines.runBlocking


@OptIn(ExperimentalCli::class)
class LoginSubcommand(
    private val securityManager: SecurityManager,
): Subcommand("login", "User login") {

    override fun execute() = runBlocking {
        securityManager.deviceLogin {
            println("Open $verificationUri and enter code $userCode")
        }
        println("Succeeded")
    }
}