package ai.promethist.cli

import kotlinx.cli.*

@OptIn(ExperimentalCli::class)
@Suppress("UNCHECKED_CAST")
abstract class CliCommand(name: String) {

    abstract val subcommands: Set<Subcommand>

    private val argParser by lazy {
        ArgParser(name).apply {
            subcommands(*subcommands.toTypedArray())
        }
    }

    open fun parse(vararg args: String) = argParser.parse(if (args.isEmpty()) arrayOf("-h") else args as Array<String>)

    fun parse(line: String) = parse(*line.split(' ').toTypedArray())
}