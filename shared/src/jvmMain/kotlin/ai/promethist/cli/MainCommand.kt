package ai.promethist.cli

import kotlinx.cli.*

@OptIn(ExperimentalCli::class)
class MainCommand(name: String, override val subcommands: Set<Subcommand>) : CliCommand(name)