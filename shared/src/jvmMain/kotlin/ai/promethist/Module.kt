package ai.promethist

//import ai.promethist.common.sharedSerializer
//import ai.promethist.di.di
//import ai.promethist.io.NotFoundException
//import ai.promethist.security.UnauthorizedException
//import ai.promethist.service.CheckService
//import ai.promethist.util.LoggerDelegate
//import io.ktor.http.*
//import io.ktor.serialization.kotlinx.json.*
//import io.ktor.server.application.*
//import io.ktor.server.auth.*
//import io.ktor.server.plugins.compression.*
//import io.ktor.server.plugins.contentnegotiation.*
//import io.ktor.server.plugins.cors.routing.*
//import io.ktor.server.plugins.statuspages.*
//import io.ktor.server.response.*
//import io.ktor.server.routing.*
//import kotlinx.serialization.json.Json
//import org.kodein.di.instance
//
//fun Application.sharedModule(serializer: Json = sharedSerializer) {
//    val logger = LoggerDelegate.createLogger(Application::class.java)
//    install(Compression)
//    install(ContentNegotiation) {
//        json(serializer)
//    }
//    install(CORS) {
//        anyHost()
//        allowMethod(HttpMethod.Options)
//        allowMethod(HttpMethod.Put)
//        allowHeader(HttpHeaders.Authorization)
//        allowHeader(HttpHeaders.AccessControlAllowHeaders)
//        allowHeader(HttpHeaders.ContentType)
//        exposeHeader("*")
//        allowNonSimpleContentTypes = true
//    }
//    install(StatusPages) {
//        exception<NotFoundException> { call, e ->
//            call.respond(HttpStatusCode.NotFound, e.message ?: e.toString())
//        }
//        exception<UnauthorizedException> { call, e ->
//            call.respond(HttpStatusCode.Unauthorized, e.message ?: e.toString())
//        }
//        exception<Throwable> { call, e ->
//            val message = e.message ?: "Unknown error"
//            logger.error(message, e)
//            //monitoring.capture(message, e)
//            call.respond(HttpStatusCode.InternalServerError, message)
//        }
//    }
//    routing {
//        val checkService by di.instance<CheckService>()
//        get("/check") {
//            call.respond(checkService.check("ready", call.principal()))
//        }
//    }
//}
