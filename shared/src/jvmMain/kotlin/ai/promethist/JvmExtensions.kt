package ai.promethist

import ai.promethist.audio.AudioFormat
import ai.promethist.time.currentTime
import java.net.InetAddress
import javax.sound.sampled.AudioFormat as JavaAudioFormat

actual val platform = Platform.Jvm

actual val runtime = Runtime(
    "TODO",//config["version"],
    "TODO",//config["git.commit"],
    "Java",
    System.getProperty("java.version"),
    System.getProperty("os.arch"),
    InetAddress.getLocalHost().hostName
)

fun waitUntil(millis: Long = 0, predicate: () -> Boolean) {
    if (!predicate()) {
        val timeout = currentTime() + millis
        while (millis == 0L || currentTime() < timeout) {
            if (predicate())
                return
            Thread.sleep(5)
        }
    }
}

fun AudioFormat.toJava() = JavaAudioFormat(sampleRate.toFloat(), 16, channels, signed, bigEndian)
