package ai.promethist.messaging

import kotlinx.serialization.Serializable
import kotlin.reflect.KClass

@Serializable
data class MessageResult(val provider: KClass<out MessageSender>, val id: String, val status: MessageStatus)