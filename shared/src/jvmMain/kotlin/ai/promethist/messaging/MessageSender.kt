package ai.promethist.messaging

interface MessageSender {

    fun sendMessage(message: Message): MessageResult
}