package ai.promethist.messaging

import ai.promethist.type.Ref
import kotlinx.serialization.Serializable

@Serializable
data class Message(val id: String, val to: String, val ref: Ref?, val icon: String?, val title: String, val text: String)