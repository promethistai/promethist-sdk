package ai.promethist.messaging

enum class MessageStatus { Sent, Failed }