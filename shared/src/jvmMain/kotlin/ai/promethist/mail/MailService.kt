package ai.promethist.mail

interface MailService {

    fun sendMail(to: String, subject: String, template: String, variables: Map<String, Any>)
}
