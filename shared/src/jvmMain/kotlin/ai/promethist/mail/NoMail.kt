package ai.promethist.mail

import ai.promethist.util.LoggerDelegate

object NoMail : MailService {

    private val logger by LoggerDelegate()

    override fun sendMail(to: String, subject: String, template: String, variables: Map<String, Any>) {
        logger.warn("NOT sending mail to $to with subject $subject using template $template (no mail service configured)")
    }
}
