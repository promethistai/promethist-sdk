package ai.promethist.util

import java.io.PrintWriter

class PrintLogger(private val out: PrintWriter) : Logger {

    override fun info(msg: String) = out.println("INFO $msg")

    override fun warn(msg: String) = out.println("WARN $msg")

    override fun debug(msg: String) = out.println("DEBUG $msg")

    override fun error(msg: String) = out.println("ERROR $msg")

    override fun error(msg: String, t: Throwable) {
        error(msg)
        t.printStackTrace(out)
    }
}