package ai.promethist.util

import org.slf4j.LoggerFactory
import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.KProperty

actual class LoggerDelegate : ReadOnlyProperty<Any?, Logger> {

    private lateinit var logger: Logger

    actual override operator fun getValue(thisRef: Any?, property: KProperty<*>): Logger {
        require(thisRef != null) { "LoggerDelegate can be used only on class properties." }

        //remove $companion from logger name
        val javaClass = thisRef.javaClass
        val clazz = if (javaClass.kotlin.isCompanion) javaClass.enclosingClass else javaClass

        if (!::logger.isInitialized) logger = Slf4jLogger(createLogger(clazz))

        return logger
    }

    companion object {
        fun <T> createLogger(clazz: Class<T>) = LoggerFactory.getLogger(clazz)
    }
}