package ai.promethist.util

class Slf4jLogger(val logger: org.slf4j.Logger) : Logger {

    override fun info(msg: String) = logger.info(msg)

    override fun warn(msg: String) = logger.warn(msg)

    override fun debug(msg: String) = logger.debug(msg)

    override fun error(msg: String) = logger.error(msg)

    override fun error(msg: String, t: Throwable) = logger.error(msg, t)
}