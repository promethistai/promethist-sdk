package ai.promethist.util

import kotlinx.serialization.KSerializer
import kotlinx.serialization.Serializable
import kotlinx.serialization.descriptors.PrimitiveKind
import kotlinx.serialization.descriptors.PrimitiveSerialDescriptor
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import kotlin.js.JsExport

@Serializable(with = ZoneId.Serializer::class)
@JsExport
data class ZoneId(val id: String) {

    object Serializer: KSerializer<ZoneId> {

        override val descriptor: SerialDescriptor =
            PrimitiveSerialDescriptor("zoneId", PrimitiveKind.STRING)

        override fun deserialize(decoder: Decoder): ZoneId =
            ZoneId(id = decoder.decodeString())

        override fun serialize(encoder: Encoder, value: ZoneId) =
            encoder.encodeString(value.id)
    }
}