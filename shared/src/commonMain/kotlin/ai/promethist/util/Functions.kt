package ai.promethist.util

import kotlin.random.Random

fun Number.padLeft(length: Int): String = toString().padStart(length, '0')

fun randomString(length: Int = 30, chars: List<Char> = ('a'..'z') + ('0'..'9')) =
    (1..length).map { Random.nextInt(0, chars.size) }.map(chars::get).joinToString("")