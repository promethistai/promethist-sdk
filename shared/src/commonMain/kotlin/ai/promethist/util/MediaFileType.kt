package ai.promethist.util

interface MediaFileType {
    val name: String
    val contentType: String
}