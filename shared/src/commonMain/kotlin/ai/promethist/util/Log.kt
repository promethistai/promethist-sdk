package ai.promethist.util

import kotlinx.datetime.Instant
import kotlinx.serialization.Serializable

interface Log {

    @Serializable
    data class Entry(
        val time: Instant? = null,
        val relativeTime: Float,
        val level: Level,
        val text: String
    ) {
        enum class Level { ERROR, WARN, INFO, DEBUG, TRACE }
    }

    val logger: Logger
    val entries: MutableList<Entry>
}