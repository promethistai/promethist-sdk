package ai.promethist.util

data class Range(val startIndex: Int, val endIndex: Int = DEFAULT_END_INDEX, val unit: String = "bytes") {

    companion object {

        const val DEFAULT_END_INDEX = 999999

        val default = Range(0, DEFAULT_END_INDEX)

        fun fromString(str: String, maxEndIndex: Int = DEFAULT_END_INDEX): Range {
            val (unit, value) = str.split('=', limit = 2)
            val (startIndex, endIndex) = value.split('-', limit = 2).map {
                if (it.isBlank())
                    -1
                else
                    it.toInt()
            }
            if (endIndex == -1)
                return Range(startIndex, maxEndIndex, unit)
            return Range(startIndex, endIndex, unit)
        }
    }
}