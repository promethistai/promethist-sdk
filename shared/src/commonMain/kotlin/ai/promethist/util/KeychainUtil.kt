package ai.promethist.util

expect object KeychainUtil {

    fun saveValue(key: String, value: String): Boolean

    fun getValue(key: String): String?

    fun removeValue(key: String)
}