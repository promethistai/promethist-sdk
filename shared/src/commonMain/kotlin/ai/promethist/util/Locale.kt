package ai.promethist.util

import ai.promethist.io.NotFoundException
import kotlinx.serialization.KSerializer
import kotlinx.serialization.Serializable
import kotlinx.serialization.descriptors.PrimitiveKind
import kotlinx.serialization.descriptors.PrimitiveSerialDescriptor
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import kotlin.js.JsExport

@Serializable(with = Locale.Serializer::class)
@JsExport
data class Locale(val language: String, val country: String = "") {

    fun isSimilarTo(locale: Locale) = language == locale.language && (country.isBlank() || country == locale.country)

    fun toLanguageTag() = language + (if (country.isNotEmpty()) "-$country" else "")

    fun withCountryOrDefault() : Locale {
        if(country.isNotBlank()){
            return this
        }
        for(supportedLocale in supportedLocales){
            if(language == supportedLocale.language){
                return supportedLocale
            }
        }
        error("Default country for $language is not defined in supportedLocales")
    }

    object Serializer: KSerializer<Locale> {

        override val descriptor: SerialDescriptor = PrimitiveSerialDescriptor("locale", PrimitiveKind.STRING)

        override fun deserialize(decoder: Decoder): Locale {
            val parts = decoder.decodeString().split('_')
            return Locale(parts[0], if (parts.size > 1) parts[1] else "")
        }

        override fun serialize(encoder: Encoder, value: Locale) =
            encoder.encodeString(value.language + (if (value.country.isNotEmpty()) "_${value.country}" else ""))
    }

    companion object {

        val en_US = Locale("en", "US")
        val en_GB = Locale("en", "GB")
        val cs_CZ = Locale("cs", "CZ")
        val empty = Locale("", "")

        val supportedLocales = listOf(en_US, en_GB, cs_CZ)

        fun forLanguageTag(tag: String): Locale {
            val parts = tag.replace('_', '-').split('-')
            return Locale(parts[0], if (parts.size > 1) parts[1] else "")
        }
    }
}
