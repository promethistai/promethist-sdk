package ai.promethist.util

/**
 * Simple implementation of a cyclic buffer
 */
class ByteBuffer(size: Int = 1024 * 1024) {
    private val buffer = ByteArray(size)
    private var readIndex = 0
    private var writeIndex = 0
    val count: Int
        get() =
            if (writeIndex >= readIndex)
                writeIndex - readIndex
            else
                buffer.size - readIndex + writeIndex

    fun write(array: ByteArray, offset: Int, count: Int) {
        for (index in 0 until count) {
            write(array[index + offset])
        }
    }

    fun write(b: Byte) {
        buffer[writeIndex++] = b
        if (writeIndex == buffer.size)
            writeIndex = 0
        if (writeIndex == readIndex)
            error("Buffer is full (${buffer.size} bytes)")
    }

    fun write(array: ByteArray) = write(array, 0, array.size)

    fun read(array: ByteArray, offset: Int, count: Int): Int {
        var index = 0
        while (index < count) {
            array[offset + index++] = buffer[readIndex++]
            if (readIndex == buffer.size)
                readIndex = 0
            if (readIndex == writeIndex)
                break
        }
        return index
    }

    fun read(array: ByteArray) = read(array, 0, array.size)

    fun read(count: Int) = ByteArray(if (this.count < count) this.count else count).also {
        read(it, 0, it.size)
    }

    fun read(size: Int, rest: Boolean = true, callback: ((ByteArray, Int) -> Unit)) {
        val block = ByteArray(size)
        while (count >= block.size) {
            read(block, 0, block.size)
            callback(block, block.size)
        }
        if (rest) {
            val c = count // Store count value before read operation
            read(block, 0, c)
            callback(block, c)
        }
    }
}