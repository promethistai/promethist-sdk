package ai.promethist.util

import kotlinx.coroutines.flow.Flow

expect object NetworkUtil {
    fun observeHasConnection(): Flow<Boolean>
    fun isConnected(): Boolean
}