package ai.promethist.util

import kotlin.reflect.KClass

object CollectionName {
    fun default(clazz: KClass<*>): String = useCamelCase(clazz)

    fun useCamelCase(clazz: KClass<*>): String {
        return clazz.simpleName!!
            .toCharArray()
            .run {
                foldIndexed(StringBuilder()) { i, s, c ->
                    s.append(
                        if (c.isUpperCase() && (i == 0 || this[i - 1].isUpperCase())) {
                            c.lowercaseChar()
                        } else {
                            c
                        }
                    )
                }.toString()
            }
    }
}