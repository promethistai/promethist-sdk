package ai.promethist.video

data class VideoTurnState(
    var currentlyProcessing: Int = 0,
    var currentIndex: Int = 0
)
