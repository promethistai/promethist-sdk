package ai.promethist.video

import ai.promethist.channel.item.OutputItem
import kotlinx.serialization.Serializable

@Serializable
data class VideoRequest(
    val videoMode: VideoMode,
    val outputItems: List<OutputItem>,
    val turnId: String? = "unknown"
)