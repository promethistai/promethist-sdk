package ai.promethist.video

import kotlinx.serialization.Serializable

@Serializable
data class VideoResponse(
    val videoMode: VideoMode,
    val videoHash: String,
    val videoUrl: String,
)