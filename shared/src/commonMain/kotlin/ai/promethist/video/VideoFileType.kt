package ai.promethist.video

import ai.promethist.util.MediaFileType
import kotlinx.serialization.Serializable

@Serializable
enum class VideoFileType(override val contentType: String) : MediaFileType {
    mp4("video/mp4"),
    flv("video/x-flv"),
    ts("video/mp2t"),
}