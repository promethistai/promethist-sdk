package ai.promethist.video

import ai.promethist.client.resources.Resources
import dev.icerock.moko.resources.desc.ResourceStringDesc
import kotlinx.serialization.Serializable

@Serializable
enum class VideoMode(val fileType: VideoFileType?) {
    None(null),
    Streamed(VideoFileType.flv),
    OnDeviceRendering(null);

    val localizedName: ResourceStringDesc get() = when (this.name) {
        None.name -> Resources.strings.settings.videoModeNone
        Streamed.name -> Resources.strings.settings.videoModeStreamed
        OnDeviceRendering.name -> Resources.strings.settings.videoModeOnDevice
        else -> Resources.strings.settings.videoModeNone
    }
}