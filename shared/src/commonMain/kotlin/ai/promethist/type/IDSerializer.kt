package ai.promethist.type

import kotlinx.serialization.KSerializer
import kotlinx.serialization.descriptors.PrimitiveKind
import kotlinx.serialization.descriptors.PrimitiveSerialDescriptor
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder

class IDSerializer : KSerializer<ID> {

    override val descriptor: SerialDescriptor = PrimitiveSerialDescriptor("id", PrimitiveKind.STRING)

    override fun deserialize(decoder: Decoder) = id(decoder.decodeString())

    override fun serialize(encoder: Encoder, value: ID) = encoder.encodeString(value.toString())
}