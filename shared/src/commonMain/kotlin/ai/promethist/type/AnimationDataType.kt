package ai.promethist.type

enum class AnimationDataType { Visemes, BlendShapes, None }