package ai.promethist.type

import ai.promethist.Textual
import kotlinx.serialization.Serializable

@Serializable
open class Token(override val text: String) : Textual {
    override fun toString() = "Token(text=$text)"
}