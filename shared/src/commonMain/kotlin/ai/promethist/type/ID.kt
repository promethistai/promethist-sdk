package ai.promethist.type

expect class ID

expect fun newId(): ID

expect fun id(s: String): ID
