package ai.promethist.type

import kotlinx.serialization.KSerializer
import kotlinx.serialization.Serializable
import kotlinx.serialization.descriptors.PrimitiveKind
import kotlinx.serialization.descriptors.PrimitiveSerialDescriptor
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder

@Serializable(with = Ref.Serializer::class)
data class Ref(val value: String) : ai.promethist.io.Serializable {

    override fun toString() = value
    fun toPackageName() = value.replace('-', '_')
    fun toClassName() = value
        .split("-")
        .joinToString("") { it.replaceFirstChar(Char::uppercase) }

    companion object {
        fun from(id: ID) = Ref(id.toString())
    }

    object Serializer: KSerializer<Ref> {

        override val descriptor: SerialDescriptor = PrimitiveSerialDescriptor("ref", PrimitiveKind.STRING)
        override fun deserialize(decoder: Decoder) = Ref(decoder.decodeString())
        override fun serialize(encoder: Encoder, value: Ref) = encoder.encodeString(value.value)
    }
}