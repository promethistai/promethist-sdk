package ai.promethist.type

interface Entity {
    val id: ID
}