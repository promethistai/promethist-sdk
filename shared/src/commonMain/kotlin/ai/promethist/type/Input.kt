package ai.promethist.type

interface Input<T> {

    fun toObject(): T
}