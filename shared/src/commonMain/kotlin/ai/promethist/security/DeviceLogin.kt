package ai.promethist.security

interface DeviceLogin {
    val userCode: String
    val verificationUri: String
}