package ai.promethist.security

const val API_KEY_HEADER = "X-Api-Key"
const val API_KEY_PARAMETER = "apiKey"