package ai.promethist.security

interface SecurityManager {
    fun deviceLogin(block: DeviceLogin.() -> Unit)
    val userPrincipal: Any?
}