package ai.promethist.security

expect object Digest {
    fun md5(input: ByteArray): String
    fun md5(input: String): String
    fun md5(): String
    fun sha1(input: ByteArray): String
    fun sha1(input: String): String
    fun sha1(): String
}