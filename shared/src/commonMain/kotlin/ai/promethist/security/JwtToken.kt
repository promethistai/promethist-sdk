package ai.promethist.security

import kotlinx.serialization.Serializable

@Serializable
data class JwtToken(
    val subject: String, // retrieved from jwt token `sub` claim, it's user unique id
    val issuer: String,
) {
    override fun toString() = "${this::class.simpleName}(subject=$subject)"
}