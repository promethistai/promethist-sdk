package ai.promethist

import ai.promethist.util.ZoneId
import ai.promethist.util.Locale
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonPrimitive

@Serializable
data class Input(
    val zoneId: ZoneId = Defaults.zoneId,
    var locale: Locale = Defaults.locale,
    val transcript: Transcript = Transcript("#silence"),
    val alternatives: List<Transcript> = emptyList(),
    val attributes: Map<String, JsonPrimitive> = mapOf(),
    val classes: MutableList<Class> = mutableListOf(),
    var action: String? = null
) {
    @Serializable
    open class Class(val type: Type, val name: String, val score: Float = 1.0F, val modelId: String = "") {
        enum class Type { Intent, Entity, Sentiment, Profanity, NegativeThought, DialogueAct }

        override fun toString(): String {
            return "Class(type=$type, name='$name', score=$score, modelId='$modelId')"
        }
    }
}
