package ai.promethist.image

import ai.promethist.Hashable
import ai.promethist.security.Digest
import kotlinx.serialization.Serializable

@Serializable
data class ImageRequestConfig(
    var provider: String,
    ) : Hashable {
    override fun hash() = Digest.md5(provider)
}