package ai.promethist.image

import ai.promethist.io.InputStream

interface ImageIntegration {
    val name: String
    fun inputStream(imageRequest: ImageRequest): InputStream
}