package ai.promethist.image

import ai.promethist.Hashable
import ai.promethist.channel.item.HashableItem
import ai.promethist.model.TtsConfig
import ai.promethist.nlp.llm.Prompt
import ai.promethist.security.Digest
import kotlinx.serialization.Serializable
import kotlin.io.encoding.Base64

@Serializable
data class ImageRequest(
    val config: ImageRequestConfig,
    val prompt: String,
    val imagePrompt: String? = null,
    val negativePrompt: String? = null
) : Hashable {
    override fun hash() = Digest.md5(prompt + imagePrompt + negativePrompt + config.hash())


    override fun toString(): String {
        return "${this::class.simpleName}(config=$config, prompt=$prompt, imagePrompt=${
            if (imagePrompt == null) {
                "null"
            } else {
                if (imagePrompt.length > 30) {
                    imagePrompt.subSequence(0, 30)
                } else {
                    imagePrompt
                }
            }
        }..., negativePrompt=${negativePrompt})"
    }
}