package ai.promethist

enum class Platform {
    Android, Ios, Jvm, Js
}