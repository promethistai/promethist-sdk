package ai.promethist.nlp.llm

interface LargeLanguageModel {
    // Needs Context for chat methods

    fun complete(prompt: String, stop: List<String> = listOf(), config: LLMConfig = LLMConfig()): String

    fun chat(
        numTurns: Int = 5,
        prompt: String = "The following is a conversation with an AI assistant. The assistant is helpful, creative, clever, and very friendly.",
        personaName: String = "System",
        config: LLMConfig = LLMConfig()
    ): String

    fun chatSegmented(numTurns: Int = 5,
                      prompt: String = "The following is a conversation with an AI assistant. The assistant is helpful, creative, clever, and very friendly.",
                      personaName: String = "System",
                      config: LLMConfig = LLMConfig()
    ): List<String>

    fun classify(
        input: String,
        examples: List<Pair<String, String>>,
        prompt: String = "Classify text.",
        config: LLMConfig = LLMConfig()
    ): String

    fun detect(
        input: String,
        prompt: String = "Extract following entities from the provided text, if the text contains them.",
        entityDescription: String,
        examples: List<Pair<String, List<String>>> = listOf(),
        config: LLMConfig = LLMConfig()
    ): List<String>
}