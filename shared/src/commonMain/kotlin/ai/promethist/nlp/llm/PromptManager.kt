package ai.promethist.nlp.llm

object PromptManager {
    // We want to have a prompt collection editable from the Studio interface
    // Ideally, the prompts required for the application need to be packed into the application archive
    // There is a set of required prompts used for standard components. These prompt need to be created in the database if they are not already there
    private val promptSet = mutableSetOf<Prompt>()

    fun register(prompt: Prompt) {
        promptSet.add(prompt)
    }

    fun get(promptName: String) = promptSet.first { it.name == promptName }

    fun get(promptName: BuiltInPromptNames) = get(promptName.name)

    enum class BuiltInPromptNames {
        REPEAT,
        PERSONA_PROFILE,
        USER_PROFILE,
        QUESTION_ANSWERING
    }
}