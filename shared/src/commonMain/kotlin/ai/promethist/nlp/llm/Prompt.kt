package ai.promethist.nlp.llm

import ai.promethist.util.Locale
import kotlinx.serialization.Serializable

@Serializable
open class Prompt(val name: String,
                  val template: String,
                  val locale: Locale,
                  val version: Long,
                  // We need to define for which model the prompt is relevant. Applicable to all models if an empty list
                  val models: List<String> = listOf()
) {
//    val value get() = template // TODO template expansion
}

fun String.toPrompt() = Prompt("", this, Locale.en_US, 1)