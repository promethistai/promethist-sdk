package ai.promethist.nlp.llm

data class LLMConfig(
    val model: String = "gpt-3.5-turbo",
    val temperature: Float = 0.9F,
    val maxTokens: Int = 150,
    val topP: Float = 1.0F,
    val frequencyPenalty: Float = 0.0F,
    val presencePenalty: Float = 0.6F
)