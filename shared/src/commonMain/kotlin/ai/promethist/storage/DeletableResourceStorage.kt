package ai.promethist.storage

interface DeletableResourceStorage {

    fun delete(path: String): Boolean
}