package ai.promethist.storage

interface ResourceStorage : ReadableResourceStorage, WritableResourceStorage