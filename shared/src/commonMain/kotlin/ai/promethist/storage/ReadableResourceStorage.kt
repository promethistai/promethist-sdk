package ai.promethist.storage

import ai.promethist.io.*
import ai.promethist.util.Logger

interface ReadableResourceStorage {

    fun get(path: String): Resource

    fun contains(path: String): Boolean =
        try {
            get(path)
            true
        } catch (e: NotFoundException) {
            false
        }

    fun inputStream(path: String): InputStream

    fun copy(path: String, outputStream: OutputStream): Int =
        inputStream(path).use {
            it.copyTo(outputStream)
        }

    fun read(path: String): ByteArray {
        val buf = ByteArrayOutputStream()
        inputStream(path).use {
            it.copyTo(buf)
        }
        return buf.byteArray.copyOfRange(0, buf.size)
    }

    fun list(path: String, recursive: Boolean = false): List<Resource>

    fun copy(
        path: String,
        targetStorage: WritableResourceStorage,
        targetPath: String = path,
        extraOutputStream: OutputStream? = null,
        logger: Logger
    ): Int {
        var count = 0
        val item = get(path)
        if (item.isDirectory) {
            list(path).forEach {
                count += copy("$path/${it.name}", targetStorage, "$targetPath/${it.name}", extraOutputStream, logger)
            }
        } else {
            logger.info("Copying $this/$path to $targetStorage/$targetPath")
            inputStream(path).use { inputStream ->
                targetStorage.outputStream(targetPath).use { outputStream: OutputStream ->
                    if (extraOutputStream != null)
                        inputStream.copyTo(outputStream, extraOutputStream)
                    else
                        inputStream.copyTo(outputStream)
                }
            }
            count++
        }
        return count
    }
}