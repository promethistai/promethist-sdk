package ai.promethist.storage

import ai.promethist.io.*
import io.ktor.utils.io.charsets.*
import io.ktor.utils.io.core.toByteArray

interface WritableResourceStorage {

    fun outputStream(path: String, metadata: Map<String, String>? = null): OutputStream

    fun write(path: String, inputStream: InputStream, metadata: Map<String, String>? = null) {
        outputStream(path, metadata).use { outputStream ->
            inputStream.copyTo(outputStream)
        }
    }

    fun write(path: String, bytes: ByteArray, metadata: Map<String, String>? = null) =
        write(path, ByteArrayInputStream(bytes), metadata)

    fun write(path: String, text: String, metadata: Map<String, String>? = null): Unit =
        write(path, text.toByteArray(Charset.forName("UTF-8")), metadata)
}
