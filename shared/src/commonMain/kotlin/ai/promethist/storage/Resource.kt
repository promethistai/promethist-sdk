package ai.promethist.storage

import ai.promethist.time.currentTime

data class Resource(
    val path: String,
    val name: String,
    var size: Int,
    val metadata: Map<String, String> = emptyMap(),
    val isDirectory: Boolean = false,
    var lastModified: Long = currentTime(),
    var isCompleted: Boolean = true,
    var createdTime: Long = currentTime()
)