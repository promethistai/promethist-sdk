package ai.promethist

interface Hashable {
    fun hash(): String
}