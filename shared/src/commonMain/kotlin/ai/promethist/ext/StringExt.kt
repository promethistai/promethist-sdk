package ai.promethist.ext

val String.isCommand: Boolean get() = this.startsWith("#")

val String.withLeadingZero: String get() = if (this.count() == 1) "0$this" else this

val String.fileName: String? get() = this.split("/").lastOrNull()