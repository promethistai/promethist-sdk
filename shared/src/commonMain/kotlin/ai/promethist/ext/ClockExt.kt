package ai.promethist.ext

import kotlinx.datetime.Clock
import kotlinx.datetime.LocalDateTime
import kotlinx.datetime.TimeZone
import kotlinx.datetime.toLocalDateTime

val LocalDateTime.Companion.now get() =
    Clock.System.now().toLocalDateTime(TimeZone.currentSystemDefault())