package ai.promethist.ext

import kotlinx.datetime.LocalDateTime

val LocalDateTime.formatShort: String get() =
    "${this.hour.toString().withLeadingZero}:${this.minute.toString().withLeadingZero}"


val LocalDateTime.formatMedium: String get() {
    val date = "${this.dayOfMonth.toString().withLeadingZero}. ${this.monthNumber.toString().withLeadingZero}."
    val time = "${this.hour.toString().withLeadingZero}:${this.minute.toString().withLeadingZero}"
    return "$date $time"
}