package ai.promethist.client.avatar.unity

import ai.promethist.client.avatar.AvatarEventsApi
import ai.promethist.client.avatar.AvatarPlayerController
import ai.promethist.client.avatar.AvatarPose
import ai.promethist.client.avatar.AvatarQualityLevel
import ai.promethist.client.common.LoggerV3

class AvatarEventsApiV1Impl(
    private val controller: AvatarPlayerController
) : AvatarEventsApi {

    override fun playAudio(base64: String) {
        val sizeKb = getSizeInKilobytesFromBase64(base64)
        LoggerV3.arc("Sending => playAudio - size: ${sizeKb}kb length: ${base64.count()}")
        emitEvent(AvatarEventV1.ReceiveData(base64 = base64))
    }

    override fun pauseAudio() {
        LoggerV3.arc("Sending => pauseAudio")
        emitEvent(AvatarEventV1.PauseAudio)
    }

    override fun stopAudio() {
        LoggerV3.arc("Sending => stopAudio")
        emitEvent(AvatarEventV1.StopAudio)
    }

    override fun resumeAudio() {
        LoggerV3.arc("Sending => resumeAudio")
        emitEvent(AvatarEventV1.ResumeAudio)
    }

    override fun skipAudio() {
        LoggerV3.arc("Sending => skipAudio")
        emitEvent(AvatarEventV1.SkipAudio)
    }

    override fun setPersona(personaId: String, withFirstContact: Boolean) {
        LoggerV3.arc("Sending => setPersona: $personaId $withFirstContact")
        emitEvent(AvatarEventV1.SetPersona(personaId, withFirstContact))
    }

    override fun turnEnd() {
        LoggerV3.arc("Sending => turnEnd")
        emitEvent(AvatarEventV1.TurnEnd)
    }

    override fun setQuality(avatarQualityLevel: AvatarQualityLevel) {
        LoggerV3.arc("Sending => setQuality ${avatarQualityLevel.name} (${avatarQualityLevel.level})")
        emitEvent(AvatarEventV1.SetQualityLevel(avatarQualityLevel.level.toString()))
    }

    override fun setBufferDuration(duration: String) {
        LoggerV3.arc("Sending => setBufferDuration $duration")
        emitEvent(AvatarEventV1.SetBufferDuration(duration))
    }

    override fun audioEnd() {
        LoggerV3.arc("Sending => audioEnd")
        emitEvent(AvatarEventV1.AudioEnd)

    }

    override fun requestTechnicalReport() {
        LoggerV3.arc("Sending => requestTechnicalReport")
        emitEvent(AvatarEventV1.RequestTechnicalReport)
    }

    override fun setPose(pose: AvatarPose) {
        LoggerV3.arc("Sending => setPose $pose")
        emitEvent(AvatarEventV1.SetPose(pose.name.lowercase()))
    }

    private fun emitEvent(event: AvatarEventV1) {
        controller.sendMessage(
            nameObject = event.nameObject,
            functionName = event.functionName,
            message = event.message
        )
    }
}

private fun getSizeInKilobytesFromBase64(base64String: String): Double {
    val base64Length = base64String.replace("=", "").length
    if (base64Length == 0) return 0.0

    val sizeInBytes = (base64Length * 3) / 4
    return sizeInBytes.toDouble() / 1024
}