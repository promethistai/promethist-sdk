package ai.promethist.client.avatar

import ai.promethist.client.ClientEvent
import ai.promethist.client.ClientModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

abstract class AvatarLink(
    private val updateWholeState: Boolean
) {
    abstract val clientModel: ClientModel
    abstract val uiStateHandler: CoroutinesFlowUiStateHandler
    private val stateCollectionScope = CoroutineScope(Job())

    open fun start() {
        clientModel.start()
        stateCollectionScope.launch {
            var last: Any? = null
            var lastError: Any? = null
            uiStateHandler.uiState.collectLatest { state ->
                if (updateWholeState) {
                    handleStateUpdate(state)
                    last = state
                } else {
                    if (state.visualItem != null && state.visualItem != last) {
                        handleStateUpdate(state.visualItem)
                        last = state.visualItem
                    }
                    if (state.error != null && state.error != lastError) {
                        handleStateUpdate(state.error)
                        lastError = state.error
                    }
                }
            }
        }
    }

    fun handleClientEvent(event: ClientEvent) = clientModel.handleEvent(event)

    abstract suspend fun handleStateUpdate(obj: Any)
    abstract suspend fun handleAudio(audio: ByteArray)
    abstract suspend fun handleTurnEnd()
    abstract suspend fun handleAvatar(ref: String)
}