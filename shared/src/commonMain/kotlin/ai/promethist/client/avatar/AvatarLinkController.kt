package ai.promethist.client.avatar

interface AvatarLinkController {
    val link: AvatarLink
}