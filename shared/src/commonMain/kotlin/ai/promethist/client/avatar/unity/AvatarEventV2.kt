package ai.promethist.client.avatar.unity

import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonClassDiscriminator

@OptIn(ExperimentalSerializationApi::class)
@Serializable
@JsonClassDiscriminator("type")
sealed class AvatarEventV2 {

    @Serializable
    @SerialName("QueueSpeech")
    data class QueueSpeechEvent(  // TODO params?
        val parameters: Parameters
    ): AvatarEventV2() {
        @Serializable
        data class Parameters(val data: String)
    }

    @Serializable
    @SerialName("PausePlayback")
    data object PausePlaybackEvent: AvatarEventV2()

    @Serializable
    @SerialName("StopPlayback")
    data object StopPlaybackEvent: AvatarEventV2()

    @Serializable
    @SerialName("ResumePlayback")
    data object ResumePlaybackEvent: AvatarEventV2()

    @Serializable
    @SerialName("ExitTurn")
    data object ExitTurnEvent: AvatarEventV2()
}