package ai.promethist.client.avatar.unity

sealed class AvatarEventV1(
    val nameObject: String,
    val functionName: String,
    val message: String = ""
) {
    data class PlayAudio(val base64: String) : AvatarEventV1(
        nameObject = "AudioManager",
        functionName = "playAudio",
        message = base64
    )

    data object PauseAudio : AvatarEventV1(
        nameObject = "AudioManager",
        functionName = "pauseAudio"
    )

    data object StopAudio : AvatarEventV1(
        nameObject = "AudioManager",
        functionName = "stopAudio"
    )

    data object ResumeAudio : AvatarEventV1(
        nameObject = "AudioManager",
        functionName = "resumeAudio"
    )

    data object SkipAudio : AvatarEventV1(
        nameObject = "AudioManager",
        functionName = "skipAudio"
    )

    data class SetPersona(val personaId: String, val withFirstContact: Boolean) : AvatarEventV1(
        nameObject = "PersonaManager",
        functionName = "setPersona",
        message = "$personaId;$withFirstContact"
    )

    data object TurnEnd : AvatarEventV1(
        nameObject = "AudioManager",
        functionName = "turnEnd"
    )

    data object AudioEnd : AvatarEventV1(
        nameObject = "AudioManager",
        functionName = "audioEnd"
    )

    class ReceiveData(base64: String) : AvatarEventV1(
        nameObject = "AudioManager",
        functionName = "receiveBinaryData",
        message = base64
    )

    class SetBufferDuration(duration: String) : AvatarEventV1(
        nameObject = "AudioManager",
        functionName = "setBufferDuration",
        message = duration
    )

    data class SetQualityLevel(val qualityLevel: String) : AvatarEventV1(
        nameObject = "SceneLoader",
        functionName = "setQualityLevel",
        message = qualityLevel
    )

    data object RequestTechnicalReport : AvatarEventV1(
        nameObject = "SceneLoader",
        functionName = "requestTechnicalReport"
    )

    data class SetPose(val pose: String) : AvatarEventV1(
        nameObject = "PersonaManager",
        functionName = "setPose",
        message = pose
    )
}