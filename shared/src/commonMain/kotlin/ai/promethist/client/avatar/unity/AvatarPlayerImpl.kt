package ai.promethist.client.avatar.unity

import ai.promethist.Platform
import ai.promethist.client.Client
import ai.promethist.client.model.PersonaId
import ai.promethist.client.avatar.AvatarEventsApi
import ai.promethist.client.avatar.AvatarPlayer
import ai.promethist.client.avatar.AvatarPlayerBridge
import ai.promethist.client.avatar.AvatarPlayerCallback
import ai.promethist.client.avatar.AvatarPlayerController
import ai.promethist.client.avatar.AvatarPose
import ai.promethist.client.avatar.AvatarQualityLevel
import ai.promethist.client.speech.LowPassFilter
import ai.promethist.client.ClientLifecycle
import ai.promethist.client.ClientLifecycleObserver
import ai.promethist.platform
import io.ktor.util.encodeBase64
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlin.time.Duration.Companion.seconds

class AvatarPlayerImpl(
    private val controller: AvatarPlayerController,
    override val api: AvatarEventsApi
) : AvatarPlayer, AvatarPlayerBridge {

    private val avatarScope = CoroutineScope(Job())
    private val lifecycleObserver: ClientLifecycle = ClientLifecycleObserver
    private var callback: AvatarPlayerCallback? = null
    private var currentPersonaId: PersonaId = PersonaId.Poppy
    private var currentAvatarPose: AvatarPose = AvatarPose.Idle
    private var _isPersonaSet: Boolean = false
    private val lowPassFilter = LowPassFilter()
    private val isAudioLowPassEnabled
        get() = Client.appConfig().duplexEnabled && platform == Platform.Android

    override var isPlaying: Boolean = false
        private set

    override var isLoadingAvatar: Boolean = false
        private set

    override val isPersonaSet: Boolean
        get() = _isPersonaSet

    override fun setCallback(callback: AvatarPlayerCallback) {
        this.callback = callback
    }

    override fun onAudioBytesReceived(data: ByteArray) {
        if (controller.getIsAudioBlocked()) {
            return
        }

        isPlaying = true
        val processedAudio = if (isAudioLowPassEnabled) {
            lowPassFilter.applyLowPass(data)
        } else {
            data
        }
        controller.getIsAudioBlocked()
        api.playAudio(processedAudio.encodeBase64())
    }

    override fun stopAudio() {
        api.stopAudio()
    }

    override fun setAudioBlocked(isBlocked: Boolean) {
        controller.setAudioBlocked(isBlocked)
    }

    override fun pauseAudio() {
        api.pauseAudio()
    }

    override fun resumeAudio() {
        api.resumeAudio()
    }

    override fun skip() {
        api.skipAudio()
    }

    override fun reset() {
        stopAudio()
        isPlaying = false
    }

    override fun setPersona(personaId: PersonaId, withFirstContact: Boolean) {
        if (personaId == currentPersonaId) return
        currentPersonaId = personaId
        _isPersonaSet = true
        isLoadingAvatar = true
        api.setPersona(personaId.avatarId, withFirstContact)
    }

    override fun turnEnd() {
        api.turnEnd()
    }

    override fun audioEnd() {
        api.audioEnd()
    }

    override fun setAvatarQuality(value: AvatarQualityLevel) {
        api.setQuality(value)
    }

    override fun setBufferDuration(duration: Float) {
        api.setBufferDuration(duration.toString())
    }

    override fun requestTechnicalReport() {
        avatarScope.launch {
            delay(2.seconds)
            api.requestTechnicalReport()
        }
    }

    override fun refreshAvatarQuality() {
        val avatarQualityLevel = Client.appConfig().avatarQualityLevel
        setAvatarQuality(avatarQualityLevel)
    }

    override fun setPose(pose: AvatarPose) {
        if (pose == currentAvatarPose) {
            return
        }

        api.setPose(pose)
        currentAvatarPose = pose
    }

    // UnityPlayer bridge --------------------------------------------------------------------------

    override fun onMessageReceived(message: String) {
        val avatarMessage = AvatarMessage.fromString(message)
        when (avatarMessage) {
            AvatarMessage.AudioStarted -> {
                callback?.onAudioPlaybackStarted()
                lifecycleObserver.onPlaybackStart()
            }
            AvatarMessage.TurnEnded -> {
                isPlaying = false
            }
            AvatarMessage.AvatarLoaded -> isLoadingAvatar = false
            AvatarMessage.SceneLoaded -> {
                controller.onSceneLoaded()
                refreshAvatarQuality()
            }
            AvatarMessage.AudioEnded -> Unit
            AvatarMessage.DownloadStarted -> Unit
            AvatarMessage.DownloadProgress -> Unit
            AvatarMessage.DownloadFailed -> Unit
            AvatarMessage.DownloadEnded -> Unit
            AvatarMessage.TechnicalReport -> {
                callback?.onTechnicalReport(avatarMessage.params)
                lifecycleObserver.onAvatarTechnicalReportReceived(avatarMessage.params)
            }
            AvatarMessage.AvatarLoadStarted -> Unit
            AvatarMessage.AvatarLoadFailed -> Unit
            AvatarMessage.Unknown -> Unit
        }
    }
}