package ai.promethist.client.avatar.unity

import ai.promethist.client.avatar.AvatarEventsApi
import ai.promethist.client.avatar.AvatarPlayerController
import ai.promethist.client.avatar.AvatarPose
import ai.promethist.client.avatar.AvatarQualityLevel
import ai.promethist.common.sharedSerializer
import kotlinx.serialization.encodeToString

class AvatarEventsApiV2Impl(
    private val controller: AvatarPlayerController
) : AvatarEventsApi {

    override fun playAudio(base64: String) {
        emitEvent(AvatarEventV2.QueueSpeechEvent(
            AvatarEventV2.QueueSpeechEvent.Parameters(
            data = base64
        )))
    }

    override fun pauseAudio() {
        emitEvent(AvatarEventV2.PausePlaybackEvent)
    }

    override fun stopAudio() {
        emitEvent(AvatarEventV2.StopPlaybackEvent)
    }

    override fun resumeAudio() {
        emitEvent(AvatarEventV2.ResumePlaybackEvent)
    }

    override fun skipAudio() {
        TODO("Not yet implemented")
    }

    override fun setPersona(personaId: String, withFirstContact: Boolean) {
        TODO("Not yet implemented")
    }

    override fun turnEnd() {
        emitEvent(AvatarEventV2.ExitTurnEvent)
    }

    override fun setQuality(avatarQualityLevel: AvatarQualityLevel) {
        TODO("Not yet implemented")
    }

    override fun setBufferDuration(duration: String) {
        TODO("Not yet implemented")
    }

    override fun audioEnd() {
        TODO("Not yet implemented")
    }

    override fun requestTechnicalReport() {
        TODO("Not yet implemented")
    }

    override fun setPose(pose: AvatarPose) {
        TODO("Not yet implemented")
    }

    private fun emitEvent(event: AvatarEventV2) {
        controller.sendMessage(
            nameObject = "ClientApi",
            functionName = "SendCommand",
            message = sharedSerializer.encodeToString(event)
        )
    }
}