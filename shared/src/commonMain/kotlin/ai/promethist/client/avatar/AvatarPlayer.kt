package ai.promethist.client.avatar

import ai.promethist.client.model.PersonaId

interface AvatarPlayer {

    val api: AvatarEventsApi
    val isPlaying: Boolean
    val isLoadingAvatar: Boolean
    val isPersonaSet: Boolean

    fun setCallback(callback: AvatarPlayerCallback)
    fun onAudioBytesReceived(data: ByteArray)
    fun stopAudio()
    fun setAudioBlocked(isBlocked: Boolean)
    fun pauseAudio()
    fun resumeAudio()
    fun skip()
    fun reset()
    fun setPersona(personaId: PersonaId, withFirstContact: Boolean)
    fun turnEnd()
    fun audioEnd()
    fun setAvatarQuality(value: AvatarQualityLevel)
    fun setBufferDuration(duration: Float)
    fun requestTechnicalReport()
    fun refreshAvatarQuality()
    fun setPose(pose: AvatarPose)
}