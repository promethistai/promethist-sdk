package ai.promethist.client.avatar

enum class AvatarPose {
    Idle, Processing, Responding, Listening
}