package ai.promethist.client.avatar

interface AvatarEventsApi {
    fun playAudio(base64: String)
    fun pauseAudio()
    fun stopAudio()
    fun resumeAudio()
    fun skipAudio()
    fun setPersona(personaId: String, withFirstContact: Boolean)
    fun turnEnd()
    fun setQuality(avatarQualityLevel: AvatarQualityLevel)
    fun setBufferDuration(duration: String)
    fun audioEnd()
    fun requestTechnicalReport()
    fun setPose(pose: AvatarPose)
}