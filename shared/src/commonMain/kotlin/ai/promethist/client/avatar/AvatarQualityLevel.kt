package ai.promethist.client.avatar

import kotlinx.serialization.Serializable
import kotlin.js.JsExport

@JsExport
@Serializable
enum class AvatarQualityLevel(val level: Int) {
    VeryLow(0),
    Low(1),
    Medium(2),
    High(3),
    VeryHigh(4),
    Ultra(5),
}