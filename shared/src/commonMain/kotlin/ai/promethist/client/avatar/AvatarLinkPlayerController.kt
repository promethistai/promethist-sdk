package ai.promethist.client.avatar

import ai.promethist.client.utils.Base64.decodeFromBase64
import ai.promethist.client.avatar.unity.AvatarEventV1
import ai.promethist.client.avatar.unity.AvatarMessage
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class AvatarLinkPlayerController(
    override val link: AvatarLink
): AvatarPlayerController, AvatarLinkController {

    override lateinit var bridge: AvatarPlayerBridge
    private val scope = CoroutineScope(Job())

    init {
        // Simulate Unity launch
        scope.launch {
            delay(1000)
            bridge.onMessageReceived(AvatarMessage.SceneLoaded.name)
        }
    }

    override fun sendMessage(nameObject: String, functionName: String, message: String) {
        //TODO("Not yet implemented")

        // Simulate Unity reacting to native communication
        when (functionName) {
            AvatarEventV1.SetPersona("", false).functionName -> {
                scope.launch {
                    //delay(500)
                    bridge.onMessageReceived(AvatarMessage.AvatarLoaded.name)
                    link.handleAvatar(message)
                }
            }
            AvatarEventV1.TurnEnd.functionName -> {
                scope.launch {
                    link.handleTurnEnd()
                    //delay(1000)
                    //bridge.onMessageReceived(AvatarMessage.TurnEnded.name)
                }
            }
            AvatarEventV1.PlayAudio("").functionName -> {
                scope.launch {
                    link.handleAudio(message.decodeFromBase64())
                }
            }
            AvatarEventV1.ReceiveData("").functionName -> {
                scope.launch {
                    link.handleAudio(message.decodeFromBase64())
                }
            }
        }
    }

    override fun onSceneLoaded() {
        //TODO("Not yet implemented")
    }

    override fun setAudioBlocked(isBlocked: Boolean) {
        //TODO("Not yet implemented")
    }

    override fun getIsAudioBlocked(): Boolean {
        //TODO("Not yet implemented")
        return false
    }

    // TODO: Implement received message from ARC on desktop
    fun onMessageReceived() {
        //bridge?.onMessageReceived()
    }
}