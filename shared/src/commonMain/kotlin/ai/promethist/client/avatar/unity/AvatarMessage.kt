package ai.promethist.client.avatar.unity

import ai.promethist.client.common.LoggerV3

enum class AvatarMessage {
    Unknown,
    AudioStarted,
    TurnEnded,
    AudioEnded,
    SceneLoaded,
    AvatarLoaded,
    DownloadStarted,
    DownloadProgress,
    DownloadEnded,
    DownloadFailed,
    AvatarLoadStarted,
    AvatarLoadFailed,
    TechnicalReport;

    val params: MutableMap<String, String> = mutableMapOf()

    companion object {
        fun fromString(value: String): AvatarMessage {
            try {
                val message = AvatarMessage.valueOf(value.substringBefore(";").replaceFirstChar(Char::uppercase))
                if (value.contains(";")) {
                    val paramString = value.substringAfter(";")
                    if (paramString.isNotEmpty()) {
                        paramString.split(";").forEach {
                            val pair = it.split(":")
                            message.params[pair[0]] = pair[1]
                        }
                    }
                }
                LoggerV3.arc("Received message => $message")
                return message
            } catch (e: Exception) {
                e.printStackTrace()
                return Unknown
            }
        }
    }
}