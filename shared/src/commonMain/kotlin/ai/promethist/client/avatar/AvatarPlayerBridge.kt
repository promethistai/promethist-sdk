package ai.promethist.client.avatar

interface AvatarPlayerBridge {
    fun onMessageReceived(message: String)
}