package ai.promethist.client.avatar

import ai.promethist.client.ClientUiState
import ai.promethist.client.ClientUiStateHandler
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock

class CoroutinesFlowUiStateHandler: ClientUiStateHandler {

    private val _uiState = MutableStateFlow(ClientUiState.Initial)
    val uiState: StateFlow<ClientUiState> = _uiState.asStateFlow()

    private val mutex = Mutex()

    override suspend fun setState(state: ClientUiState) {
        mutex.withLock {
            _uiState.update { state }
        }
    }

    override suspend fun getState(): ClientUiState {
        return mutex.withLock { uiState.value }
    }
}