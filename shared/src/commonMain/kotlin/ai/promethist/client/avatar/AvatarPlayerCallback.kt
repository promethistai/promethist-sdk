package ai.promethist.client.avatar

interface AvatarPlayerCallback {
    fun onSceneLoaded()
    fun onAudioPlaybackStarted()
    fun onTechnicalReport(parameters: Map<String, String>)
}