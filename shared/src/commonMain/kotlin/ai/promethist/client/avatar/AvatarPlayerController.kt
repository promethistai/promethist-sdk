package ai.promethist.client.avatar

interface AvatarPlayerController {
    var bridge: AvatarPlayerBridge
    fun sendMessage(nameObject: String, functionName: String, message: String)
    fun onSceneLoaded()
    fun setAudioBlocked(isBlocked: Boolean)
    fun getIsAudioBlocked(): Boolean
}