package ai.promethist.client

object ClientTarget {
    var currentTarget: ClientTargetPreset = ClientTargetPreset.Promethist
}

enum class ClientTargetPreset {
    Promethist, Elysai, DFHA, TMobile, TMobileV3, Gaia;

    val appName: String get() = when (this) {
        Promethist -> "Promethist"
        Elysai -> "Elysai"
        DFHA -> "DFHA"
        TMobile, TMobileV3 -> "T-Mobile"
        Gaia -> "Gaia"
    }
}