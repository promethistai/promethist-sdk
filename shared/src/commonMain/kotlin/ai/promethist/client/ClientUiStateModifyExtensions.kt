package ai.promethist.client

import ai.promethist.client.model.Message
import ai.promethist.client.model.AudioIndicatorState
import ai.promethist.ext.isCommand

fun ClientUiState.setProcessUserInputState(input: String): ClientUiState {
    var messages = this.messages
    if (!input.isCommand) {
        messages = messages + Message(text = input)
    }
    return this.copy(
        visualItem = null,
        modal = null,
        messages = messages,
        transcript = null,
        isPaused = false,
        isBargeInActive = false,
        audioIndicatorState = AudioIndicatorState.Playing,
    )
}

fun ClientUiState.setResetState(): ClientUiState = this.copy(
    error = null,
    modal = null,
    visualItem = null,
    isPaused = false,
    transcript = null,
    audioIndicatorState = AudioIndicatorState.Playing,
)

fun ClientUiState.setBargeInActiveState(): ClientUiState = this.copy(
    audioIndicatorState = AudioIndicatorState.StartSpeaking,
    isBargeInActive = true
)

fun ClientUiState.setStartTranscribeState(): ClientUiState = this.copy(
    audioIndicatorState = AudioIndicatorState.StartSpeaking,
)

fun ClientUiState.setActiveTranscribeState(transcript: String): ClientUiState = this.copy(
    transcript = transcript,
    audioIndicatorState = AudioIndicatorState.Speaking,
)

fun ClientUiState.setPausedState(isPaused: Boolean): ClientUiState = this.copy(
    isPaused = isPaused,
)

fun ClientUiState.setLoading(isLoading: Boolean): ClientUiState = this.copy(
    isLoading = isLoading,
)