package ai.promethist.client

import ai.promethist.Defaults
import ai.promethist.SharedRes
import ai.promethist.channel.item.PersonaItem
import ai.promethist.channel.model.Gender
import ai.promethist.channel.model.Persona
import ai.promethist.client.auth.AuthPolicy
import ai.promethist.client.auth.OAuth2Client
import ai.promethist.client.avatar.AvatarQualityLevel
import ai.promethist.client.model.Appearance
import ai.promethist.client.model.AvatarManifest
import ai.promethist.client.model.InteractionMode
import ai.promethist.client.model.PersonaId
import ai.promethist.client.speech.DuplexMode
import ai.promethist.model.TtsConfig
import ai.promethist.video.VideoMode

object ClientDefaults {

    val locale
        get() = defaultPreset.locale

    val zoneId = Defaults.zoneId

    val avatarId
        get() = personas[0].avatarId ?: Defaults.avatarId

    val personas = when (ClientTarget.currentTarget) {
        ClientTargetPreset.DFHA -> listOf(
            PersonaItem(id = "000000000000000000000000", name = "Jakub", avatarId = "jakub")
        )
        ClientTargetPreset.TMobile, ClientTargetPreset.TMobileV3 -> listOf(
            PersonaItem(id = "000000000000000000000000", name = "Teodorik", avatarId = "teodorik")
        )
        ClientTargetPreset.Gaia -> listOf(
            PersonaItem(id = "000000000000000000000000", name = "TerraAdult", avatarId = "terraadult")
        )
        else -> listOf(
            PersonaItem(id = "000000000000000000000000", name = "Poppy", avatarId = "poppy")
        )
    }

    fun primaryPersona(language: String? = null) = when(language) {
        "cs" -> Persona("000000000000000000000001","Jarmila", "", Gender.Female, null, null, "jarmila", null, null, false, TtsConfig.Voice.Gabriela.config)
        else -> Persona("000000000000000000000000","Poppy", "", Gender.Female, null, null, "poppy", null, null, false, TtsConfig.Voice.Audrey.config)
    }

    val introVideo = SharedRes.files.t_assistant_intro_video

    val presets: List<AppPreset> = when (ClientTarget.currentTarget) {
        ClientTargetPreset.Promethist -> listOf(
            AppPreset.EngineV3,
            AppPreset.Engine,
            AppPreset.EnginePreview
        )
        ClientTargetPreset.DFHA -> listOf(
            AppPreset.DFHA,
            AppPreset.DFHAPreview,
            AppPreset.DFHAEN,
            AppPreset.DFHAENPreview,
            AppPreset.DFHASystemTest,
            AppPreset.DFHASystemTestPreview,
            AppPreset.TestLocal,
            AppPreset.Custom,
            AppPreset.CustomCz,
        )
        ClientTargetPreset.TMobile -> listOf(
            AppPreset.TMobile,
            AppPreset.TMobilePreproduction,
            AppPreset.TMobilePreview,
            AppPreset.TMobileDevelopment,
            AppPreset.TMobileEN,
            AppPreset.TMobilePreproductionEN,
            AppPreset.TMobilePreviewEN,
            AppPreset.TMobileDevelopmentEN,
            AppPreset.TMobileSystemTest,
            AppPreset.TMobileSystemTestPreview,
            AppPreset.TestLocal,
            AppPreset.Custom,
            AppPreset.CustomCz,
        )
        ClientTargetPreset.TMobileV3 -> listOf(
            AppPreset.TMobileV3,
            AppPreset.TMobileV3Preview,
            AppPreset.TMobileV3Dev,
            AppPreset.RoamingV3,
            AppPreset.RoamingV3Preview,
            AppPreset.DefaultV3,
            AppPreset.DefaultV3Preview,
            AppPreset.DefaultV3Dev,
            AppPreset.Custom,
            AppPreset.CustomCz
        )
        ClientTargetPreset.Gaia -> listOf(
            AppPreset.Gaia,
            AppPreset.GaiaPreview,
            AppPreset.GaiaSystemTest,
            AppPreset.GaiaSystemTestPreview,
            AppPreset.TestLocal,
            AppPreset.Custom,
            AppPreset.CustomCz,
        )
        else -> listOf(
            AppPreset.ElysaiPreview,
            AppPreset.Elysai,
            AppPreset.SystemTest,
            AppPreset.SystemTestPreview,
            AppPreset.Custom,
            AppPreset.CustomCz,
        )
    }

    val defaultPreset: AppPreset = when (ClientTarget.currentTarget) {
        ClientTargetPreset.Promethist -> AppPreset.EngineV3
        ClientTargetPreset.Elysai -> AppPreset.Elysai
        ClientTargetPreset.DFHA -> AppPreset.DFHA
        ClientTargetPreset.TMobile -> AppPreset.TMobile
        ClientTargetPreset.TMobileV3 -> AppPreset.RoamingV3Preview
        ClientTargetPreset.Gaia -> AppPreset.Gaia
    }

    val defaultDisplayTerms = when (ClientTarget.currentTarget) {
        ClientTargetPreset.Elysai -> true
        else -> false
    }

    val defaultDisplayConsent = when (ClientTarget.currentTarget) {
        ClientTargetPreset.Elysai -> true
        else -> false
    }

    val interactionMode = InteractionMode.Voice
    val appearance = Appearance.System
    val avatarQualityLevel = AvatarQualityLevel.Medium

    val videoMode = when (ClientTarget.currentTarget) {
        ClientTargetPreset.DFHA -> VideoMode.Streamed
        ClientTargetPreset.Elysai -> VideoMode.Streamed
        ClientTargetPreset.TMobileV3 -> VideoMode.OnDeviceRendering
        ClientTargetPreset.Promethist -> VideoMode.OnDeviceRendering
        else -> VideoMode.Streamed
    }

    val personaViewTitleVisible: Boolean = when (ClientTarget.currentTarget) {
        ClientTargetPreset.Elysai -> true
        else -> false
    }

    val requiresAppKeyInput: Boolean = when (ClientTarget.currentTarget) {
        ClientTargetPreset.Promethist -> true
        else -> false
    }

    var appKeyInputDefaultPreset: AppPreset = when (ClientTarget.currentTarget) {
        ClientTargetPreset.TMobileV3 -> AppPreset.RoamingV3Preview
        else -> AppPreset.DefaultV3
    }

    var authPolicy: AuthPolicy = when (ClientTarget.currentTarget) {
        ClientTargetPreset.TMobileV3 -> AuthPolicy.Required
        ClientTargetPreset.Promethist -> AuthPolicy.Optional
        else -> AuthPolicy.Disabled
    }

    var defaultAvatarPlayerPersona: PersonaId = when (ClientTarget.currentTarget) {
        else -> PersonaId.Richard
    }

    val defaultAvatarManifest = AvatarManifest(avatars = emptyList(), backgrounds = emptyList())

    val duplexMode = DuplexMode.OnDevice

    val showIntroVideo: Boolean = when (ClientTarget.currentTarget) {
        ClientTargetPreset.TMobileV3 -> true
        else -> false
    }

    val resourcesBaseUrl = "https://repository.promethist.ai"
    val otelUrl = "https://otel-collector.promethist.ai"

    val showAppKeyBackgroundImage: Boolean = when (ClientTarget.currentTarget) {
        ClientTargetPreset.Promethist -> true
        else -> false
    }

    val authSettings = OAuth2Client.OAuth2ClientSettings(
        authorization_endpoint = "https://keycloak.promethist.ai/realms/promethist/protocol/openid-connect/auth",
        token_endpoint = "https://keycloak.promethist.ai/realms/promethist/protocol/openid-connect/token",
        userinfo_endpoint = "https://keycloak.promethist.ai/realms/promethist/protocol/openid-connect/userinfo",
        logout_endpoint = "https://keycloak.promethist.ai/realms/promethist/protocol/openid-connect/logout",
        client_id = "mobile",
        client_secret = null,
        redirect_uri = "https://promethist.ai/",
        issuer = "https://keycloak.promethist.ai/realms/promethist",
        scope = "openid",
    )
}