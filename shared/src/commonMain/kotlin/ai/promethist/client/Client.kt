package ai.promethist.client

import ai.promethist.client.auth.OAuth2Client
import ai.promethist.client.auth.AuthStorage
import ai.promethist.client.auth.oauth2.GenericOAuth2Client
import ai.promethist.client.auth.SecureAuthStorage
import ai.promethist.client.common.AppConfig
import ai.promethist.client.channel.ClientChannel
import ai.promethist.client.channel.WebSocketClientChannel
import ai.promethist.common.configuredHttpClient
import com.russhwolf.settings.Settings
import org.kodein.di.DI
import org.kodein.di.bindSingleton
import org.kodein.di.direct
import org.kodein.di.instance
import kotlin.native.concurrent.ThreadLocal

@ThreadLocal
object Client {

    val di = DI {
        bindSingleton {
            configuredHttpClient()
        }
        bindSingleton<ClientChannel> {
            WebSocketClientChannel()
        }
        bindSingleton {
            Settings()
        }
        bindSingleton {
            AppConfig(instance())
        }
        bindSingleton<AuthStorage> {
            SecureAuthStorage()
        }
        bindSingleton<OAuth2Client> {
            GenericOAuth2Client(instance(), instance(), instance())
        }
    }
    fun channel() = di.direct.instance<ClientChannel>()

    fun appConfig() = di.direct.instance<AppConfig>()

    fun authSecureStorage() = di.direct.instance<SecureAuthStorage>()

    fun authClient() = di.direct.instance<OAuth2Client>()
}