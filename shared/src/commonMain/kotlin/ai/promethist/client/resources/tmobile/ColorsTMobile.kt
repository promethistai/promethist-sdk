package ai.promethist.client.resources.tmobile

import ai.promethist.SharedRes
import ai.promethist.client.resources.default.ColorsDefault
import dev.icerock.moko.resources.ColorResource

class ColorsTMobile: ColorsDefault() {
    override val accent: ColorResource
        get() = SharedRes.colors.accentPink
    override val regularMaterial: ColorResource
        get() = SharedRes.colors.regularMaterialGrey
    override val backgroundOverlay: ColorResource
        get() = SharedRes.colors.backgroundOverlayTmobile
    override val accentSecondary: ColorResource
        get() = SharedRes.colors.accentSecondaryPink

    override val growthModuleDone: ColorResource
        get() = SharedRes.colors.growthModuleDonePink
    override val growthModuleNew: ColorResource
        get() = SharedRes.colors.growthModuleNewPink
    override val growthModuleInProgress: ColorResource
        get() = SharedRes.colors.growthModuleInProgressPink
    override val growthModuleLocked: ColorResource
        get() = SharedRes.colors.growthModuleLockedPink
    override val growthSubmoduleDone: ColorResource
        get() = SharedRes.colors.growthSubmoduleDonePink
    override val growthSubmoduleNew: ColorResource
        get() = SharedRes.colors.growthSubmoduleNewPink
    override val growthSubmoduleInProgress: ColorResource
        get() = SharedRes.colors.growthSubmoduleInProgressPink
    override val growthSubmoduleLocked: ColorResource
        get() = SharedRes.colors.growthSubmoduleLockedPink
    override val growthProgressBackground: ColorResource
        get() = SharedRes.colors.growthProgressBackgroundPink
    override val growthProgress: ColorResource
        get() = SharedRes.colors.growthProgressPink
    override val growthProgressLocked: ColorResource
        get() = SharedRes.colors.growthProgressLockedPink
    override val backToMenuButtons: ColorResource
        get() = SharedRes.colors.accentPink
}