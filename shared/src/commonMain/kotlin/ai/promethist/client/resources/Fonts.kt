package ai.promethist.client.resources

import dev.icerock.moko.resources.FontResource

interface Fonts {

    val primaryFont: PrimaryFont

    interface PrimaryFont {
        val thin: FontResource
        val light: FontResource
        val regular: FontResource
        val medium: FontResource
        val bold: FontResource
        val italic: FontResource
    }
}