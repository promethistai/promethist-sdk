package ai.promethist.client.resources

import ai.promethist.client.ClientTarget
import ai.promethist.client.ClientTargetPreset
import ai.promethist.client.resources.dfha.ColorsDFHA
import ai.promethist.client.resources.dfha.FontsDFHA
import ai.promethist.client.resources.dfha.ImagesDFHA
import ai.promethist.client.resources.dfha.StringsDFHA
import ai.promethist.client.resources.default.ColorsDefault
import ai.promethist.client.resources.default.FontsDefault
import ai.promethist.client.resources.default.ImagesDefault
import ai.promethist.client.resources.default.StringsDefault
import ai.promethist.client.resources.elysai.ColorsElysai
import ai.promethist.client.resources.elysai.FontsElysai
import ai.promethist.client.resources.elysai.ImagesElysai
import ai.promethist.client.resources.elysai.StringsElysai
import ai.promethist.client.resources.gaia.ColorsGaia
import ai.promethist.client.resources.gaia.FontsGaia
import ai.promethist.client.resources.gaia.ImagesGaia
import ai.promethist.client.resources.gaia.StringsGaia
import ai.promethist.client.resources.tmobile.ColorsTMobile
import ai.promethist.client.resources.tmobile.FontsTMobile
import ai.promethist.client.resources.tmobile.ImagesTMobile
import ai.promethist.client.resources.tmobile.StringsTMobile

object Resources {

    val images: Images = when (ClientTarget.currentTarget) {
        ClientTargetPreset.Promethist -> ImagesDefault()
        ClientTargetPreset.Elysai -> ImagesElysai()
        ClientTargetPreset.DFHA -> ImagesDFHA()
        ClientTargetPreset.TMobile, ClientTargetPreset.TMobileV3 -> ImagesTMobile()
        ClientTargetPreset.Gaia -> ImagesGaia()
    }

    val colors: Colors = when (ClientTarget.currentTarget) {
        ClientTargetPreset.Promethist -> ColorsDefault()
        ClientTargetPreset.Elysai -> ColorsElysai()
        ClientTargetPreset.DFHA -> ColorsDFHA()
        ClientTargetPreset.TMobile, ClientTargetPreset.TMobileV3 -> ColorsTMobile()
        ClientTargetPreset.Gaia -> ColorsGaia()
    }

    val fonts: Fonts = when (ClientTarget.currentTarget) {
        ClientTargetPreset.Promethist -> FontsDefault()
        ClientTargetPreset.Elysai -> FontsElysai()
        ClientTargetPreset.DFHA -> FontsDFHA()
        ClientTargetPreset.TMobile, ClientTargetPreset.TMobileV3 -> FontsTMobile()
        ClientTargetPreset.Gaia -> FontsGaia()
    }

    val strings: Strings = when (ClientTarget.currentTarget) {
        ClientTargetPreset.Promethist -> StringsDefault()
        ClientTargetPreset.Elysai -> StringsElysai()
        ClientTargetPreset.DFHA -> StringsDFHA()
        ClientTargetPreset.TMobile, ClientTargetPreset.TMobileV3 -> StringsTMobile()
        ClientTargetPreset.Gaia -> StringsGaia()
    }
}