package ai.promethist.client.resources

import ai.promethist.SharedRes
import dev.icerock.moko.resources.desc.Resource
import dev.icerock.moko.resources.desc.ResourceStringDesc
import dev.icerock.moko.resources.desc.StringDesc

interface Strings {

    val onboarding: Onboarding
    val settings: Settings
    val persona: Persona
    val home: Home
    val subscription: Subscription
    val profile: Profile
    val auth: Auth
    val modals: Modals
    val error: Error
    val network: Network

    interface Onboarding {
        val termsAndConditionsTitle: ResourceStringDesc
        val termsAndConditionsText: ResourceStringDesc
        val dataProtectionTitle: ResourceStringDesc
        val dataProtectionText: ResourceStringDesc
        val decline: ResourceStringDesc
        val accept: ResourceStringDesc
        val dissent: ResourceStringDesc
        val consent: ResourceStringDesc
        val insertYourAppKey: ResourceStringDesc
        val invalidAppKey: ResourceStringDesc
        val youEnteredInvalidAppKey: ResourceStringDesc
    }

    interface Settings {
        val title: ResourceStringDesc
        val application: ResourceStringDesc
        val presets: ResourceStringDesc
        val userInterface: ResourceStringDesc
        val defaultInteractionMode: ResourceStringDesc
        val appearance: ResourceStringDesc
        val homeTextInput: ResourceStringDesc
        val requireOnDeviceRecognition: ResourceStringDesc
        val preventDisplaySleep: ResourceStringDesc
        val showExitConversationAlert: ResourceStringDesc
        val showNetworkAlert: ResourceStringDesc
        val onScreenDebug: ResourceStringDesc
        val showAppKeyEntry: ResourceStringDesc
        val videoMode: ResourceStringDesc
        val arcQualityLevel: ResourceStringDesc
        val changePersona: ResourceStringDesc
        val subscription: ResourceStringDesc
        val clearCache: ResourceStringDesc
        val preventDisplayTurningOff: ResourceStringDesc
        val feedback: ResourceStringDesc
        val sos: ResourceStringDesc
        val report: ResourceStringDesc
        val version: ResourceStringDesc
        val buildNumber: ResourceStringDesc
        val support: ResourceStringDesc
        val about: ResourceStringDesc
        val openPermissionSettings: ResourceStringDesc
        val permissionsNeededText: ResourceStringDesc
        val permissionsNeededTitle: ResourceStringDesc
        val redownloadAssets: ResourceStringDesc
        val micPermissionDenied: ResourceStringDesc
        val downloadedNecessaryAssets: ResourceStringDesc
        val streamedVideosPrefetch: ResourceStringDesc
        val v3: ResourceStringDesc
        val asr: ResourceStringDesc
        val initialCommand: ResourceStringDesc
        val done: ResourceStringDesc
        val asrLocale: ResourceStringDesc
        val restartRequired: ResourceStringDesc
        val restart: ResourceStringDesc
        val debugClicksRemaining: ResourceStringDesc
        val debugAlreadyActivated: ResourceStringDesc
        val debugActivated: ResourceStringDesc
        val privacyPolicy: ResourceStringDesc
        val privacyPolicyUrl: ResourceStringDesc
        val dataProtection: ResourceStringDesc
        val dataProtectionUrl: ResourceStringDesc
        val videoModeNone: ResourceStringDesc
        val videoModeStreamed: ResourceStringDesc
        val videoModeOnDevice: ResourceStringDesc
        val engineUrl: ResourceStringDesc
        val applicationKey: ResourceStringDesc
        val duplexEnabled: ResourceStringDesc
        val duplexMode: ResourceStringDesc
        val duplexModeStreamed: ResourceStringDesc
        val duplexModeOnDevice: ResourceStringDesc
        val duplexForceOutput: ResourceStringDesc
        val changeOtelUrl: ResourceStringDesc
    }

    interface Persona {
        val home: ResourceStringDesc
        val typeHere: ResourceStringDesc
        val homeDisclaimer: ResourceStringDesc
        val networkDisclaimer: ResourceStringDesc
        val overloadDisclaimer: ResourceStringDesc
        val backToMenu: ResourceStringDesc
        val resume: ResourceStringDesc
        val dontShowThisMessage: ResourceStringDesc
        val choosePrimaryPersona: ResourceStringDesc
        val startSpeaking: ResourceStringDesc
        val searchInConversation: ResourceStringDesc
        val chatCancel: ResourceStringDesc
        val chatDone: ResourceStringDesc
        val introduction: ResourceStringDesc
        val homeDisclaimerText: ResourceStringDesc
        val homeDisclaimerYes: ResourceStringDesc
        val homeDisclaimerNo: ResourceStringDesc
    }

    interface Home {
        val title: ResourceStringDesc
        val titleAlt: ResourceStringDesc
        val subtitle: ResourceStringDesc
        val startConversation: ResourceStringDesc
        val historyOfConversations: ResourceStringDesc
    }

    interface Subscription {
        val subscription: ResourceStringDesc
        val subscriptionPurchasesDisabled: ResourceStringDesc
        val subscriptionPurchaseFailed: ResourceStringDesc
        val subscriptionPurchaseSuccess: ResourceStringDesc
    }

    interface Profile {
        val resetApplication: ResourceStringDesc
        val deviceId: ResourceStringDesc
        val longTapToCopy: ResourceStringDesc
        val copyToClipboard: ResourceStringDesc
        val user: ResourceStringDesc
        val signIn: ResourceStringDesc
        val signOut: ResourceStringDesc
        val title: ResourceStringDesc
        val edit: ResourceStringDesc
    }

    interface Auth {
        val title: ResourceStringDesc
        val logout: ResourceStringDesc
        val login: ResourceStringDesc
        val notLoggedIn: ResourceStringDesc
        val welcome: ResourceStringDesc
        val required: ResourceStringDesc
        val requiredLogin: ResourceStringDesc
    }

    interface Modals {
        val orderSummaryTitle: ResourceStringDesc
        val orderTotal: ResourceStringDesc
        val orderCancel: ResourceStringDesc
        val orderSubmit: ResourceStringDesc
        val callText: ResourceStringDesc
        val callCancel: ResourceStringDesc
        val callSubmit: ResourceStringDesc
        val profileTitle: ResourceStringDesc
        val profileName: ResourceStringDesc
        val profileNotFilled: ResourceStringDesc
        val profileGender: ResourceStringDesc
        val profileGenderMale: ResourceStringDesc
        val profileGenderFemale: ResourceStringDesc
        val profileSubmit: ResourceStringDesc
        val profileInvalidNickname: ResourceStringDesc
    }

    interface Error {
        val errorTitle: ResourceStringDesc
        val errorOutageMessage: ResourceStringDesc
        val errorRetry: ResourceStringDesc
        val errorMicNotAllowed: ResourceStringDesc
        val errorInvalidDomain: ResourceStringDesc
        val errorSocketConnection: ResourceStringDesc
    }

    interface Network {
        val networkWarning: ResourceStringDesc
        val networkError: ResourceStringDesc
    }
}