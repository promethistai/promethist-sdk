package ai.promethist.client.resources.default

import ai.promethist.SharedRes
import ai.promethist.client.resources.Strings
import dev.icerock.moko.resources.desc.Resource
import dev.icerock.moko.resources.desc.ResourceStringDesc
import dev.icerock.moko.resources.desc.StringDesc

open class StringsDefault: Strings {

    override val onboarding: Strings.Onboarding
        get() = OnboardingDefault
    override val settings: Strings.Settings
        get() = SettingsDefault
    override val persona: Strings.Persona
        get() = PersonaDefault()
    override val home: Strings.Home
        get() = HomeDefault
    override val subscription: Strings.Subscription
        get() = SubscriptionDefault
    override val profile: Strings.Profile
        get() = ProfileDefault
    override val auth: Strings.Auth
        get() = AuthDefault
    override val modals: Strings.Modals
        get() = ModalsDefault
    override val error: Strings.Error
        get() = ErrorDefault
    override val network: Strings.Network
        get() = NetworkDefault

    object OnboardingDefault: Strings.Onboarding {
        override val termsAndConditionsTitle: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.onboarding_terms_and_conditions_title)
        override val termsAndConditionsText: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.onboarding_terms_and_conditions_text)
        override val dataProtectionTitle: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.onboarding_data_protection_title)
        override val dataProtectionText: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.onboarding_data_protection_text)
        override val decline: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.onboarding_decline)
        override val accept: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.onboarding_accept)
        override val dissent: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.onboarding_dissent)
        override val consent: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.onboarding_consent)
        override val insertYourAppKey: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.insert_your_app_key)
        override val invalidAppKey: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.invalid_app_key)
        override val youEnteredInvalidAppKey: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.you_entered_invalid_app_key)
    }

    object SettingsDefault: Strings.Settings {
        override val title: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.settings_title)
        override val application: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.settings_application)
        override val presets: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.settings_preset)
        override val userInterface: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.settings_user_interface)
        override val defaultInteractionMode: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.settings_default_interaction_mode)
        override val appearance: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.settings_appearance)
        override val homeTextInput: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.settings_home_text_input)
        override val requireOnDeviceRecognition: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.settings_require_on_device_recognition)
        override val preventDisplaySleep: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.settings_prevent_display_sleep)
        override val showExitConversationAlert: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.settings_show_exit_conversation_alert)
        override val showNetworkAlert: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.settings_show_network_alert)
        override val onScreenDebug: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.settings_on_screen_debug)
        override val showAppKeyEntry: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.settings_show_app_key_entry)
        override val videoMode: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.settings_video_mode)
        override val arcQualityLevel: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.settings_arc_quality_level)
        override val changePersona: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.settings_change_persona)
        override val subscription: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.subscription)
        override val clearCache: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.settings_clear_cache)
        override val preventDisplayTurningOff: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.settings_prevent_display_sleep)
        override val feedback: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.settings_feedback)
        override val sos: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.settings_sos)
        override val report: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.settings_report)
        override val version: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.settings_version)
        override val buildNumber: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.settings_build_number)
        override val support: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.settings_support)
        override val about: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.settings_about)
        override val openPermissionSettings: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.settings_open_permissions)
        override val permissionsNeededTitle: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.settings_permissions_needed_title)
        override val permissionsNeededText: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.settings_permissions_needed_text)
        override val redownloadAssets: ResourceStringDesc
            get() = StringDesc.Resource((SharedRes.strings.settings_redownload_assets))
        override val micPermissionDenied: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.mic_permission_denied)
        override val downloadedNecessaryAssets: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.downloaded_necessary_assets)
        override val streamedVideosPrefetch: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.settings_streamed_videos_prefetch)
        override val v3: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.settings_v3)
        override val asr: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.settings_asr)
        override val initialCommand: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.settings_initial_command)
        override val done: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.settings_done)
        override val asrLocale: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.settings_asr_locale)
        override val restartRequired: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.settings_restart_required)
        override val restart: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.settings_restart)
        override val debugClicksRemaining: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.settings_debug_clicks_remaining)
        override val debugAlreadyActivated: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.settings_debug_already_activated)
        override val debugActivated: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.settings_debug_activated)
        override val privacyPolicy: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.onboarding_terms_and_conditions_name)
        override val privacyPolicyUrl: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.onboarding_terms_and_conditions_url)
        override val dataProtection: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.onboarding_data_protection_name)
        override val dataProtectionUrl: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.onboarding_data_protection_url)
        override val videoModeNone: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.settings_video_mode_none)
        override val videoModeStreamed: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.settings_video_mode_streamed)
        override val videoModeOnDevice: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.settings_video_mode_on_device)
        override val engineUrl: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.settings_engine_url)
        override val applicationKey: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.settings_application_key)
        override val duplexEnabled: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.settings_duplex_enabled)
        override val duplexMode: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.settings_duplex_mode)
        override val duplexModeStreamed: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.settings_duplex_mode_streamed)
        override val duplexModeOnDevice: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.settings_duplex_mode_on_device)
        override val duplexForceOutput: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.settings_duplex_force_output)
        override val changeOtelUrl: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.settings_change_otel_url)
    }

    open class PersonaDefault: Strings.Persona {
        override val home: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.persona_home)
        override val typeHere: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.type_here)
        override val homeDisclaimer: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.persona_home_disclaimer)
        override val networkDisclaimer: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.persona_network_disclaimer)
        override val overloadDisclaimer: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.persona_overload_disclaimer)
        override val backToMenu: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.persona_back_to_menu)
        override val resume: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.persona_resume)
        override val dontShowThisMessage: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.persona_dont_show_this_message)
        override val choosePrimaryPersona: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.choose_primary_persona)
        override val startSpeaking: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.persona_start_speaking)
        override val searchInConversation: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.persona_search_in_conversation)
        override val chatCancel: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.persona_chat_cancel)
        override val chatDone: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.persona_chat_done)
        override val introduction: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.persona_introduction)
        override val homeDisclaimerText: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.persona_home_disclaimer_text)
        override val homeDisclaimerYes: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.persona_home_disclaimer_yes)
        override val homeDisclaimerNo: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.persona_home_disclaimer_no)
    }

    object HomeDefault: Strings.Home {
        override val title: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.home_title)
        override val titleAlt: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.home_title_alt)
        override val subtitle: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.home_subtitle)
        override val startConversation: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.home_start_conversation)
        override val historyOfConversations: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.home_history_of_conversations)
    }

    object SubscriptionDefault: Strings.Subscription {
        override val subscription: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.subscription)
        override val subscriptionPurchasesDisabled: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.subscription_purchases_disabled)
        override val subscriptionPurchaseFailed: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.subscription_purchase_failed)
        override val subscriptionPurchaseSuccess: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.subscription_purchase_success)
    }

    object ProfileDefault: Strings.Profile {
        override val resetApplication: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.profile_reset_application)
        override val deviceId: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.profile_device_id)
        override val longTapToCopy: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.profile_long_tap_to_copy)
        override val copyToClipboard: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.profile_copy_to_clipboard)
        override val user: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.profile_user)
        override val signIn: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.profile_sign_in)
        override val signOut: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.profile_sign_out)
        override val title: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.profile_title)
        override val edit: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.profile_edit)
    }

    object AuthDefault: Strings.Auth {
        override val title: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.auth_title)
        override val logout: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.auth_logout)
        override val login: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.auth_login)
        override val notLoggedIn: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.auth_not_logged_in)
        override val welcome: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.auth_welcome)
        override val required: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.auth_required)
        override val requiredLogin: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.auth_required_login)
    }

    object ModalsDefault: Strings.Modals {
        override val orderSummaryTitle: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.modal_order_summary_title)
        override val orderTotal: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.modal_order_total)
        override val orderCancel: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.modal_order_cancel)
        override val orderSubmit: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.modal_order_submit)
        override val callText: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.modal_call_text)
        override val callCancel: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.modal_call_cancel)
        override val callSubmit: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.modal_call_submit)
        override val profileTitle: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.modal_profile_title)
        override val profileName: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.modal_profile_name)
        override val profileNotFilled: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.modal_profile_not_filled)
        override val profileGender: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.modal_profile_gender)
        override val profileGenderMale: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.modal_profile_gender_male)
        override val profileGenderFemale: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.modal_profile_gender_female)
        override val profileSubmit: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.modal_profile_submit)
        override val profileInvalidNickname: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.modal_profile_invalid_nickname)
    }

    object NetworkDefault: Strings.Network {
        override val networkWarning: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.network_warning)
        override val networkError: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.network_error)
    }

    object ErrorDefault: Strings.Error {
        override val errorTitle: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.error_title)
        override val errorOutageMessage: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.error_outage_message)
        override val errorRetry: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.error_retry)
        override val errorMicNotAllowed: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.error_mic_not_allowed)
        override val errorInvalidDomain: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.error_invalid_domain)
        override val errorSocketConnection: ResourceStringDesc
            get() = StringDesc.Resource(SharedRes.strings.error_socket_connection)
    }
}