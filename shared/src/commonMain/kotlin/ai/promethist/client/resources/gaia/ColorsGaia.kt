package ai.promethist.client.resources.gaia

import ai.promethist.SharedRes
import ai.promethist.client.resources.default.ColorsDefault
import dev.icerock.moko.resources.ColorResource

class ColorsGaia: ColorsDefault() {
    override val accent: ColorResource
        get() = SharedRes.colors.accentGaia
    override val regularMaterial: ColorResource
        get() = SharedRes.colors.regularMaterialGrey
    override val backgroundOverlay: ColorResource
        get() = SharedRes.colors.backgroundOverlayGaia
    override val accentSecondary: ColorResource
        get() = SharedRes.colors.accentSecondaryGaia
    override val growthModuleDone: ColorResource
        get() = SharedRes.colors.growthModuleDoneGaia
    override val growthModuleNew: ColorResource
        get() = SharedRes.colors.growthModuleNewGaia
    override val growthModuleInProgress: ColorResource
        get() = SharedRes.colors.growthModuleInProgressGaia
    override val growthModuleLocked: ColorResource
        get() = SharedRes.colors.growthModuleLockedGaia
    override val growthSubmoduleDone: ColorResource
        get() = SharedRes.colors.growthSubmoduleDoneGaia
    override val growthSubmoduleNew: ColorResource
        get() = SharedRes.colors.growthSubmoduleNewGaia
    override val growthSubmoduleInProgress: ColorResource
        get() = SharedRes.colors.growthSubmoduleInProgressGaia
    override val growthSubmoduleLocked: ColorResource
        get() = SharedRes.colors.growthSubmoduleLockedGaia
    override val growthProgressBackground: ColorResource
        get() = SharedRes.colors.growthProgressBackgroundGaia
    override val growthProgress: ColorResource
        get() = SharedRes.colors.growthProgressGaia
    override val backToMenuButtons: ColorResource
        get() = SharedRes.colors.accentGaia
}