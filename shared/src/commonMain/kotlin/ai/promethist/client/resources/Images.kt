package ai.promethist.client.resources

import dev.icerock.moko.resources.ImageResource

interface Images {
    val icArrowRight: ImageResource
    val icArrowUturnBackward: ImageResource
    val icCheckmark: ImageResource
    val icClose: ImageResource
    val icForwardEnd: ImageResource
    val icHouse: ImageResource
    val icLockFill: ImageResource
    val icQuestionMarkCircle: ImageResource
    val icWandAndStars: ImageResource
    val icCopy: ImageResource
    val icPlay: ImageResource
    val iconOptions: ImageResource
    val iconProfile: ImageResource
    val iconTutorial: ImageResource
    val splashLogo: ImageResource
    val splashTitle: ImageResource
    val appKeyEntryLogo: ImageResource
    val icDebug: ImageResource
    val icMicDenied: ImageResource
    val icDownload: ImageResource
    val icQrCode: ImageResource
    val icKey: ImageResource
    val icNetwork: ImageResource

    val anthonyPortraitL: ImageResource
    val anthonyPortraitM: ImageResource
    val poppyPortraitL: ImageResource
    val poppyPortraitM: ImageResource
    val quinnPortraitL: ImageResource
    val quinnPortraitM: ImageResource
    val sebPortraitL: ImageResource
    val sebPortraitM: ImageResource

    val anthonyBackground: ImageResource
    val poppyBackground: ImageResource
    val quinnBackground: ImageResource
    val sebBackground: ImageResource
    val leoBackground: ImageResource

    val personCropCircle: ImageResource
    val backArrow: ImageResource

    val appKeyEntryBackground: ImageResource

    val personaHand: ImageResource
    val personaHome: ImageResource
    val personaChat: ImageResource
    val personaMicrophone: ImageResource
    val personaPerson: ImageResource
    val personaAudioIndicatorActive: ImageResource
    val personaAudioIndicatorInactive: ImageResource
    val personaSettings: ImageResource
    val personaSearch: ImageResource
    val personaPause: ImageResource
    val personaPlay: ImageResource
    val personaSkip: ImageResource
    val personaArrowBack: ImageResource
}