package ai.promethist.client.resources.tmobile

import ai.promethist.SharedRes
import ai.promethist.client.resources.default.ImagesDefault
import dev.icerock.moko.resources.ImageResource

class ImagesTMobile: ImagesDefault() {
    override val splashLogo: ImageResource
        get() = SharedRes.images.splash_logo_tmobile
    override val splashTitle: ImageResource
        get() = SharedRes.images.splash_title_tmobile
    override val appKeyEntryLogo: ImageResource
        get() = SharedRes.images.splash_logo_tmobile
    override val icHouse: ImageResource
        get() = SharedRes.images.ic_house
    override val iconOptions: ImageResource
        get() = SharedRes.images.icon_options_modern
    override val iconProfile: ImageResource
        get() = SharedRes.images.icon_profile_modern
}