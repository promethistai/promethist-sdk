package ai.promethist.client.resources.dfha

import ai.promethist.SharedRes
import ai.promethist.client.resources.default.ImagesDefault
import dev.icerock.moko.resources.ImageResource

class ImagesDFHA: ImagesDefault() {
    override val splashLogo: ImageResource
        get() = SharedRes.images.splash_logo_cs
    override val splashTitle: ImageResource
        get() = SharedRes.images.splash_title_cs
    override val appKeyEntryLogo: ImageResource
        get() = SharedRes.images.splash_logo_cs
    override val icHouse: ImageResource
        get() = SharedRes.images.ic_house
    override val iconOptions: ImageResource
        get() = SharedRes.images.icon_options_modern
    override val iconProfile: ImageResource
        get() = SharedRes.images.icon_profile_modern
}