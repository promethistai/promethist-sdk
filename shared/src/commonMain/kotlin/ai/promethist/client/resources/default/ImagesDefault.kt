package ai.promethist.client.resources.default

import ai.promethist.SharedRes
import ai.promethist.client.resources.Images
import dev.icerock.moko.resources.ImageResource

open class ImagesDefault: Images {
    override val icArrowRight: ImageResource
        get() = SharedRes.images.ic_arrow_right
    override val icArrowUturnBackward: ImageResource
        get() = SharedRes.images.ic_arrow_uturn_backward
    override val icCheckmark: ImageResource
        get() = SharedRes.images.ic_checkmark
    override val icClose: ImageResource
        get() = SharedRes.images.ic_close
    override val icForwardEnd: ImageResource
        get() = SharedRes.images.ic_forward_end
    override val icHouse: ImageResource
        get() = SharedRes.images.ic_house_variant
    override val icLockFill: ImageResource
        get() = SharedRes.images.ic_lock_fill
    override val icQuestionMarkCircle: ImageResource
        get() = SharedRes.images.ic_questionmark_circle
    override val icWandAndStars: ImageResource
        get() = SharedRes.images.ic_wand_and_stars
    override val icCopy: ImageResource
        get() = SharedRes.images.ic_copy
    override val icPlay: ImageResource
        get() = SharedRes.images.ic_play
    override val iconOptions: ImageResource
        get() = SharedRes.images.icon_options
    override val iconProfile: ImageResource
        get() = SharedRes.images.icon_profile
    override val iconTutorial: ImageResource
        get() = SharedRes.images.ic_questionmark_circle
    override val splashLogo: ImageResource
        get() = SharedRes.images.splash_logo_promethist
    override val splashTitle: ImageResource
        get() = SharedRes.images.splash_title_promethist
    override val appKeyEntryLogo: ImageResource
        get() = SharedRes.images.app_key_entry_logo_promethist
    override val icDebug: ImageResource
        get() = SharedRes.images.ic_debug
    override val icMicDenied: ImageResource
        get() = SharedRes.images.ic_mic_denied
    override val icDownload: ImageResource
        get() = SharedRes.images.ic_download
    override val icQrCode: ImageResource
        get() = SharedRes.images.ic_qr_code
    override val icKey: ImageResource
        get() = SharedRes.images.ic_key
    override val icNetwork: ImageResource
        get() = SharedRes.images.ic_network
    override val anthonyPortraitL: ImageResource
        get() = SharedRes.images.anthony_portrait_l
    override val anthonyPortraitM: ImageResource
        get() = SharedRes.images.anthony_portrait_m
    override val poppyPortraitL: ImageResource
        get() = SharedRes.images.poppy_portrait_l
    override val poppyPortraitM: ImageResource
        get() = SharedRes.images.poppy_portrait_m
    override val quinnPortraitL: ImageResource
        get() = SharedRes.images.quinn_portrait_l
    override val quinnPortraitM: ImageResource
        get() = SharedRes.images.quinn_portrait_m
    override val sebPortraitL: ImageResource
        get() = SharedRes.images.seb_portrait_l
    override val sebPortraitM: ImageResource
        get() = SharedRes.images.seb_portrait_m
    override val anthonyBackground: ImageResource
        get() = SharedRes.images.anthony_background
    override val poppyBackground: ImageResource
        get() = SharedRes.images.poppy_background
    override val quinnBackground: ImageResource
        get() = SharedRes.images.quinn_background
    override val sebBackground: ImageResource
        get() = SharedRes.images.seb_background
    override val leoBackground: ImageResource
        get() = SharedRes.images.leo_background
    override val personCropCircle: ImageResource
        get() = SharedRes.images.person_crop_circle
    override val backArrow: ImageResource
        get() = SharedRes.images.back_arrow
    override val appKeyEntryBackground: ImageResource
        get() = SharedRes.images.promethist_persona_background
    override val personaHand: ImageResource
        get() = SharedRes.images.persona_hand
    override val personaHome: ImageResource
        get() = SharedRes.images.persona_home
    override val personaChat: ImageResource
        get() = SharedRes.images.persona_chat
    override val personaMicrophone: ImageResource
        get() = SharedRes.images.persona_microphone
    override val personaPerson: ImageResource
        get() = SharedRes.images.persona_person
    override val personaAudioIndicatorActive: ImageResource
        get() = SharedRes.images.persona_audio_indicator_active
    override val personaAudioIndicatorInactive: ImageResource
        get() = SharedRes.images.persona_audio_indicator_inactive
    override val personaSettings: ImageResource
        get() = SharedRes.images.persona_settings
    override val personaSearch: ImageResource
        get() = SharedRes.images.persona_search
    override val personaPause: ImageResource
        get() = SharedRes.images.persona_pause
    override val personaPlay: ImageResource
        get() = SharedRes.images.persona_play
    override val personaSkip: ImageResource
        get() = SharedRes.images.persona_skip
    override val personaArrowBack: ImageResource
        get() = SharedRes.images.persona_arrow_back
}