package ai.promethist.client.resources.default

import ai.promethist.SharedRes
import ai.promethist.client.resources.Colors
import dev.icerock.moko.resources.ColorResource

open class ColorsDefault: Colors {
    override val brandPrimary: ColorResource
        get() = SharedRes.colors.brandPrimary
    override val inactiveBackgroundColor: ColorResource
        get() = SharedRes.colors.inactiveBackgroundColor
    override val accent: ColorResource
        get() = SharedRes.colors.accentBlue
    override val accentSecondary: ColorResource
        get() = SharedRes.colors.accentSecondaryGrey
    override val primaryLabel: ColorResource
        get() = SharedRes.colors.primaryLabel
    override val regularMaterial: ColorResource
        get() = SharedRes.colors.regularMaterialGrey
    override val backgroundOverlay: ColorResource
        get() = SharedRes.colors.backgroundOverlay
    override val growthTint: ColorResource
        get() = SharedRes.colors.growthTint
    override val growthTintLight: ColorResource
        get() = SharedRes.colors.growthTintLight
    override val growthSurface: ColorResource
        get() = SharedRes.colors.growthSurface
    
    override val growthModuleDone: ColorResource
        get() = SharedRes.colors.growthModuleDone
    override val growthModuleNew: ColorResource
        get() = SharedRes.colors.growthModuleNew
    override val growthModuleInProgress: ColorResource
        get() = SharedRes.colors.growthModuleInProgress
    override val growthModuleLocked: ColorResource
        get() = SharedRes.colors.growthModuleLocked
    override val growthSubmoduleDone: ColorResource
        get() = SharedRes.colors.growthSubmoduleDone
    override val growthSubmoduleNew: ColorResource
        get() = SharedRes.colors.growthSubmoduleNew
    override val growthSubmoduleInProgress: ColorResource
        get() = SharedRes.colors.growthSubmoduleInProgress
    override val growthSubmoduleLocked: ColorResource
        get() = SharedRes.colors.growthSubmoduleLocked
    override val growthProgressBackground: ColorResource
        get() = SharedRes.colors.growthProgressBackground
    override val growthProgress: ColorResource
        get() = SharedRes.colors.growthProgress
    override val growthProgressLocked: ColorResource
        get() = SharedRes.colors.growthProgressLocked
    override val backToMenuButtons: ColorResource
        get() = SharedRes.colors.accentBlue

    override val personaGlassyBackground: ColorResource
        get() = SharedRes.colors.personaGlassyBackground
    override val personaGlassyForeground: ColorResource
        get() = SharedRes.colors.personaGlassyForeground
    override val personaGlassyActiveBackground: ColorResource
        get() = SharedRes.colors.personaGlassyActiveBackground
    override val personaGreenGlassyBackground: ColorResource
        get() = SharedRes.colors.personaGreenGlassyBackground
    override val personaSurface: ColorResource
        get() = SharedRes.colors.personaSurface
    override val personaSurfaceSecondary: ColorResource
        get() = SharedRes.colors.personaSurfaceSecondary
    override val personaBackground: ColorResource
        get() = SharedRes.colors.personaBackground
    override val personaChatBubbleBackground: ColorResource
        get() = SharedRes.colors.personaChatBubbleBackground
    override val personaChatBubbleForeground: ColorResource
        get() = SharedRes.colors.personaChatBubbleForeground
    override val personaGrey: ColorResource
        get() = SharedRes.colors.personaGrey
    override val personaTextPrimary: ColorResource
        get() = SharedRes.colors.personaTextPrimary
    override val personaTextSecondary: ColorResource
        get() = SharedRes.colors.personaTextSecondary
    override val personaBorderGrey: ColorResource
        get() = SharedRes.colors.personaBorderGrey
    override val personaGrey2: ColorResource
        get() = SharedRes.colors.personaGrey2
}