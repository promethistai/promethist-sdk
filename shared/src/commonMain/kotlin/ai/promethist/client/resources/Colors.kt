package ai.promethist.client.resources

import dev.icerock.moko.resources.ColorResource

interface Colors {
    val brandPrimary: ColorResource
    val inactiveBackgroundColor: ColorResource
    val accent: ColorResource
    val accentSecondary: ColorResource
    val primaryLabel: ColorResource
    val regularMaterial: ColorResource
    val backgroundOverlay: ColorResource
    val growthTint: ColorResource
    val growthTintLight: ColorResource
    val growthSurface: ColorResource

    val growthModuleDone: ColorResource
    val growthModuleNew: ColorResource
    val growthModuleInProgress: ColorResource
    val growthModuleLocked: ColorResource
    val growthSubmoduleDone: ColorResource
    val growthSubmoduleNew: ColorResource
    val growthSubmoduleInProgress: ColorResource
    val growthSubmoduleLocked: ColorResource
    val growthProgressBackground: ColorResource
    val growthProgress: ColorResource
    val growthProgressLocked: ColorResource
    val backToMenuButtons: ColorResource

    val personaGlassyBackground: ColorResource
    val personaGlassyForeground: ColorResource
    val personaGlassyActiveBackground: ColorResource
    val personaGreenGlassyBackground: ColorResource
    val personaSurface: ColorResource
    val personaSurfaceSecondary: ColorResource
    val personaBackground: ColorResource
    val personaChatBubbleBackground: ColorResource
    val personaChatBubbleForeground: ColorResource
    val personaGrey: ColorResource
    val personaTextPrimary: ColorResource
    val personaTextSecondary: ColorResource
    val personaBorderGrey: ColorResource
    val personaGrey2: ColorResource
}