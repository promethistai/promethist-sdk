package ai.promethist.client.resources.elysai

import ai.promethist.SharedRes
import ai.promethist.client.resources.default.ColorsDefault
import dev.icerock.moko.resources.ColorResource

class ColorsElysai: ColorsDefault() {

    override val accent: ColorResource
        get() = SharedRes.colors.accentOrange

    override val regularMaterial: ColorResource
        get() = SharedRes.colors.regularMaterialGrey
}