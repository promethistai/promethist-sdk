package ai.promethist.client.resources.default

import ai.promethist.SharedRes
import ai.promethist.client.resources.Fonts
import dev.icerock.moko.resources.FontResource

open class FontsDefault: Fonts {

    override val primaryFont: Fonts.PrimaryFont
        get() = PrimaryFontDefault

    object PrimaryFontDefault: Fonts.PrimaryFont {
        override val thin: FontResource
            get() = SharedRes.fonts.AktivGrotesk.thin
        override val light: FontResource
            get() = SharedRes.fonts.AktivGrotesk.light
        override val regular: FontResource
            get() = SharedRes.fonts.AktivGrotesk.regular
        override val medium: FontResource
            get() = SharedRes.fonts.AktivGrotesk.medium
        override val bold: FontResource
            get() = SharedRes.fonts.AktivGrotesk.bold
        override val italic: FontResource
            get() = SharedRes.fonts.AktivGrotesk.italic
    }
}