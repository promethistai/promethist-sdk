package ai.promethist.client.resources.dfha

import ai.promethist.SharedRes
import ai.promethist.client.resources.default.ColorsDefault
import dev.icerock.moko.resources.ColorResource

class ColorsDFHA: ColorsDefault() {
    override val accent: ColorResource
        get() = SharedRes.colors.accentBlue
    override val regularMaterial: ColorResource
        get() = SharedRes.colors.regularMaterialGrey
    override val backgroundOverlay: ColorResource
        get() = SharedRes.colors.backgroundOverlayCs
    override val accentSecondary: ColorResource
        get() = SharedRes.colors.accentSecondaryOrange
    override val backToMenuButtons: ColorResource
        get() = SharedRes.colors.growthTint
}