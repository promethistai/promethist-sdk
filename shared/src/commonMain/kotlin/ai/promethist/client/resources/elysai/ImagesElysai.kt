package ai.promethist.client.resources.elysai

import ai.promethist.SharedRes
import ai.promethist.client.resources.default.ImagesDefault
import dev.icerock.moko.resources.ImageResource

class ImagesElysai: ImagesDefault() {
    override val splashLogo: ImageResource
        get() = SharedRes.images.splash_logo
    override val splashTitle: ImageResource
        get() = SharedRes.images.splash_title
    override val appKeyEntryLogo: ImageResource
        get() = SharedRes.images.splash_logo
    override val icHouse: ImageResource
        get() = SharedRes.images.ic_house
}