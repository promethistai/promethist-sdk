package ai.promethist.client.resources.gaia

import ai.promethist.SharedRes
import ai.promethist.client.resources.default.ImagesDefault
import dev.icerock.moko.resources.ImageResource

class ImagesGaia: ImagesDefault() {
    override val splashLogo: ImageResource
        get() = SharedRes.images.splash_logo_gaia
    override val appKeyEntryLogo: ImageResource
        get() = SharedRes.images.splash_logo_gaia
    override val icHouse: ImageResource
        get() = SharedRes.images.ic_house
    override val iconOptions: ImageResource
        get() = SharedRes.images.icon_options_modern
    override val iconProfile: ImageResource
        get() = SharedRes.images.icon_profile_modern
}