package ai.promethist.client.processor

import ai.promethist.channel.item.BinaryAudioItem
import ai.promethist.channel.item.OutputItem
import ai.promethist.client.ClientDefaults
import ai.promethist.client.avatar.AvatarPlayer
import ai.promethist.client.ClientLifecycle
import kotlinx.coroutines.delay
import kotlin.time.Duration.Companion.milliseconds

class BinaryAudioItemProcessor(
    private val avatarPlayer: AvatarPlayer,
    private val lifecycleObserver: ClientLifecycle,
    private val isPaused: () -> Boolean
): OutputItemProcessor {

    override suspend fun process(item: OutputItem) {
        val typedItem = item as? BinaryAudioItem ?: return

        lifecycleObserver.onAudioChunkProcessed()

        if (!avatarPlayer.isPersonaSet) {
            avatarPlayer.setPersona(
                personaId = ClientDefaults.defaultAvatarPlayerPersona,
                withFirstContact = false
            )
            delay(500.milliseconds)
        }
        while (isPaused() || avatarPlayer.isLoadingAvatar) {
            delay(100.milliseconds)
        }

        avatarPlayer.onAudioBytesReceived(typedItem.bytes)
    }
}