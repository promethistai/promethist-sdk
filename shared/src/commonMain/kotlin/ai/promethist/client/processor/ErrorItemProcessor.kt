package ai.promethist.client.processor

import ai.promethist.channel.item.ErrorItem
import ai.promethist.channel.item.OutputItem
import ai.promethist.client.ClientUiStateHandler
import ai.promethist.client.updateUiState

class ErrorItemProcessor(
    private val uiStateHandler: ClientUiStateHandler
): OutputItemProcessor {

    override suspend fun process(item: OutputItem) {
        val typedItem = item as? ErrorItem ?: return

        uiStateHandler.updateUiState { this.copy(
            error = typedItem,
        ) }
    }
}