package ai.promethist.client.processor

import ai.promethist.channel.item.OutputItem
import ai.promethist.channel.item.SetPersonaItem
import ai.promethist.channel.model.PersonaType
import ai.promethist.client.ClientUiStateHandler
import ai.promethist.client.updateUiState

class SetPersonaItemProcessor(
    private val uiStateHandler: ClientUiStateHandler,
    private val onSetPersonaEvent: (ref: String, avatarRef: String, sendAsInput: Boolean) -> Unit
): OutputItemProcessor {

    override suspend fun process(item: OutputItem) {
        val typedItem = item as? SetPersonaItem ?: return

        if (typedItem.isSinglePersona && typedItem.content.type == PersonaType.Primary) {
            typedItem.content.personas.firstOrNull()?.let {
                onSetPersonaEvent(it.getAvatar(), it.id, false)
                return
            }
        }

        uiStateHandler.updateUiState { this.copy(
            modal = typedItem,
            visualItem = null,
            transcript = null
        ) }
    }
}