package ai.promethist.client.processor

import ai.promethist.channel.item.OutputItem
import ai.promethist.channel.item.SessionEndItem
import ai.promethist.client.avatar.AvatarPlayer
import ai.promethist.client.ClientUiStateHandler
import ai.promethist.client.updateUiState
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlin.time.Duration.Companion.milliseconds

class SessionEndItemProcessor(
    private val uiStateHandler: ClientUiStateHandler,
    private val avatarPlayer: AvatarPlayer,
    private val scope: ProcessorScope
): OutputItemProcessor {

    override suspend fun process(item: OutputItem) {
        val typedItem = item as? SessionEndItem ?: return

        scope.launch {
            while (avatarPlayer.isPlaying) {
                delay(10.milliseconds)
            }

            uiStateHandler.updateUiState { this.copy(
                modal = typedItem,
                visualItem = null,
                transcript = null
            ) }
        }
    }
}