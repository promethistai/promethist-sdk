package ai.promethist.client.processor

import ai.promethist.channel.item.OutputItem
import ai.promethist.channel.item.PersonaItem
import ai.promethist.client.model.PersonaId
import ai.promethist.client.avatar.AvatarPlayer

class PersonaItemProcessor(
    private val avatarPlayer: AvatarPlayer
): OutputItemProcessor {

    override suspend fun process(item: OutputItem) {
        val typedItem = item as? PersonaItem ?: return

        val personaId = PersonaId.fromAvatarId(typedItem.getAvatar())
        avatarPlayer.setPersona(personaId, false)
    }
}