package ai.promethist.client.processor

import ai.promethist.channel.item.AudioEndItem
import ai.promethist.channel.item.OutputItem
import ai.promethist.client.avatar.AvatarPlayer

class AudioEndItemProcessor(
    private val avatarPlayer: AvatarPlayer
): OutputItemProcessor {

    override suspend fun process(item: OutputItem) {
        if (item !is AudioEndItem) return
        avatarPlayer.audioEnd()
    }
}