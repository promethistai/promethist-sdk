package ai.promethist.client.processor

import ai.promethist.channel.item.OutputItem

interface OutputItemProcessor {
    suspend fun process(item: OutputItem)
}