package ai.promethist.client.processor

import ai.promethist.channel.item.OutputItem
import ai.promethist.channel.item.StreamedTextItem
import ai.promethist.channel.item.VisualItem
import ai.promethist.client.model.Message
import ai.promethist.client.model.PersonaId
import ai.promethist.client.ClientLifecycle
import ai.promethist.client.ClientUiStateHandler
import ai.promethist.client.updateUiState

class StreamedTextItemProcessor(
    private val uiStateHandler: ClientUiStateHandler,
    private val lifecycleObserver: ClientLifecycle
): OutputItemProcessor {

    override suspend fun process(item: OutputItem) {
        val typedItem = item as? StreamedTextItem ?: return

        // Notify lifecycle observer
        lifecycleObserver.onStreamedTextProcessed()

        // Update messages
        var messages = uiStateHandler.getState().messages
        val isNewMessage = !messages.any { it.uuid == typedItem.uuid }
        if (isNewMessage) {
            val newMessage = Message(
                uuid = typedItem.uuid,
                text = typedItem.text,
                isUser = false,
                personaId = typedItem.personaId,
                name = PersonaId.fromAvatarId(typedItem.personaId).name
            )
            messages = messages + newMessage
        }
        messages = messages.map {
            var message = it
            if (message.uuid == typedItem.uuid && message.text != typedItem.text) {
                message = it.copy(text = typedItem.text)
            }
            message
        }

        // Update UI state
        uiStateHandler.updateUiState { this.copy(
            visualItem = VisualItem(text = typedItem.text),
            messages = messages
        ) }
    }
}