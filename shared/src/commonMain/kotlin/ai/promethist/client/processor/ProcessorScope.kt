package ai.promethist.client.processor

import kotlinx.coroutines.CoroutineScope
import kotlin.coroutines.CoroutineContext

class ProcessorScope(override val coroutineContext: CoroutineContext) : CoroutineScope