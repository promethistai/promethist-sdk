package ai.promethist.client.processor

import ai.promethist.channel.item.ActionsItem
import ai.promethist.channel.item.CorrectInputItem
import ai.promethist.channel.item.GrowthContentItem
import ai.promethist.channel.item.InputItem
import ai.promethist.channel.item.Modal
import ai.promethist.channel.item.OutputItem
import ai.promethist.channel.item.QuestionnaireItem
import ai.promethist.channel.item.SessionEndItem
import ai.promethist.channel.item.SetPersonaItem
import ai.promethist.client.ClientUiStateHandler
import ai.promethist.client.updateUiState

class ModalItemProcessor(
    private val uiStateHandler: ClientUiStateHandler
): OutputItemProcessor {

    override suspend fun process(item: OutputItem) {
        val typedItem = item as? Modal ?: return

        when (typedItem) {
            is ActionsItem -> Unit
            is InputItem -> Unit
            is QuestionnaireItem -> Unit
            is GrowthContentItem -> Unit
            is CorrectInputItem -> Unit
            is SetPersonaItem -> return
            is SessionEndItem -> return
        }

        uiStateHandler.updateUiState { this.copy(
            modal = typedItem,
            visualItem = null,
            transcript = null,
            isLoading = false
        ) }
    }
}