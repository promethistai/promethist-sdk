package ai.promethist.client.processor

import ai.promethist.channel.item.EndItem
import ai.promethist.channel.item.OutputItem
import ai.promethist.client.common.OutputQueue
import ai.promethist.client.avatar.AvatarPlayer

class EndItemProcessor(
    private val avatarPlayer: AvatarPlayer,
    private val outputQueue: OutputQueue
): OutputItemProcessor {

    override suspend fun process(item: OutputItem) {
        if (item !is EndItem) return

        avatarPlayer.turnEnd()
        outputQueue.setIsCompleted(true)
    }
}