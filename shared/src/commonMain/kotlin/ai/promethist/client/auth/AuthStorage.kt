package ai.promethist.client.auth

interface AuthStorage {
    var accessToken: String?
    var refreshToken: String?
    var idToken: String?
    var name: String?
    var email: String?
    var avatarUrl: String?

    fun clear() {
        accessToken = null
        refreshToken = null
        idToken = null
        name = null
        email = null
        avatarUrl = null
    }
}