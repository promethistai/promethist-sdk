package ai.promethist.client.auth.jwt

interface Payload {
    val issuer: String
    val subject: String
    val expiresAt: Long
    val issuedAt: Long
    val claims: Map<String, String>
    fun getClaim(name: String): String = claims[name] ?: error("Claim not found.")
}