package ai.promethist.client.auth

enum class AuthPolicy {
    Disabled, Optional, Required
}