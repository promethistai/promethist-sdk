package ai.promethist.client.auth

import ai.promethist.util.KeychainUtil

class SecureAuthStorage: AuthStorage {

    private val keychain by lazy { KeychainUtil }

    override var accessToken: String?
        get() = keychain.getValue(Key.AccessToken.key)
        set(value) {
            value?.let {
                keychain.saveValue(Key.AccessToken.key, it)
            } ?: run {
                keychain.removeValue(Key.AccessToken.key)
            }
        }

    override var refreshToken: String?
        get() = keychain.getValue(Key.RefreshToken.key)
        set(value) {
            value?.let {
                keychain.saveValue(Key.RefreshToken.key, it)
            } ?: run {
                keychain.removeValue(Key.RefreshToken.key)
            }
        }

    override var idToken: String?
        get() = keychain.getValue(Key.IdToken.key).takeIf { (it ?: "").isNotBlank() }
        set(value) {
            value?.let {
                keychain.saveValue(Key.IdToken.key, it)
            } ?: run {
                keychain.removeValue(Key.IdToken.key)
            }
        }

    override var name: String?
        get() = keychain.getValue(Key.Name.key).takeIf { (it ?: "").isNotBlank() }
        set(value) {
            value?.let {
                keychain.saveValue(Key.Name.key, it)
            } ?: run {
                keychain.removeValue(Key.Name.key)
            }
        }
    override var email: String?
        get() = keychain.getValue(Key.Email.key).takeIf { (it ?: "").isNotBlank() }
        set(value) {
            value?.let {
                keychain.saveValue(Key.Email.key, it)
            } ?: run {
                keychain.removeValue(Key.Email.key)
            }
        }

    override var avatarUrl: String?
        get() = keychain.getValue(Key.AvatarUrl.key).takeIf { (it ?: "").isNotBlank() }
        set(value) {
            value?.let {
                keychain.saveValue(Key.AvatarUrl.key, it)
            } ?: run {
                keychain.removeValue(Key.AvatarUrl.key)
            }
        }

    private enum class Key {
        AccessToken,
        RefreshToken,
        IdToken,
        Name,
        Email,
        AvatarUrl
        ;

        val key: String get() = name.replaceFirstChar(Char::lowercase)
    }
}