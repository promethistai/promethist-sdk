package ai.promethist.client.auth

import io.ktor.http.Url

fun Url.getString(): String = this.toString()

fun String.toKtorUrl(): Url = Url(urlString = this)