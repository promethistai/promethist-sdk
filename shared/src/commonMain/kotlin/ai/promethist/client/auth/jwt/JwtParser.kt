package ai.promethist.client.auth.jwt

import ai.promethist.common.sharedSerializer
import saschpe.kase64.*

object JwtParser {

    fun ByteArray.removeEOF(eofMarker: Byte): ByteArray {
        return this.filter { it != eofMarker }.toByteArray()
    }

    inline fun <reified T : Payload> decodeToken(token: String): JwtToken<T> {
        val parts = token.split(".")
        if (parts.size != 3) {
            throw IllegalArgumentException("Invalid JWT token")
        }

        val payloadJson = parts[1].base64UrlDecodedBytes
            .removeEOF(0x1A)
            .removeEOF(0x00)
            .decodeToString()
            .replace("\u0000", "")

        val payload = sharedSerializer.decodeFromString<T>(payloadJson)

        return JwtToken(token,payload)
    }
}