package ai.promethist.client.auth.jwt

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class RefreshTokenPayload(
    @SerialName("iss") override val issuer: String,
    @SerialName("sub") override val subject: String,
    @SerialName("exp") override val expiresAt: Long,
    @SerialName("iat") override val issuedAt: Long,
    override val claims: Map<String, String> = emptyMap(),
    @SerialName("aud") val audience: String,
    @SerialName("scope") val scope: String,
) : Payload
