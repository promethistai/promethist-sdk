package ai.promethist.client.auth

sealed class AuthException(override val message: String) : Exception(message) {
    data object MissingAuthorizationUrl : AuthException("No authorizationUrl specified in AppPreset")
    data object InvalidCredentials : AuthException("Invalid credentials")
    data object SessionExpired : AuthException("User session has expired")
    data object NotAuthenticated : AuthException("User is not authenticated")
}