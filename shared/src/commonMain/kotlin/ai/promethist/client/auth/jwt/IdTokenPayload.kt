package ai.promethist.client.auth.jwt

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class IdTokenPayload(
    @SerialName("iss") override val issuer: String,
    @SerialName("sub") override val subject: String,
    @SerialName("exp") override val expiresAt: Long,
    @SerialName("iat") override val issuedAt: Long,
    override val claims: Map<String, String> = emptyMap(),

    @SerialName("name") val name: String,
    @SerialName("family_name") val familyName: String,
    @SerialName("given_name") val givenName: String,
    @SerialName("email") val email: String,
    @SerialName("picture") val picture: String,

    ) : Payload
