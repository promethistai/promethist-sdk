package ai.promethist.client.auth.jwt

data class JwtToken<T : Payload>(
    val token: String,
    val payload: T,
)