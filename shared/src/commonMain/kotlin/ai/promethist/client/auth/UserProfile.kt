package ai.promethist.client.auth

data class UserProfile(
    val id: String?,
    val name: String?,
    val email: String?,
    val avatarUrl: String?
)
