package ai.promethist.client.auth

import ai.promethist.client.auth.jwt.AccessTokenPayload
import ai.promethist.client.auth.jwt.IdTokenPayload
import ai.promethist.client.auth.jwt.JwtToken
import ai.promethist.client.common.Result
import ai.promethist.client.model.ClientError
import io.ktor.http.Url
import kotlinx.serialization.Serializable

interface OAuth2Client {

    val decodedAccessToken: JwtToken<AccessTokenPayload>?
    val decodedIdToken: JwtToken<IdTokenPayload>?

    fun getAuthorizationUrl(): Url

    suspend fun retrieveTokens(code: String): Result<Unit>
    suspend fun refreshTokens(): Result<Unit>
    suspend fun logout()

    /**
     * Check whether url is a valid callback with authorization code.
     */
    fun shouldHandleCallback(url: Url): Boolean

    /**
     * Handle callback, extract authorization code and retrieve tokens.
     */
    suspend fun handleCallback(url: Url): Result<Unit> {
        if (!shouldHandleCallback(url)) return Result.Error(ClientError.UnknownError())

        retrieveTokens(url.parameters[PARAM_AUTHORIZATION_CODE]!!)
        return Result.Success(Unit)
    }

    /**
     * Get access token. Refreshes it in case it is expired.
     */
    suspend fun getAccessToken(): JwtToken<AccessTokenPayload>

    /**
     * Checks whether the user is logged in and we have valid tokens
     */
    fun isLoggedIn(): Boolean

    suspend fun getUserInfo(): Map<String, String>

    fun getStoredUserInfo(): Map<String, String?>

    @Serializable
    data class OAuth2ClientSettings(
        val authorization_endpoint: String,
        val client_id: String,
        val client_secret: String?,
        val token_endpoint: String,
        val logout_endpoint: String,
        val userinfo_endpoint: String,
        val redirect_uri: String,
        val issuer: String,
        val scope: String,
        val useState: Boolean = false,
    )

    companion object {
        const val PARAM_AUTHORIZATION_CODE = "code"
    }
}