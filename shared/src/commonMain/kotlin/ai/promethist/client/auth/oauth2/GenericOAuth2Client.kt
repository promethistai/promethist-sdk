package ai.promethist.client.auth.oauth2

import ai.promethist.client.*
import ai.promethist.client.auth.AuthException
import ai.promethist.client.auth.AuthStorage
import ai.promethist.client.auth.OAuth2Client
import ai.promethist.client.auth.OAuth2Client.Companion.PARAM_AUTHORIZATION_CODE
import ai.promethist.client.auth.jwt.*
import ai.promethist.client.common.AppConfig
import ai.promethist.client.common.Result
import ai.promethist.client.common.Success
import ai.promethist.client.common.catchingBackendExceptions
import ai.promethist.client.common.catchingNetworkExceptions
import ai.promethist.client.common.resultsTo
import ai.promethist.client.model.ClientError
import ai.promethist.client.model.CommonError
import ai.promethist.common.configuredHttpClient
import ai.promethist.util.LoggerDelegate
import io.ktor.client.HttpClient
import io.ktor.client.request.forms.FormDataContent
import io.ktor.client.request.header
import io.ktor.client.request.headers
import io.ktor.client.request.post
import io.ktor.client.request.setBody
import io.ktor.http.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import kotlinx.datetime.Clock
import kotlinx.datetime.Instant

class GenericOAuth2Client(
    appConfig: AppConfig,
    private val authStorage: AuthStorage,
    private val httpClient: HttpClient = configuredHttpClient(),
) : OAuth2Client {

    private val settings: OAuth2Client.OAuth2ClientSettings = appConfig.preset.authSettings ?: ClientDefaults.authSettings

    private val logger by LoggerDelegate()

    override val decodedAccessToken: JwtToken<AccessTokenPayload>?
        get() = decode(authStorage.accessToken)

    override val decodedIdToken: JwtToken<IdTokenPayload>?
        get() = decode(authStorage.idToken)

    private val decodedRefreshToken: JwtToken<RefreshTokenPayload>?
        get() = decode(authStorage.refreshToken)

    private inline fun <reified T : Payload> decode(token: String?): JwtToken<T>? = token?.let(JwtParser::decodeToken)

    override fun isLoggedIn(): Boolean =
        decodedRefreshToken?.let { Instant.fromEpochMilliseconds(it.payload.expiresAt * 1000) > Clock.System.now() }
            ?: with(authStorage) {
                clear()
                false
            }

    override fun getAuthorizationUrl(): Url = URLBuilder(settings.authorization_endpoint).apply {
        parameters.apply {
            append("response_type", "code")
            append("client_id", settings.client_id)
            if (settings.client_secret != null) append("client_secret", settings.client_secret)
            append("redirect_uri", settings.redirect_uri)
            append("scope", "openid")
        }
    }.build()

    override suspend fun getUserInfo(): Map<String, String> {
        val result: Result<UserInfoResult> = try {
            val accessToken = getAccessToken().token
            catchingBackendExceptions {
                httpClient
                    .post(settings.userinfo_endpoint) {
                        headers {
                            append("Content-Type", CONTENT_TYPE)
                            append("Authorization", "Bearer $accessToken")
                        }
                    }
                    .resultsTo(Success)
            }
        } catch (e: Exception) {
            Result.Error(ClientError.UnknownError(throwable = e))
        }

        when (result) {
            is Result.Success -> {
                logger.info("AuthClient: User info retrieved successfully.")
                authStorage.name = result.data.name
                authStorage.email = result.data.email
                val defaultAvatarUrl = buildDefaultAvatarUrl(result.data.name)
                authStorage.avatarUrl = result.data.picture ?: defaultAvatarUrl
                return mapOf(
                    "name" to result.data.name,
                    "email" to result.data.email,
                    "avatarUrl" to (result.data.picture ?: defaultAvatarUrl)
                 )
            }

            is Result.Error -> {
                logger.info("AuthClient: User info retrieval failed: ${result.error}")
                return mapOf()
            }
        }
    }

    override fun getStoredUserInfo(): Map<String, String?> = mapOf(
        "name" to authStorage.name,
        "email" to authStorage.email,
        "avatarUrl" to authStorage.avatarUrl
    )

    private fun isExpired(token: JwtToken<out Payload>): Boolean {
        val currentTime = Clock.System.now()
        return currentTime < Instant.fromEpochMilliseconds(token.payload.expiresAt)
    }

    private fun buildDefaultAvatarUrl(name: String): String {
        return "https://ui-avatars.com/api/" +
                "?name=${name.uppercase()}" +
                "&background=FFFFFF" +
                "&color=1F1F23" +
                "&font-size=0.38" +
                "&size=128"
    }

    override suspend fun retrieveTokens(code: String): ai.promethist.client.common.Result<Unit> {
        val result: Result<TokenResult> = try {
            catchingBackendExceptions {
                httpClient
                    .post(settings.token_endpoint) {
                        headers {
                            append("Content-Type", CONTENT_TYPE)
                        }
                        setBody(FormDataContent(Parameters.build {
                            append("grant_type", "authorization_code")
                            append("code", code)
                            append("client_id", "mobile")
                            append("redirect_uri", settings.redirect_uri)
                        }))
                    }
                    .resultsTo(Success)
            }
        } catch (e: Exception) {
            Result.Error(ClientError.UnknownError(throwable = e))
        }

        when (result) {
            is Result.Success -> {
                logger.info("AuthClient: Tokens retrieved successfully.")
                authStorage.accessToken = result.data.accessToken
                authStorage.refreshToken = result.data.refreshToken
                return Result.Success(Unit)
            }

            is Result.Error -> {
                logger.info("AuthClient: Login failed: ${result.error}")
                return Result.Error(result.error)
            }
        }
    }

    override suspend fun refreshTokens(): Result<Unit> {
        logger.info("AuthClient: Refreshing tokens.")
        val refreshToken = authStorage.refreshToken

        if (refreshToken == null || isExpired(decode<RefreshTokenPayload>(refreshToken)!!)) throw AuthException.SessionExpired

        val result: Result<TokenResult> = try {
            catchingNetworkExceptions {
                httpClient
                    .post(settings.token_endpoint) {
                        headers {
                            append("Content-Type", CONTENT_TYPE)
                        }
                        setBody(FormDataContent(Parameters.build {
                            append("grant_type", "refresh_token")
                            append("client_id", "mobile")
                            append("refresh_token", refreshToken)
                        }))
                    }
                    .resultsTo(Success)
            }
        } catch (e: Exception) {
            Result.Error(ClientError.UnknownError(throwable = e))
        }

        when (result) {
            is Result.Success -> {
                logger.info("AuthClient: Token were refreshed successfully.")
                authStorage.accessToken = result.data.accessToken
                authStorage.refreshToken = result.data.refreshToken
                return Result.Success(Unit)
            }

            is Result.Error -> {
                logger.info("AuthClient: Renew token failed: ${result.error}")
                if (result.error !is CommonError.NoNetworkConnection) {
                    throw AuthException.SessionExpired
                }
                return Result.Error(result.error)
            }
        }
    }

    override suspend fun logout() = withContext(Dispatchers.Default) {
        logger.info("AuthClient: Logging out..")
        authStorage.clear()

        val result: Result<Unit> = try {
            catchingBackendExceptions {
                httpClient
                    .post(settings.logout_endpoint) {
                        parameters {
                            append("post_logout_redirect", settings.redirect_uri)
                        }
                        headers {
                            authStorage.accessToken?.let {
                                append("Content-Type", CONTENT_TYPE)
                                header("Authorization", "Bearer $it")
                            }
                        }
                    }
                    .resultsTo(Success)
            }
        } catch (e: Exception) {
            e.printStackTrace()
            Result.Error(ClientError.UnknownError(throwable = e))
        }
        logger.info("AuthClient: Logout result ${result.isSuccess()}")
    }

    override fun shouldHandleCallback(url: Url): Boolean =
        url.protocolWithAuthority + url.encodedPath == settings.redirect_uri && url.parameters[PARAM_AUTHORIZATION_CODE] != null

    override suspend fun getAccessToken(): JwtToken<AccessTokenPayload> {
        decodedAccessToken?.let {
            if (!isExpired(it) || refreshTokens().isSuccess()) return decodedAccessToken!!
        }

        throw AuthException.NotAuthenticated
    }

    companion object {
        private const val CONTENT_TYPE = "application/x-www-form-urlencoded"
    }
}