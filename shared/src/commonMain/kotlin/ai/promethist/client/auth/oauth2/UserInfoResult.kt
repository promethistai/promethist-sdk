package ai.promethist.client.auth.oauth2

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class UserInfoResult(
    @SerialName("name") val name: String,
    @SerialName("preferred_username") val preferredUsername: String,
    @SerialName("given_name") val givenName: String,
    @SerialName("family_name") val familyName: String,
    @SerialName("email") val email: String,
    @SerialName("picture") val picture: String? = null
)
