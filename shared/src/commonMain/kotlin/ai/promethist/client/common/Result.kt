package ai.promethist.client.common

import io.ktor.client.call.body
import io.ktor.client.statement.HttpResponse
import kotlin.contracts.ExperimentalContracts
import kotlin.contracts.contract

/**
 * ```
 * val result = Result.Success("Hello World!")
 * val error = Result.Error(ErrorResult("Some error text"))
```
 */

sealed class Result<out T : Any> {

    data class Success<out T : Any>(val data: T) : Result<T>()

    data class Error<out T : Any>(val error: ErrorResult, val data: T? = null) : Result<T>()

    override fun toString(): String {
        return when (this) {
            is Success -> "Success[data=$data]"
            is Error -> "Error[exception=${error.throwable}"
        }
    }

    @OptIn(ExperimentalContracts::class)
    fun isSuccess(): Boolean {
        contract { returns(true) implies (this@Result is Success) }
        return this is Success
    }

    @OptIn(ExperimentalContracts::class)
    fun isError(): Boolean {
        contract { returns(true) implies (this@Result is Error) }
        return this is Error
    }

    /**
     * Returns the encapsulated value if this instance represents [Success] or cached data when available in [Running] state
     */
    fun getOrNull(): T? = when (this) {
        is Success -> data
        is Error -> data
    }

    /**
     * Returns an [ErrorResult] if this instance represents [Error] or null for [Success]
     */
    fun getErrorOrNull(): ErrorResult? = when (this) {
        is Success -> null
        is Error -> error
    }

}

abstract class ErrorResult(
    var message: String? = null,
    open val throwable: Throwable? = null
)

// === HELPER FUNCTIONS

/** Transform Result data object */
inline fun <T : Any, R : Any> Result<T>.map(transform: (T) -> R) =
    when (this) {
        is Result.Success -> Result.Success(transform(data))
        is Result.Error -> Result.Error(error, data?.let(transform))
    }

//inline fun <T : Any> Result<T>.mapToEmpty(): Result<Unit> = map { }


sealed interface ResultType
object Success : ResultType
class Error(val error: ErrorResult) : ResultType

suspend inline infix fun <reified T : Any> HttpResponse.resultsTo(result: ResultType): Result<T> =
    when (result) {
        Success -> Result.Success(body())
        is Error -> Result.Error(result.error, null)
    }


infix fun <T : Any> T.resultsTo(result: ResultType): Result<T> =
    when (result) {
        Success -> Result.Success(this)
        is Error -> Result.Error(result.error, null)
    }

infix fun <T : Any> T?.resultsTo(result: ErrorResult): Result<T> =
    this?.resultsTo(Error(result)) ?: Result.Error(result, null)