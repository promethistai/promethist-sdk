package ai.promethist.client.common

object Validator {

    fun isNameValid(entry: String): Boolean {
        val regex = "^\\p{L}{2,30}$".toRegex()
        return entry.matches(regex)
    }
}