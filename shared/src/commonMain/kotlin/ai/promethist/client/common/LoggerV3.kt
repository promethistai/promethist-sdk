package ai.promethist.client.common

import ai.promethist.client.model.LogTurnModel
import ai.promethist.client.model.SessionLogModel
import ai.promethist.time.currentTime
import ai.promethist.util.Log
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import kotlinx.datetime.Clock
import kotlinx.datetime.Instant

object LoggerV3 {

    private val loggerScope = CoroutineScope(SupervisorJob())
    private val logMutex = Mutex()
    private var log = MutableStateFlow("")
    private val sessionLogEntries = MutableStateFlow<List<SessionLogModel>>(emptyList())
    private var firstSessionTranscript: Instant? = null

    private var transcripted: Instant? = null
    private var firstTokenLatency: Long? = null
    private var firstSentenceLatency: Long? = null // Not used for now
    private var firstAudioBlockLatency: Long? = null
    private var lastAudioBlockLatency: Long? = null
    private var audioPlaybackStartLatency: Long? = null

    // Latency -------------------------------------------------------------------------------------

    fun setTranscripted() {
        transcripted = Clock.System.now()
        if (firstSessionTranscript == null)
            firstSessionTranscript = Clock.System.now()
    }

    fun resetFirstSessionTranscript() {
        firstSessionTranscript = null
    }

    fun setFirstTokenLatency(value: Long) {
        firstTokenLatency = value
    }

    fun setFirstSentenceLatency(value: Long) {
        firstSentenceLatency = value
    }

    fun setFirstAudioBlockLatency(value: Long) {
        firstAudioBlockLatency = value
    }

    fun setLastAudioBlockLatency(value: Long) {
        lastAudioBlockLatency = value
    }

    fun setAudioPlaybackStartLatency(value: Long) {
        audioPlaybackStartLatency = value
    }

    fun reset() {
        transcripted = null
        firstTokenLatency = null
        firstSentenceLatency = null
        firstAudioBlockLatency = null
        lastAudioBlockLatency = null
        audioPlaybackStartLatency = null
    }

    suspend fun clearSessionLogEntries() {
        logMutex.withLock { sessionLogEntries.value = emptyList() }
    }

    fun buildLogTurnModel(): LogTurnModel = LogTurnModel(
        transcripted = transcripted,
        firstTokenLatency = firstTokenLatency,
        firstSentenceLatency = firstSentenceLatency,
        firstAudioBlockLatency = firstAudioBlockLatency,
        lastAudioBlockLatency = lastAudioBlockLatency,
        audioPlaybackStartLatency = audioPlaybackStartLatency
    )

    // Logging -------------------------------------------------------------------------------------

    fun getLogs(): String = runBlocking { logMutex.withLock { log.value } }

    suspend fun getSessionLogEntries(
        sessionId: String,
        turnId: String?
    ): List<SessionLogModel> {
        logMutex.withLock {
            return sessionLogEntries.value.map { it.copy(sessionId = sessionId, turnId = turnId) }
        }
    }

    fun input(message: String) = loggerScope.launch {
        logMutex.withLock {
            log.update { "$it➡️ [INPUT] $message \n" }
            sessionLogEntries.update { it + SessionLogModel(
                relativeTime = relativeTime,
                msg = "[INPUT] $message"
            ) }
        }
    }

    fun output(message: String) = loggerScope.launch {
        logMutex.withLock {
            log.update { "$it⬅️️ [OUTPUT] $message \n" }
            sessionLogEntries.update { it + SessionLogModel(
                relativeTime = relativeTime,
                msg = "[OUTPUT] $message"
            ) }
        }
    }

    fun arc(message: String) = loggerScope.launch {
        logMutex.withLock {
            log.update { "$it🎥 [ARC] $message \n" }
            sessionLogEntries.update { it + SessionLogModel(
                relativeTime = relativeTime,
                msg = "[ARC] $message"
            ) }
        }
    }

    fun latency(seconds: Double, message: String = "") = loggerScope.launch {
        logMutex.withLock {
            log.update {  "$it⏳ [LATENCY] $message ${seconds.toString().take(5)}s \n" }
            sessionLogEntries.update { it + SessionLogModel(
                relativeTime = relativeTime,
                msg = "[LATENCY] $message ${seconds.toString().take(5)}s"
            ) }
        }
    }

    fun network(message: String) = loggerScope.launch {
        logMutex.withLock {
            log.update { "$it🤝 [SOCKET] $message \n" }
            sessionLogEntries.update { it + SessionLogModel(
                relativeTime = relativeTime,
                msg = "[SOCKET] $message"
            ) }
        }
    }

    fun networkBig(message: String) = loggerScope.launch {
        log.update { "$it\n\n" }
        network("=================================")
        network("======== $message ========")
        network("=================================")
        log.update { "$it\n\n" }
        logMutex.withLock {
            sessionLogEntries.update { it + SessionLogModel(
                relativeTime = relativeTime,
                msg = "======== $message ========"
            ) }
        }
    }

    fun d(message: String) = loggerScope.launch {
        logMutex.withLock {
            println("ℹ️ [DEBUG] $message \n")
            log.update { "${it}ℹ️ [DEBUG] $message \n" }
            sessionLogEntries.update { it + SessionLogModel(
                relativeTime = relativeTime,
                msg = "[DEBUG] $message"
            ) }
        }
    }

    fun e(message: String, exception: Exception) = loggerScope.launch {
        logMutex.withLock {
            val errorMessage = "‼️ [ERROR] $message ${exception.message} \n"
            log.update { it + errorMessage }
            sessionLogEntries.update { it + SessionLogModel(
                relativeTime = relativeTime,
                msg = "[ERROR] $message",
                level = Log.Entry.Level.ERROR
            ) }
        }
    }

    private val relativeTime: Float get() {
        val firstInputTime = firstSessionTranscript?.toEpochMilliseconds() ?: return 0F
        return (currentTime() - firstInputTime) / 1000F
    }
}