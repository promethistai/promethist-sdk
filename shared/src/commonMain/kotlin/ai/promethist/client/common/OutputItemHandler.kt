package ai.promethist.client.common

import ai.promethist.channel.item.OutputItem

interface OutputItemHandler {
    fun onOutputItemReceived(item: OutputItem)
    fun clearOutputQueue()
}