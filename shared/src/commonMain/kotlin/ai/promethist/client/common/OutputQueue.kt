package ai.promethist.client.common

import ai.promethist.channel.item.OutputItem
import kotlinx.coroutines.delay
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import kotlin.concurrent.Volatile

data class OutputQueue(
    private val outputItems: MutableList<OutputItem>,
    @Volatile private var _isCompleted: Boolean
) {
    private val mutex = Mutex()

    val items: List<OutputItem> get() = outputItems
    val isCompleted: Boolean get() = _isCompleted

    suspend fun setIsCompleted(isComplete: Boolean) = mutex.withLock {
        this._isCompleted = isComplete
    }

    suspend fun addItem(item: OutputItem) = mutex.withLock {
        outputItems.add(item)
    }

    suspend fun nextItem(): OutputItem? {
        while (!_isCompleted && outputItems.isEmpty()) {
            delay(50)
        }

        return if (outputItems.isEmpty()) {
            null
        } else {
            mutex.withLock {
                outputItems.removeAt(0)
            }
        }
    }

    suspend fun clear() = mutex.withLock {
        outputItems.clear()
    }

    companion object {
        val Empty = OutputQueue(
            outputItems = mutableListOf(),
            _isCompleted = false
        )
    }
}