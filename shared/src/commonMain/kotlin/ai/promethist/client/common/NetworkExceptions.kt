package ai.promethist.client.common

import ai.promethist.client.model.CommonError
import io.ktor.client.plugins.*
import io.ktor.client.statement.*
import io.ktor.http.*
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json

inline fun <R : Any> runtimeExceptions(block: () -> Result<R>): Result<R> =
    try {
        block()
    } catch (ex: RuntimeException) {
        throw ex
    }

inline fun <R : Any> catchingNetworkExceptions(block: () -> Result<R>): Result<R> =
    try {
        runtimeExceptions {
            block()
        }
    } catch (e: Throwable) {
        when (e::class.simpleName) {
            "ConnectException",
            "UnknownHostException",
            "NoRouteToHostException",
            "IOException",
            "SSLException",
            "SocketException",
            "HttpRequestTimeoutException",
            "SocketTimeoutException" -> Result.Error(CommonError.NoNetworkConnection(e))
            else -> {
                throw e
            }
        }
    } catch (ex: Exception) {
        throw ex
    }

internal suspend inline fun <R : Any> catchingBackendExceptions(
    block: () -> Result<R>
): Result<R> = try {
    catchingNetworkExceptions {
        block()
    }
} catch (e: ResponseException) {
    val errorBody = e.getError()
    val errorType: Int = e.response.status.value / 100

    when {
        e.response.status == HttpStatusCode.Unauthorized -> Result.Error(CommonError.Unauthorized(e.cause))
        errorType == 4 -> throw ClientErrorException(e.response, errorBody)
        errorType == 5 -> throw ServerErrorException(e.response, errorBody)
        else -> {
            throw e
        }
    }
}

internal abstract class ErrorException(
    val response: HttpResponse,
    val errorBody: ErrorResponseBody?,
    val status: HttpStatusCode = response.status
) : Exception("$errorBody") {
    override fun toString(): String = buildString {
        append("ClientErrorException(${response.status.value}) @ ${response.request.url}\n")
        append(errorBody.toString().prependIndent(" - "))
    }
}

internal class ClientErrorException(
    response: HttpResponse,
    errorBody: ErrorResponseBody?,
    status: HttpStatusCode = response.status
) : ErrorException(response, errorBody, status)

internal class ServerErrorException(
    response: HttpResponse,
    errorBody: ErrorResponseBody?,
    status: HttpStatusCode = response.status
) : ErrorException(response, errorBody, status)

@Serializable
internal data class ErrorResponseBody(
    val id: String?,
    @SerialName("inner_exception")
    val innerException: String?,
    val message: String?
) {
    override fun toString(): String = buildString {
        append("Id: ${id}\n")
        append("Inner exception: ${innerException}\n")
        append("Message: $message")
    }
}

internal suspend fun ResponseException.getError(): ErrorResponseBody {
    return response.bodyAsText().let {
        try {
            Json.decodeFromString(it)
        } catch (e: Exception) {
            ErrorResponseBody(null, null, null)
        }
    }
}