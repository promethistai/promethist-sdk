package ai.promethist.client.common

import ai.promethist.channel.command.PersonaSelection
import ai.promethist.channel.command.ProfileData
import ai.promethist.channel.command.TutorialData
import ai.promethist.channel.item.PersonaItem
import ai.promethist.client.AppPreset
import ai.promethist.client.ClientDefaults
import ai.promethist.client.avatar.AvatarQualityLevel
import ai.promethist.client.model.Appearance
import ai.promethist.client.model.AvatarManifest
import ai.promethist.client.model.CameraPermission
import ai.promethist.client.model.InteractionMode
import ai.promethist.client.model.MicPermission
import ai.promethist.client.speech.DuplexMode
import ai.promethist.runtime
import ai.promethist.util.Locale
import ai.promethist.video.VideoMode
import com.russhwolf.settings.ExperimentalSettingsApi
import com.russhwolf.settings.Settings
import com.russhwolf.settings.serialization.decodeValue
import com.russhwolf.settings.serialization.encodeValue
import kotlinx.serialization.ExperimentalSerializationApi

@OptIn(ExperimentalSerializationApi::class, ExperimentalSettingsApi::class)
class AppConfig(val settings: Settings) : ClientConfig {

    override val url get() =
        if (preset is AppPreset.Custom || preset is AppPreset.CustomCz) {
            customUrl
        } else {
            preset.url.ifEmpty { engineDomain }
        }

    val fileUrl get() =
        if (preset is AppPreset.Custom || preset is AppPreset.CustomCz) customFileUrl else preset.fileUrl

    override val key get() =
        if (preset is AppPreset.Custom || preset is AppPreset.CustomCz) {
            customKey
        } else {
            preset.key.ifEmpty { engineSetupName }
        }

    override val locale
        get() = engineLocale ?: preset.locale

    override val zoneId = ClientDefaults.zoneId

    override var protocolVersion = 2

    var customUrl: String
        get() = settings.getString(AppSettingsKey.CustomUrl.key, AppPreset.Custom.url)
        set(value) {
            settings.putString(AppSettingsKey.CustomUrl.key, value)
        }
    var customFileUrl: String
        get() = settings.getString(AppSettingsKey.CustomFileUrl.key, AppPreset.Custom.fileUrl)
        set(value) {
            settings.putString(AppSettingsKey.CustomFileUrl.key, value)
        }
    var customKey: String
        get() = settings.getString(AppSettingsKey.CustomKey.key, AppPreset.Custom.key)
        set(value) {
            settings.putString(AppSettingsKey.CustomKey.key, value)
        }

    override var sendLogs: Boolean
        get() = settings.getBoolean(AppSettingsKey.SendLogs.key, true)
        set(value) {
            settings.putBoolean(AppSettingsKey.SendLogs.key, value)
        }

    override var videoMode: VideoMode
        get() = settings.decodeValue(VideoMode.serializer(), AppSettingsKey.VideoMode.key,
            ClientDefaults.videoMode
        )
        set(value) {
            settings.encodeValue(VideoMode.serializer(), AppSettingsKey.VideoMode.key, value)
        }

    override var avatarQualityLevel: AvatarQualityLevel
        get() = settings.decodeValue(AvatarQualityLevel.serializer(), AppSettingsKey.QualityLevel.key,
            ClientDefaults.avatarQualityLevel
        )
        set(value) {
            settings.encodeValue(AvatarQualityLevel.serializer(), AppSettingsKey.QualityLevel.key, value)
        }

    override val deviceId: String
        get() {
            val storedId = settings.getStringOrNull(AppSettingsKey.DeviceId.key)

            return if (storedId == null) {
                val generatedId = ("${
                    runtime.systemName.lowercase()
                }:${
                    List(16) { (('a'..'f') + ('0'..'9')).random() }.joinToString("")
                }")

                settings.putString(AppSettingsKey.DeviceId.key, generatedId)
                generatedId
            } else
                storedId
        }

    override val primaryPersonaId: String?
        get() {
            return primaryPersonaItem.id//settings.getStringOrNull(AppSettingsKey.PrimaryPersonaId.key) ?: Defaults.primaryPersonaId
        }

    var primaryPersonaItem: PersonaItem
        get() = settings.decodeValue(PersonaItem.serializer(), AppSettingsKey.PrimaryPersonaItem.key, ClientDefaults.personas[0])
        set(value) {
            settings.encodeValue(PersonaItem.serializer(), AppSettingsKey.PrimaryPersonaItem.key, value)
        }

    var termsAndConditionsAccepted: Boolean
        get() = settings.getBoolean(AppSettingsKey.TermsAndConditionsAccepted.key, !ClientDefaults.defaultDisplayTerms)
        set(value) {
            settings.putBoolean(AppSettingsKey.TermsAndConditionsAccepted.key, value)
        }

    var dataProtectionAccepted: Boolean
        get() = settings.getBoolean(AppSettingsKey.DataProtectionAccepted.key, !ClientDefaults.defaultDisplayConsent)
        set(value) {
            settings.putBoolean(AppSettingsKey.DataProtectionAccepted.key, value)
        }

    var preset: AppPreset
        get() = settings.decodeValue(
            AppPreset.serializer(), AppSettingsKey.Preset.key,
            ClientDefaults.defaultPreset
        )
        set(value) {
            settings.encodeValue(AppPreset.serializer(), AppSettingsKey.Preset.key, value)
        }

    var interactionMode: InteractionMode
        get() = settings.decodeValue(InteractionMode.serializer(), AppSettingsKey.InteractionMode.key,
            ClientDefaults.interactionMode
        )
        set(value) {
            settings.encodeValue(InteractionMode.serializer(), AppSettingsKey.InteractionMode.key, value)
        }

    var appearance: Appearance
        get() = settings.decodeValue(Appearance.serializer(), AppSettingsKey.Appearance.key,
            ClientDefaults.appearance
        )
        set(value) {
            settings.encodeValue(Appearance.serializer(), AppSettingsKey.Appearance.key, value)
        }

    var homeTextInput: Boolean
        get() = settings.getBoolean(AppSettingsKey.HomeTextInput.key, false)
        set(value) {
            settings.putBoolean(AppSettingsKey.HomeTextInput.key, value)
        }

    var requireOnDeviceRecognition: Boolean
        get() = settings.getBoolean(AppSettingsKey.RequireOnDeviceRecognition.key, false)
        set(value) {
            settings.putBoolean(AppSettingsKey.RequireOnDeviceRecognition.key, value)
        }

    var preventDisplaySleep: Boolean
        get() = settings.getBoolean(AppSettingsKey.PreventDisplaySleep.key, false)
        set(value) {
            settings.putBoolean(AppSettingsKey.PreventDisplaySleep.key, value)
        }

    var showExitConversationAlert: Boolean
        get() = settings.getBoolean(AppSettingsKey.ShowExitConversationAlert.key, true)
        set(value) {
            settings.putBoolean(AppSettingsKey.ShowExitConversationAlert.key, value)
        }

    var showOverloadDisclaimerAlert: Boolean
        get() = settings.getBoolean(AppSettingsKey.ShowOverloadDisclaimerAlert.key, true)
        set(value) {
            settings.putBoolean(AppSettingsKey.ShowOverloadDisclaimerAlert.key, value)
        }

    var showNetworkAlert: Boolean
        get() = settings.getBoolean(AppSettingsKey.ShowNetworkAlert.key, true)
        set(value) {
            settings.putBoolean(AppSettingsKey.ShowNetworkAlert.key, value)
        }

    var onScreenDebug: Boolean
        get() = settings.getBoolean(AppSettingsKey.OnScreenDebug.key, false)
        set(value) {
            settings.putBoolean(AppSettingsKey.OnScreenDebug.key, value)
        }

    var micPermission: MicPermission
        get() = settings.decodeValue(MicPermission.serializer(), AppSettingsKey.MicPermission.key, MicPermission.Unknown)
        set(value) {
            settings.encodeValue(MicPermission.serializer(), AppSettingsKey.MicPermission.key, value)
        }

    var cameraPermission: CameraPermission
        get() = settings.decodeValue(CameraPermission.serializer(), AppSettingsKey.CameraPermission.key, CameraPermission.Unknown)
        set(value) {
            settings.encodeValue(CameraPermission.serializer(), AppSettingsKey.CameraPermission.key, value)
        }

    var personaSelection: PersonaSelection
        get() = settings.decodeValue(PersonaSelection.serializer(), AppSettingsKey.PersonaSelection.key, PersonaSelection(title = null, personasList = null))
        set(value) {
            settings.encodeValue(PersonaSelection.serializer(), AppSettingsKey.PersonaSelection.key, value)
        }

    var assetsVersion: Int
        get() = settings.getInt(AppSettingsKey.AssetsVersion.key, 0)
        set(value) {
            settings.putInt(AppSettingsKey.AssetsVersion.key, value)
        }

    var tutorialData: TutorialData?
        get() = settings.decodeValue(TutorialData.serializer(), AppSettingsKey.TutorialData.key, TutorialData.Empty)
        set(value) {
            value?.let {
                settings.encodeValue(TutorialData.serializer(), AppSettingsKey.TutorialData.key, it)
            } ?: run {
                settings.remove(AppSettingsKey.TutorialData.key)
            }
        }

    var profileData: ProfileData?
        get() = settings.decodeValue(ProfileData.serializer(), AppSettingsKey.ProfileData.key, ProfileData.Empty)
        set(value) {
            value?.let {
                settings.encodeValue(ProfileData.serializer(), AppSettingsKey.ProfileData.key, it)
            } ?: run {
                settings.remove(AppSettingsKey.TutorialData.key)
            }
        }

    var engineAppKey: String?
        get() = settings.getString(AppSettingsKey.EngineAppKey.key, AppPreset.Custom.key)
        set(value) {
            value?.let {
                settings.putString(AppSettingsKey.EngineAppKey.key, it)
            } ?: run {
                settings.remove(AppSettingsKey.EngineAppKey.key)
            }
        }

    var engineDomain: String
        get() = settings.getString(AppSettingsKey.EngineDomain.key, ClientDefaults.appKeyInputDefaultPreset.url)
        set(value) {
            settings.putString(AppSettingsKey.EngineDomain.key, value)
        }

    var engineSetupName: String
        get() = settings.getString(AppSettingsKey.EngineSetupName.key, ClientDefaults.appKeyInputDefaultPreset.key)
        set(value) {
            settings.putString(AppSettingsKey.EngineSetupName.key, value)
        }

    var engineLocale: Locale?
        get() = settings.decodeValue(Locale.serializer(), AppSettingsKey.EngineLocale.key, Locale.empty)
            .takeIf { it != Locale.empty }
        set(value) {
            settings.encodeValue(Locale.serializer(), AppSettingsKey.EngineLocale.key, value ?: Locale.empty)
        }

    var streamedVideosPrefetch: Boolean
        get() = settings.getBoolean(AppSettingsKey.StreamedVideosPrefetch.key, true)
        set(value) {
            settings.putBoolean(AppSettingsKey.StreamedVideosPrefetch.key, value)
        }

    var initialCommand: String
        get() = settings.getString(AppSettingsKey.InitialCommand.key, "#intro")
        set(value) {
            settings.putString(AppSettingsKey.InitialCommand.key, value)
        }

    var debugMode: Boolean
        get() = settings.getBoolean(AppSettingsKey.DebugMode.key, false)
        set(value) {
            settings.putBoolean(AppSettingsKey.DebugMode.key, value)
        }

    override var avatarManifest: AvatarManifest
        get() = settings.decodeValue(AvatarManifest.serializer(), AppSettingsKey.AvatarManifest.key,
            ClientDefaults.defaultAvatarManifest
        )
        set(value) {
            settings.encodeValue(AvatarManifest.serializer(), AppSettingsKey.AvatarManifest.key, value)
        }

    var duplexEnabled: Boolean
        get() = settings.getBoolean(AppSettingsKey.DuplexEnabled.key, false)
        set(value) {
            settings.putBoolean(AppSettingsKey.DuplexEnabled.key, value)
        }

    var duplexMode: DuplexMode
        get() = settings.decodeValue(DuplexMode.serializer(), AppSettingsKey.DuplexMode.key,
            ClientDefaults.duplexMode
        )
        set(value) {
            settings.encodeValue(DuplexMode.serializer(), AppSettingsKey.DuplexMode.key, value)
        }

    var duplexForceOutput: Boolean
        get() = settings.getBoolean(AppSettingsKey.DuplexForceOutput.key, false)
        set(value) {
            settings.putBoolean(AppSettingsKey.DuplexForceOutput.key, value)
        }

    var otelUrl: String
        get() = settings.getString(AppSettingsKey.OtelUrl.key, ClientDefaults.otelUrl)
        set(value) {
            settings.putString(AppSettingsKey.OtelUrl.key, value)
        }

    var showAppKeyEntry: Boolean
        get() = settings.getBoolean(AppSettingsKey.ShowAppKeyEntry.key, false)
        set(value) {
            settings.putBoolean(AppSettingsKey.ShowAppKeyEntry.key, value)
        }

    var showIntroVideo: Boolean
        get() = settings.getBoolean(
            AppSettingsKey.ShowIntroVideo.key,
            ClientDefaults.showIntroVideo
        )
        set(value) {
            settings.putBoolean(AppSettingsKey.ShowIntroVideo.key, value)
        }

    var lastSelectedAvatarId: String?
        get() = settings.getString(AppSettingsKey.LastSelectedAvatarId.key, "")
            .takeIf { it.isNotBlank() }
        set(value) {
            value?.let {
                settings.putString(AppSettingsKey.LastSelectedAvatarId.key, it)
            } ?: run {
                settings.remove(AppSettingsKey.LastSelectedAvatarId.key)
            }
        }

    private enum class AppSettingsKey {
        PrimaryPersonaItem,
        VideoMode,
        SendLogs,
        DeviceId,
        TermsAndConditionsAccepted,
        DataProtectionAccepted,
        Preset,
        InteractionMode,
        Appearance,
        HomeTextInput,
        RequireOnDeviceRecognition,
        PreventDisplaySleep,
        ShowExitConversationAlert,
        ShowOverloadDisclaimerAlert,
        ShowNetworkAlert,
        PersonaSelection,
        OnScreenDebug,
        CustomUrl,
        CustomFileUrl,
        CustomKey,
        CameraPermission,
        MicPermission,
        TutorialData,
        ProfileData,
        AssetsVersion,
        EngineAppKey,
        StreamedVideosPrefetch,
        InitialCommand,
        TtsEnabled,
        EngineDomain,
        EngineSetupName,
        EngineLocale,
        QualityLevel,
        DebugMode,
        AvatarManifest,
        DuplexEnabled,
        DuplexMode,
        DuplexForceOutput,
        OtelUrl,
        ShowAppKeyEntry,
        ShowIntroVideo,
        LastSelectedAvatarId;

        val key: String get() = name.replaceFirstChar(Char::lowercase)
    }

    override var ttsEnabled: Boolean
        get() = settings.getBoolean(AppSettingsKey.TtsEnabled.key, true)
        set(value) {
            settings.putBoolean(AppSettingsKey.TtsEnabled.key, value)
        }

    override fun toString() = """AppConfig(
    url=$url
    key=$key
    locale=$locale
    zoneId=$zoneId
    deviceId=$deviceId
    sendLogs=$sendLogs
)""".trimIndent()
}