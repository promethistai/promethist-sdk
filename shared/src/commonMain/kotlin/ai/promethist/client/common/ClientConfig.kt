package ai.promethist.client.common

import ai.promethist.client.avatar.AvatarQualityLevel
import ai.promethist.client.model.AvatarManifest
import ai.promethist.util.Locale
import ai.promethist.util.ZoneId
import ai.promethist.video.VideoMode

interface ClientConfig {
    val locale: Locale
    val zoneId: ZoneId
    val url: String
    val key: String
    val deviceId: String
    val primaryPersonaId: String?
    var videoMode: VideoMode
    var avatarQualityLevel: AvatarQualityLevel
    val protocolVersion: Int
    val sendLogs: Boolean
    val ttsEnabled: Boolean
    var avatarManifest: AvatarManifest
}