package ai.promethist.client.speech

import ai.promethist.client.Client
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.cancel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.datetime.Clock.System.now

class SpeechRecognizer(
    private val controller: SpeechRecognizerController
): SpeechRecognizerControllerCallback {

    private var speechRecognizerScope = CoroutineScope(Job())
    private var silenceLengthSec: Int = 30 // TODO
    private val requestAttentionIntervalSec = 5
    private var isFinal: Boolean = false
    private var isPaused: Boolean = false
    private var isFirstWordDetected: Boolean = false
    private var isAttentionRequested: Boolean = false
    private var startTime = now()

    private var onResultCallback: ResultCallback = { _, _ -> }
    private var onRawBlockCallback: RawAudioCallback = { _ -> }
    private var onFirstWordDetectedCallback: FirstWordDetectedCallback = {}

    private val isDuplexEnabled: Boolean by lazy {
        Client.appConfig().duplexEnabled
    }

    suspend fun transcribe(
        onRequestAttention: RequestAttentionCallback = {},
        onFirstWordDetected: FirstWordDetectedCallback = {},
        onResult: ResultCallback
    ) {
        if (isDuplexEnabled) {
            return
        }

        clear()

        onResultCallback = onResult
        onFirstWordDetectedCallback = onFirstWordDetected

        speechRecognizerScope.coroutineContext.cancel()
        speechRecognizerScope = CoroutineScope(Job())
        speechRecognizerScope.launch {
            startTime = now()
            while (!isFinal) {
                if (!isPaused) {
                    val durationSec: Long = (now() - startTime).inWholeSeconds

                    // User was silent too long - stop transcribe
                    if (durationSec >= silenceLengthSec) {
                        isFinal = true
                        controller.stopTranscribe()
                        onResultCallback("", true)
                    }

                    // Request attention from user
                    if (durationSec >= requestAttentionIntervalSec
                        && !isAttentionRequested
                        && requestAttentionIntervalSec > silenceLengthSec
                    ) {
                        onRequestAttention(durationSec)
                        isAttentionRequested = true
                    }
                }
                delay(50)
            }
        }

        controller.startTranscribe()
    }

    fun setOnResultCallback(callback: ResultCallback) {
        onResultCallback = callback
    }

    fun setRawBlockCallback(callback: RawAudioCallback) {
        onRawBlockCallback = callback
    }

    fun stopTranscribe() {
        isFinal = true
        controller.stopTranscribe()
    }

    fun pause() {
        isPaused = true
        controller.pause()
    }

    fun resume() {
        isPaused = false
        startTime = now()
        controller.resume()
    }

    fun setSilenceLength(length: Int) {
        silenceLengthSec = length
        controller.setSilenceLength(length)
    }

    fun onVisualItemReceived() {
        isFinal = true
        controller.onVisualItemReceived()
    }

    fun restartTranscribe() {
        controller.restartTranscribe()
    }

    fun setPrivileged(value: Boolean) {
        controller.setPrivileged(value)
    }

    private fun clear() {
        isFinal = false
        isFirstWordDetected = false
        isAttentionRequested = false
        onResultCallback = { _, _ -> }
        onFirstWordDetectedCallback = {}
    }

    override fun onTranscribeResult(transcript: String, isFinal: Boolean) {
        onResultCallback(transcript, isFinal)
        this.isFinal = isFinal
        startTime = now()

        // Check for first word
        if (!isFinal && !isFirstWordDetected) {
            onFirstWordDetectedCallback(transcript)
        }
    }

    override fun onBytesReceived(base64: String) {
        onRawBlockCallback(base64)
    }
}

private typealias ResultCallback = (String, Boolean) -> Unit
private typealias RawAudioCallback = (String) -> Unit
private typealias FirstWordDetectedCallback = (String) -> Unit
private typealias RequestAttentionCallback = (Long) -> Unit