package ai.promethist.client.speech

interface SpeechRecognizerControllerCallback {
    fun onTranscribeResult(transcript: String, isFinal: Boolean)
    fun onBytesReceived(base64: String)
}