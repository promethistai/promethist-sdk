package ai.promethist.client.speech

interface SpeechRecognizerController {
    fun setCallback(callback: SpeechRecognizerControllerCallback)
    suspend fun startTranscribe()
    fun stopTranscribe()
    fun pause()
    fun resume()
    fun setSilenceLength(length: Int)
    fun onVisualItemReceived()

    // Duplex
    fun restartTranscribe()
    fun setPrivileged(value: Boolean)
}