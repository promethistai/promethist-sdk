package ai.promethist.client.speech

import ai.promethist.client.resources.Resources
import dev.icerock.moko.resources.desc.ResourceStringDesc
import kotlinx.serialization.Serializable

@Serializable
enum class DuplexMode {
    OnDevice, Streamed;

    val localizedName: ResourceStringDesc get() = when (this.name) {
        OnDevice.name -> Resources.strings.settings.duplexModeOnDevice
        Streamed.name -> Resources.strings.settings.duplexModeStreamed
        else -> Resources.strings.settings.duplexMode
    }
}