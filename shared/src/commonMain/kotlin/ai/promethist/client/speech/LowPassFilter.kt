package ai.promethist.client.speech

import kotlin.math.PI
import kotlin.math.sin

/**
 * A class that applies a low-pass filter to audio data in WAV format.
 *
 * This implementation focuses on filtering out high-frequency components
 * above a specified cutoff frequency to smooth the audio signal.
 */
class LowPassFilter {

    companion object {
        // The sample rate in Hz for the audio data.
        private const val SAMPLE_RATE = 16000

        // The cutoff frequency in Hz for the low-pass filter.
        private const val CUT_OFF_FREQUENCY = 3500.0

        // The size of the kernel used in the low-pass filter.
        private const val SLOPE = 101
    }

    /**
     * Applies a low-pass filter to a WAV audio file represented as a byte array.
     *
     * @param data A byte array containing the WAV file data. Must include a valid WAV header.
     * @return A byte array containing the filtered audio data with the original WAV header.
     * @throws IllegalArgumentException if the input data is too small to contain a valid WAV header.
     */
    fun applyLowPass(data: ByteArray, isWav: Boolean = true): ByteArray {
        return if (isWav) {
            applyLowPassToWav(data)
        } else {
            applyLowPassToPcm(data)
        }
    }

    /**
     * Applies a low-pass filter to a WAV audio file represented as a byte array.
     *
     * @param data A byte array containing the WAV file data.
     * @return A byte array containing the filtered audio data with the original WAV header.
     */
    private fun applyLowPassToWav(data: ByteArray): ByteArray {
        val headerSize = 44
        if (data.size <= headerSize) {
            throw IllegalArgumentException("Invalid WAV data: too small to contain header and audio")
        }

        val header = data.copyOfRange(0, headerSize)
        val audioData = data.copyOfRange(headerSize, data.size)
        val samples = audioData.toShortArray()
        val filteredSamples = applyLowPassFilter(samples)
        val filteredAudioData = filteredSamples.toByteArray()
        return header + filteredAudioData
    }

    /**
     * Applies a low-pass filter to raw PCM audio data represented as a byte array.
     *
     * @param data A byte array containing raw PCM audio data.
     * @return A byte array containing the filtered PCM audio data.
     */
    private fun applyLowPassToPcm(data: ByteArray): ByteArray {
        if (data.isEmpty()) {
            throw IllegalArgumentException("Invalid PCM data: empty input")
        }

        val samples = data.toShortArray()
        val filteredSamples = applyLowPassFilter(samples)
        return filteredSamples.toByteArray()
    }

    /**
     * Applies a low-pass filter kernel to the given audio samples.
     */
    private fun applyLowPassFilter(samples: ShortArray): ShortArray {
        val kernel = createLowPassKernel()
        return convolve(samples, kernel)
    }

    /**
     * Creates a low-pass filter kernel using the sinc function and a Hamming window.
     *
     * @param cutOff The cutoff frequency in Hz (default is [CUT_OFF_FREQUENCY]).
     * @param sampleRate The sample rate in Hz (default is [SAMPLE_RATE]).
     * @param kernelSize The size of the kernel (default is [SLOPE]).
     * @return A kernel represented as a normalized array of floating-point values.
     */
    private fun createLowPassKernel(
        cutOff: Double = CUT_OFF_FREQUENCY,
        sampleRate: Int = SAMPLE_RATE,
        kernelSize: Int = SLOPE
    ): FloatArray {
        val kernel = FloatArray(kernelSize)
        val normalizedCutoff = cutOff / sampleRate
        val center = kernelSize / 2

        for (i in kernel.indices) {
            val x = i - center
            kernel[i] = if (x == 0) {
                (2 * normalizedCutoff).toFloat()
            } else {
                val sincValue = sin(2 * PI * normalizedCutoff * x) / (PI * x)
                sincValue.toFloat()
            }

            kernel[i] *= (0.54 - 0.46 * kotlin.math.cos(2 * PI * i / (kernelSize - 1))).toFloat()
        }

        val sum = kernel.sum()
        for (i in kernel.indices) {
            kernel[i] /= sum
        }

        return kernel
    }

    /**
     * Convolves the audio samples with the given filter kernel.
     *
     * @param samples An array of 16-bit audio samples.
     * @param kernel A low-pass filter kernel.
     * @return A filtered array of 16-bit audio samples.
     */
    private fun convolve(samples: ShortArray, kernel: FloatArray): ShortArray {
        val result = ShortArray(samples.size)
        val kernelHalfSize = kernel.size / 2

        for (i in samples.indices) {
            var acc = 0.0
            for (j in kernel.indices) {
                val sampleIndex = i + j - kernelHalfSize
                if (sampleIndex in samples.indices) {
                    acc += samples[sampleIndex] * kernel[j]
                }
            }
            result[i] = acc.coerceIn(Short.MIN_VALUE.toDouble(), Short.MAX_VALUE.toDouble()).toInt().toShort()
        }

        return result
    }

    /**
     * Converts a byte array to a short array (16-bit audio samples).
     */
    private fun ByteArray.toShortArray(): ShortArray {
        val shortArray = ShortArray(this.size / 2)
        for (i in shortArray.indices) {
            shortArray[i] = ((this[i * 2 + 1].toInt() shl 8) or (this[i * 2].toInt() and 0xFF)).toShort()
        }
        return shortArray
    }

    /**
     * Converts a short array (16-bit audio samples) to a byte array.
     */
    private fun ShortArray.toByteArray(): ByteArray {
        val byteArray = ByteArray(this.size * 2)
        for (i in this.indices) {
            byteArray[i * 2] = (this[i].toInt() and 0xFF).toByte()
            byteArray[i * 2 + 1] = ((this[i].toInt() shr 8) and 0xFF).toByte()
        }
        return byteArray
    }
}