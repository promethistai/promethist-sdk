package ai.promethist.client

import ai.promethist.client.common.OutputQueue
import ai.promethist.client.utils.TechnicalReportProvider
import ai.promethist.client.avatar.AvatarPlayer
import ai.promethist.client.speech.SpeechRecognizer
import ai.promethist.client.channel.ClientChannel

interface ClientModel {
    val config: ClientModelConfig
    val lifecycleObserver: ClientLifecycle
    val uiStateHandler: ClientUiStateHandler
    val channel: ClientChannel
    val avatarPlayer: AvatarPlayer
    val speechRecognizer: SpeechRecognizer
    val technicalReportProvider: TechnicalReportProvider
    val outputQueue: OutputQueue

    fun start()
    fun stop()
    fun handleEvent(event: ClientEvent)
}