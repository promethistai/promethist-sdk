package ai.promethist.client

import ai.promethist.channel.item.OutputItem
import ai.promethist.channel.model.Action
import ai.promethist.client.avatar.AvatarPlayer
import ai.promethist.client.channel.ClientChannel
import ai.promethist.client.common.OutputItemHandler
import ai.promethist.client.common.OutputQueue
import ai.promethist.client.model.ClientConnectionState
import ai.promethist.client.model.PersonaId
import ai.promethist.client.processor.AudioEndItemProcessor
import ai.promethist.client.processor.BinaryAudioItemProcessor
import ai.promethist.client.processor.EndItemProcessor
import ai.promethist.client.processor.ErrorItemProcessor
import ai.promethist.client.processor.ModalItemProcessor
import ai.promethist.client.processor.OutputItemProcessor
import ai.promethist.client.processor.PersonaItemProcessor
import ai.promethist.client.processor.ProcessorScope
import ai.promethist.client.processor.SessionEndItemProcessor
import ai.promethist.client.processor.SetPersonaItemProcessor
import ai.promethist.client.processor.StreamedTextItemProcessor
import ai.promethist.client.service.ClientService
import ai.promethist.client.service.DuplexRecognizerService
import ai.promethist.client.service.LatencyMeasurementService
import ai.promethist.client.service.LoadingIndicatorService
import ai.promethist.client.service.ReachabilityService
import ai.promethist.client.service.SyncAvatarManifestService
import ai.promethist.client.service.TechnicalReportService
import ai.promethist.client.speech.SpeechRecognizer
import ai.promethist.client.utils.TechnicalReportProvider
import ai.promethist.telemetry.Span
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.IO
import kotlinx.coroutines.Job
import kotlinx.coroutines.cancelChildren
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlin.time.Duration.Companion.milliseconds

class ClientModelImpl(
    override val config: ClientModelConfig,
    override val lifecycleObserver: ClientLifecycle,
    override val uiStateHandler: ClientUiStateHandler,
    override val avatarPlayer: AvatarPlayer,
    override val speechRecognizer: SpeechRecognizer,
    override val technicalReportProvider: TechnicalReportProvider,
    override val outputQueue: OutputQueue,
    override val channel: ClientChannel
) : ClientModel, OutputItemHandler {

    constructor(
        config: ClientModelConfig,
        uiStateHandler: ClientUiStateHandler,
        avatarPlayer: AvatarPlayer,
        speechRecognizer: SpeechRecognizer,
        technicalReportProvider: TechnicalReportProvider,
        channel: ClientChannel
    ) : this(
        config = config,
        lifecycleObserver = ClientLifecycleObserver,
        uiStateHandler = uiStateHandler,
        avatarPlayer = avatarPlayer,
        speechRecognizer = speechRecognizer,
        technicalReportProvider = technicalReportProvider,
        outputQueue = OutputQueue.Empty,
        channel = channel
    )

    init {
        channel.setup(outputItemHandler = this, lifecycleObserver = lifecycleObserver)
    }

    private val mainScope: CoroutineScope = CoroutineScope(Job())
    private val pipelineScope: CoroutineScope = CoroutineScope(Job())
    private val processorScope: ProcessorScope = ProcessorScope(Job())

    private val outputItemProcessors: Array<OutputItemProcessor> = arrayOf(
        StreamedTextItemProcessor(uiStateHandler, lifecycleObserver),
        BinaryAudioItemProcessor(avatarPlayer, lifecycleObserver, ::isPaused),
        ModalItemProcessor(uiStateHandler),
        SetPersonaItemProcessor(uiStateHandler, ::onSetPersonaEvent),
        PersonaItemProcessor(avatarPlayer),
        AudioEndItemProcessor(avatarPlayer),
        SessionEndItemProcessor(uiStateHandler, avatarPlayer, processorScope),
        EndItemProcessor(avatarPlayer, outputQueue),
        ErrorItemProcessor(uiStateHandler),
    )

    private val services: Array<ClientService> = arrayOf(
        LatencyMeasurementService(),
        TechnicalReportService(channel, technicalReportProvider),
        ReachabilityService(uiStateHandler),
        SyncAvatarManifestService(channel),
        DuplexRecognizerService(speechRecognizer, ::onSpeechRecognizerResult),
        LoadingIndicatorService(uiStateHandler),
    )

    private var isPaused: Boolean = false

    override fun start() {
        services.forEach { it.start() }
        lifecycleObserver.onStart()
        handleEvent(ClientEvent.Reset(sendAsInput = !config.isHomeScreenEnabled))
    }

    override fun stop() {
        lifecycleObserver.onStop()
        services.forEach { it.stop() }
        cancelProcessPipeline()
        cancelMainScope()
    }

    override fun handleEvent(event: ClientEvent) {
        if (event.shouldCancelPrevious) {
            cancelMainScope()
        }

        mainScope.launch {
            when (event) {
                is ClientEvent.UserInput -> processUserInput(event.input)
                is ClientEvent.SetPersona -> onSetPersonaEvent(event.ref, event.avatarRef, event.sendAsInput)
                is ClientEvent.Reset -> onResetEvent(event.sendAsInput)
                is ClientEvent.Pause -> onPauseEvent()
                is ClientEvent.Resume -> onResumeEvent()
                is ClientEvent.BargeIn -> onBargeInEvent()
                is ClientEvent.SyncSettings -> onSyncSettingsEvent()
            }
        }
    }

    override fun onOutputItemReceived(item: OutputItem) {
        mainScope.launch { outputQueue.addItem(item) }
    }

    override fun clearOutputQueue() {
        mainScope.launch { outputQueue.clear() }
    }

    private suspend fun onResetEvent(sendAsInput: Boolean) {
        lifecycleObserver.onReset()
        cancelProcessPipeline()
        channel.reset()
        speechRecognizer.stopTranscribe()
        outputQueue.clear()
        avatarPlayer.reset()

        uiStateHandler.updateUiState { this.setResetState() }
        isPaused = false

        if (!sendAsInput) {
            outputQueue.setIsCompleted(false)
            return
        }
        processUserInput(input = Action.Intro.text)
    }

    private suspend fun onPauseEvent() {
        speechRecognizer.pause()
        avatarPlayer.pauseAudio()

        uiStateHandler.updateUiState { this.setPausedState(true) }
        isPaused = true
    }

    private suspend fun onResumeEvent() {
        speechRecognizer.resume()
        avatarPlayer.resumeAudio()

        uiStateHandler.updateUiState { this.setPausedState(false) }
        isPaused = false
    }

    private fun onSetPersonaEvent(
        ref: String,
        avatarRef: String,
        sendAsInput: Boolean
    ) {
        val personaId = PersonaId.fromAvatarId(avatarRef)
        val input = Action.buildSetPersonaFromAvatarRef(ref)

        avatarPlayer.setPersona(personaId, withFirstContact = false)
        if (!sendAsInput) return
        processUserInput(input)
    }

    private suspend fun onBargeInEvent() {
        if (!isInternetConnected()) return
        //channel.setIgnoreIncomingData(true) TODO
        cancelProcessPipeline()
        outputQueue.clear()
        avatarPlayer.stopAudio()
        isPaused = false
        uiStateHandler.updateUiState { this.setBargeInActiveState() }
        startTranscribe()
    }

    private suspend fun onSyncSettingsEvent() = withContext(Dispatchers.IO) {
        avatarPlayer.refreshAvatarQuality()
    }

    private fun processUserInput(input: String) = mainScope.launch {
        if (!isInternetConnected()) {
            return@launch
        }

        cancelProcessPipeline()
        lifecycleObserver.onUserInput()
        avatarPlayer.stopAudio()
        uiStateHandler.updateUiState { this.setProcessUserInputState(input) }
        outputQueue.clear()
        outputQueue.setIsCompleted(false)

        withUserInputTrace(channel) { span ->
            launchProcessPipeline(span)
            channel.process(input)
        }
    }

    private fun launchProcessPipeline(span: Span? = null) = pipelineScope.launch {
        while (true) {
            val item: OutputItem = outputQueue.nextItem() ?: break
            withOutputItemTrace(outputItem = item, parent = span) {
                outputItemProcessors.forEach {
                    it.process(item)
                }
            }
        }

        waitTillPlaybackFinish()
        waitTillConnectionAvailable()
        startTranscribe()
    }

    private suspend fun startTranscribe() {
        if (!isStartTranscribeAllowed()) {
            return
        }

        lifecycleObserver.onStartTranscribe()
        uiStateHandler.updateUiState { this.setStartTranscribeState() }
        speechRecognizer.transcribe(onResult = ::onSpeechRecognizerResult)
    }

    private fun onSpeechRecognizerResult(transcript: String, isFinal: Boolean) {
        mainScope.launch {
            uiStateHandler.updateUiState { this.setActiveTranscribeState(transcript) }
            if (isFinal) outputQueue.clear()
        }
        if (isFinal) {
            processUserInput(transcript)
        }
    }

    private fun cancelProcessPipeline() {
        pipelineScope.coroutineContext.cancelChildren()
        processorScope.coroutineContext.cancelChildren()
    }

    private fun cancelMainScope() {
        mainScope.coroutineContext.cancelChildren()
    }

    private suspend fun waitTillConnectionAvailable() {
        while (!isInternetConnected())
            delay(200.milliseconds)
    }

    private suspend fun waitTillPlaybackFinish() {
        while (avatarPlayer.isPlaying)
            delay(200.milliseconds)
    }

    private suspend fun isStartTranscribeAllowed(): Boolean =
        uiStateHandler.getState().modal == null

    private suspend fun isInternetConnected(): Boolean =
        uiStateHandler.getState().connectionState != ClientConnectionState.Disconnected
}