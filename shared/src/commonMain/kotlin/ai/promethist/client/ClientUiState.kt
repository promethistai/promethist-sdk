package ai.promethist.client

import ai.promethist.channel.item.ErrorItem
import ai.promethist.channel.item.Modal
import ai.promethist.channel.item.VisualItem
import ai.promethist.client.model.ClientConnectionState
import ai.promethist.client.model.Message
import ai.promethist.client.model.AudioIndicatorState

/**
 * Represents the state of the client UI, encapsulating various aspects such as
 * visual elements, modals, error messages, user interactions, and system states.
 *
 * @property visualItem The current visual item displayed in the UI, or null if none.
 * @property modal The currently active modal, or null if no modal is active.
 * @property error The current error message or details, or null if no error exists.
 * @property messages The list of messages displayed in the UI.
 * @property transcript The user's transcript, or null if no transcript exists.
 * @property isListening Indicates whether the client is actively listening for input.
 * @property isPaused Indicates whether the client is in a paused state.
 * @property isLoading Indicates whether the client is currently performing a loading action.
 * @property isBargeInActive Indicates whether a barge-in event is currently active.
 * @property connectionState The current state of the client's connection.
 * @property audioIndicatorState The current state of the audio indicator in the UI.
 */
data class ClientUiState(
    val visualItem: VisualItem?,
    val modal: Modal?,
    val error: ErrorItem?,
    val messages: List<Message>,
    val transcript: String?,

    val isPaused: Boolean,
    var isLoading: Boolean,
    val isBargeInActive: Boolean,

    val connectionState: ClientConnectionState,
    val audioIndicatorState: AudioIndicatorState,
) {
    val isListening: Boolean
        get() = audioIndicatorState in setOf(
            AudioIndicatorState.StartSpeaking,
            AudioIndicatorState.Speaking
        )

    companion object {
        val Initial = ClientUiState(
            visualItem = null,
            modal = null,
            error = null,
            messages = emptyList(),
            transcript = null,

            isPaused = false,
            isLoading = false,
            isBargeInActive = false,

            connectionState = ClientConnectionState.Connected,
            audioIndicatorState = AudioIndicatorState.None,
        )
    }
}
