package ai.promethist.client

import ai.promethist.client.auth.OAuth2Client
import ai.promethist.util.Locale
import kotlinx.serialization.Serializable

@Serializable
sealed class AppPreset(
    val id: String,
    val name: String,
    val url: String,
    val fileUrl: String = url,
    val key: String,
    val shouldConfirmMic: Boolean = false,
    val shouldConfirmCamera: Boolean = false,
    val locale: Locale = Locale.en_US,
    @Deprecated("Use authSettings")
    val authorizationUrl: String? = null,
    val authSettings: OAuth2Client.OAuth2ClientSettings? = null,
) {
    @Serializable
    data object Engine : AppPreset(
        id = "engine",
        name = "Engine",
        url = "https://engine.promethist.ai",
        fileUrl = "https://engine.promethist.ai",
        key = Client.appConfig().engineAppKey ?: "",
        locale = Locale.cs_CZ,
        shouldConfirmMic = true
    )

    @Serializable
    data object EngineV3 : AppPreset(
        id = "engine-v3",
        name = "Engine V3",
        url = "",
        fileUrl = "https://engine.promethist.ai",
        key = "",
        locale = Locale.cs_CZ,
        authorizationUrl = "https://keycloak.promethist.ai/realms/promethist/protocol/openid-connect/auth?response_type=code&client_id=mobile&scope=openid&redirect_uri=http://promethist.ai"
    )

    @Serializable
    data object EnginePreview : AppPreset(
        id = "engine-preview",
        name = "Engine (preview)",
        url = "https://engine-preview.promethist.ai",
        fileUrl = "https://engine-preview.promethist.ai",
        key = Client.appConfig().engineAppKey ?: "",
        locale = Locale.cs_CZ,
        shouldConfirmMic = true,
        authorizationUrl = "https://keycloak.promethist.ai/realms/promethist/protocol/openid-connect/auth?response_type=code&client_id=mobile&scope=openid&redirect_uri=http://promethist.ai"
    )

    @Serializable
    data object ElysaiPreview : AppPreset(
        id = "elysai-preview",
        name = "Elysai (preview)",
        url = "https://core-elysai-preview.flowstorm.ai",
        fileUrl = "https://engine-preview.promethist.ai",
        key = "64ad211ef3a1040e23386b12",
        shouldConfirmMic = true
    )

    @Serializable
    data object Elysai : AppPreset(
        id = "elysai",
        name = "Elysai",
        url = "https://core-elysai.flowstorm.ai",
        fileUrl = "https://engine.promethist.ai",
        key = "64ad211ef3a1040e23386b12",
        shouldConfirmMic = true,
        shouldConfirmCamera = true
    )

    @Serializable
    data object ElysaiOldPreview : AppPreset(
        id = "elysai-old-preview",
        name = "Old Elysai (preview)",
        url = "https://core-elysai-preview.flowstorm.ai",
        fileUrl = "https://engine-preview.promethist.ai",
        key = "632af932091b903bd1ba601b"
    )

    @Serializable
    data object ElysaiOld : AppPreset(
        id = "elysai-old",
        name = "Old Elysai",
        url = "https://core-elysai.flowstorm.ai",
        fileUrl = "https://engine.promethist.ai",
        key = "632af932091b903bd1ba601b"
    )

    @Serializable
    data object SystemTestPreview : AppPreset(
        id = "test-preview",
        name = "System Test (preview)",
        url = "https://core-preview.flowstorm.ai",
        fileUrl = "https://engine-preview.promethist.ai",
        key = "646b32029878b4484e1dce3b"
    )

    @Serializable
    data object SystemTest : AppPreset(
        id = "test",
        name = "System Test",
        url = "https://core.flowstorm.ai",
        key = "646b32029878b4484e1dce3b"
    )

    @Serializable
    data object TestLocal : AppPreset(
        id = "test-local",
        name = "System Test (local)",
        url = "https://zayda.eu.ngrok.io",
        key = "646b32029878b4484e1dce3b"
    )

    @Serializable
    data object DFHA : AppPreset(
        id = "dfha",
        name = "DFHA",
        url = "https://core-csas.flowstorm.ai",
        fileUrl = "https://engine.promethist.ai",
        key = "65327552bcfa4117212f705a",
        locale = Locale.cs_CZ,
        shouldConfirmMic = true,
        shouldConfirmCamera = true
    )

    @Serializable
    data object DFHAPreview : AppPreset(
        id = "dfha-preview",
        name = "DFHA (preview)",
        url = "https://core-csas-preview.flowstorm.ai",
        fileUrl = "https://engine-preview.promethist.ai",
        key = "65327552bcfa4117212f705a",
        locale = Locale.cs_CZ,
        shouldConfirmMic = true,
        shouldConfirmCamera = true
    )

    @Serializable
    data object DFHAEN : AppPreset(
        id = "dfha-en",
        name = "DFHA English",
        url = "https://core-csas.flowstorm.ai",
        fileUrl = "https://engine.promethist.ai",
        key = "66604fcc7da7d6132b8242f1",
        shouldConfirmMic = true,
        shouldConfirmCamera = true
    )

    @Serializable
    data object DFHAENPreview : AppPreset(
        id = "dfha-en-preview",
        name = "DFHA English (preview)",
        url = "https://core-csas-preview.flowstorm.ai",
        fileUrl = "https://engine-preview.promethist.ai",
        key = "66604fcc7da7d6132b8242f1",
        shouldConfirmMic = true,
        shouldConfirmCamera = true
    )

    @Serializable
    data object DFHASystemTest : AppPreset(
        id = "dfha-system-test",
        name = "DFHA System Test",
        url = "https://core-csas.flowstorm.ai",
        fileUrl = "https://engine.promethist.ai",
        key = "646b32029878b4484e1dce3b",
        locale = Locale.cs_CZ
    )

    @Serializable
    data object DFHASystemTestPreview : AppPreset(
        id = "dfha-system-test-preview",
        name = "DFHA System Test (preview)",
        url = "https://core-csas-preview.flowstorm.ai",
        fileUrl = "https://engine-preview.promethist.ai",
        key = "646b32029878b4484e1dce3b",
        locale = Locale.cs_CZ,
        shouldConfirmMic = true
    )

    @Serializable
    data object Custom : AppPreset(
        id = "custom",
        name = "Custom",
        url = "https://core.flowstorm.ai",
        key = "646b32029878b4484e1dce3b",
        fileUrl = "https://engine.promethist.ai"
    )

    @Serializable
    data object CustomCz : AppPreset(
        id = "custom-cz",
        name = "Custom (Czech)",
        url = "https://core-preview.flowstorm.ai",
        key = "65f848f51748cd5991f41e44",
        fileUrl = "https://engine.promethist.ai",
        locale = Locale.cs_CZ
    )

    @Serializable
    data object TMobile : AppPreset(
        id = "tmobile",
        name = "T-Mobile",
        url = "https://core-tmobile.flowstorm.ai",
        fileUrl = "https://engine.promethist.ai",
        key = "662f5bf0b0ce5916797efe11",
        locale = Locale.cs_CZ,
        authorizationUrl = "https://keycloak.promethist.ai/realms/promethist/protocol/openid-connect/auth?response_type=code&client_id=mobile&scope=openid&redirect_uri=http://promethist.ai"
    )

    @Serializable
    data object TMobilePreproduction : AppPreset(
        id = "tmobile-preproduction",
        name = "T-Mobile (pre-production)",
        url = "https://core-tmobile.flowstorm.ai",
        fileUrl = "https://engine.promethist.ai",
        key = "66a74b497186c556a48131cb",
        locale = Locale.cs_CZ,
        authorizationUrl = "https://keycloak.promethist.ai/realms/promethist/protocol/openid-connect/auth?response_type=code&client_id=mobile&scope=openid&redirect_uri=http://promethist.ai"
    )

    @Serializable
    data object TMobilePreview : AppPreset(
        id = "tmobile-preview",
        name = "T-Mobile (preview)",
        url = "https://core-tmobile-preview.flowstorm.ai",
        fileUrl = "https://engine-preview.promethist.ai",
        key = "664767af2c66c43792f59e3b",
        locale = Locale.cs_CZ,
        authorizationUrl = "https://keycloak.promethist.ai/realms/promethist/protocol/openid-connect/auth?response_type=code&client_id=mobile&scope=openid&redirect_uri=http://promethist.ai"
    )

    @Serializable
    data object TMobileDevelopment : AppPreset(
        id = "tmobile-development",
        name = "T-Mobile (roaming development)",
        url = "https://core-tmobile-preview.flowstorm.ai",
        fileUrl = "https://engine-preview.promethist.ai",
        key = "66867709de4a3f3c5964b194",
        locale = Locale.cs_CZ,
        authorizationUrl = "https://keycloak.promethist.ai/realms/promethist/protocol/openid-connect/auth?response_type=code&client_id=mobile&scope=openid&redirect_uri=http://promethist.ai"
    )

    @Serializable
    data object TMobileEN : AppPreset(
        id = "tmobile-en",
        name = "T-Mobile EN",
        url = "https://core-tmobile.flowstorm.ai",
        fileUrl = "https://engine.promethist.ai",
        key = "66f410c469fa18744bb925fc",
        authorizationUrl = "https://keycloak.promethist.ai/realms/promethist/protocol/openid-connect/auth?response_type=code&client_id=mobile&scope=openid&redirect_uri=http://promethist.ai"
    )

    @Serializable
    data object TMobilePreproductionEN : AppPreset(
        id = "tmobile-preproduction-en",
        name = "T-Mobile EN (pre-production)",
        url = "https://core-tmobile.flowstorm.ai",
        fileUrl = "https://engine.promethist.ai",
        key = "66f410c469fa18744bb925fc",
        authorizationUrl = "https://keycloak.promethist.ai/realms/promethist/protocol/openid-connect/auth?response_type=code&client_id=mobile&scope=openid&redirect_uri=http://promethist.ai"
    )

    @Serializable
    data object TMobilePreviewEN : AppPreset(
        id = "tmobile-preview-en",
        name = "T-Mobile EN (preview)",
        url = "https://core-tmobile-preview.flowstorm.ai",
        fileUrl = "https://engine-preview.promethist.ai",
        key = "66f410c469fa18744bb925fc",
        authorizationUrl = "https://keycloak.promethist.ai/realms/promethist/protocol/openid-connect/auth?response_type=code&client_id=mobile&scope=openid&redirect_uri=http://promethist.ai"
    )

    @Serializable
    data object TMobileDevelopmentEN : AppPreset(
        id = "tmobile-development-en",
        name = "T-Mobile EN (roaming development)",
        url = "https://core-tmobile-preview.flowstorm.ai",
        fileUrl = "https://engine-preview.promethist.ai",
        key = "66f410c469fa18744bb925fc",
        authorizationUrl = "https://keycloak.promethist.ai/realms/promethist/protocol/openid-connect/auth?response_type=code&client_id=mobile&scope=openid&redirect_uri=http://promethist.ai"
    )

    @Serializable
    data object TMobileSystemTest : AppPreset(
        id = "tmobile-system-test",
        name = "T-Mobile System Test",
        url = "https://core-tmobile.flowstorm.ai",
        fileUrl = "https://engine.promethist.ai",
        key = "646b32029878b4484e1dce3b",
        locale = Locale.cs_CZ,
        authorizationUrl = "https://keycloak.promethist.ai/realms/promethist/protocol/openid-connect/auth?response_type=code&client_id=mobile&scope=openid&redirect_uri=http://promethist.ai"
    )

    @Serializable
    data object TMobileSystemTestPreview : AppPreset(
        id = "tmobile-system-test-preview",
        name = "T-Mobile System Test (preview)",
        url = "https://core-tmobile-preview.flowstorm.ai",
        fileUrl = "https://engine-preview.promethist.ai",
        key = "646b32029878b4484e1dce3b",
        locale = Locale.cs_CZ,
        authorizationUrl = "https://keycloak.promethist.ai/realms/promethist/protocol/openid-connect/auth?response_type=code&client_id=mobile&scope=openid&redirect_uri=http://promethist.ai"
    )

    @Serializable
    data object Gaia : AppPreset(
        id = "gaia",
        name = "Gaia",
        url = "https://core-strommy.flowstorm.ai",
        fileUrl = "https://engine.promethist.ai",
        key = "660ffdd369db3736f98f1098",
        locale = Locale.cs_CZ,
        shouldConfirmMic = false,
        shouldConfirmCamera = false
    )

    @Serializable
    data object GaiaPreview : AppPreset(
        id = "gaia-preview",
        name = "Gaia (preview)",
        url = "https://core-strommy-preview.flowstorm.ai",
        fileUrl = "https://engine-preview.promethist.ai",
        key = "660ffdd369db3736f98f1098",
        locale = Locale.cs_CZ,
        shouldConfirmMic = false,
        shouldConfirmCamera = false
    )

    @Serializable
    data object GaiaSystemTest : AppPreset(
        id = "gaia-system-test",
        name = "Gaia System Test",
        url = "https://core-strommy.flowstorm.ai",
        fileUrl = "https://engine.promethist.ai",
        key = "646b32029878b4484e1dce3b",
        locale = Locale.cs_CZ
    )

    @Serializable
    data object GaiaSystemTestPreview : AppPreset(
        id = "gaia-system-test-preview",
        name = "Gaia System Test (preview)",
        url = "https://core-strommy-preview.flowstorm.ai",
        fileUrl = "https://engine-preview.promethist.ai",
        key = "646b32029878b4484e1dce3b",
        locale = Locale.cs_CZ
    )

    @Serializable
    data object DefaultV3Dev : AppPreset(
        id = "default-v3-dev",
        name = "Default V3 (development)",
        url = "https://engine.promethist.dev",
        fileUrl = "https://engine-preview.promethist.ai",
        key = "default"
    )

    @Serializable
    data object DefaultV3Preview : AppPreset(
        id = "default-v3-preview",
        name = "Default V3 (preview)",
        url = "https://engine-preview.promethist.ai",
        fileUrl = "https://engine-preview.promethist.ai",
        key = "default"
    )

    @Serializable
    data object DefaultV3 : AppPreset(
        id = "default-v3",
        name = "Default V3",
        url = "https://engine.promethist.ai",
        fileUrl = "https://engine.promethist.ai",
        key = "default"
    )

    @Serializable
    data object TMobileV3Dev : AppPreset(
        id = "tmobile-v3-dev",
        name = "T-Mobile V3 (development)",
        url = "https://tmobile-engine.promethist.dev",
        fileUrl = "https://tmobile-engine.promethist.dev",
        key = "persona:embedded",
        locale = Locale.cs_CZ,
        authorizationUrl = "https://keycloak.promethist.ai/realms/promethist/protocol/openid-connect/auth?response_type=code&client_id=mobile&scope=openid&redirect_uri=http://promethist.ai"
    )

    @Serializable
    data object TMobileV3Preview : AppPreset(
        id = "tmobile-v3-preview",
        name = "T-Mobile V3 (preview)",
        url = "https://tmobile-engine-preview.promethist.ai",
        fileUrl = "https://tmobile-engine-preview.promethist.ai",
        key = "persona:embedded",
        locale = Locale.cs_CZ,
        authorizationUrl = "https://keycloak.promethist.ai/realms/promethist/protocol/openid-connect/auth?response_type=code&client_id=mobile&scope=openid&redirect_uri=http://promethist.ai"
    )

    @Serializable
    data object TMobileV3 : AppPreset(
        id = "tmobile-v3",
        name = "T-Mobile V3",
        url = "https://tmobile-engine.promethist.ai",
        fileUrl = "https://tmobile-engine.promethist.ai",
        key = "persona:embedded",
        locale = Locale.cs_CZ,
        authorizationUrl = "https://keycloak.promethist.ai/realms/promethist/protocol/openid-connect/auth?response_type=code&client_id=mobile&scope=openid&redirect_uri=http://promethist.ai"
    )

    @Serializable
    data object RoamingV3 : AppPreset(
        id = "roaming-v3",
        name = "Roaming V3",
        url = "https://tmobile-engine.promethist.ai",
        fileUrl = "https://tmobile-engine.promethist.ai",
        key = "persona:t_mobile_roaming_o36el",
        locale = Locale.cs_CZ,
        authSettings = OAuth2Client.OAuth2ClientSettings(
            authorization_endpoint = "https://keycloak.promethist.dev/realms/tmobile/protocol/openid-connect/auth",
            token_endpoint = "https://keycloak.promethist.dev/realms/tmobile/protocol/openid-connect/token",
            userinfo_endpoint = "https://keycloak.promethist.dev/realms/tmobile/protocol/openid-connect/userinfo",
            logout_endpoint = "https://keycloak.promethist.dev/realms/tmobile/protocol/openid-connect/logout",
            client_id = "mobile",
            client_secret = null,
            redirect_uri = "https://tmo.promethist.ai/",
            issuer = "https://keycloak.promethist.dev/realms/tmobile",
            scope = "openid",
        )
    )

    @Serializable
    data object RoamingV3Preview : AppPreset(
        id = "roaming-v3-preview",
        name = "Roaming V3 (preview)",
        url = "https://tmobile-engine-preview.promethist.ai",
        fileUrl = "https://tmobile-engine-preview.promethist.ai",
        key = "persona:t_mobile_roaming_o36el",
        locale = Locale.cs_CZ,
        authSettings = OAuth2Client.OAuth2ClientSettings(
            authorization_endpoint = "https://keycloak.promethist.dev/realms/tmobile/protocol/openid-connect/auth",
            token_endpoint = "https://keycloak.promethist.dev/realms/tmobile/protocol/openid-connect/token",
            userinfo_endpoint = "https://keycloak.promethist.dev/realms/tmobile/protocol/openid-connect/userinfo",
            logout_endpoint = "https://keycloak.promethist.dev/realms/tmobile/protocol/openid-connect/logout",
            client_id = "mobile",
            client_secret = null,
            redirect_uri = "https://tmo.promethist.ai/",
            issuer = "https://keycloak.promethist.dev/realms/tmobile",
            scope = "openid",
        )
    )
}