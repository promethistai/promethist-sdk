package ai.promethist.client.channel

import ai.promethist.channel.item.ErrorItem
import ai.promethist.client.common.AppConfig
import ai.promethist.client.model.ApplicationInfo
import ai.promethist.client.Client
import ai.promethist.client.common.OutputItemHandler
import ai.promethist.client.common.LoggerV3
import ai.promethist.client.resources.Resources
import ai.promethist.client.ClientLifecycle
import ai.promethist.client.ClientLifecycleObserver
import ai.promethist.client.auth.OAuth2Client
import ai.promethist.common.configuredHttpClient
import ai.promethist.telemetry.SpanContext
import io.ktor.client.HttpClient
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch

abstract class ClientChannel(
    val httpClient: HttpClient = configuredHttpClient(),
    val latencyMonitor: ClientChannelLatencyMonitor = ClientChannelLatencyMonitor(),
    private var diagnosticsScope: CoroutineScope = CoroutineScope(SupervisorJob()),
    protected val appConfig: AppConfig = Client.appConfig(),
    protected val authClient: OAuth2Client = Client.authClient(),
) {
    protected lateinit var outputItemHandler: OutputItemHandler
    protected lateinit var lifecycleObserver: ClientLifecycle
    protected lateinit var outputItemParser: OutputItemParser
    protected lateinit var reachabilityMonitor: ClientChannelReachabilityMonitor

    protected var sessionId: String? = null
    val sessionConfig = ClientSessionConfig(appConfig, ::sessionId)
    var applicationInfo: ApplicationInfo? = null

    abstract suspend fun reset()
    abstract suspend fun sessionTrace(spanContext: SpanContext)

    open suspend fun process(input: String) {
        if (!::outputItemHandler.isInitialized || !::lifecycleObserver.isInitialized
            || !::outputItemParser.isInitialized || !::reachabilityMonitor.isInitialized) {
            throw Exception("ClientChannel was not initialized, use setup method before process!")
        }
    }

    fun setup(
        outputItemHandler: OutputItemHandler,
        lifecycleObserver: ClientLifecycle = ClientLifecycleObserver
    ) {
        this.outputItemHandler = outputItemHandler
        this.lifecycleObserver = lifecycleObserver
        this.outputItemParser = OutputItemParser(outputItemHandler)
        this.reachabilityMonitor = ClientChannelReachabilityMonitor(lifecycleObserver, latencyMonitor)
    }

    protected fun sendDiagnostics(turnId: String?) = diagnosticsScope.launch {
        try {
            sendLatency(turnId = turnId)
            sendLogs(
                sessionId = sessionId,
                turnId = turnId
            )
        } catch (e: Exception) {
            LoggerV3.e("Sending diagnostics", e)
            e.printStackTrace()
        }
    }

    companion object {
        val socketConnectionError = ErrorItem(localizedText = Resources.strings.error.errorSocketConnection)
        val sessionExpiredError = ErrorItem(text = "session_expired")

        fun buildCouldNotStartSessionError(throwable: Throwable? = null): ErrorItem =
            ErrorItem(
                text = throwable?.message ?: "",
                localizedText = Resources.strings.error.errorInvalidDomain,
                throwable = throwable
            )
    }
}