package ai.promethist.client.channel

interface ClientChannelSocketIOCallback {
    fun onTextReceived(payload: String)
    fun onErrorReceived(message: String?)
}