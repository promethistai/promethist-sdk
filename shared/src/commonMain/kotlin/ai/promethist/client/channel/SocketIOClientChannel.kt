package ai.promethist.client.channel

import ai.promethist.channel.item.ErrorItem
import ai.promethist.client.auth.AuthException
import ai.promethist.client.auth.jwt.AccessTokenPayload
import ai.promethist.client.auth.jwt.JwtToken
import ai.promethist.client.common.LoggerV3
import ai.promethist.telemetry.SpanContext
import ai.promethist.util.LoggerDelegate
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock

class SocketIOClientChannel(
    private val controller: ClientChannelSocketIOController
) : ClientChannel(), ClientChannelSocketIOCallback {

    private val refreshSessionLock = Mutex()
    private val logger by LoggerDelegate()
    private var currentAccessToken: JwtToken<AccessTokenPayload>? = null

    init {
        controller.setup()
    }

    override suspend fun process(input: String) {
        if (!refreshSession()) return
        reachabilityMonitor.observeNetworkReachability()
        cancelProcessing()
        emitAuthorizationIfNeeded()
        try {
            logger.info("$CHANNEL_LOG_PREFIX Sending input: $input")
            controller.registerCallback(this)
            outputItemParser.clean()
            latencyMonitor.onProcessingStart()
            controller.emit(input.ensureProperInputEnding())
        } catch (e: Exception) {
            LoggerV3.e("Socket sending error", e)
            e.printStackTrace()
            outputItemHandler.onOutputItemReceived(ErrorItem("${e.message}"))
        }
    }

    override fun onTextReceived(payload: String) {
        reachabilityMonitor.checkProcessConnectionStability()
        outputItemParser.parse(payload)
        when {
            payload.startsWith("#binary:") -> {
                reachabilityMonitor.checkAudioConnectionStability()
            }
            payload.startsWith("#audio-end") || payload.startsWith("#error") -> {
                latencyMonitor.clearLastBinaryMessageTimestamp()
            }
            payload.startsWith("#exit") -> {
                val parameters = payload.parseParameters(
                    prefix = "#exit:",
                    keys = arrayOf("turn.id", "session.id")
                )
                val sessionId = parameters["session.id"]
                this@SocketIOClientChannel.sessionId = sessionId

                latencyMonitor.clearLastBinaryMessageTimestamp()
                sendDiagnostics(parameters["turn.id"])
                cancelProcessing()
            }
        }
    }

    override fun onErrorReceived(message: String?) {
        val exception = Exception(message)
        outputItemHandler.onOutputItemReceived(buildCouldNotStartSessionError(exception))
        LoggerV3.e("Socket error", exception)
    }

    override suspend fun reset() {
        LoggerV3.network("Reset")
        cancelProcessing()
        closeSession()
    }

    override suspend fun sessionTrace(spanContext: SpanContext) {
        if (!refreshSession()) return
        try {
            val traceId = spanContext.getTraceId()
            val spanId = spanContext.getSpanId()
            controller.emit("#session-trace:traceId=$traceId,spanId=$spanId")
        } catch (e: Exception) {
            LoggerV3.e("Could not send #session-trace", e)
            e.printStackTrace()
        }
        LoggerV3.d("#session-trace sent successfully")
    }

    private suspend fun refreshSession(): Boolean = refreshSessionLock.withLock {
        return@withLock try {
            if (authClient.isLoggedIn()) {
                authClient.refreshTokens()
            }
            controller.connect(sessionConfig)
            true
        } catch (e: Exception) {
            when {
                e.isCancellationException() -> Unit
                e is AuthException.SessionExpired -> {
                    outputItemHandler.onOutputItemReceived(sessionExpiredError)
                }
                else -> {
                    LoggerV3.e("Could not start session", e)
                    outputItemHandler.onOutputItemReceived(buildCouldNotStartSessionError(e))
                }
            }
            false
        }
    }

    private suspend fun emitAuthorizationIfNeeded() {
        val accessToken = authClient.getAccessToken()
        if (currentAccessToken == accessToken) return

        try {
            controller.emit("#authorization:${accessToken.token}")
            currentAccessToken = accessToken
        } catch (e: Exception) {
            LoggerV3.e("Could not send #authorization", e)
            e.printStackTrace()
        }
        LoggerV3.d("#authorization sent successfully")
    }

    private suspend fun closeSession() {
        controller.disconnect()
        sessionId = null
    }

    private fun cancelProcessing() {
        controller.unregisterCallback()
    }
}