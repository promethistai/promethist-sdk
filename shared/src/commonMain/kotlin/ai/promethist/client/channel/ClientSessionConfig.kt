package ai.promethist.client.channel

import ai.promethist.Defaults
import ai.promethist.client.Client
import ai.promethist.client.common.ClientConfig
import ai.promethist.client.ClientTarget
import ai.promethist.client.auth.OAuth2Client
import ai.promethist.client.speech.DuplexMode
import ai.promethist.runtime
import ai.promethist.video.VideoMode

class ClientSessionConfig(
    private val config: ClientConfig,
    private val sessionId: () -> String?
) {

    val url: String get() {
        val domain = config.url.ensureProperUrl().replace("http", "ws")
        var url = "${domain}/socket/pipeline/${config.key}"
        sessionId()?.let {
            url += "?sessionId=$it"
        }
        return url
    }

    val applicationInfoUrl: String get() {
        val domain = config.url.ensureProperUrl()
        val url = "${domain}/api/application/${config.key}"
        return url
    }

    val headers: Map<String, String> get() {
        val headers = mutableMapOf<String, String>()

        headers["X-Device-Id"] = config.deviceId
        headers["X-Use-Only-Base64"] = "true"
        headers["Accept-Language"] = config.locale.language
        headers["User-Agent"] = userAgent
        headers["X-Stt-Sample-Rate"] = Defaults.audioFormat.sampleRate.toString()
        if (isDuplexStreamed) { headers["X-Interim-Transcripts"] = "true" }
        outputMedia?.let { headers["X-Output-Media"] = it }

        return headers
    }

    private val outputMedia: String? get() = when (config.videoMode) {
        VideoMode.OnDeviceRendering -> "pcm"
        else -> null
    }

    private val userAgent: String get() = with(runtime) {
        "${ClientTarget.currentTarget.appName}/$version $systemName/$systemVersion"
    }

    private val isDuplexStreamed: Boolean get() {
        return Client.appConfig().duplexEnabled
                && Client.appConfig().duplexMode == DuplexMode.Streamed
    }
}