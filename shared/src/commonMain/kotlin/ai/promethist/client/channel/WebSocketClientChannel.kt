package ai.promethist.client.channel

import ai.promethist.channel.item.AudioEndItem
import ai.promethist.channel.item.EndItem
import ai.promethist.channel.item.ErrorItem
import ai.promethist.client.auth.AuthException
import ai.promethist.client.common.LoggerV3
import ai.promethist.telemetry.SpanContext
import ai.promethist.util.LoggerDelegate
import io.ktor.client.plugins.websocket.DefaultClientWebSocketSession
import io.ktor.client.plugins.websocket.webSocketSession
import io.ktor.client.request.header
import io.ktor.client.request.headers
import io.ktor.utils.io.core.EOFException
import io.ktor.websocket.Frame
import io.ktor.websocket.close
import io.ktor.websocket.readText
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.IO
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.cancelChildren
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch

class WebSocketClientChannel: ClientChannel() {

    private var processScope = CoroutineScope(SupervisorJob())
    private val logger by LoggerDelegate()
    private var _session: DefaultClientWebSocketSession? = null

    override suspend fun process(input: String) {
        val session = getSession() ?: return
        reachabilityMonitor.observeNetworkReachability()
        cancelProcessing()
        processScope.launch(Dispatchers.IO) {
            outputItemParser.clean()
            while (true) {
                try {
                    when (val data = session.incoming.receive()) {
                        is Frame.Text -> {
                            val message = data.readText()
                            reachabilityMonitor.checkProcessConnectionStability()
                            outputItemParser.parse(message)
                            when {
                                message.startsWith("#binary:") -> {
                                    reachabilityMonitor.checkAudioConnectionStability()
                                }
                                message.startsWith("#audio-end") || message.startsWith("#error") -> {
                                    latencyMonitor.clearLastBinaryMessageTimestamp()
                                }
                                message.startsWith("#exit") -> {
                                    val parameters = message.parseParameters(
                                        prefix = "#exit:",
                                        keys = arrayOf("turn.id", "session.id")
                                    )
                                    val sessionId = parameters["session.id"]
                                    this@WebSocketClientChannel.sessionId = sessionId

                                    latencyMonitor.clearLastBinaryMessageTimestamp()
                                    sendDiagnostics(parameters["turn.id"])
                                }
                            }
                        }
                        is Frame.Close -> {
                            LoggerV3.e("Socket close frame received", Exception())
                            outputItemHandler.onOutputItemReceived(socketConnectionError)
                        }
                        else -> Unit
                    }
                } catch (e: Exception) {
                    LoggerV3.e("Socket connection error", e)
                    when {
                        e.isCancellationException() -> {
                            LoggerV3.e("Cancellation exception", e)
                        }
                        e.isKtorTimeout() -> {
                            LoggerV3.e("Timeout exception, restarting session", e)
                            outputItemHandler.clearOutputQueue()
                            process(input)
                        }
                        e is EOFException -> {
                            LoggerV3.e("EOF exception, skipping turn", e)
                            outputItemHandler.onOutputItemReceived(AudioEndItem)
                            outputItemHandler.onOutputItemReceived(EndItem)
                            reset()
                        }
                        //e is ClosedReceiveChannelException
                        else -> {
                            LoggerV3.e("Unhandled stream error", e)
                            outputItemHandler.onOutputItemReceived(socketConnectionError)
                        }
                    }
                    break
                }
            }
        }

        try {
            logger.info("$CHANNEL_LOG_PREFIX Sending input: $input")
            LoggerV3.d("Sending input: $input")
            latencyMonitor.onProcessingStart()
            session.send(Frame.Text(input.ensureProperInputEnding()))
        } catch (e: Exception) {
            LoggerV3.e("Socket sending error", e)
            e.printStackTrace()
            outputItemHandler.onOutputItemReceived(ErrorItem("${e.message}"))
        }
    }

    override suspend fun reset() {
        LoggerV3.network("Reset")
        cancelProcessing()
        closeSession()
    }

    override suspend fun sessionTrace(spanContext: SpanContext) {
        val session = getSession() ?: return

        val traceId = spanContext.getTraceId()
        val spanId = spanContext.getSpanId()
        try {
            logger.info("$CHANNEL_LOG_PREFIX Sending session trace")
            session.send(Frame.Text("#session-trace:traceId=$traceId,spanId=$spanId"))
        } catch (e: Exception) {
            LoggerV3.e("Could not send #session-trace", e)
            e.printStackTrace()
        }
        LoggerV3.d("#session-trace sent successfully")
    }

    private suspend fun getSession(): DefaultClientWebSocketSession? {
        try {
            _session?.takeIf { it.isActive }?.let {
                return it
            }

            if (authClient.isLoggedIn()) {
                authClient.refreshTokens()
            }
            val token = if (authClient.isLoggedIn()) authClient.getAccessToken().token else null

            logger.info("$CHANNEL_LOG_PREFIX Connecting to ws: ${sessionConfig.url}")
            val session = httpClient.webSocketSession(sessionConfig.url) {
                headers {
                    sessionConfig.headers.forEach { (key, value) ->
                        header(key, value)
                    }
                    token?.let {
                        header("Authorization", "Bearer $it")
                    }
                }
            }
            _session = session
            return session
        } catch (e: Exception) {
            when {
                e.isCancellationException() -> Unit
                e is AuthException.SessionExpired || e is AuthException.NotAuthenticated -> {
                    outputItemHandler.onOutputItemReceived(sessionExpiredError)
                }
                else -> {
                    LoggerV3.e("Could not start session", e)
                    outputItemHandler.onOutputItemReceived(buildCouldNotStartSessionError(e))
                }
            }
            return null
        }
    }

    private suspend fun closeSession() {
        _session?.close()
        _session = null
        sessionId = null
    }

    private fun cancelProcessing() {
        processScope.coroutineContext.cancelChildren()
    }
}

const val CHANNEL_LOG_PREFIX = "ClientChannel:"