package ai.promethist.client.channel

import ai.promethist.client.model.ApplicationInfo
import ai.promethist.client.Client
import ai.promethist.client.model.ClientError
import ai.promethist.client.common.Result
import ai.promethist.client.common.Success
import ai.promethist.client.common.LoggerV3
import ai.promethist.client.model.AvatarManifest
import ai.promethist.client.ClientDefaults
import ai.promethist.client.model.TechnicalReport
import ai.promethist.client.common.resultsTo
import ai.promethist.client.model.CommonError
import io.ktor.client.call.body
import io.ktor.client.request.get
import io.ktor.client.request.headers
import io.ktor.client.request.preparePost
import io.ktor.client.request.setBody
import io.ktor.http.HttpStatusCode

suspend fun ClientChannel.getApplicationInfo(): ApplicationInfo {
    applicationInfo?.let {
        return it
    }

    return try {
        val remote: ApplicationInfo = httpClient.get(sessionConfig.applicationInfoUrl).body()
        applicationInfo = remote
        remote
    } catch (e: Exception) {
        LoggerV3.network("Could not get application info $e")
        ApplicationInfo()
    }
}

suspend fun ClientChannel.sendLatency(turnId: String?) {
    if (turnId == null) return

    try {
        val domain = Client.appConfig().url.ensureProperUrl()
        val url = "${domain}/api/log/turn/$turnId"
        val logTurn = LoggerV3.buildLogTurnModel()

        httpClient.preparePost(url) {
            headers {
                append("Content-Type", "application/json")
            }
            setBody(logTurn)
        }.execute {
            if (it.status != HttpStatusCode.OK) {
                LoggerV3.e("Could not send latency to server", CommonError.SendDiagnosticsFailed)
                return@execute
            }
            LoggerV3.d("Sent latency to server: $logTurn")

        }
    } catch (e: Exception) {
        LoggerV3.e("Could not send latency to server", e)
    }
}

suspend fun ClientChannel.sendLogs(sessionId: String?, turnId: String?) {
    if (sessionId == null) return

    try {
        val domain = Client.appConfig().url.ensureProperUrl()
        val url = "${domain}/api/log/session/$sessionId"
        val entries = LoggerV3.getSessionLogEntries(sessionId, turnId)

        httpClient.preparePost(url) {
            headers {
                append("Content-Type", "application/json")
            }
            setBody(entries)
        }.execute {
            if (it.status != HttpStatusCode.OK) {
                LoggerV3.e("Could not send logs to server", CommonError.SendDiagnosticsFailed)
                return@execute
            }
            LoggerV3.clearSessionLogEntries()
        }
    } catch (e: Exception) {
        LoggerV3.e("Could not send logs to server", e)
    }
}

suspend fun ClientChannel.sendTechnicalReport(technicalReport: TechnicalReport) {
    try {
        val domain = Client.appConfig().url.ensureProperUrl()
        val url = "${domain}/api/report"

        httpClient.preparePost(url) {
            headers {
                append("Content-Type", "application/json")
            }
            setBody(technicalReport)
        }.execute {
            if (it.status != HttpStatusCode.OK) {
                LoggerV3.e("Could not send report to server", CommonError.SendDiagnosticsFailed)
                return@execute
            }
            LoggerV3.d("Technical report sent to server for turn: $technicalReport")
        }
    } catch (e: Exception) {
        LoggerV3.e("Could not send report to server", e)
    }
}

suspend fun ClientChannel.syncAvatarManifest() {
    val url = "${ClientDefaults.resourcesBaseUrl}/avatar/index.json"
    val result: Result<AvatarManifest> = try {
        httpClient
            .get(url) {
                headers {
                    append("Content-Type", "application/json")
                }
            }
            .resultsTo(Success)
    } catch (ex: Exception) {
        Result.Error(ClientError.UnknownError(throwable = ex))
    }
    when (result) {
        is Result.Success -> {
            Client.appConfig().avatarManifest = result.data
            LoggerV3.d("Avatar manifest has been updated")
        }
        is Result.Error -> {
            LoggerV3.e("Could not load avatar manifest", Exception(result.error.throwable))
        }
    }
}