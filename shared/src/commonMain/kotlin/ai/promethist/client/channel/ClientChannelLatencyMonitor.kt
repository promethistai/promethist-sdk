package ai.promethist.client.channel

import kotlinx.datetime.Clock

class ClientChannelLatencyMonitor {

    private var lastBinaryMessageTimestamp: Long? = null
    private var lastProcessStartTimestamp: Long? = null

    fun onProcessingStart() {
        lastProcessStartTimestamp = Clock.System.now().toEpochMilliseconds()
    }

    fun onBinaryMessageReceived() {
        lastBinaryMessageTimestamp = Clock.System.now().toEpochMilliseconds()
    }

    fun getCurrentProcessDelay(): Long? {
        val currentTime = Clock.System.now().toEpochMilliseconds()
        val delay = lastProcessStartTimestamp?.let { currentTime - it }
        return delay
    }

    fun getCurrentBinaryMessageDelay(): Long? {
        val currentTime = Clock.System.now().toEpochMilliseconds()
        val delay = lastBinaryMessageTimestamp?.let { currentTime - it }
        return delay
    }

    fun clearLastProcessStartTimestamp() {
        lastProcessStartTimestamp = null
    }

    fun clearLastBinaryMessageTimestamp() {
        lastBinaryMessageTimestamp = null
    }
}