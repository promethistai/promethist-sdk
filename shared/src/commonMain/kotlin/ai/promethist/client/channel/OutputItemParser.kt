package ai.promethist.client.channel

import ai.promethist.channel.command.SessionEnd
import ai.promethist.channel.item.AudioEndItem
import ai.promethist.channel.item.BinaryAudioItem
import ai.promethist.channel.item.EndItem
import ai.promethist.channel.item.ErrorItem
import ai.promethist.channel.item.OutputItem
import ai.promethist.channel.item.PersonaItem
import ai.promethist.channel.item.SessionEndItem
import ai.promethist.channel.item.SpeechItem2
import ai.promethist.channel.item.StreamedTextItem
import ai.promethist.channel.item.TranscriptItem
import ai.promethist.client.Client
import ai.promethist.client.common.LoggerV3
import ai.promethist.client.common.OutputItemHandler
import ai.promethist.client.speech.DuplexMode
import ai.promethist.client.utils.Base64.decodeFromBase64
import ai.promethist.common.sharedSerializer
import ai.promethist.type.newId
import ai.promethist.util.LoggerDelegate

class OutputItemParser(
    private val outputItemHandler: OutputItemHandler
) {
    private val logger by LoggerDelegate()

    private var responseText = ""
    private var uuid = newId()
    private var currentPersona: String? = null

    private val isDuplexStreamed: Boolean get() {
        return Client.appConfig().duplexEnabled
                && Client.appConfig().duplexMode == DuplexMode.Streamed
    }

    fun parse(message: String) {
        when {
            message.startsWith("#binary:") -> {
                val base64 = message.substringAfter("#binary:")
                val byteArray = base64.decodeFromBase64()
                val log = "$LOG_PREFIX Received base64 binary ${byteArray.size}"
                logger.info(log)
                LoggerV3.output(log)
                outputItemHandler.onOutputItemReceived(BinaryAudioItem(byteArray))
            }
            message.startsWith("#speech-item") -> {
                val json = message.substringAfter("#speech-item:")
                val speechItem = sharedSerializer.decodeFromString<SpeechItem2>(json)
                val byteArray = speechItem.bytes.decodeFromBase64()
                val log = "$LOG_PREFIX Received speech-item ${byteArray.size}"
                logger.info(log)
                outputItemHandler.onOutputItemReceived(BinaryAudioItem(byteArray))
                if (speechItem.isLast) {
                    outputItemHandler.onOutputItemReceived(AudioEndItem)
                }
            }
            message.startsWith("#persona:") -> {
                val parameters = message.parseParameters(
                    prefix = "#persona:",
                    keys = arrayOf("name", "avatarRef")
                )
                val personaName = parameters["name"] ?: return
                val avatarRef = parameters["avatarRef"]
                currentPersona = avatarRef
                logger.info("$LOG_PREFIX Received persona: $personaName")
                LoggerV3.output("Received persona: $personaName")
                outputItemHandler.onOutputItemReceived(PersonaItem(
                    id = personaName,
                    name = personaName,
                    avatarId = avatarRef
                ))
            }
            message.startsWith("#output-item:") -> {
                val json = message.substringAfter("#output-item:")
                try {
                    val outputItem = sharedSerializer.decodeFromString<OutputItem>(json)
                    outputItemHandler.onOutputItemReceived(outputItem)
                    logger.info("$LOG_PREFIX Received outputitem: $outputItem")
                } catch (e: Exception) {
                    logger.info("$LOG_PREFIX Error parsing output item: $e")
                    e.printStackTrace()
                }
            }
            message.startsWith("#audio-end") -> {
                logger.info("$LOG_PREFIX Received audio end")
                outputItemHandler.onOutputItemReceived(AudioEndItem)
            }
            message.startsWith("#error") -> {
                val parameters = message.parseParameters(
                    prefix = "#error:",
                    keys = arrayOf("message")
                )
                val errorMessage = parameters["message"] ?: "Cause unknown"
                logger.info("$LOG_PREFIX Received error: $errorMessage")
                LoggerV3.output("Received error: $errorMessage")
                outputItemHandler.onOutputItemReceived(ErrorItem(text = errorMessage))
            }
            message.startsWith("#exit") -> {
                logger.info("$LOG_PREFIX Received full message: $responseText")
                outputItemHandler.onOutputItemReceived(StreamedTextItem(
                    uuid = uuid,
                    text = responseText,
                    isFinal = true,
                    personaId = currentPersona
                ))

                LoggerV3.network("Received full message: $responseText")
                LoggerV3.networkBig("Turn finished")
                logger.info("$LOG_PREFIX Turn finished")
                outputItemHandler.onOutputItemReceived(EndItem)

                if (isDuplexStreamed) {
                    responseText = ""
                } else {
                    return
                }
            }
            message.startsWith("#sessionShouldEnd") -> {
                logger.info("$LOG_PREFIX Received session should end")
                LoggerV3.output("Received session should end")
                outputItemHandler.onOutputItemReceived(SessionEndItem(SessionEnd()))
            }
            message.startsWith("#transcript") -> {
                if (isDuplexStreamed) {
                    val parameters = message.parseParameters(
                        prefix = "#transcript:",
                        keys = arrayOf("text")
                    )
                    val transcript = parameters["text"] ?: return
                    outputItemHandler.onOutputItemReceived(TranscriptItem(text = transcript))
                }
            }
            message.startsWith("#") -> {
                logger.info("$LOG_PREFIX Unsupported message received: $message")
                return
            }
            else -> {
                logger.info("$LOG_PREFIX Received text: $message")
                LoggerV3.output("Received text: $message")
                responseText += message
                if (message.matches(Regex(END_LINE_REGEX))) {
                    responseText += "\n"
                }

                outputItemHandler.onOutputItemReceived(StreamedTextItem(
                    uuid = uuid,
                    text = responseText,
                    isFinal = false,
                    personaId = currentPersona
                ))
            }
        }
    }

    fun clean() {
        responseText = ""
        uuid = newId()
    }

    companion object {
        private const val END_LINE_REGEX = ".*(?<!\\d)[?!.]\$"
        private const val LOG_PREFIX = "ClientChannel:"
    }
}