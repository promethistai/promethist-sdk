package ai.promethist.client.channel

import ai.promethist.ext.isCommand
import io.ktor.utils.io.errors.IOException
import kotlinx.coroutines.CancellationException

fun String.ensureProperInputEnding(): String {
    return if (this.endsWith(".") || this.endsWith("?") || this.endsWith("!")) {
        this
    } else if (this.isCommand) {
        this
    } else {
        "$this."
    }
}

fun String.ensureProperUrl(): String {
    var normalizedUrl = if (!this.contains("http") && !this.contains("ws")) {
        "https://${this}"
    } else {
        this
    }
    if (normalizedUrl.endsWith("/")) {
        normalizedUrl = normalizedUrl.dropLast(1)
    }

    return normalizedUrl
}

fun String.parseParameters(prefix: String, vararg keys: String): Map<String, String?> {
    return this.removePrefix(prefix)
        .split("&")
        .map { it.split("=") }
        .filter { it.size == 2 && keys.contains(it[0]) }
        .associate { it[0] to it[1] }
        .withDefault { null }
}

fun Throwable.isCancellationException(): Boolean {
    return this is CancellationException
}

fun Throwable.isKtorTimeout(): Boolean {
    return (this is IOException && this.message?.contains("timeout") == true)
}