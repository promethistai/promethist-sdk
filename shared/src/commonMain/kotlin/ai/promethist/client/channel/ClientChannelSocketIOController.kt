package ai.promethist.client.channel

interface ClientChannelSocketIOController {
    suspend fun emit(input: String)
    suspend fun connect(config: ClientSessionConfig)
    suspend fun disconnect()

    fun setup()
    fun registerCallback(callback: ClientChannelSocketIOCallback)
    fun unregisterCallback()
}