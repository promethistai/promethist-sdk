package ai.promethist.client.channel

import ai.promethist.client.model.ClientConnectionState
import ai.promethist.client.common.LoggerV3
import ai.promethist.client.ClientLifecycle
import ai.promethist.util.NetworkUtil
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.cancel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class ClientChannelReachabilityMonitor(
    private val lifecycleObserver: ClientLifecycle,
    private val latencyMonitor: ClientChannelLatencyMonitor
) {
    private var reachabilityScope = CoroutineScope(SupervisorJob())

    fun observeNetworkReachability() {
        reachabilityScope.coroutineContext.cancel()
        reachabilityScope = CoroutineScope(SupervisorJob())
        reachabilityScope.launch {
            NetworkUtil.observeHasConnection().collect { hasConnection ->
                if (hasConnection) {
                    lifecycleObserver.onConnectionStateUpdate(ClientConnectionState.Connected)
                } else {
                    lifecycleObserver.onConnectionStateUpdate(ClientConnectionState.Disconnected)
                }
                LoggerV3.network("Connection state updated ($hasConnection)")
            }
        }
    }

    fun checkAudioConnectionStability() {
        val delay = latencyMonitor.getCurrentBinaryMessageDelay()
        if (delay != null) {
            if (delay > AUDIO_DELAY_THRESHOLD_MS) {
                lifecycleObserver.onConnectionStateUpdate(ClientConnectionState.Unstable)
                LoggerV3.network("Slow audio connection detected ($delay ms)")
            }
        }

        latencyMonitor.onBinaryMessageReceived()
    }

    fun checkProcessConnectionStability() {
        val delay = latencyMonitor.getCurrentProcessDelay()
        if (delay != null) {
            if (delay > PROCESS_DELAY_THRESHOLD_MS) {
                reachabilityScope.launch {
                    delay(300)
                    lifecycleObserver.onConnectionStateUpdate(ClientConnectionState.Unstable)
                }
                LoggerV3.network("Slow process connection detected ($delay ms)")
            }
        }

        latencyMonitor.clearLastProcessStartTimestamp()
    }

    companion object {
        private const val AUDIO_DELAY_THRESHOLD_MS = 500L
        private const val PROCESS_DELAY_THRESHOLD_MS = 850L
    }
}

interface ClientReachabilityCallback {
    fun onConnectionStateUpdate(state: ClientConnectionState)
}