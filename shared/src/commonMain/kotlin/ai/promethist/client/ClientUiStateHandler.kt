package ai.promethist.client

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import kotlinx.coroutines.withContext

/**
 * Interface defining a handler for managing the client UI state.
 * Implementations should provide a mechanism to retrieve and update the UI state.
 */
interface ClientUiStateHandler {

    /**
     * Updates the client UI state with the provided [state].
     *
     * @param state The new state to be set.
     */
    suspend fun setState(state: ClientUiState)

    /**
     * Retrieves the current client UI state.
     *
     * @return The current [ClientUiState].
     */
    suspend fun getState(): ClientUiState
}

/**
 * A thread-safe extension function to update the UI state in an atomic manner.
 * It ensures that only one coroutine can modify the state at a time, preventing race conditions.
 *
 * @param transform A function that takes the current state and returns an updated state.
 */
suspend fun ClientUiStateHandler.updateUiState(
    transform: ClientUiState.() -> ClientUiState
) = withContext(Dispatchers.Main) {
    stateMutex.withLock {
        val currentState = this@updateUiState.getState()
        val updatedState = currentState.transform()
        this@updateUiState.setState(updatedState)
    }
}

// Update state Mutex for atomic state updates
private val stateMutex = Mutex()