package ai.promethist.client

import ai.promethist.client.model.ClientConnectionState
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch

interface ClientLifecycle {
    fun onStart()
    fun onStop()
    fun onReset()
    fun onUserInput()
    fun onPlaybackStart()
    fun onStreamedTextProcessed()
    fun onAudioChunkProcessed()
    fun onAvatarTechnicalReportReceived(params: Map<String, String>)
    fun onConnectionStateUpdate(state: ClientConnectionState)
    fun onStartTranscribe()
}

object ClientLifecycleObserver: ClientLifecycle {

    private val emitterScope = CoroutineScope(Job())
    private val _eventFlow = MutableSharedFlow<ClientLifecycleEvent>()

    fun subscribe(scope: CoroutineScope, block: suspend (ClientLifecycleEvent) -> Unit) =
        _eventFlow.onEach(block).launchIn(scope)

    private fun emit(appEvent: ClientLifecycleEvent) = emitterScope.launch {
        _eventFlow.emit(appEvent)
    }

    override fun onStart() {
        emit(ClientLifecycleEvent.OnStart)
    }

    override fun onStop() {
        emit(ClientLifecycleEvent.OnStop)
    }

    override fun onReset() {
        emit(ClientLifecycleEvent.OnReset)
    }

    override fun onUserInput() {
        emit(ClientLifecycleEvent.OnUserInput)
    }

    override fun onPlaybackStart() {
        emit(ClientLifecycleEvent.OnPlaybackStart)
    }

    override fun onStreamedTextProcessed() {
        emit(ClientLifecycleEvent.OnStreamedTextProcessed)
    }

    override fun onAudioChunkProcessed() {
        emit(ClientLifecycleEvent.OnAudioChunkProcessed)
    }

    override fun onAvatarTechnicalReportReceived(params: Map<String, String>) {
        emit(ClientLifecycleEvent.OnAvatarTechnicalReportReceived(params))
    }

    override fun onConnectionStateUpdate(state: ClientConnectionState) {
        emit(ClientLifecycleEvent.OnConnectionStateUpdate(state))
    }

    override fun onStartTranscribe() {
        emit(ClientLifecycleEvent.OnStartTranscribe)
    }
}

sealed interface ClientLifecycleEvent {
    data object OnStart : ClientLifecycleEvent
    data object OnStop : ClientLifecycleEvent
    data object OnReset : ClientLifecycleEvent
    data object OnUserInput : ClientLifecycleEvent
    data object OnPlaybackStart : ClientLifecycleEvent
    data object OnStreamedTextProcessed : ClientLifecycleEvent
    data object OnAudioChunkProcessed : ClientLifecycleEvent
    data class OnAvatarTechnicalReportReceived(val params: Map<String, String>) : ClientLifecycleEvent
    data class OnConnectionStateUpdate(val state: ClientConnectionState) : ClientLifecycleEvent
    data object OnStartTranscribe : ClientLifecycleEvent
}