package ai.promethist.client.utils

import ai.promethist.channel.model.BackgroundOrientation
import ai.promethist.channel.model.ResourceSize
import ai.promethist.client.Client
import ai.promethist.client.ClientDefaults
import ai.promethist.client.model.PersonaId

object RemotePersonaResourceManager {

    private val config by lazy { Client.appConfig() }

    fun getPortrait(personaId: PersonaId, size: ResourceSize): String? {
        val host = ClientDefaults.resourcesBaseUrl
        val avatarManifest = config.avatarManifest

        val avatar = avatarManifest.avatars.firstOrNull {
            it.ref.lowercase() == personaId.name.lowercase()
        } ?: return null
        val revision = avatar.revisions.lastOrNull() ?: return null

        return "$host/avatar/resources/portraits/${avatar.ref}-${revision}-${size.name.lowercase()}.png"
    }

    fun getBackground(orientation: BackgroundOrientation = BackgroundOrientation.Portrait): String? {
        val host = ClientDefaults.resourcesBaseUrl
        val avatarManifest = config.avatarManifest
        val background = avatarManifest.backgrounds.firstOrNull() ?: return null // TODO ???

        return "$host/avatar/resources/backgrounds/${background.ref}-${orientation.raw}.png"
    }
}