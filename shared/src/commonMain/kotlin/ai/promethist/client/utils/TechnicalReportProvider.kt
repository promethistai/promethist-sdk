package ai.promethist.client.utils

import ai.promethist.client.model.TechnicalReport

fun interface TechnicalReportProvider {
    fun technicalReport(parameters: Map<String, String>): TechnicalReport
}