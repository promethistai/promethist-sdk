package ai.promethist.client.utils

import ai.promethist.client.model.ClientConnectionState

interface ClientReachabilityCallback {
    fun onConnectionStateUpdate(state: ClientConnectionState)
}