package ai.promethist.client.utils

object AppLinkParser {

    private const val AppLinkPattern = "(promethist|promethist-tmobile)://([^/]+)/client/([^/]+)"

    fun parse(value: String): AppLink? {
        val pattern = Regex(AppLinkPattern)
        val matchResult = pattern.matchEntire(value) ?: return null

        return AppLink(
            engineDomain = matchResult.groupValues[2],
            setupName = matchResult.groupValues[3]
        )
    }

    data class AppLink(
        val engineDomain: String,
        val setupName: String
    )
}