package ai.promethist.client.utils

import ai.promethist.SharedRes
import ai.promethist.channel.model.ResourceSize
import ai.promethist.client.model.PersonaId
import dev.icerock.moko.resources.ImageResource

object PersonaResourcesManager {

    fun getPortrait(personaId: PersonaId, size: ResourceSize): ImageResource =
        when (personaId) {
            PersonaId.Poppy -> when (size) {
                ResourceSize.M -> SharedRes.images.poppy_portrait_m
                else -> SharedRes.images.poppy_portrait_l
            }
            PersonaId.Seb -> when (size) {
                ResourceSize.M -> SharedRes.images.seb_portrait_m
                else -> SharedRes.images.seb_portrait_l
            }
            PersonaId.Quinn -> when (size) {
                ResourceSize.M -> SharedRes.images.quinn_portrait_m
                else -> SharedRes.images.quinn_portrait_l
            }
            PersonaId.Anthony -> when (size) {
                ResourceSize.M -> SharedRes.images.anthony_portrait_m
                else -> SharedRes.images.anthony_portrait_l
            }
            PersonaId.Jakub -> when (size) {
                ResourceSize.M -> SharedRes.images.jakub_portrait_m
                else -> SharedRes.images.jakub_portrait_l
            }
            PersonaId.Aurora -> when (size) {
                ResourceSize.M -> SharedRes.images.aurora_portrait_m
                else -> SharedRes.images.aurora_portrait_l
            }
            PersonaId.Leo -> when (size) {
                ResourceSize.M -> SharedRes.images.leo_portrait_m
                else -> SharedRes.images.leo_portrait_l
            }
            PersonaId.Hannah -> when (size) {
                ResourceSize.M -> SharedRes.images.hannah_portrait_m
                else -> SharedRes.images.hannah_portrait_l
            }
            PersonaId.Marketa -> when (size) {
                ResourceSize.M -> SharedRes.images.marketa_portrait_m
                else -> SharedRes.images.marketa_portrait_l
            }
            PersonaId.Ezra -> when (size) {
                ResourceSize.M -> SharedRes.images.ezra_portrait_m
                else -> SharedRes.images.ezra_portrait_l
            }
            PersonaId.Teodorik -> when (size) {
                ResourceSize.M -> SharedRes.images.teodorik_portrait_m
                else -> SharedRes.images.teodorik_portrait_l
            }
            PersonaId.Viola -> when (size) {
                ResourceSize.M -> SharedRes.images.viola_portrait_m
                else -> SharedRes.images.viola_portrait_l
            }
            PersonaId.Richard -> when (size) {
                ResourceSize.M -> SharedRes.images.richard_portrait_m
                else -> SharedRes.images.richard_portrait_l
            }
            PersonaId.Bety -> when (size) {
                ResourceSize.M -> SharedRes.images.bety_portrait_m
                else -> SharedRes.images.bety_portrait_l
            }
            PersonaId.TerraAdult -> when (size) {
                ResourceSize.M -> SharedRes.images.terraadult_portrait_m
                else -> SharedRes.images.terraadult_portrait_l
            }
            PersonaId.IgnacAdult -> when (size) {
                ResourceSize.M -> SharedRes.images.ignacadult_portrait_m
                else -> SharedRes.images.ignacadult_portrait_l
            }
            PersonaId.Elvira ->  when (size) {
                ResourceSize.M -> SharedRes.images.viola_portrait_m
                else -> SharedRes.images.elvira_portrait_l
            }
            // TODO correct portraits
            PersonaId.Terra -> when (size) {
                ResourceSize.M -> SharedRes.images.poppy_portrait_m
                else -> SharedRes.images.poppy_portrait_l
            }
            PersonaId.Ignac -> when (size) {
                ResourceSize.M -> SharedRes.images.seb_portrait_m
                else -> SharedRes.images.seb_portrait_l
            }
            PersonaId.Roman -> when (size) {
                ResourceSize.M -> SharedRes.images.jakub_portrait_m
                else -> SharedRes.images.jakub_portrait_l
            }
            PersonaId.Maja -> when (size) {
                ResourceSize.M -> SharedRes.images.quinn_portrait_m
                else -> SharedRes.images.quinn_portrait_l
            }
            PersonaId.Josefina -> when (size) {
                ResourceSize.M -> SharedRes.images.marketa_portrait_m
                else -> SharedRes.images.marketa_portrait_l
            }
            // END TODO
        }

    fun getBackground(personaId: PersonaId): ImageResource =
        when (personaId) {
            PersonaId.Poppy -> SharedRes.images.poppy_background
            PersonaId.Seb -> SharedRes.images.seb_background
            PersonaId.Quinn -> SharedRes.images.quinn_background
            PersonaId.Anthony -> SharedRes.images.anthony_background
            PersonaId.Jakub -> SharedRes.images.jakub_background
            PersonaId.Aurora -> SharedRes.images.aurora_background
            PersonaId.Leo -> SharedRes.images.leo_background
            PersonaId.Hannah -> SharedRes.images.hannah_background
            PersonaId.Marketa -> SharedRes.images.jakub_background
            PersonaId.Ezra -> SharedRes.images.ezra_background
            PersonaId.Teodorik -> SharedRes.images.teodorik_background
            PersonaId.Viola -> SharedRes.images.viola_background
            PersonaId.Richard -> SharedRes.images.richard_background
            PersonaId.Bety -> SharedRes.images.richard_background
            PersonaId.Terra -> SharedRes.images.terra_background
            PersonaId.Ignac -> SharedRes.images.ignac_background
            PersonaId.TerraAdult -> SharedRes.images.terraadult_background
            PersonaId.IgnacAdult -> SharedRes.images.ignacadult_background
            PersonaId.Roman -> SharedRes.images.roman_background
            PersonaId.Elvira -> SharedRes.images.viola_background
            // TODO correct backgrounds
            PersonaId.Maja -> SharedRes.images.terra_background
            PersonaId.Josefina -> SharedRes.images.terra_background
        }
}