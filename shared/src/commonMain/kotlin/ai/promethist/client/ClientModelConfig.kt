package ai.promethist.client

/**
 * A data class representing client configuration.
 *
 *  @property isHomeScreenEnabled Enables showing of home screen with persona selection.
 */
data class ClientModelConfig(
    val isHomeScreenEnabled: Boolean
) {

    companion object {
        val Default = ClientModelConfig(
            isHomeScreenEnabled = false
        )
    }
}
