package ai.promethist.client.model

import kotlinx.serialization.Serializable

@Serializable
enum class Appearance {
    System, Light, Dark
}