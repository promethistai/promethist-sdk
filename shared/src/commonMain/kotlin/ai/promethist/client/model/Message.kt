package ai.promethist.client.model

import ai.promethist.channel.item.VisualItem
import ai.promethist.ext.formatShort
import ai.promethist.ext.now
import ai.promethist.type.ID
import ai.promethist.type.newId
import kotlinx.datetime.LocalDateTime
import kotlin.math.abs

data class Message(
    val uuid: ID,
    val id : Int,
    val isUser: Boolean,
    val personaId: String?,
    val name: String?,
    val text: String,
    val image: String?,
    val showRatingUI: Boolean = false,
    var thumbsUpActive: Boolean = false,
    var thumbsDownActive: Boolean = false,
    val turnId: String = "unknown",
    val nodeId: String? = null,
    var searchedIndexes: MutableList<Int>? = null,
    val created: LocalDateTime = LocalDateTime.now
) {

    constructor(
        uuid: ID = newId(),
        isUser: Boolean = true,
        text: String,
        personaId: String? = "",
        name: String? = "You",
        image: String? = null
    ): this(
        uuid = uuid,
        id = abs((0..99999999).random()),
        isUser = isUser,
        personaId = personaId,
        name = name,
        text = text.replace("\n", ""),
        image = image,
    )

    constructor(
        visualItem: VisualItem
    ): this(
        uuid = newId(),
        id = abs((0..99999999).random()),
        isUser = false,
        personaId = visualItem.personaId,
        name = visualItem.name,
        text = (visualItem.text ?: "").replace("\n", ""),
        image = visualItem.image,
    )

    val createdFormatted: String get() = created.formatShort
}