package ai.promethist.client.model

import ai.promethist.channel.model.ResourceSize
import ai.promethist.client.utils.PersonaResourcesManager
import ai.promethist.client.utils.RemotePersonaResourceManager
import dev.icerock.moko.resources.ImageResource
import kotlinx.serialization.Serializable

@Serializable
data class ApplicationInfo(
    val ref: String = "",
    val name: String = "",
    val languages: List<String> = emptyList(),
    val personas: List<Persona> = emptyList()
) {

    @Serializable
    data class Persona(
        val ref: String = "",
        val name: String = "",
        val description: String = "",
        val avatarRef: String = "",
        val backgroundRef: String = ""
    ) {

        val avatarImageUrl: String? get() =
            RemotePersonaResourceManager.getPortrait(
                personaId = PersonaId.fromAvatarId(avatarRef),
                size = ResourceSize.L
            )

        val avatarImageLocal: ImageResource get() =
            PersonaResourcesManager.getPortrait(
                personaId = PersonaId.fromAvatarId(avatarRef),
                size = ResourceSize.L
            )
    }
}