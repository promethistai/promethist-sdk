package ai.promethist.client.model

import ai.promethist.client.avatar.AvatarQualityLevel
import ai.promethist.time.currentTimeInstant
import ai.promethist.type.Entity
import ai.promethist.type.ID
import ai.promethist.type.IDSerializer
import ai.promethist.type.newId
import ai.promethist.video.VideoMode
import kotlinx.datetime.Instant
import kotlinx.serialization.Serializable

@Serializable
data class TechnicalReport(
    val deviceId: String,
    val memoryUsageBytes: Long,
    val memoryUsagePercent: Double,
    val cpuUsagePercent: Double,
    val micPermission: Boolean,
    val cameraPermission: Boolean,
    val videoMode: VideoMode,
    val avatarQualityLevel: AvatarQualityLevel,
    val arcFps: Float,
    val arcMemory: Float,
    val created: Instant = currentTimeInstant(),
    @Serializable(with = IDSerializer::class)
    override val id: ID = newId()
): Entity
