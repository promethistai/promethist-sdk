package ai.promethist.client.model

import ai.promethist.time.currentTimeInstant
import ai.promethist.util.Log
import kotlinx.datetime.Instant
import kotlinx.serialization.Serializable

@Serializable
data class SessionLogModel(
    val created: Instant,
    val threadName: String = "client",
    val relativeTime: Float,
    val sessionId: String,
    val turnId: String? = null,
    val level: Log.Entry.Level,
    val msg: String
) {
    constructor(
        created: Instant = currentTimeInstant(),
        relativeTime: Float = 0f,
        sessionId: String = "",
        level: Log.Entry.Level = Log.Entry.Level.INFO,
        msg: String
    ) : this(
        created = created,
        threadName = "client",
        relativeTime = relativeTime,
        sessionId = sessionId,
        turnId = null,
        level = level,
        msg = msg
    )
}
