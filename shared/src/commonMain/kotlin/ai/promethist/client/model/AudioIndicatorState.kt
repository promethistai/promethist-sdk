package ai.promethist.client.model

enum class AudioIndicatorState {
    None, Playing, StartSpeaking, Speaking
}