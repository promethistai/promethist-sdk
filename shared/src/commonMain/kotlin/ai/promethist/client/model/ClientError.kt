package ai.promethist.client.model

import ai.promethist.channel.item.ErrorItem
import ai.promethist.client.common.ErrorResult

sealed class ClientError(
    message: String? = null,
    throwable: Throwable? = null
) : ErrorResult(message, throwable = throwable) {

    data class Common(
        val errorItem: ErrorItem,
        val stackTrace: String? = null,
        override val throwable: Throwable? = null
    ) : ClientError(message = stackTrace, throwable = throwable)
    data class ServerOutage(
        val stackTrace: String? = null,
        override val throwable: Throwable? = null
    ) : ClientError(message = stackTrace, throwable = throwable)
    data class RequestCancelled(
        val stackTrace: String? = null,
        override val throwable: Throwable? = null
    ) : ClientError(message = stackTrace, throwable = throwable)
    data class UnknownError(
        val stackTrace: String? = null,
        override val throwable: Throwable? = null
    ) : ClientError(message = stackTrace, throwable = throwable)
}