package ai.promethist.client.model

import kotlinx.serialization.Serializable

@Serializable
data class AvatarManifest(
    val avatars: List<Avatar>,
    val backgrounds: List<Background>
) {

    @Serializable
    data class Avatar(
        val ref: String,
        val revisions: List<String>
    )

    @Serializable
    data class Background(
        val ref: String
    )

    companion object {
        val empty = AvatarManifest(emptyList(), emptyList())
    }
}