package ai.promethist.client.model

import ai.promethist.client.ClientDefaults

enum class PersonaId(val avatarId: String) {
    Poppy("poppy"),
    Seb("seb"),
    Quinn("quinn"),
    Anthony("anthony"),
    Jakub("jakub"),
    Aurora("aurora"),
    Leo("leo"),
    Hannah("hannah"),
    Marketa("marketa"),
    Ezra("ezra"),
    Teodorik("teodorik"),
    Viola("viola"),
    Richard("richard"),
    Bety("bety"),
    Terra("terra"),
    Ignac("ignac"),
    TerraAdult("terraadult"),
    IgnacAdult("ignacadult"),
    Roman("roman"),
    Maja("maja"),
    Josefina("josefina"),
    Elvira("elvira");

    val resourcePrefix: String get() = this.name.lowercase()

    val supportsStyle: Boolean get() = when (this) {
        Seb, Anthony, Quinn -> false
        else -> true
    }

    companion object {
        fun fromAvatarId(name: String?): PersonaId {
            val avatarId = name ?: ClientDefaults.avatarId
            return entries.firstOrNull { avatarId.equals(it.avatarId, ignoreCase = true) } ?:
                   entries.firstOrNull { avatarId.contains(it.avatarId, ignoreCase = true) } ?:
                   Poppy
        }
    }
}