package ai.promethist.client.model

import kotlinx.serialization.Serializable

@Serializable
enum class CameraPermission {
    Granted, Denied, Unknown;

    val text get() = name.lowercase()
}