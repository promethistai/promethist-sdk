package ai.promethist.client.model

enum class ClientConnectionState {
    Connected, Disconnected, Unstable
}