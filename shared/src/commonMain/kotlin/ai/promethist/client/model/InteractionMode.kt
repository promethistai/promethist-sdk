package ai.promethist.client.model

import kotlinx.serialization.Serializable

@Serializable
enum class InteractionMode {
    Voice, Text
}