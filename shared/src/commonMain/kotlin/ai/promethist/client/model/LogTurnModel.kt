package ai.promethist.client.model

import kotlinx.datetime.Instant
import kotlinx.serialization.Serializable

@Serializable
data class LogTurnModel(
    val transcripted: Instant?,
    val firstTokenLatency: Long?,
    val firstSentenceLatency: Long?,
    val firstAudioBlockLatency: Long?,
    val lastAudioBlockLatency: Long?,
    val audioPlaybackStartLatency: Long?
)