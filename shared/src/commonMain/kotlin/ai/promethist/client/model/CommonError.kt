package ai.promethist.client.model

import ai.promethist.client.common.ErrorResult

sealed class CommonError(throwable: Throwable? = null) : ErrorResult(throwable = throwable) {
    class NoNetworkConnection(t: Throwable?) : CommonError(t)
    class Unauthorized(t: Throwable?) : CommonError(t)
    object SendDiagnosticsFailed : Exception()
}
