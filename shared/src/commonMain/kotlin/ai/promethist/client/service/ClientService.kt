package ai.promethist.client.service

import ai.promethist.client.ClientLifecycleEvent
import ai.promethist.client.ClientLifecycleObserver
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.cancelChildren

abstract class ClientService {

    private val serviceScope = CoroutineScope(SupervisorJob())

    open fun start() {
        ClientLifecycleObserver.subscribe(serviceScope) {
            handleLifecycleEvent(it)
        }
    }

    open fun stop() {
        serviceScope.coroutineContext.cancelChildren()
    }

    abstract suspend fun handleLifecycleEvent(lifecycleEvent: ClientLifecycleEvent)
}