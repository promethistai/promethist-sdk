package ai.promethist.client.service

import ai.promethist.client.ClientLifecycleEvent
import ai.promethist.client.ClientUiStateHandler
import ai.promethist.client.updateUiState

class ReachabilityService(
    private val uiStateHandler: ClientUiStateHandler
): ClientService() {

    override suspend fun handleLifecycleEvent(lifecycleEvent: ClientLifecycleEvent) {
        when (lifecycleEvent) {
            is ClientLifecycleEvent.OnConnectionStateUpdate -> {
                uiStateHandler.updateUiState { this.copy(
                    connectionState = lifecycleEvent.state
                ) }
            }
            else -> Unit
        }
    }
}