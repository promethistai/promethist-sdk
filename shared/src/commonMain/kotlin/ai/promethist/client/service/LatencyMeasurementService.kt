package ai.promethist.client.service

import ai.promethist.client.common.LoggerV3
import ai.promethist.client.ClientLifecycleEvent
import ai.promethist.time.currentTime

class LatencyMeasurementService: ClientService() {

    private var reactionTime = currentTime()
    private var isReactionTimeLogged = false
    private var isFirstBinaryItemTimeLogged = false
    private var isFirstTextItemTimeLogged = false
    private var isAudioPlaybackLogged = false

    override fun stop() {
        super.stop()
        clear()
    }

    override suspend fun handleLifecycleEvent(lifecycleEvent: ClientLifecycleEvent) {
        when (lifecycleEvent) {
            ClientLifecycleEvent.OnReset -> {
                LoggerV3.clearSessionLogEntries()
                LoggerV3.resetFirstSessionTranscript()
            }
            ClientLifecycleEvent.OnUserInput -> {
                reactionTime = currentTime()
                isReactionTimeLogged = false
                isFirstBinaryItemTimeLogged = false
                isFirstTextItemTimeLogged = false
                isAudioPlaybackLogged = false
                LoggerV3.reset()
                LoggerV3.setTranscripted()
            }
            ClientLifecycleEvent.OnPlaybackStart -> {
                if (!isAudioPlaybackLogged) {
                    val latencyMs: Long = (currentTime() - reactionTime)
                    LoggerV3.setAudioPlaybackStartLatency(latencyMs)
                    isAudioPlaybackLogged = true
                }
            }
            ClientLifecycleEvent.OnStreamedTextProcessed -> {
                if (!isFirstTextItemTimeLogged) {
                    val latencyMs: Long = (currentTime() - reactionTime)
                    LoggerV3.latency(latencyMs / 1000.0, message = "first text received in:")
                    LoggerV3.setFirstTokenLatency(latencyMs)
                    isFirstTextItemTimeLogged = true
                }
            }
            ClientLifecycleEvent.OnAudioChunkProcessed -> {
                if (!isFirstBinaryItemTimeLogged) {
                    val latencyMs: Long = (currentTime() - reactionTime)
                    LoggerV3.latency(latencyMs / 1000.0, message = "first binary received in:")
                    LoggerV3.setFirstAudioBlockLatency(latencyMs)
                    isFirstBinaryItemTimeLogged = true
                }

                val latencyMs: Long = (currentTime() - reactionTime)
                if (!isReactionTimeLogged) {
                    LoggerV3.latency(latencyMs / 1000.0, "total:")
                    isReactionTimeLogged = true
                }
                LoggerV3.setLastAudioBlockLatency(latencyMs)
            }
            else -> Unit
        }
    }

    private fun clear() {
        isReactionTimeLogged = false
        isFirstBinaryItemTimeLogged = false
        isFirstTextItemTimeLogged = false
        isAudioPlaybackLogged = false
    }
}