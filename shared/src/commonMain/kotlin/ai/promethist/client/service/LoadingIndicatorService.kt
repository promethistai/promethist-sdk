package ai.promethist.client.service

import ai.promethist.client.ClientLifecycleEvent
import ai.promethist.client.ClientUiStateHandler
import ai.promethist.client.setLoading
import ai.promethist.client.updateUiState

class LoadingIndicatorService(
    private val uiStateHandler: ClientUiStateHandler
): ClientService() {

    override suspend fun handleLifecycleEvent(lifecycleEvent: ClientLifecycleEvent) {
        when (lifecycleEvent) {
            ClientLifecycleEvent.OnUserInput -> {
                uiStateHandler.updateUiState { this.setLoading(true) }
            }
            ClientLifecycleEvent.OnPlaybackStart -> {
                uiStateHandler.updateUiState { this.setLoading(false) }
            }
            else -> Unit
        }
    }
}