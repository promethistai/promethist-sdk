package ai.promethist.client.service

import ai.promethist.client.ClientLifecycleEvent
import ai.promethist.client.channel.ClientChannel
import ai.promethist.client.channel.syncAvatarManifest

class SyncAvatarManifestService(
    private val channel: ClientChannel,
): ClientService() {

    override suspend fun handleLifecycleEvent(lifecycleEvent: ClientLifecycleEvent) {
        when (lifecycleEvent) {
            ClientLifecycleEvent.OnStart -> {
                channel.syncAvatarManifest()
            }
            else -> Unit
        }
    }
}