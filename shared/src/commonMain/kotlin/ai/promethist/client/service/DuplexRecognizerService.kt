package ai.promethist.client.service

import ai.promethist.client.Client
import ai.promethist.client.ClientLifecycleEvent
import ai.promethist.client.speech.SpeechRecognizer

class DuplexRecognizerService(
    private val speechRecognizer: SpeechRecognizer,
    private val onSpeechRecognizerResult: (String, Boolean) -> Unit
): ClientService() {

    private val isDuplexEnabled: Boolean by lazy {
        Client.appConfig().duplexEnabled
    }

    override suspend fun handleLifecycleEvent(lifecycleEvent: ClientLifecycleEvent) {
        when (lifecycleEvent) {
            ClientLifecycleEvent.OnUserInput -> {
                if (!isDuplexEnabled) return
                speechRecognizer.setPrivileged(false)
                speechRecognizer.setOnResultCallback(onSpeechRecognizerResult)
            }
            ClientLifecycleEvent.OnStartTranscribe -> {
                speechRecognizer.restartTranscribe()
                speechRecognizer.setPrivileged(true)
            }
            else -> Unit
        }
    }
}