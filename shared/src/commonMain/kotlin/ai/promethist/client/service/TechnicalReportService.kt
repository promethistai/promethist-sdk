package ai.promethist.client.service

import ai.promethist.client.utils.TechnicalReportProvider
import ai.promethist.client.ClientLifecycleEvent
import ai.promethist.client.channel.ClientChannel
import ai.promethist.client.channel.sendTechnicalReport

class TechnicalReportService(
    private val channel: ClientChannel,
    private val technicalReportProvider: TechnicalReportProvider
): ClientService() {

    override suspend fun handleLifecycleEvent(lifecycleEvent: ClientLifecycleEvent) {
        when (lifecycleEvent) {
            is ClientLifecycleEvent.OnAvatarTechnicalReportReceived -> {
                val parameters = lifecycleEvent.params
                channel.sendTechnicalReport(technicalReportProvider.technicalReport(parameters))
            }
            else -> Unit
        }
    }
}