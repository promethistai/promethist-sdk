package ai.promethist.client

/**
 * A sealed interface representing various events that client can emit.
 */
sealed interface ClientEvent {

    /**
     * Send user input to platform.
     *
     * @property input The input provided by the user.
     */
    data class UserInput(val input: String) : ClientEvent

    /**
     * Set a persona.
     *
     * @property ref The new persona ref to set.
     * @property avatarRef The new persona avatarRef to set.
     * @property sendAsInput Should send as input to platform.
     */
    data class SetPersona(val ref: String, val avatarRef: String, val sendAsInput: Boolean) :
        ClientEvent

    /**
     * Reset the conversation.
     *
     * @property sendAsInput Should send #intro input to platform.
     */
    data class Reset(val sendAsInput: Boolean) : ClientEvent

    /**
     * Pause conversation.
     */
    data object Pause : ClientEvent

    /**
     * Resume conversation.
     */
    data object Resume : ClientEvent

    /**
     * Interrupt ongoing turn. Stop audio playback and start speech recognition
     */
    data object BargeIn : ClientEvent

    /**
     * Sync the current user settings across components.
     */
    data object SyncSettings : ClientEvent
}

val ClientEvent.shouldCancelPrevious: Boolean get() = this is ClientEvent.Reset