package ai.promethist.client

import ai.promethist.channel.item.ActionsItem
import ai.promethist.channel.item.ErrorItem
import ai.promethist.channel.item.InputItem
import ai.promethist.channel.item.OutputItem
import ai.promethist.channel.item.PersonaItem
import ai.promethist.channel.item.SessionEndItem
import ai.promethist.channel.item.SpeechItem
import ai.promethist.channel.item.VisualItem
import ai.promethist.client.channel.ClientChannel
import ai.promethist.telemetry.Span
import ai.promethist.telemetry.Telemetry.suspendableTrace

suspend fun withUserInputTrace(channel: ClientChannel, block: suspend (Span) -> Unit) {
    val name = "ClientModel.onUserInput"
    val span = suspendableTrace.startSpan(name)

    try {
        channel.sessionTrace(span.getSpanContext())
        block(span)
    } catch (e: Exception) {
        throw e
    } finally {
        span.end()
    }
}

suspend fun withOutputItemTrace(outputItem: OutputItem, parent: Span?, block: suspend (Span) -> Unit) {
    val name = "ClientModel.process:${outputItem::class.simpleName ?: "UnknownItem"}"
    val span = parent?.let {
        suspendableTrace.startSpan(span = it, name = name)
    } ?: run {
        suspendableTrace.startSpan(name = name)
    }
    span.addOutputItemAttributes(outputItem)

    try {
        block(span)
    } catch (e: Exception) {
        throw e
    } finally {
        span.end()
    }
}

fun Span.addOutputItemAttributes(item: OutputItem) {
    when (item) {
        is ErrorItem -> {
            this.setAttribute("text", item.text)
            this.setAttribute("source", item.source)
        }
        is VisualItem -> {
            this.setAttribute("name", item.name ?: "")
            this.setAttribute("text", item.text ?: "")
            this.setAttribute("image", item.image ?: "")
            this.setAttribute("personaId", item.personaId ?: "")
        }
        is ActionsItem -> {
            this.setAttribute("title", item.content.title)
            this.setAttribute("size", item.content.tiles.size.toLong())
        }
        is SpeechItem -> {
            this.setAttribute("text", item.text)
            this.setAttribute("url", item.url)
            this.setAttribute("style", item.style)
        }
        is InputItem -> {
            this.setAttribute("name", item.name)
            this.setAttribute("textInput", item.textInput)
        }
        is PersonaItem -> {
            this.setAttribute("id", item.id)
            this.setAttribute("name", item.name)
        }
        is SessionEndItem -> {
            this.setAttribute("hasRating", item.content.hasRating)
        }
        else -> {}
    }
}