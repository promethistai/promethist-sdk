package ai.promethist

import ai.promethist.channel.model.Persona
import ai.promethist.model.IVA
import ai.promethist.model.TtsConfig
import ai.promethist.util.Log
import kotlinx.serialization.Serializable
import kotlinx.serialization.Transient

@Serializable
@Deprecated("Will be removed")
data class Response(
    @Transient val logs: List<Log.Entry> = emptyList(),
    val items: List<Item> = emptyList(),
    val sessionEnded: Boolean = false,
    val sleepTimeout: Int = 0,
    val turnId: String? = null,
) {
    @Serializable
    @Deprecated("OutputItems should be used instead")
    data class Item(
        var text: String? = "",
        var ssml: String? = null,
        var confidence: Double = 1.0,
        var image: String? = null,
        var audio: String? = null,
        var video: String? = null,
        var code: String? = null,
        var background: String? = null,
        var repeatable: Boolean = true,
        var persona: Persona? = null,
        var ttsConfig: TtsConfig? = null,
        val turnId: String? = null,
        val dialogueNodeId: String? = null,
        val nodeId: Int? = null,
        val style: String? = null
    )

    fun text() = items.joinToString("\n") { it.text ?: "..." }.trim()

    fun ssml(iva: IVA) = items.joinToString("\n") {
        val ssml = it.ssml ?: if (!(it.text ?: "").startsWith('#')) it.text else ""
        if (!ssml.isNullOrBlank() && it.ttsConfig != null) {
            val ttsConfig = it.ttsConfig!!
            when (iva) {
                IVA.AmazonAlexa -> {
                    when {
                        ttsConfig.amazonAlexa != null ->
                            "<voice name=\"${ttsConfig.amazonAlexa}\">$ssml</voice>"
                        ttsConfig.provider == "Amazon" ->
                            "<voice name=\"${ttsConfig.name}\">$ssml</voice>"
                        else -> ssml
                    }
                }
                IVA.GoogleAssistant ->
                    "<voice gender=\"${ttsConfig.gender.name.lowercase()}\" variant=\"${ttsConfig.googleAssistant ?: 1}\">$ssml</voice>"
            }
        } else ssml ?: ""
    }.trim()
}