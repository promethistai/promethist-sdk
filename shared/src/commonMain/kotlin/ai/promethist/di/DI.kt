package ai.promethist.di

import org.kodein.di.DI
import org.kodein.di.conf.ConfigurableDI

/**
 * DI container
 */
val di: DI by ::configurableDI

/**
 * DSL method for configuring DI container.
 */
fun di(config: DI.MainBuilder.() -> Unit): Unit = configurableDI.addConfig(config)

/**
 * Configurable DIC instance is internal - accessible only from di extension method.
 * Configuration is only possible until the first dependency is resolved.
 */
internal val configurableDI = ConfigurableDI()
