package ai.promethist.channel

import ai.promethist.channel.event.ChannelEvent

fun interface ChannelEventSubscriber {
    fun onEvent(event: ChannelEvent)
}