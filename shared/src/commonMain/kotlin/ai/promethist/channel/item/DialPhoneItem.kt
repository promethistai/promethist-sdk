package ai.promethist.channel.item

import ai.promethist.channel.command.DialPhone
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
@SerialName("DialPhone")
data class DialPhoneItem(
    val content: DialPhone
) : OutputItem(), Modal