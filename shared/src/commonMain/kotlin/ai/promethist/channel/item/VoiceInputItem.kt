package ai.promethist.channel.item

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlin.js.JsExport

@JsExport
@Serializable
@SerialName("VoiceInput")
class VoiceInputItem(
) : OutputItem(), Modal

@JsExport
@Serializable
@SerialName("TextInput")
class TextInputItem(
) : OutputItem(), Modal