package ai.promethist.channel.item

import ai.promethist.channel.command.Actions
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlin.js.JsExport

@Serializable
@SerialName("Actions")
data class ActionsItem(
    val content: Actions
) : OutputItem(), Modal