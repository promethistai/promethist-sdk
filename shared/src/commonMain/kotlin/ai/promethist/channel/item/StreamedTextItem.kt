package ai.promethist.channel.item

import ai.promethist.type.ID

data class StreamedTextItem(
    val uuid: ID,
    val text: String,
    val isFinal: Boolean,
    val personaId: String? = null
) : OutputItem()