package ai.promethist.channel.item

import ai.promethist.channel.command.Beep
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
@SerialName("BeepOn")
data class BeepOnItem(val beep: Beep): OutputItem()
