package ai.promethist.channel.item

data class BinaryAudioItem(
    val bytes: ByteArray
): OutputItem() {

    override fun toString(): String {
        return "BinaryAudioItem(bytes=${bytes.size})"
    }
}