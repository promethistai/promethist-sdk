package ai.promethist.channel.item

import ai.promethist.Hashable
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
@SerialName("Hashable")
sealed class HashableItem : Hashable, OutputItem() {
    abstract override fun hash(): String
}