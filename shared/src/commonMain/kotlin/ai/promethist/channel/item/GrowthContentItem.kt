package ai.promethist.channel.item

import ai.promethist.channel.command.GrowthContent
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
@SerialName("GrowthContent")
data class GrowthContentItem(
    val content: GrowthContent
) : OutputItem(), Modal