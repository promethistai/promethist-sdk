package ai.promethist.channel.item

import ai.promethist.security.Digest
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
@SerialName("Code")
data class CodeItem(val name: String, val code: String) : HashableItem() {
    override fun hash() = Digest.md5(name + code)
}