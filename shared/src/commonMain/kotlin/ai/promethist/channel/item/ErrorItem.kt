package ai.promethist.channel.item

import dev.icerock.moko.resources.desc.ResourceStringDesc
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.Transient

@Serializable
@SerialName("Error")
data class ErrorItem(
    val text : String = "",
    val source: String = "",
    @Transient val localizedText: ResourceStringDesc? = null,
    @Transient val throwable: Throwable? = null
): OutputItem()