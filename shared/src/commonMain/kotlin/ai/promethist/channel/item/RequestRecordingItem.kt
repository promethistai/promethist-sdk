package ai.promethist.channel.item

import ai.promethist.channel.command.RequestRecording
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlin.js.JsExport

@JsExport
@Serializable
@SerialName("RequestRecording")
data class RequestRecordingItem(
    val content: RequestRecording
): Modal, OutputItem()
