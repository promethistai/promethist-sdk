package ai.promethist.channel.item

import ai.promethist.channel.command.ProfileData
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
@SerialName("Profile")
data class ProfileItem(val data: ProfileData): OutputItem()
