package ai.promethist.channel.item

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
@SerialName("CancelNotification")
data class CancelNotificationItem(
    val tag: String? = null
): OutputItem()