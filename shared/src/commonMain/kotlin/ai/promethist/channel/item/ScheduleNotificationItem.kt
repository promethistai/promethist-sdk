package ai.promethist.channel.item

import ai.promethist.channel.command.ScheduledNotification
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
@SerialName("ScheduleNotification")
data class ScheduleNotificationItem(
    val content: ScheduledNotification
): OutputItem()