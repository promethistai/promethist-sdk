package ai.promethist.channel.item

import ai.promethist.channel.command.RestartDirectly
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
@SerialName("RestartDirectly")
data class RestartDirectlyItem(
    val content: RestartDirectly
) : OutputItem(), Modal