package ai.promethist.channel.item

import ai.promethist.channel.command.RequestText
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlin.js.JsExport

@JsExport
@Serializable
@SerialName("RequestText")
data class RequestTextItem(
    val content: RequestText
): Modal, OutputItem()
