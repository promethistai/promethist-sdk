package ai.promethist.channel.item

import ai.promethist.channel.command.SessionEnd
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
@SerialName("SessionEnd")
data class SessionEndItem(
    val content: SessionEnd
) : OutputItem(), Modal