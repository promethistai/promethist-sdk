package ai.promethist.channel.item

import ai.promethist.security.Digest
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlin.js.JsExport

@JsExport
@Serializable
@SerialName("Audio")
data class AudioItem(
    val url: String,
    @Deprecated("use SpeechItem to represent speech")
    val isSpeech: Boolean
) : HashableItem() {
    override fun hash() = Digest.md5(url)
}