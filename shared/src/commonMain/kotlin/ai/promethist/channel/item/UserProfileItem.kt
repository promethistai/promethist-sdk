package ai.promethist.channel.item

import ai.promethist.channel.command.UserProfileData
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
@SerialName("UserProfile")
data class UserProfileItem(
    val content: UserProfileData
) : OutputItem(), Modal