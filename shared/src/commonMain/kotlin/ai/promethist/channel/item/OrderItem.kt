package ai.promethist.channel.item

import ai.promethist.channel.command.OrderSummary
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
@SerialName("Order")
data class OrderSummaryItem(
    val content: OrderSummary
) : OutputItem(), Modal