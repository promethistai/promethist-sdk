package ai.promethist.channel.item

import ai.promethist.channel.command.TutorialData
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
@SerialName("Tutorial")
data class TutorialItem(val data: TutorialData): OutputItem()
