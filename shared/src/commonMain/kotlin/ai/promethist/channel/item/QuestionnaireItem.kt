package ai.promethist.channel.item

import ai.promethist.channel.command.Questionnaire
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
@SerialName("Questionnaire")
data class QuestionnaireItem(
    val content: Questionnaire
) : OutputItem(), Modal