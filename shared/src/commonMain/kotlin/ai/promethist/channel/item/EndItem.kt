package ai.promethist.channel.item

// Sent as the last OutputItem when using streamed mode, not received from server
data object EndItem : OutputItem()
