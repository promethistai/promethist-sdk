package ai.promethist.channel.item

import ai.promethist.security.Digest
import kotlinx.serialization.SerialName
import kotlin.js.JsExport

@JsExport
@SerialName("Image")
data class ImageItem(val url: String) : HashableItem() {
    override fun hash() = Digest.md5(url)
}