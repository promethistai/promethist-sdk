package ai.promethist.channel.item

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
@SerialName("Visual")
data class VisualItem(
    val personaId: String? = null,
    val name: String? = null,
    val text: String?,
    val image: String? = null
) : OutputItem()