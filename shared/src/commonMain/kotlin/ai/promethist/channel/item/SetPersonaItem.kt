package ai.promethist.channel.item

import ai.promethist.channel.command.PersonaSelection
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
@SerialName("SetPersona")
data class SetPersonaItem(
    val content: PersonaSelection
) : OutputItem(), Modal {

    val isSinglePersona: Boolean get() =
        content.personas.count() < 2
}