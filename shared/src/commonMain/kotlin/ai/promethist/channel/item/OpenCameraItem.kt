package ai.promethist.channel.item

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
@SerialName("OpenCamera")
data object OpenCameraItem: OutputItem()