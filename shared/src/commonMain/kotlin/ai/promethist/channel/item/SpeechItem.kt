package ai.promethist.channel.item

import ai.promethist.security.Digest
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlin.js.JsExport

@JsExport
@Serializable
data class SpeechItem(
    val text: String,
    val ssml: String? = null,
    val url: String = "",
    val style: String = ""
) : HashableItem() {
    /**
     * Computes hash primarily used as s file name in a cache.
     * Hash is computed using all [SpeechItem] parameters.
     * URL is included in the hash to reflect different voices and voice setting which is included in the has in the url.
     * As a result, a different environment (default, preview, ...) will also produce a different hash
     */
    override fun hash() = Digest.md5("$text$style${ url.substringBefore("?")} ")
}

@Serializable
@SerialName("Speech2")
data class SpeechItem2(
    val bytes: String,
    val isLast: Boolean,
) : OutputItem()