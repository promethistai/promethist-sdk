package ai.promethist.channel.item

import ai.promethist.channel.model.PersonaDefinition
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
@SerialName("Home")
data class HomeItem(
    val content: List<PersonaDefinition>
) : OutputItem(), Modal