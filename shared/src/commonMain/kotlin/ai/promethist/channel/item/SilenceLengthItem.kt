package ai.promethist.channel.item

import ai.promethist.channel.command.SilenceLength
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
@SerialName("SilenceLength")
data class SilenceLengthItem(val silenceLength: SilenceLength) : OutputItem()