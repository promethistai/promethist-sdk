package ai.promethist.channel.item

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlin.js.JsExport

@JsExport
@Serializable
@SerialName("Camera")
class CameraItem: Modal