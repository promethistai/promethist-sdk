package ai.promethist.channel.item

import ai.promethist.Defaults
import ai.promethist.security.Digest
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlin.js.JsExport

@JsExport
@Serializable
@SerialName("Persona")
data class PersonaItem(
    val id: String,
    val gender: String = "female",
    val name: String,
    val description: String? = null,
    val avatarId: String? = null,
    val background: String? = null,
    val startFrame: String = "A",
    val endFrame: String = "A"
) : HashableItem() {

    override fun hash() = Digest.md5(avatarId ?: name)

    val avatarName =
        (avatarId ?: name).substringBefore('_').substringBefore(' ').lowercase()

    fun getAvatar() =
        avatarId ?: if (Regex(AvatarRegex).matches(id)) Defaults.avatarId else id

    companion object {
        const val AvatarRegex = "[a-f0-9]{24}"
    }
}