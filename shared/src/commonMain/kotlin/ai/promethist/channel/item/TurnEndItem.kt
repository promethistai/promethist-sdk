package ai.promethist.channel.item

import ai.promethist.type.ID
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

// Sent as the last OutputItem when using streamed mode, not received from server
@Serializable
@SerialName("TurnEnd")
data class TurnEndItem(val turnId: String, val sessionEnded: Boolean = false) : OutputItem()