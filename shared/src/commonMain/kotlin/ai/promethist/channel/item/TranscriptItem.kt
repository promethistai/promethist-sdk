package ai.promethist.channel.item

data class TranscriptItem(
    val text: String
): OutputItem()
