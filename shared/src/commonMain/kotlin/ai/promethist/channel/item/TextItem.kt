package ai.promethist.channel.item

import ai.promethist.security.Digest
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
@SerialName("Text")
data class TextItem(val text: String) : HashableItem() {

    override fun hash() = Digest.md5(text)
}