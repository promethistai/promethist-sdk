package ai.promethist.channel.item

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
@SerialName("ToolbarVisibility")
data class ToolbarVisibilityItem(val value: Boolean): OutputItem()