package ai.promethist.channel.item

import ai.promethist.channel.command.UserInput
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
@SerialName("Input")
data class InputItem(
    val name: String,
    val textInput: Boolean,
    var input: UserInput
): OutputItem(), Modal