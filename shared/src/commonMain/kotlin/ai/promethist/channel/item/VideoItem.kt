package ai.promethist.channel.item

import ai.promethist.security.Digest
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
@SerialName("Video")
data class VideoItem(
    val url: String,
    val isSpeech: Boolean,
    val isFirstVideo: Boolean = false
) : HashableItem() {

    override fun hash() = Digest.md5(url)
}