package ai.promethist.channel.item

import ai.promethist.security.Digest
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
@SerialName("Gesture")
data class GestureItem(val name: String, val intensity: Double = 1.0) : HashableItem() {

    override fun hash() = Digest.md5("$name$intensity")
}