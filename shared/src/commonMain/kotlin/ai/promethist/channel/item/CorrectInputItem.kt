package ai.promethist.channel.item

import ai.promethist.channel.command.CorrectInput
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
@SerialName("CorrectInput")
data class CorrectInputItem(
    val correctInput: CorrectInput
): OutputItem(), Modal
