package ai.promethist.channel.item

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
@SerialName("HomeButton")
data class HomeButtonItem(val value: Boolean): OutputItem()
