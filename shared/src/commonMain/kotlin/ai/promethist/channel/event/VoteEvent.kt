package ai.promethist.channel.event

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

/**
 * @var vote -1, 1
 */
@Serializable
@SerialName("Vote")
class VoteEvent(val turnId: String, val nodeId: String? = null, val vote: Int) : ChannelEvent() {
    override fun toString() = "VoteEvent(turnId=$turnId, nodeId=$nodeId, vote=$vote)"
}