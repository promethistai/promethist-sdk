package ai.promethist.channel.event

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
@SerialName("Binary")
data class BinaryEvent(val data: ByteArray, val contentType: String = "application/octet-stream") : ChannelEvent() {

    override fun equals(other: Any?): Boolean {
        if (other === this) return true
        if (other == null || this::class != other::class) return false
        return data.contentEquals((other as BinaryEvent).data)
    }

    override fun hashCode() = data.contentHashCode()

    override fun toString() = "BinaryEvent(${data.size})"
}