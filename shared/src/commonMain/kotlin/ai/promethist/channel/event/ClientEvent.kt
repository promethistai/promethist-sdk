package ai.promethist.channel.event

import ai.promethist.Defaults
import ai.promethist.util.ZoneId
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
@SerialName("Client")
class ClientEvent(
    //val type: String, //conflict with discriminator??
    val category: String,
    val text: String,
    val turnId: String? = null,
    val nodeId: String? = null,
    val applicationSessionId: String? = null,
    val applicationSpentTime: Int? = null,
    val zoneId: ZoneId = Defaults.zoneId,
    val email: String? = null
) : ChannelEvent() {
    override fun toString() = "ClientEvent(category=$category, text=$text, turnId=$turnId, nodeId=$nodeId, applicationSessionId=$applicationSessionId, applicationSpentTime=$applicationSpentTime, zodeId=$zoneId)"
}