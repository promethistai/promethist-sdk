package ai.promethist.channel.event

import ai.promethist.channel.item.OutputItem
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
@SerialName("OutputItem")
data class OutputItemEvent(val outputItem: OutputItem) : ChannelEvent() {
    override fun toString() = "OutputItemEvent(outputItem=$outputItem)"
}