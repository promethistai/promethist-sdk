package ai.promethist.channel.event

import ai.promethist.Input
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
@SerialName("Input")
data class InputEvent(val input: Input, val trace: String? = null) : ChannelEvent() {
    override fun toString() = "InputEvent(input=$input, trace=$trace)"
}