package ai.promethist.channel.event

import kotlinx.serialization.Serializable

@Serializable
sealed class ChannelEvent