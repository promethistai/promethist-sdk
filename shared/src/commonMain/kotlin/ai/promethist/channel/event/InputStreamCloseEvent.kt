package ai.promethist.channel.event

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
@SerialName("InputStreamClose")
class InputStreamCloseEvent : ChannelEvent() {
    override fun toString() = "InputStreamCloseEvent"
}