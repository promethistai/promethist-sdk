package ai.promethist.channel.event

import ai.promethist.Response
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
@SerialName("Response")
data class ResponseEvent(val response: Response) : ChannelEvent() {
    override fun toString() = "ResponseEvent(response=$response)"
}