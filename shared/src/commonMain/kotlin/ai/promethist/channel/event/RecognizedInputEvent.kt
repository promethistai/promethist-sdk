package ai.promethist.channel.event

import ai.promethist.Input
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
@SerialName("RecognizedInput")
data class RecognizedInputEvent(val input: Input, val isFinal: Boolean = true) : ChannelEvent() {
    override fun toString() = "RecognizedInputEvent(input=$input, isFinal=$isFinal)"
}