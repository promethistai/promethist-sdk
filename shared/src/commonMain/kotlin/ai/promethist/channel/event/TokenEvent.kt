package ai.promethist.channel.event

import ai.promethist.type.Token
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
@SerialName("Token")
data class TokenEvent(val token: Token, val trace: String? = null) : ChannelEvent() {
    override fun toString() = "TokenEvent(text=$token, trace=$trace)"
}