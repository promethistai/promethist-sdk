package ai.promethist.channel.event

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
@SerialName("Error")
data class ErrorEvent(val text: String, val source: String = "Unknown") : ChannelEvent() {
    override fun toString() = "ErrorEvent(text=$text, source=$source)"

}