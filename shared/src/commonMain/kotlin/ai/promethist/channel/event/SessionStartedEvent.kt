package ai.promethist.channel.event

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
@SerialName("SessionStarted")
data class SessionStartedEvent(val sessionId: String) : ChannelEvent() {
    override fun toString() = "SessionStartedEvent(sessionId=$sessionId)"
}