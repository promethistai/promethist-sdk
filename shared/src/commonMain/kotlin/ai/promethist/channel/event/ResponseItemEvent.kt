package ai.promethist.channel.event

import ai.promethist.Response
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
@SerialName("ResponseItem")
data class ResponseItemEvent(val responseItem: Response.Item) : ChannelEvent() {
    override fun toString() = "ResponseItemEvent(responseItem = $responseItem)"
}