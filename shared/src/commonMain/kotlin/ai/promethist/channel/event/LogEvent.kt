package ai.promethist.channel.event

import ai.promethist.util.Log
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
@SerialName("Log")
class LogEvent(val entries: List<Log.Entry>, val shouldPersist: Boolean = true) : ChannelEvent() {
    override fun toString() = "LogEvent(${entries.size})"
}