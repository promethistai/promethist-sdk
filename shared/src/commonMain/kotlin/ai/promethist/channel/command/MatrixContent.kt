package ai.promethist.channel.command

data class MatrixContent(
    val matrix: MatrixColumn
) {
    val isColumn: Boolean
        get() {
            if (matrix.rows.isEmpty())
                return false
            return matrix.rows[0].values.size == 1
        }
}

data class MatrixColumn(
    val rows: List<MatrixRow>
)

data class MatrixRow(
    val values: List<MatrixElement>
)

data class MatrixElement(
    val label: String,
    val value: String
)
val emojiMatrix =
    ("\uD83D\uDE20,\uD83D\uDE28,\uD83D\uDE2E,\uD83D\uDE00,\uD83D\uDE01," +
            "\uD83D\uDE16,\uD83D\uDE27,\uD83D\uDE2F,\uD83D\uDE03,\uD83D\uDE06," +
            "\uD83D\uDE2B️,\uD83D\uDE1F,\uD83D\uDE36,\uD83D\uDE32,\uD83D\uDE04," +
            "\uD83D\uDE23️,\uD83D\uDE41,\uD83D\uDE10,\uD83D\uDE42,☺️," +
            "\uD83D\uDE1E️,\uD83D\uDE14,\uD83D\uDE11,\uD83D\uDE0C,\uD83D\uDE0A").split(",")
