package ai.promethist.channel.command

import kotlinx.serialization.Serializable
import kotlin.js.JsExport

@JsExport
@Serializable
data class RequestText(
    val title: String? = null,
    val text: String? = null
)
