package ai.promethist.channel.command

import kotlinx.serialization.Serializable

@Serializable
data class ProfileData(
    val name: String,
    val values: List<ProfileValue>
) {
    @Serializable
    data class ProfileValue(
        val key: String,
        val value: String
    )
    companion object {
        val Empty = ProfileData("", listOf())
    }
}