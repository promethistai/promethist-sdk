package ai.promethist.channel.command

import kotlinx.serialization.Serializable

@Serializable
data class UserProfileData(
    val showGenderSelection: Boolean = true
)
