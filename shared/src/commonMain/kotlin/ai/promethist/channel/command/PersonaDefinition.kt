package ai.promethist.channel.command

import kotlinx.serialization.Serializable

@Serializable
data class PersonaDefinition(
    var name: String,
    var modules: List<PersonaModule>
)