package ai.promethist.channel.command

import kotlinx.serialization.Serializable
import kotlin.js.JsExport

@JsExport
@Serializable
data class Actions(
    val title: String,
    var square: Boolean = false,
    var backAction: String = "",
    var fullScreen: Boolean = false,
    var background: String = "",
    val tiles: List<ActionTile> = emptyList() // JS will probably require to use Array instead of List
)

@JsExport
@Serializable
data class ActionTile(
    val action: String,
    val title: String,
    val text: String? = null,
    val iosTitle: String? = null,
    val iosIcon: String? = null,
    var background: String = "",
    var active: Boolean = true,
    var enabled: Boolean = true,
    val preferredInteractionMode: String? = null
) {

    val actionProcess: String get() = if (action.contains("#")) action else "#$action"
}