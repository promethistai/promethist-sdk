package ai.promethist.channel.command

import kotlinx.serialization.Serializable

@Serializable
data class TutorialData(
    val title: String,
    val returnText: String,
    val topics: List<TopicItem>,
) {

    @Serializable
    data class TopicItem(
        val title: String,
        val text: String,
        val image: String? = null,
    )

    companion object {
        val Empty = TutorialData(title = "", returnText = "", topics = emptyList())
    }
}
