package ai.promethist.channel.command

enum class Attribute {
    ClientType,
    DeviceType,
    FirstInAppSession,
    PrimaryPersonaId;

    val text get() = name.replaceFirstChar(Char::lowercase)
}