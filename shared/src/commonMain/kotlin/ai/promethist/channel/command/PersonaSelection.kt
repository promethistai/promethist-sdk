package ai.promethist.channel.command

import ai.promethist.channel.item.PersonaItem
import ai.promethist.channel.model.PersonaType
import ai.promethist.channel.model.ResourceSize
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.Transient

@Serializable
data class PersonaSelection(
    val title: String?,
    val type: PersonaType = PersonaType.Primary,
    val size: ResourceSize = ResourceSize.L,
    @SerialName("personas") var personasList: List<PersonaItem>? = null
) {

    @Transient var personas: List<PersonaItem> = personasList ?: emptyList()
}