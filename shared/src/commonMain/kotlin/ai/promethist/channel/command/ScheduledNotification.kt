package ai.promethist.channel.command

import kotlinx.serialization.Serializable
import kotlin.time.DurationUnit

@Serializable
data class ScheduledNotification(
    val tag: String,
    val name: String,
    val text: String,
    val image: String? = null,
    val timeDelay: Long? = null,
    val timeUnit: DurationUnit? = null,
    val scheduledTime: String? = null
)