package ai.promethist.channel.command

import kotlinx.serialization.Serializable
import kotlin.js.JsExport

@JsExport
@Serializable
data class RestartDirectly(
    val value: Boolean = false
)