package ai.promethist.channel.command

import kotlinx.serialization.Serializable

@Serializable
data class GrowthContent (
    val title: String,
    var progress: Float? = null,
    var goal: String? = null,
    var text: String? = null,
    val modules: List<GrowthModule>
)

enum class GrowthState {
    New, Done, InProgress, Locked, Unlocked
}

@Serializable
data class GrowthModule(
    val title: String,
    var description: String? = null,
    val progress: Float? = null,
    var state: GrowthState = GrowthState.Locked,
    val imageURL: String? = null,
    var submoduleGroups: List<GrowthSubmoduleGroup> = emptyList(),
    val submodules: List<List<GrowthSubmodule>> = emptyList()
)

@Serializable
data class GrowthSubmoduleGroup(
    var title: String? = null,
    val submodules: List<GrowthSubmodule>
)

@Serializable
data class GrowthSubmodule(
    val title: String,
    val action: String,
    var description: String? = null,
    val imageURL: String? = null,
    val progress: Float? = null,
    var state: GrowthState = GrowthState.Locked
)