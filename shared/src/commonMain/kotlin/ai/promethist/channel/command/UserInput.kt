package ai.promethist.channel.command

import kotlinx.serialization.Serializable

@Serializable
data class UserInput(
    var title: String? = null,
    var text: String? = null,
    val values: List<String>,
    val birthYear: Boolean = false
) {
    fun createMatrixContent() = MatrixContent(
            MatrixColumn(
                values.map {
                    MatrixRow(listOf(MatrixElement(it, it)))
                }
            )
        )
}