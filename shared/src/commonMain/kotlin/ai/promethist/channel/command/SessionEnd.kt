package ai.promethist.channel.command

import kotlinx.serialization.Serializable
import kotlin.js.JsExport

@JsExport
@Serializable
data class SessionEnd(
    val hasRating: Boolean = false,
    val ratingTitle: String? = null
)