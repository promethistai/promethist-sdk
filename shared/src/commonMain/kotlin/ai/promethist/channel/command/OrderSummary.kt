package ai.promethist.channel.command

import kotlinx.serialization.Serializable

@Serializable
data class OrderSummary(
    val items: List<OrderItem>? = null,
    val totalAmount: String? = null,
    val totalAmountWithoutVat: String? = null
)

@Serializable
data class OrderItem(
    val id: String,
    val name: String,
    val price: String? = null,
    val priceWithoutVat: String? = null
)