package ai.promethist.channel.command

import kotlinx.serialization.Serializable

@Serializable
data class PersonaModule(
    val name: String,
    val action: String,
    val mode: String
) {

    val actionProcess: String get() = if (action.contains("#")) action else "#$action"
}