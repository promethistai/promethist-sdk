package ai.promethist.channel.command

import kotlinx.serialization.Serializable

@Serializable
data class Questionnaire(
    val title: String?,
    val texts: List<String>,
    val labels: List<String> = emptyList(),
    val subLabels: List<String> = emptyList()
) {
    fun createMatrixContent(): MatrixContent {
        if (texts.size == 25) {
            val zipped = emojiMatrix.zip(texts)
            val column = mutableListOf<MatrixRow>()
            for (i in 0..4){
                val row = mutableListOf<MatrixElement>()
                for (j in 0..4) {
                    val pair = zipped[i*5 + j]
                    row.add(MatrixElement(pair.first, pair.second))
                }
                column.add(MatrixRow(row))
            }
            return MatrixContent(MatrixColumn(column))
        }
        return MatrixContent(MatrixColumn(listOf(MatrixRow(
            labels.zip(texts)
                .map { MatrixElement(it.first, it.second) }
        ))))
    }
}