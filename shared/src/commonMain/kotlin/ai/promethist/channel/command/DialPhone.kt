package ai.promethist.channel.command

import kotlinx.serialization.Serializable

@Serializable
data class DialPhone(
    val phoneNumber: String
)
