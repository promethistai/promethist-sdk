package ai.promethist.channel.model

import kotlinx.serialization.Serializable

@Serializable
data class PersonaModule(
    val name: String,
    val action: String,
    val mode: String,
) {

    val id: String get() = action

    val actionProcess: String get() = if (action.contains("#")) action else "#$action"
}