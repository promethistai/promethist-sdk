package ai.promethist.channel.model

import ai.promethist.client.ClientDefaults
import ai.promethist.model.TtsConfig
import kotlinx.serialization.Serializable
import kotlinx.serialization.Transient

@Serializable
data class Persona(
    @Transient val id: String = ClientDefaults.primaryPersona().id,
    val name: String,
    val description: String = "",
    val gender: Gender,
    val icon: String?,
    val background: String?,
    val avatarId: String = ClientDefaults.primaryPersona().avatarId,
    val dialogueId: String?,
    val brainId: String? = null,
    val isStrong: Boolean = false,
    val ttsConfig: TtsConfig? = null,
)
