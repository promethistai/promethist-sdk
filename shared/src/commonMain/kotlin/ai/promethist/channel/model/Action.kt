package ai.promethist.channel.model

enum class Action {
    Intro,
    SetPersona,
    Persona,
    Silence,
    Feedback,
    Sos,
    GrowthContent,
    MicGranted,
    MicDenied,
    CameraGranted,
    CameraDenied,
    SpeechProcessingError,
    Undo,
    PicSent,
    PicNotSent;

    val text get() = "#" + name.replaceFirstChar(Char::lowercase)

    companion object {
        fun buildSetPersonaFromAvatarRef(avatarRef: String): String {
            return Persona.text + ":" + avatarRef
        }
    }
}