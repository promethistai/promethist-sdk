package ai.promethist.channel.model

data class ElysaiError(
    val type: String,
    val message: String,
    val isUserFriendly: Boolean = false
)

