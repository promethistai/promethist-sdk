package ai.promethist.channel.model

enum class BackgroundOrientation {
    Portrait, Landscape;

    val raw: String get() = when (this) {
        Portrait -> "p"
        Landscape -> "l"
    }
}