package ai.promethist.channel.model

import kotlinx.serialization.KSerializer
import kotlinx.serialization.Serializable
import kotlinx.serialization.descriptors.PrimitiveKind
import kotlinx.serialization.descriptors.PrimitiveSerialDescriptor
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder

@Serializable(with = ResourceSize.Serializer::class)
enum class ResourceSize {
    M, L, XL;

    object Serializer: KSerializer<ResourceSize> {

        override val descriptor: SerialDescriptor =
            PrimitiveSerialDescriptor("size", PrimitiveKind.STRING)

        override fun deserialize(decoder: Decoder): ResourceSize =
            when(decoder.decodeString()) {
                "M" -> M
                "L" -> L
                "XL" -> XL
                else -> M
            }

        override fun serialize(encoder: Encoder, value: ResourceSize) =
            encoder.encodeString(value.name)
    }
}