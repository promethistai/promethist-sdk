package ai.promethist.channel.model

import kotlinx.serialization.KSerializer
import kotlinx.serialization.Serializable
import kotlinx.serialization.descriptors.PrimitiveKind
import kotlinx.serialization.descriptors.PrimitiveSerialDescriptor
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder

@Serializable(with = PersonaType.Serializer::class)
enum class PersonaType {
    Primary, Secondary, Tertiary;

    val setPersonaAttribute: String get() = "${this.name.lowercase()}${SetPersonaAttributeSuffix}"

    object Serializer: KSerializer<PersonaType> {

        override val descriptor: SerialDescriptor =
            PrimitiveSerialDescriptor("type", PrimitiveKind.STRING)

        override fun deserialize(decoder: Decoder): PersonaType =
            when(decoder.decodeString()) {
                "primary" -> Primary
                "secondary" -> Secondary
                "tertiary" -> Tertiary
                else -> Primary
            }

        override fun serialize(encoder: Encoder, value: PersonaType) =
            encoder.encodeString(value.name.lowercase())
    }

    companion object {
        private const val SetPersonaAttributeSuffix = "PersonaId"
    }
}