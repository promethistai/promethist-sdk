package ai.promethist.channel.model

import kotlinx.serialization.Serializable

@Serializable
data class PersonaDefinition(
    var name: String,
    //var id: String,
    var modules: List<PersonaModule>
)