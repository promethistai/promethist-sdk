package ai.promethist.channel.model

enum class Gender { Male, Female, NonBinary }