package ai.promethist

import kotlinx.serialization.Serializable
import kotlin.js.JsExport

@Serializable
@JsExport
data class Runtime(
    val version: String,
    val buildNumber: String,
    val systemName: String,
    val systemVersion: String,
    val systemArch: String,
    val deviceName: String
) {
    fun text() = "appVersion = $version\nbuildNumber = $buildNumber\nsystemName = $systemName\nsystemVersion = $systemVersion\ndeviceName = $deviceName\n"

    val clientType: String =
        "$systemName:$version:$buildNumber"

    val deviceType: String =
        "$deviceName:$systemName:$systemVersion"
}
