package ai.promethist.time

import kotlinx.datetime.Clock
import kotlinx.datetime.Instant

fun currentTime() = Clock.System.now().toEpochMilliseconds()

fun currentTimeInstant(): Instant = Clock.System.now()