package ai.promethist.telemetry

expect interface SpanContext {

    fun getTraceId(): String
    fun getSpanId(): String
}