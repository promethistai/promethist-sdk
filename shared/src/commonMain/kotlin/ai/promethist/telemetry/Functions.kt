package ai.promethist.telemetry

import io.ktor.util.encodeBase64

fun SpanContext.encodeTrace(): String {
    val traceId = getTraceId()
    val spanId = getSpanId()
    return if (traceId.isNotBlank() && spanId.isNotBlank())
        "$traceId:$spanId".encodeBase64()
    else
        ""
}
