package ai.promethist.telemetry

interface Trace {

    fun interface Block<T> {
        @Throws(Throwable::class)
        fun process(span: Span): T
    }

    val currentSpan: Span

    fun <T> withSpan(
        name: String? = null,
        isRoot: Boolean = false,
        links: Set<Span> = emptySet(),
        block: Block<T>
    ): T

    fun <T> withSpan(
        traceId: String,
        spanId: String,
        name: String = "FROM-REMOTE",
        block: Block<T>
    ): T

    fun <T> withSpan(
        span: Span,
        name: String = "NESTED",
        block: Block<T>
    ): T = with(span.getSpanContext()) {
        withSpan(getTraceId(), getSpanId(), name, block)
    }

    fun startSpan(
        name: String? = null,
        isRoot: Boolean = false,
        links: Set<Span> = emptySet(),
    ): Span
}