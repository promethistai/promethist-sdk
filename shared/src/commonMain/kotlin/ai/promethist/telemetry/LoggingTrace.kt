package ai.promethist.telemetry

import ai.promethist.time.currentTime
import ai.promethist.util.LoggerDelegate

class LoggingTrace(override val currentSpan: Span) : Trace {

    private val logger by LoggerDelegate()

    override fun <T> withSpan(name: String?, isRoot: Boolean, links: Set<Span>, block: Trace.Block<T>): T {
        val time = currentTime()
        return try {
            block.process(currentSpan)
        } finally {
            logger.info("Span (name=$name, isRoot=$isRoot) took ${currentTime() - time} ms")
        }
    }

    override fun <T> withSpan(traceId: String, spanId: String, name: String, block: Trace.Block<T>): T {
        val time = currentTime()
        return try {
            block.process(currentSpan)
        } finally {
            logger.info("Span (traceId=$traceId, spanId=$spanId) took ${currentTime() - time} ms")
        }
    }

    override fun startSpan(name: String?, isRoot: Boolean, links: Set<Span>): Span {
        logger.info("Span (name=$name, isRoot=$isRoot) created")
        return currentSpan
    }
}