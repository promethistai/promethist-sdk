package ai.promethist.telemetry

expect object Telemetry {
    fun register(name: String, endpoint: String = "https://otel-collector.promethist.ai")
    val trace: Trace
    val suspendableTrace: SuspendableTrace
}