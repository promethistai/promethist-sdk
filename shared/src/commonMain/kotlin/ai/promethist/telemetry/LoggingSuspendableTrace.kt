package ai.promethist.telemetry

import ai.promethist.time.currentTime
import ai.promethist.util.LoggerDelegate

class LoggingSuspendableTrace(override val currentSpan: Span) : SuspendableTrace {

    private val logger by LoggerDelegate()

    override suspend fun <T> withSpan(name: String?, isRoot: Boolean, links: Set<Span>, block: SuspendableTrace.Block<T>): T {
        val time = currentTime()
        return try {
            block.process(currentSpan)
        } finally {
            logger.info("Span (name=$name, isRoot=$isRoot) took ${currentTime() - time} ms")
        }
    }

    override suspend fun <T> withSpan(traceId: String, spanId: String, name: String, block: SuspendableTrace.Block<T>): T {
        val time = currentTime()
        return try {
            block.process(currentSpan)
        } finally {
            logger.info("Span (traceId=$traceId, spanId=$spanId) took ${currentTime() - time} ms")
        }
    }

    override suspend fun <T> withSpan(span: Span, name: String, block: SuspendableTrace.Block<T>): T =
        with(span.getSpanContext()) {
            withSpan(getTraceId(), getSpanId(), name, block)
        }

    override fun startSpan(name: String?, isRoot: Boolean, links: Set<Span>): Span {
        logger.info("Span (name=$name, isRoot=$isRoot) created")
        return currentSpan
    }

    override fun startSpan(span: Span, name: String): Span {
        logger.info("Span (span=$span, name=$name) created")
        return currentSpan
    }
}