package ai.promethist.telemetry

interface SuspendableTrace {

    interface Block<T> {
        @Throws(Throwable::class)
        suspend fun process(span: Span): T
    }

    val currentSpan: Span

    suspend fun <T> withSpan(
        name: String? = null,
        isRoot: Boolean = false,
        links: Set<Span> = emptySet(),
        block: Block<T>
    ): T

    suspend fun <T> withSpan(
        traceId: String,
        spanId: String,
        name: String = "FROM-REMOTE",
        block: Block<T>
    ): T

    suspend fun <T> withSpan(
        span: Span,
        name: String = "NESTED",
        block: Block<T>
    ): T

    fun startSpan(
        name: String? = null,
        isRoot: Boolean = false,
        links: Set<Span> = emptySet()
    ): Span

    fun startSpan(
        span: Span,
        name: String = "NESTED",
    ): Span
}