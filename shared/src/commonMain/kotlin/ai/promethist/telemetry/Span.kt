package ai.promethist.telemetry

expect interface Span {

    fun getSpanContext(): SpanContext
    abstract fun end()
    open fun recordException(exception: Throwable): Span
    open fun setAttribute(key: String, value: String): Span
    open fun setAttribute(key: String, value: Long): Span
    open fun setAttribute(key: String, value: Double): Span
    open fun setAttribute(key: String, value: Boolean): Span
}