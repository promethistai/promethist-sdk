package ai.promethist

interface Textual {
    val text: String
}