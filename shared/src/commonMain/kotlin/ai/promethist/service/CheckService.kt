package ai.promethist.service

import ai.promethist.model.Check

interface CheckService {

    fun check(status: String): Check

    companion object : CheckService {

        override fun check(status: String) = Check(status)
    }
}