package ai.promethist

import ai.promethist.util.Locale
import kotlinx.serialization.Serializable

@Serializable
data class Transcript(
    var text: String,
    val confidence: Float = 1.0F,
    val provider: String? = null,
    val locale: Locale? = null,
    var normalizedText: String = text
)