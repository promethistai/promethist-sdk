package ai.promethist.io

import ai.promethist.time.currentTime
import ai.promethist.util.LoggerDelegate
import ai.promethist.util.padLeft

class LoggingOutputStream(
    private val outputStream: OutputStream,
    private val name: String = outputStream.toString(),
    private val logSize: Int = 100000
) : OutputStream() {

    private val logger by LoggerDelegate()
    private val createTime = currentTime()
    private var size = 0

    override fun write(b: Int) {
        outputStream.write(b)
        if (++size % logSize == 0)
            logger.info("+${(currentTime() - createTime).padLeft(5)} WRITING $name $size byte(s)")
    }

    override fun close() {
        outputStream.close()
        logger.info("+${(currentTime() - createTime).padLeft(5)} WRITTEN $name $size byte(s)")
    }
}