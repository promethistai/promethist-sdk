package ai.promethist.io

expect interface Closeable {
    fun close()
}