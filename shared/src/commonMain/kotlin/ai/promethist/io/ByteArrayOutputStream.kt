package ai.promethist.io

class ByteArrayOutputStream(private val blockSize: Int = DEFAULT_BUFFER_SIZE) : OutputStream() {

    var byteArray = ByteArray(blockSize)
        private set
    var size = 0
        private set

    override fun write(b: Int) {
        byteArray[size++] = b.toByte()
        if (size == byteArray.size) {
            ByteArray(byteArray.size + blockSize).let {
                byteArray.copyInto(it)
                byteArray = it
            }
        }
    }

    override fun close() {
        byteArray = byteArray.copyOf(size)
    }

    fun closeAndReturn(): ByteArrayOutputStream {
        close()
        return this;
    }
}
