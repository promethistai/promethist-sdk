package ai.promethist.io

class ByteArrayInputStream(private val buf: ByteArray) : InputStream() {

    private var i = 0

    override fun read() = if (i < buf.size) buf[i++].toInt() and 0xFF else -1

    override fun close() {}
}