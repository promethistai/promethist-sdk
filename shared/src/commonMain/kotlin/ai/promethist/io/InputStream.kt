package ai.promethist.io

expect abstract class InputStream() : Closeable {
    abstract fun read(): Int
    fun read(buf: ByteArray, off: Int, len: Int): Int
}