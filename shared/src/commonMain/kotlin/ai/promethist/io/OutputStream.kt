package ai.promethist.io

expect abstract class OutputStream() : Closeable {
    abstract fun write(b: Int)
    fun write(buf: ByteArray, off: Int, len: Int)
}