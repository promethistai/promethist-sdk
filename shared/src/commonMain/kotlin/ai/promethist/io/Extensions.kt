package ai.promethist.io

fun InputStream.copyTo(vararg outputStream: OutputStream, bufferSize: Int = DEFAULT_BUFFER_SIZE): Int {
    val buffer = ByteArray(bufferSize)
    var count = 0
    var bytesRead: Int
    while (read(buffer, 0, bufferSize).also { bytesRead = it } != -1) {
        for (output in outputStream) {
            output.write(buffer, 0, bytesRead)
            count += bytesRead
        }
    }
    return count
}

inline fun <C : Closeable, R> C.use(block: (C) -> R) =
    try {
        block(this)
    } finally {
        close()
    }