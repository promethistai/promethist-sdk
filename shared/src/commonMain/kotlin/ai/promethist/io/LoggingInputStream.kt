package ai.promethist.io

import ai.promethist.time.currentTime
import ai.promethist.util.LoggerDelegate
import ai.promethist.util.padLeft

class LoggingInputStream(
    private val inputStream: InputStream,
    private val name: String = inputStream.toString(),
    private val logSize: Int = 100000
) : InputStream() {

    private val logger by LoggerDelegate()
    private val createTime = currentTime()
    private var size = 0

    override fun read(): Int {
        val b = inputStream.read()
        if (b == -1)
            logger.info("+${(currentTime() - createTime).padLeft(5)} READ $name $size byte(s)")
        else if (++size % logSize == 0)
            logger.info("+${(currentTime() - createTime).padLeft(5)} READING $name $size byte(s)")
        return b
    }

    override fun close() {
        inputStream.close()
    }
}