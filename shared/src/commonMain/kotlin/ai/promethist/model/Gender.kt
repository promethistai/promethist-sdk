package ai.promethist.model

import kotlinx.serialization.KSerializer
import kotlinx.serialization.Serializable
import kotlinx.serialization.descriptors.PrimitiveKind
import kotlinx.serialization.descriptors.PrimitiveSerialDescriptor
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder

@Serializable(with = Gender.Serializer::class)
enum class Gender {
    Male, Female, NonBinary;

    object Serializer: KSerializer<Gender> {

        override val descriptor: SerialDescriptor =
            PrimitiveSerialDescriptor("locale", PrimitiveKind.STRING)

        override fun deserialize(decoder: Decoder): Gender =
            when(decoder.decodeString()) {
                "Male" -> Male
                "Female" -> Female
                else -> NonBinary
            }

        override fun serialize(encoder: Encoder, value: Gender) =
            encoder.encodeString(when(value) {
                Male -> "Male"
                Female -> "Female"
                NonBinary -> "NonBinary"
            })
    }
}