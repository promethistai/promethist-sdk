package ai.promethist.model

import kotlinx.serialization.Serializable

/**
 * Class is used for resolving Conflicts
 * @param defaultStrategy is [String] value that can obtain two values: 'ours' and 'theirs'
 * @param changes is a list of [ResolveConflict] that do determines specific file changes
 *
 * @see [ResolveConflict]
 */
@Serializable
data class ResolveConflictRequest(
    val defaultStrategy: String,
    val changes: List<ResolveConflict>
) {
    fun getStrategy(path: String): String {
        for (change in changes) {
            if (change.filePath == path) {
                return change.strategy
            }
        }
        return defaultStrategy
    }

    companion object {
        val ALL_OURS = ResolveConflictRequest("ours", emptyList())
        val ALL_THEIRS = ResolveConflictRequest("theirs", emptyList())
    }

    /**
     * Class is used to set strategy for specific paths
     * @param strategy is a strategy specific for file path ("ours","theirs")
     * @param filePath is a specific path to file that resolve strategy should be used.
     *
     */
    @Serializable
    data class ResolveConflict(
        val strategy: String,
        val filePath: String
    )
}
