package ai.promethist.model

import kotlinx.serialization.Serializable

@Serializable
data class PublishChangesRequest(val commitMessage: String)