package ai.promethist.model

import ai.promethist.Hashable
import ai.promethist.security.Digest
import ai.promethist.util.Locale
import kotlinx.serialization.Serializable

@Serializable
data class TtsConfig(
    val provider: String,
    val locale: Locale,
    val gender: Gender,
    val name: String,
    val engine: String? = null,
    val amazonAlexa: String? = null,
    val googleAssistant: String? = null
) : Hashable {
    override fun hash() = Digest.md5(provider + locale.toString() + gender.name + name + engine)

    enum class Voice(val config: TtsConfig) {
        George(TtsConfig("Google", Locale.en_US, Gender.Male, "en-US-Standard-B")),
        Grace(TtsConfig("Google", Locale.en_US, Gender.Female, "en-US-Standard-C")),
        Gordon(TtsConfig("Google", Locale.en_GB, Gender.Male, "en-GB-Wavenet-B")),
        Gwyneth(TtsConfig("Google", Locale.en_GB, Gender.Female, "en-GB-Wavenet-C")),
        Gabriela(TtsConfig("Google", Locale.cs_CZ, Gender.Female, "cs-CZ-Standard-A")),
        Anthony(TtsConfig("Amazon", Locale.en_US, Gender.Male,"Matthew", "neural")),
        Audrey(TtsConfig("Amazon", Locale.en_US, Gender.Female,"Joanna", "neural")),
        Arthur(TtsConfig("Amazon", Locale.en_GB, Gender.Male,"Brian", "neural")),
        Amy(TtsConfig("Amazon", Locale.en_GB, Gender.Female,"Amy", "neural")),
        Michael(TtsConfig("Microsoft", Locale.en_US, Gender.Male, "en-US-GuyNeural")),
        Mary(TtsConfig("Microsoft", Locale.en_US, Gender.Female, "en-US-AriaNeural")),
        Milan(TtsConfig("Microsoft", Locale.cs_CZ, Gender.Male,"cs-CZ-AntoninNeural")),
        Monika(TtsConfig("Microsoft", Locale.cs_CZ, Gender.Female,"cs-CZ-VlastaNeural"));
    }
}
