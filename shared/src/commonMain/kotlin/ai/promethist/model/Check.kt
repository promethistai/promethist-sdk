package ai.promethist.model

import ai.promethist.Runtime
import kotlinx.serialization.Serializable

@Serializable
data class Check(
    val status: String,
    val runtime: Runtime = ai.promethist.runtime,
)