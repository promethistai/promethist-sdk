package ai.promethist.audio

data class AudioFormat(
    val sampleRate: Int = 24000,
    val encoding: AudioEncoding = AudioEncoding.LINEAR16,
    val channels: Int = 1,
    val signed: Boolean = true,
    val bigEndian: Boolean = false
)