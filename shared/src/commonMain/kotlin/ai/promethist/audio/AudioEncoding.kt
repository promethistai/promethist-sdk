package ai.promethist.audio

enum class AudioEncoding(val sampleSizeInBits: Int) {

    LINEAR16(16),
    ALAW(8),
    MULAW(8);

    companion object {
        fun valueOf(sampleSizeInBits: Int) = when (sampleSizeInBits) {
            16 -> LINEAR16
            8 -> MULAW
            else -> error("Unsupported sample size in bits: $sampleSizeInBits")
        }

        fun wavHeader(size: Long, sampleRate: Int, encoding: AudioEncoding, channels: Int = 1, sampleSizeInBits: Int = 16): ByteArray {
            val header = ByteArray(44)
            //val data = get16BitPcm(pcmdata)

            val totalDataLen = (size + 36)
            val bitrate = (sampleRate * channels * sampleSizeInBits).toLong()

            header[0] = 'R'.code.toByte()
            header[1] = 'I'.code.toByte()
            header[2] = 'F'.code.toByte()
            header[3] = 'F'.code.toByte()
            header[4] = (totalDataLen and 0xff).toByte()
            header[5] = (totalDataLen shr 8 and 0xff).toByte()
            header[6] = (totalDataLen shr 16 and 0xff).toByte()
            header[7] = (totalDataLen shr 24 and 0xff).toByte()
            header[8] = 'W'.code.toByte()
            header[9] = 'A'.code.toByte()
            header[10] = 'V'.code.toByte()
            header[11] = 'E'.code.toByte()
            header[12] = 'f'.code.toByte()
            header[13] = 'm'.code.toByte()
            header[14] = 't'.code.toByte()
            header[15] = ' '.code.toByte()
            header[16] = 16
            header[17] = 0
            header[18] = 0
            header[19] = 0
            header[20] = when (encoding) {
                LINEAR16 -> 1
                ALAW -> 6
                MULAW -> 7
            }
            header[21] = 0
            header[22] = channels.toByte()
            header[23] = 0
            header[24] = (sampleRate and 0xff).toByte()
            header[25] = (sampleRate shr 8 and 0xff).toByte()
            header[26] = (sampleRate shr 16 and 0xff).toByte()
            header[27] = (sampleRate shr 24 and 0xff).toByte()
            header[28] = (bitrate / 8 and 0xff).toByte()
            header[29] = (bitrate / 8 shr 8 and 0xff).toByte()
            header[30] = (bitrate / 8 shr 16 and 0xff).toByte()
            header[31] = (bitrate / 8 shr 24 and 0xff).toByte()
            header[32] = (channels * sampleSizeInBits / 8).toByte()
            header[33] = 0
            header[34] = sampleSizeInBits.toByte()
            header[35] = 0
            header[36] = 'd'.code.toByte()
            header[37] = 'a'.code.toByte()
            header[38] = 't'.code.toByte()
            header[39] = 'a'.code.toByte()
            header[40] = (size and 0xff).toByte()
            header[41] = (size shr 8 and 0xff).toByte()
            header[42] = (size shr 16 and 0xff).toByte()
            header[43] = (size shr 24 and 0xff).toByte()

            return header
        }
    }


}