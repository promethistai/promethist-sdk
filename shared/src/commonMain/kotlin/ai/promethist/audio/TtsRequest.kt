package ai.promethist.audio

import ai.promethist.Hashable
import ai.promethist.model.TtsConfig
import ai.promethist.security.Digest
import ai.promethist.type.AnimationDataType
import ai.promethist.util.Locale
import kotlinx.serialization.Serializable

@Serializable
data class TtsRequest(
    val config: TtsConfig,
    var text: String,
    var locale: Locale,
    var isSsml: Boolean = false,
    var style: String = "",
    var sampleRate: Int? = null,
    var speakingRate: Double = 1.0,
    var speakingPitch: Double = 0.0,
    var speakingVolumeGain: Double = 1.0,
    var animationDataType: AnimationDataType = AnimationDataType.None,
) : Hashable {
    override fun hash() = Digest.md5(text + isSsml + config.hash() + (sampleRate ?: "") + speakingRate + speakingPitch + speakingVolumeGain + style + animationDataType.name)
}