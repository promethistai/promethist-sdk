package ai.promethist.audio

import ai.promethist.io.InputStream

interface Synthesizer {

    val name: String
    val fileTypes: List<AudioFileType>

    fun maxSampleRate(fileType: AudioFileType): Int

    fun inputStream(ttsRequest: TtsRequest, fileType: AudioFileType): InputStream
}