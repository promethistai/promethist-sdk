package ai.promethist.audio

import ai.promethist.util.MediaFileType

enum class AudioFileType(override val contentType: String) : MediaFileType {
    a2x("audio/a2x"),
    pcm("audio/pcm"),
    mp3("audio/mpeg"),
    wav("audio/wav"),
    ogg("audio/ogg"),
    alaw("audio/basic"),
    mulaw("audio/basic")
}
