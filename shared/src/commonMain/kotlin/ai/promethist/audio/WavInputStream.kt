package ai.promethist.audio

import ai.promethist.io.InputStream

class WavInputStream(private val pcmInputStream: InputStream, sampleRate: Int, audioEncoding: AudioEncoding, size: Long = 1024 * 1024 * 128) : InputStream() {
    private val header = AudioEncoding.wavHeader(size, sampleRate, audioEncoding)
    private var index = 0
    override fun read() = if (index < header.size) header[index++].toInt() and 0xFF else pcmInputStream.read()
    override fun close() = pcmInputStream.close()
}