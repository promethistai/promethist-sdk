package ai.promethist.audio

import ai.promethist.Defaults
import ai.promethist.util.Locale

data class SttStreamConfig(
    val locale: Locale,
    val sampleRate: Int,
    val encoding: AudioEncoding = Defaults.audioFormat.encoding,
    val mode: SttMode = SttMode.SingleUtterance,
    val model: SttModel = SttModel.General,
    val sttConfig: SttConfig = SttConfig("Google"),
    val interimResults: Boolean = false,
)