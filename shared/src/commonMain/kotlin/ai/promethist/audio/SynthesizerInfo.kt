package ai.promethist.audio

import kotlinx.serialization.Serializable

@Serializable
data class SynthesizerInfo(
    val name: String,
    val formats: List<Format>
) {
    @Serializable
    data class Format(
        val fileType: AudioFileType,
        val maxSampleRate: Int
    )
}