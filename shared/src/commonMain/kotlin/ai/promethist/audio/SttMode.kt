package ai.promethist.audio

enum class SttMode {
    SingleUtterance,
    Continuous
}