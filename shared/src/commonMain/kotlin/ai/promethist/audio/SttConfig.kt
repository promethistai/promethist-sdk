package ai.promethist.audio

import kotlinx.serialization.Contextual
import kotlinx.serialization.Serializable

@Serializable
data class SttConfig(
    val provider: String = "Google",
    val maxSilence: Int = 5, // in seconds between 1..60
    val maxPause: Int = 5, // 1 = min, 5 = max
    val properties: Map<String, @Contextual Any> = mapOf()
) {

    @Deprecated("Use SttMode instead", replaceWith = ReplaceWith("SttMode"))
    enum class Mode { Default, SingleUtterance, Continuous, Duplex }
}