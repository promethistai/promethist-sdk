package ai.promethist.audio

enum class SttModel(val googleName: String) {
    General("default"),
    PhoneCall("phone_call"),
    LatestLong("latest_long"),
    LatestShort("latest_short"),
    Telephony("telephony"),
    TelephonyShort("telephony_short"),
    Video("video"),
    MedicalDictation("medical_dictation"),
    MedicalConversation("medical_conversation"),
    CommandAndSearch("command_and_search")
}