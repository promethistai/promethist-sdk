package ai.promethist

import ai.promethist.audio.AudioFormat
import ai.promethist.util.Locale
import ai.promethist.util.ZoneId

object Defaults {
    val zoneId = ZoneId("Europe/Prague")
    val locale = Locale("en")
    val locales = mapOf(
        "en" to Locale("en", "US"),
        "de" to Locale("de", "DE"),
        "fr" to Locale("fr", "FR"),
        "es" to Locale("es", "ES"),
        "cs" to Locale("cs", "CZ")
    )
    val audioFormat = AudioFormat()
    const val avatarId = "poppy"
    const val primaryPersonaId = "Poppy"
}