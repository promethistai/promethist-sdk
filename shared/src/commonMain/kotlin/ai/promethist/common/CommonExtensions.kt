package ai.promethist.common

import io.ktor.client.HttpClient
import io.ktor.client.plugins.HttpTimeout
import io.ktor.client.plugins.contentnegotiation.ContentNegotiation
import io.ktor.client.plugins.cookies.*
import io.ktor.serialization.kotlinx.json.json
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.json.Json
import kotlinx.serialization.modules.SerializersModule

fun configuredHttpClient(
    httpClient: HttpClient = sharedHttpClient
): HttpClient = httpClient.config {
    followRedirects = false
    expectSuccess = true

    install(ContentNegotiation) {
        json(sharedSerializer)
    }

    install(HttpTimeout) {
        socketTimeoutMillis = 120_000L
        requestTimeoutMillis = 120_000L
        connectTimeoutMillis = 120_000L
    }

    install(HttpCookies)
}

expect val sharedHttpClient: HttpClient

@OptIn(ExperimentalSerializationApi::class)
val sharedSerializer = Json {
    prettyPrint = true
    isLenient = true
    encodeDefaults = true
    explicitNulls = false
    ignoreUnknownKeys = true
    coerceInputValues = true
    useAlternativeNames = false
    serializersModule = SerializersModule {

    }
}

val configuredHttpClient = configuredHttpClient()