package ai.promethist.common

import io.ktor.http.URLProtocol

internal sealed class ServerBuildConfig(
    val baseUrl: String,
    val port: Int,
    val additionalPath: String? = null,
    val urlProtocol: URLProtocol
) {
    class Debug(url: String, additionalPath: String? = null) :
        ServerBuildConfig(
            baseUrl = url,
            port = 443,
            urlProtocol = URLProtocol.HTTPS,
            additionalPath = additionalPath
        )

    class Release(url: String, additionalPath: String? = null) :
        ServerBuildConfig(
            baseUrl = url,
            port = 443,
            urlProtocol = URLProtocol.HTTPS,
            additionalPath = additionalPath
        )
}