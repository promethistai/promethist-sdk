package ai.promethist.common

import io.ktor.client.HttpClient
import io.ktor.client.engine.js.Js

actual val sharedHttpClient: HttpClient = HttpClient(Js) {}