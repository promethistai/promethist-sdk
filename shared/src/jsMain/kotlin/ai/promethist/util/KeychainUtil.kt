package ai.promethist.util

import ai.promethist.client.Client

actual object KeychainUtil {

    private val settings by lazy { Client.appConfig().settings }

    actual fun saveValue(key: String, value: String): Boolean {
        settings.putString(key, value)
        return true
    }

    actual fun getValue(key: String): String? {
        return settings.getString(key, "").takeIf { it.isNotEmpty() }
    }

    actual fun removeValue(key: String) {
        settings.remove(key)
    }
}