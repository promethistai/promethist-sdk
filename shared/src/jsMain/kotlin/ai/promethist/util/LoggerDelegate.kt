package ai.promethist.util

import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.KProperty

actual class LoggerDelegate : ReadOnlyProperty<Any?, Logger> {

    actual override operator fun getValue(thisRef: Any?, property: KProperty<*>): Logger = ConsoleLogger
}