package ai.promethist.util

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

actual object NetworkUtil {

    actual fun observeHasConnection(): Flow<Boolean> {
        return flow {}
    }

    actual fun isConnected(): Boolean {
        return true
    }
}