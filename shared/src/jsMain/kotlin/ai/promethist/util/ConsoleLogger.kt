package ai.promethist.util

object ConsoleLogger : Logger {

    override fun info(msg: String) = console.info(msg)

    override fun warn(msg: String) = console.warn(msg)

    override fun debug(msg: String) = console.log(msg)

    override fun error(msg: String) = console.error(msg)

    override fun error(msg: String, t: Throwable) = console.error(msg, t)
}