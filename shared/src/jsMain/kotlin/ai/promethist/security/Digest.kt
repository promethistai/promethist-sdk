package ai.promethist.security

import kotlinext.js.require

actual object Digest {

    actual fun md5(input: ByteArray): String = TODO()
    actual fun md5(input: String): String = TODO()
    actual fun md5(): String = TODO()
    actual fun sha1(input: ByteArray): String = TODO()
    actual fun sha1(input: String): String = TODO()
    actual fun sha1(): String = TODO()
}