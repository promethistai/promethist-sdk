package ai.promethist

import kotlinx.browser.window

actual val platform = Platform.Js

actual val runtime = Runtime(
    "1.0.0",
    "1",
    window.navigator.appName + "/" + window.navigator.platform,
    window.navigator.appVersion,
    window.navigator.oscpu,
    window.navigator.userAgent
)
