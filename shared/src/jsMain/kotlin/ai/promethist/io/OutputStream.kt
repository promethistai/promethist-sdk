package ai.promethist.io

actual abstract class OutputStream : Closeable {
    actual abstract fun write(b: Int)
    actual fun write(buf: ByteArray, off: Int, len: Int) {
        for (i in off until off + len)
            write(buf[i].toInt() and 0xFF)
    }
}