package ai.promethist.io

actual interface Closeable {
    actual fun close()
}