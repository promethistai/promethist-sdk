package ai.promethist.io

actual class NotFoundException : Exception()