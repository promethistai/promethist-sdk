package ai.promethist.client

import ai.promethist.util.randomString
import kotlinx.browser.localStorage
import org.w3c.dom.Element
import react.create
import react.dom.client.createRoot

@JsExport
fun deviceId(): String {
    val key = "ai.promethist.client.deviceId"
    return localStorage.getItem(key) ?: "D3${randomString()}".also {
        localStorage.setItem(key, it)
    }
}

/**
 * Creates Kotlin/JS implementation of the client view to be exported and inserted into
 * any JS application.
 * TODO bind various OutputItems to specific (React) view components
 */
@JsExport
fun createView(container: Element) {
    createRoot(container).render(PersonaView.create {
        text = "#intro"
    })
}