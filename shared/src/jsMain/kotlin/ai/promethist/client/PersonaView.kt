package ai.promethist.client

import kotlinx.coroutines.DelicateCoroutinesApi
import react.FC
import react.Props
import react.dom.html.InputType
import react.dom.html.ReactHTML.button
import react.dom.html.ReactHTML.div
import react.dom.html.ReactHTML.input
import react.useState

external interface PersonaViewProps : Props {
    var text: String
}

@OptIn(DelicateCoroutinesApi::class)
val PersonaView = FC<PersonaViewProps> { props ->
    var text by useState(props.text)
    div {
        +"You: $text"
    }
    input {
        type = InputType.text
        value = text
        onChange = { event ->
            text = event.target.value
        }
    }
    button {
        +"Send"
        disabled = text.isBlank()
        onClick = {
        }
    }
}