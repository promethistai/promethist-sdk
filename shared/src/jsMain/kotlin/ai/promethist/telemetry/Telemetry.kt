package ai.promethist.telemetry

import ai.promethist.util.LoggerDelegate

actual object Telemetry {

    private val blankSpan = object : Span {
        override fun getSpanContext(): SpanContext = object : SpanContext {
            override fun getTraceId(): String = ""
            override fun getSpanId(): String = ""
        }
        override fun end() {}
    }
    private val logger by LoggerDelegate()

    actual fun register(name: String, endpoint: String) =
        logger.warn("Telemetry registration to endpoint $endpoint not implemented yet")

    actual val trace: Trace = LoggingTrace(blankSpan)

    actual val suspendableTrace: SuspendableTrace = LoggingSuspendableTrace(blankSpan)
}