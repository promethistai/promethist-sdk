package ai.promethist.desktop.cli

import ai.promethist.Defaults
import ai.promethist.channel.item.*
import ai.promethist.client.*
import ai.promethist.client.avatar.AvatarQualityLevel
import ai.promethist.client.common.ClientConfig
import ai.promethist.client.common.OutputItemHandler
import ai.promethist.client.model.AvatarManifest
import ai.promethist.video.VideoMode
import kotlinx.cli.*
import kotlinx.coroutines.*
import java.io.BufferedReader
import java.io.InputStreamReader
import java.net.Inet4Address
import java.net.NetworkInterface

@OptIn(ExperimentalCli::class)
class ClientSubcommand : Subcommand("client", "Run desktop client"), ClientConfig,
    OutputItemHandler {

    override val url by option(ArgType.String, "url", "u", "Engine URL")
        .default("https://engine.promethist.ai")
    override val key by option(ArgType.String, "key", "k", "Application key")
        .default("default")
    override val protocolVersion by option(ArgType.Int, "protocol", "p", "Protocol version")
        .default(3)
    override val deviceId by option(ArgType.String, "device-id", "did", "Device ID")
        .default("desktop:${
            NetworkInterface.getNetworkInterfaces().toList().firstOrNull {
                it.name == "en0" || it.name == "eth0" || it.name == "wlan0"
            }?.inetAddresses?.toList()?.filterIsInstance<Inet4Address>()?.firstOrNull()?.hostAddress ?: "-"}")
    override val primaryPersonaId by option(ArgType.String, "primary-persona-id", "pid", "Primary persona ID")
        .default(Defaults.primaryPersonaId)
    override var videoMode by option(ArgType.Choice<VideoMode>(), "video-mode", "vm", "Video mode")
        .default(VideoMode.None)
    override var avatarQualityLevel: AvatarQualityLevel = AvatarQualityLevel.Medium
    override var sendLogs by option(ArgType.Boolean, "send-logs", "sl", "Send logs")
        .default(false)
    override val locale = Defaults.locale
    override val zoneId = Defaults.zoneId
    private val out by lazy { System.out }
    private val channel = Client.channel()
    override var avatarManifest = AvatarManifest(emptyList(), emptyList())

    init {
        channel.setup(this)
    }

    override val ttsEnabled by option(ArgType.Boolean, "ttsEnabled", "tts", "Tts Enabled")
        .default(true)

    override fun onOutputItemReceived(item: OutputItem) {
        when (item) {
            is PersonaItem -> out.print("${item.name}[${item.avatarId ?: "-"}] ")
            is SpeechItem -> out.println("${item.text}" + (if (item.url.isNotBlank()) " [audio:${item.url}]" else ""))
            is AudioItem -> out.println("[audio:${item.url}]")
            is VideoItem -> out.println("[video:${item.url}]")
            is TextItem -> out.println(item.text)
            is TurnEndItem -> out.println("[turnId:${item.turnId}]")
            is ErrorItem -> out.println("ERROR from ${item.source}: ${item.text}")
            else -> out.println("# $item")
        }
    }

    override fun clearOutputQueue() {
        out.println("[clearOutputQueue]")
    }

    override fun execute() = runBlocking {
        out.println("# locale = $locale")
        out.println("# zoneId = $zoneId")
        out.println("# deviceId = $deviceId")
        out.println("# url = $url")
        out.println("# key = $key")
        out.println("# videoMode = $videoMode")
        out.println("# protocolVersion = $protocolVersion")
        //val channel = Client.channel(this@ClientSubcommand, ClientVersion.entries.first { it.version == protocolVersion })
        channel.process("#intro")
        BufferedReader(InputStreamReader(System.`in`)).use { input ->
            while (true) {
                val text = input.readLine()?.trim() ?: break
                if (text.equals("exit", true))
                    break
                channel.process(text)
            }
        }
    }
}