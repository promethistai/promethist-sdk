package ai.promethist.desktop.cli

import ai.promethist.audio.RawAudioPlayer
import ai.promethist.io.InputStream
import io.ktor.client.HttpClient
import io.ktor.client.engine.okhttp.OkHttp
import io.ktor.client.plugins.websocket.WebSockets
import io.ktor.client.plugins.websocket.webSocket
import io.ktor.http.HttpMethod
import io.ktor.websocket.Frame
import io.ktor.websocket.readText
import kotlinx.cli.ArgType
import kotlinx.cli.ExperimentalCli
import kotlinx.cli.Subcommand
import kotlinx.cli.default
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import java.io.BufferedReader
import java.io.InputStreamReader

@OptIn(ExperimentalCli::class)
class AvatarLinkClientSubcommand: Subcommand("avatar-link-client", "Run avatar link reference client") {

    private val host by option(ArgType.String, "host", description = "AvatarLink service host").default("localhost")
    private val port by option(ArgType.Int, "port", description = "AvatarLink service port").default(9009)
    private val player = RawAudioPlayer()
    private val client = HttpClient(OkHttp) {
        install(WebSockets)
    }

    private fun readLines(input: InputStream = System.`in`): Flow<String> = flow {
        BufferedReader(InputStreamReader(input)).use { reader ->
            while (true) {
                val line = withContext(Dispatchers.IO) {
                    reader.readLine()?.trim()
                } ?: break
                emit(line.trim())
            }
        }
    }

    override fun execute() = runBlocking {
        client.webSocket(HttpMethod.Get, host, port, "/link") {
            println("Connected to AvatarLink at $host:$port")
            launch {
                readLines().collect { line ->
                    send(Frame.Text(line)) // Send input line as a text frame
                }
            }
            for (frame in incoming) {
                when (frame) {
                    is Frame.Text -> {
                        val text = frame.readText()
                        println(text)
                        if (text == "#exit") {
                            player.reset(true)
                            send(Frame.Text(text))
                        }
                    }
                    is Frame.Binary -> {
                        println("AUDIO ${frame.data.size} BYTE(S)")
                        player.add(frame.data)
                    }
                    is Frame.Ping -> send(Frame.Pong(frame.data))
                    else -> println("FRAME $frame")
                }
            }
        }
    }
}