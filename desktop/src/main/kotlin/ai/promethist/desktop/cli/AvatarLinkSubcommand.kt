package ai.promethist.desktop.cli

import ai.promethist.client.Client
import ai.promethist.client.ClientEvent
import ai.promethist.desktop.avatar.DesktopAvatarLink
import ai.promethist.video.VideoMode
import kotlinx.cli.ArgType
import kotlinx.cli.ExperimentalCli
import kotlinx.cli.Subcommand
import kotlinx.cli.default
import kotlinx.coroutines.runBlocking
import java.io.BufferedReader
import java.io.InputStreamReader


@OptIn(ExperimentalCli::class)
class AvatarLinkSubcommand: Subcommand("avatar-link", "Run avatar link") {

    private val url by option(ArgType.String, "url", "u", "Engine URL")
    private val key by option(ArgType.String, "key", "k", "Application key")
    private val sendLogs by option(ArgType.Boolean, "send-logs", "sl", "Send logs")
    private val updateWholeState by option(ArgType.Boolean, "update-whole-state", "uws", "Update whole state").default(false)

    override fun execute() = runBlocking {
        val config = Client.appConfig()
        config.videoMode = VideoMode.OnDeviceRendering
        url?.let { config.engineDomain = it }
        key?.let { config.engineSetupName = it }
        sendLogs?.let { config.sendLogs = it }
        println(config)
        val link = DesktopAvatarLink(updateWholeState = updateWholeState)
        link.start()
        BufferedReader(InputStreamReader(System.`in`)).use { input ->
            while (true) {
                val text = input.readLine()?.trim() ?: break
                if (text.equals("exit", true))
                    break
                link.handleClientEvent(ClientEvent.UserInput(text))
            }
        }
    }
}