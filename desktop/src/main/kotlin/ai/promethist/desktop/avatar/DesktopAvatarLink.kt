package ai.promethist.desktop.avatar

import ai.promethist.channel.item.ErrorItem
import ai.promethist.channel.item.VisualItem
import ai.promethist.client.avatar.CoroutinesFlowUiStateHandler
import ai.promethist.client.avatar.AvatarLink
import ai.promethist.client.avatar.AvatarLinkPlayerController
import ai.promethist.client.avatar.AvatarPlayerController
import ai.promethist.client.avatar.unity.AvatarEventsApiV1Impl
import ai.promethist.client.avatar.unity.AvatarMessage
import ai.promethist.client.avatar.unity.AvatarPlayerImpl
import ai.promethist.client.speech.SpeechRecognizer
import ai.promethist.client.ClientEvent
import ai.promethist.client.ClientModel
import ai.promethist.client.ClientModelConfig
import ai.promethist.client.ClientModelImpl
import ai.promethist.client.ClientUiState
import ai.promethist.client.channel.WebSocketClientChannel
import ai.promethist.desktop.speech.DesktopSpeechRecognizerController
import ai.promethist.util.LoggerDelegate
import io.ktor.server.application.install
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import io.ktor.server.request.ApplicationRequest
import io.ktor.server.routing.routing
import io.ktor.server.websocket.DefaultWebSocketServerSession
import io.ktor.server.websocket.WebSockets
import io.ktor.server.websocket.pingPeriod
import io.ktor.server.websocket.timeout
import io.ktor.server.websocket.webSocket
import io.ktor.websocket.Frame
import io.ktor.websocket.readText
import io.ktor.websocket.send
import kotlinx.coroutines.channels.ClosedReceiveChannelException
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import java.time.Duration
import java.util.Collections

class DesktopAvatarLink(
    private val port: Int = 9009,
    updateWholeState: Boolean = false
) : AvatarLink(updateWholeState) {
    override val clientModel: ClientModel
    override val uiStateHandler: CoroutinesFlowUiStateHandler
    private val playerController: AvatarPlayerController
    private val sessions = Collections.synchronizedSet(mutableSetOf<DefaultWebSocketServerSession>())
    private val mutex = Mutex()
    private val logger by LoggerDelegate()

    init {
        playerController = AvatarLinkPlayerController(this)
        val avatarEventsApi = AvatarEventsApiV1Impl(playerController)
        val avatarPlayer = AvatarPlayerImpl(playerController, avatarEventsApi)
        playerController.bridge = avatarPlayer

        val speechRecognizerController = DesktopSpeechRecognizerController(this)
        val speechRecognizer = SpeechRecognizer(speechRecognizerController)
        speechRecognizerController.setCallback(speechRecognizer)

        uiStateHandler = CoroutinesFlowUiStateHandler()
        clientModel = ClientModelImpl(
            config = ClientModelConfig(isHomeScreenEnabled = false),
            uiStateHandler = uiStateHandler,
            avatarPlayer = avatarPlayer,
            speechRecognizer = speechRecognizer,
            technicalReportProvider = { TODO("Not implemented") },
            channel = WebSocketClientChannel()
        )
    }

    override fun start() {
        super.start()
        embeddedServer(Netty, port = port) {
            install(WebSockets) {
                pingPeriod = Duration.ofSeconds(15)
                timeout = Duration.ofSeconds(30)
                maxFrameSize = Long.MAX_VALUE
                masking = false
            }

            routing {
                webSocket("/link") {
                    addSession(call.request, this)
                    try {
                        for (frame in incoming) {
                            if (frame is Frame.Text) {
                                val text = frame.readText()
                                if (text == "#exit") { // linked client is exiting from turn
                                    playerController.bridge.onMessageReceived(AvatarMessage.TurnEnded.name)
                                } else {
                                    val userInput: ClientEvent.UserInput =
                                        if (text.startsWith('{'))
                                            Json.decodeFromString(text)
                                        else
                                            ClientEvent.UserInput(text)
                                    handleClientEvent(userInput)
                                }
                            }
                        }
                    } catch (e: ClosedReceiveChannelException) {
                        println("Client disconnected")
                    } finally {
                        removeSession(call.request, this)
                    }
                }
            }
        }.start()
    }

    private suspend fun addSession(
        request: ApplicationRequest,
        session: DefaultWebSocketServerSession
    ) = mutex.withLock {
        sessions.add(session)
        logger.info("Session from ${request.local.remoteAddress} added")
    }

    private suspend fun removeSession(
        request: ApplicationRequest,
        session: DefaultWebSocketServerSession
    ) = mutex.withLock {
        sessions.remove(session)
        logger.info("Session from ${request.local.remoteAddress} removed")
    }

    private suspend fun eachSession(block: suspend (DefaultWebSocketServerSession) -> Unit) {
        mutex.withLock {
            sessions.forEach { session ->
                block(session)
            }
        }
    }

    override suspend fun handleStateUpdate(obj: Any) {
        logger.info("Handling state update $obj")
        eachSession {
            it.send("#state:${when (obj) {
                is VisualItem -> Json.encodeToString(obj)
                is ErrorItem -> Json.encodeToString(obj)
                is ClientUiState -> Json.encodeToString(obj)
                else -> "?"
            }}")
        }
    }

    override suspend fun handleAudio(audio: ByteArray) {
        logger.info("Handling audio (${audio.size} bytes)")
        eachSession {
            it.send(audio)
        }
    }

    override suspend fun handleTurnEnd() {
        logger.info("Handling turn end")
        eachSession {
            it.send("#exit")
        }
    }

    override suspend fun handleAvatar(ref: String) {
        logger.info("Handling avatar")
        eachSession {
            it.send("#persona:avatarRef=$ref")
        }
    }
}