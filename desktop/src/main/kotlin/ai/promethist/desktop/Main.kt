package ai.promethist.desktop

import ai.promethist.cli.CheckSubcommand
import ai.promethist.desktop.cli.AvatarLinkClientSubcommand
import ai.promethist.desktop.cli.AvatarLinkSubcommand
import ai.promethist.desktop.cli.ClientSubcommand
import ai.promethist.service.CheckService
import kotlinx.cli.ArgParser
import kotlinx.cli.ExperimentalCli

@OptIn(ExperimentalCli::class)
fun main(args: Array<String>) {

    with(ArgParser("promethist-desktop")) {
        subcommands(
            ClientSubcommand(),
            AvatarLinkSubcommand(),
            AvatarLinkClientSubcommand(),
            CheckSubcommand(CheckService)
        )
        parse(if (args.isEmpty()) arrayOf("-h") else args)
    }
}
