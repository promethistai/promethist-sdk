package ai.promethist.desktop.speech

import ai.promethist.audio.GoogleSpeechRecognizer
import ai.promethist.audio.Microphone
import ai.promethist.client.Client
import ai.promethist.client.avatar.AvatarLink
import ai.promethist.client.avatar.AvatarLinkController
import ai.promethist.client.speech.SpeechRecognizerController
import ai.promethist.client.speech.SpeechRecognizerControllerCallback
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch

class DesktopSpeechRecognizerController(
    override val link: AvatarLink
): SpeechRecognizerController, AvatarLinkController {

    private var callback: SpeechRecognizerControllerCallback? = null
    private val speechRecognizer = GoogleSpeechRecognizer(Microphone())
    private var silenceLength = 5000
    private val config = Client.appConfig()
    private var scope = CoroutineScope(SupervisorJob())

    override suspend fun startTranscribe() = startRecognition()
    override fun stopTranscribe() = stopRecognition()
    override fun pause() = stopRecognition()
    override fun resume() = startRecognition()
    override fun onVisualItemReceived() = stopTranscribe()
    override fun restartTranscribe() = Unit
    override fun setPrivileged(value: Boolean) = Unit

    override fun setCallback(callback: SpeechRecognizerControllerCallback) {
        this.callback = callback
    }

    override fun setSilenceLength(length: Int) {
        silenceLength = length
    }

    private fun startRecognition() {
        scope.coroutineContext.cancel()
        scope = CoroutineScope(SupervisorJob())
        scope.launch {
            speechRecognizer.recognize(
                config.locale.toLanguageTag(),
                silenceLength
            ) { result, isFinal ->
                callback?.onTranscribeResult(result, isFinal)
            }
            scope.coroutineContext.cancel()
        }
    }

    private fun stopRecognition() = scope.coroutineContext.cancel()
}
