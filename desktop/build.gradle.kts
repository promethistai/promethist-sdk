plugins {
    kotlin("jvm")
    id("com.github.johnrengelman.shadow") version "7.0.0"
    application
}

description = "Promethist Desktop"

val ktorVersion: String by project
val kotlinxCliVersion: String by project

dependencies {
    implementation(project(":promethist-shared"))
    implementation("org.jetbrains.kotlinx:kotlinx-cli:$kotlinxCliVersion")
    implementation("io.ktor:ktor-serialization:$ktorVersion")
    implementation("io.ktor:ktor-serialization-kotlinx-json:$ktorVersion")
    implementation("io.ktor:ktor-server-core:$ktorVersion")
    implementation("io.ktor:ktor-server-netty:$ktorVersion")
    implementation("io.ktor:ktor-server-websockets:$ktorVersion")
    implementation("io.ktor:ktor-client-websockets:$ktorVersion")
    implementation("io.ktor:ktor-client-okhttp:$ktorVersion")
    implementation("org.slf4j:slf4j-simple:1.7.32")
}

tasks.getByName<Jar>("shadowJar") {
    isZip64 = true
}

tasks.named<JavaExec>("run") {
    standardInput = System.`in`
}

application {
    mainClass.set("ai.promethist.desktop.MainKt")
}