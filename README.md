
# PromethistSDK Integration Guide

<img src="https://promethist.ai/assets/images/logo-promethist.c59d832e66ecb98325adae41c73704bd.svg" alt="drawing" width="300"/>

## Overview

This document provides step-by-step instructions on how to integrate the PromethistSDK into your Android or iOS application. The PromethistSDK enables various functionalities to enhance your app's capabilities.

## Android Prerequisites

- Android Studio installed
- A native Android project set up
- Jetpack Compose enabled
- Internet access for downloading the SDK

## Android Installation

### 1. Add Maven Repository

Add Maven Repository:

```kotlin
maven {
    url = uri("https://gitlab.com/api/v4/projects/57872399/packages/maven")
}
```
### 2. Add the SDK Dependency

Open your project's `build.gradle` file and add the PromethistSDK dependency:

```kotlin
dependencies {
    implementation("ai.promethist:promethist-android-sdk:x.x.x")
}
```

### 3. Sync Your Project

After adding the dependency, sync your project with Gradle to download the necessary files.

## Android Integration

### 1. Initialize the SDK

Create a new instance of `Promethist` in your Android activity. The `target` parameter should be set according to your configuration.

### 2. Handle Activity Lifecycle Methods

Integrate the PromethistSDK methods into your activity lifecycle methods to ensure proper functionality.

```kotlin
class MyPersonaActivity : ComponentActivity() {

    private val promethist = Promethist(target)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        promethist.onCreate(this)

        setContent {
            PromethistView(viewModel = promethist.viewModel)
        }
    }

    override fun onPause() {
        super.onPause()
        promethist.onPause(this)
    }

    override fun onResume() {
        super.onResume()
        promethist.onResume(this)
    }

    override fun onStop() {
        promethist.onStop(this)
        super.onStop()
    }
}
```

### 3. Add Necessary Permissions to Manifest

Optionally, you can add permissions to Android manifest. PromethistSDK requires:

```xml
<uses-permission android:name="android.permission.INTERNET"/>
<uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
<uses-permission android:name="android.permission.RECORD_AUDIO" />
<uses-permission android:name="android.permission.MODIFY_AUDIO_SETTINGS" />
<uses-permission android:name="android.permission.CAMERA" />
```

## Android Usage

Once integrated, you can use the PromethistSDK's view in your UI components. The example above demonstrates setting up the `PromethistView` with the SDK's view model:

```kotlin
setContent {
    PromethistView(viewModel = promethist.viewModel)
}
```

## iOS Prerequisites

- Xcode installed
- A native iOS app project set up
- SwiftUI enabled
- Internet access for downloading the SDK

## iOS Installation


### 1. Add the SDK Dependency

1. Open your Xcode project.
2. Go to `File` > `Add Packages`.
3. In the search bar, enter the URL of the PromethistSDK repository: `https://gitlab.com/promethistai/PromethistSDK`.
4. Select the appropriate package and add it to your project.

### 2. Update Package Dependencies

After adding the dependency, Xcode will automatically fetch and integrate the necessary files.

## iOS Integration

### 1. Import the SDK

Import the PromethistSDK into your Swift files where you intend to use it.

```swift
import PromethistSDK
```

### 2. Update info.plist

Add necessary permissions to `info.plist` file:

```xml
<key>NSCameraUsageDescription</key>
<string>Insert your description</string>

<key>NSMicrophoneUsageDescription</key>
<string>Insert your description</string>

<key>NSSpeechRecognitionUsageDescription</key>
<string>Insert your description</string>
```


### 2. Initialize the SDK

Create an instance of `Promethist` in your main view controller. The `target` parameter should be set according to your configuration.

```swift
import PromethistSDK

struct  MyPersonaView: View {

	var body: some View {
		PromethistRouter()
	}
}
```

## iOS Usage

Once integrated, you can use the PromethistSDK's view in your UI components. The example above demonstrates setting up the `PromethistRouter`:

```kotlin
var body: some View {
    VStack {
        Text("See my cool persona!")
        PromethistRouter()
    }
}
```

## Conclusion

Following the steps outlined in this guide, you should be able to integrate PromethistSDK persona into your Android or iOS application. For more detailed information and advanced configurations, refer to the official PromethistSDK documentation.
