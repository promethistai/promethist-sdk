pluginManagement {
    repositories {
        google()
        gradlePluginPortal()
        mavenCentral()
    }
    resolutionStrategy {
        eachPlugin {
            when {
                requested.id.id == "kotlinx-serialization" -> useModule("org.jetbrains.kotlin:kotlin-serialization:${requested.version}")
            }
        }
    }
}

plugins {
    id("com.gradle.enterprise") version "3.6.3"
}

dependencyResolutionManagement {
    repositories {
        google()
        mavenLocal()
        mavenCentral()
    }
}

rootProject.name = "promethist"
arrayOf(
    "shared",
    "desktop",
    //"browser",
    "android/app",
    "android/sdk",
).forEach {
    val projectPath = ":${rootProject.name}-${it.replace('/', '-')}"
    include(projectPath)
    project(projectPath).projectDir = file(it)
}

include(":unityLibrary")
project(":unityLibrary").projectDir = File("android/Unity/unityLibrary")