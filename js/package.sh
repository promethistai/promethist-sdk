#!/bin/bash
cd "$(dirname "$0")"

VER="$1"
APP_PATH=./dist/apps/app

if [ -z "$VER" ]; then
    echo "Tag is empty!"
    exit 1
fi

echo "Version: $VER"

# Prerequisites
yarn
npx nx run core:build:production
npx nx run app:build:production

# App
sed -i -e 's+href="+href="client/res/+g' $APP_PATH/index.html
sed -i -e 's+src="+src="client/res/+g' $APP_PATH/index.html
sed -i -e 's+href="client/res//"+href="/"+g' $APP_PATH/index.html

mkdir ./_temp
mv $APP_PATH/* ./_temp
mkdir $APP_PATH/res
mv ./_temp/* $APP_PATH/res
mv $APP_PATH/res/index.html $APP_PATH/index.html
tar -czvf promethist-client-$VER.tar.gz -C $APP_PATH .

# Core
cd ./dist/libs/core
yarn version --new-version $VER --no-git-tag-version
yarn pack
