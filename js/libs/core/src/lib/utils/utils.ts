export const REGEXP_RECOGNIZE_COMMAND = /#(.*?):/;
export const REGEXP_RECOGNIZE_END_OF_LINE = /[#?.!]/;

export const blobToBase64 = (blob: Blob) => {
  const reader = new FileReader();
  reader.readAsDataURL(blob);
  return new Promise((resolve) => {
    reader.onloadend = () => {
      resolve(reader.result);
    };
  });
};

export const arrayBufferToBase64 = (buffer: ArrayBuffer): string => {
  let binary = '';
  const bytes = new Uint8Array(buffer);
  const len = bytes.byteLength;
  for (let i = 0; i < len; i++) {
    binary += String.fromCharCode(bytes[i]);
  }
  return window.btoa(binary);
};
export const int16ToBase64 = (int16s: Int16Array): string => {
  let binary = '';
  const len = int16s.length;

  // Determine if the system is little-endian
  const littleEndian =
    new Uint8Array(new Uint32Array([0x01020304]).buffer)[0] === 0x04;

  for (let i = 0; i < len; i++) {
    const value = int16s[i];
    let byte1, byte2;
    if (littleEndian) {
      byte1 = value & 0xff; // Low byte
      byte2 = (value >> 8) & 0xff; // High byte
    } else {
      byte1 = (value >> 8) & 0xff; // High byte
      byte2 = value & 0xff; // Low byte
    }
    binary += String.fromCharCode(byte1) + String.fromCharCode(byte2);
  }
  return window.btoa(binary);
};

export function mergeUint8Arrays(...arrays: Uint8Array[]): Uint8Array {
  const totalSize = arrays.reduce((acc, e) => acc + e.length, 0);
  const merged = new Uint8Array(totalSize);

  arrays.forEach((array, i, arrays) => {
    const offset = arrays.slice(0, i).reduce((acc, e) => acc + e.length, 0);
    merged.set(array, offset);
  });
  return merged;
}

export const convertPCMToFloat32 = (pcmData: ArrayBuffer): Float32Array => {
  pcmData.byteLength % 2 === 0 ||
    console.error('PCM data must be in 16-bit format');

  const pcm16Array = new Int16Array(pcmData);
  const float32Array = new Float32Array(pcm16Array.length);

  // Convert PCM 16-bit to Float32 (Web Audio API uses Float32)
  for (let i = 0; i < pcm16Array.length; i++) {
    float32Array[i] = pcm16Array[i] / 32768; // Convert to [-1, 1] range
  }

  return float32Array;
};

export function compareArrayBuffers(buf1: ArrayBuffer, buf2: ArrayBuffer) {
  if (buf1.byteLength !== buf2.byteLength) return false;
  const dv1 = new Uint8Array(buf1);
  const dv2 = new Uint8Array(buf2);
  for (let i = 0; i !== buf1.byteLength; i++) {
    if (dv1[i] !== dv2[i]) return false;
  }
  return true;
}
