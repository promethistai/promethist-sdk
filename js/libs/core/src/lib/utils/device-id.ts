const userAgent = window.navigator.userAgent;
const platform = window.navigator.platform;
const randomString =
  Math.random().toString(20).substring(2, 14) +
  Math.random().toString(20).substring(2, 14);

export const deviceId = `${userAgent}-${platform}-${randomString}`;
