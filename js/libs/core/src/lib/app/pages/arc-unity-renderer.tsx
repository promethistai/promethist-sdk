import { useRef } from 'react';
import { Unity } from 'react-unity-webgl';
import { ProgressBar } from '../components';
import { TranscriptOverlay } from '../components/transcript-overlay';
import { useAppStore, useRefStore } from '../contexts';
import { useHandleArcLoading } from '../hooks/use-handle-arc-loading';
import { ChatPageProps } from './types';

export const ArcUnityRenderer: React.FC<ChatPageProps> = ({
  arcRepositoryUrl,
  arcVersion,
  applicationValue,
  style,
}) => {
  const { getIsClientReady, isArcReady } = useAppStore();
  const { arcUnityRef } = useRefStore();
  const divRef = useRef<HTMLDivElement>(null);
  const isClientReady = getIsClientReady();

  const isDefault = applicationValue === 'default';

  useHandleArcLoading(arcRepositoryUrl, arcVersion, isDefault);

  if (!arcUnityRef?.current) return null;

  return (
    <div
      ref={divRef}
      style={{
        position: 'relative',
        display: 'flex',
        justifyContent: 'center',
        overflow: 'hidden',
        height: '100dvh',
        alignSelf: 'center',
        margin: -8,
        ...style,
      }}
    >
      {!isClientReady && <ProgressBar />}
      <Unity
        unityProvider={arcUnityRef.current?.unityProvider}
        style={{
          height: style?.height || '100dvh',
          overflow: 'hidden',
          maxWidth: '736px',
        }}
        disabledCanvasEvents={['scroll', 'scrollend', 'drag', 'dragend']}
      />
      {/* <GreenLight /> */}
      {isArcReady && <TranscriptOverlay />}
    </div>
  );
};
