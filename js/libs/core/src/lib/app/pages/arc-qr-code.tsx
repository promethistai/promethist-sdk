import { QRCode, Typography } from 'antd';

export const ArcQrCode = ({
  qrCode,
  icon,
  color,
  iconSize,
  text = 'Link for mobile app',
}: {
  qrCode: string | null;
  color?: string;
  text?: string;
  icon?: string;
  iconSize?: number;
}) => {
  return (
    <div
      style={{
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column',
      }}
    >
      <QRCode
        status={qrCode ? 'active' : 'loading'}
        value={qrCode || ''}
        size={350}
        bordered={false}
        color={color}
        icon={icon}
        iconSize={iconSize}
      />
      <Typography.Text
        style={{ marginTop: 8 }}
        underline
        copyable={{ text: qrCode || '' }}
      >
        {text}
      </Typography.Text>
    </div>
  );
};
