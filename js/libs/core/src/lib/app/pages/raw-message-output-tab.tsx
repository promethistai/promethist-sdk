import { List } from 'antd';
import dayjs from 'dayjs';
import { DATE_TIME_FORMAT } from '../constants';

export type RawMessageType = {
  text: string;
  date: Date;
};

interface RawMessageOutputTabProps {
  messages: RawMessageType[];
  lastMessage: string;
}

export const RawMessageOutputTab = ({ messages }: RawMessageOutputTabProps) => {
  return (
    <div
      style={{
        height: '100%',
        overflowY: 'scroll',
        overflowX: 'hidden',
        textWrap: 'wrap',
      }}
    >
      <List
        size="small"
        bordered={false}
        dataSource={messages}
        renderItem={(item) => (
          <List.Item style={{ padding: 4 }}>
            <div style={{ padding: 10, borderBottom: 20 }}>
              <div style={{ fontSize: 12, color: '#999' }}>
                {dayjs(item.date).format(DATE_TIME_FORMAT + ':ss.SSS')}
              </div>
              <div
                style={{
                  whiteSpace: 'pre-wrap',
                  wordBreak: 'break-all',
                  overflowWrap: 'anywhere',
                }}
              >
                {item.text}
              </div>
            </div>
          </List.Item>
        )}
      />
    </div>
  );
};
