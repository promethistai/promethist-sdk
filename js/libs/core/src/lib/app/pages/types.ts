import React from 'react';
import { SendMessage } from '../hooks/types';
import { RawMessageType } from './raw-message-output-tab';

export type ArcSourceAssetType = {
  item: { name: string; ref: string; revision: number };
};

export enum ArcObject {
  AudioManager = 'AudioManager',
  PersonaManager = 'PersonaManager',
  ClientApi = 'ClientApi',
}

export enum ArcEvent {
  MessageReceived = 'onMessageReceived',
  SetPersona = 'setPersona',
  SendCommand = 'SendCommand',
}

export enum OutgoingArcMessage {
  PlayAudio = 'PlayAudio',
  PauseAudio = 'PausePlayback',
  ResumeAudio = 'ResumePlayback',
  StopAudio = 'StopPlayback',
  ExitTurn = 'ExitTurn',
  AudioEnd = 'AudioEnd',
  QueueSpeech = 'QueueSpeech',
}

export enum IncomingArcMessage {
  AudioEnded = 'AudioTrackEnded',
  AudioStarted = 'AudioTrackStarted',
  SceneLoaded = 'sceneLoaded',
  AvatarLoaded = 'avatarLoaded',
  DownloadEnded = 'downloadEnded',
  AssetsFetchRequired = 'assetsFetchRequired',
  AssetsFetchFinished = 'assetsFetchFinished',
  AssetsFetchFailed = 'assetsFetchFailed',
  DownloadProgress = 'downloadProgress',
  ReceiveBinaryData = 'receiveBinaryData',
  TurnEnded = 'TurnEnded',
}

export type ChatPageProps = {
  sendMessage: SendMessage;
  messages: RawMessageType[];
  lastMessage: string;
  analyzerData: {
    dataArray: Uint8Array;
    bufferLength: number;
    analyzer: AnalyserNode | null;
  };
  applicationValue: string;
  arcRepositoryUrl: string;
  arcVersion: string;
  style?: React.CSSProperties;
  arcStyle?: React.CSSProperties;
};

export type EngineProps = {
  engineUrl: string;
  applicationValue: string;
  sttSampleRate?: number;
  outputMedia?: string;
  interimTranscripts?: boolean;
  locale?: string;
};

export type ArcTabsProps = {
  qrCode: string | null;
  onCloseRef: React.MutableRefObject<() => void>;
  onOpenRef: React.MutableRefObject<() => void>;
  engineProps: EngineProps;
};
