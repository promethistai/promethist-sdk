import { useEffect, useState } from 'react';
import { ensureCorrectEndingOfUserInput } from '../../api/protocol-logic';
import { UseEngineConnectionFactory } from '../../api/use-engine-connection-factory';
import { MessagesList } from '../components';
import { ChatInput } from '../components/chat-input';
import { GreenLight } from '../components/green-light';
import { useAppStore } from '../contexts';
import { useScrollableRef } from '../hooks/use-scrollable-ref';
import { RawMessageType } from './raw-message-output-tab';

type ChatPageProps = Pick<
  UseEngineConnectionFactory,
  'sendMessage' | 'messages' | 'lastMessage'
> & {
  analyzerData: {
    dataArray: Uint8Array;
    bufferLength: number;
    analyzer: AnalyserNode | null;
  };
  wrapperStyle?: React.CSSProperties;
};

export const ChatComponent = ({
  sendMessage,
  messages,
  lastMessage,
  wrapperStyle,
}: ChatPageProps) => {
  const [text, setText] = useState<string>('');
  const { getIsMicrophone, persona, setIsChatReady } = useAppStore();

  const isMicrophone = getIsMicrophone();
  useEffect(() => {
    setIsChatReady(true);
    return () => {
      setIsChatReady(false);
    };
  }, []);

  const messagesContainerRef = useScrollableRef(messages.length);

  const _messages: RawMessageType[] = [
    ...messages,
    { date: new Date(), text: lastMessage },
  ];

  return (
    <div style={{ height: '98vh', ...wrapperStyle }}>
      <div
        style={{
          display: 'flex',
          flexDirection: 'column',
          justifyContent: 'space-between',
          height: '100%',
        }}
      >
        <div
          style={{ overflowY: 'scroll', height: '100%' }}
          ref={messagesContainerRef}
        >
          <MessagesList
            searchValue={''}
            messages={_messages}
            applicationName={persona?.name ?? 'Unnamed Persona'}
          />
        </div>
        {!isMicrophone ? (
          <ChatInput
            value={text}
            onChange={(value) => setText(value)}
            onSend={() => {
              sendMessage(ensureCorrectEndingOfUserInput(text));
              setText('');
            }}
          />
        ) : (
          <GreenLight />
        )}
      </div>
    </div>
  );
};
