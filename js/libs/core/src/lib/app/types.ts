export type PromethistMessage = {
  text: string;
  position: 'left' | 'right';
};
