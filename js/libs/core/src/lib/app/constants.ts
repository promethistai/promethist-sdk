import { v4 as uuidv4 } from 'uuid';

export const DATE_TIME_FORMAT = 'DD. MM. YYYY  HH:mm';
export enum LOCALE {
  en = 'en-US',
  cs = 'cs-CZ',
  de = 'de-DE',
  es = 'es-ES',
}

export const LOCALE_TRANSLATION: Record<LOCALE, string> = {
  [LOCALE.en]: 'English',
  [LOCALE.cs]: 'Czech',
  [LOCALE.de]: 'German',
  [LOCALE.es]: 'Spanish',
};

export const localeEnum = Object.entries(LOCALE_TRANSLATION).map(
  ([value, label]) => ({
    value,
    label,
  })
);

export const ensureLongLocale = (_locale: string) => {
  return (
    Object.entries(LOCALE).find(
      ([key, it]) => _locale === key || it === _locale
    )?.[1] || LOCALE.en
  );
};

export const DEFAULT_CLIENT_MODES = {
  chat: 'chat',
  avatar: 'avatar',
  chatOverlay: 'chatOverlay',
};

export function generateUniqueId(): string {
  return uuidv4();
}
