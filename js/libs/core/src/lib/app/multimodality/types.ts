export type MultimodalityObject = {
  title: string;
  progress?: number;
  state?: string;
  goal?: string | null;
  text?: string | null;
  modules: {
    title?: string;
    state?: string;
    description?: string;
    imageURL?: string;
    progress?: number;
    submoduleGroups?: Array<SubmoduleGroups>;
    submodules?: Array<Array<Submodules>>;
  }[];
};

export type ProfileContent = {
  name: string;
  values: Array<{ key: string; value: string }>;
};

export type ProfileContentWrapper = GenericMultimodalityObject & {
  data: ProfileContent;
};

export type SubmoduleGroups = {
  title?: string;
  progress?: number;
  submodules: Array<Submodules>;
};

export type MultimodalProps = {
  title?: string;
  state?: string;
  description?: string;
  progress?: number;
  imageURL?: string | null;
  action?: string;
  children?: MultimodalProps[];
};

export type MultimodalityObjectWrapper = GenericMultimodalityObject & {
  content: MultimodalityObject;
};

export type PersonaContent = {
  title: string;
  type: string;
  size: string;
  personasList: Array<PersonaObject>;
  personas: Array<PersonaObject>;
};

export type PersonaObject = {
  type: string;
  Companion: unknown;
  id: string;
  gender: string;
  name: string;
  description: string | null;
  avatarId: string;
  background: string | null;
  startFrame: string;
  endFrame: string;
  avatarName: string;
  AvatarRegex: string;
};

export type PersonaContentWrapper = GenericMultimodalityObject & {
  content: PersonaContent;
};

export type Submodules = {
  title: string;
  action?: string;
  state?: string;
  progress?: number;
  imageURL?: string | null;
  description?: string;
};

export type GenericMultimodalityObject<T = Record<string, any>> = {
  // omit Companion and type from T
  type: string;
  Companion: unknown;
  // Generic key-value pairs
} & T;
