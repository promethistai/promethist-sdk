import {
  MultimodalityObject,
  MultimodalityObjectWrapper,
  PersonaContentWrapper,
  ProfileContentWrapper,
} from './types';

export const growthContent: MultimodalityObjectWrapper = {
  type: 'GrowthContent',
  Companion: {},
  content: {
    title:
      'Vítejte v testovacím prostředí T-Mobile!\n\nPřihlašte se jedním z přednastavených profilů:',
    progress: 0.0,
    goal: null,
    text: null,
    modules: [
      {
        title: 'Petr Novák',
        description: '',
        progress: 0.0,
        state: 'Unlocked',
        imageURL: '',
        submoduleGroups: [],
        submodules: [
          [
            {
              title: 'Přihlásit se',
              action: 'Petr_log',
              description: '',
              imageURL: null,
              progress: 0.0,
              state: 'Unlocked',
            },
            {
              title: 'Prohlédnout si profil',
              action: 'Petr_read',
              description: '',
              imageURL: null,
              progress: 0.0,
              state: 'Unlocked',
            },
          ],
        ],
      },
      {
        title: 'Františka Zelená',
        description: '',
        progress: 0.0,
        state: 'Unlocked',
        imageURL: '',
        submoduleGroups: [],
        submodules: [
          [
            {
              title: 'Přihlásit se',
              action: 'Frantiska_log',
              description: '',
              imageURL: null,
              progress: 0.0,
              state: 'Unlocked',
            },
            {
              title: 'Prohlédnout si profil',
              action: 'Frantiska_read',
              description: '',
              imageURL: null,
              progress: 0.0,
              state: 'Unlocked',
            },
          ],
        ],
      },
      {
        title: 'Honza Pichl',
        description: '',
        progress: 0.0,
        state: 'Unlocked',
        imageURL: '',
        submoduleGroups: [],
        submodules: [
          [
            {
              title: 'Přihlásit se',
              action: 'Honza_log',
              description: '',
              imageURL: null,
              progress: 0.0,
              state: 'Unlocked',
            },
            {
              title: 'Prohlédnout si profil',
              action: 'Honza_read',
              description: '',
              imageURL: null,
              progress: 0.0,
              state: 'Unlocked',
            },
          ],
        ],
      },
    ],
  },
};

export const profileContent: ProfileContentWrapper = {
  type: 'Profile',
  Companion: {},
  data: {
    name: 'Petr Novák',
    values: [
      { key: 'Datové balíčky', value: 'nemáte žádné aktivní balíčky' },
      { key: 'Roaming pro volání a SMS', value: 'aktivní' },
      { key: 'Datový roaming', value: 'vypnutý' },
      { key: 'Mezinárodní hovory', value: 'aktivní' },
      { key: 'Společnost', value: 'Chef Parade' },
      { key: 'Firemní admin', value: 'Honza Pichl' },
      { key: 'Kontakt na firemního admina', value: '606-473-236' },
    ],
  },
};

export const dataContent: MultimodalityObject = {
  title: "Prohlížíte si profil uživatele 'Honza Pichl'",
  progress: 0,
  goal: null,
  text: null,
  modules: [
    {
      title: 'Datové balíčky: A, B, C',
      description: '',
      progress: 0,
      state: 'Unlocked',
      imageURL: '',
      submoduleGroups: [],
      submodules: [],
    },
    {
      title: 'Datový roaming: aktivní',
      description: '',
      progress: 0,
      state: 'Unlocked',
      imageURL: '',
      submoduleGroups: [],
      submodules: [],
    },
    {
      title: 'Roaming pro volání a SMS: aktivní',
      description: '',
      progress: 0,
      state: 'Unlocked',
      imageURL: '',
      submoduleGroups: [],
      submodules: [],
    },
    {
      title: 'Mezinárodní hovory: aktivní',
      description: '',
      progress: 0,
      state: 'Unlocked',
      imageURL: '',
      submoduleGroups: [],
      submodules: [],
    },
    {
      title: 'Společnost: Chef Parade',
      description: '',
      progress: 0,
      state: 'Unlocked',
      imageURL: '',
      submoduleGroups: [],
      submodules: [],
    },
    {
      title: 'Role: administrátor',
      description: '',
      progress: 0,
      state: 'Unlocked',
      imageURL: '',
      submoduleGroups: [],
      submodules: [],
    },
  ],
};

export const setPersonaData: PersonaContentWrapper = {
  type: 'SetPersona',
  Companion: {},
  content: {
    title: '',
    type: 'Primary',
    size: 'L',
    personasList: [
      {
        type: 'Persona',
        Companion: {},
        id: 'ai.promethist.tmobile.asset.persona.persona_e',
        gender: 'female',
        name: 'Ezra',
        description: null,
        avatarId: 'ezra',
        background: null,
        startFrame: 'A',
        endFrame: 'A',
        avatarName: 'ezra',
        AvatarRegex: '[a-f0-9]{24}',
      },
    ],
    personas: [
      {
        type: 'Persona',
        Companion: {},
        id: 'ai.promethist.tmobile.asset.persona.persona_e',
        gender: 'female',
        name: 'Ezra',
        description: null,
        avatarId: 'ezra',
        background: null,
        startFrame: 'A',
        endFrame: 'A',
        avatarName: 'ezra',
        AvatarRegex: '[a-f0-9]{24}',
      },
    ],
  },
};
