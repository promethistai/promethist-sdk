import { Button, Col, Flex, Row, Typography } from 'antd';
import { useAppStore } from '../contexts';
import { WebSocketMessage } from '../hooks/types';

export type MultimodalYesNoProps = {
  onClick: (action: WebSocketMessage) => void;
} & SelectionProps;

export type SelectionProps = {
  title: string;
  actions: Array<{
    action: string;
    label: string;
    type: 'PRIMARY' | 'SECONDARY';
  }>;
};

const SecondaryButtonStyle = {
  borderRadius: '30px',
  background: 'rgba(29, 61, 224, 0.20)',
  backdropFilter: 'blur(17.51532554626465px)',
  width: '100%',
  height: '60px',
};

const PrimaryButtonStyle = {
  borderRadius: '30px',
  height: '60px',
  background: '#1D3DE0',
  backdropFilter: 'blur(17.51532554626465px)',
  width: '100%',
};

const PrimaryButtonText = {
  color: '#fff',
  fontSize: '18px',
  fontStyle: 'normal',
  fontWeight: 600,
};

const SecondaryButtonText = {
  color: '#1D3DE0',
  fontFamily: 'Inter',
  fontSize: '16px',
  fontStyle: 'normal',
  fontWeight: 600,
  lineHeight: 'normal',
};

export const MultimodalYesNo: React.FC<MultimodalYesNoProps> = ({
  title,
  actions,
  onClick,
}) => {
  const { closeMultimodality, setIsMultimodalityBeingHandled } = useAppStore();
  const handleAction = (action: string) => {
    onClick(action);
    setIsMultimodalityBeingHandled(true);
    closeMultimodality();
  };
  return (
    <Flex vertical gap={'1rem'}>
      <Typography
        style={{
          color: '#000',
          textAlign: 'center',
          fontFamily: 'Inter',
          fontSize: '30px',
          fontStyle: 'normal',
          fontWeight: 500,
        }}
      >
        {title}
      </Typography>
      <Row style={{ width: '100%' }}>
        {actions.map((action) => (
          <Col
            key={action.action}
            span={actions.length === 2 ? 12 : 24}
            style={{ width: '100%', padding: '0 0.5rem' }}
          >
            <Button
              key={action.action}
              style={
                action.type === 'PRIMARY'
                  ? { ...PrimaryButtonStyle }
                  : { ...SecondaryButtonStyle }
              }
              onClick={() => handleAction(action.action)}
            >
              <Typography
                style={
                  action.type === 'PRIMARY'
                    ? PrimaryButtonText
                    : SecondaryButtonText
                }
              >
                {action.label}
              </Typography>
            </Button>
          </Col>
        ))}
      </Row>
    </Flex>
  );
};
