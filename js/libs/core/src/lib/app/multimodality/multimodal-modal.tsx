import { Modal } from 'antd';
import { sanizizeSelectionMessage } from '../../api';
import { useAppStore } from '../contexts';
import { WebSocketMessage } from '../hooks/types';
import { MultimodalYesNo, SelectionProps } from './multimodal-yes-no';

enum MultimodalType {
  SELECTION = '#selection',
}

export const MultimodalModal = ({
  onClick,
}: {
  onClick: (message: WebSocketMessage) => void;
}) => {
  const { isMultimodalityOpen, multimodalityContent } = useAppStore();
  if (!multimodalityContent) return null;
  const modalType = multimodalityContent?.split(':')[0];
  const modalContent = sanizizeSelectionMessage(multimodalityContent);
  const parsedModalContent: SelectionProps = JSON.parse(modalContent);
  return (
    <Modal
      key={'multimodal-modal'}
      open={isMultimodalityOpen}
      style={{ top: 'calc(100dvh - 200px)' }}
      footer={null}
      closable={false}
      destroyOnClose={true}
    >
      {modalType === MultimodalType.SELECTION && (
        <MultimodalYesNo
          onClick={onClick}
          actions={parsedModalContent.actions}
          title={parsedModalContent.title}
        />
      )}
    </Modal>
  );
};
