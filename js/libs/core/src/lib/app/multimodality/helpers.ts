import {
  MultimodalityObject,
  MultimodalityObjectWrapper,
  MultimodalProps,
  ProfileContent,
  ProfileContentWrapper,
} from './types';

export const transformMultimodalityObjectToMultimodalProps = (
  multimodalityObject: MultimodalityObject
): MultimodalProps => {
  const transformModule = (
    module: MultimodalityObject['modules'][0]
  ): MultimodalProps => {
    const children = module.submodules
      ?.map((submodule) => {
        return submodule.map((subsubmodule) => {
          return {
            title: subsubmodule.title,
            action: subsubmodule.action,
            state: subsubmodule.state,
            description: subsubmodule.description,
            imageURL: subsubmodule.imageURL,
            progress: subsubmodule.progress,
          };
        });
      })
      .flat();

    return {
      title: module.title,
      state: module.state,
      progress: module.progress,
      imageURL: module.imageURL,
      description: module.description,
      // children: module.submoduleGroups?.map(
      //   (submoduleGroup: SubmoduleGroups): MultimodalProps => {
      //     return {
      //       title: submoduleGroup.title,
      //       progress: submoduleGroup.progress,
      //       children: submoduleGroup.submodules.map(
      //         (submodule): MultimodalProps => {
      //           return {
      //             title: submodule.title,
      //             action: submodule.action,
      //             state: submodule.state,
      //             description: submodule.description,
      //             imageURL: submodule.imageURL,
      //             progress: submodule.progress,
      //           };
      //         }
      //       ),
      //     };
      //   }
      // ),
      children,
    };
  };

  return {
    title: multimodalityObject.title,
    progress: multimodalityObject.progress,
    state: multimodalityObject.state,
    children: multimodalityObject.modules.map(transformModule),
  };
};

export const transformProfileContentToMultimodalProps = (
  profileContent: ProfileContent
): MultimodalProps => {
  return {
    title: profileContent.name,
    children: profileContent.values.map((val) => {
      return { title: val.key + ': ' + val.value };
    }),
  };
};

export enum MultimodalityType {
  GrowthContent = 'GrowthContent',
  Profile = 'Profile',
  SetPersona = 'SetPersona',
}

export const multimodalTransformer = (
  object: MultimodalityObjectWrapper | ProfileContentWrapper
) => {
  switch (object.type) {
    case MultimodalityType.GrowthContent:
      return transformMultimodalityObjectToMultimodalProps(object.content);
    case MultimodalityType.Profile:
      return transformProfileContentToMultimodalProps(object.data);
    default:
      return null;
  }
};
