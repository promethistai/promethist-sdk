import { useEffect, useRef, useState } from 'react';
import styled from 'styled-components';
type StreamedTextProps = {
  text: string;
  speed?: number;
};

export const StreamedText = ({ text, speed = 20 }: StreamedTextProps) => {
  const [displayText, setDisplayText] = useState('');
  const containerRef = useRef<HTMLDivElement>(null);
  const indexRef = useRef(0);

  useEffect(() => {
    const container = containerRef.current;
    let animationFrame: number;

    const typeText = () => {
      setDisplayText((prev) => {
        if (!text.includes(prev)) {
          indexRef.current = 0;
          return '';
        }
        const nextChar = text[indexRef.current];
        if (!nextChar) {
          return prev;
        }
        indexRef.current += 1;

        // Auto-scroll when approaching bottom
        if (container) {
          const { scrollTop, clientHeight, scrollHeight } = container;
          if (scrollHeight - (scrollTop + clientHeight) < 50) {
            requestAnimationFrame(() => {
              container.scrollTo({
                top: scrollHeight,
                behavior: 'smooth',
              });
            });
          }
        }

        return prev + nextChar;
      });
      animationFrame = requestAnimationFrame(typeText);
    };

    animationFrame = requestAnimationFrame(typeText);
    return () => cancelAnimationFrame(animationFrame);
  }, [text, speed]);

  return (
    <ScrollContainer ref={containerRef}>
      <TypingText>{displayText}</TypingText>
    </ScrollContainer>
  );
};

// Container with fixed height and smooth scrolling
export const ScrollContainer = styled.div`
  position: absolute;
  top: 60%;
  max-height: 20vh;
  max-width: 600px;
  overflow-y: auto;
  padding-left: 12px;
  padding-right: 12px;
  padding: 20px; /* To še mi líbí */
  border-radius: 8px;
  font-family: Inter;
  line-height: 1.6;
  color: whitesmoke;
  font-size: 1.2rem;
  text-shadow: 0px 0px 3px #393838, 0px 0px 5px #393838, 0px 0px 8px #393838;

  /* Smooth scrollbar */
  scroll-behavior: smooth;
  &::-webkit-scrollbar {
    width: 8px;
  }
  &::-webkit-scrollbar-thumb {
    border-radius: 4px;
  }
`;

// Typing text element
const TypingText = styled.div`
  white-space: pre-wrap;
  overflow-wrap: break-word;
`;
