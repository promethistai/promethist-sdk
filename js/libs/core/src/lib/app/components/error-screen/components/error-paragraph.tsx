import { Typography } from 'antd';

export const ErrorParagraph = ({
  errorMessage,
  copyable,
}: {
  errorMessage: string;
  copyable?: boolean;
}) => {
  return (
    <Typography.Text
      copyable={copyable}
      style={{
        color: '#39354E',
        fontFamily: 'Inter',
        fontSize: 18,
        marginBottom: 0,
        fontStyle: 'normal',
        fontWeight: 400,
        textAlign: 'center',
      }}
    >
      {errorMessage}
    </Typography.Text>
  );
};

// Main component to render parsed data
export const ErrorDetails = ({
  parsedData,
}: {
  parsedData: { [key: string]: string };
}) => {
  return (
    <div>
      {Object.entries(parsedData).map(([key, value]) => (
        <div key={key}>
          <ErrorParagraph errorMessage={key + ': '} />
          <ErrorParagraph errorMessage={value} copyable />
        </div>
      ))}
    </div>
  );
};
