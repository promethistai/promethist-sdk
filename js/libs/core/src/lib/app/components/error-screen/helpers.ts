export function parseQueryString(queryString: string): {
  [key: string]: string;
} {
  if (queryString.startsWith('?')) {
    queryString = queryString.slice(1);
  }

  const pairs = queryString.split('&');
  const result: { [key: string]: string } = {};

  pairs.forEach((pair) => {
    const [key, value] = pair.split('=');

    result[decodeURIComponent(key)] = decodeURIComponent(value || '');
  });

  return result;
}
