import { Typography } from 'antd';

export const ErrorHeader = ({ title }: { title: string }) => {
  return (
    <Typography.Paragraph
      style={{
        lineHeight: 1.1,
        fontFamily: 'Inter',
        fontSize: 60,
        marginBottom: 60,
        fontStyle: 'normal',
        fontWeight: 900,
        background: 'linear-gradient(106deg, #1D3DE0 40.65%, #3E1087 83.4%)',
        backgroundClip: 'text',
        WebkitBackgroundClip: 'text',
        WebkitTextFillColor: 'transparent',
        textAlign: 'center',
      }}
    >
      {title}
    </Typography.Paragraph>
  );
};
