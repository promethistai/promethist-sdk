import { useAppStore } from '../../contexts';
import { ErrorHeader } from './components/error-header';
import { ErrorDetails } from './components/error-paragraph';
import { parseQueryString } from './helpers';

const KNOWN_ERROR_PERSONA_NOT_PRESENT =
  'type=InvocationTargetException&message=Unknown';

export const ErrorOverlay = () => {
  const { errorMessage } = useAppStore();

  const renderErrorContent = () => {
    const parsedErrorMessage = parseQueryString(errorMessage);

    if (errorMessage.includes(KNOWN_ERROR_PERSONA_NOT_PRESENT)) {
      return (
        <>
          <ErrorHeader title="Persona is not accessible" />
          <ErrorDetails parsedData={parsedErrorMessage} />;
        </>
      );
    }
    return (
      <>
        <ErrorHeader title="Something went wrong" />
        <ErrorDetails parsedData={parsedErrorMessage} />;
      </>
    );
  };

  return (
    <div
      style={{
        position: 'fixed',
        width: '100%',
        height: '100dvh',
        zIndex: 10000,
        backgroundColor: 'rgba(255,255,255, 1)',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        padding: 20,
      }}
    >
      {renderErrorContent()}
    </div>
  );
};
