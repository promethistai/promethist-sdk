import { useEffect, useState } from 'react';
import styled from 'styled-components';
import { useAppStore, useRefStore } from '../contexts';
import { ChatPageProps } from '../pages';
import { ChatSearchbar } from './chat-searchbar';
import { MessagesList } from './messages-list';

export const ChatOverlay: React.FC<ChatPageProps> = ({ messages }) => {
  const { persona, setIsChatReady } = useAppStore();
  const { arcUnityRef } = useRefStore();

  const [searchValue, setSearchValue] = useState('');

  useEffect(() => {
    setIsChatReady(true);
    return () => {
      setIsChatReady(false);
    };
  }, []);

  if (!arcUnityRef?.current) return null;

  return (
    <StyledChatOverlay>
      <MessagesList
        messages={messages}
        searchValue={searchValue}
        applicationName={persona?.name ?? 'Unnamed Persona'}
      />
      <ChatSearchbar value={searchValue} onSearch={setSearchValue} />
    </StyledChatOverlay>
  );
};

const StyledChatOverlay = styled.div`
  height: 100vh;
  max-width: 736px;
  width: 100%;
  left: 50%;
  transform: translateX(-50%);
  position: fixed;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  overflow: hidden;
  z-index: 21;
`;
