import { useEffect } from 'react';
import { useAppStore } from '../contexts';
import { usePermissionMicValue } from '../hooks';
import { PauseOverlay } from './pause-overlay';
import { ScrollContainer, StreamedText } from './streamed-text';

export const TranscriptOverlay = () => {
  const { lastMessage, userTranscript, isDebugMode } = useAppStore();
  const [_, setIsMic] = usePermissionMicValue();

  if (isDebugMode) console.log('TranscriptOverlay', lastMessage);
  useEffect(() => {
    setIsMic(true);
  }, []);

  return (
    <PauseOverlay>
      {lastMessage && !userTranscript && (
        <StreamedText text={lastMessage}></StreamedText>
      )}
      {userTranscript && <ScrollContainer>{userTranscript}</ScrollContainer>}
    </PauseOverlay>
  );
};
