import Icon, { CaretRightOutlined, PauseOutlined } from '@ant-design/icons';
import { FloatButton } from 'antd';
import { ReactNode, useRef } from 'react';
import styled from 'styled-components';
import { SoundWaveIcon } from '../../icons';
import { GreenSoundWaveIcon } from '../../icons/green-sound-wave';
import { useAppStore } from '../contexts';

export const PauseOverlay = ({ children }: { children: ReactNode }) => {
  const {
    sendAudioPause,
    sendAudioResume,
    isAudioBeingHandled,
    disableMicrophone,
    enableMicrophone,
    getIsMicrophone,
    isDebugMode,
    isClientPaused,
    setIsClientPaused,
  } = useAppStore();

  const isMicrophone = getIsMicrophone();

  const divRef = useRef<HTMLDivElement>(null);

  const pause = () => {
    if (isDebugMode) console.log('Pausing ARC, pausing microphone');
    setIsClientPaused(true);
    disableMicrophone();
    sendAudioPause();
  };
  const resume = () => {
    setIsClientPaused(false);
    if (isDebugMode)
      console.log('Resuming ARC: ', 'isAudioBeingHandled', isAudioBeingHandled);
    if (!isAudioBeingHandled) {
      enableMicrophone();
    }
    sendAudioResume();
  };

  return (
    <StyledPauseOverlay ref={divRef}>
      <FloatButton
        style={{
          left: 24,
          bottom: 24,
          height: 50,
          width: 50,
          justifyContent: 'flex-end',
          backgroundColor: 'rgba(138, 137, 155, 1)',
        }}
        type="primary"
        onClick={() => (isClientPaused ? resume() : pause())}
        icon={isClientPaused ? <CaretRightOutlined /> : <PauseOutlined />}
      />
      <Icon
        style={{
          position: 'fixed',
          bottom: 24,
          width: 'calc(100% - 200px)',
          maxWidth: '400px',
          left: '50%',
          transform: 'translateX(-50%)',
          backgroundColor: isMicrophone ? 'rgba(125, 255, 158, 0.7)' : 'grey',
          mixBlendMode: 'darken',
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
          fontSize: '120px',
          zIndex: 21,
          height: '50px',
          borderRadius: '25px',
          opacity: 0.7,
        }}
        component={isMicrophone ? GreenSoundWaveIcon : SoundWaveIcon}
      />
      {isClientPaused && (
        <CaretRightOutlined
          onClick={() => resume()}
          style={{ fontSize: '200px', zIndex: 21, color: 'lightgrey' }}
        />
      )}
      <>{children}</>
    </StyledPauseOverlay>
  );
};

const StyledPauseOverlay = styled.div`
  width: 100%;
  height: 100%;
  position: absolute;
  display: flex;
  justify-content: center;
  align-items: center;
  overflow: hidden;
  z-index: 21;
`;
