import { useKeyPress } from 'ahooks';
import { Carousel } from 'antd';
import { CarouselRef } from 'antd/es/carousel';
import { useRef } from 'react';
export type CarouselProps = {
  children: React.ReactNode;
};

export const CustomCarousel: React.FC<CarouselProps> = ({ children }) => {
  const ref = useRef<CarouselRef>(null);
  useKeyPress(39, () => {
    ref?.current?.next();
  });
  useKeyPress(37, () => {
    ref?.current?.prev();
  });
  return (
    <Carousel
      dotPosition="top"
      ref={ref}
      arrows
      infinite={false}
      style={{
        position: 'fixed',
        width: '100%',
        height: '100dvh',
        zIndex: 1000,
      }}
    >
      {children}
    </Carousel>
  );
};
