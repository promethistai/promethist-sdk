import { SendOutlined } from '@ant-design/icons';
import { Button, Input } from 'antd';
import { useAppStore } from '../contexts';

type ChatInputProps = {
  value: string;
  onChange: (value: string) => void;
  onSend: () => void;
};
export const ChatInput = (props: ChatInputProps) => {
  const { isInteractible } = useAppStore();
  return (
    <div style={{ display: 'flex', paddingTop: 12 }}>
      <div style={{ flex: 1 }}>
        <Input
          disabled={!isInteractible}
          value={props.value}
          placeholder="Type here..."
          onChange={(e) => props.onChange(e.currentTarget.value)}
          size="large"
          onKeyDown={(e) => e.key === 'Enter' && props.onSend()}
        />
      </div>
      <Button size="large" style={{ marginLeft: 4 }} onClick={props.onSend}>
        <SendOutlined />
      </Button>
    </div>
  );
};
