import { FileSearchOutlined, UserOutlined } from '@ant-design/icons';
import styled from 'styled-components';
import { StoreState } from '../contexts';

const Switch = styled.div`
  position: relative;
  display: inline-block;
  width: 169px;
  height: 60px;
`;

interface ToggleSwitchProps {
  onChange?: (checked: boolean) => void;
  checked?: boolean;
}

interface IconProps {
  color?: string;
  isLeft?: boolean;
}

const Slider = styled.div<ToggleSwitchProps>`
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: rgba(255, 255, 255, 0.1);
  backdrop-filter: blur(17.51532554626465px);
  transition: 0.4s;
  border-radius: 124px;
  border: 1px solid rgba(255, 255, 255, 0.3);
  width: 167px;
  height: 60px;

  &::before {
    position: absolute;
    content: '';
    height: 60px;
    width: 90px;
    left: ${(props) => (props.checked ? '77px' : '0px')};
    background-color: rgba(255, 255, 255, 1);
    transition: 0.4s;
    border-radius: 40px;
    z-index: 21;
  }
`;

const IconContainer = styled.div<IconProps>`
  position: absolute;
  top: 50%;
  transform: ${(props) =>
    props.isLeft ? 'translate(-50%, -50%)' : 'translate(50%, -50%)'};
  font-size: 16px;
  color: #fff;
  pointer-events: none;
`;

const LeftIcon = styled(IconContainer)<IconProps>`
  left: 45px;
  z-index: 20;
  transition: 0.4s;
  color: ${(props) => props.color};
`;

// Right background icon (for "on" state)
const RightIcon = styled(IconContainer)<IconProps>`
  right: 45px;
  z-index: 20;
  transition: 0.4s;
  color: ${(props) => props.color};
`;

export const ToggleSwitch = ({
  onChange,
  isChatOverlay,
}: {
  onChange: (key: StoreState['mode']) => void;
  isChatOverlay: boolean;
}) => {
  const handleToggle = () => {
    onChange(isChatOverlay ? 'avatar' : 'chatOverlay');
  };

  return (
    <div>
      <Switch onClick={handleToggle}>
        <LeftIcon
          isLeft
          color={
            !isChatOverlay ? 'rgba(207, 56, 122, 1)' : 'rgba(255, 255, 255, 1)'
          }
        >
          <UserOutlined />
        </LeftIcon>
        <RightIcon
          color={
            isChatOverlay ? 'rgba(207, 56, 122, 1)' : 'rgba(255, 255, 255, 1)'
          }
        >
          <FileSearchOutlined />
        </RightIcon>
        <Slider checked={isChatOverlay} />
      </Switch>
    </div>
  );
};
