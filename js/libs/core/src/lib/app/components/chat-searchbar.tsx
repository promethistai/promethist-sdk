import { SearchOutlined } from '@ant-design/icons';
import { ConfigProvider, Input } from 'antd';

export const ChatSearchbar = ({
  value,
  onSearch,
}: {
  value: string;
  onSearch: (input: string) => void;
}) => {
  return (
    <ConfigProvider
      theme={{
        components: {
          Input: {
            colorIcon: 'rgb(255,255,255)',
            colorText: 'rgb(255,255,255)',
            colorTextDescription: 'rgb(255,255,255)',
            colorTextPlaceholder: 'rgb(255,255,255)',
          },
        },
      }}
    >
      <Input
        prefix={<SearchOutlined />}
        placeholder="Search in the conversation..."
        allowClear
        style={{
          bottom: '3rem',
          left: '50%',
          position: 'fixed',
          width: 'calc(100% - 24px)',
          transform: 'translate(-50%, -50%)',
          height: 50,
          borderRadius: 25,
          color: 'rgba(255, 255, 255, 1)',
          border: 'none',
          background: 'var(--Surface-opacity-blur, rgba(245, 245, 247, 0.20))',
          backdropFilter: 'blur(17.51532554626465px)',
        }}
        value={value}
        onChange={(event) => {
          onSearch(event.target.value);
        }}
      />
    </ConfigProvider>
  );
};
