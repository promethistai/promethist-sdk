import { Typography } from 'antd';

export const SmallText = ({
  children,
  style,
  centerText,
  className,
}: {
  children: React.ReactNode;
  style?: React.CSSProperties;
  centerText?: boolean;
  className?: string;
}) => {
  return (
    <Typography.Paragraph
      className={className}
      style={{
        color: '#39354E',
        fontFamily: 'Inter',
        fontSize: 18,
        marginBottom: 0,
        fontStyle: 'normal',
        fontWeight: 400,
        userSelect: 'none',
        ...style,
        textAlign: centerText ? 'center' : 'left',
      }}
    >
      {children}
    </Typography.Paragraph>
  );
};

export const LargeText = ({
  children,
  style,
  centerText,
}: {
  children: React.ReactNode;
  style?: React.CSSProperties;
  centerText?: boolean;
}) => {
  return (
    <Typography.Paragraph
      style={{
        lineHeight: 1.1,
        fontFamily: 'Inter',
        fontSize: 60,
        marginBottom: 0,
        fontStyle: 'normal',
        fontWeight: 900,
        background: 'linear-gradient(106deg, #1D3DE0 40.65%, #3E1087 83.4%)',
        backgroundClip: 'text',
        WebkitBackgroundClip: 'text',
        userSelect: 'none',
        WebkitTextFillColor: 'transparent',
        ...style,
        textAlign: centerText ? 'center' : 'left',
      }}
    >
      {children}
    </Typography.Paragraph>
  );
};

export const BoldText = ({ children }: { children: React.ReactNode }) => {
  return (
    <Typography.Paragraph
      style={{
        color: '#1E1E1E',
        fontFamily: 'Inter',
        fontSize: 20,
        userSelect: 'none',
        fontStyle: 'normal',
        fontWeight: 600,
      }}
    >
      {children}
    </Typography.Paragraph>
  );
};

export const TranscriptText = ({ children }: { children: React.ReactNode }) => {
  return (
    <Typography.Paragraph
      style={{
        color: '#1E1E1E',
        fontFamily: 'Inter',
        fontSize: 16,
        userSelect: 'none',
        fontStyle: 'normal',
        fontWeight: 400,
      }}
    >
      {children}
    </Typography.Paragraph>
  );
};
