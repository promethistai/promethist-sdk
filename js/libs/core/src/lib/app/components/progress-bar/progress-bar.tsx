import { LoadingOutlined } from '@ant-design/icons';
import { Progress, Spin } from 'antd';
import { useEffect, useState } from 'react';
import { useAppStore } from '../../contexts';
import { SmallText } from '../texts/texts';
import './text-slider.css';

export const ProgressBar: React.FC = () => {
  const { progress, getIsClientReady } = useAppStore();
  const isClientReady = getIsClientReady();
  const fixedProgress = Number(progress.toFixed(0));
  return (
    <div
      style={{
        position: 'absolute',
        width: '100%',
        height: '100%',
        zIndex: 900,
      }}
    >
      <div
        style={{
          width: '100dvw',
          height: '100dvh',
          justifyContent: 'center',
          alignItems: 'center',
          display: 'flex',
          zIndex: 900,
        }}
      >
        {!isClientReady && (
          <>
            {progress >= 100 ? (
              <LoadingSpinner />
            ) : (
              <Progress
                percent={fixedProgress}
                format={() => (
                  <SmallText
                    style={{
                      textShadow:
                        '0px 0px 3px #393838, 0px 0px 5px #393838, 0px 0px 8px #393838',
                      color: 'white',
                      display: 'flex',
                      justifyContent: 'center',
                    }}
                  >
                    {fixedProgress}%
                  </SmallText>
                )}
                type="circle"
              />
            )}
            <TextSlider isDownloading={progress < 100} />
          </>
        )}
      </div>
    </div>
  );
};

export const TextSlider = ({ isDownloading = false, interval = 3000 }) => {
  const [currentIndex, setCurrentIndex] = useState(0);

  const slideText = [
    'Prosíme vydržte',
    'Výstavba kanceláře',
    'Avatar se obléká',
    'Příprava na pracovní den',
    'Nahazování úsměvu',
  ];

  useEffect(() => {
    const timer = setInterval(() => {
      setCurrentIndex((prevIndex) => (prevIndex + 1) % slideText.length);
    }, interval);

    return () => clearInterval(timer); // Cleanup on unmount
  }, [slideText.length, interval]);

  return (
    <div className="text-slider-container">
      {isDownloading && (
        <SmallText
          centerText
          style={{
            textShadow:
              '0px 0px 3px #393838, 0px 0px 5px #393838, 0px 0px 8px #393838',
            color: 'white',
          }}
        >
          Stahování aplikace...
        </SmallText>
      )}
      {!isDownloading &&
        slideText.map((text, index) => (
          <SmallText
            key={index}
            centerText
            style={{
              textShadow:
                '0px 0px 3px #393838, 0px 0px 5px #393838, 0px 0px 8px #393838',
              color: 'white',
            }}
            className={`text-slide ${index === currentIndex ? 'active' : ''}`}
          >
            {text}
          </SmallText>
        ))}
    </div>
  );
};

export const LoadingSpinner = () => {
  return (
    <Spin indicator={<LoadingOutlined style={{ fontSize: 120 }} spin />} />
  );
};
