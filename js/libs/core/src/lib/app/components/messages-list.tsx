import Chat, { Bubble, MessageProps } from '@chatui/core';
import '@chatui/core/dist/index.css';
import '@chatui/core/es/styles/index.less';
import { Space, Typography } from 'antd';
import React, { useMemo } from 'react';
import styled from 'styled-components';
import {
  ensureIsCommandTranscript,
  sanitizeMessageList,
  sanitizeTranscriptMessage,
} from '../../api/protocol-logic';
import { RawMessageType } from '../pages';
import './message-list/chatui-theme.css';

type MessagesListProps = {
  messages: RawMessageType[];
  applicationName?: string;
  searchValue: string;
};

export const MessagesList: React.FC<MessagesListProps> = ({
  messages,
  searchValue,
  applicationName,
}) => {
  const _messageList: MessageProps[] = useMemo(
    () =>
      sanitizeMessageList(messages).map(
        (it, id): MessageProps => ({
          _id: id.toString(),
          type: 'text',
          content: {
            user: ensureIsCommandTranscript(it.text) ? 'User' : applicationName,
            text: sanitizeTranscriptMessage(it.text),
            createdAt: it.date.toDateString(),
            position: ensureIsCommandTranscript(it.text) ? 'right' : 'left',
          },
          position: ensureIsCommandTranscript(it.text) ? 'right' : 'left',
          renderMessageContent: renderMessageContent,
        })

        // eslint-disable-next-line react-hooks/exhaustive-deps
      ),
    [messages, applicationName]
  );

  const highlightText = (text: string, search: string) => {
    if (!search) return text;

    const regex = new RegExp(`(${search})`, 'gi');
    const parts = text.split(regex);

    return parts.map((part, index) =>
      part.toLowerCase() === search.toLowerCase() ? (
        <span key={index} style={{ backgroundColor: 'yellow' }}>
          {part}
        </span>
      ) : (
        part
      )
    );
  };

  function renderMessageContent(msg: MessageProps) {
    const { content } = msg;
    if (content.position === 'left') {
      return (
        <Bubble
          style={{
            borderRadius: '30px',
            background: 'rgba(255, 255, 255, 0.70)',
            backdropFilter: 'blur(17.51532554626465px)',
          }}
          type="text"
        >
          <Space direction="horizontal">
            <MessageUserName>{content.user}</MessageUserName>
            <Typography.Text
              style={{
                color: 'var(--Text-secondary, #5C5E69)',
                fontFamily: 'SF Pro',
                fontSize: '12px',
                fontStyle: 'normal',
                fontWeight: 400,
                lineHeight: 'normal',
                opacity: 0.6,
              }}
            >
              {content.createdAt}
            </Typography.Text>
          </Space>
          <MessageText>{highlightText(content.text, searchValue)}</MessageText>
        </Bubble>
      );
    }
    return (
      <Bubble
        style={{
          borderRadius: '30px',
          background: 'var(--Surface-secondary, #E20074)',
        }}
        type="text"
      >
        <Space direction="horizontal">
          <Typography.Text
            style={{
              color: 'rgba(255, 255, 255, 1)',
              fontFamily: 'SF Pro',
              fontSize: '12px',
              fontStyle: 'normal',
              fontWeight: 400,
              lineHeight: 'normal',
              opacity: 0.7,
            }}
          >
            {content.createdAt}
          </Typography.Text>
        </Space>
        <MessageText>{highlightText(content.text, searchValue)}</MessageText>
      </Bubble>
    );
  }

  return (
    <Chat
      onSend={(msg) => console.log(msg)}
      messages={_messageList}
      renderMessageContent={renderMessageContent}
      Composer={() => null}
    />
  );
};

const MessageUserName = styled.p`
  border-radius: 20px;
  background: #fff;
  color: var(--Text-primary, #1f1f23);
  text-align: center;
  font-family: 'SF Pro';
  font-size: 12px;
  font-style: normal;
  font-weight: 400;
  line-height: normal;
  padding: 3px 6px;
`;

const MessageText = styled.p`
  color: #1d1d1d;
  font-family: 'SF Pro Display';
  font-size: 18px;
  font-style: normal;
  font-weight: 400;
  line-height: normal;
`;

export const MessageListWrapper = styled.div`
  .rce-mbox.rce-mbox-right {
    background-color: rgba(20, 119, 249, 0.75);
    color: white;
    & > .rce-mbox-body > .rce-mbox-time {
      color: #fefefe;
    }
  }

  .rce-mbox.rce-mbox--clear-notch {
    max-width: 60%;
  }
`;
