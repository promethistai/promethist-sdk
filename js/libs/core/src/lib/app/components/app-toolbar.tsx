import {
  AudioMutedOutlined,
  AudioOutlined,
  ClearOutlined,
  MutedOutlined,
  SoundOutlined,
} from '@ant-design/icons';
import { Button, Select, Space } from 'antd';
import { ReactNode } from 'react';
import { localeEnum } from '../constants';
import { useAppStore } from '../contexts';
import { usePermissionAudioValue } from '../hooks/local-storage-values/use-permission-audio-value';
import { usePermissionMicValue } from '../hooks/local-storage-values/use-permission-mic-value';

interface AppToolbarProps {
  children?: ReactNode;
  handleResetMessages: () => void;
}

export const AppToolbar = ({
  children,
  handleResetMessages,
}: AppToolbarProps) => {
  const [isMic, setIsMic] = usePermissionMicValue();
  const [isAudio, setIsAudio] = usePermissionAudioValue();
  const { locale, setLocale } = useAppStore();

  return (
    <Space
      style={{
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingBottom: 8,
        padding: '5px 16px',
      }}
    >
      <Button
        title="Reset messages"
        icon={<ClearOutlined />}
        onClick={() => handleResetMessages()}
      />
      <Select
        onChange={(value) => setLocale(value)}
        options={localeEnum}
        value={locale}
      />
      <Button
        danger={!isAudio}
        onClick={() => setIsAudio(!isAudio)}
        icon={isAudio ? <SoundOutlined /> : <MutedOutlined />}
      />
      <Button
        // disabled
        danger={!isMic}
        onClick={() =>
          navigator.mediaDevices
            .getUserMedia({
              audio: true,
            })
            .then(() => setIsMic(!isMic))
        }
        icon={isMic ? <AudioOutlined /> : <AudioMutedOutlined />}
      />

      {children}
    </Space>
  );
};
