import styled from 'styled-components';
import { useAppStore } from '../contexts';

export const GreenLight = () => {
  const { getIsMicrophone } = useAppStore();

  const isMicrophone = getIsMicrophone();
  const greenLightStyle: React.CSSProperties = {
    opacity: isMicrophone ? 1 : 0,
  };

  return <StyledListeningIndicator style={greenLightStyle} />;
};

const StyledListeningIndicator = styled.div`
  position: absolute;
  width: 100%;
  height: 100%;
  left: 0;
  bottom: 0;
  background-image: radial-gradient(
    circle 40vh at 50% 100%,
    rgba(2, 255, 44, 0.9),
    rgba(2, 255, 44, 0)
  );
  transition: ease-in-out opacity 0.7s;
  z-index: 1;
`;
