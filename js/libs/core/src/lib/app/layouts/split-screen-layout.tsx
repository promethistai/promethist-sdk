import { Layout } from 'antd';
import { ReactNode } from 'react';

export const SplitScreenLayout = ({
  leftChild,
  rightChild,
}: {
  leftChild: ReactNode;
  rightChild: ReactNode;
}) => {
  return (
    <Layout>
      <Layout.Content style={{ display: 'flex', height: '100vh' }}>
        <Layout.Content style={{ flex: 1, padding: 12 }}>
          {leftChild}
        </Layout.Content>
        <Layout.Content style={{ flex: 1, padding: 12 }}>
          {rightChild}
        </Layout.Content>
      </Layout.Content>
    </Layout>
  );
};
