import { Layout } from 'antd';
import { ReactNode } from 'react';

export const CenterScreenLayout = ({ children }: { children: ReactNode }) => {
  return (
    <Layout>
      <Layout.Content
        style={{
          display: 'flex',
          height: '100vh',
          justifyContent: 'center',
          alignItems: 'center',
        }}
      >
        {children}
      </Layout.Content>
    </Layout>
  );
};
