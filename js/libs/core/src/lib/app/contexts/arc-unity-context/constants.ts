export const ARC_REPOSITORY_BASE_URL = 'https://repository.promethist.ai';
export const ARC_REPOSITORY_URL =
  ARC_REPOSITORY_BASE_URL + '/avatar/player/develop/webgl-1.1.3/';
export const ARC_VERSION = 'latest';
