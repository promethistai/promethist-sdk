import { createContext, ReactNode, useContext, useEffect, useRef } from 'react';
import { UnityContextHook } from 'react-unity-webgl/distribution/types/unity-context-hook';
import { createStore, StoreApi, useStore } from 'zustand';
import { devtools } from 'zustand/middleware';
import {
  ensureIsCommandExit,
  ensureIsCommandSpeechItem,
  sanitizeSpeechItemMessage,
} from '../../api';
import { convertPCMToFloat32 } from '../../utils/utils';
import { DEFAULT_CLIENT_MODES, LOCALE } from '../constants';
import { usePermissionAudioValue } from '../hooks';
import { constructArcMessage } from '../hooks/use-handle-arc-loading';
import { ArcEvent, ArcObject, OutgoingArcMessage } from '../pages';
import { ExternalStoreRefs, StoreType } from './types';

type StoreContextType = {
  store: StoreApi<StoreType>;
  refs: ExternalStoreRefs;
};

const StoreContext = createContext<StoreContextType | null>(null);

export type StoreProviderProps = {
  children: ReactNode;
  defaultStore: Partial<Pick<StoreType, 'mode' | 'modeEnum'>>;
};

function deleteAllCookies() {
  const cookies = document.cookie.split(';');

  for (let i = 0; i < cookies.length; i++) {
    const cookie = cookies[i];
    const eqPos = cookie.indexOf('=');
    const name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
    document.cookie = name + '=;expires=Thu, 01 Jan 1970 00:00:00 GMT;path=/';
  }
}

export const StoreProvider = ({
  children,
  defaultStore,
}: StoreProviderProps) => {
  const arcUnityRef = useRef<UnityContextHook>(null);
  const audioContextRef = useRef<AudioContext>();
  const gainNodeRef = useRef<GainNode>();
  const audioBufferSourceNodeRef = useRef<AudioBufferSourceNode>();
  const audioBufferQueueRef = useRef<(ArrayBufferLike | string)[]>([]);
  const audioStreamRef = useRef<MediaStream>();
  const transcriptQueueRef = useRef<string[]>([]);

  useEffect(() => {
    window.addEventListener('beforeunload', () => {
      deleteAllCookies();
    });
  }, []);

  const storeRef = useRef<StoreApi<StoreType>>();
  if (!storeRef.current) {
    storeRef.current = createStore<StoreType, [['zustand/devtools', never]]>(
      devtools(
        (set, get) => ({
          getIsClientReady: () => {
            const state = get();
            return state.isArcReady || state.isChatReady;
          },
          isTranscript: false,
          isAudio: false,
          isMicrophoneEnabled: false,
          isConversationEnded: false,
          isArcReady: false,
          isChatReady: false,
          isInteractible: false,
          isMultimodalityBeingHandled: false,
          isAudioBeingHandled: false,
          isAudioBeingReceived: false,
          isClientPaused: false,
          lastMessage: '',
          errorMessage: '',
          hasSentIntro: false,
          userTranscript: '',
          getShouldClientSendIntro: () => {
            const state = get();
            return (
              (state.isSceneLoaded || state.isChatReady) && !state.hasSentIntro
            );
          },
          isUnityLoaded: false,
          mode: defaultStore.mode || DEFAULT_CLIENT_MODES.avatar,
          modeEnum: defaultStore.modeEnum || DEFAULT_CLIENT_MODES,
          isSceneLoaded: false,
          persona: undefined,
          progress: 0,
          isApplicationReady: false,
          sessionEnded: false,
          isDebugMode: false,
          testVolume: 0,

          // Multimodality State
          isMultimodalityOpen: false,
          multimodalityContent: null,

          // Audio State
          locale: LOCALE.en,
          audioSampleRate: 24000,
          isAudioContextReady: false,

          // Client State Actions
          setIsClientPaused: (value) => set({ isClientPaused: value }),
          setIsTranscript: (value) => set({ isTranscript: value }),
          setUserTranscript: (transcript) =>
            set({ userTranscript: transcript }),
          setIsConversationEnded: (value) =>
            set({ isConversationEnded: value }),
          setIsArcReady: (value) => set({ isArcReady: value }),
          setIsChatReady: (value) => set({ isChatReady: value }),
          setIsInteractible: (value) => set({ isInteractible: value }),
          disableMicrophone: () => set({ isMicrophoneEnabled: false }),
          setHasSentIntro: (value) => set({ hasSentIntro: value }),
          enableMicrophone: () => set({ isMicrophoneEnabled: true }),
          disableAudio: () => set({ isAudio: false }),
          enableAudio: () => set({ isAudio: true }),
          setIsAudioBeingHandled: (value) =>
            set({ isAudioBeingHandled: value }),
          setIsAudioBeingReceived: (value) =>
            set({ isAudioBeingReceived: value }),
          setIsUnityLoaded: (value) => set({ isUnityLoaded: value }),
          setMode: (mode) => set({ mode }),
          setIsSceneLoaded: (value) => set({ isSceneLoaded: value }),
          setPersona: (persona) => set({ persona }),
          setProgress: (progress) => set({ progress }),
          setIsApplicationReady: (value) => set({ isApplicationReady: value }),
          setSessionEnded: (value) => set({ sessionEnded: value }),
          setIsDebugMode: (value) => set({ isDebugMode: value }),
          setTestVolume: (volume) => set({ testVolume: volume }),
          setIsMultimodalityBeingHandled: (value) =>
            set({ isMultimodalityBeingHandled: value }),
          setLastMessage: (message) => set({ lastMessage: message }),
          setErrorMessage: (message) => set({ errorMessage: message }),

          // Multimodality Actions
          toggleMultimodality: () =>
            set((state) => ({
              isMultimodalityOpen: !state.isMultimodalityOpen,
            })),
          setMultimodalityContent: (content) =>
            set({ multimodalityContent: content }),
          openMultimodality: () => set({ isMultimodalityOpen: true }),
          closeMultimodality: () => set({ isMultimodalityOpen: false }),

          // Audio Actions
          setLocale: (locale) => set({ locale: locale }),
          setAudioSampleRate: (sampleRate) =>
            set({ audioSampleRate: sampleRate }),
          startMicrophone: async () => {
            navigator.mediaDevices
              .getUserMedia({
                audio: { autoGainControl: true, noiseSuppression: true },
                video: false,
              })
              .then((stream) => {
                audioStreamRef.current = stream;
              });
          },
          stopMicrophone: () => {
            audioStreamRef.current?.getTracks().forEach((track) => {
              track.stop();
            });
          },
          setAudioContextReady: (value) => set({ isAudioContextReady: value }),
          processAudio: () => {
            const mode = get().mode;
            const setIsAudioBeingHandled = get().setIsAudioBeingHandled;
            const processAvatarAudioSpeechItem =
              get().processAvatarAudioSpeechItem;
            const processChatAudio = get().processChatAudio;
            setIsAudioBeingHandled(true);
            if (mode === DEFAULT_CLIENT_MODES.avatar) {
              processAvatarAudioSpeechItem();
            }
            if (mode === DEFAULT_CLIENT_MODES.chat) processChatAudio();
          },

          processAvatarAudioSpeechItem: () => {
            const isArcReady = get().isArcReady;
            const sendAudioSpeechItem = get().sendAudioSpeechItem;
            const isApplicationReady = get().isApplicationReady;
            const sendTurnEnd = get().sendTurnEnd;
            const processAudio = get().processAudio;
            if (!isArcReady || !isApplicationReady) return;
            const speechItem = audioBufferQueueRef?.current?.shift();
            if (!speechItem) return;
            if (typeof speechItem === 'string') {
              ensureIsCommandSpeechItem(speechItem) &&
                sendAudioSpeechItem(sanitizeSpeechItemMessage(speechItem));
              ensureIsCommandExit(speechItem) && sendTurnEnd();
              processAudio();
            }
          },

          processChatAudio: () => {
            const isAudioBeingHandled = get().isAudioBeingHandled;
            const processAudio = get().processAudio;
            const setIsAudioBeingHandled = get().setIsAudioBeingHandled;
            if (
              !audioContextRef?.current ||
              !gainNodeRef?.current ||
              !audioBufferQueueRef?.current ||
              !audioBufferSourceNodeRef ||
              isAudioBeingHandled
            )
              return;

            setIsAudioBeingHandled(true);
            const audioContext = audioContextRef.current;
            const gainNode = gainNodeRef.current;

            audioBufferSourceNodeRef.current =
              audioContextRef?.current.createBufferSource();

            if (audioBufferQueueRef.current.length === 0) return;
            const pcmData = audioBufferQueueRef.current.shift();
            if (!pcmData) return;

            if (typeof pcmData === 'string') {
              processAudio();
              return;
            }

            // Assuming PCM is 16-bit little-endian, mono, 44.1kHz
            const sampleRate = 24000; // Adjust this according to your PCM stream
            const numChannels = 1;

            const float32Array = convertPCMToFloat32(pcmData);

            const audioBuffer = audioContext.createBuffer(
              numChannels,
              float32Array.length / numChannels,
              sampleRate
            );

            audioBuffer.copyToChannel(float32Array, 0);
            audioBufferSourceNodeRef.current.buffer = audioBuffer;
            audioBufferSourceNodeRef.current.connect(gainNode);
            audioBufferSourceNodeRef.current.start(0);
            audioBufferSourceNodeRef.current.onended = () => {
              if (
                audioBufferQueueRef?.current?.length &&
                audioBufferQueueRef.current.length > 0
              ) {
                processAudio();
              } else {
                setIsAudioBeingHandled(false);
              }
            };
          },

          getIsMicrophone: () => {
            const state = get();

            return (
              state.isMicrophoneEnabled &&
              !state.isConversationEnded &&
              !state.isMultimodalityOpen &&
              !state.isMultimodalityBeingHandled &&
              !state.sessionEnded
            );
          },

          // Arc Actions
          sendTurnEnd: () => {
            const isArcReady = get().isArcReady;
            if (isArcReady) {
              arcUnityRef.current?.sendMessage(
                ArcObject.ClientApi,
                ArcEvent.SendCommand,
                constructArcMessage(OutgoingArcMessage.ExitTurn)
              );
            }
          },
          sendAudioEnd: () => {
            const isArcReady = get().isArcReady;
            if (isArcReady) {
              arcUnityRef.current?.sendMessage(
                ArcObject.ClientApi,
                ArcEvent.SendCommand,
                constructArcMessage(OutgoingArcMessage.AudioEnd)
              );
            }
          },
          sendAudioBlock: (buffer) => {
            const isArcReady = get().isArcReady;
            if (isArcReady) {
              arcUnityRef.current?.sendMessage(
                ArcObject.ClientApi,
                ArcEvent.SendCommand,
                buffer
              );
            }
          },
          sendAudioSpeechItem: (buffer) => {
            const isArcReady = get().isArcReady;

            if (isArcReady) {
              arcUnityRef.current?.sendMessage(
                ArcObject.ClientApi,
                ArcEvent.SendCommand,
                constructArcMessage(OutgoingArcMessage.QueueSpeech, buffer)
              );
            }
          },
          sendAudioPause: () => {
            const isArcReady = get().isArcReady;
            if (isArcReady) {
              arcUnityRef.current?.sendMessage(
                ArcObject.ClientApi,
                ArcEvent.SendCommand,
                constructArcMessage(OutgoingArcMessage.PauseAudio)
              );
            }
          },
          sendAudioResume: () => {
            const isArcReady = get().isArcReady;
            if (isArcReady) {
              arcUnityRef.current?.sendMessage(
                ArcObject.ClientApi,
                ArcEvent.SendCommand,
                constructArcMessage(OutgoingArcMessage.ResumeAudio)
              );
            }
          },
          sendAudioStop: () => {
            const isArcReady = get().isArcReady;
            if (isArcReady) {
              arcUnityRef.current?.sendMessage(
                ArcObject.AudioManager,
                ArcEvent.SendCommand,
                constructArcMessage(OutgoingArcMessage.StopAudio)
              );
            }
          },
        }),
        { anonymousActionType: 'Anonymous Action', enabled: true, name: 'test' }
      )
    );
  }

  const { audioSampleRate, setAudioContextReady } = storeRef.current.getState();
  const [audioValue] = usePermissionAudioValue();
  useEffect(() => {
    if (!audioSampleRate) return;
    audioContextRef.current = new (window.AudioContext ||
      (window as unknown as { webkitAudioContext: typeof AudioContext })
        .webkitAudioContext)({ sampleRate: audioSampleRate });
    setAudioContextReady(true);
  }, [audioSampleRate]);

  gainNodeRef.current = audioContextRef?.current?.createGain();

  useEffect(() => {
    if (!audioContextRef.current || !gainNodeRef.current) return;
    gainNodeRef.current.gain.value = audioValue ? 1 : 0;
    gainNodeRef.current.connect(audioContextRef.current.destination);
  }, [audioValue]);

  useEffect(() => {
    if (!audioContextRef.current || !gainNodeRef.current) return;
    if (audioValue) {
      gainNodeRef.current.gain.setValueAtTime(
        1,
        audioContextRef.current.currentTime
      );
    } else {
      gainNodeRef.current.gain.setValueAtTime(
        0,
        audioContextRef.current.currentTime
      );
    }
  }, [audioValue, gainNodeRef]);

  return (
    <StoreContext.Provider
      value={{
        store: storeRef.current,
        refs: {
          arcUnityRef,
          audioBufferQueueRef,
          transcriptQueueRef,
          audioBufferSourceNodeRef,
          audioContextRef,
          audioStreamRef,
          gainNodeRef,
        },
      }}
    >
      {children}
    </StoreContext.Provider>
  );
};

export const useAppStore = () => {
  const storeContext = useContext(StoreContext);
  if (!storeContext?.store || storeContext.store === null) {
    throw new Error('Missing StoreProvider');
  }
  return useStore(storeContext.store);
};

export const useRefStore = () => {
  const storeContext = useContext(StoreContext);
  if (!storeContext?.refs || storeContext.refs === null) {
    throw new Error('Missing StoreProvider');
  }
  return storeContext.refs;
};
