import { UnityContextHook } from 'react-unity-webgl/distribution/types/unity-context-hook';
import { PersonaMessage } from '../../api';
import { LOCALE } from '../constants';

export type ModeEnum = Record<string, string>;

export interface StoreState {
  // Client State
  isClientPaused: boolean;
  getIsClientReady: () => boolean;
  hasSentIntro: boolean;
  isTranscript: boolean;
  isAudio: boolean;
  lastMessage: string;
  errorMessage: string;
  userTranscript: string;
  isMicrophoneEnabled: boolean;
  isConversationEnded: boolean;
  isArcReady: boolean;
  isChatReady: boolean;
  isInteractible: boolean;
  isAudioBeingHandled: boolean;
  isMultimodalityBeingHandled: boolean;
  isAudioBeingReceived: boolean;
  getShouldClientSendIntro: () => boolean;
  isUnityLoaded: boolean;
  mode: string;
  modeEnum: ModeEnum;
  isSceneLoaded: boolean;
  persona?: PersonaMessage;
  progress: number;
  isApplicationReady: boolean;
  sessionEnded: boolean;
  isDebugMode: boolean;
  testVolume: number;

  // Multimodality State
  isMultimodalityOpen: boolean;
  multimodalityContent: string | null;

  // Audio State
  locale: LOCALE;
  audioSampleRate?: number;
  isAudioContextReady: boolean;
}

export interface ExternalStoreRefs {
  // Refs
  arcUnityRef: React.MutableRefObject<UnityContextHook | null>;
  audioContextRef: React.MutableRefObject<AudioContext | undefined>;
  gainNodeRef: React.MutableRefObject<GainNode | undefined>;
  audioBufferSourceNodeRef: React.MutableRefObject<
    AudioBufferSourceNode | undefined
  >;
  audioBufferQueueRef: React.MutableRefObject<(ArrayBufferLike | string)[]>;
  transcriptQueueRef: React.MutableRefObject<string[]>;
  audioStreamRef: React.MutableRefObject<MediaStream | undefined>;
}

export interface StoreActions {
  // Client State Actions
  setHasSentIntro: (value: boolean) => void;
  setIsClientPaused: (value: boolean) => void;
  setIsTranscript: (value: boolean) => void;
  setIsConversationEnded: (value: boolean) => void;
  setIsArcReady: (value: boolean) => void;
  setIsChatReady: (value: boolean) => void;
  setIsInteractible: (value: boolean) => void;
  disableMicrophone: () => void;
  enableMicrophone: () => void;
  disableAudio: () => void;
  enableAudio: () => void;
  setUserTranscript: (transcript: string) => void;
  setIsAudioBeingHandled: (value: boolean) => void;
  setIsAudioBeingReceived: (value: boolean) => void;
  setIsUnityLoaded: (value: boolean) => void;
  setMode: (mode: string) => void;
  setIsSceneLoaded: (value: boolean) => void;
  setPersona: (persona: PersonaMessage) => void;
  setProgress: (progress: number) => void;
  setIsApplicationReady: (value: boolean) => void;
  setSessionEnded: (value: boolean) => void;
  setIsDebugMode: (value: boolean) => void;
  setTestVolume: (volume: number) => void;
  setLastMessage: (message: string) => void;
  setErrorMessage: (message: string) => void;

  // Multimodality Actions
  toggleMultimodality: () => void;
  setMultimodalityContent: (content: string) => void;
  openMultimodality: () => void;
  closeMultimodality: () => void;
  setIsMultimodalityBeingHandled: (value: boolean) => void;

  // Audio Actions
  setLocale: (locale: LOCALE) => void;
  setAudioSampleRate: (sampleRate: number) => void;
  startMicrophone: () => Promise<void>;
  stopMicrophone: () => void;
  setAudioContextReady: (value: boolean) => void;
  getIsMicrophone: () => boolean;
  processAudio: () => void;
  processAvatarAudioSpeechItem: () => void;
  processChatAudio: () => void;
}

export interface ArcActions {
  sendTurnEnd: () => void;
  sendAudioEnd: () => void;
  sendAudioBlock: (buffer: string) => void;
  sendAudioSpeechItem: (buffer: string) => void;
  sendAudioPause: () => void;
  sendAudioResume: () => void;
  sendAudioStop: () => void;
}
export type StoreType = StoreState & StoreActions & ArcActions;
