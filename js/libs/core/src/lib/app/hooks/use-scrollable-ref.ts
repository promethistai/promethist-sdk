import { createRef, useEffect } from 'react';

export const useScrollableRef = <T>(dependencyArray: T) => {
  const containerRef = createRef<HTMLDivElement>();

  useEffect(() => {
    if (containerRef && containerRef.current) {
      const element = containerRef.current;
      element.scroll({
        top: element.scrollHeight,
        left: 0,
        behavior: 'smooth',
      });
    }
  }, [containerRef, dependencyArray]);

  return containerRef;
};
