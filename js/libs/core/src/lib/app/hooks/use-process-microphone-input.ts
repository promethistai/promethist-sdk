import { useEffect, useRef } from 'react';
import { int16ToBase64 } from '../../utils/utils';
import { useAppStore, useRefStore } from '../contexts';
import { SendMessage } from './types';

export type UseProcessMicrophoneInputProps = {
  sendMessage: SendMessage;
};

export const useProcessMicrophoneInput = ({
  sendMessage,
}: UseProcessMicrophoneInputProps) => {
  const {
    getIsMicrophone,
    getIsClientReady,
    setTestVolume,
    audioSampleRate,
    isAudioContextReady,
    startMicrophone,
    stopMicrophone,
  } = useAppStore();
  const isMicrophone = getIsMicrophone();
  const isClientReady = getIsClientReady();

  const { audioStreamRef, audioContextRef } = useRefStore();

  const analyserRef = useRef<AnalyserNode | null>(null);
  const bufferLengthRef = useRef(0);
  const dataArrayRef = useRef<Uint8Array>(new Uint8Array(0));

  useEffect(() => {
    if (!audioContextRef?.current || !audioSampleRate || !isAudioContextReady)
      return;

    startMicrophone();

    // Ensure analyser is created in the correct context
    if (!analyserRef.current) {
      analyserRef.current = audioContextRef.current.createAnalyser();
      analyserRef.current.fftSize = 64;
      bufferLengthRef.current = analyserRef.current.frequencyBinCount;
      dataArrayRef.current = new Uint8Array(bufferLengthRef.current);
    }

    return () => {
      stopMicrophone();
    };
  }, [
    audioContextRef,
    startMicrophone,
    stopMicrophone,
    audioSampleRate,
    isAudioContextReady,
  ]);

  useEffect(() => {
    if (!audioStreamRef?.current) return;
    if (!audioContextRef?.current) return;
    if (!analyserRef.current) return;

    const inputPoint = audioContextRef.current.createGain();
    const source = audioContextRef.current.createMediaStreamSource(
      audioStreamRef.current
    );

    audioContextRef?.current?.resume();

    const processor = audioContextRef.current.createScriptProcessor(
      16384,
      1,
      1
    );

    // Connect all nodes within the same context
    source.connect(analyserRef.current);
    source.connect(processor);
    processor.connect(audioContextRef.current.destination);

    processor.onaudioprocess = onaudioprocess;

    return () => {
      // Cleanup to avoid connecting to wrong contexts on re-renders
      inputPoint.disconnect();
      source.disconnect();
      processor.disconnect();
    };
  }, [isClientReady, isMicrophone, audioContextRef, audioStreamRef]);

  const onaudioprocess = (e: AudioProcessingEvent) => {
    const floatSamples = e.inputBuffer.getChannelData(0);
    // INFO: This is black magic taken from flow storm. DO NOT TOUCH IT!
    const intarr = Int16Array.from(
      floatSamples.map((n) => {
        const v = n < 0 ? n * 32768 : n * 32767;
        return Math.max(-32768, Math.min(32768, v));
      })
    );
    if (isMicrophone) {
      sendMessage(`#binary:${int16ToBase64(intarr)}`);
      //create average of the samples
      const sum = intarr.reduce((acc, cur) => acc + cur, 0);
      const avg = sum / intarr.length;
      setTestVolume(Number(avg.toPrecision(3)));
    }
  };

  return {
    audioStream: audioStreamRef?.current,
    analyzerData: {
      analyzer: analyserRef.current ?? null,
      bufferLength: bufferLengthRef.current,
      dataArray: dataArrayRef.current,
    },
  };
};
