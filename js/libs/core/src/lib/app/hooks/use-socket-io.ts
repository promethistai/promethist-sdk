import { notification } from 'antd';
import { useEffect, useRef } from 'react';
import { io, Socket } from 'socket.io-client';
import {
  constructEngineConnectionString,
  UseEngineConnectionFactoryProps,
} from '../../api';
import { useAppStore } from '../contexts';
import { WebSocketMessage } from '../hooks/types';

export const useSocketIO = (
  props: UseEngineConnectionFactoryProps,
  onMessage: (message: WebSocketMessage) => void
) => {
  const { isDebugMode } = useAppStore();
  const engineConnectionString = constructEngineConnectionString({
    ...props,
  });

  const socket = useRef<Socket | null>(null);

  useEffect(() => {
    socket.current = io(engineConnectionString, {
      transports: ['websocket', 'webtransport'],
      query: {
        // sid: sid.current,
        // sessionId: sid.current,
        key: props.applicationValue,
        sttSampleRate: props.sttSampleRate,
        outputMedia: props.outputMedia,
        interimTranscripts: props.interimTranscripts,
        locale: props.locale,
      },
    });

    socket.current.connect();

    socket.current.on('connect', () => {
      if (isDebugMode) {
        notification.success({
          message: 'Socket Connected!',
        });
      }
    });

    socket.current.on('disconnect', (reason) => {
      if (isDebugMode) {
        notification.warning({
          message: 'Socket disconnected!',
          description: reason,
          duration: 0,
        });
      }
    });

    socket.current.on('connect_error', (err) => {
      if (isDebugMode) {
        console.error('Connection error:', err);
        notification.error({
          message: err.message,
          description: err.stack,
          duration: 0,
        });
      }
    });

    socket.current.on('text', (message: WebSocketMessage) => {
      if (isDebugMode) {
        console.log('Incoming data from pipeline:', message.toString());
      }
      onMessage(message);
    });

    return () => {
      socket.current?.disconnect();
    };
  }, [
    engineConnectionString,
    props.locale,
    props.sttSampleRate,
    props.outputMedia,
    props.interimTranscripts,
    isDebugMode,
  ]);

  const sendMessage = (message: WebSocketMessage) => {
    if (isDebugMode) console.log('Sending message:', message, { socket });
    socket.current?.emit('text', message);
  };

  const closeSocket = () => {
    if (isDebugMode) {
      console.error('Closing socket');
      notification.warning({
        message: 'Closing socket by end of session',
      });
    }
    socket.current?.disconnect();
  };

  return {
    isConnected: socket.current?.connected || false,
    sendMessage,
    closeSocket,
  };
};
