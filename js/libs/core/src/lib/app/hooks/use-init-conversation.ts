import { useEffect } from 'react';
import { useAppStore } from '../contexts';
import { RawMessageType } from '../pages';

type UseInitConversationProps = {
  messages: RawMessageType[];
  sendIntro: () => void;
};

export const useInitConversation = (props: UseInitConversationProps) => {
  const { getShouldClientSendIntro, setHasSentIntro } = useAppStore();
  const shouldClientSendIntro = getShouldClientSendIntro();
  useEffect(() => {
    if (!shouldClientSendIntro) return;
    if (props.messages.length === 0) {
      console.log('sendIntro');
      setHasSentIntro(true);
      props.sendIntro();
    }
  }, [props, shouldClientSendIntro]);
};
