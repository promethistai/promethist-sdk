import { useState } from 'react';

export type UseOpenHandlerReturn = {
  open: boolean;
  show: () => void;
  close: () => void;
};

export const useOpenHandler = (): UseOpenHandlerReturn => {
  const [open, setOpen] = useState(false);

  const show = () => {
    setOpen(true);
  };

  const close = () => {
    setOpen(false);
  };

  return { open, show, close };
};
