import { useLocalStorageState } from 'ahooks';
import { LOCAL_STORAGE_PERMISSION_MIC } from '../../../api/constants';

export const usePermissionMicValue = () => {
  return useLocalStorageState<boolean>(LOCAL_STORAGE_PERMISSION_MIC, {
    defaultValue: false,
    listenStorageChange: true,
  });
};
