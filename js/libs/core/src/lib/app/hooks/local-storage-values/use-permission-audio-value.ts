import { useLocalStorageState } from 'ahooks';
import { LOCAL_STORAGE_PERMISSION_AUDIO } from '../../../api/constants';

export const usePermissionAudioValue = () => {
  return useLocalStorageState<boolean>(LOCAL_STORAGE_PERMISSION_AUDIO, {
    defaultValue: true,
    listenStorageChange: true,
  });
};
