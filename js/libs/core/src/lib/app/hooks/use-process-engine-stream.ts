import { useRef, useState } from 'react';
import {
  ensureIsCommand,
  ensureIsCommandError,
  ensureIsCommandExit,
  ensureIsCommandOutputItem,
  ensureIsCommandPersona,
  ensureIsCommandSelection,
  ensureIsCommandSessionShouldEnd,
  ensureIsCommandSpeechItem,
  ensureIsCommandTranscript,
  isPersonaEquals,
  sanitizeErrorMessage,
  sanitizePersonaMessage,
  sanitizeTranscriptMessage,
} from '../../api/protocol-logic';
import { useAppStore, useRefStore } from '../contexts';
import { RawMessageType } from '../pages';
import { WebSocketMessage } from './types';

export const useProcessEngineDataStream = () => {
  const {
    disableMicrophone,
    setIsAudioBeingReceived,
    isChatReady,
    persona,
    setPersona,
    setIsInteractible,
    isAudioBeingHandled,
    setIsConversationEnded,
    setMultimodalityContent,
    openMultimodality,
    processAudio,
    setErrorMessage,
    setUserTranscript,
    userTranscript,
    isDebugMode,
  } = useAppStore();
  const { audioBufferQueueRef, transcriptQueueRef } = useRefStore();

  // const { open, setContent } = useMultimodalityContext();

  const [rawMessageList, setRawMessageList] = useState<RawMessageType[]>([]);

  const tokenizedMessageRef = useRef('');

  const handleMessage = (message: WebSocketMessage) => {
    if (!message) return;
    if (!audioBufferQueueRef?.current) return;

    const isString = typeof message === 'string';

    if (!isString) return;

    const isCommand = ensureIsCommand(message);

    if (isCommand) {
      // if (ensureIsCommandLocale(message)) {
      //   setIsInteractible(false);
      //   if (isDebugMode) console.log('LOCALE Command, disabling microphone');
      //   if (isChatReady) setIsAudioBeingReceived(true);
      //   else disableMicrophone();
      // }
      if (ensureIsCommandSessionShouldEnd(message)) {
        if (isDebugMode) console.log('Disabling microphone, session ended');
        disableMicrophone();
        setIsConversationEnded(true);
        setIsInteractible(false);
      }
      if (ensureIsCommandPersona(message)) {
        if (isDebugMode)
          console.log('Persona message:', {
            ...sanitizePersonaMessage(message),
          });
        const newPersona = sanitizePersonaMessage(message);
        if (!persona || !isPersonaEquals(newPersona, persona)) {
          setPersona(sanitizePersonaMessage(message));
        }
      }
      if (ensureIsCommandSpeechItem(message) || ensureIsCommandExit(message)) {
        audioBufferQueueRef.current.push(message);
        if (
          (!isAudioBeingHandled && audioBufferQueueRef.current.length > 5) ||
          ensureIsCommandExit(message)
        ) {
          if (isDebugMode)
            console.log(
              'Processing audio: ',
              'isAudioBeingHandled',
              isAudioBeingHandled,
              'audioBufferQueueRef.current.length',
              audioBufferQueueRef.current.length,
              'isCommandExit',
              ensureIsCommandExit(message)
            );
          processAudio();
        }
      }
      if (ensureIsCommandOutputItem(message)) {
        // DEPRECATED
        // const sanitizedMessage = sanitizeOutputItemMessage(message);
        // const parsedMessage: MultimodalityObjectWrapper =
        //   JSON.parse(sanitizedMessage);
        // setContent(parsedMessage);
        // open();
        return;
      }
      if (ensureIsCommandError(message)) {
        if (isDebugMode)
          console.log('Error message', sanitizeErrorMessage(message));
        setErrorMessage(sanitizeErrorMessage(message));
        return;
      }
      if (ensureIsCommandSelection(message)) {
        if (isDebugMode) console.log('Command selection: ', message);
        setMultimodalityContent(message);
        openMultimodality();
        return;
      }
      if (ensureIsCommandTranscript(message)) {
        if (isDebugMode) console.log('Command selection: ', message);
        if (message.includes('&isAction')) return;
        if (message.includes('&isInterim=true')) {
          if (
            sanitizeTranscriptMessage(message).length > userTranscript.length
          ) {
            setUserTranscript(sanitizeTranscriptMessage(message));
            return;
          }
        } else {
          disableMicrophone();
          if (isDebugMode)
            console.log('Final Transcript Command, disabling microphone');
          if (isChatReady) setIsAudioBeingReceived(true);
        }
        setUserTranscript(sanitizeTranscriptMessage(message));
        return;
      }
      if (
        ensureIsCommandExit(message) &&
        tokenizedMessageRef.current.length > 0
      ) {
        rawMessageList.push({
          text: tokenizedMessageRef.current,
          date: new Date(),
        });
        setRawMessageList([...rawMessageList]);
        if (/[.!?][^ \n]/.test(tokenizedMessageRef.current)) {
          tokenizedMessageRef.current = '';
        }
      }
      rawMessageList.push({
        text: message.toString(),
        date: new Date(),
      });
      setRawMessageList([...rawMessageList]);
    } else {
      if (/[.!?][^ \n]/.test(tokenizedMessageRef.current)) {
        tokenizedMessageRef.current += ' ';
      }

      tokenizedMessageRef.current += message;
      transcriptQueueRef.current.push(message);
    }
    return;
  };

  const resetMessages = () => {
    setRawMessageList([]);
  };

  return {
    handleMessage,
    resetMessages,
    messages: rawMessageList,
    lastMessage: tokenizedMessageRef.current,
  };
};
