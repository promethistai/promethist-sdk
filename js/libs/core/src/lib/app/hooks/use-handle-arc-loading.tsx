import { useEffect, useState } from 'react';
import { useUnityContext } from 'react-unity-webgl';
import {
  ensureIsCommandAudioEnd,
  ensureIsCommandExit,
  ensureIsCommandSpeechItem,
  sanitizeSpeechItemMessage,
} from '../../api';
import { arrayBufferToBase64 } from '../../utils/utils';
import { useAppStore, useRefStore } from '../contexts';
import {
  ArcEvent,
  ArcObject,
  IncomingArcMessage,
  OutgoingArcMessage,
} from '../pages';

export const useHandleArcLoading = (
  arcRepositoryUrl: string,
  arcVersion: string,
  isDefault: boolean
) => {
  const {
    setIsArcReady,
    setIsUnityLoaded,
    setIsSceneLoaded,
    setProgress,
    isSceneLoaded,
    setIsAudioBeingHandled,
    setIsMultimodalityBeingHandled,
    isArcReady,
    isClientPaused,
    persona,
    enableMicrophone,
    disableMicrophone,
    setIsTranscript,
    isApplicationReady,
    sendAudioSpeechItem,
    sendAudioBlock,
    sendAudioEnd,
    sendTurnEnd,
    setLastMessage,
    setUserTranscript,
    isDebugMode,
  } = useAppStore();
  const { arcUnityRef, audioBufferQueueRef, transcriptQueueRef } =
    useRefStore();

  arcUnityRef.current = useUnityContext({
    loaderUrl: arcRepositoryUrl + arcVersion + '.loader.js',
    dataUrl: arcRepositoryUrl + arcVersion + '.data.unityweb',
    frameworkUrl: arcRepositoryUrl + arcVersion + '.framework.js.unityweb',
    codeUrl: arcRepositoryUrl + arcVersion + '.wasm.unityweb',
    streamingAssetsUrl: 'StreamingAssets',
    companyName: 'DefaultCompany',
    productName: 'My project',
    productVersion: '1.0',
  });
  const [isInitialAudioQueueEmpty, setIsInitialAudioQueueEmpty] =
    useState(false);
  const emptyAudioQueue = () => {
    const audioBlock = audioBufferQueueRef?.current.shift();
    if (!audioBlock) return;
    if (typeof audioBlock === 'string') {
      ensureIsCommandSpeechItem(audioBlock) &&
        sendAudioSpeechItem(sanitizeSpeechItemMessage(audioBlock));
      ensureIsCommandAudioEnd(audioBlock) && sendAudioEnd();
      ensureIsCommandExit(audioBlock) && sendTurnEnd();
    } else sendAudioBlock(arrayBufferToBase64(audioBlock));
    if (audioBufferQueueRef?.current.length === 0) {
      setIsInitialAudioQueueEmpty(true);
    } else {
      emptyAudioQueue();
    }
  };

  useEffect(() => {
    if (isInitialAudioQueueEmpty || !isArcReady || !isApplicationReady) return;
    if (isDebugMode)
      console.log(
        'Emptying audio queue',
        isInitialAudioQueueEmpty,
        'isArcReady',
        isArcReady,
        'isApplicationReady',
        isApplicationReady
      );
    emptyAudioQueue();
  }, [isInitialAudioQueueEmpty, isArcReady, isApplicationReady]);

  useEffect(() => {
    setIsUnityLoaded(arcUnityRef?.current?.isLoaded ?? false);
  }, [arcUnityRef, arcUnityRef?.current?.isLoaded]);

  useEffect(() => {
    if (!arcUnityRef) return;
    arcUnityRef.current?.addEventListener(
      ArcEvent.MessageReceived,
      (message) => {
        const decodedMessage = decodeMotherfucknigJsonShitFromArc(
          message?.toString()
        );
        if (!decodedMessage) return;
        if (typeof decodedMessage === 'string') {
          if (decodedMessage === IncomingArcMessage.SceneLoaded) {
            if (isDebugMode) console.log('ARC Scene loaded');
            setIsSceneLoaded(true);
          }
          if (decodedMessage === IncomingArcMessage.AvatarLoaded) {
            if (isDebugMode) console.log('ARC Avatar loaded');
            setIsArcReady(true);
          }
          if (decodedMessage.startsWith(IncomingArcMessage.DownloadProgress)) {
            const progress = Number(
              decodedMessage.split(';')[1].split(':')[1].replace(',', '.')
            );
            setProgress(progress);
          }
          if (decodedMessage === IncomingArcMessage.DownloadEnded) {
            setProgress(100);
          }
        } else {
          if (decodedMessage.type === IncomingArcMessage.TurnEnded) {
            if (isDebugMode) console.log('ARC Turn ended');
            setIsAudioBeingHandled(false);
            setIsMultimodalityBeingHandled(false);
            setIsTranscript(false);
            if (!isClientPaused) {
              if (isDebugMode) console.log('Enabling microphone');
              enableMicrophone();
            }
          }
          if (decodedMessage.type === IncomingArcMessage.AudioStarted) {
            if (isDebugMode) console.log('ARC Audio started');
            disableMicrophone();
            const message = transcriptQueueRef.current.shift();
            if (isDebugMode) console.log('Popping transcript queue', message);
            if (message) {
              setLastMessage(message);
              setUserTranscript('');
            }
            setIsAudioBeingHandled(true);
          }
        }
      }
    );

    return () => {
      // eslint-disable-next-line @typescript-eslint/no-empty-function
      arcUnityRef.current?.removeEventListener(
        ArcEvent.MessageReceived,
        () => undefined
      );
    };
  }, [arcUnityRef?.current?.isLoaded]);

  useEffect(() => {
    if (isDefault && isSceneLoaded) {
      const timeoutId = setTimeout(() => {
        if (isDebugMode) console.log('Setting default persona');
        arcUnityRef?.current?.sendMessage(
          ArcObject.PersonaManager,
          ArcEvent.SetPersona,
          'jiri;false'
        );
      }, 200);

      return () => clearTimeout(timeoutId);
    }
    if (persona && isSceneLoaded) {
      const timeoutId = setTimeout(() => {
        if (isDebugMode) console.log('Setting persona', persona.avatarRef);
        arcUnityRef?.current?.sendMessage(
          ArcObject.PersonaManager,
          ArcEvent.SetPersona,
          persona.avatarRef + ';false'
        );
      }, 200);

      return () => clearTimeout(timeoutId);
    }
    return;
  }, [isSceneLoaded, isDefault, persona?.avatarRef]);

  useEffect(() => {
    return () => {
      setIsSceneLoaded(false);
      setIsArcReady(false);
    };
  }, []);
};

export const constructArcMessage = (
  type: OutgoingArcMessage,
  data?: string
): string => {
  return `{ "type": "${type}", "parameters": ${data || '{}'} }`;
};
type ArcMessage = {
  type: IncomingArcMessage;
  data: string;
};
export const decodeMotherfucknigJsonShitFromArc = (
  message?: string
): ArcMessage | string | null => {
  if (!message) return null;
  try {
    return JSON.parse(message) as ArcMessage;
  } catch (e) {
    return message;
  }
};
