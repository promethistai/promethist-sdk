import { useEffect } from 'react';
import { useRefStore } from '../contexts';

export type UseHandleAudioStateProps = {
  onOpenRef: React.MutableRefObject<() => void>;
  onCloseRef: React.MutableRefObject<() => void>;
  audioContextRef: React.MutableRefObject<AudioContext | undefined> | undefined;
  audioBufferSourceNodeRef:
    | React.MutableRefObject<AudioBufferSourceNode | undefined>
    | undefined;
};

export const useHandleAudioState = ({
  audioBufferSourceNodeRef,
  audioContextRef,
  onCloseRef,
  onOpenRef,
}: UseHandleAudioStateProps) => {
  const { arcUnityRef } = useRefStore();

  useEffect(() => {
    onOpenRef.current = () => {
      audioContextRef?.current?.resume();
    };

    onCloseRef.current = () => {
      try {
        arcUnityRef?.current?.UNSAFE__unityInstance?.Quit();
        audioContextRef?.current?.suspend();
        audioBufferSourceNodeRef?.current?.stop();
      } catch (error) {
        console.error(
          '[useHandleAudioState] Error while trying to close the app',
          error
        );
      }
    };
  }, []);
};
