import { RawMessageType } from '../app';
import { ENGINE_CONNECTION_LIST } from './constants';
import { PersonaMessage } from './use-engine-connection-factory';

export function constructEngineConnectionString({
  engineConnectionValue,
}: {
  engineConnectionValue: string | undefined;
  applicationValue: string | undefined;
  locale?: string;
  outputMedia?: string;
  interimTranscripts?: boolean;
  sttSampleRate?: number;
}) {
  let connectionUrl = '' + engineConnectionValue || ENGINE_CONNECTION_LIST[0];
  if (!engineConnectionValue?.endsWith('/')) connectionUrl += '/';
  connectionUrl += 'socket/io/pipeline';
  return connectionUrl;
}

export const ensureIsCommand = (message: string | null) =>
  String(message).startsWith('#');
export const ensureIsCommandTranscript = (message: string | null) =>
  String(message).startsWith('#transcript');
export const ensureIsCommandExit = (message: string | null) =>
  String(message).startsWith('#exit');
export const ensureIsCommandIntro = (message: string | null) =>
  String(message).startsWith('#intro');
export const ensureIsCommandMemory = (message: string | null) =>
  String(message).startsWith('#memory');
export const ensureIsCommandSessionShouldEnd = (message: string | null) =>
  String(message).startsWith('#sessionShouldEnd');
export const ensureIsCommandOutputItem = (message: string | null) =>
  String(message).startsWith('#output-item');
export const ensureIsCommandPersona = (message: string | null) => {
  return String(message).startsWith('#persona');
};
export const ensureIsCommandLocale = (message: string | null) =>
  String(message).startsWith('#locale');
export const ensureIsCommandAudioEnd = (message: string | null) => {
  return String(message).startsWith('#audio-end');
};
export const ensureIsCommandSpeechItem = (message: string | null) => {
  return String(message).startsWith('#speech-item');
};
export const ensureIsCommandSelection = (message: string | null) => {
  return String(message).startsWith('#selection');
};
export const ensureIsCommandError = (message: string | null) => {
  return String(message).startsWith('#error');
};

export const ensureCorrectEndingOfUserInput = (
  userInputString: string
): string =>
  userInputString.startsWith('#') ||
  userInputString.endsWith('.') ||
  userInputString.endsWith('?') ||
  userInputString.endsWith('!')
    ? userInputString
    : userInputString + '.';

export const sanitizeOutputItemMessage = (outputItem: string) =>
  outputItem.replace('#output-item:', '');
export const sanitizeSpeechItemMessage = (speechItem: string) =>
  speechItem.replace('#speech-item:', '');
export const sanizizeSelectionMessage = (selection: string) =>
  selection.replace('#selection:', '');
export const sanitizeTranscriptMessage = (transcript: string) =>
  transcript.replace('#transcript:text=', '').replace(new RegExp(`&.*`), '');

export const sanitizeMessageList = (
  messageList: RawMessageType[]
): RawMessageType[] =>
  messageList.filter(
    (it) =>
      (ensureIsCommandTranscript(it.text) && !ensureIsCommandIntro(it.text)) ||
      !ensureIsCommand(it.text)
  );

export const sanitizeErrorMessage = (errorMessage: string) =>
  errorMessage.replace('#error:', '');
export const sanitizePersonaMessage = (personaMessage: string) => {
  const message = personaMessage
    .replace('#persona:', '')
    .split('&')
    .map((it) => it.split('='));

  const persona: PersonaMessage = {
    ref: message[0][1],
    name: message[1][1],
    avatarRef: message[2][1],
  };
  return persona;
};

export const isPersonaEquals = (
  persona1: PersonaMessage,
  persona2: PersonaMessage
) => {
  return (
    persona1.ref === persona2.ref &&
    persona1.name === persona2.name &&
    persona1.avatarRef === persona2.avatarRef
  );
};
