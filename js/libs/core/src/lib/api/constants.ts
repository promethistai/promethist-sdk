export const ENGINE_CONNECTION_URL =
  'ws://192.168.0.197:9010/socket/pipeline/default?locale=en-US&outputMedia=wav&sttSampleRate=48000';

export const ENGINE_CONNECTION_LIST = [
  'ws://localhost:9011',
  'wss://engine.promethist.dev',
  'wss://engine.promethist.ai',
];
export const APPLICATION_LIST = ['default', 'persona:tmobile-1'];

export enum ReadyState {
  UNINSTANTIATED = -1,
  CONNECTING = 0,
  OPEN = 1,
  CLOSING = 2,
  CLOSED = 3,
}

export const CONNECTION_STATUS_ENUM = {
  [ReadyState.CONNECTING]: 'Connecting',
  [ReadyState.OPEN]: 'Open',
  [ReadyState.CLOSING]: 'Closing',
  [ReadyState.CLOSED]: 'Closed',
  [ReadyState.UNINSTANTIATED]: 'Uninstantiated',
};

export const MAGIC_PACKET_END_OF_AUDIO_STREAM = (() => {
  const _arr = new Uint8Array(4);
  _arr.set([69, 79, 66, 46]);
  return _arr.buffer;
})();

const LS_PREFIX = 'promethist-client-';
export const LOCAL_STORAGE_ENGINE_CONNECTION = LS_PREFIX + 'engine-connection';
export const LOCAL_STORAGE_ENGINE_CONNECTION_LIST =
  LS_PREFIX + 'engine-connection-list';

export const LOCAL_STORAGE_APPLICATION = LS_PREFIX + 'application';
export const LOCAL_STORAGE_APPLICATION_LIST = LS_PREFIX + 'application-list';
export const LOCAL_STORAGE_PERMISSION_MIC = LS_PREFIX + 'permission-microphone';
export const LOCAL_STORAGE_MIC_READING = LS_PREFIX + 'microphone-reading';
export const LOCAL_STORAGE_PERMISSION_AUDIO = LS_PREFIX + 'permission-audio';
