import { SendMessage } from '../app/hooks/types';
import { useProcessEngineDataStream } from '../app/hooks/use-process-engine-stream';
import { useSocketIO } from '../app/hooks/use-socket-io';
import { RawMessageType } from '../app/pages/raw-message-output-tab';

export interface UseEngineConnectionFactory {
  isConnected: boolean;
  sendMessage: SendMessage;
  messages: RawMessageType[];
  lastMessage: string;
  resetMessages: () => void;
  closeSocket: () => void;
}

export interface UseEngineConnectionFactoryProps {
  engineConnectionValue: string | undefined;
  applicationValue: string | undefined;
  locale?: string;
  sttSampleRate?: number;
  outputMedia?: string;
  interimTranscripts?: boolean;
}

export type PersonaMessage = {
  ref: string;
  name: string;
  avatarRef: string;
};

export function useEngineConnectionFactory({
  applicationValue,
  engineConnectionValue,
  interimTranscripts = false,
  locale = 'cs-CZ',
  outputMedia = 'pcm',
  sttSampleRate = 24000,
}: UseEngineConnectionFactoryProps): UseEngineConnectionFactory {
  const { lastMessage, messages, resetMessages, handleMessage } =
    useProcessEngineDataStream();
  const { isConnected, sendMessage, closeSocket } = useSocketIO(
    {
      applicationValue,
      engineConnectionValue,
      interimTranscripts,
      locale,
      outputMedia,
      sttSampleRate,
    },
    handleMessage
  );

  return {
    isConnected,
    messages,
    lastMessage,
    sendMessage,
    resetMessages,
    closeSocket,
  };
}
