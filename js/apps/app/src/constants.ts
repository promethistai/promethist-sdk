import { DEFAULT_CLIENT_MODES } from '@js/core';

export const CLIENT_APP_MODES = {
  ...DEFAULT_CLIENT_MODES,
  qr: 'qr',
  output: 'output',
};
