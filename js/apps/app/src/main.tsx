import '@fontsource/inter';
import { ConfigProvider } from 'antd';
import * as ReactDOM from 'react-dom/client';
import { RouterProvider } from 'react-router-dom';
import { StoreProvider } from '../../../libs/core/src';
import { router } from './app/router';
import './styles.css';

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);
root.render(
  <ConfigProvider
    theme={{
      token: {
        colorPrimary: '#1479F9',
      },
      components: {
        Typography: {
          colorTextSecondary: '#5C5E69',
          colorText: '#1F1F23',
          fontFamily: 'Inter',
          fontSize: 22,
          fontSizeLG: 40,
          fontWeightStrong: 400,
        },
      },
    }}
  >
    <StoreProvider defaultStore={{}}>
      <RouterProvider router={router} />
    </StoreProvider>
  </ConfigProvider>
);
