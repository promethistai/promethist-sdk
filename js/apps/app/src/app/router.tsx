import { createBrowserRouter } from 'react-router-dom';
import { NoParamsRedirect } from './components/no-params-redirect';
import { WebClient } from './components/web-client';

export const router = createBrowserRouter([
  {
    path: '/',
    element: <NoParamsRedirect />,
    id: 'app',
  },
  {
    path: '/client/:applicationValue',
    element: <WebClient />,
    id: 'web-client',
  },
  {
    path: '*',
    element: <NoParamsRedirect />,
  },
]);
