import Icon, { LeftOutlined, RightOutlined } from '@ant-design/icons';
import { ArrowDown, GreenSoundWaveIcon, LargeText, SmallText } from '@js/core';
import { useKeyPress } from 'ahooks';
import { ButtonProps, Carousel, Space } from 'antd';
import { CarouselRef } from 'antd/es/carousel';
import { useRef } from 'react';
import styled from 'styled-components';
import { MotionButton } from './components/motion-button';
type CarouselPhaseProps = {
  nextAction?: () => void;
  prevAction?: () => void;
};

export const ColoredCircle = ({
  size = 285,
  color = 'rgba(29, 61, 224, 0.10)',
  filter = '50px',
  top = '75%',
  left = 0,
}: {
  size?: number;
  color?: React.CSSProperties['backgroundColor'];
  filter?: React.CSSProperties['filter'];
  top?: React.CSSProperties['top'];
  left?: React.CSSProperties['left'];
}) => {
  return (
    <div
      style={{
        top: top,
        left: left,
        position: 'fixed',
        filter: `blur(${filter})`,
        width: size,
        height: size,
        borderRadius: size / 2,
        backgroundColor: color,
      }}
    ></div>
  );
};

export const PhaseOne: React.FC<CarouselPhaseProps> = ({ nextAction }) => {
  return (
    <CarouselContent>
      <ContentContainer>
        <Space
          direction="vertical"
          size="large"
          style={{ paddingLeft: 25, paddingRight: 25 }}
        >
          <LargeText>Hra s AI avatarem</LargeText>
          <SmallText>
            Vyzkoušejte si hru s AI avatarem a otestujte, jak jste odolní vůči
            manipulativním technikám. V následujících několika krocích vám
            ukážeme, jak s avatarem správně komunikovat.
          </SmallText>
        </Space>
      </ContentContainer>
      <ButtonContainer style={{ paddingBottom: 20, paddingRight: 20 }}>
        <NextButton onClick={nextAction} />
      </ButtonContainer>
    </CarouselContent>
  );
};

export const PhaseTwo: React.FC<CarouselPhaseProps> = ({
  nextAction,
  prevAction,
}) => {
  return (
    <CarouselContent>
      <ContentContainer>
        <Space
          direction="vertical"
          size="large"
          style={{ paddingLeft: 25, paddingRight: 25 }}
        >
          <LargeText>Svítí zelená, mluvte</LargeText>
          <SmallText>
            Mluvte pouze tehdy, když panel ve spodní části obrazovky svítí
            zeleně.
          </SmallText>
        </Space>
      </ContentContainer>
      <ButtonContainer
        style={{ justifyContent: 'space-around', paddingBottom: 20 }}
      >
        <div
          style={{
            display: 'flex',
            justifyContent: 'space-between',
            paddingLeft: 20,
            paddingRight: 20,
            width: '100%',
          }}
        >
          <PrevButton onClick={prevAction} />
          <Icon
            style={{
              width: 'calc(100vw - 200px)',
              maxWidth: '400px',
              backgroundColor: 'rgba(125, 255, 158, 0.7)',
              mixBlendMode: 'darken',
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
              fontSize: '120px',
              zIndex: 21,
              height: '60px',
              borderRadius: '30px',
              opacity: 0.7,
            }}
            component={GreenSoundWaveIcon}
          />
          <NextButton onClick={nextAction} />
        </div>
      </ButtonContainer>
      <div
        style={{
          position: 'absolute',
          top: '70%',
          left: '50%',
          transform: 'translate(-50%, -50%)',
        }}
      >
        <ArrowDown />
      </div>
    </CarouselContent>
  );
};

export const PhaseThree: React.FC<CarouselPhaseProps> = ({
  nextAction,
  prevAction,
}) => {
  return (
    <CarouselContent>
      <ContentContainer>
        <Space
          direction="vertical"
          size="large"
          style={{ paddingLeft: 25, paddingRight: 25 }}
        >
          <LargeText>Mluvte v kratších větách</LargeText>
          <SmallText>
            Pokud toho chcete říct víc, soustřeďte se, aby nevznikaly výraznější
            pauzy. AI avatar je neumí rozeznat od konců promluvy.
          </SmallText>
        </Space>
      </ContentContainer>
      <ButtonContainer
        style={{ justifyContent: 'space-around', paddingBottom: 20 }}
      >
        <div
          style={{
            display: 'flex',
            justifyContent: 'space-between',
            paddingLeft: 20,
            paddingRight: 20,
            width: '100%',
          }}
        >
          <PrevButton onClick={prevAction} />
          <NextButton onClick={nextAction} />
        </div>
      </ButtonContainer>
    </CarouselContent>
  );
};

export const PrevButton: React.FC<ButtonProps> = ({ onClick }) => {
  return (
    <MotionButton
      onClick={onClick}
      background="rgba(29, 61, 224, 0.20)"
      textColor="#1D3DE0"
    >
      <LeftOutlined />
    </MotionButton>
  );
};

export const NextButton: React.FC<ButtonProps> = ({ onClick }) => {
  return (
    <MotionButton onClick={onClick} background="#1D3DE0" textColor="#fff">
      <RightOutlined />
    </MotionButton>
  );
};

export type OnboardingCarouselProps = {
  onAcceptTerms: () => void;
};

export const OnboardingCarousel: React.FC<OnboardingCarouselProps> = ({
  onAcceptTerms,
}) => {
  const ref = useRef<CarouselRef>(null);
  useKeyPress(39, () => {
    ref?.current?.next();
  });
  useKeyPress(37, () => {
    ref?.current?.prev();
  });
  return (
    <Carousel
      dotPosition="top"
      ref={ref}
      arrows
      infinite={false}
      style={{
        position: 'fixed',
        width: '100%',
        height: '100dvh',
        zIndex: 1000,
      }}
    >
      <div>
        <PhaseOne
          prevAction={() => ref.current?.prev()}
          nextAction={() => ref.current?.next()}
        />
      </div>
      <div>
        <PhaseTwo
          prevAction={() => ref.current?.prev()}
          nextAction={() => ref.current?.next()}
        />
      </div>
      <div>
        <PhaseThree
          prevAction={() => ref.current?.prev()}
          nextAction={() => onAcceptTerms()}
        />
      </div>
    </Carousel>
  );
};

export const CarouselContent = styled.div`
  margin: 0;
  height: 100dvh;
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: space-between; /* Ensures content is spaced evenly */
  align-items: center;
  z-index: 1000;
  background: #fff;
`;

export const ContentContainer = styled.div`
  flex-grow: 1; /* Takes available space */
  display: flex;
  justify-content: center; /* Centers the content vertically */
  align-items: center; /* Centers the content horizontally */
  width: 100%;
`;

export const ButtonContainer = styled.div`
  width: 100%;
  display: flex;
  justify-content: end;
  align-self: flex-end;
`;
