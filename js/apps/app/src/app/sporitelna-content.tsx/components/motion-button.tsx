import { ButtonProps } from 'antd';
import { motion } from 'motion/react';

export const MotionButton: React.FC<
  ButtonProps & { background: string; textColor: string }
> = ({ onClick, background, textColor, children }) => {
  return (
    <motion.div
      onClick={onClick}
      style={{
        width: 60,
        height: 60,
        borderRadius: 30,
        background: background,
        color: textColor,
        backdropFilter: 'blur(17.51532554626465px)',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
      }}
      whileHover={{ scale: 1.1 }}
      whileTap={{ scale: 0.95 }}
    >
      {children}
    </motion.div>
  );
};
