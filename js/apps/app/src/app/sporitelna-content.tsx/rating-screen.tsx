import { LargeText, SmallText, WebSocketMessage } from '@js/core';
import { Form, Radio, Space, Typography } from 'antd';
import TextArea from 'antd/es/input/TextArea';
import { RadioButtonProps } from 'antd/es/radio/radioButton';
import React, { useState } from 'react';
import styled from 'styled-components';
import { CarouselContent, ContentContainer } from './carousel-content';
export type RatingType = {
  score: number;
  note: string;
};
export const RatingScreen = ({
  sendMessage,
  closeSocket,
}: {
  sendMessage: (message: WebSocketMessage) => void;
  closeSocket: () => void;
}) => {
  const [answerSent, setAnswerSent] = useState(false);

  const sendRatingMessage = (data: RatingType) => {
    const dataMessage = {
      type: 'csat',
      subtype: 'conversationWithAvatar',
      revision: 1,
      data: {
        score: data.score,
        ...(data.note && { comment: data.note }),
      },
    };
    sendMessage(`#rating:data=${JSON.stringify(dataMessage)}`);
    closeSocket();
    setAnswerSent(true);
  };
  return (
    <div
      style={{
        position: 'fixed',
        width: '100%',
        height: '100dvh',
        zIndex: 1000,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        overflowY: 'scroll',
      }}
    >
      <CarouselContent
        style={{ maxWidth: 400, paddingLeft: 25, paddingRight: 25 }}
      >
        {!answerSent && (
          <ContentContainer style={{ padding: 25, backgroundColor: 'white' }}>
            <RatingContent onSubmit={sendRatingMessage} />
          </ContentContainer>
        )}
        {answerSent && (
          <ContentContainer>
            <Space direction="vertical" size="large">
              <LargeText centerText>Děkujeme!</LargeText>
              <SmallText centerText>
                Vážíme si vašeho hodnocení a jsme rádi, že jste si našel/la čas
                a vyzkoušel/la si interakci s AI avatarem.
              </SmallText>
            </Space>
          </ContentContainer>
        )}
      </CarouselContent>
    </div>
  );
};

const RatingHeadingText = ({
  children,
  style,
}: {
  children: React.ReactNode;
  style?: React.CSSProperties;
}) => {
  return (
    <Typography.Text
      style={{
        margin: 0,
        color: '#F161E',
        fontFamily: 'Inter',
        fontSize: '18px',
        fontStyle: 'normal',
        fontWeight: 500,
        lineHeight: 'normal',
        ...style,
      }}
    >
      {children}
    </Typography.Text>
  );
};

const RatingText = ({ children }: { children: React.ReactNode }) => {
  return (
    <Typography.Paragraph
      style={{
        margin: 0,
        color: '#384452',
        fontFamily: 'Inter',
        fontSize: '14px',
        fontStyle: 'normal',
        fontWeight: 400,
        lineHeight: 'normal',
      }}
    >
      {children}
    </Typography.Paragraph>
  );
};

type StyledRadioProps = {
  children: React.ReactNode;
} & RadioButtonProps;

const RatingContent = ({
  onSubmit,
}: {
  onSubmit: (data: RatingType) => void;
}) => {
  const StyledRadio: React.FC<StyledRadioProps> = ({ children, ...props }) => {
    return (
      <Radio
        {...props}
        style={{
          borderRadius: 20,
          border: '1px solid #EBEBEB',
          background: '#FFF',
          padding: 10,
          minHeight: '5dvh',
          height: 'auto',
          maxHeight: '150px',
          overflow: 'hidden',
          display: 'flex',
          alignItems: 'center',
          whiteSpace: 'normal',
          wordBreak: 'break-word',
        }}
      >
        {children}
      </Radio>
    );
  };

  return (
    <Form
      onFinish={(values) => {
        onSubmit({ note: values.notes, score: values.rating });
      }}
      layout="vertical"
    >
      <LargeText style={{ fontSize: 40, paddingBottom: 10 }}>
        Jak se vám líbila konverzace s AI avatarem?
      </LargeText>
      <Form.Item
        name="rating"
        rules={[{ required: true, message: 'Prosím vyberte hodnocení' }]}
      >
        <Radio.Group
          style={{ display: 'flex', flexDirection: 'column', gap: '1rem' }}
        >
          <StyledRadio value={5}>
            <span style={{ fontSize: 24 }} role="img" aria-label="love">
              😍
            </span>
            <RatingHeadingText>Skvělá</RatingHeadingText>
          </StyledRadio>
          <StyledRadio value={4}>
            <span style={{ fontSize: 24 }} role="img" aria-label="good">
              🙂
            </span>
            <RatingHeadingText>Dobrá</RatingHeadingText>
          </StyledRadio>
          <StyledRadio value={3}>
            <span style={{ fontSize: 24 }} role="img" aria-label="not good">
              🫤
            </span>
            <RatingHeadingText>Nic moc</RatingHeadingText>
          </StyledRadio>
          <StyledRadio value={2}>
            <span style={{ fontSize: 24 }} role="img" aria-label="dislike">
              🙁
            </span>
            <RatingHeadingText>Nelíbila</RatingHeadingText>
          </StyledRadio>
          <StyledRadio value={1}>
            <span style={{ fontSize: 24 }} role="img" aria-label="very bad">
              😫
            </span>
            <RatingHeadingText>Špatná</RatingHeadingText>
          </StyledRadio>
        </Radio.Group>
      </Form.Item>
      <Form.Item
        label={<RatingText>Chcete ještě něco doplnit?</RatingText>}
        name="notes"
      >
        <TextArea
          style={{ borderRadius: 20, border: '1px solid #EBEBEB' }}
          autoSize={{ minRows: 4, maxRows: 6 }}
        />
      </Form.Item>
      <Form.Item shouldUpdate>
        {({ getFieldValue, submit }) => (
          <StyledButton
            onClick={submit}
            disabled={getFieldValue('rating') === undefined}
          >
            <RatingHeadingText style={{ color: '#fff', fontWeight: 600 }}>
              Odeslat hodnocení
            </RatingHeadingText>
          </StyledButton>
        )}
      </Form.Item>
    </Form>
  );
};

// Styled Components
const StyledButton = styled.button`
  border-radius: 30px;
  height: 60px;
  width: 100%;
  max-width: 400px;
  background: #1d3de0;
  backdrop-filter: blur(17.51532554626465px);
`;
