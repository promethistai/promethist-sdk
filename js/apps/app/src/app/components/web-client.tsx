import { EngineProps, useAppStore } from '@js/core';
import { useEffect } from 'react';
import { useParams, useSearchParams } from 'react-router-dom';
import { TabSwitcher } from './tab-switcher';

export const WebClient = () => {
  const { applicationValue } = useParams();
  const { setIsDebugMode } = useAppStore();
  const [searchParams] = useSearchParams();

  const locale = searchParams.get('locale');
  const outputMedia = searchParams.get('outputMedia');
  const sttSampleRate = searchParams.get('sttSampleRate');
  const interimTranscripts = searchParams.get('interimTranscripts');
  const isDebug = searchParams.get('debug');

  useEffect(() => {
    setIsDebugMode(isDebug === 'true');
  }, []);

  const engineProps: EngineProps = {
    applicationValue: applicationValue!,
    engineUrl: '/',
    locale: locale ?? undefined,
    outputMedia: outputMedia ?? undefined,
    sttSampleRate: sttSampleRate ? parseInt(sttSampleRate) : undefined,
    interimTranscripts: interimTranscripts === 'true',
  };

  return <TabSwitcher {...engineProps} />;
};
