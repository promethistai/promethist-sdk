import {
  ARC_REPOSITORY_URL,
  ARC_VERSION,
  ArcUnityRenderer,
  ChatPageProps,
  EngineProps,
  ErrorOverlay,
  MultimodalModal,
  useAppStore,
  useEngineConnectionFactory,
  useHandleAudioState,
  useInitConversation,
  usePermissionMicValue,
  useProcessMicrophoneInput,
  useRefStore,
} from '@js/core';
import React, { useEffect, useRef } from 'react';
import { BrowserView, MobileView } from 'react-device-detect';
import { OnboardingCarousel } from '../sporitelna-content.tsx/carousel-content';
import { RatingScreen } from '../sporitelna-content.tsx/rating-screen';

export const TabSwitcher: React.FC<EngineProps> = ({
  applicationValue,
  engineUrl,
  locale,
  outputMedia,
  sttSampleRate,
  interimTranscripts,
}) => {
  const {
    isConversationEnded,
    getIsClientReady,
    isAudioBeingHandled,
    setIsApplicationReady,
    isApplicationReady,
    errorMessage,
  } = useAppStore();
  const { audioContextRef, audioBufferSourceNodeRef } = useRefStore();
  const isClientReady = getIsClientReady();

  const onOpenRef = useRef(() => undefined);
  const onCloseRef = useRef(() => undefined);
  usePermissionMicValue();

  const { sendMessage, messages, lastMessage, closeSocket } =
    useEngineConnectionFactory({
      engineConnectionValue: engineUrl,
      applicationValue,
      locale: locale,
      outputMedia: outputMedia,
      interimTranscripts,
      sttSampleRate: sttSampleRate ?? 24000,
    });

  useHandleAudioState({
    onOpenRef,
    onCloseRef,
    audioContextRef,
    audioBufferSourceNodeRef,
  });

  useInitConversation({
    messages: messages,
    sendIntro: () => sendMessage('#intro'),
  });

  // setAudioSampleRate(sttSampleRate ?? 24000);
  const { analyzerData } = useProcessMicrophoneInput({
    sendMessage: sendMessage,
  });

  const isCyberSecurity = applicationValue.startsWith('persona:cyber_security');

  useEffect(() => {
    //TODO: remove this when the we have a better solution
    if (!isCyberSecurity) setIsApplicationReady(true);
  }, [isCyberSecurity]);

  const arcProps: ChatPageProps = {
    sendMessage,
    messages,
    lastMessage,
    analyzerData,
    applicationValue,
    arcRepositoryUrl: ARC_REPOSITORY_URL,
    arcVersion: ARC_VERSION,
  };

  return (
    <>
      {errorMessage && <ErrorOverlay />}
      {!isApplicationReady && isCyberSecurity && (
        <OnboardingCarousel onAcceptTerms={() => setIsApplicationReady(true)} />
      )}
      {!isAudioBeingHandled && isConversationEnded && (
        <RatingScreen closeSocket={closeSocket} sendMessage={sendMessage} />
      )}
      <MobileView>
        {isClientReady && !isAudioBeingHandled && isApplicationReady && (
          <MultimodalModal onClick={sendMessage} />
        )}
        <ArcUnityRenderer
          style={{ margin: 0 }}
          {...arcProps}
          analyzerData={analyzerData}
        />
      </MobileView>
      <BrowserView>
        {isClientReady && !isAudioBeingHandled && isApplicationReady && (
          <MultimodalModal onClick={sendMessage} />
        )}
        <ArcUnityRenderer
          style={{ margin: 0 }}
          {...arcProps}
          analyzerData={analyzerData}
        />
      </BrowserView>
    </>
  );
};
