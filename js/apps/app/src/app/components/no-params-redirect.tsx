import { Button, Result } from 'antd';
import { useNavigate } from 'react-router-dom';

export const NoParamsRedirect = () => {
  const navigate = useNavigate();
  return (
    <Result
      title="No application value has been provided. Do you want to redirect to a
        default application?"
      extra={
        <Button type="primary" onClick={() => navigate('/client/default')}>
          Redirect!
        </Button>
      }
    />
  );
};
