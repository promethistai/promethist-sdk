import { StoreState, useAppStore } from '@js/core';
import { CLIENT_APP_MODES } from '../../constants';
export const useChangeView = () => {
  const {
    mode,
    setMode,
    setIsArcReady,
    setIsChatReady,
    enableMicrophone,
    disableMicrophone,
    sendAudioStop,
    sendAudioResume,
  } = useAppStore();

  const changeActiveMode = (key: StoreState['mode']) => {
    if (mode === key) return;
    setIsArcReady(false);
    setIsChatReady(false);
    if (mode === CLIENT_APP_MODES.chat) {
      sendAudioResume();
      setIsArcReady(true);
      setMode(key);
    } else if (mode === CLIENT_APP_MODES.avatar) {
      disableMicrophone();
      sendAudioStop();
      setMode(key);
    } else if (mode === CLIENT_APP_MODES.chatOverlay) {
      enableMicrophone();
      setIsArcReady(true);
      setMode(key);
    } else {
      // audioBufferSourceNodeRef?.current?.stop();
      setMode(key);
    }
  };

  return {
    changeActiveMode,
  };
};
