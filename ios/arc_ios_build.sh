#!/bin/bash

# Print "Promethists" with a stylized echo
echo
echo " ______                            _     _           "
echo "(_____ \                      _   | |   (_)     _    "
echo " _____) )___ ___  ____   ____| |_ | | _  _  ___| |_  "
echo "|  ____/ ___) _ \|    \ / _  )  _)| || \| |/___)  _) "
echo "| |   | |  | |_| | | | ( (/ /| |__| | | | |___ | |__ "
echo "|_|   |_|   \___/|_|_|_|\____)\___)_| |_|_(___/ \___)"
echo

# Git pull
printf "\n"
printf "\e[1;34mPulling latest client changes from git..\e[0m"
printf "\n"
git pull

# URL entry
printf "\n"
printf "\e[1;32mYour ZIP file should contain the Unity build, not a single directory with build in it! (Edit -> Paste Escaped Text)\e[0m"
printf "\n"
printf "\n"
read -p "Enter ARC build zip url: " downloadUrl
printf "\n"
printf "\e[1;34mStarting download..\e[0m"
printf "\n"
echo "$downloadUrl"

# Clear
rm -rf /tmp/arc_build
rm arc_build.zip

# Download ARC build
curl -L "$downloadUrl" -o "arc_build.zip" --progress-bar

# Unzip
unzip arc_build.zip -d /tmp/arc_build

# Replace Unity build
rm -rf /Unity/Data
rm -rf /Unity/Il2CppOutputProject
rm -rf /Unity/Libraries
cp -r /tmp/arc_build/Data Unity/
cp -r /tmp/arc_build/Il2CppOutputProject Unity/
cp -r /tmp/arc_build/Libraries Unity/

# Clear
rm -rf /tmp/arc_build
rm arc_build.zip
printf "\e[1;34mDone\e[0m"

# Open Xcode
printf "\n"
printf "\e[1;32mOpening XCode.. To start the app, click on ▶ play button on top \e[0m"
printf "\n\n"
countdown=3
while [ $countdown -gt 0 ]; do
  echo "opening xcode in: $countdown s"
  sleep 1
  countdown=$((countdown - 1))
done
open -a /Applications/Xcode.app "Promethist.xcodeproj"
killall Terminal
