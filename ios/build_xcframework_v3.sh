#!/bin/bash

# See: https://developer.apple.com/videos/play/wwdc2020/10147/
# Print "Promethists" with a stylized echo
echo
echo " ______                            _     _           "
echo "(_____ \                      _   | |   (_)     _    "
echo " _____) )___ ___  ____   ____| |_ | | _  _  ___| |_  "
echo "|  ____/ ___) _ \|    \ / _  )  _)| || \| |/___)  _) "
echo "| |   | |  | |_| | | | ( (/ /| |__| | | | |___ | |__ "
echo "|_|   |_|   \___/|_|_|_|\____)\___)_| |_|_(___/ \___)"
echo

# Clean build archives

printf "\n"
printf "\e[1;34mCleaning archives..\e[0m"
rm -rf build/PromethistSDK.xcarchive
rm -rf build/PromethistSDK.xcframework
rm -rf build/UnityFramework.xcframework

# Clean previous version of SDK

printf "\n"
printf "\e[1;34mCleaning previous version..\e[0m"
rm -rf ../../ios_sdk

# Prepare SDK files

printf "\n"
printf "\e[1;34mCreating SDK dir..\e[0m"
mkdir -p ../../ios_sdk
printf "\n"
printf "\e[1;34mCopying base SDK files..\e[0m"
cp build/Package.swift ../../ios_sdk
cp build/README.md ../../ios_sdk
cp -r  build/Sources ../../ios_sdk

# Build

printf "\n"
printf "\e[1;34mBuilding iOS archive..\e[0m\n"
xcodebuild archive \
-scheme PromethistSDK \
-destination "generic/platform=iOS" \
-archivePath build/PromethistSDK \
SKIP_INSTALL=NO \
BUILD_LIBRARY_FOR_DISTRIBUTION=YES

# Create Promethist XCFramework

printf "\n"
printf "\e[1;34mCreating Promethist xcframework..\e[0m\n"
xcodebuild -create-xcframework \
-framework build/PromethistSDK.xcarchive/Products/Library/Frameworks/PromethistSDK.framework \
-output build/PromethistSDK.xcframework

# Create UnityFramework XCFramework

printf "\n"
printf "\e[1;34mCreating Unity xcframework..\e[0m\n"
xcodebuild -create-xcframework \
-framework build/PromethistSDK.xcarchive/Products/Library/Frameworks/UnityFramework.framework \
-output build/UnityFramework.xcframework

# Copy frameworks
cp -r  build/PromethistSDK.xcframework ../../ios_sdk
cp -r  build/UnityFramework.xcframework ../../ios_sdk
cp -r  MobileVLCKit.xcframework ../../ios_sdk

# Cleanup

printf "\n"
printf "\e[1;34mCleaning archives..\e[0m"
rm -rf build/PromethistSDK.xcarchive
rm -rf build/PromethistSDK.xcframework
rm -rf build/UnityFramework.xcframework

# Create shared XCFramework

printf "\n"
printf "\e[1;34mBuilding shared..\e[0m\n"
cd ..
./gradlew assembleSharedXCFramework

# Copy shared framework

cp -r  shared/build/XCFrameworks/release/shared.xcframework ../ios_sdk

# Cleanup
rm -rf shared/build/XCFrameworks/debug
rm -rf shared/build/XCFrameworks/release

# Finish

printf "\n"
printf "\e[1;32mBuild finished!\e[0m"

# Zip & compute checksum

#echo "Build finished with success!"
#echo "Creating zip archive with xcframework.."
#cd build
#zip -vr PromethistSDK.xcframework.zip PromethistSDK.xcframework/ -x "*.DS_Store"
#echo "Computing checksum.."
#swift package compute-checksum PromethistSDK.xcframework.zip
