#!/bin/sh

# Copy libraries
mobile_vlc_path=~/Libs/MobileVLCKit.xcframework

if [ -e MobileVLCKit.xcframework ]; then
  	echo "MobileVLCKit found"
else
	if [ ! -e "$mobile_vlc_path" ]; then
  		echo "Error! MobileVLCKit.xcframework not found: $mobile_vlc_path does not exist."
  		exit 1
	fi
  	echo "Copying MobileVLCKit.."
  	cp -r "$mobile_vlc_path" .
  	echo "Copying MobileVLCKit finished"
fi

# Build

echo "Building iOS archive.."
xcodebuild archive \
-scheme PromethistSDK \
-destination "generic/platform=iOS" \
-archivePath build/PromethistSDK \
SKIP_INSTALL=NO \
BUILD_LIBRARY_FOR_DISTRIBUTION=YES

echo "Building iOS simulator archive.."
xcodebuild archive \
-scheme PromethistSDK \
-destination "generic/platform=iOS Simulator" \
-archivePath build/PromethistSDK-Sim \
SKIP_INSTALL=NO \
BUILD_LIBRARY_FOR_DISTRIBUTION=YES

echo "Creating xcframework.."
xcodebuild -create-xcframework \
-framework build/PromethistSDK.xcarchive/Products/Library/Frameworks/PromethistSDK.framework \
-framework build/PromethistSDK-Sim.xcarchive/Products/Library/Frameworks/PromethistSDK.framework \
-output build/PromethistSDK.xcframework

echo "Cleaning archives.."
rm -rf build/PromethistSDK.xcarchive
rm -rf build/PromethistSDK-Sim.xcarchive
echo "Build finished with success!"

# Zip & compute checksum

echo "Creating zip archive with xcframework.."
cd build
zip -vr PromethistSDK.xcframework.zip PromethistSDK.xcframework/ -x "*.DS_Store"
echo "Computing checksum.."
swift package compute-checksum PromethistSDK.xcframework.zip