//
//  PromethistKit.h
//  PromethistKit
//
//  Created by Vojtěch Vošmík on 06.05.2024.
//  Copyright © 2024 orgName. All rights reserved.
//

#import <Foundation/Foundation.h>

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wincomplete-umbrella"

#if !(TARGET_IPHONE_SIMULATOR)
#import <UnityFramework/NativeCallProxy.h>
#endif

#pragma clang diagnostic pop

//! Project version number for PromethistKit.
FOUNDATION_EXPORT double PromethistKitVersionNumber;

//! Project version string for PromethistKit.
FOUNDATION_EXPORT const unsigned char PromethistKitVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <PromethistKit/PublicHeader.h>


