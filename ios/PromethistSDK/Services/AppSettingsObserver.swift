import Foundation
import shared

public class AppSettingsObserver: ObservableObject {

    @Published public var appConfig: AppConfig = Client().appConfig()

    public init() {
        self.appConfig = appConfig
    }

    public func update() {
        appConfig = Client().appConfig()
    }
}
