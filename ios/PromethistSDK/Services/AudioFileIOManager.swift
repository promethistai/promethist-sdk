import Foundation

class AudioFileIOManager {

    func getWavFileSize(from data: Data) -> UInt32? {
        guard data.count >= 44 else {
            log("Data does not contain enough bytes to be a valid WAV file.")
            return nil
        }

        let riffChunkSizeRange = 4..<8
        let riffChunkSizeData = data.subdata(in: riffChunkSizeRange)
        let riffChunkSize = riffChunkSizeData.withUnsafeBytes {
            $0.load(as: UInt32.self)
        }
        return riffChunkSize + 8
    }

    func writeToFile(data: Data, fileName: String) -> URL? {
        guard let directory = FileManager.default.urls(
            for: .documentDirectory,
            in: .userDomainMask
        ).last else {
            return nil
        }
        let fileurl = directory.appendingPathComponent("\(fileName)")

        if FileManager.default.fileExists(atPath: fileurl.path) {
            if let fileHandle = FileHandle(forWritingAtPath: fileurl.path) {
                fileHandle.seekToEndOfFile()
                fileHandle.write(data)
                fileHandle.closeFile()
                return fileurl
            }
            else {
                log("Can't open file to write.")
            }
        } else {
            do {
                try data.write(to: fileurl, options: .atomic)
                return fileurl
            } catch {
                log("Unable to write in new file.")
            }
        }

        return nil
    }

    func generateAudioFileName() -> String {
        "\(UUID()).wav"
    }

    private func log(_ message: String) {
        print("AudioFileIOManager => \(message)")
    }
}
