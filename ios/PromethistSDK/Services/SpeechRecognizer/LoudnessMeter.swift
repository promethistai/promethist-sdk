import AVFAudio

class LoudnessMeter {

    static let shared = LoudnessMeter()

    private init() {}

    func getLoudness(from buffer: AVAudioPCMBuffer) -> Float {
        let audioBuffer = buffer.audioBufferList.pointee.mBuffers
        let frameLength = Int(buffer.frameLength)

        guard frameLength > 0, let data = audioBuffer.mData else { return -160 }

        let samples = UnsafeBufferPointer(start: data.assumingMemoryBound(to: Float.self), count: frameLength)
        let rms = sqrt(samples.reduce(0.0) { $0 + $1 * $1 } / Float(frameLength))

        let rmsClamped = max(rms, 0.000_000_1)
        let db = 20 * log10(rmsClamped)

        return db
    }
}
