//
//  AVAudioPCMBuffer.swift
//  Promethist
//
//  Created by Vojtěch Vošmík on 26.11.2024.
//  Copyright © 2024 orgName. All rights reserved.
//

import AVFoundation

extension AVAudioPCMBuffer {

    func data() -> NSData? {
        let buffer = self

        // Ensure the buffer has audio data
        guard let audioBuffer = buffer.audioBufferList.pointee.mBuffers.mData,
              buffer.frameLength > 0 else {
            return nil
        }

        // Calculate the data size in bytes
        let dataSize = Int(buffer.frameLength) * Int(buffer.format.streamDescription.pointee.mBytesPerFrame)

        // Create NSData from the audio buffer
        let data = NSData(bytes: audioBuffer, length: dataSize)

        return data
    }
}
