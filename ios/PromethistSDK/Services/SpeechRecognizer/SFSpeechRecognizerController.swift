import Foundation
@_implementationOnly import shared
import Speech
import SwiftUI

class SFSpeechRecognizerController {
    
    public var callback: SpeechRecognizerControllerCallback
    
    private let recognizer: SFSpeechRecognizer?
    private var recognitionRequest: SFSpeechAudioBufferRecognitionRequest?
    private var recognitionTask: SFSpeechRecognitionTask?
    private var audioEngine: AVAudioEngine?
    private var audioRecorder: AVAudioRecorder?

    private var silenceLength = 30
    private let endOfUtteranceTimeout = 2.0
    private let isHapticEnabled = true
    private let icMicPowerEnabled = true
    private var isFinal = false
    private var transcript: String? = nil
    private var transcriptTime: Date?
    private var paused: Bool = false

    private var shouldSendMicPermissionAction: Bool {
        false
    }

    init() {
        self.callback = SpeechRecognizerControllerCallbackMockImpl()

        let currentPresetLocale = Client().appConfig().locale
        print("SFSpeechRecognizerController: current locale is \(currentPresetLocale)")
        self.recognizer = SFSpeechRecognizer(locale: Locale(identifier: currentPresetLocale.language == "cs" ? "cs" : "en-US"))
    }

    func checkPermissions() async -> String? {
        if !SFSpeechRecognizer.isDetermined || !AVAudioSession.isDetermined {
            do {
                guard recognizer != nil else {
                    throw RecognizerError.nilRecognizer
                }

                print("SFSpeechRecognizerController: Asking audio permission")
                let hasAuthorizationToRecognize = await SFSpeechRecognizer.hasAuthorizationToRecognize()
                let hasPermissionToRecord = await AVAudioSession.sharedInstance().hasPermissionToRecord()
                print("SFSpeechRecognizerController: Recognize permission: \(hasAuthorizationToRecognize ? "granted" : "denied")")
                print("SFSpeechRecognizerController: Record permission: \(hasPermissionToRecord ? "granted" : "denied")")

                guard hasAuthorizationToRecognize else {
                    throw RecognizerError.notAuthorizedToRecognize
                }
                guard hasPermissionToRecord else {
                    throw RecognizerError.notPermittedToRecord
                }

                return Action.micgranted.text
            } catch {
                //await setAppError(text: error.localizedDescription, source: "Speech Recognizer")
                /*if defaultInteractionMode != .text {
                    defaultInteractionMode = .text
                }*/
                return Action.micdenied.text
            }
        }
        return nil
    }

    static func isMicDenied() async -> Bool {
        if SFSpeechRecognizer.isDetermined && AVAudioSession.isDetermined {
            do {
                let hasAuthorizationToRecognize = await SFSpeechRecognizer.hasAuthorizationToRecognize()
                let hasPermissionToRecord = await AVAudioSession.sharedInstance().hasPermissionToRecord()

                guard hasAuthorizationToRecognize else {
                    throw RecognizerError.notAuthorizedToRecognize
                }
                guard hasPermissionToRecord else {
                    throw RecognizerError.notPermittedToRecord
                }

                return false
            } catch {
                return true
            }
        }
        return false
    }

    private func captureAudio() {
        let documentPath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let audioFilename = documentPath.appendingPathComponent("recording.m4a")
        let settings = [
            AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
            AVSampleRateKey: 12000,
            AVNumberOfChannelsKey: 1,
            AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue
        ]

        do {
            audioRecorder = try AVAudioRecorder(url: audioFilename, settings: settings)
            audioRecorder?.record()
            audioRecorder?.isMeteringEnabled = true
        } catch {
            print("ERROR: Failed to start recording process.")
        }
    }
}

extension SFSpeechRecognizerController: SpeechRecognizerController {

    func setCallback(callback: any SpeechRecognizerControllerCallback) {
        self.callback = callback
    }

    func restartTranscribe() {}

    func setPrivileged(value: Bool) {}

    func startTranscribe() async throws {
        print("SFSpeechRecognizerController: Starting transcribe")

        paused = false
        let permissionsResult = await checkPermissions()

        if let permissionsResult {
            if shouldSendMicPermissionAction {
                callback.onTranscribeResult(transcript: permissionsResult, isFinal: true)
                return
            } else if permissionsResult == Action.micdenied.text {
                return
            }
        }

        stopTranscribe()
        setupAudioSession()
        transcript = nil
        audioEngine = AVAudioEngine()

        #if targetEnvironment(simulator)
        await simulateTranscribe()
        return
        #endif

        guard let audioEngine = audioEngine else { return }
        let inputNode = audioEngine.inputNode
        let sampleRate = inputNode.inputFormat(forBus: 0).sampleRate

        inputNode.removeTap(onBus: 0)

        guard let recordingFormat = AVAudioFormat(commonFormat: .pcmFormatFloat32, sampleRate: sampleRate, channels: 1, interleaved: true) else { return }

        var retryBusyCount = 3
        while audioInputIsBusy(recordingFormat: recordingFormat), retryBusyCount > 0 {
            print("SFSpeechRecognizerController: audioInputIsBusy retry count: \(retryBusyCount)")
            print("SFSpeechRecognizerController: audioInputIsBusy retry count: \(retryBusyCount)")
            try? await Task.sleep(nanoseconds: 0_500_000_000)
            retryBusyCount -= 1
        }

        if retryBusyCount < 1 {
            print("SFSpeechRecognizerController: resseting audioEngine")
            print("SFSpeechRecognizerController: resseting audioEngine")
            audioEngine.reset()
            setupAudioSession()
        }

        inputNode.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) { [weak self] (buffer: AVAudioPCMBuffer, when: AVAudioTime) in
            self?.recognitionRequest?.append(buffer)
        }

        recognitionRequest = SFSpeechAudioBufferRecognitionRequest()
        recognitionRequest?.shouldReportPartialResults = true
        recognitionRequest?.requiresOnDeviceRecognition = false

        if icMicPowerEnabled {
            captureAudio()
        }

        guard let recognitionRequest = recognitionRequest else { return }
        recognitionTask = recognizer?.recognitionTask(with: recognitionRequest) { [weak self] result, error in
            guard let self = self else { return }
            guard !paused else { return }

            if let result = result {
                let transcript = result.bestTranscription.formattedString

                if !transcript.isEmpty {
                    callback.onTranscribeResult(transcript: transcript, isFinal: false)
                    self.transcript = transcript
                    self.transcriptTime = .now
                    if isHapticEnabled {
                        Vibration.rigid.vibrate()
                    }
                }

                if result.isFinal {
                    isFinal = true
                    cancelRecognitionTask()
                }
            }

            if let error = error {
                // TODO: Handle SpeechRecognizer error
                print("SFSpeechRecognizerController: Error: \(error)")
                print(error)
                self.cancelRecognitionTask()
            }

            if error != nil || self.isFinal == true {
                self.stopTranscribe()
            }
        }

        audioEngine.prepare()
        try? audioEngine.start()

        await waitForTranscribe()
    }

    private func waitForTranscribe() async {
        var time = 0
        while !isFinal {
            // Power indicator
            if icMicPowerEnabled && audioRecorder?.isRecording == true {
                DispatchQueue.main.async { [weak self] in
                    guard let self = self else { return }
                    self.audioRecorder?.updateMeters()
                    let db = self.audioRecorder?.averagePower(forChannel: 0) ?? 0
                    appEvents.send(.micPowerUpdate(db))
                }
            }

            // Check if transcribe was canceled
            if recognitionRequest == nil {
                break
            }

            // Recognizer paused
            while paused {
                try? await Task.sleep(nanoseconds: 0_050_000_000)
            }

            // User is silent
            if transcript == nil && time > silenceLength * 1000 {
                callback.onTranscribeResult(transcript: "", isFinal: true)
                stopTranscribe()
                if isHapticEnabled {
                    Vibration.medium.vibrate()
                }
                break
            }

            // End of utterance
            if let transcriptTime = transcriptTime, let transcript = self.transcript, (transcriptTime + endOfUtteranceTimeout) < .now {
                callback.onTranscribeResult(transcript: transcript, isFinal: true)
                stopTranscribe()
                if isHapticEnabled {
                    Vibration.medium.vibrate()
                }
                break
            }

            try? await Task.sleep(nanoseconds: 0_050_000_000)
            time += 50
        }
    }

    private func setupAudioSession() {
        let audioSession = AVAudioSession.sharedInstance()
        try? audioSession.setCategory(AVAudioSession.Category.playAndRecord, mode: .default, options: [.defaultToSpeaker, .mixWithOthers])
        try? audioSession.setActive(true)
    }

    func stopTranscribe() {
        print("SFSpeechRecognizerController: Stopping transcribe")

        audioEngine?.stop()
        audioEngine?.inputNode.removeTap(onBus: 0)
        audioEngine = nil

        cancelRecognitionTask()

        recognitionRequest?.endAudio()
        recognitionRequest = nil

        isFinal = false
        transcriptTime = nil

        if icMicPowerEnabled {
            audioRecorder?.stop()
            audioRecorder = nil
        }
    }

    func pause() {
        paused = true
        audioEngine?.pause()
    }

    func resume() {
        guard recognitionTask != nil else { return }
        paused = false
        try? audioEngine?.start()
    }

    func setSilenceLength(length: Int32) {
        self.silenceLength = Int(length)
    }

    func onVisualItemReceived() {
        guard recognitionTask != nil else { return }
        stopTranscribe()
    }

    private func cancelRecognitionTask() {
        recognitionTask?.cancel()
        recognitionTask?.finish()
        recognitionTask = nil
    }

    /// Checks if recording format is busy
    /// Calling installTap with busy format can cause crash
    private func audioInputIsBusy(recordingFormat: AVAudioFormat) -> Bool {
        guard recordingFormat.sampleRate == 0 || recordingFormat.channelCount == 0 else {
            return false
        }

        return true
    }
}

private extension SFSpeechRecognizerController {

    func simulateTranscribe() async {
        try? await Task.sleep(nanoseconds: 1_000_000_000)
        callback.onTranscribeResult(transcript: "To", isFinal: false)
        try? await Task.sleep(nanoseconds: 1_000_000_000)
        callback.onTranscribeResult(transcript: "To je skvělé", isFinal: false)
        try? await Task.sleep(nanoseconds: 0_500_000_000)
        callback.onTranscribeResult(transcript: "To je skvělé, roaming prosím", isFinal: true)
        stopTranscribe()
    }
}

private class SpeechRecognizerControllerCallbackMockImpl: SpeechRecognizerControllerCallback {
    func onTranscribeResult(transcript: String, isFinal: Bool) {}
    func onBytesReceived(base64: String) {}
}
