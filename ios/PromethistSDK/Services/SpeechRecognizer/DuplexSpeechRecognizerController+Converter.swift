//
//  Converter.swift
//  Promethist
//
//  Created by Vojtěch Vošmík on 26.11.2024.
//  Copyright © 2024 orgName. All rights reserved.
//

import Speech
@_implementationOnly import shared

extension DuplexSpeechRecognizerController {

    var serverAudioFormat: AVAudioFormat {
        let defaultAudioFormat = Defaults.shared.audioFormat

        let commonFormat: AVAudioCommonFormat = switch defaultAudioFormat.encoding {
        case .linear16: .pcmFormatInt16
        default: .pcmFormatInt16
        }
        let sampleRate = Double(defaultAudioFormat.sampleRate)
        let channels = AVAudioChannelCount(defaultAudioFormat.channels)

        return AVAudioFormat(
            commonFormat: commonFormat,
            sampleRate: sampleRate,
            channels: channels,
            interleaved: true
        )!
    }

    func convertBuffer(_ inputBuffer: AVAudioPCMBuffer, from inputFormat: AVAudioFormat, to outputFormat: AVAudioFormat) -> AVAudioPCMBuffer? {
        // Create a converter
        guard let converter = AVAudioConverter(from: inputFormat, to: outputFormat) else {
            log("Failed to create AVAudioConverter")
            return nil
        }

        // Prepare output buffer
        guard let outputBuffer = AVAudioPCMBuffer(
            pcmFormat: outputFormat,
            frameCapacity: AVAudioFrameCount(outputFormat.sampleRate * Double(inputBuffer.frameLength) / inputFormat.sampleRate)
        ) else {
            log("Failed to create output buffer")
            return nil
        }

        // Perform conversion
        var error: NSError?
        converter.convert(to: outputBuffer, error: &error) { inNumPackets, outStatus in
            outStatus.pointee = .haveData
            return inputBuffer
        }

        // Handle errors
        if let error = error {
            log("Conversion error: \(error.localizedDescription)")
            return nil
        }

        return outputBuffer
    }
}
