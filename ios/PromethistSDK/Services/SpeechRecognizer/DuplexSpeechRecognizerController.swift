import Foundation
@_implementationOnly import shared
import Speech
import SwiftUI

class DuplexSpeechRecognizerController {

    // Public properties
    static let shared = DuplexSpeechRecognizerController()
    public var callback: SpeechRecognizerControllerCallback?
    public var allowDuplex: Bool = true

    // Private properties
    private var audioEngine: AVAudioEngine?
    private var speechRecognizer: SFSpeechRecognizer?
    private var speechRecognitionRequest: SFSpeechAudioBufferRecognitionRequest?
    private var speechRecognitionTask: SFSpeechRecognitionTask?

    // State properties
    private var isListening: Bool = false
    private var isProcessing: Bool = false
    private var isPrivileged: Bool = false
    private var currentTranscript: String = ""
    private var lastTranscriptUpdateTime: Date?
    private var isPaused: Bool = false

    // Constants
    private static let defaultBus = 0
    private static let defaultBufferSize: UInt32 = 1024
    private static let enableVoiceProcessing = true
    private static let endOfUtteranceTimeout = 2.0
    private static let channels: UInt32 = 1
    private static let interleaved = true
    private static let useStorage = false
    private static let loudnessThresholdDb: Float = -35

    // Dependencies
    private var duplexMode: DuplexMode { Client().appConfig().duplexMode }
    private var forceAudioOutput: Bool { Client().appConfig().duplexForceOutput }
    private var avatarPlayer: AppleUnityPlayerController { AppleUnityPlayerController.shared }
    private var storage: DuplexStorage { DuplexStorage.shared }
    private let loudnessMeter = LoudnessMeter.shared

    private init() {
        self.callback = nil
    }

    func setup() {
        log("Setup")
        // Setup components
        setupAudioSession()
        setupAudioEngine()
        setupSpeechRecognizer()

        // Start audio engine
        startAudioEngine()

        // Save to storage if enabled
        if Self.useStorage {
            storage.startSavingPCM()
        }
    }

    func onAudioPlaybackStarted() {
        isListening = true
        if !isSpeechRecognitionActive && allowDuplex {
            stopSpeechRecognition()
            startSpeechRecognition()
        } else if !allowDuplex {}
    }

    func onAudioPlaybackEnded() {}
}

// MARK: - Results processing

private extension DuplexSpeechRecognizerController {

    func onAudioBufferReceived(with buffer: AVAudioPCMBuffer) {
        switch duplexMode {
        case .ondevice:
            // Send data to speech recognizer
            guard isListening else { return }

            // Check loudness
            let loudness = loudnessMeter.getLoudness(from: buffer)
            DispatchQueue.main.async {
                appEvents.send(.micPowerUpdate(loudness))
            }

            let isSpeaking = loudness >= Self.loudnessThresholdDb || !currentTranscript.isEmpty
            if isSpeaking || isPrivileged {
                self.speechRecognitionRequest?.append(buffer)
            }
        case .streamed:
            // Send raw data to KMP
            guard isListening else { return }
            if let base64 = buffer.data()?.base64EncodedString(options: []) {
                self.callback?.onBytesReceived(base64: base64)
                if Self.useStorage {
                    storage.appendPCMBufferToFile(base64: base64)
                }
            }
        default: break
        }
    }

    func onSpeechRecognizerResult(transcript: String, isFinal: Bool) {
        guard !transcript.isEmpty else { return }
        let processedTranscript = transcript.processTranscript()

        // Update state
        self.currentTranscript = processedTranscript
        self.lastTranscriptUpdateTime = .now

        // Update transcript display
        self.callback?.onTranscribeResult(transcript: processedTranscript, isFinal: false)

        // Stop audio playback
        Task {
            log("Stopping Unity audio playback")
            await UnityEmbeddedSwift.stopPlayback()
            avatarPlayer.setBlocked(true)
        }

        // Send final transcript
        if isFinal {
            log("Final transcript: \(processedTranscript)")
            self.callback?.onTranscribeResult(transcript: processedTranscript, isFinal: true)
            endSpeechRecognizer()
        }

        // Enqueue processing
        Task {
            await enqueueTranscriptProcessing()
        }
    }

    func enqueueTranscriptProcessing() async {
        guard !isProcessing else { return }
        isProcessing = true
        log("Enqueue transcript processing..")

        var time = 0
        while isProcessing {
            // Delay while paused
            if isPaused {
                self.lastTranscriptUpdateTime = .now
                continue
            }

            // End of utterance detection
            if let lastTranscriptUpdateTime = self.lastTranscriptUpdateTime {
                let endOfUtteranceEstimation = (lastTranscriptUpdateTime + Self.endOfUtteranceTimeout)
                if .now > endOfUtteranceEstimation && !currentTranscript.isEmpty {
                    log("Final transcript (EOU): \(currentTranscript)")
                    self.callback?.onTranscribeResult(transcript: currentTranscript, isFinal: true)
                    self.endSpeechRecognizer()
                }
            }

            try? await Task.sleep(nanoseconds: 0_050_000_000)
            time += 50
        }
    }

    func endSpeechRecognizer() {
        log("Ending speech recognizer")

        // Update state properties
        isListening = false
        isProcessing = false
        currentTranscript = ""
        lastTranscriptUpdateTime = nil

        // Stop Apple recognition task
        stopSpeechRecognition()

        // Unblock Unity audio playback
        avatarPlayer.setBlocked(false)
    }
}

// MARK: - Controller

extension DuplexSpeechRecognizerController: SpeechRecognizerController {

    func setCallback(callback: any SpeechRecognizerControllerCallback) {
        self.callback = callback
    }

    func stopTranscribe() {
        endSpeechRecognizer()
    }

    func restartTranscribe() {
        guard allowDuplex else { return }
        guard duplexMode == .ondevice else { return }
        guard currentTranscript.isEmpty else { return }
        log("Restarting speech recognizer..")
        endSpeechRecognizer()
        startSpeechRecognition()
        isListening = true
    }

    func setPrivileged(value: Bool) {
        isPrivileged = value
    }

    func pause() {
        log("Pause")
        // TODO: Stop saving better somehow
        if Self.useStorage {
            storage.stopSavingPCM()
        }

        isPaused = true
    }

    func startTranscribe() async throws {
        isPaused = false
        guard !allowDuplex else { return }
        endSpeechRecognizer()
        startSpeechRecognition()
        isListening = true
        isPrivileged = true
    }

    func resume() {
        log("Resume")
        isPaused = false
    }

    func onVisualItemReceived() { /* Ignoring events in duplex mode */ }

    func setSilenceLength(length: Int32) { /* Ignoring events in duplex mode */ }
}

// MARK: - AudioEngine

private extension DuplexSpeechRecognizerController {

    var isAudioEngineSetup: Bool {
        self.audioEngine != nil
    }

    var isAudioEngineActive: Bool {
        self.audioEngine?.isRunning == true
    }

    func setupAudioEngine() {
        guard self.audioEngine == nil else { return }
        log("Setting up audio engine..")

        // Create audio engine
        let audioEngine = AVAudioEngine()
        self.audioEngine = audioEngine

        // Build input node & format
        let inputNode = audioEngine.inputNode
        let inputFormat = inputNode.inputFormat(forBus: Self.defaultBus)
        log("Input format: \(inputFormat)")
        let recordingFormat = AVAudioFormat(
            commonFormat: .pcmFormatFloat32,
            sampleRate: inputFormat.sampleRate,
            channels: Self.channels,
            interleaved: Self.interleaved
        )!
        log("Recording format: \(recordingFormat)")
        log("Server format: \(serverAudioFormat)")

        // Enable VoiceProcessing
        try! inputNode.setVoiceProcessingEnabled(Self.enableVoiceProcessing)

        // Install tap
        inputNode.installTap(
            onBus: Self.defaultBus,
            bufferSize: Self.defaultBufferSize,
            format: recordingFormat
        ) { [weak self] (buffer: AVAudioPCMBuffer, when: AVAudioTime) in
            guard let self = self else { return }
            guard !isPaused else { return }

            switch duplexMode {
            case .ondevice:
                onAudioBufferReceived(with: buffer)
            case .streamed:
                if let convertedBuffer = self.convertBuffer(buffer, from: recordingFormat, to: serverAudioFormat) {
                    onAudioBufferReceived(with: convertedBuffer)
                }
            default: break
            }
        }
    }

    func startAudioEngine() {
        guard let audioEngine = audioEngine else { return }
        guard !audioEngine.isRunning else {
            log("Audio engine is already running..")
            return
        }
        log("Starting audio engine..")

        audioEngine.prepare()
        try! audioEngine.start()
        setupAudioSession()
    }

    func setupAudioSession() {
        AudioManager.setupAudioSession()

        let session = AVAudioSession.sharedInstance()
        if let inputs = session.availableInputs {
            for input in inputs {
                print("Available Input: \(input.portType.rawValue)")
            }
        }
        if let airPodsInput = session.availableInputs?.first(where: { $0.portType == .bluetoothHFP }) {
            try? session.setPreferredInput(airPodsInput)
            return
        }

        //if !allowDuplex {
            try? AVAudioSession.sharedInstance().overrideOutputAudioPort(.none)
            try? AVAudioSession.sharedInstance().overrideOutputAudioPort(.speaker)
            log("overrideOutputAudioPort to speaker")
        //}
    }
}

// MARK: - Speech recognizer

private extension DuplexSpeechRecognizerController {

    var isSpeechRecognitionActive: Bool {
        self.speechRecognitionRequest != nil && self.speechRecognitionTask != nil
    }

    func startSpeechRecognition() {
        guard audioEngine != nil else { return }
        guard let speechRecognizer = speechRecognizer else { return }

        let speechRecognitionRequest = buildSpeechRecognitionRequest()
        self.speechRecognitionRequest = speechRecognitionRequest

        // Fixes the volume
        if forceAudioOutput {
            try? AVAudioSession.sharedInstance().overrideOutputAudioPort(.none)
            try? AVAudioSession.sharedInstance().overrideOutputAudioPort(.speaker)
        }

        log("Starting recognition..")
        let speechRecognitionTask = speechRecognizer.recognitionTask(
            with: speechRecognitionRequest
        ) { [weak self] result, error in
            if let result = result {
                let transcript = result.bestTranscription.formattedString
                let isFinal = result.isFinal
                self?.log("Result: \(transcript) (isFinal: \(isFinal)")
                self?.onSpeechRecognizerResult(transcript: transcript, isFinal: isFinal)
            } else if let error = error {
                self?.log("Result error: \(error.localizedDescription)")
            }
        }
        self.speechRecognitionTask = speechRecognitionTask

        #if targetEnvironment(simulator)
        simulateTranscribe()
        return
        #endif
    }

    func stopSpeechRecognition() {
        if let speechRecognitionRequest = self.speechRecognitionRequest {
            log("Stopping recognition..")
            speechRecognitionRequest.endAudio()
            self.speechRecognitionRequest = nil
        }
        if let speechRecognitionTask = self.speechRecognitionTask {
            speechRecognitionTask.cancel()
            self.speechRecognitionTask = nil
        }
    }

    func buildSpeechRecognitionRequest() -> SFSpeechAudioBufferRecognitionRequest {
        log("Building recognition request..")
        let recognitionRequest = SFSpeechAudioBufferRecognitionRequest()
        recognitionRequest.shouldReportPartialResults = true
        recognitionRequest.requiresOnDeviceRecognition = false
        recognitionRequest.contextualStrings = []
        recognitionRequest.taskHint = .unspecified

        if #available(iOS 16, *) {
            recognitionRequest.addsPunctuation = true
        }

        return recognitionRequest
    }

    func setupSpeechRecognizer() {
        let localeKey = Client().appConfig().locale.language == "cs" ? "cs" : "en-US"
        let locale = Locale(identifier: localeKey)
        let speechRecognizer = SFSpeechRecognizer(locale: locale)
        self.speechRecognizer = speechRecognizer
    }
}

// MARK: - Permissions

extension DuplexSpeechRecognizerController {

    func checkPermissions() async -> String? {
        if !SFSpeechRecognizer.isDetermined || !AVAudioSession.isDetermined {
            do {
                guard speechRecognizer != nil else {
                    throw RecognizerError.nilRecognizer
                }

                print("SFSpeechRecognizerController: Asking audio permission")
                let hasAuthorizationToRecognize = await SFSpeechRecognizer.hasAuthorizationToRecognize()
                let hasPermissionToRecord = await AVAudioSession.sharedInstance().hasPermissionToRecord()
                print("SFSpeechRecognizerController: Recognize permission: \(hasAuthorizationToRecognize ? "granted" : "denied")")
                print("SFSpeechRecognizerController: Record permission: \(hasPermissionToRecord ? "granted" : "denied")")

                guard hasAuthorizationToRecognize else {
                    throw RecognizerError.notAuthorizedToRecognize
                }
                guard hasPermissionToRecord else {
                    throw RecognizerError.notPermittedToRecord
                }

                return Action.micgranted.text
            } catch {
                return Action.micdenied.text
            }
        }
        return nil
    }

    static func isMicDenied() async -> Bool {
        if SFSpeechRecognizer.isDetermined && AVAudioSession.isDetermined {
            do {
                let hasAuthorizationToRecognize = await SFSpeechRecognizer.hasAuthorizationToRecognize()
                let hasPermissionToRecord = await AVAudioSession.sharedInstance().hasPermissionToRecord()

                guard hasAuthorizationToRecognize else {
                    throw RecognizerError.notAuthorizedToRecognize
                }
                guard hasPermissionToRecord else {
                    throw RecognizerError.notPermittedToRecord
                }

                return false
            } catch {
                return true
            }
        }
        return false
    }
}

// MARK: - Logging

extension DuplexSpeechRecognizerController {

    func log(_ message: String) {
        print("🎤 DuplexSpeech: \(message)")
    }
}

// MARK: - Mock

private extension DuplexSpeechRecognizerController {

    func simulateTranscribe() {
        Task {
            try? await Task.sleep(nanoseconds: 1_000_000_000)
            self.onSpeechRecognizerResult(transcript: "řekni mi", isFinal: false)
            try? await Task.sleep(nanoseconds: 1_500_000_000)
            self.onSpeechRecognizerResult(transcript: "řekni mi vtip", isFinal: true)
        }
    }
}

private extension String {

    func processTranscript() -> String {
        if self.hasPrefix("A ") || self.hasPrefix("V ") {
            return String(self.dropFirst(2))
        }
        return self
    }
}
