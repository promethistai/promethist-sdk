//
//  DuplexStorage.swift
//  Promethist
//
//  Created by Vojtěch Vošmík on 26.11.2024.
//  Copyright © 2024 orgName. All rights reserved.
//

import Foundation
import AVFAudio

class DuplexStorage {

    static let shared = DuplexStorage()

    private init() {}

    private var pcmFileHandle: FileHandle?
    private static var path = ""

    func startSavingPCM() {
        // Build path
        let documentsPath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH-mm-ss"
        let timeString = dateFormatter.string(from: Date())
        let filePath1 = documentsPath.appendingPathComponent("output-\(timeString).pcm").path
        Self.path = filePath1

        let filePath: String = Self.path
        let fileURL = URL(fileURLWithPath: filePath)

        // Create the file if it doesn’t exist
        if !FileManager.default.fileExists(atPath: filePath) {
            FileManager.default.createFile(atPath: filePath, contents: nil)
        }

        do {
            print("DuplexStorage: File opened: \(Self.path)")
            pcmFileHandle = try FileHandle(forWritingTo: fileURL)
        } catch {
            print("DuplexStorage: Failed to open file for writing: \(error.localizedDescription)")
        }
    }

    func appendPCMBufferToFile(buffer: AVAudioPCMBuffer) {
        // ‼️ This can only be used with 16bit format

        guard let fileHandle = pcmFileHandle else {
            print("DuplexStorage: File handle is not available")
            return
        }

        // Access the raw PCM data
        guard let channelData = buffer.int16ChannelData else {
            print("DuplexStorage: Buffer does not contain Int16 data ‼️")
            return
        }

        let frameLength = Int(buffer.frameLength)
        let channelCount = Int(buffer.format.channelCount)

        print("DuplexStorage: Appending: \(frameLength)")

        // Combine PCM data from all channels
        for channel in 0..<channelCount {
            let samples = channelData[channel]
            let data = Data(bytes: samples, count: frameLength * MemoryLayout<Int16>.size)
            fileHandle.write(data) // Append data to file
        }
    }

    func appendPCMBufferToFile(base64: String) {
        guard let fileHandle = pcmFileHandle else {
            print("DuplexStorage: File handle is not available")
            return
        }

        // Decode the Base64 string into raw PCM data
        guard let decodedData = Data(base64Encoded: base64) else {
            print("DuplexStorage: Failed to decode Base64 string")
            return
        }

        // Write the decoded PCM data to the file
        fileHandle.write(decodedData)
        print("DuplexStorage: Appended Base64 PCM data of size: \(decodedData.count) bytes")
    }

    func stopSavingPCM() {
        do {
            try pcmFileHandle?.close()
            print("DuplexStorage: File successfully saved. \(Self.path)")
        } catch {
            print("DuplexStorage: Error closing file: \(error.localizedDescription)")
        }
        pcmFileHandle = nil
    }
}
