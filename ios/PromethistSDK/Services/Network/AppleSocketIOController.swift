//
//  AppleSocketIOController.swift
//  Promethist
//
//  Created by Vojtěch Vošmík on 12.02.2025.
//  Copyright © 2025 orgName. All rights reserved.
//

import SocketIO
@_implementationOnly import shared

class AppleSocketIOController {

    private let manager = SocketManager(socketURL: URL(string: "https://engine-preview.promethist.ai")!)

    private var socket: SocketIOClient?
    private var callback: ClientChannelSocketIOCallback?

    private func registerHandlers() {
        guard let socket = self.socket else { return }
        socket.on("text") { [weak self] data, ack in
            guard let self = self else { return }
            guard let payload = data[0] as? String else { return }
            self.callback?.onTextReceived(payload: payload)

            if !payload.contains("#speech-item") {
                print("[SocketIO] text: \(payload)")
            }
        }

        socket.on(clientEvent: .statusChange) { data, ack in
            print("[SocketIO] statusChange: \(data)")
        }

        socket.on(clientEvent: .disconnect) { _, _ in
            print("[SocketIO] disconnect")
        }

        socket.on(clientEvent: .error) { [weak self] data, ack in
            guard let self = self else { return }
            if let errorMessage = data.first as? String {
                print("[SocketIO] error: \(errorMessage)")
                self.callback?.onErrorReceived(message: errorMessage)
            } else {
                print("[SocketIO] unknown error: \(data)")
                self.callback?.onErrorReceived(message: nil)
            }
        }
    }
}

extension AppleSocketIOController: ClientChannelSocketIOController {

    func setup() {
        let socket = manager.socket(forNamespace: "/socket/io/pipeline")
        self.socket = socket
        self.callback = nil
        registerHandlers()
    }

    func emit(input: String) async throws {
        guard let socket = self.socket else { return }
        print("[SocketIO] emit \(input)")
        socket.emit("text", input)
    }

    func connect(config: ClientSessionConfig) async throws {
        guard let socket = self.socket else { return }
        guard socket.status != .connected else {
            print("[SocketIO] skipping refresh")
            return
        }

        print("[SocketIO] refreshing session..")
        print("[SocketIO] headers: \(config.headers)")

        manager.reconnect()
        manager.setConfigs([
            .log(true),
            .compress,
            .forceWebsockets(true),
            .connectParams(["key": "default"]),
            .extraHeaders(config.headers)
        ])
        socket.connect()

        var retryCount = 0
        while socket.status != .connected && retryCount < 200 {
            print("[SocketIO] connecting: \(socket.status)")
            try? await Task.sleep(nanoseconds: 0_050_000_000)
            retryCount += 1
        }
    }

    func disconnect() async throws {
        guard let socket = self.socket else { return }
        guard socket.status == .connected else {
            print("[SocketIO] skipping close (status: \(socket.status))")
            return
        }
        print("[SocketIO] close")
        socket.disconnect()
    }

    func registerCallback(callback: any ClientChannelSocketIOCallback) {
        self.callback = callback
    }

    func unregisterCallback() {
        self.callback = nil
    }
}
