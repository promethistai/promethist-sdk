//
//  MicPowerCalculator.swift
//  PromethistSDK
//
//  Created by Vojtěch Vošmík on 02.08.2024.
//  Copyright © 2024 orgName. All rights reserved.
//

import Foundation

class MicPowerCalculator {

    private let minDb: Float = -46
    private let maxDb: Float = 0

    private let minPadding: CGFloat = 0
    private let maxPadding: CGFloat = 20
    private let minOpacity: CGFloat = 0.5
    private let maxOpacity: CGFloat = 1.0

    private func mapDbToUIValue(dbValue: Float, minDb: Float, maxDb: Float, minUI: CGFloat, maxUI: CGFloat) -> CGFloat {
        let normalizedDb = (dbValue - minDb) / (maxDb - minDb)
        let uiValue = CGFloat(normalizedDb) * (maxUI - minUI) + minUI
        return uiValue
    }

    func calculatePadding(_ currentDb: Float) -> CGFloat {
        let padding = mapDbToUIValue(dbValue: currentDb, minDb: minDb, maxDb: maxDb, minUI: minPadding, maxUI: maxPadding)
        let final = ((padding * 5) / CGFloat(100))
        return final
    }

    func calculateOpacity(_ currentDb: Float) -> CGFloat {
        let opacity = mapDbToUIValue(dbValue: currentDb, minDb: minDb, maxDb: maxDb, minUI: minOpacity, maxUI: maxOpacity)
        return opacity
    }
}
