//
//  AppleTechnicalReportProvider.swift
//  Promethist
//
//  Created by Vojtěch Vošmík on 20.11.2024.
//  Copyright © 2024 orgName. All rights reserved.
//

@_implementationOnly import shared
import AVFoundation

class AppleTechnicalReportProvider: TechnicalReportProvider {

    private let appConfig: AppConfig = Client().appConfig()

    func technicalReport(parameters: [String : String]) -> TechnicalReport {
        let memoryUsageBytes = getUsedMemory() ?? 0
        let memoryUsagePercent = getMemoryUsagePercentage() ?? 0.0
        let cpuUsagePercent = getCPUUsagePercentage() ?? 0.0
        let micPermission = isMicrophonePermissionGranted()
        let fps = (parameters["fps"] ?? "0.0").replacingOccurrences(of: ",", with: ".")
        let memory = (parameters["memory"] ?? "0.0").replacingOccurrences(of: ",", with: ".")

        let report = TechnicalReport(
            deviceId: Client().appConfig().deviceId,
            memoryUsageBytes: memoryUsageBytes,
            memoryUsagePercent: memoryUsagePercent,
            cpuUsagePercent: cpuUsagePercent,
            micPermission: micPermission,
            cameraPermission: false,
            videoMode: appConfig.videoMode,
            avatarQualityLevel: appConfig.avatarQualityLevel,
            arcFps: Float(fps) ?? 0.0,
            arcMemory: Float(memory) ?? 0.0,
            created: FunctionsKt.currentTimeInstant(),
            id: IDKt.doNewId()
        )
        return report
    }
}

private extension AppleTechnicalReportProvider {

    func getUsedMemory() -> Int64? {
        var info = mach_task_basic_info()
        var count = mach_msg_type_number_t(MemoryLayout.size(ofValue: info)) / 4

        let result = withUnsafeMutablePointer(to: &info) { infoPtr in
            infoPtr.withMemoryRebound(to: integer_t.self, capacity: Int(count)) { reboundInfoPtr in
                task_info(mach_task_self_,
                          task_flavor_t(MACH_TASK_BASIC_INFO),
                          reboundInfoPtr,
                          &count)
            }
        }

        guard result == KERN_SUCCESS else {
            return nil
        }

        return Int64(info.resident_size)
    }

    func getMemoryUsagePercentage() -> Double? {
        // Get used memory by the app
        var info = mach_task_basic_info()
        var count = mach_msg_type_number_t(MemoryLayout.size(ofValue: info)) / 4

        let result = withUnsafeMutablePointer(to: &info) { infoPtr in
            infoPtr.withMemoryRebound(to: integer_t.self, capacity: Int(count)) { reboundInfoPtr in
                task_info(mach_task_self_,
                          task_flavor_t(MACH_TASK_BASIC_INFO),
                          reboundInfoPtr,
                          &count)
            }
        }

        guard result == KERN_SUCCESS else {
            return nil
        }

        let usedMemory = Int64(info.resident_size) // Memory used by the app in bytes

        // Get total physical memory
        let totalMemory = ProcessInfo.processInfo.physicalMemory // Total memory in bytes

        // Calculate percentage
        let percentage = Double(usedMemory) / Double(totalMemory) * 100.0

        return percentage
    }

    func getCPUUsagePercentage() -> Double? {
        var threadsList: thread_act_array_t?
        var threadCount = mach_msg_type_number_t()
        var totalUsage = 0.0

        // Get list of all threads in the current task
        let result = task_threads(mach_task_self_, &threadsList, &threadCount)
        guard result == KERN_SUCCESS, let threads = threadsList else {
            return nil
        }

        for i in 0..<Int(threadCount) {
            var threadInfo = thread_basic_info()
            var threadInfoCount = mach_msg_type_number_t(THREAD_INFO_MAX)

            let threadResult = withUnsafeMutablePointer(to: &threadInfo) { threadInfoPtr in
                threadInfoPtr.withMemoryRebound(to: integer_t.self, capacity: Int(threadInfoCount)) {
                    thread_info(threads[i], thread_flavor_t(THREAD_BASIC_INFO), $0, &threadInfoCount)
                }
            }

            guard threadResult == KERN_SUCCESS else { continue }

            // Check if thread is idle
            if threadInfo.flags & TH_FLAGS_IDLE == 0 {
                // Add CPU usage for the thread
                totalUsage += Double(threadInfo.cpu_usage) / Double(TH_USAGE_SCALE) * 100.0
            }
        }

        // Deallocate thread list
        vm_deallocate(mach_task_self_, vm_address_t(bitPattern: threads), vm_size_t(threadCount) * UInt(MemoryLayout<thread_act_t>.size))

        return totalUsage
    }

    func isMicrophonePermissionGranted() -> Bool {
        let micPermissionStatus = AVCaptureDevice.authorizationStatus(for: .audio)

        switch micPermissionStatus {
        case .authorized:
            // Permission already granted
            return true
        case .denied, .restricted:
            // Permission denied or restricted
            return false
        case .notDetermined:
            // Cannot handle this synchronously, return false as a placeholder
            return false
        @unknown default:
            // Handle unknown cases conservatively
            return false
        }
    }
}
