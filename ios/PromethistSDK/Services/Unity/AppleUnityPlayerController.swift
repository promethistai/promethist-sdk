import Foundation
@_implementationOnly import shared

class AppleUnityPlayerController {

    static let shared = AppleUnityPlayerController()
    public var bridge: any AvatarPlayerBridge

    private let audioFileIOManager = AudioFileIOManager()
    private var isBlocked = false

    init() {
        self.bridge = AvatarPlayerBridgeMockImpl()
    }

    func setup() {
        observeUnityMessages()
    }

    func setBlocked(_ isBlocked: Bool) {
        self.isBlocked = isBlocked
    }

    func log(_ message: String) {
        print("🥷 UnityPlayer => \(message)")
    }
}

// MARK: NativeCallsProtocol implementation

#if !targetEnvironment(simulator)
extension AppleUnityPlayerController: NativeCallsProtocol {

    func observeUnityMessages() {
        NSClassFromString("FrameworkLibAPI")?.registerAPIforNativeCalls(self)
    }

    nonisolated func sendMessage(toMobileApp message: String) {
        log("Unity message received: \(message)")

        // Check avatar scene state
        switch message {
        case "sceneLoaded": appEvents.send(.sceneLoaded)
        case "downloadStarted": appEvents.send(.avatarDownloadState(state: .started))
        case "avatarLoaded" /*"downloadEnded"*/: appEvents.send(.avatarDownloadState(state: .finished))
        default: break
        }

        // Check avatar download progress
        if message.contains("downloadProgress"),
           let progressString = message.components(separatedBy: "progress:").last,
           let progress = Float(progressString.replacingOccurrences(of: ",", with: ".")) {
            appEvents.send(.avatarDownloadState(state: .inProgress(progress: progress / 100)))
        }

        bridge.onMessageReceived(message: message)

        if Client().appConfig().duplexEnabled || !DuplexSpeechRecognizerController.shared.allowDuplex {
            switch message {
            case "audioStarted":
                DuplexSpeechRecognizerController.shared.onAudioPlaybackStarted()
            case "audioEnded":
                DuplexSpeechRecognizerController.shared.onAudioPlaybackEnded()
            default: break
            }
        }
    }
}
#else

extension AppleUnityPlayerController {
    func observeUnityMessages() {}
    nonisolated func sendMessage(toMobileApp message: String) {}
}

#endif

// MARK: UnityPlayerController implementation

extension AppleUnityPlayerController: AvatarPlayerController {

    func setAudioBlocked(isBlocked: Bool) {
        self.isBlocked = isBlocked

        if isBlocked {
            sendMessage(nameObject: "AudioManager", functionName: "stopAudio", message: "")
        }
    }

    func getIsAudioBlocked() -> Bool {
        return self.isBlocked
    }

    func sendMessage(nameObject: String, functionName: String, message: String) {
        UnityEmbeddedSwift.sendUnityMessage(
            nameObject,
            methodName: functionName,
            message: message
        )
    }

    func onSceneLoaded() {}
}

private class AvatarPlayerBridgeMockImpl: AvatarPlayerBridge {
    func onMessageReceived(message: String) {}
}
