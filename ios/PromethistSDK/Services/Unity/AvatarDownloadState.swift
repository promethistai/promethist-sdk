//
//  UnityDownloadState.swift
//  Promethist
//
//  Created by Vojtěch Vošmík on 20.12.2024.
//  Copyright © 2024 orgName. All rights reserved.
//

public enum AvatarDownloadState {
    case started
    case inProgress(progress: Float)
    case finished
}
