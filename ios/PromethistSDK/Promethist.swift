//
//  Promethist.swift
//  PromethistSDK
//
//  Created by Vojtěch Vošmík on 21.05.2024.
//  Copyright © 2024 orgName. All rights reserved.
//

import Foundation
@_implementationOnly import shared

public func setTarget() {
    ClientTarget.shared.currentTarget = TargetKt.iosTarget
}
