import Foundation

class DeviceId {

    static func createDeviceId() -> String {
        "ios:" + String(format: "%08X%08X", arc4random(), arc4random())
    }
}
