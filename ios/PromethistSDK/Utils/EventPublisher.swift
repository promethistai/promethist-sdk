import Combine
import Foundation

public typealias EventPublisher<T> = PassthroughSubject<T, Never>
