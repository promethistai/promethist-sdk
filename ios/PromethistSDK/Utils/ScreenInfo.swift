import SwiftUI

final class ScreenInfo: ObservableObject {

    enum Orientation: String {
        case Portrait
        case Landscape
    }

    @Published var orientation: Orientation
    @Published var width: Double
    @Published var height: Double

    private let bounds = UIScreen.main.bounds
    private var _observer: NSObjectProtocol?

    init() {
        if UIWindow.isLandscape && UIDevice.current.userInterfaceIdiom == .pad {
            orientation = .Landscape
            width = bounds.height > bounds.width ? bounds.height : bounds.width
            height = bounds.height > bounds.width ? bounds.width : bounds.height
        } else {
            orientation = .Portrait
            width = bounds.height < bounds.width ? bounds.height : bounds.width
            height = bounds.height < bounds.width ? bounds.width : bounds.height
        }

        // unowned self because we unregister before self becomes invalid
        _observer = NotificationCenter.default.addObserver(forName: UIDevice.orientationDidChangeNotification, object: nil, queue: nil) { [unowned self] note in
            guard let device = note.object as? UIDevice else {
                return
            }
            if device.orientation.isPortrait || UIDevice.current.userInterfaceIdiom != .pad {
                orientation = .Portrait
                width = bounds.height < bounds.width ? bounds.height : bounds.width
                height = bounds.height < bounds.width ? bounds.width : bounds.height
            }
            else if device.orientation.isLandscape {
                orientation = .Landscape
                width = bounds.height > bounds.width ? bounds.height : bounds.width
                height = bounds.height > bounds.width ? bounds.width : bounds.height
            }
        }
    }

    deinit {
        if let observer = _observer {
            NotificationCenter.default.removeObserver(observer)
        }
    }
}

private extension UIWindow {

    static var isLandscape: Bool {
        if #available(iOS 13.0, *) {
            return UIApplication.shared.firstKeyWindow?
                .windowScene?
                .interfaceOrientation
                .isLandscape ?? false
        } else {
            return UIApplication.shared.statusBarOrientation.isLandscape
        }
    }
}
