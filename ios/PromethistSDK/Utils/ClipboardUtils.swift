//
//  ClipboardUtils.swift
//  Promethist
//
//  Created by Vojtěch Vošmík on 27.11.2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import UniformTypeIdentifiers
import UIKit

class ClipboardUtils {

    static let shared = ClipboardUtils()

    func copy(_ text: String) {
        UIPasteboard.general.setValue(text, forPasteboardType: UTType.plainText.identifier)
    }
}
