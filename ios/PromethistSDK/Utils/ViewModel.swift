import Combine

@MainActor
protocol ViewModel {
    
    // MARK: Lifecycle
    
    func onAppear()
    func onDisappear()
}

extension ViewModel {
    
    func onAppear() {
    }
    
    func onDisappear() {
    }
}
