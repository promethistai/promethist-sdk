//
//  AudioManager.swift
//  PromethistSDK
//
//  Created by Vojtěch Vošmík on 17.09.2024.
//  Copyright © 2024 orgName. All rights reserved.
//

import Foundation
import AVFoundation
@_implementationOnly import shared

class AudioManager {

    static func setupAudioSession() {
        let session = AVAudioSession.sharedInstance()

        if Client().appConfig().duplexEnabled {
            try? session.setCategory(
                AVAudioSession.Category.playAndRecord,
                mode: .voiceChat,
                options: [.mixWithOthers, .defaultToSpeaker, .allowBluetooth, .allowBluetoothA2DP]
            )
        } else {
            try? session.setCategory(
                AVAudioSession.Category.playAndRecord,
                mode: .voiceChat,
                options: [.mixWithOthers, .defaultToSpeaker, .allowBluetooth, .allowBluetoothA2DP]
            )
        }

        try? session.setActive(true)
    }
}
