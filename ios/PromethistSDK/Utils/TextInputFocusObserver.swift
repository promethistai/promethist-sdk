import Combine

class TextInputFocusObserver: ObservableObject {
    
    @Published var isFocused: Bool = false
}
