import SwiftUI
@_implementationOnly import shared

enum AppTheme {
    
    enum Fonts {
        
        /// Aktiv Grotesk, 30pt, bold
        static let heading1 = PrimaryFont.bold(size: 30)
        
        /// Aktiv Grotesk, 20pt, bold
        static let heading2 = PrimaryFont.bold(size: 20)
        
        /// System, 15pt, bold
        static let heading3 = PrimaryFont.medium(size: 15)
        
        /// System, 17.5pt, bold
        static let modal = PrimaryFont.medium(size: 17.5)
        
        /// System, 17.5pt, regular
        static let regular = PrimaryFont.regular(size: 17.5)
        
        /// System, 12.5pt, semibold
        static let minimal = PrimaryFont.medium(size: 12.5)
    }
    
    enum PrimaryFont {
        
        static func regular(size: Double) -> Font {
            Font(Resources().fonts.primaryFont.regular.uiFont(withSize: size))
        }
        
        static func medium(size: Double) -> Font {
            Font(Resources().fonts.primaryFont.medium.uiFont(withSize: size))
        }
        
        static func bold(size: Double) -> Font {
            Font(Resources().fonts.primaryFont.bold.uiFont(withSize: size))
        }
    }
    
    enum CornerRadius: CGFloat {
        
        case zero = 0
        
        /// 3pt
        case small = 3
        
        /// 6pt
        case medium = 6
        
        /// 10pt
        case large = 10
        
        /// 15pt
        case xlarge = 15
        
        /// 30pt
        case xxlarge = 30

        /// 44pt
        case xxxlarge = 38
    }
    
    enum Spacing: CGFloat {
        
        case zero = 0
        
        /// 2pt
        case xxsmall = 2
        
        /// 4pt
        case xsmall = 4
        
        /// 8pt
        case small = 8
        
        /// 12pt
        case mediumSmall = 12
        
        /// 16pt
        case medium = 16
        
        /// 20pt
        case mediumLarge = 20
        
        /// 24pt
        case large = 24
        
        /// 32pt
        case xlarge = 32
        
        /// 44pt
        case xxlarge = 44
        
        /// 64pt
        case xxxlarge = 64
    }
}
