//
//  PermissionsManager.swift
//  Promethist
//
//  Created by Vojtěch Vošmík on 04.02.2025.
//  Copyright © 2025 orgName. All rights reserved.
//

import Speech

class PermissionsManager {

    static let shared = PermissionsManager()

    private init() {}

    func getAudioPermissionsState() async -> AudioPermissionState {
        let hasPermissionToRecord = await AVAudioSession.sharedInstance().hasPermissionToRecord()
        let hasAuthorizationToRecognize = await SFSpeechRecognizer.hasAuthorizationToRecognize()

        guard hasPermissionToRecord else { return .denied }
        guard hasAuthorizationToRecognize else { return .denied }

        return .allowed
    }
}

enum AudioPermissionState {
    case allowed
    case denied
}
