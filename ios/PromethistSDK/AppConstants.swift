struct AppConstants {

    static let auth0Audience = "https://app.elysai.com"
    static let auth0Scope = "openid profile email offline_access"
}
