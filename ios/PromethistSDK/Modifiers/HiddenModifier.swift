//
//  HiddenModifier.swift
//  Promethist
//
//  Created by Vojtěch Vošmík on 20.11.2024.
//  Copyright © 2024 orgName. All rights reserved.
//

import SwiftUI

struct HiddenModifier: ViewModifier {
    let isHidden: Bool

    func body(content: Content) -> some View {
        Group {
            if !isHidden {
                content
            }
        }
    }
}

extension View {
    func isHidden(_ hidden: Bool) -> some View {
        self.modifier(HiddenModifier(isHidden: hidden))
    }
}
