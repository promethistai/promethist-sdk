import SwiftUI
@_implementationOnly import shared

struct ColorSchemeModifier: ViewModifier {

    var colorScheme: ColorScheme
    var appearance: Appearance

    func body(content: Content) -> some View {
        switch appearance {
        case .light:
            return content.preferredColorScheme(.light)
        case .dark:
            return content.preferredColorScheme(.dark)
        default:
            return content.preferredColorScheme(colorScheme)
        }
    }
}
