//
//  LottieAsset.swift
//  Promethist
//
//  Created by Vojtěch Vošmík on 23.12.2024.
//  Copyright © 2024 orgName. All rights reserved.
//

import SwiftUI

enum LottieAsset {
    case loading
    case loadingAlt

    var path: String {
        switch self {
        case .loading:
            return Bundle.main.path(forResource: "loading", ofType: "json")!
        case .loadingAlt:
            return Bundle.main.path(forResource: "loading_alt", ofType: "json")!
        }
    }
}
