//
//  UIApplication+Window.swift
//  Promethist
//
//  Created by Vojtěch Vošmík on 28.11.2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import UIKit

extension UIApplication {

    var firstKeyWindow: UIWindow? {
        return self.connectedScenes
            .filter { $0.activationState == .foregroundActive }
            .first(where: { $0 is UIWindowScene })
            .flatMap({ $0 as? UIWindowScene })?.windows
            .first(where: \.isKeyWindow)
    }
}
