//
//  Text+Shadowing.swift
//  Promethist
//
//  Created by Vojtěch Vošmík on 13.03.2024.
//  Copyright © 2024 orgName. All rights reserved.
//

import SwiftUI

extension View {

    func textShadowing() -> some View {
        self.shadow(color: .black, radius: 1)
    }
}
