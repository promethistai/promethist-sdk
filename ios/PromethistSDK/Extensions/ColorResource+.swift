import Foundation
import SwiftUI
@_implementationOnly import shared

extension shared.ColorResource {
    
    var color: SwiftUI.Color {
        Color(self.getUIColor())
    }
}
