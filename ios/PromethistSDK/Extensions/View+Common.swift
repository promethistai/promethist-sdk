import SwiftUI

extension View {
    
    func bottomCardSheet() -> some View {
        padding(.large)
        .frame(maxWidth: .infinity)
        .background(.regularMaterial)
        .cornerRadius(radius: AppTheme.CornerRadius.xxlarge.rawValue, corners: [.topLeft, .topRight])
        .shadow(radius: 20)
        .transition(.move(edge: .bottom))
    }
}
