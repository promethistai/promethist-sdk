@_implementationOnly import shared

//extension Message: Swift.Hashable {}

//extension PersonaModule_: Swift.Hashable {}

/*extension ErrorItem: Swift.Identifiable {}

extension PersonaItem: Swift.Identifiable {}

extension NetworkLogEntry: Swift.Identifiable {}

extension ProfileData.ProfileValue: Swift.Identifiable {}*/

struct ErrorItemWrapper: Identifiable {
    var id: String
    var errorItem: ErrorItem

    init(errorItem: ErrorItem) {
        self.id = Foundation.UUID().uuidString
        self.errorItem = errorItem
    }
}
