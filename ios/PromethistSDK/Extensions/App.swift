//
//  App.swift
//  Promethist
//
//  Created by Vojtěch Vošmík on 16.02.2024.
//  Copyright © 2024 orgName. All rights reserved.
//

import Foundation
import UIKit

func openPermissionSettings() {
    if let settingsUrl = URL(string: UIApplication.openSettingsURLString) {
       UIApplication.shared.open(settingsUrl)
    }
}
