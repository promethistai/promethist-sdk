import Speech

extension SFSpeechRecognizer {
    
    static func hasAuthorizationToRecognize() async -> Bool {
        await withCheckedContinuation { continuation in
            requestAuthorization { status in
                continuation.resume(returning: status == .authorized)
            }
        }
    }
    
    static var isDetermined: Bool {
        if SFSpeechRecognizer.authorizationStatus() == .notDetermined {
            return false
        }
        return true
    }
}
