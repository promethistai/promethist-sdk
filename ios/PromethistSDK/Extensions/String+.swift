//
//  String+.swift
//  Promethist
//
//  Created by Vojtěch Vošmík on 17.12.2024.
//  Copyright © 2024 orgName. All rights reserved.
//

extension String {

    func ifEmpty(_ value: String) -> String {
        if self.isEmpty {
            return value
        }
        return self
    }
}
