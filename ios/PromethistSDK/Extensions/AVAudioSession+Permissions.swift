import Foundation
import AVFAudio

extension AVAudioSession {
    
    func hasPermissionToRecord() async -> Bool {
        await withCheckedContinuation { continuation in
            requestRecordPermission { authorized in
                continuation.resume(returning: authorized)
            }
        }
    }
    
    static var isDetermined: Bool {
        if AVAudioSession.sharedInstance().recordPermission == .undetermined {
            return false
        }
        return true
    }
}
