import SwiftUI

extension View {
    
    func offset(x: AppTheme.Spacing = .zero, y: AppTheme.Spacing = .zero) -> some View {
        offset(x: x.rawValue, y: y.rawValue)
    }
    
    func cornerRadius(_ radius: AppTheme.CornerRadius) -> some View {
        cornerRadius(radius.rawValue)
    }
    
    func padding(_ spacing: AppTheme.Spacing) -> some View {
        padding(spacing.rawValue)
    }
    
    func padding(_ edges: Edge.Set, _ spacing: AppTheme.Spacing?) -> some View {
        padding(edges, spacing?.rawValue)
    }
    
    func contentPadding(_ value: AppTheme.Spacing = .large) -> some View {
        padding([.leading, .trailing], value)
    }
}

extension HStack {
    
    init(alignment: VerticalAlignment = .center, spacing: AppTheme.Spacing, @ViewBuilder content: () -> Content) {
        self.init(alignment: alignment, spacing: spacing.rawValue, content: content)
    }
}

extension LazyHStack {
    
    init(alignment: VerticalAlignment = .center, spacing: AppTheme.Spacing, @ViewBuilder content: () -> Content) {
        self.init(alignment: alignment, spacing: spacing.rawValue, pinnedViews: [], content: content)
    }
}

extension VStack {
    
    init(alignment: HorizontalAlignment = .center, spacing: AppTheme.Spacing, @ViewBuilder content: () -> Content) {
        self.init(alignment: alignment, spacing: spacing.rawValue, content: content)
    }
}

extension LazyVStack {
    
    init(alignment: HorizontalAlignment = .center, spacing: AppTheme.Spacing, @ViewBuilder content: () -> Content) {
        self.init(alignment: alignment, spacing: spacing.rawValue, pinnedViews: [], content: content)
    }
}
