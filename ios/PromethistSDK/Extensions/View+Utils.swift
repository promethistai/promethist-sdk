import SwiftUI

extension View {

    func toAny() -> AnyView {
        AnyView(self)
    }

    @MainActor
    func lifecycle<VM: ViewModel>(_ viewModel: VM) -> some View {
        self.onAppear {
            viewModel.onAppear()
        }
        .onDisappear {
            viewModel.onDisappear()
        }
    }
    
    @ViewBuilder
    func `if`<Content: View>(_ conditional: Bool, content: (Self) -> Content) -> some View {
         if conditional {
             content(self)
         } else {
             self
         }
     }

    func mediumSheet(fraction: Double = 0.55) -> some View {
        if #available(iOS 16, *) {
            return self.presentationDetents([.fraction(fraction)])
        } else {
            return self
        }
    }

    func sheetRaidus(_ radius: CGFloat) -> some View {
        if #available(iOS 16.4, *) {
            return self.presentationCornerRadius(radius)
        } else {
            return self
        }
    }
}

extension View {
    
    func withOverlay(
        isActive: Bool,
        alignment: Alignment = .center,
        @ViewBuilder content: @escaping () -> some View
    ) -> some View {
        self.overlay(
            Group {
                if isActive {
                    content()
                }
            },
            alignment: alignment
        )
    }
}

extension View {

    func onNavigationLinkTap(onTap: @escaping () -> Void) -> some View {
        simultaneousGesture(TapGesture().onEnded(onTap))
    }
}
