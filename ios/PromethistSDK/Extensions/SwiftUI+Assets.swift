import SwiftUI
@_implementationOnly import shared

extension Image {
    
    init(resource: KeyPath<Images, shared.ImageResource>) {
        self.init(uiImage: Resources().images[keyPath: resource].toUIImage()!)
    }
}

extension SwiftUI.Color {
    
    init(resource: KeyPath<Colors, shared.ColorResource>) {
        self.init(uiColor: Resources().colors[keyPath: resource].getUIColor())
    }
}

extension String {
    
    init(resource: KeyPath<Strings, shared.ResourceStringDesc>) {
        self.init(Resources().strings[keyPath: resource].localized())
    }
}

extension SwiftUI.Text {
    
    init(resource: KeyPath<Strings, shared.ResourceStringDesc>) {
        self.init(String(resource: resource))
    }
}

let regularMaterial = Resources().colors.regularMaterial.color.opacity(0.9)
let accentSecondary = Resources().colors.accentSecondary.color.opacity(
    ClientTarget.shared.currentTarget == .dfha ? 1 : 0.25
)
