@_implementationOnly import shared

extension NSMutableArray {
    
    var isEmpty: Bool {
        self.count < 1
    }
    
    func removeFirst() {
        guard !self.isEmpty else { return }
        self.removeObject(at: 0)
    }
}
