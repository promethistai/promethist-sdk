import SwiftUI
@_implementationOnly import shared

struct UnityPlayerView: View {

    let showPauseIndicator: Bool
    let isPaused: Bool

    var body: some View {
        ZStack(alignment: .center) {
            UnityViewRepresentable()

            /*if showPauseIndicator {
                pausedIndicator
            }*/
        }
    }

    @ViewBuilder private var pausedIndicator: some View {
        if isPaused {
            Image(resource: \.icPlay)
                .renderingMode(.template)
                .resizable()
                .scaledToFit()
                .scaleEffect(0.5)
                .shadow(radius: 10)
                .frame(width: 100, height: 100)
                .foregroundColor(Color.white)
                .opacity(0.3)
        }
    }
}
