import SwiftUI

struct UnityViewRepresentable: UIViewRepresentable, UnityViewDelegate {

    let manager: UnityViewManager
    let view = UIView()

    init() {
        self.manager = UnityViewManager.shared
    }

    func makeUIView(context: Context) -> some UIView {
        manager.delegate = self
        view.backgroundColor = UIColor.black
        return view
    }

    func updateUIView(_ uiView: UIViewType, context: Context) {}

    func onViewUpdated(view: UIView) {
        self.view.addSubview(view)
    }
}

class UnityViewManager {
    static let shared = UnityViewManager()
    var delegate: UnityViewDelegate?
    var isUnityViewActive: Bool = false
}

protocol UnityViewDelegate {
    func onViewUpdated(view: UIView)
}
