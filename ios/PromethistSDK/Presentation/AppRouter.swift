import Foundation
import SwiftUI
@_implementationOnly import Kingfisher
import shared

@MainActor
public struct PromethistRouter: View {

    @Environment(\.colorScheme) private var colorScheme
    
    @ObservedObject var appRouterModel: AppRouterModel
    @StateObject private var screenInfo = ScreenInfo()
    @StateObject private var appSettingsObserver = AppSettingsObserver()

    @StateObject private var uiStateHandler: ObservableUiStateHandler
    private var clientViewModel: ClientModelImpl

    public init() {
        let clientModel = ClientModelFactory.create()

        self.appRouterModel = AppRouterModel(clientViewModel: clientModel)

        let uiStateHandler = clientModel.uiStateHandler as! ObservableUiStateHandler
        self.clientViewModel = clientModel
        self._uiStateHandler = StateObject(wrappedValue: uiStateHandler)
    }
    
    public var body: some View {
        ZStack {
            switch appRouterModel.startPoint {
            case .main, .appKeyEntry:
                main
                if case .appKeyEntry = appRouterModel.startPoint {
                    appKeyEntry.ignoresSafeArea(.all)
                }
            case .splash:
                EmptyView()
            }
        }
        .lifecycle(appRouterModel)
        .addKeyboardVisibilityToEnvironment()
        .environmentObject(screenInfo)
        .environmentObject(appSettingsObserver)
        .accentColor(Color(resource: \.accent))
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .withOverlay(isActive: appRouterModel.isHomeScreenPresented, content: home)
        .withOverlay(isActive: appRouterModel.isShowingSplash, content: splash)
        .withOverlay(isActive: appRouterModel.isIntroPresented, content: intro)
        .withPermissionAlert(isPresented: $appRouterModel.isPermissionsAlertPresented)
        .sheet(
            isPresented: $appRouterModel.isAuthSheetPresented,
            onDismiss: appRouterModel.onAuthSheetDismiss,
            content: auth
        )
        .sheet(
            isPresented: $appRouterModel.isDevSheetPresented,
            onDismiss: { appRouterModel.isDevSheetPresented = false },
            content: dev
        )
        .modifier(ColorSchemeModifier(
            colorScheme: colorScheme,
            appearance: appSettingsObserver.appConfig.appearance
        ))
        .onReceive(NotificationCenter.default.publisher(for: UIApplication.willEnterForegroundNotification)) { _ in
            clientViewModel.handleEvent(event: ClientEventResume())
        }
        .onReceive(NotificationCenter.default.publisher(for: UIApplication.didEnterBackgroundNotification)) { _ in
            clientViewModel.handleEvent(event: ClientEventPause())
        }
        .onShake(perform: appRouterModel.onShake)
    }
    
    @ViewBuilder private var main: some View {
        NavigationView {
            PersonaView(
                viewModel: clientViewModel,
                appConfig: appSettingsObserver.appConfig,
                onHomeClick: {
                    if clientViewModel.config.isHomeScreenEnabled {
                        appRouterModel.showHomeScreen()
                    }
                },
                uiStateHandler: uiStateHandler
            )
        }
        .navigationViewStyle(StackNavigationViewStyle())
    }

    @ViewBuilder private func home() -> some View {
        NavigationStack {
            PersonaSelectionView(
                channel: clientViewModel.channel,
                onPersonaSelected: { item in
                    appRouterModel.hideHomeScreen()
                    clientViewModel.handleEvent(event: ClientEventSetPersona(
                        ref: item.ref,
                        avatarRef: item.avatarRef,
                        sendAsInput: true
                    ))
                },
                onReset: {
                    clientViewModel.handleEvent(event: ClientEventReset(sendAsInput: true))
                },
                onUpdateAvatarQulity: {
                    clientViewModel.handleEvent(event: ClientEventSyncSettings())
                },
                onSendTechnicalReport: {
                    clientViewModel.avatarPlayer.requestTechnicalReport()
                }
            )
        }
        .accentColor(Color(resource: \.accent))
        .navigationViewStyle(StackNavigationViewStyle())
        .navigationBarHidden(true)
    }

    @ViewBuilder private var appKeyEntry: some View {
        AppKeyEntryView {
            appRouterModel.updateStartPoint(.main)
            clientViewModel.handleEvent(event: ClientEventReset(
                sendAsInput: !clientViewModel.config.isHomeScreenEnabled
            ))
            if clientViewModel.config.isHomeScreenEnabled {
                appRouterModel.showHomeScreen()
            }
        }
    }

    @ViewBuilder private func splash() -> some View {
        SplashView(appRouterModel: appRouterModel)
    }

    @ViewBuilder private func intro() -> some View {
        IntroScreen(
            appRouterModel: appRouterModel,
            onFinish: appRouterModel.onIntroFinished
        )
    }

    @ViewBuilder private func auth() -> some View {
        AuthSheetView(
            isActive: $appRouterModel.isAuthSheetPresented,
            action: $appRouterModel.authSheetAction,
            shouldHandleCallback: {
                Client.shared.authClient().shouldHandleCallback(url: $0)
            },
            handleCallback: { url in
                Task {
                    if let result = try? await Client.shared.authClient().handleCallback(url: url) {
                        print("AuthClient: result \(result)")
                        print("AuthClient: result isSuccess \(result.isSuccess())")
                        appRouterModel.finishLogin(isSuccess: result.isSuccess())
                    }
                }
            }
        )
        .interactiveDismissDisabled(appRouterModel.isAuthRequired)
    }

    @ViewBuilder private func dev() -> some View {
        DevScreen()
    }
}
