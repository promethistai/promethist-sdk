import SwiftUI
import CodeScanner

struct QrScannerSheet: View {

    let onResult: (String) -> Void

    init(onResult: @escaping (String) -> Void) {
        self.onResult = onResult
    }

    var body: some View {
        VStack(alignment: .center, spacing: .medium) {
            Spacer()

            CodeScannerView(codeTypes: [.qr], simulatedData: "engine.promethist.ai") { response in
                switch response {
                case .success(let result):
                    onResult(result.string)
                case .failure(let error):
                    print(error.localizedDescription)
                }
            }
            .frame(width: qrScannerSize, height: qrScannerSize)
            .cornerRadius(26)

            Spacer()
        }
    }
}

private let qrScannerSize: CGFloat = 280
