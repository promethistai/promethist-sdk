import SwiftUI
@_implementationOnly import shared

struct AppKeyEntryView: View {

    private let appConfig: AppConfig
    private let onContinue: () -> Void

    @State var domainValue: String
    @State var setupNameValue: String
    @State private var invalidAppKeyAlert = false
    @State private var isQrScannerSheetPresented: Bool = false
    @FocusState private var focusedField: Field?

    var isValid: Bool {
        !domainValue.isEmpty && !setupNameValue.isEmpty
    }

    init(onContinue: @escaping () -> Void) {
        let appConfig: AppConfig = Client().appConfig()
        self.appConfig = appConfig
        self.domainValue = normalizeDomain(appConfig.engineDomain.ifEmpty(Client.shared.appConfig().url))
        self.setupNameValue = appConfig.engineSetupName.ifEmpty(Client.shared.appConfig().key)
        self.onContinue = onContinue
    }

    var body: some View {
        VStack {
            Spacer()
            
            VStack(alignment: .center, spacing: .medium) {
                Image(resource: \.appKeyEntryLogo)
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(height: 50)

                HStack {
                    VStack {
                        IconTextField(icon: \.icNetwork, title: String(resource: \.settings.engineUrl), text: $domainValue)
                            .focused($focusedField, equals: .domain)

                        IconTextField(icon: \.icKey, title: String(resource: \.settings.applicationKey), text: $setupNameValue)
                            .focused($focusedField, equals: .ref)
                    }

                    VStack(alignment: .center, spacing: .mediumSmall) {
                        Button(action: {
                            isQrScannerSheetPresented = true
                        }) {
                            Image(resource: \.icQrCode)
                                .renderingMode(.template)
                                .resizable()
                                .scaledToFit()
                                .scaleEffect(0.5)
                                .frame(width: 40, height: 40)
                                .foregroundColor(Color.accentColor)
                        }

                        Button(action: submit) {
                            Image(resource: \.icArrowRight)
                                .renderingMode(.template)
                                .resizable()
                                .scaledToFit()
                                .scaleEffect(0.5)
                                .frame(width: 40, height: 40)
                                .foregroundColor(Color.white)
                                .background(Color.accentColor)
                                .clipShape(Circle())
                        }
                        .disabled(!isValid)
                    }
                }
                .padding(.bottom, 60)
                .frame(minHeight: .zero)
            }
            .padding(.bottom, .small)
            .bottomCardSheet()
        }
        .background {
            if ClientDefaults.shared.showAppKeyBackgroundImage {
                Image(resource: \.appKeyEntryBackground)
                    .resizable()
                    .aspectRatio(contentMode: .fill)
            }
        }
        .background(Color.accentColor)
        .adaptsToKeyboard()
        .modifier(DismissingKeyboardModifier())
        .alert(isPresented: $invalidAppKeyAlert) {
            Alert(
                title: Text(resource: \.onboarding.invalidAppKey),
                message: Text(resource: \.onboarding.youEnteredInvalidAppKey),
                dismissButton: Alert.Button.default(Text("OK"), action: {
                    invalidAppKeyAlert = false
                })
            )
        }
        .sheet(isPresented: $isQrScannerSheetPresented) {
            QrScannerSheet { result in
                if let appLink = AppLinkParser.shared.parse(value: result) {
                    domainValue = appLink.engineDomain
                    setupNameValue = appLink.setupName
                }
                isQrScannerSheetPresented = false
            }
        }
        .onReceive(appEvents) { event in
            switch event {
            case .appKeyReceived(let appKey): 
                setupNameValue = appKey
            case .appLinkReceived(let engineDomain, let engineSetupName):
                domainValue = engineDomain
                setupNameValue = engineSetupName
            default: break
            }
        }
        .onSubmit {
            switch focusedField {
            case .domain: focusedField = .ref
            default: submit()
            }
        }
    }

    private func submit() {
        guard isValidAppKey(setupNameValue) else {
            invalidAppKeyAlert = true
            return
        }
        
        appConfig.engineDomain = normalizeDomain(domainValue)
        appConfig.engineSetupName = setupNameValue
        appConfig.preset = AppPreset.EngineV3()
        onContinue()
    }

    private func isValidAppKey(_ input: String) -> Bool {
        return input.count > 0
    }
}

fileprivate enum Field: Int {
    case domain, ref
}

fileprivate func normalizeDomain(_ input: String) -> String {
    input
        .replacingOccurrences(of: "https://", with: "")
        //.replacingOccurrences(of: "http://", with: "")
        .replacingOccurrences(of: "ws://", with: "")
        .replacingOccurrences(of: "wss://", with: "")
}

private let inactiveButtonColor = SwiftUI.Color(red: 189, green: 198, blue: 207)
