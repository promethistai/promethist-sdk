//
//  PersonaSelectionViewModel.swift
//  Promethist
//
//  Created by Vojtěch Vošmík on 11.02.2025.
//  Copyright © 2025 orgName. All rights reserved.
//

import SwiftUI
@_implementationOnly import shared

final class PersonaSelectionViewModel: ViewModel, ObservableObject {

    let channel: ClientChannel

    // MARK: - Lifecycle

    init(channel: ClientChannel) {
        self.channel = channel
    }

    func onAppear() {
        onIntent(.loadData)
    }

    // MARK: - State

    @Published var state = State()
    private(set) var events = EventPublisher<PersonaSelectionEvent>()

    struct State {
        var isLoading = false
        var isHistoryAvailable = false
        var selectedIndex: Int? = nil
        var personas: [ApplicationInfo.Persona] = []
    }

    // MARK: - Intent

    enum Intent {
        case loadData
        case setSelectionIndex(Int)
    }

    func onIntent(_ intent: Intent) {
        Task {
            switch intent {
            case .loadData: await loadData()
            case .setSelectionIndex(let index): state.selectedIndex = index
            }
        }
    }

    func getSelectedPersona() -> ApplicationInfo.Persona? {
        guard let selectedIndex = state.selectedIndex else { return nil }
        let personaItem = state.personas[selectedIndex]
        Client().appConfig().lastSelectedAvatarId = personaItem.avatarRef
        return personaItem
    }
}

private extension PersonaSelectionViewModel {

    func loadData() async {

        // History available

        let isHistoryAvailable = Client().appConfig().lastSelectedAvatarId != nil
        state.isHistoryAvailable = isHistoryAvailable

        // Personas

        if state.personas.isEmpty {
            state.isLoading = true
        }

        let applicationInfo = try? await channel.getApplicationInfo()
        let personas = applicationInfo?.personas ?? []
        state.personas = personas

        withAnimation(.easeIn(duration: 0.3)) {
            state.isLoading = false
        }

        // Auto scroll

        guard state.selectedIndex == nil else { return }
        try? await Task.sleep(nanoseconds: 0_300_000_000)
        let lastSelected = Client().appConfig().lastSelectedAvatarId
        let index = personas.firstIndex(where: { personaItem in
            personaItem.avatarRef == lastSelected
        })

        state.selectedIndex = index
        if #unavailable(iOS 17.0) {
            scrollToSelection(index: index)
        }
    }

    func scrollToSelection(index: Int?) {
        guard let index else { return }
        events.send(.scrollToPersonaWithIndex(index))
    }
}

public enum PersonaSelectionEvent {
    case scrollToPersonaWithIndex(Int)
}
