//
//  HistoryListView.swift
//  Promethist
//
//  Created by Vojtěch Vošmík on 11.02.2025.
//  Copyright © 2025 orgName. All rights reserved.
//

import SwiftUI
@_implementationOnly import shared

struct HistoryListView: View {

    var body: some View {
        ForEach(1..<7) { index in
            NavigationLink(destination: {
                HistoryDetailView(messages: [
                    Message(
                        uuid: "123",
                        isUser: false,
                        text: "Ahoj, jsem Ezra. Věděli jste že sloni mají tak dobrou kontrolu nad svým chobotem, že jím dokážou zvednout i minci ze země… ale zároveň jsou tak nemotorní, že se někdy sami zakopnou o vlastní nohu! 🐘 😂",
                        personaId: "ezra",
                        name: "Ezra",
                        image: nil
                    )
                ]).navigationBarHidden(true)
            }) {
                Text("Zajímavosti o slonech \(index)")
                    .frame(maxWidth: .infinity, alignment: .leading)
                    .promethistFont(size: 18, weight: .regular)
                    .foregroundStyle(Color(resource: \.personaTextPrimary))
                    .padding(.medium)
                    .overlay {
                        RoundedRectangle(cornerRadius: 10)
                            .stroke(Color(resource: \.personaBorderGrey), lineWidth: 1)
                    }
            }
            .padding(.horizontal, .mediumLarge)
        }
    }
}

struct HistoryTitleView: View {

    var body: some View {
        Text(resource: \.home.historyOfConversations)
            .font(AppTheme.PrimaryFont.medium(size: 24))
            .foregroundStyle(Color(resource: \.personaTextPrimary))
            .frame(maxWidth: .infinity, alignment: .leading)
            .lineLimit(1)
            .truncationMode(.tail)
            .padding(.horizontal, .mediumLarge)
            .padding(.top, .medium)
            .padding(.bottom, .small)
    }
}
