//
//  PersonaCarouselView.swift
//  Promethist
//
//  Created by Vojtěch Vošmík on 13.02.2025.
//  Copyright © 2025 orgName. All rights reserved.
//

import SwiftUI

struct PersonaCarouselView: View {
    
    @ObservedObject var viewModel: PersonaSelectionViewModel

    var body: some View {
        if #available(iOS 17.0, *) {
            pagingCarousel()
        } else {
            regularCarousel()
        }
    }

    @available(iOS 17.0, *)
    @ViewBuilder
    private func pagingCarousel() -> some View {
        ScrollView(.horizontal, showsIndicators: false) {
            LazyHStack(alignment: .center, spacing: .zero) {
                ForEach(Array(viewModel.state.personas.enumerated()), id: \.element) { index, item in
                    Button(action: {
                        viewModel.onIntent(.setSelectionIndex(index))
                        Vibration.selection.vibrate()
                    }) {
                        LargePersonaItemView(
                            item: item,
                            isSelected: viewModel.state.selectedIndex == index
                        )
                        .scaleEffect((viewModel.state.selectedIndex == index || viewModel.state.selectedIndex == nil) ? 1.0 : 0.88)
                        .animation(
                            .spring(response: 0.3, dampingFraction: 0.5, blendDuration: 0),
                            value: viewModel.state.selectedIndex
                        )
                    }
                    .id(index)
                }
            }
            .scrollTargetLayout()
            .padding(.horizontal, .mediumLarge)
            .padding(.xxsmall)
        }
        .frame(height: 310)
        .scrollTargetBehavior(.viewAligned)
        .scrollPosition(id: $viewModel.state.selectedIndex, anchor: .leading)
        .onChange(of: viewModel.state.selectedIndex) { _ in
            let generator = UISelectionFeedbackGenerator()
            generator.prepare()
            generator.selectionChanged()
        }
    }

    @ViewBuilder
    private func regularCarousel() -> some View {
        ScrollViewReader { scrollProxy in
            ScrollView(.horizontal, showsIndicators: false) {
                LazyHStack(alignment: .center, spacing: .medium) {
                    ForEach(Array(viewModel.state.personas.enumerated()), id: \.element) { index, item in
                        Button(action: {
                            viewModel.onIntent(.setSelectionIndex(index))
                            Vibration.selection.vibrate()
                        }) {
                            LargePersonaItemView(
                                item: item,
                                isSelected: viewModel.state.selectedIndex == index
                            )
                        }
                        .id(index)
                    }
                }
                .padding(.horizontal, .mediumLarge)
                .padding(.xxsmall)
            }
            .onReceive(viewModel.events) { event in
                switch event {
                case .scrollToPersonaWithIndex(let index):
                    withAnimation {
                        scrollProxy.scrollTo(index, anchor: .center)
                    }
                }
            }
        }
    }
}
