//
//  SmallPersonaItemView.swift
//  Promethist
//
//  Created by Vojtěch Vošmík on 04.02.2025.
//  Copyright © 2025 orgName. All rights reserved.
//

import SwiftUI
@_implementationOnly import Kingfisher
@_implementationOnly import shared

struct SmallPersonaItemView: View {

    let item: ApplicationInfo.Persona
    let isSelected: Bool

    private var borderColor: SwiftUI.Color {
        isSelected ? Color(resource: \.accent) : Color(resource: \.personaBorderGrey)
    }
    private var borderWidth: CGFloat {
        isSelected ? 3 : 1
    }

    var body: some View {
        HStack(alignment: .top, spacing: .medium) {
            Group {
                if let remoteUrl = URL(string: item.avatarImageUrl ?? "") {
                    KFImage(remoteUrl)
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                } else if let localImage = item.avatarImageLocal.toUIImage() {
                    Image(uiImage: localImage)
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                }
            }
            .frame(width: 92, height: 110, alignment: .bottom)
            .background(Color(resource: \.personaGrey2))
            .cornerRadius(.xlarge)

            VStack(alignment: .leading, spacing: .xsmall) {
                Text(item.name)
                    .promethistFont(size: 24, weight: .medium)
                    .foregroundStyle(Color(resource: \.personaTextPrimary))
                    .lineLimit(1)
                    .truncationMode(.tail)
                    .frame(maxWidth: .infinity, alignment: .leading)

                Text(/*item.description_ ??*/ "I am a description, please add me to the JSON and I will be displayed 🙂")
                    .promethistFont(size: 14)
                    .foregroundStyle(Color(resource: \.personaTextSecondary))
                    .lineLimit(3)
                    .truncationMode(.tail)
                    .multilineTextAlignment(.leading)
            }
            .frame(minHeight: 82, alignment: .center)
            .frame(maxWidth: .infinity)
            .padding(.top, 10)
        }
        .padding(.medium)
        .background {
            RoundedRectangle(cornerRadius: 20)
                .fill(Color(resource: \.personaSurface))
        }
        .overlay {
            RoundedRectangle(cornerRadius: 20)
                .stroke(borderColor, lineWidth: borderWidth)
                .animation(.easeInOut, value: isSelected)
        }
    }
}
