//
//  LargePersonaItemView.swift
//  Promethist
//
//  Created by Vojtěch Vošmík on 11.02.2025.
//  Copyright © 2025 orgName. All rights reserved.
//

import SwiftUI
@_implementationOnly import Kingfisher
@_implementationOnly import shared

struct LargePersonaItemView: View {

    let item: ApplicationInfo.Persona
    let isSelected: Bool

    private var borderColor: SwiftUI.Color {
        isSelected ? Color(resource: \.accent) : Color(resource: \.personaBorderGrey)
    }

    private var borderWidth: CGFloat {
        isSelected ? 3 : 1
    }

    private var width: CGFloat {
        let dynamic = UIScreen.main.bounds.width - (AppTheme.Spacing.mediumLarge.rawValue * 2)
        guard dynamic <= 330 else { return 330 }
        return dynamic
    }

    var body: some View {
        VStack(alignment: .center, spacing: .mediumSmall) {
            Group {
                if let remoteUrl = URL(string: item.avatarImageUrl ?? "") {
                    KFImage(remoteUrl)
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                } else if let localImage = item.avatarImageLocal.toUIImage() {
                    Image(uiImage: localImage)
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                }
            }
            .frame(width: 140, height: 180, alignment: .bottom)
            .background(Color(resource: \.personaGrey2))
            .cornerRadius(.xlarge)

            VStack(alignment: .center, spacing: .xsmall) {
                Text(item.name)
                    .promethistFont(size: 24, weight: .medium)
                    .foregroundStyle(Color(resource: \.personaTextPrimary))
                    .lineLimit(1)
                    .truncationMode(.tail)
                    .frame(maxWidth: .infinity, alignment: .center)

                Text(/*item.description_ ??*/ "I am a description, please add me to the JSON and I will be displayed 🙂")
                    .promethistFont(size: 14)
                    .foregroundStyle(Color(resource: \.personaTextSecondary))
                    .lineLimit(2)
                    .truncationMode(.tail)
                    .multilineTextAlignment(.center)
                    .frame(maxWidth: .infinity, alignment: .center)
            }
            .frame(maxWidth: .infinity, alignment: .center)
        }
        .padding(.medium)
        .frame(width: width, height: 302)
        .background {
            RoundedRectangle(cornerRadius: 20)
                .fill(Color(resource: \.personaSurface))
        }
        .overlay {
            RoundedRectangle(cornerRadius: 20)
                .stroke(borderColor, lineWidth: borderWidth)
                .animation(.easeInOut, value: isSelected)
        }
    }
}
