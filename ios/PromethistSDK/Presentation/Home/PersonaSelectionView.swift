import SwiftUI
@_implementationOnly import shared
import Kingfisher

struct PersonaSelectionView: View {

    @Environment(\.safeAreaInsets) private var safeAreaInsets
    @StateObject private var viewModel: PersonaSelectionViewModel
    let onPersonaSelected: (ApplicationInfo.Persona) -> Void

    let onReset: () -> Void
    let onUpdateAvatarQulity: () -> Void
    let onSendTechnicalReport: () -> Void

    private let authClient = Client.shared.authClient()

    init(
        channel: ClientChannel,
        onPersonaSelected: @escaping (ApplicationInfo.Persona) -> Void,
        onReset: @escaping () -> Void,
        onUpdateAvatarQulity: @escaping () -> Void,
        onSendTechnicalReport: @escaping () -> Void
    ) {
        self._viewModel = StateObject(wrappedValue: PersonaSelectionViewModel(channel: channel))
        self.onPersonaSelected = onPersonaSelected
        self.onReset = onReset
        self.onUpdateAvatarQulity = onUpdateAvatarQulity
        self.onSendTechnicalReport = onSendTechnicalReport
    }

    var body: some View {
        ScrollView(.vertical, showsIndicators: false) {
            VStack(spacing: .medium) {
                header
                    .padding(.top, .medium)
                    .padding(.bottom, .xsmall)
                    .padding(.horizontal, .mediumLarge)

                if !viewModel.state.isLoading {
                    personasList.padding(.bottom, 90)
                } else {
                    loading
                }
            }
        }
        .padding(.bottom, safeAreaInsets.bottom)
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .background(Color(resource: \.personaBackground))
        .overlay(alignment: .bottom) {
            PromethistPrimaryButton(text: String(resource: \.home.startConversation), isEnabled: viewModel.state.selectedIndex != nil) {
                let authClient = Client.shared.authClient()
                if !authClient.isLoggedIn() && ClientDefaults.shared.authPolicy == .required {
                    Task {
                        try? await authClient.logout()
                        appEvents.send(.authSessionUpdated)
                    }
                    return
                }

                guard let persona = viewModel.getSelectedPersona() else { return }
                onPersonaSelected(persona)
                Vibration.selection.vibrate()
            }
            .padding(.bottom, .small)
            .padding(.horizontal, .mediumLarge)
        }
        .withStatusBarStyle(.darkContent, onDisappear: .lightContent)
        .lifecycle(viewModel)
    }

    @ViewBuilder private var header: some View {
        VStack(alignment: .leading, spacing: .small) {
            HStack(alignment: .center, spacing: .zero) {
                Spacer()

                NavigationLink {
                    SettingsView(
                        onReset: onReset,
                        onUpdateAvatarQulity: onUpdateAvatarQulity,
                        onSendTechnicalReport: onSendTechnicalReport
                    )
                } label: {
                    PromethistCircleButton(
                        icon: \.personaSettings,
                        isActive: true
                    )
                    .overlay(
                        Circle()
                            .stroke(Color(resource: \.personaBorderGrey), lineWidth: 1)
                    )
                    .overlay {
                        if let avatarUrl = URL(string: "\(authClient.getStoredUserInfo()["avatarUrl"] ?? "")") {
                            KFImage(avatarUrl)
                                .resizable()
                                .clipped()
                                .clipShape(Circle())
                                .scaledToFit()
                        }
                    }
                }
            }

            Text(viewModel.state.isHistoryAvailable ? String(resource: \.home.titleAlt) : String(resource: \.home.title))
                .promethistFont(size: 40, weight: .bold)
                .foregroundStyle(Color(resource: \.personaTextPrimary))

            Text(resource: \.home.subtitle)
                .promethistFont(size: 18)
                .foregroundStyle(Color(resource: \.personaTextSecondary))
        }
        .frame(maxWidth: .infinity, alignment: .leading)
    }

    @ViewBuilder private var personasList: some View {
        if viewModel.state.isHistoryAvailable {
            LazyVStack(spacing: .mediumSmall) {
                PersonaCarouselView(viewModel: viewModel)
                HistoryTitleView()
                HistoryListView()
            }
        } else {
            LazyVStack(spacing: .medium) {
                ForEach(Array(viewModel.state.personas.enumerated()), id: \.element) { index, item in
                    Button(action: {
                        viewModel.onIntent(.setSelectionIndex(index))
                        Vibration.selection.vibrate()
                    }) {
                        SmallPersonaItemView(
                            item: item,
                            isSelected: viewModel.state.selectedIndex == index
                        )
                    }
                }
                .padding(.horizontal, .mediumLarge)
            }
        }
    }

    @ViewBuilder private var loading: some View {
        ZStack(alignment: .center) {
            ProgressView()
        }
        .frame(height: 400)
        .frame(maxWidth: .infinity)
    }
}
