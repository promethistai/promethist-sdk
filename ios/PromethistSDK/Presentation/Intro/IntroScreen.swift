//
//  IntroScreen.swift
//  Promethist
//
//  Created by Vojtěch Vošmík on 04.02.2025.
//  Copyright © 2025 orgName. All rights reserved.
//

import SwiftUI
import AVFoundation
@_implementationOnly import shared

struct IntroScreen: View {

    @ObservedObject var appRouterModel: AppRouterModel
    @State private var player: AVQueuePlayer
    @State private var playerOpacity: Double
    @State private var playbackStarted: Bool
    @State private var playbackObserver: NSObjectProtocol?

    let onFinish: () -> Void

    init(appRouterModel: AppRouterModel, onFinish: @escaping () -> Void) {
        self.appRouterModel = appRouterModel
        self.onFinish = onFinish

        let item = AVPlayerItem(asset: AVAsset(url: ClientDefaults.shared.introVideo.url))
        let player = AVQueuePlayer(playerItem: item)
        self.player = player
        self.playerOpacity = 0.0
        self.playbackStarted = false
    }

    var body: some View {
        VStack {
            Spacer()

            bottomBar
                .padding(.horizontal, .mediumLarge)
                .padding(.vertical, .medium)
        }
        .frame(maxWidth: .infinity)
        .background {
            GeometryReader { geometry in
                ZStack(alignment: .center) {
                    Color.black
                    VideoPlayerView(videoPlayer: player).opacity(playerOpacity)
                }
                .frame(width: geometry.size.width, height: geometry.size.height)
            }
            .ignoresSafeArea(.all, edges: .all)
        }
        .onAppear {
            guard !playbackStarted else { return }

            playbackObserver = NotificationCenter.default.addObserver(
                forName: .AVPlayerItemDidPlayToEndTime,
                object: player.currentItem,
                queue: .main
            ) { _ in
                onFinish()
                removeObserver()
            }

            player.play()
            Task {
                try? await Task.sleep(nanoseconds: 150_000_000)
                withAnimation(.easeInOut(duration: 0.4)) { playerOpacity = 1 }
            }
            playbackStarted = true
        }
    }

    @ViewBuilder private var bottomBar: some View {
        HStack(alignment: .center, spacing: .medium) {
            PromethistCircleButton(
                icon: \.personaSkip,
                onClick: {}
            )
            .opacity(0)

            ZStack(alignment: .center) {
                Text(resource: \.persona.introduction)
                    .foregroundStyle(.white)
                    .promethistFont(size: 17, weight: .medium)
                    .minimumScaleFactor(0.4)
                    .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .center)
                    .padding(.mediumSmall)
                    .lineLimit(1)
                    .truncationMode(.tail)
            }
            .frame(height: 60, alignment: .center)
            .glassyBackground()
            .cornerRadius(30)

            PromethistCircleButton(
                icon: \.personaSkip,
                onClick: {
                    removeObserver()
                    onFinish()
                }
            )
        }
    }

    func removeObserver() {
        if let observer = playbackObserver {
            NotificationCenter.default.removeObserver(observer)
            playbackObserver = nil
        }
    }
}
