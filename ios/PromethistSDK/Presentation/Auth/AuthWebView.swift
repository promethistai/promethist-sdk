//
//  WebView.swift
//  Promethist
//
//  Created by Vojtěch Vošmík on 15.10.2024.
//  Copyright © 2024 orgName. All rights reserved.
//

import SwiftUI
@preconcurrency import WebKit
@_implementationOnly import shared

struct AuthWebView: UIViewRepresentable {

    let webView: WKWebView
    let url: URL
    var shouldHandleCallback: (String) -> Bool
    var handleCallback: (String) -> Void
    var onProgressUpdate: (Double) -> Void

    init(
        url: URL,
        shouldHandleCallback: @escaping (String) -> Bool,
        handleCallback: @escaping (String) -> Void,
        onProgressUpdate: @escaping (Double) -> Void
    ) {
        webView = WKWebView(frame: .zero)
        self.url = url
        self.shouldHandleCallback = shouldHandleCallback
        self.handleCallback = handleCallback
        self.onProgressUpdate = onProgressUpdate
    }

    class Coordinator: NSObject, WKNavigationDelegate {
        var parent: AuthWebView
        var webView: WKWebView
        var shouldHandleCallback: (String) -> Bool
        var handleCallback: (String) -> Void
        var onProgressUpdate: (Double) -> Void

        init(
            parent: AuthWebView,
            webView: WKWebView,
            shouldHandleCallback: @escaping (String) -> Bool,
            handleCallback: @escaping (String) -> Void,
            onProgressUpdate: @escaping (Double) -> Void
        ) {
            self.parent = parent
            self.webView = webView
            self.shouldHandleCallback = shouldHandleCallback
            self.handleCallback = handleCallback
            self.onProgressUpdate = onProgressUpdate
        }

        func setup() {
            webView.addObserver(self, forKeyPath: #keyPath(WKWebView.estimatedProgress), options: .new, context: nil)
        }

        func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
            self.onProgressUpdate(1.0)
        }

        func webView(
            _ webView: WKWebView,
            decidePolicyFor navigationAction: WKNavigationAction,
            decisionHandler: @escaping (WKNavigationActionPolicy) -> Void
        ) {
            let request = navigationAction.request
            if let url = request.url?.absoluteString {
                if shouldHandleCallback(url) {
                    handleCallback(url)
                    decisionHandler(WKNavigationActionPolicy.cancel)
                    return
                }
            }

            decisionHandler(WKNavigationActionPolicy.allow)
        }

        override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
            if keyPath == "estimatedProgress" {
                let progress = webView.estimatedProgress
                self.onProgressUpdate(progress)
            }
        }
    }

    func makeCoordinator() -> Coordinator {
        let coordinator = Coordinator(
            parent: self,
            webView: webView,
            shouldHandleCallback: shouldHandleCallback,
            handleCallback: handleCallback,
            onProgressUpdate: onProgressUpdate
        )
        coordinator.setup()
        return coordinator
    }

    func makeUIView(context: Context) -> WKWebView {
        webView.customUserAgent = "Mozilla/5.0 (iPhone; CPU iPhone OS 15_0 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/15.0 Mobile/15E148 Safari/604.1"
        webView.navigationDelegate = context.coordinator
        webView.load(URLRequest(url: url))
        return webView
    }

    func updateUIView(_ uiView: WKWebView, context: Context) {}
}
