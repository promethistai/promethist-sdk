//
//  AuthSheetView.swift
//  Promethist
//
//  Created by Vojtěch Vošmík on 15.10.2024.
//  Copyright © 2024 orgName. All rights reserved.
//

import SwiftUI
@_implementationOnly import shared

struct AuthSheetView: View {

    @Binding var isActive: Bool
    @Binding var action: AuthSheetAction

    @State private var isWelcomePresented: Bool = ClientDefaults.shared.authPolicy == .required
    @State private var isLoading: Bool = false

    let shouldHandleCallback: (Ktor_httpUrl) -> Bool
    let handleCallback: (Ktor_httpUrl) -> Void

    private var url: String {
        Client.shared.authClient().getAuthorizationUrl().getString()
    }

    var body: some View {
        NavigationView {
            isWelcomePresented ? welcome.toAny() : webView.toAny()
        }
        .onAppear {
            action = .nothing
        }
    }

    @ViewBuilder private var webView: some View {
        AuthWebView(
            url: URL(string: url)!,
            shouldHandleCallback: { url in
                let ktorUrl = UrlExtensionKt.toKtorUrl(url)
                return shouldHandleCallback(ktorUrl)
            },
            handleCallback: { url in
                let ktorUrl = UrlExtensionKt.toKtorUrl(url)
                handleCallback(ktorUrl)
            },
            onProgressUpdate: { progress in
                withAnimation {
                    self.isLoading = progress < 1
                }
            }
        )
        .withOverlay(isActive: isLoading, content: loading)
    }

    @ViewBuilder private var welcome: some View {
        ZStack(alignment: .center) {
            VStack(alignment: .center, spacing: .medium) {
                Text(resource: \.auth.welcome)
                    .promethistFont(size: 40, weight: .bold)
                    .foregroundStyle(Color(resource: \.personaTextPrimary))

                Text(resource: \.auth.required)
                    .promethistFont(size: 18)
                    .foregroundStyle(Color(resource: \.personaTextSecondary))
                    .multilineTextAlignment(.center)
            }
            .padding(.mediumLarge)
        }
        .frame(maxWidth: .infinity, alignment: .center)
        .frame(maxHeight: .infinity, alignment: .center)
        .background(Color(resource: \.personaBackground))
        .overlay(alignment: .bottom) {
            PromethistPrimaryButton(text: String(resource: \.auth.requiredLogin)) {
                withAnimation {
                    isWelcomePresented = false
                }
            }
            .padding(.bottom, .small)
            .padding(.horizontal, .mediumLarge)
        }
    }

    @ViewBuilder private func loading() -> some View {
        VStack(alignment: .center, spacing: .xxlarge) {
            Spacer()
            ProgressView()
            Spacer()
        }
        .frame(maxWidth: .infinity, alignment: .center)
        .background(Color(resource: \.personaBackground))
    }
}

public enum AuthSheetAction {
    case cancel
    case nothing
}
