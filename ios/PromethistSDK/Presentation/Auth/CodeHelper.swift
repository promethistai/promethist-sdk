//
//  JwtHelper.swift
//  Promethist
//
//  Created by Vojtěch Vošmík on 23.10.2024.
//  Copyright © 2024 orgName. All rights reserved.
//

class CodeHelper {

    class func isValidCode(_ code: String) -> Bool {
        let parts = code.split(separator: ".")

        if parts.count != 3 {
            return false
        }

        return true
    }
}
