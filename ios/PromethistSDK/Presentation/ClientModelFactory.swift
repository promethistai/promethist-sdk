import SwiftUI
import shared

public class ClientModelFactory {

    @MainActor
    public static func create() -> ClientModelImpl {
        // MARK: - config
        let config = ClientModelConfig(
            isHomeScreenEnabled: true
        )

        // MARK: - uiStateHandler
        let uiStateHandler = ObservableUiStateHandler()

        // MARK: - avatarPlayer
        let unityController = AppleUnityPlayerController.shared
        unityController.setup()
        let avatarEventsApi = AvatarEventsApiV1Impl(controller: unityController)
        let avatarPlayer = AvatarPlayerImpl(controller: unityController, api: avatarEventsApi)
        unityController.bridge = avatarPlayer

        // MARK: - speechRecognizer
        let speechRecognizerController: SpeechRecognizerController = DuplexSpeechRecognizerController.shared
        DuplexSpeechRecognizerController.shared.allowDuplex = Client().appConfig().duplexEnabled
        let speechRecognizer = SpeechRecognizer(controller: speechRecognizerController)
        speechRecognizerController.setCallback(callback: speechRecognizer)

        // MARK: - technicalReportProvider
        let technicalReportProvider = AppleTechnicalReportProvider()

        // MARK: - channel
        //let socketIOController = AppleSocketIOController()
        let clientChannel: ClientChannel = WebSocketClientChannel() //SocketIOClientChannel(controller: socketIOController)

        return ClientModelImpl(
            config: config,
            uiStateHandler: uiStateHandler,
            avatarPlayer: avatarPlayer,
            speechRecognizer: speechRecognizer,
            technicalReportProvider: technicalReportProvider,
            channel: clientChannel
        )
    }
}
