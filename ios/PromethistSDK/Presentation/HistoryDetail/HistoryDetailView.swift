//
//  HistoryDetailView.swift
//  Promethist
//
//  Created by Vojtěch Vošmík on 11.02.2025.
//  Copyright © 2025 orgName. All rights reserved.
//

import SwiftUI
@_implementationOnly import shared

struct HistoryDetailView: View {

    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @StateObject private var textInputFocusObserver = TextInputFocusObserver()

    let messages: [Message]

    init(messages: [Message]) {
        self.messages = messages
    }

    var body: some View {
        ZStack {
            chatMode
        }
        .background(background)
        .overlay(alignment: .top, content: toolbar)
    }

    @ViewBuilder private var chatMode: some View {
        ZStack {
            TranscriptionRoot(messages: messages)
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .center)
        .environmentObject(textInputFocusObserver)
    }

    @ViewBuilder private func toolbar() -> some View {
        ZStack(alignment: .top) {
            PromethistTopFadeView(isActive: true)

            HStack(alignment: .center, spacing: .small) {
                PromethistCircleButton(
                    icon: \.personaArrowBack,
                    onClick: {
                        presentationMode.wrappedValue.dismiss()
                    }
                )

                Spacer()
            }
            .padding(.horizontal, .mediumLarge)
            .padding(.vertical, .mediumSmall)
        }
    }

    @ViewBuilder private var background: some View {
        Image(uiImage: PersonaResourcesManager().getBackground(personaId: .ezra).toUIImage()!) // TODO
            .resizable()
            .frame(maxWidth: .infinity, maxHeight: .infinity)
            .scaledToFill()
            .blur(radius: 25, opaque: true)
            .ignoresSafeArea(.all, edges: .all)
    }
}
