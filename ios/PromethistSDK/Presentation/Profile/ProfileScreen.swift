import SwiftUI
@_implementationOnly import Kingfisher

struct ProfileScreen: View {

    @Environment(\.dismiss) var dismiss
    @StateObject var viewModel = ProfileViewModel()

    var body: some View {
        VStack {
            avatar

            VStack(alignment: .center, spacing: .small) {
                Text(viewModel.isLoggedIn ? (viewModel.name ?? "Guest") : "Guest")
                    .font(.system(size: 22, weight: .medium))

                if let email = viewModel.email {
                    Text(email)
                        .font(.system(size: 16, weight: .regular))
                        .foregroundStyle(.gray)
                }
            }
            .padding(.vertical, .mediumSmall)

            form
        }
        .lifecycle(viewModel)
        .withStatusBarStyle(.darkContent)
    }

    @ViewBuilder private var avatar: some View {
        Group {
            if let avatar = viewModel.avatarUrl, viewModel.isLoggedIn {
                KFImage(avatar)
                    .resizable()
                    .cornerRadius(avatarSize / 2)
            } else {
              Image(resource: \.personCropCircle).resizable()
            }
        }
        .aspectRatio(contentMode: .fill)
        .frame(width: avatarSize, height: avatarSize)
    }

    @ViewBuilder private var form: some View {
        Form {
            if viewModel.showLogin {
                loginSection
            }

            if viewModel.showProfileInfo {
                profileInfo
            }

            Section {
                Text(viewModel.deviceId)
                    .contextMenu {
                        Button(action: {
                            UIPasteboard.general.string = viewModel.deviceId
                        }) {
                            Text(resource: \.profile.copyToClipboard)
                            Image(systemName: copySystemName)
                        }
                     }
            } header: {
                Text(resource: \.profile.deviceId)
            } footer: {
                Text(resource: \.profile.longTapToCopy)
            }
        }
    }

    @ViewBuilder private var profileInfo: some View {
        Section {
            ForEach(viewModel.profileData.values, id: \.self) { value in
                profileInfoItem(key: value.key, value: value.value)
            }
        }
    }

    @ViewBuilder private func profileInfoItem(key: String, value: String) -> some View {
        HStack {
            Text(key).font(.system(size: 15, weight: .regular))

            Spacer()

            Text(value).font(.system(size: 15, weight: .medium))
        }
    }

    @ViewBuilder private var loginSection: some View {
        Section {
            if viewModel.isLoginOptionAvailable {
                Button(action: {
                    dismiss()
                    appEvents.send(.openAuthWebView)
                }) {
                    Text(resource: \.profile.signIn)
                }
            } else if viewModel.isLoggedIn {
                Button(action: {
                    viewModel.logout()
                    dismiss()
                    appEvents.send(.authSessionUpdated)
                }) {
                    Text(resource: \.profile.signOut)
                }
            }
        }
    }
}

private let avatarSize: CGFloat = 100
private let copySystemName = "doc.on.doc"
