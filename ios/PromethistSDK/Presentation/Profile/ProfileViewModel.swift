@_implementationOnly import shared
import Combine

class ProfileViewModel: ViewModel, ObservableObject {

    private let authClient = Client.shared.authClient()
    private let currentTarget = ClientTarget.shared.currentTarget
    @Published var deviceId: String = ""
    @Published var profileData: ProfileData = Client().appConfig().profileData ?? ProfileData.companion.Empty

    var isLoggedIn: Bool {
        authClient.isLoggedIn()
    }

    var isLoginOptionAvailable: Bool {
        !authClient.isLoggedIn() && ClientDefaults.shared.authPolicy != .disabled
    }

    var userInfo: [String : Any] {
        authClient.getStoredUserInfo()
    }

    var avatarUrl: URL? {
        URL(string: (userInfo["avatarUrl"] as? String ?? ""))
    }

    var name: String? {
        if let name = userInfo["name"] as? String {
            return name
        }
        return nil
    }

    var email: String? {
        if let name = userInfo["email"] as? String {
            return name
        }
        return nil
    }

    var showProfileInfo: Bool {
        switch currentTarget {
        case .dfha: return true
        default: return false
        }
    }

    var showLogin: Bool {
        switch currentTarget {
        case .dfha: return false
        default: return true
        }
    }

    func onAppear() {
        deviceId = DeviceId.createDeviceId()
        Task {
            let _ = try? await authClient.getUserInfo()
        }
    }

    func logout() {
        WebCacheCleaner.clean()
        Task {
            try? await authClient.logout()
        }
    }
}
