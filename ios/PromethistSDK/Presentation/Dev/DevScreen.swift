//
//  DevScreen.swift
//  Promethist
//
//  Created by Vojtěch Vošmík on 10.03.2025.
//  Copyright © 2025 orgName. All rights reserved.
//

import SwiftUI
@_implementationOnly import shared

import AVFoundation
import AVKit

struct DevScreen: View {

    var body: some View {
        NavigationView {
            Form {
                Section("Log") {
                    NavigationLink(destination: {
                        DevApplicationLogsScreen()
                    }) {
                        Text("Application logs 📝")
                    }
                }
            }
            .navigationTitle("Developer Options")
            .navigationBarTitleDisplayMode(.inline)
        }
    }
}

private struct DevApplicationLogsScreen: View {

    @State var logs: String = LoggerV3.shared.getLogs()
    @State var isShareSheetPresented = false

    var body: some View {
        ScrollView {
            Text(logs)
                .font(.system(size: 13))
                .frame(maxWidth: .infinity, alignment: .topLeading)
                .padding(.small)
        }
        .refreshable {
            logs = LoggerV3.shared.getLogs()
        }
        .onAppear {
            logs = LoggerV3.shared.getLogs()
        }
        .toolbar {
            Button(action: {
                isShareSheetPresented.toggle()
            }) {
                Text("Share")
            }
        }
        .sheet(isPresented: $isShareSheetPresented) {
            ShareSheet(items: [logs], applicationActivities: nil)
        }
    }
}
