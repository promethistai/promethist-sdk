import SwiftUI
@_implementationOnly import shared

struct TermsSheetView: View {
    
    let onAccept: () -> Void
    let onDecline: () -> Void
    
    var body: some View {
        VStack(spacing: .medium) {
            HStack {
                Text(resource: \.onboarding.termsAndConditionsTitle)
                    .font(AppTheme.Fonts.heading1)
                
                Spacer()
            }
            
            Text(resource: \.onboarding.termsAndConditionsText)

            HStack {
                IconButton(
                    text: String(resource: \.onboarding.decline),
                    onClick: onDecline
                )
                IconButton(
                    text: String(resource: \.onboarding.accept),
                    onClick: onAccept
                )
            }
        }
        .padding(.bottom, .small)
        .bottomCardSheet()
    }
}
