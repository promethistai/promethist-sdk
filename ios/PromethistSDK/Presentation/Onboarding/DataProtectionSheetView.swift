import SwiftUI
@_implementationOnly import shared

struct DataProtectionSheetView: View {
    
    let onAccept: () -> Void
    let onDecline: () -> Void
    
    var body: some View {
        VStack(spacing: .medium) {
            HStack {
                Text(resource: \.onboarding.dataProtectionTitle)
                     .font(AppTheme.Fonts.heading1)
                Spacer()
            }
            
            Text(resource: \.onboarding.dataProtectionText)
            
            HStack {
                IconButton(
                    text: String(resource: \.onboarding.dissent),
                    onClick: onDecline
                )
                IconButton(
                    text: String(resource: \.onboarding.consent),
                    onClick: onAccept
                )
            }
        }
        .padding(.bottom, .small)
        .bottomCardSheet()
    }
}
