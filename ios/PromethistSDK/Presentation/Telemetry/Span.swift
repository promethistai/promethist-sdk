//
//  Span.swift
//  Promethist
//
//  Created by Vojtěch Vošmík on 02.04.2024.
//  Copyright © 2024 orgName. All rights reserved.
//

@_implementationOnly import shared
@_implementationOnly import OpenTelemetryApi

class SpanImpl: shared.Span {

    let span: OpenTelemetryApi.Span

    init(span: OpenTelemetryApi.Span) {
        self.span = span
    }

    func end() {
        span.end()
    }

    func recordException(exception: KotlinThrowable) -> shared.Span {
        span.status = .error(description: "\(exception)")
        return self
    }

    func getSpanContext() -> shared.SpanContext {
        return SpanContextImpl(spanContext: span.context)
    }
    
    func setAttribute(key: String, value: Bool) -> shared.Span {
        span.setAttribute(key: key, value: .bool(value))
        return self
    }
    
    func setAttribute(key: String, value_ value: Double) -> shared.Span {
        span.setAttribute(key: key, value: .double(value))
        return self
    }
    
    func setAttribute(key: String, value__ value: Int64) -> shared.Span {
        span.setAttribute(key: key, value: .int(Int(value)))
        return self
    }
    
    func setAttribute(key: String, value___ value: String) -> shared.Span {
        span.setAttribute(key: key, value: .string(value))
        return self
    }
}
