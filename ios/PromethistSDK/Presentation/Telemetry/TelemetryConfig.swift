//
//  TelemetryConfig.swift
//  Promethist
//
//  Created by Vojtěch Vošmík on 04.04.2024.
//  Copyright © 2024 orgName. All rights reserved.
//

@_implementationOnly import shared

class TelemetryConfig {

    static let serviceName = "promethist-ios"
    static let endpoint = Client().appConfig().otelUrl
    static let path = "/v1/traces"
    static let instrumentationScopeName = "promethist-ios"
    static let instrumentationScopeVersion = "semver:0.1.0"
}
