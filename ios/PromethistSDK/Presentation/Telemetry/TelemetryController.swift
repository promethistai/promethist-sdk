@_implementationOnly import shared
@_implementationOnly import OpenTelemetrySdk
@_implementationOnly import OpenTelemetryApi
@_implementationOnly import OpenTelemetryProtocolExporterHttp
@_implementationOnly import StdoutExporter

class TelemetryControllerImpl: TelemetryController {

    private var tracer: Tracer? = nil

    func register(name: String, endpoint: String) {
        guard let url = URL(string: endpoint) else { return }

        let endpoint = endpoint + TelemetryConfig.path
        let resource = Resource(attributes: [
            ResourceAttributes.serviceName.rawValue: AttributeValue.string(name),
        ])

        let otlpHttpTraceExporter = OtlpHttpTraceExporter(endpoint: url)
        let stdoutExporter = StdoutExporter()
        let spanExporter = MultiSpanExporter(spanExporters: [otlpHttpTraceExporter, stdoutExporter])
        let spanProcessor = SimpleSpanProcessor(spanExporter: spanExporter)
        let tracerProvider = TracerProviderBuilder()
            .add(spanProcessor: spanProcessor)
            .with(resource: resource)
            .build()

        OpenTelemetry.registerTracerProvider(tracerProvider: tracerProvider)
        tracer = OpenTelemetry.instance.tracerProvider.get(
            instrumentationName: TelemetryConfig.instrumentationScopeName,
            instrumentationVersion: TelemetryConfig.instrumentationScopeVersion
        )
    }

    func getTrace() -> Trace {
        OpenTelemetryTrace(tracer: tracer!)
    }

    func getSuspendableTrace() -> SuspendableTrace {
        OpenTelemetrySuspendableTrace(tracer: tracer!)
    }
}
