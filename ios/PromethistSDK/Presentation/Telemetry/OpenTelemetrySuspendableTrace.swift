@_implementationOnly import shared
@_implementationOnly import OpenTelemetrySdk
@_implementationOnly import OpenTelemetryApi

class OpenTelemetrySuspendableTrace: SuspendableTrace {

    let tracer: Tracer

    init(tracer: Tracer) {
        self.tracer = tracer
    }

    private var currentSpanImpl: shared.Span? = nil

    var currentSpan: shared.Span {
        currentSpanImpl!
    }

    private func createSpan(
        name: String?,
        isRoot: Bool,
        links: Set<AnyHashable>
    ) -> shared.Span {
        let spanBuilder = tracer.spanBuilder(spanName: name ?? "")

        if isRoot {
            spanBuilder.setNoParent()
            spanBuilder.setActive(true)
        }
        for link in links {
            spanBuilder.addLink(spanContext: (link as! OpenTelemetryApi.Span).context)
        }

        let span = spanBuilder.startSpan()
        OpenTelemetry.instance.contextProvider.setActiveSpan(span)

        return SpanImpl(span: span)
    }

    func withSpan(name: String?, isRoot: Bool, links: Set<AnyHashable>, block: SuspendableTraceBlock) async throws -> Any? {
        let span = createSpan(name: name, isRoot: isRoot, links: links)
        let spanImpl = (span as! SpanImpl).span
        defer {
            spanImpl.end()
        }
        return try await block.process(span: span)
    }

    func withSpan(span: shared.Span, name: String, block: SuspendableTraceBlock) async throws -> Any? {
        try await withSpan(traceId: span.getSpanContext().getTraceId(), spanId: span.getSpanContext().getSpanId(), name: name, block: block)
    }

    func withSpan(traceId: String, spanId: String, name: String, block: SuspendableTraceBlock) async throws -> Any? {
        let currentSpan = SpanContext.createFromRemoteParent(
            traceId: TraceId(fromHexString: traceId),
            spanId: SpanId(fromHexString: spanId),
            traceFlags: TraceFlags().settingIsSampled(true),
            traceState: TraceState.init()
        )

        let spanBuilder = tracer.spanBuilder(spanName: name)
        spanBuilder.setParent(currentSpan)
        spanBuilder.setActive(true)

        let span = spanBuilder.startSpan()

        let spanImpl = SpanImpl(span: span)
        defer {
            span.end()
        }
        return try await block.process(span: spanImpl)
    }

    func startSpan(name: String?, isRoot: Bool, links: Set<AnyHashable>) -> shared.Span {
        let span = createSpan(name: name, isRoot: isRoot, links: links)
        return span
    }

    func startSpan(span: shared.Span, name: String) -> shared.Span {
        let traceId = TraceId(fromHexString: span.getSpanContext().getTraceId())
        let spanId = SpanId(fromHexString: span.getSpanContext().getSpanId())

        let currentSpan = SpanContext.createFromRemoteParent(
            traceId: traceId,
            spanId: spanId,
            traceFlags: TraceFlags().settingIsSampled(true),
            traceState: TraceState.init()
        )

        let spanBuilder = tracer.spanBuilder(spanName: name)
        spanBuilder.setParent(currentSpan)
        spanBuilder.setActive(true)

        let span = spanBuilder.startSpan()

        let spanImpl = SpanImpl(span: span)

        return spanImpl
    }

    func endSpan(span: shared.Span) {
        (span as! SpanImpl).span.end()
    }
}
