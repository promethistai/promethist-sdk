//
//  OpenTelemetryTrace.swift
//  Promethist
//
//  Created by Vojtěch Vošmík on 02.04.2024.
//  Copyright © 2024 orgName. All rights reserved.
//

@_implementationOnly import shared
@_implementationOnly import OpenTelemetrySdk
@_implementationOnly import OpenTelemetryApi

class OpenTelemetryTrace: Trace {
    
    let tracer: Tracer

    init(tracer: Tracer) {
        self.tracer = tracer
    }

    private var currentSpanImpl: shared.Span? = nil

    var currentSpan: shared.Span {
        currentSpanImpl!
    }

    private func createSpan(
        name: String?,
        isRoot: Bool,
        links: Set<AnyHashable>
    ) -> shared.Span {
        let spanBuilder = tracer.spanBuilder(spanName: name ?? "")

        if isRoot {
            spanBuilder.setNoParent()
            spanBuilder.setActive(true)
        }
        for link in links {
            spanBuilder.addLink(spanContext: (link as! OpenTelemetryApi.Span).context)
        }

        let span = spanBuilder.startSpan()
        OpenTelemetry.instance.contextProvider.setActiveSpan(span)

        return SpanImpl(span: span)
    }

    func startSpan(name: String?, isRoot: Bool, links: Set<AnyHashable>) -> shared.Span {
        let span = createSpan(name: name, isRoot: isRoot, links: links)
        return span
    }

    func withSpan(name: String?, isRoot: Bool, links: Set<AnyHashable>, block: TraceBlock) -> Any? {
        let span = createSpan(name: name, isRoot: isRoot, links: links)
        let spanImpl = (span as! SpanImpl).span
        defer {
            spanImpl.end()
        }
        return try? block.process(span: span)
    }

    func withSpan(span: shared.Span, name: String, block: TraceBlock) -> Any? {
        withSpan(traceId: span.getSpanContext().getTraceId(), spanId: span.getSpanContext().getSpanId(), name: name, block: block)
    }

    func withSpan(traceId: String, spanId: String, name: String, block: TraceBlock) -> Any? {
        let currentSpan = SpanContext.createFromRemoteParent(
            traceId: TraceId(fromHexString: traceId),
            spanId: SpanId(fromHexString: spanId),
            traceFlags: TraceFlags().settingIsSampled(true),
            traceState: TraceState.init()
        )

        let spanBuilder = tracer.spanBuilder(spanName: name)
        spanBuilder.setParent(currentSpan)
        
        let span = spanBuilder.startSpan()

        let spanImpl = SpanImpl(span: span)
        defer {
            span.end()
        }
        return try? block.process(span: spanImpl)
    }
}
