//
//  SpanContextImpl.swift
//  Promethist
//
//  Created by Vojtěch Vošmík on 02.04.2024.
//  Copyright © 2024 orgName. All rights reserved.
//

@_implementationOnly import shared
@_implementationOnly import OpenTelemetryApi

class SpanContextImpl: shared.SpanContext {
    
    let spanContext: OpenTelemetryApi.SpanContext

    init(spanContext: OpenTelemetryApi.SpanContext) {
        self.spanContext = spanContext
    }

    func getSpanId() -> String {
        spanContext.spanId.hexString
    }

    func getTraceId() -> String {
        spanContext.traceId.hexString
    }
}
