//
//  SplashView.swift
//  Promethist
//
//  Created by Vojtěch Vošmík on 23.12.2024.
//  Copyright © 2024 orgName. All rights reserved.
//

import SwiftUI
import Lottie

struct SplashView: View {

    @ObservedObject var appRouterModel: AppRouterModel

    var body: some View {
        GeometryReader { geometry in
            ZStack(alignment: .bottom) {
                background
                logo
            }
            .frame(width: geometry.size.width)
            .ignoresSafeArea(.container, edges: [.top, .bottom])
            .overlay {
                if let progress = appRouterModel.avatarAssetsDownloadProgress {
                    progressIndication(progress: progress)
                } else {
                    activityIndication
                }
            }
        }
    }

    @ViewBuilder private var logo: some View {
        VStack(alignment: .center) {
            Spacer()

            Image(resource: \.splashLogo)
                .resizable()
                .scaledToFit()
                .frame(width: splashContentWidth)
                .padding(.bottom, .medium)

            Image(resource: \.splashTitle)
                .resizable()
                .scaledToFit()
                .frame(width: splashContentWidth)
                .padding(.bottom, .xxxlarge)

            Spacer()
        }
        .padding(.top , topSplashOffset)
    }

    @ViewBuilder private var activityIndication: some View {
        VStack {
            Spacer()
            Spacer()

            LottieView(animation: .filepath(AppTheme.loadingAnimation.path))
                .looping()
                .frame(height: 100)

            Spacer()
        }
    }

    @ViewBuilder private func progressIndication(progress: Float) -> some View {
        VStack {
            Spacer()
            Spacer()
            Spacer()

            ProgressIndicatorView(progress: progress)
                .padding(.horizontal, .xxxlarge)
                .frame(maxWidth: 500)

            Spacer()
        }
    }

    @ViewBuilder private var background: some View {
        Rectangle()
            .fill(Color(resource: \.backgroundOverlay))
            .frame(maxWidth: .infinity, maxHeight: .infinity)
    }
}

fileprivate var topSplashOffset: CGFloat = 75
fileprivate var splashContentWidth: CGFloat = 200
