import SwiftUI
import shared

@MainActor
public class ObservableUiStateHandler: ObservableObject, ClientUiStateHandler {

    @Published public var uiState = ClientUiState.companion.Initial

    private let serialQueue = DispatchQueue(label: "updateUiState")

    public func getState() async throws -> ClientUiState {
        uiState
    }

    public func setState(state: ClientUiState) async throws {
        serialQueue.sync {
            self.uiState = state
            self.objectWillChange.send()
        }
    }
}
