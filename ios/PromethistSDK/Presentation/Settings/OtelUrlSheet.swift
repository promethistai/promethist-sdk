//
//  Untitled.swift
//  Promethist
//
//  Created by Vojtěch Vošmík on 27.11.2024.
//  Copyright © 2024 orgName. All rights reserved.
//

import SwiftUI

struct OtelUrlSheet: View {

    @State private var text: String
    let onUrlChange: (String) -> Void
    let onDismiss: () -> Void

    init(text: String, onUrlChange: @escaping (String) -> Void, onDismiss: @escaping () -> Void) {
        self.text = text
        self.onUrlChange = onUrlChange
        self.onDismiss = onDismiss
    }

    var body: some View {
        NavigationView {
            ZStack {
                TextEditor(text: $text)
                    .padding(.mediumLarge)
            }
            .onChange(of: text) { newValue in
                onUrlChange(newValue)
            }
            .navigationBarTitle(Text(resource: \.settings.changeOtelUrl), displayMode: .inline)
            .navigationBarItems(trailing: Button(action: {
                onDismiss()
            }) {
                Text(resource: \.settings.done).bold()
            })
        }
        .interactiveDismissDisabled()
    }
}
