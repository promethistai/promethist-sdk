@_implementationOnly import shared
import Foundation
import SwiftUI

class SettingsViewModel: ViewModel, ObservableObject {
    
    @Published var preset: AppPreset
    @Published var videoMode: VideoMode
    @Published var locale: shared.Locale
    let presets = ClientDefaults.shared.presets
    let videoModes = VideoMode.entries
    let locales = shared.Locale.Companion.shared.supportedLocales
    let duplexModes = DuplexMode.entries

    @Published var interactionMode: InteractionMode
    @Published var appearance: Appearance
    @Published var preventDisplaySleep: Bool
    @Published var showExitConversationAlert: Bool
    @Published var onScreenDebug: Bool
    @Published var streamedVideosPrefetch: Bool
    @Published var initialCommand: String
    @Published var avatarQualityLevel: AvatarQualityLevel
    @Published var debugMode: Bool
    @Published var duplexEnabled: Bool
    @Published var duplexMode: DuplexMode
    @Published var duplexForceOutput: Bool
    @Published var otelUrl: String
    @Published var showAppKeyEntry: Bool

    private let appConfig: AppConfig = Client().appConfig()

    init() {
        self.preset = appConfig.preset
        self.videoMode = appConfig.videoMode
        self.locale = appConfig.locale
        self.interactionMode = appConfig.interactionMode
        self.appearance = appConfig.appearance
        self.preventDisplaySleep = appConfig.preventDisplaySleep
        self.showExitConversationAlert = appConfig.showExitConversationAlert
        self.onScreenDebug = appConfig.onScreenDebug
        self.streamedVideosPrefetch = appConfig.streamedVideosPrefetch
        self.initialCommand = appConfig.initialCommand.isEmpty ? "#intro" : appConfig.initialCommand
        self.avatarQualityLevel = appConfig.avatarQualityLevel
        self.debugMode = appConfig.debugMode
        self.duplexEnabled = appConfig.duplexEnabled
        self.duplexMode = appConfig.duplexMode
        self.duplexForceOutput = appConfig.duplexForceOutput
        self.otelUrl = appConfig.otelUrl
        self.showAppKeyEntry = appConfig.showAppKeyEntry
    }
    
    func onAppear() {}
    
    func changeAppPreset(_ preset: AppPreset) {
        appConfig.preset = preset
        self.locale = appConfig.locale
    }

    func changeVideoMode(_ videoMode: VideoMode) {
        appConfig.videoMode = videoMode
    }

    func changeInteractionMode(_ interactionMode: InteractionMode) {
        appConfig.interactionMode = interactionMode
    }
    
    func changeAppearance(_ appearance: Appearance) {
        appConfig.appearance = appearance
    }
    
    func changePreventDisplaySleep(_ preventDisplaySleep: Bool) {
        appConfig.preventDisplaySleep = preventDisplaySleep
    }
    
    func changeShowExitConversationAlert(_ showExitConversationAlert: Bool) {
        appConfig.showExitConversationAlert = showExitConversationAlert
    }
    
    func changeOnScreenDebug(_ onScreenDebug: Bool) {
        appConfig.onScreenDebug = onScreenDebug
    }

    func changeStreamedVideosPrefetch(_ streamedVideosPrefetch: Bool) {
        appConfig.streamedVideosPrefetch = streamedVideosPrefetch
    }

    func changeInitialCommand(_ value: String) {
        appConfig.initialCommand = value
        initialCommand = value
    }

    func changeLocale(_ locale: shared.Locale) {
        appConfig.engineLocale = locale
    }

    func changeAvatarQualityLevel(_ value: AvatarQualityLevel) {
        appConfig.avatarQualityLevel = value
    }

    func changeDebugMode(_ value: Bool) {
        withAnimation {
            debugMode = value
        }
        appConfig.debugMode = value
    }

    func changeDuplexEnabled(_ value: Bool) {
        appConfig.duplexEnabled = value
    }

    func changeDuplexMode(_ duplexMode: DuplexMode) {
        appConfig.duplexMode = duplexMode
    }

    func changeDuplexForceOutput(_ value: Bool) {
        appConfig.duplexForceOutput = value
    }

    func changeOtelUrl(_ value: String) {
        appConfig.otelUrl = value
        otelUrl = value
    }

    func changeShowAppKeyEntry(_ showAppKeyEntry: Bool) {
        appConfig.showAppKeyEntry = showAppKeyEntry
    }
}
