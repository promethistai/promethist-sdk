import SwiftUI
@_implementationOnly import shared

struct SettingsView: View {

    @Environment(\.openURL) var openURL
    @Environment(\.dismiss) var dismiss
    @EnvironmentObject private var appSettingsObserver: AppSettingsObserver

    @StateObject var settingsViewModel = SettingsViewModel()
    @StateObject var profileViewModel = ProfileViewModel()
    @State private var isInitialCommandSheetVisible = false
    @State private var isOtelUrlSheetVisible = false
    @State private var isRestartRequiredAlertVisible = false
    @State private var isDebugModeAlreadyActivatedAlertVisible = false
    @State private var isEditProfileVisible = false
    let onReset: () -> Void
    let onUpdateAvatarQulity: () -> Void
    let onSendTechnicalReport: () -> Void

    var isDebugModeEnabled: Bool {
        settingsViewModel.debugMode
    }

    var body: some View {
        Form {
            profileSection

            Section(String(resource: \.settings.application)) {
                Picker(String(resource: \.settings.presets), selection: $settingsViewModel.preset) {
                    ForEach(settingsViewModel.presets, id: \.id) {
                        Text($0.name)
                            .tag($0)
                    }
                }
                .isHidden(!isDebugModeEnabled)

                Picker(String(resource: \.settings.videoMode), selection: $settingsViewModel.videoMode) {
                    ForEach(settingsViewModel.videoModes, id: \.self) {
                        Text(String($0.localizedName.localized()))
                            .tag($0)
                    }
                }
                .isHidden(!isDebugModeEnabled)

                Picker(String(resource: \.settings.arcQualityLevel), selection: $settingsViewModel.avatarQualityLevel) {
                    ForEach(AvatarQualityLevel.entries, id: \.self) {
                        Text($0.name)
                            .tag($0)
                    }
                }
                .isHidden(!isDebugModeEnabled)

                Picker(String(resource: \.settings.asrLocale), selection: $settingsViewModel.locale) {
                    ForEach(settingsViewModel.locales, id: \.self) {
                        Text($0.toLanguageTag())
                            .tag($0)
                    }
                }

                /*Button(action: {
                    isInitialCommandSheetVisible = true
                }) {
                    Text(resource: \.settings.initialCommand)
                }
                .isHidden(!isDebugModeEnabled)

                Button(action: {
                    Client.shared.appConfig().showIntroVideo = true
                    isRestartRequiredAlertVisible = true
                }) {
                    Text("Reset intro video state")
                }
                .isHidden(!isDebugModeEnabled)

                Button(action: {
                    isOtelUrlSheetVisible = true
                }) {
                    Text(resource: \.settings.changeOtelUrl)
                }
                .isHidden(!isDebugModeEnabled)*/

                Button(action: {
                    dismiss()
                    onSendTechnicalReport()
                }) {
                    Text(resource: \.settings.report)
                }
                .isHidden(!isDebugModeEnabled)

                Button(action: {
                    openPermissionSettings()
                }) {
                    Text(resource: \.settings.openPermissionSettings)
                }

                Toggle(isOn: $settingsViewModel.streamedVideosPrefetch) {
                    Text(resource: \.settings.streamedVideosPrefetch)
                }
                .isHidden(!isDebugModeEnabled)
            }
            .isHidden(!isDebugModeEnabled)

            Section(String(resource: \.settings.userInterface)) {
                Picker(String(resource: \.settings.appearance), selection: $settingsViewModel.appearance) {
                    ForEach(Appearance.entries, id: \.self) {
                        Text($0.name)
                            .tag($0)
                    }
                }

                Picker(String(resource: \.settings.defaultInteractionMode), selection: $settingsViewModel.interactionMode) {
                    ForEach(InteractionMode.entries, id: \.self) {
                        Text($0.name)
                            .tag($0)
                    }
                }
                .isHidden(!isDebugModeEnabled)

                Toggle(isOn: $settingsViewModel.preventDisplaySleep) {
                    Text(resource: \.settings.preventDisplaySleep)
                }

                Toggle(isOn: $settingsViewModel.showExitConversationAlert) {
                    Text(resource: \.settings.showExitConversationAlert)
                }

                Toggle(isOn: $settingsViewModel.onScreenDebug) {
                    Text(resource: \.settings.onScreenDebug)
                }
                .isHidden(!isDebugModeEnabled)

                Toggle(isOn: $settingsViewModel.showAppKeyEntry) {
                    Text(resource: \.settings.showAppKeyEntry)
                }
                .isHidden(!isDebugModeEnabled || !ClientDefaults.shared.requiresAppKeyInput)
            }
            .isHidden(!isDebugModeEnabled)

            Section(String(resource: \.settings.asr)) {
                Toggle(isOn: $settingsViewModel.duplexEnabled) {
                    Text(resource: \.settings.duplexEnabled)
                }

                Picker(String(resource: \.settings.duplexMode), selection: $settingsViewModel.duplexMode) {
                    ForEach(settingsViewModel.duplexModes, id: \.self) {
                        Text(String($0.localizedName.localized()))
                            .tag($0)
                    }
                }
                .isHidden(!settingsViewModel.duplexEnabled)

                Toggle(isOn: $settingsViewModel.duplexForceOutput) {
                    Text(resource: \.settings.duplexForceOutput)
                }
                .isHidden(!settingsViewModel.duplexEnabled)
            }
            .isHidden(!isDebugModeEnabled)

            Section(String(resource: \.settings.about)) {
                HStack {
                    Text(resource: \.settings.version)
                    Spacer()
                    Text("\(Bundle.main.infoDictionary?[kCFBundleVersionKey as String] ?? "")")
                }
                .contentShape(Rectangle())
                .onLongPressGesture {
                    if isDebugModeEnabled {
                        isDebugModeAlreadyActivatedAlertVisible = true
                        Vibration.error.vibrate()
                        return
                    }
                    Vibration.success.vibrate()
                    settingsViewModel.changeDebugMode(true)
                }

                Button(action: {
                    openURL(URL(string: String(resource: \.settings.privacyPolicyUrl))!)
                }) {
                    Text(resource: \.settings.privacyPolicy)
                }

                Button(action: {
                    openURL(URL(string: String(resource: \.settings.dataProtectionUrl))!)
                }) {
                    Text(resource: \.settings.dataProtection)
                }
            }
        }
        .background(Color(resource: \.personaBackground))
        .navigationTitle(String(resource: \.profile.title))
        .navigationBarTitleDisplayMode(.large)
        .lifecycle(settingsViewModel)
        .sheet(isPresented: $isInitialCommandSheetVisible) {
            InitialCommandSheet(
                command: settingsViewModel.initialCommand,
                onCommandChange: {
                    settingsViewModel.changeInitialCommand($0)
                },
                onDismiss: {
                    isInitialCommandSheetVisible = false
                }
            )
        }
        .sheet(isPresented: $isOtelUrlSheetVisible) {
            OtelUrlSheet(
                text: settingsViewModel.otelUrl,
                onUrlChange: {
                    settingsViewModel.changeOtelUrl($0)
                },
                onDismiss: {
                    isOtelUrlSheetVisible = false
                    isRestartRequiredAlertVisible = true
                }
            )
        }
        .alert(String(resource: \.settings.restartRequired), isPresented: $isRestartRequiredAlertVisible) {
            Button(String(resource: \.settings.restart), role: .cancel) {
                exit(0)
            }
        }
        .alert(String(resource: \.settings.debugAlreadyActivated), isPresented: $isDebugModeAlreadyActivatedAlertVisible) {
            Button("Ok", role: .cancel) {
                isDebugModeAlreadyActivatedAlertVisible = false
            }
        }
        .onChange(of: settingsViewModel.preset) { newValue in
            settingsViewModel.changeAppPreset(newValue)
            onReset()
            dismiss()
        }
        .onChange(of: settingsViewModel.interactionMode) { newValue in
            settingsViewModel.changeInteractionMode(newValue)
        }
        .onChange(of: settingsViewModel.appearance) { newValue in
            settingsViewModel.changeAppearance(newValue)
            appSettingsObserver.update()
        }
        .onChange(of: settingsViewModel.preventDisplaySleep) { newValue in
            settingsViewModel.changePreventDisplaySleep(newValue)
        }
        .onChange(of: settingsViewModel.showExitConversationAlert) { newValue in
            settingsViewModel.changeShowExitConversationAlert(newValue)
        }
        .onChange(of: settingsViewModel.onScreenDebug) { newValue in
            settingsViewModel.changeOnScreenDebug(newValue)
            appSettingsObserver.update()
        }
        .onChange(of: settingsViewModel.videoMode) { newValue in
            settingsViewModel.changeVideoMode(newValue)
            isRestartRequiredAlertVisible = true
        }
        .onChange(of: settingsViewModel.streamedVideosPrefetch) { newValue in
            settingsViewModel.changeStreamedVideosPrefetch(newValue)
        }
        .onChange(of: settingsViewModel.locale) { newValue in
            settingsViewModel.changeLocale(newValue)
            isRestartRequiredAlertVisible = true
        }
        .onChange(of: settingsViewModel.avatarQualityLevel) { newValue in
            settingsViewModel.changeAvatarQualityLevel(newValue)
            onUpdateAvatarQulity()
            dismiss()
        }
        .onChange(of: settingsViewModel.duplexEnabled) { newValue in
            settingsViewModel.changeDuplexEnabled(newValue)
            isRestartRequiredAlertVisible = true
        }
        .onChange(of: settingsViewModel.duplexMode) { newValue in
            settingsViewModel.changeDuplexMode(newValue)
        }
        .onChange(of: settingsViewModel.duplexForceOutput) { newValue in
            settingsViewModel.changeDuplexForceOutput(newValue)
        }
        .onChange(of: settingsViewModel.showAppKeyEntry) { newValue in
            settingsViewModel.changeShowAppKeyEntry(newValue)
            isRestartRequiredAlertVisible = true
        }
        .responsiveSheet(isPresented: $isEditProfileVisible) {
            UserProfileModalView(
                item: UserProfileItem(content: UserProfileData(showGenderSelection: true)),
                showTitle: false,
                onSubmit: {
                    // TODO: Send user input
                    isEditProfileVisible = false
                }
            )
            .padding(.horizontal, .mediumLarge)
            .padding(.bottom, .medium)
        }
        .onAppear {
            let authClient = Client.shared.authClient()
            if !authClient.isLoggedIn() && ClientDefaults.shared.authPolicy == .required {
                Task {
                    profileViewModel.logout()
                    dismiss()
                    appEvents.send(.authSessionUpdated)
                }
            }
        }
        .withStatusBarStyle(.darkContent)
    }

    @ViewBuilder private var profileSection: some View {
        Section {
            PromethistProfileView(viewModel: profileViewModel)

            if profileViewModel.isLoginOptionAvailable {
                Button(action: {
                    dismiss()
                    appEvents.send(.openAuthWebView)
                }) {
                    Text(resource: \.profile.signIn)
                }
            } else if profileViewModel.isLoggedIn {
                Button(action: {
                    isEditProfileVisible = true
                }) {
                    Text(resource: \.profile.edit)
                }

                Button(action: {
                    profileViewModel.logout()
                    dismiss()
                    appEvents.send(.authSessionUpdated)
                }) {
                    Text(resource: \.profile.signOut)
                }
            }
        }
    }
}
