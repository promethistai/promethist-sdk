//
//  InitialCommandSheet.swift
//  PromethistSDK
//
//  Created by Vojtěch Vošmík on 15.08.2024.
//  Copyright © 2024 orgName. All rights reserved.
//

import SwiftUI

struct InitialCommandSheet: View {

    @State private var text: String
    let onCommandChange: (String) -> Void
    let onDismiss: () -> Void

    init(command: String, onCommandChange: @escaping (String) -> Void, onDismiss: @escaping () -> Void) {
        self.text = command
        self.onCommandChange = onCommandChange
        self.onDismiss = onDismiss
    }

    var body: some View {
        NavigationView {
            ZStack {
                TextEditor(text: $text)
                    .padding(.mediumLarge)
            }
            .onChange(of: text) { newValue in
                onCommandChange(newValue)
            }
            .navigationBarTitle(Text(resource: \.settings.initialCommand), displayMode: .inline)
            .navigationBarItems(trailing: Button(action: {
                onDismiss()
            }) {
                Text(resource: \.settings.done).bold()
            })
        }
    }
}
