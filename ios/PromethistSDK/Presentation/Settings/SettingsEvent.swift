var settingsEvents = EventPublisher<SettingsEvent>()

enum SettingsEvent {
    case apperanceUpdated
}
