@_implementationOnly import shared

extension AppTheme {

    private static var currentTarget: ClientTargetPreset {
        ClientTarget.shared.currentTarget
    }

    // MARK: - Styles

    static var secondaryButtonStyle: PromethistPrimaryButton.Style {
        switch currentTarget {
        case .tmobilev3: .secondaryAlt
        default: .secondary
        }
    }

    static var primaryButtonStyle: PromethistPrimaryButton.Style {
        switch currentTarget {
        case .tmobilev3: .primary
        default: .primary
        }
    }

    // MARK: - Animations

    static var loadingAnimation: LottieAsset {
        switch currentTarget {
        case .tmobilev3: .loadingAlt
        default: .loading
        }
    }
}
