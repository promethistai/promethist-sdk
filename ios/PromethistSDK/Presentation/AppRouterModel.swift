import SwiftUI
import Combine
import shared

enum StartPoint {
    case appKeyEntry
    case main
    case splash
}

public final class AppRouterModel: ViewModel, ObservableObject {
    
    public static var isAppLinkParsed: Bool = false

    @Published private(set) var startPoint: StartPoint = .splash
    @Published public var isShowingSplash: Bool = true
    @Published public var isAuthSheetPresented: Bool = false
    @Published public var isHomeScreenPresented: Bool = false
    @Published public var authSheetAction = AuthSheetAction.nothing
    @Published public var isIntroPresented: Bool = false
    @Published public var isPermissionsAlertPresented: Bool = false
    @Published public var avatarAssetsDownloadProgress: Float? = nil
    @Published public var isDevSheetPresented: Bool = false

    private var cancellables = Set<AnyCancellable>()
    private let clientViewModel: ClientModelImpl
    private let appSettings: AppConfig
    private let authClient = Client.shared.authClient()
    private let permissionsManager = PermissionsManager.shared
    private var latestAssetsVersion: Int = 0

    public init(clientViewModel: ClientModelImpl) {
        self.clientViewModel = clientViewModel

        Telemetry.shared.telemetryController = TelemetryControllerImpl()
        Telemetry.shared.register(
            name: "\(TelemetryConfig.serviceName):\((Bundle.main.infoDictionary?[kCFBundleNameKey as String] as? String)!)",
            endpoint: TelemetryConfig.endpoint
        )

        let appSettings = Client().appConfig()
        self.appSettings = appSettings

        subscribeAppEvents()
    }
    
    func onAppear() {
        #if targetEnvironment(simulator)
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) { [weak self] in
            self?.onSceneLoaded()
        }
        #endif

        Task {
            let permissionsState = await permissionsManager.getAudioPermissionsState()

            switch permissionsState {
            case .allowed:
                determineStartPoint()
            case .denied:
                isPermissionsAlertPresented = true
            }
        }
    }

    private func subscribeAppEvents() {
        let subscription = appEvents.sink { [weak self] event in
            guard let self = self else { return }
            switch event {
            case .authSessionUpdated:
                self.onAuthSessionUpdated()
            case .openAuthWebView:
                self.clientViewModel.handleEvent(event: ClientEventPause())
                self.forceOpenAuthWebView()
            case .openAppKeyEntry:
                self.forceOpenAppKeyEntry()
            case .sceneLoaded:
                self.onSceneLoaded()
            case .avatarDownloadState(let state):
                self.onAvatarDownloadState(state)
            default: break
            }
        }
        subscription.store(in: &cancellables)
    }

    func onSceneLoaded() {
        // Hide splash screen
        withAnimation {
            self.isShowingSplash = false
        }

        // Start ClientViewModel & show auth if required
        if self.isAuthRequired && !self.authClient.isLoggedIn() {
            self.isAuthSheetPresented = true
        } else if !appSettings.showAppKeyEntry {
            if appSettings.showIntroVideo {
                isIntroPresented = true
            } else {
                onStart()
            }
        }
    }

    func onStart() {
        self.clientViewModel.start()
        if clientViewModel.config.isHomeScreenEnabled {
            isHomeScreenPresented = true
        }
    }

    func onShake() {
        isDevSheetPresented = true
    }

    private func determineStartPoint() {
        if appSettings.showAppKeyEntry || AppRouterModel.isAppLinkParsed {
            updateStartPoint(.appKeyEntry)
            return
        }

        updateStartPoint(.main)
    }

    func updateStartPoint(_ startPoint: StartPoint) {
        withAnimation {
            self.startPoint = startPoint
        }
    }

    func forceOpenAppKeyEntry() {
        updateStartPoint(.appKeyEntry)
    }

    func onAvatarDownloadState(_ state: AvatarDownloadState) {
        switch state {
        case .started:
            withAnimation { self.isShowingSplash = true }
        case .finished:
            withAnimation { self.isShowingSplash = false }
            self.avatarAssetsDownloadProgress = nil
        case .inProgress(let progress):
            withAnimation { self.avatarAssetsDownloadProgress = progress }
        }
    }
}

// MARK: - Home screen

extension AppRouterModel {

    func showHomeScreen() {
        withAnimation { isHomeScreenPresented = true }
    }

    func hideHomeScreen() {
        withAnimation { isHomeScreenPresented = false }
    }
}

// MARK: - Auth

extension AppRouterModel {

    var isAuthRequired: Bool {
        ClientDefaults.shared.authPolicy == .required
    }

    var shouldLogoutOnAuthSheetDismiss: Bool {
        ClientDefaults.shared.authPolicy == .optional
            && !authClient.isLoggedIn()
    }

    func finishLogin(isSuccess: Bool) {
        Task {
            if isSuccess {
                isAuthSheetPresented = false
                if appSettings.showIntroVideo {
                    isIntroPresented = true
                } else {
                    onStart()
                }
                let _ = try? await authClient.getUserInfo()
            } else {
                // TODO: Show error message
                isAuthSheetPresented = false
                try? await authClient.logout()
                try? await Task.sleep(nanoseconds: 1_000_000_000)
                isAuthSheetPresented = true
            }
        }
    }

    func onAuthSessionUpdated() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [weak self] in
            guard let self = self else { return }
            if self.isAuthRequired && !self.authClient.isLoggedIn() {
                self.isAuthSheetPresented = true
            } else {
                onStart()
            }
        }
    }

    func onAuthSheetDismiss() {
        if authSheetAction == .nothing, shouldLogoutOnAuthSheetDismiss {
            Task {
                await logout()
                onStart()
            }
        }
    }

    func logout() async {
        try? await authClient.logout()
    }

    func forceOpenAuthWebView() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [weak self] in
            guard let self = self else { return }
            self.isAuthSheetPresented = true
        }
    }
}

// MARK: - Intro

extension AppRouterModel {

    func onIntroFinished() {
        withAnimation { isIntroPresented = false }
        onStart()
        appSettings.showIntroVideo = false
    }
}

fileprivate var splashDuration: DispatchTime { return .now() + 1.2 }
