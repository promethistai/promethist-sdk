public var appEvents = EventPublisher<AppEvent>()

public enum AppEvent {
    case assetsUpdateAvailable
    case appKeyReceived(String)
    case appLinkReceived(engineDomain: String, engineSetupName: String)
    case micPowerUpdate(Float)
    case authSessionUpdated
    case openAuthWebView
    case openAppKeyEntry
    case sceneLoaded
    case avatarDownloadState(state: AvatarDownloadState)
}
