//
//  ImagePicker.swift
//  Promethist
//
//  Created by Vojtěch Vošmík on 06.03.2024.
//  Copyright © 2024 orgName. All rights reserved.
//

import SwiftUI
import UIKit

struct ImagePicker: UIViewControllerRepresentable {

    var sourceType: UIImagePickerController.SourceType = .photoLibrary
    var onPickedImage: (Data?) -> Void

    @Environment(\.presentationMode) private var presentationMode

    func makeCoordinator() -> Coordinator {
        return Coordinator(parent: self, maxSize: maxSize)
    }

    func makeUIViewController(context: UIViewControllerRepresentableContext<ImagePicker>) -> UIImagePickerController {
        let imagePicker = UIImagePickerController()
        imagePicker.allowsEditing = false
        imagePicker.sourceType = sourceType
        imagePicker.cameraDevice = .front
        imagePicker.delegate = context.coordinator
        return imagePicker
    }

    func updateUIViewController(_ uiViewController: UIImagePickerController, context: UIViewControllerRepresentableContext<ImagePicker>) {}

    final class Coordinator: NSObject, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

        var parent: ImagePicker
        var maxSize: CGSize

        init(parent: ImagePicker, maxSize: CGSize) {
            self.parent = parent
            self.maxSize = maxSize
        }

        func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
            if let image = info[.originalImage] as? UIImage {
                parent.onPickedImage(image.photoData(maxSize: maxSize))
            }

            parent.presentationMode.wrappedValue.dismiss()
        }

        func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
            parent.onPickedImage(nil)
            parent.presentationMode.wrappedValue.dismiss()
        }
    }
}

extension UIImage {

    func photoData(maxSize: CGSize = maxSize) -> Data? {
        var resized = self
        if resized.size.width > maxSize.width || resized.size.height > maxSize.height {
            if let image = resize(to: maxSize) {
                resized = image
            } else {
                return nil
            }
        }

        return resized.jpegData(compressionQuality: quality)
    }

    func resize(to targetSize: CGSize) -> UIImage? {
        let widthRatio  = targetSize.width  / size.width
        let heightRatio = targetSize.height / size.height

        var newSize: CGSize
        if widthRatio > heightRatio {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio, height: size.height * widthRatio)
        }

        let rect = CGRect(origin: .zero, size: newSize)

        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return newImage
    }
}

private let maxSize = CGSize(width: 500, height: 500)
private let quality = 0.75
