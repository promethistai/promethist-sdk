//
//  PersonaView+Computed.swift
//  Promethist
//
//  Created by Vojtěch Vošmík on 02.12.2024.
//  Copyright © 2024 orgName. All rights reserved.
//

@_implementationOnly import shared

extension PersonaView {

    var showMessages: Bool {
        // TODO: Rework
        //!clientModel.persona.isVideoEnabled && !clientModel.persona.showModules
        false
    }

    var showTextInput: Bool {
        // TODO: Rework
        //showMessages || (appConfig.homeTextInput && clientModel.persona.showModules)
        false
    }

    var showDebugInfo: Bool {
        appConfig.onScreenDebug
    }

    var modulesBottomOffset: CGFloat {
        appConfig.homeTextInput ? (bottomOffset + 78) : bottomOffset
    }

    var showHelpButton: Bool {
        // TODO: Rework
        /*clientModel.persona.isVideoEnabled && clientModel.persona.currentModal == nil
            && ClientDefaults.shared.platformVersion != .v3*/
        false
    }

    var transcriptBottomOffset: CGFloat {
        bottomOffset - bottomNavOffset
    }

    var bottomNavOffset: CGFloat {
        // TODO: Rework
        /*switch clientModel.persona.bottomNavOffset {
        case .small: 40
        case .medium: 74
        case .large: 100
        default: 0
        }*/
        return 0
    }

    var hasBottomNavOffset: Bool {
        // TODO: Rework
        //clientModel.persona.bottomNavOffset != .none
        false
    }

    func updateMicDeniedState() {
        // TODO: Rework
        /*guard ClientTarget.shared.currentTarget == .dfha else { return }
        Task {
            let isMicDenied = await SFSpeechRecognizerController.isMicDenied()
            viewModel.handleEvent(event: ClientEvent.SetMicDeniedState(state: isMicDenied))
        }*/
    }
}

fileprivate let bottomOffset: CGFloat = 80
