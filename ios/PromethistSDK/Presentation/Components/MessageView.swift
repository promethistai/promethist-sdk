import SwiftUI
@_implementationOnly import shared
@_implementationOnly import Kingfisher

struct MessageView: View {
    
    let message: Message
    
    var textColor: SwiftUI.Color {
        message.isUser ? .white : .primary
    }
        
    var body: some View {
        HStack(alignment: .top) {
            if message.isUser {
                Spacer()
            }
            
            if let personaId = message.personaId, !message.isUser {
                Image(uiImage: PersonaResourcesManager().getPortrait(personaId: PersonaId.Companion().fromAvatarId(name: personaId), size: .m).toUIImage()!)
                    .resizable()
                    .scaledToFill()
                    .frame(width: avatarSize, height: avatarSize)
                    .background(Color.white.opacity(0.2))
                    .cornerRadius(avatarSize / 2)
            }
            
            messageBubble
            
            if !message.isUser {
                Spacer()
            }
        }
        .padding(.leading, message.isUser ? 76 : 0)
    }
    
    @ViewBuilder private var messageBubble: some View {
        VStack(alignment: .leading, spacing: .xsmall) {
            /*if let name = message.name {
                Text("\(name):")
                    .bold()
                    .font(AppTheme.Fonts.heading3)
                    .foregroundColor(textColor)
            }*/

            Text(message.createdFormatted)
                .promethistFont(size: 12)
                .foregroundColor(textColor.opacity(0.7))

            Text(message.text.replacingOccurrences(of: "\n", with: ""))
                .promethistFont(size: 18)
                .foregroundColor(textColor)

            if let image = message.image {
                KFImage(URL(string: image))
                    .resizable()
                    .aspectRatio(contentMode: .fit)
            }
        }
        .padding(.medium)
        .background {
            if message.isUser {
                Color(resource: \.accent)
            } else {
                Color.white.opacity(0.7)
            }
        }
        .cornerRadius(
            radius: 26,
            corners: message.isUser ? [.topLeft, .topRight, .bottomLeft] : [.topLeft, .topRight, .bottomRight]
        )
    }
}

fileprivate let avatarSize: CGFloat = 60
