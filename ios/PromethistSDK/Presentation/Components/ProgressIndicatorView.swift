//
//  ProgressIndicatorView.swift
//  Promethist
//
//  Created by Vojtěch Vošmík on 30.12.2024.
//  Copyright © 2024 orgName. All rights reserved.
//

import SwiftUI

public struct ProgressIndicatorView: View {

    let progress: Float
    let tint: Color

    public init(
        progress: Float,
        tint: Color = .white
    ) {
        self.progress = progress
        self.tint = tint
    }

    public var body: some View {
        GeometryReader { geometry in
            ZStack(alignment: .leading) {
                RoundedRectangle(cornerRadius: height)
                    .stroke(tint, lineWidth: 2)

                RoundedRectangle(cornerRadius: height)
                    .fill(tint)
                    .frame(width: CGFloat(progress) * geometry.size.width, height: height)
                    .padding(3)
            }

        }
        .frame(height: height)
    }
}

private let height: CGFloat = 10
