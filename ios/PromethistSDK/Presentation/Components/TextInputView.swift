import SwiftUI
@_implementationOnly import shared

struct TextInputView: View {

    @EnvironmentObject private var textInputFocusObserver: TextInputFocusObserver

    @State var text: String = ""
    @FocusState private var isTextFieldFocused: Bool

    let isTypingDisabled: Bool
    let isSendDisabled: Bool
    let onSend: (String) -> Void

    var body: some View {
        HStack {
            TextField(String(resource: \.persona.typeHere), text: $text)
                    .focused($isTextFieldFocused)
                    .padding(10)
                    .font(AppTheme.Fonts.regular)
                    .background(.regularMaterial, in: RoundedRectangle(cornerRadius: 10))
                    .onSubmit(sendText)

            Button(action: sendText) {
                Image(resource: \.icArrowRight)
                    .renderingMode(.template)
                    .resizable()
                    .scaledToFit()
                    .scaleEffect(0.5)
                    .frame(width: 40, height: 40)
                    .foregroundColor(Color.white)
                    .background(Color.accentColor)
                    .opacity(isSendDisabled ? 0.5 : 1.0)
                    .clipShape(Circle())
            }
            .disabled(isSendDisabled)
        }
        .disabled(isTypingDisabled)
        .frame(minHeight: .zero)
        .onChange(of: isTextFieldFocused) { isFocused in
            textInputFocusObserver.isFocused = isFocused
        }
    }

    private func sendText() {
        guard !isSendDisabled && !text.isEmpty else { return }
        let _text = text
        text = ""
        onSend(_text)
    }
}
