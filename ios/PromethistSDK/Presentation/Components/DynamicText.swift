import SwiftUI

// TODO: Implemented from old iOS project - some refactor needed
struct DynamicText: View {

    private static let deviceType = UIDevice.current.userInterfaceIdiom
    
    let text: String
    let weight: Font.Weight

    init(_ text: String, weight: Font.Weight = .regular) {
        self.text = text
        self.weight = weight
    }

    var body: some View {
        let words = text.components(separatedBy: .whitespacesAndNewlines)
        
        return Text(text)
            .font(AppTheme.PrimaryFont.bold(size: (words.count < 5 ? 30 : words.count < 15 ? 25 : words.count < 35 ? 20 : 18) * (Self.deviceType != .phone ? 1.5 : 1)))
            .fixedSize(horizontal: false, vertical: true)
    }
}
