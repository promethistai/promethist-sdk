import SwiftUI
@_implementationOnly import shared

struct PersonaView: View {
    
    @Environment(\.safeAreaInsets) private var safeAreaInsets

    var viewModel: ClientModelImpl
    var appConfig: AppConfig
    var onHomeClick: () -> Void
    @StateObject var uiStateHandler: ObservableUiStateHandler
    @StateObject private var textInputFocusObserver = TextInputFocusObserver()

    @State private var isHomeDisclaimerPresented: Bool = false
    @State private var isInfoSheetPresented: Bool = false
    @State private var isPersonaSelectionPresented: Bool = false
    @State private var setPersonaItem: SetPersonaItem? = nil
    @State private var interactionMode: PromethistInteractionMode = .voice

    private var isHomeScreenEnabled: Bool {
        viewModel.config.isHomeScreenEnabled
    }

    private var isChatMode: Bool {
        interactionMode == .chat
    }

    var body: some View {
        PersonaViewLayout(
            viewModel: viewModel,
            appConfig: appConfig,
            uiStateHandler: uiStateHandler,
            applyBlur: isChatMode
        ) {
            ZStack {
                // MARK: - Main content
                switch interactionMode {
                case .voice: voiceMode
                case .chat: chatMode
                }
            }
            .animation(interactionAnimation, value: interactionMode)
            .frame(maxWidth: .infinity, maxHeight: .infinity)
            .overlay(alignment: .top) {
                // MARK: - Top controls
                ZStack(alignment: .top) {
                    PromethistTopFadeView(isActive: isChatMode)

                    PromethistToolbarView(
                        interactionMode: $interactionMode,
                        onHomeClick: {
                            if interactionMode == .chat {
                                interactionMode = .voice
                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                                    viewModel.handleEvent(event: ClientEventPause())
                                    textInputFocusObserver.isFocused = false
                                    isHomeDisclaimerPresented = true
                                }
                                return
                            }
                            viewModel.handleEvent(event: ClientEventPause())
                            textInputFocusObserver.isFocused = false
                            isHomeDisclaimerPresented = true
                        },
                        onProfileClick: {
                            viewModel.handleEvent(event: ClientEventPause())
                        },
                        onReset: {
                            viewModel.handleEvent(event: ClientEventReset(sendAsInput: true))
                        },
                        onUpdateAvatarQulity: {
                            viewModel.handleEvent(event: ClientEventSyncSettings())
                        },
                        onSendTechnicalReport: {
                            viewModel.avatarPlayer.requestTechnicalReport()
                        }
                    )
                    .padding(.horizontal, .mediumLarge)
                    .padding(.vertical, .medium)
                }
            }
            .onChange(of: interactionMode) { value in
                switch value {
                case .chat:
                    viewModel.handleEvent(event: ClientEventPause())
                case .voice:
                    isHomeDisclaimerPresented = false
                    viewModel.handleEvent(event: ClientEventResume())
                }
            }
        }
        .withStatusBarStyle(.lightContent)
    }

    @ViewBuilder private var voiceMode: some View {
        ZStack {
            // MARK: - Transcript area
            PromethistTranscriptView(
                response: uiStateHandler.uiState.visualItem?.text
                //isResponseVisible: uiStateHandler.uiState.isVisualItemVisible TODO: Rework
            )

            // MARK: - Bottom controls
            VStack {
                Spacer()
                PromethistPersonaControlView(
                    audioState: uiStateHandler.uiState.audioIndicatorState,
                    isHandActive: uiStateHandler.uiState.isBargeInActive,
                    isHandEnabled: !uiStateHandler.uiState.isListening,
                    isPaused: uiStateHandler.uiState.isPaused,
                    onPauseClick: {
                        guard uiStateHandler.uiState.modal == nil else { return }
                        Vibration.selection.vibrate()
                        if uiStateHandler.uiState.isPaused {
                            viewModel.handleEvent(event: ClientEventResume())
                        } else {
                            viewModel.handleEvent(event: ClientEventPause())
                        }
                    },
                    onHandClick: {
                        Vibration.selection.vibrate()
                        viewModel.handleEvent(event: ClientEventBargeIn())
                    }
                )
            }
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .center)
        .overlay(alignment: .bottom) {
            // MARK: - MultiModality
            PromethistMultiModalView(
                modalItem: uiStateHandler.uiState.modal,
                isHomeDisclaimerVisible: isHomeDisclaimerPresented,
                isTypingDisabled: false, //!clientModel.persona.isTypingEnabled, TODO: Rework
                isSendDisabled: false, //!clientModel.persona.isSendEnabled, TODO: Rework
                showTextInput: showTextInput,
                isBottomOffsetEnabled: hasBottomNavOffset,
                onPersonaSelected: { personaItem, personaType in
                    viewModel.handleEvent(event: ClientEventSetPersona(
                        ref: personaItem.id,
                        avatarRef: personaItem.getAvatar(),
                        sendAsInput: true
                    ))
                },
                onUserInput: {
                    viewModel.handleEvent(event: ClientEventUserInput(input: $0))
                },
                onBackToMenuClick: {
                    textInputFocusObserver.isFocused = false
                    viewModel.handleEvent(event: ClientEventReset(sendAsInput: !isHomeScreenEnabled))
                    onHomeClick()
                },
                onHomeDisclaimerAction: {
                    isHomeDisclaimerPresented = false
                    if $0 {
                        //appConfig.showExitConversationAlert = !dontShowAgain
                        viewModel.handleEvent(event: ClientEventReset(sendAsInput: !isHomeScreenEnabled))
                        onHomeClick()
                    } else {
                        //appConfig.showExitConversationAlert = !dontShowAgain
                        viewModel.handleEvent(event: ClientEventResume())
                    }
                }
            )
            .environmentObject(textInputFocusObserver)
            .padding(.top, 45 + AppTheme.Spacing.medium.rawValue)
        }
        .padding(.horizontal, .mediumLarge)
        .padding(.vertical, .medium)
    }

    @ViewBuilder private var chatMode: some View {
        ZStack {
            TranscriptionRoot(messages: uiStateHandler.uiState.messages)
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .center)
        .environmentObject(textInputFocusObserver)
    }
}

private let topOffset: CGFloat = 60
private let bottomOffset: CGFloat = 80
private let interactionAnimation: Animation = .spring(duration: 0.35, bounce: 0.38)
