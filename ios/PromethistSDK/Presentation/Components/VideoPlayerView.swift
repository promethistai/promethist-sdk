import SwiftUI
import AVKit
import AVFoundation

struct VideoPlayerView: UIViewRepresentable {

    let videoPlayer: AVQueuePlayer

    func updateUIView(_ uiView: VideoPlayerUIView, context: UIViewRepresentableContext<VideoPlayerView>) {}

    func makeUIView(context: Context) -> VideoPlayerUIView {
        VideoPlayerUIView(frame: .zero, player: videoPlayer)
    }
}

class VideoPlayerUIView: UIView {

    private var playerLayer = AVPlayerLayer()
    private var player: AVQueuePlayer!
    private var token: NSKeyValueObservation?

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    init(frame: CGRect, player: AVQueuePlayer) {
        super.init(frame: frame)
        
        NotificationCenter.default.addObserver(self, selector: #selector(didEnterBackground), name: UIApplication.didEnterBackgroundNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(willEnterForeground), name: UIApplication.willEnterForegroundNotification, object: nil)
        
        playerLayer.player = player
        playerLayer.videoGravity = .resizeAspectFill // Ensures center crop
        layer.addSublayer(playerLayer)
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        guard let videoSize = playerLayer.player?.currentItem?.asset.tracks(withMediaType: .video).first?.naturalSize else {
            playerLayer.frame = bounds
            return
        }

        let videoAspect = videoSize.height / videoSize.width
        let viewAspect = bounds.height / bounds.width

        if videoAspect > viewAspect {
            // Video is taller than the view, so crop from the top
            let newHeight = bounds.width * videoAspect
            playerLayer.frame = CGRect(
                x: 0,
                y: bounds.height - newHeight, // Align to bottom
                width: bounds.width,
                height: newHeight
            )
        } else {
            // Default behavior
            playerLayer.frame = bounds
        }
    }

    @objc func didEnterBackground() {
        // TODO
    }

    @objc func willEnterForeground() {
        // TODO
    }
}
