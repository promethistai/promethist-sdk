import SwiftUI
@_implementationOnly import shared

struct DisclaimerView: View {
    
    let text: KeyPath<Strings, shared.ResourceStringDesc>
    var showCheckBox: Bool = false
    let onResume: (Bool) -> Void
    let onHome: (Bool) -> Void
    
    @State private var checked = false
    
    var body: some View {
        ZStack(alignment: .center) {
            Color.black.opacity(0.45).edgesIgnoringSafeArea(.all)
            
            VStack(alignment: .center, spacing: 22) {
                Text(resource: text)
                    .font(AppTheme.Fonts.regular)
                
                HStack(alignment: .center, spacing: .small) {
                    IconButton(resource: \.persona.backToMenu, color: Color.white, background: Color(resource: \.backToMenuButtons)) {
                        onHome(checked)
                    }
                    
                    IconButton(resource: \.persona.resume, color: Color.white, background: Color(resource: \.backToMenuButtons)) {
                        onResume(checked)
                    }
                }
                
                HStack(alignment: .center, spacing: 16) {
                    CheckBoxView(checked: checked)
                    
                    Text(resource: \.persona.dontShowThisMessage)
                }
                .onTapGesture {
                    checked.toggle()
                }
            }
            .padding(24)
            .background(.regularMaterial)
            .cornerRadius(radius: 20)
            .padding(28)
        }
        .accentColor(Color(resource: \.accent))
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .background(ClearBackgroundView())
    }
}
