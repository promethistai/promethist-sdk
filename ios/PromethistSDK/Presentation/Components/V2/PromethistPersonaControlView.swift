import SwiftUI
@_implementationOnly import shared

struct PromethistPersonaControlView: View {

    let audioState: AudioIndicatorState
    let isHandActive: Bool
    let isHandEnabled: Bool
    let isPaused: Bool
    let onPauseClick: () -> Void
    let onHandClick: () -> Void

    var body: some View {
        HStack(alignment: .center, spacing: .medium) {
            PromethistCircleButton(
                icon: isPaused ? \.personaPlay : \.personaPause,
                isActive: isPaused,
                onClick: onPauseClick
            )

            PromethistAudioIndicatorView(
                state: audioState
            )
            .frame(maxWidth: .infinity, alignment: .center)

            PromethistCircleButton(
                icon: \.personaHand,
                isActive: isHandActive,
                isEnabled: isHandEnabled && !isHandActive,
                onClick: onHandClick
            )
        }
    }
}
