//
//  PromethistPrimaryButton.swift
//  Promethist
//
//  Created by Vojtěch Vošmík on 12.01.2025.
//  Copyright © 2025 orgName. All rights reserved.
//

import SwiftUI
@_implementationOnly import shared

struct PromethistPrimaryButton: View {

    enum Style {
        case primary
        case secondary
        case secondaryAlt
        case custom(backgroundColor: SwiftUI.Color, foregroundColor: SwiftUI.Color, outline: Bool)

        var backgroundColor: SwiftUI.Color {
            switch self {
            case .primary: Color(resource: \.accent)
            case .secondary: Color(resource: \.accentSecondary)
            case .secondaryAlt: Color(resource: \.personaSurface)
            case .custom(let backgroundColor, _, _): backgroundColor
            }
        }

        var foregroundColor: SwiftUI.Color {
            switch self {
            case .primary: .white
            case .secondary: Color(resource: \.accent)
            case .secondaryAlt: Color(resource: \.primaryLabel)
            case .custom(_, let foregroundColor, _): foregroundColor
            }
        }

        var isOutlined: Bool {
            switch self {
            case .primary: false
            case .secondary: false
            case .secondaryAlt: true
            case .custom(_, _, let outline): outline
            }
        }
    }

    let text: String
    let style: Style
    let isEnabled: Bool
    let onClick: (() -> Void)?

    internal init(
        text: String,
        style: Style = AppTheme.primaryButtonStyle,
        isEnabled: Bool = true,
        onClick: (() -> Void)? = nil
    ) {
        self.text = text
        self.style = style
        self.isEnabled = isEnabled
        self.onClick = onClick
    }

    var body: some View {
        if let onClick = onClick {
            Button(action: onClick) {
                content
            }
            .disabled(!isEnabled)
        } else {
            content
        }
    }

    @ViewBuilder private var content: some View {
        ZStack(alignment: .center) {
            if style.isOutlined {
                RoundedRectangle(cornerRadius: radius)
                    .stroke(style.foregroundColor.opacity(isEnabled ? 1 : outlinedDisabledOpacity), lineWidth: outlinedLineWidth)
            } else {
                RoundedRectangle(cornerRadius: radius)
                    .fill(style.backgroundColor.opacity(isEnabled ? 1 : fillDisabledOpacity))
            }

            Text(text)
                .foregroundStyle(style.foregroundColor.opacity((isEnabled || !style.isOutlined) ? 1 : outlinedDisabledOpacity))
                .promethistFont(size: 18, weight: .semibold)
                .lineLimit(1)
                .truncationMode(.tail)
                .padding(.horizontal, .medium)
                .padding(.vertical, .mediumSmall)
        }
        .frame(height: height)
        .animation(.easeInOut(duration: 0.3), value: isEnabled)
    }
}

private let height: CGFloat = 59
private let radius: CGFloat = height / 2
private let outlinedLineWidth: CGFloat = 1
private let outlinedDisabledOpacity: CGFloat = 0.5
private let fillDisabledOpacity: CGFloat = 0.3
