import SwiftUI
@_implementationOnly import shared

enum PromethistInteractionMode: String, CaseIterable, Identifiable {
    case voice = "voice"
    case chat = "chat"

    var id: String { return self.rawValue }

    var icon: KeyPath<Images, shared.ImageResource> {
        switch self {
        case .voice:
            \.personaPerson
        case .chat:
            \.personaChat
        }
    }
}
