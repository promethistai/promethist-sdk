//
//  TranscriptionRoot.swift
//  Promethist
//
//  Created by Vojtěch Vošmík on 11.01.2025.
//  Copyright © 2025 orgName. All rights reserved.
//

import Combine
import SwiftUI
@_implementationOnly import shared

struct TranscriptionRoot: View {

    @Environment(\.safeAreaInsets) private var safeAreaInsets
    @Environment(\.keyboardShowing) var keyboardShowing

    @StateObject private var store: Store
    @State private var showSearchBar = true
    let highlightColor: SwiftUI.Color
    let currentHighlightColor: SwiftUI.Color

    init(
        showSearchBar: Bool = false,
        highlightColor: SwiftUI.Color? = nil,
        currentHighlightColor: SwiftUI.Color? = nil,
        messages: [Message]
    ) {
        self.showSearchBar = showSearchBar
        self.highlightColor = highlightColor ?? .cyan.opacity(0.3)
        self.currentHighlightColor = currentHighlightColor ?? .yellow.opacity(0.7)
        self._store = StateObject(wrappedValue: Store(transcriptions: messages))
    }

    var body: some View {
        ZStack(alignment: .bottom) {
            Color.clear

            VStack {
                ScrollViewReader { scrollProxy in
                    List(0..<store.transcriptions.count, id: \.self) { index in
                        let transcription = store.transcriptions[index]
                        let isFirst = transcription.id == store.transcriptions.first?.id
                        let isLast = transcription.id == store.transcriptions.last?.id

                        ZStack {
                            if !transcription.text.normalizeChat().isEmpty {
                                TranscriptionRow(
                                    message: transcription,
                                    store: store,
                                    highlightColor: highlightColor,
                                    currentHighlightColor: currentHighlightColor,
                                    bold: false,
                                    link: true
                                )
                                .padding([.leading, .trailing], 20)
                                .padding(.bottom, .medium)
                            }
                        }
                        .padding(.top, isFirst ? topOffset : 0)
                        .padding(.bottom, isLast ? bottomOffset : 0)
                        .listRowInsets(EdgeInsets())
                        .listRowSeparator(.hidden)
                        .listRowBackground(Color.clear)
                        .onAppear { store.onScreenID[transcription.id] = index }
                        .onDisappear { store.onScreenID.removeValue(forKey: transcription.id) }
                        .id(transcription.id)
                    }
                    .listStyle(PlainListStyle())
                    .scrollContentBackground(.hidden)
                    .onChange(of: store.currentPosition) { [lastID = store.currentID] _ in
                        if store.keyword.isEmpty {
                            return
                        }

                        let currentID = store.currentID
                        if lastID != currentID {
                            withAnimation {
                                scrollProxy.scrollTo(currentID, anchor: .center)
                            }
                        } else if let currentID, store.onScreenID[currentID] == nil {
                            withAnimation {
                                scrollProxy.scrollTo(currentID, anchor: .center)
                            }
                        }
                    }
                    .environment(\.openURL, OpenURLAction { url in
                        switch url.scheme {
                        case positionScheme:
                            if let host = url.host(), let position = Int(host) {
                                store.scrollToPosition(position)
                            }
                            return .handled
                        default:
                            return .systemAction
                        }
                    })
                    .onAppear {
                        // Scroll to the last item in the list on appear
                        if let lastID = store.transcriptions.last?.id {
                            scrollProxy.scrollTo(lastID, anchor: .bottom)
                        }
                    }
                }
                .overlay(alignment: .bottom) {
                    ZStack(alignment: .bottomLeading) {
                        PromethistBottomFadeView()
                            .offset(y: safeAreaInsets.bottom)

                        safeSearchBar
                            .padding(.bottom, keyboardShowing ? 0 : safeAreaInsets.bottom)
                            .animation(.easeInOut, value: keyboardShowing)
                    }
                    //.transition(.move(edge: .bottom).combined(with: .opacity))
                }
            }
        }
        .ignoresSafeArea(.container, edges: [.top, .bottom])
        .onAppear {
            UITableView.appearance().contentInset.top = -35
        }
    }

    private var topOffset: CGFloat {
        80 + safeAreaInsets.top + AppTheme.Spacing.mediumLarge.rawValue
    }

    private var bottomOffset: CGFloat {
        60 + safeAreaInsets.bottom + AppTheme.Spacing.mediumLarge.rawValue
    }
}

extension TranscriptionRoot {

    private func dismissSearchBar() {
        store.reset()
        showSearchBar = false
    }

    @ViewBuilder
    var safeSearchBar: some View {
        SearchBar(
            store: store,
            dismiss: dismissSearchBar
        )
        .padding(.horizontal, 20)
        .ignoresSafeArea(.all)
    }
}

extension String {

    func normalizeChat() -> String {
        self.replacingOccurrences(of: "\n", with: "")
    }
}
