//
//  MicPowerCalculatorV2.swift
//  Promethist
//
//  Created by Vojtěch Vošmík on 11.01.2025.
//  Copyright © 2025 orgName. All rights reserved.
//

import Foundation

class DecibelToPercentageConverter {
    
    private let minDb: Double = -60.0
    private let maxDb: Double = -11.0

    private let minResult = 0.08

    /// Converts a decibel value to a percentage (0 to 1) based on the loudness of a human voice.
    /// - Parameter decibel: The decibel value to convert.
    /// - Returns: A percentage value (0 to 100) representing the relative loudness.
    func convertToPercentage(from decibel: Double) -> Double {
        // Ensure the value is clamped within the valid range
        let clampedDecibel = max(minDb, min(decibel, maxDb))
        // Normalize the value to a 0-100 scale
        let percentage = ((clampedDecibel - minDb) / (maxDb - minDb))

        if percentage < minResult {
            return minResult
        }

        return percentage
    }
}
