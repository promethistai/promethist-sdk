//
//  Publisher.swift
//  Promethist
//
//  Created by Vojtěch Vošmík on 11.01.2025.
//  Copyright © 2025 orgName. All rights reserved.
//

import Foundation
import Combine

// https://www.fatbobman.com/posts/combineAndAsync/

public extension Publisher {
    func task<T>(maxPublishers: Subscribers.Demand = .unlimited,
                 _ transform: @escaping (Output) async -> T) -> Publishers.FlatMap<Deferred<Future<T, Never>>, Self> {
        flatMap(maxPublishers: maxPublishers) { value in
            Deferred {
                Future { promise in
                    Task {
                        let output = await transform(value)
                        promise(.success(output))
                    }
                }
            }
        }
    }
}

public extension Publisher where Self.Failure == Never {
    func emptySink() -> AnyCancellable {
        sink(receiveValue: { _ in })
    }
}
