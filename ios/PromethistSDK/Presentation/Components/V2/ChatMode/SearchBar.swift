//
//  SearchBar.swift
//  Promethist
//
//  Created by Vojtěch Vošmík on 11.01.2025.
//  Copyright © 2025 orgName. All rights reserved.
//

import Foundation
import SwiftUI

struct SearchBar: View {
    @ObservedObject var store: Store
    var dismiss: () -> Void

    @State private var searching = false
    @FocusState private var focused: Bool

    @State var isExpanded: Bool = false

    var body: some View {
        HStack {
            HStack(alignment: .center, spacing: .zero) {
                Image(resource: \.personaSearch)
                    .renderingMode(.template)
                    .resizable()
                    .scaledToFit()
                    .foregroundStyle(.white)
                    .frame(width: 28, height: 60)

                TextField("", text: $store.keyword, prompt: Text(resource: \.persona.searchInConversation).foregroundColor(.white.opacity(0.5)))
                    .textInputAutocapitalization(.never)
                    .focused($focused)
                    .frame(height: 60)
                    .promethistFont(size: 18)
                    .foregroundStyle(.white.opacity(0.8))
                    .background(
                        Rectangle()
                            .foregroundColor(.clear)
                    )
                    .contentShape(Rectangle())
                    .toolbar {
                        ToolbarItemGroup(placement: .keyboard) {
                            HStack {
                                previousButton
                                nextButton
                                Spacer()
                                doneButton
                            }
                        }
                    }
                    .onSubmit {
                        focused = false
                    }
                    .onTapGesture {
                        focused = true
                    }
                    .padding(.leading, .medium)
                    .isHidden(!isExpanded)

                if focused || !store.keyword.isEmpty {
                    positionText
                }
                if focused {
                    cancelButton
                }
            }
            .padding(.leading, .medium)
            .padding(.trailing, (!store.keyword.isEmpty && focused) ? .zero : .medium)
            .glassyBackground(cornerRadius: 30)
            .onTapGesture {
                if !isExpanded {
                    isExpanded = true
                    focused = true
                }
            }
        }
        .padding(.bottom, .medium)
        .animation(.easeInOut, value: focused)
        .animation(.spring(duration: 0.35, bounce: 0.24), value: isExpanded)
        .onChange(of: focused) { value in
            if !value && store.keyword.isEmpty {
                isExpanded = false
            }
        }
    }
}

extension SearchBar {
    @ViewBuilder
    var dismissButton: some View {
        Button(String(resource: \.persona.chatCancel)) {
            withAnimation {
                focused = false
            }
            dismiss()
        }
    }

    @ViewBuilder
    var cancelButton: some View {
        if !store.keyword.isEmpty {
            Button {
                store.reset()
                focused = false
                isExpanded = false
            }
            label: {
                Image(systemName: "plus.circle.fill") // 􀁍
                    .rotationEffect(.degrees(45))
                    .foregroundColor(.white.opacity(0.6))
                    .padding(.xsmall)
                    .frame(width: 60, height: 60)
                    .contentShape(Rectangle())
            }
            .buttonStyle(.plain)
        }
    }

    @ViewBuilder
    var positionText: some View {
        if let currentPosition = store.currentPosition, store.count > 0 {
            VStack {
                Text(currentPosition + 1, format: .number) + Text("/") + Text(store.count, format: .number)
            }
            .foregroundColor(.white.opacity(0.6))
        }
    }

    @ViewBuilder
    var nextButton: some View {
        Button {
            store.gotoNext()
        }
    label: {
            Image(systemName: "chevron.down") // 􀆈
                .foregroundColor((store.currentPosition ?? 0) >= store.count - 1 ? .secondary : .primary)
        }
        .disabled((store.currentPosition ?? 0) >= store.count - 1)
    }

    @ViewBuilder
    var previousButton: some View {
        Button {
            store.gotoPrevious()
        }
    label: {
            Image(systemName: "chevron.up") // 􀆇
                .foregroundColor(store.currentPosition ?? -1 <= 0 ? .secondary : .primary)
        }
        .disabled(store.currentPosition ?? -1 <= 0)
    }

    @ViewBuilder
    var doneButton: some View {
        Button {
            focused = false
        }
    label: {
            Text(resource: \.persona.chatDone)
        }
    }
}
