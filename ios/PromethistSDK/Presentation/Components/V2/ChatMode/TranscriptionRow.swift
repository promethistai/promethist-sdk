//
//  TranscriptionRow.swift
//  Promethist
//
//  Created by Vojtěch Vošmík on 11.01.2025.
//  Copyright © 2025 orgName. All rights reserved.
//

import Foundation
import SwiftUI
@_implementationOnly import shared
@_implementationOnly import Kingfisher

struct TranscriptionRow: View {
    
    let message: Message
    @ObservedObject var store: Store
    let highlightColor: SwiftUI.Color
    let currentHighlightColor: SwiftUI.Color
    let bold: Bool
    let link: Bool

    var textColor: SwiftUI.Color {
        message.isUser ? .white : Color(resource: \.personaChatBubbleForeground)
    }

    var avatarUrl: URL? {
        guard let personaId = message.personaId else { return nil }
        return URL(string: "https://repository.promethist.ai/avatar/resources/portraits/\(personaId)-1.0-m.png")
    }

    var body: some View {
        HStack(alignment: .top) {
            if message.isUser {
                Spacer()
            }

            Group {
                if let avatarUrl = avatarUrl, !message.isUser {
                    KFImage(avatarUrl)
                        .resizable()
                        .scaledToFill()
                } else if let personaId = message.personaId, !message.isUser {
                    Image(uiImage: PersonaResourcesManager().getPortrait(personaId: PersonaId.Companion().fromAvatarId(name: personaId), size: .m).toUIImage()!)
                        .resizable()
                        .scaledToFill()
                }
            }
            .frame(width: avatarSize, height: avatarSize)
            .background(Color(resource: \.personaSurface).opacity(0.2))
            .cornerRadius(avatarSize / 2)

            messageBubble

            if !message.isUser {
                Spacer()
            }
        }
        .padding(.leading, message.isUser ? 76 : 0)
    }

    @ViewBuilder private var messageBubble: some View {
        VStack(alignment: .leading, spacing: .xsmall) {
            HStack(alignment: .center, spacing: .xxsmall) {
                if let name = message.name {
                    Text(name)
                        .bold()
                        .font(AppTheme.PrimaryFont.regular(size: 12))
                        .foregroundColor(Color(resource: \.primaryLabel))
                        .padding(.vertical, .xsmall)
                        .padding(.horizontal, .small)
                        .background {
                            RoundedRectangle(cornerRadius: 10)
                                .fill(Color(resource: \.personaSurfaceSecondary))
                        }
                }

                Text(" • \(message.createdFormatted)")
                    .promethistFont(size: 12)
                    .foregroundColor(textColor.opacity(0.7))
            }

            text
                .promethistFont(size: 18)
                .foregroundColor(textColor)

            if let image = message.image {
                KFImage(URL(string: image))
                    .resizable()
                    .aspectRatio(contentMode: .fit)
            }
        }
        .padding(.medium)
        .background {
            if message.isUser {
                Color(resource: \.accent)
            } else {
                Color(resource: \.personaChatBubbleBackground)
            }
        }
        .cornerRadius(
            radius: 26,
            corners: message.isUser ? [.topLeft, .topRight, .bottomLeft] : [.topLeft, .topRight, .bottomRight]
        )
    }

    @ViewBuilder private var text: some View {
        let ranges = store.getKeywordsResult(for: message.id)
        if let ranges {
            attributedText(ranges)
        } else {
            Text(message.text.normalizeChat())
        }
    }

    func attributedText(_ ranges: KeywordsResult) -> some View {
        var text = AttributedString(message.text.normalizeChat())
        for transcriptionRange in ranges.transcriptionRanges {
            if let lowerBound = AttributedString.Index(transcriptionRange.range.lowerBound, within: text),
               let upperBound = AttributedString.Index(transcriptionRange.range.upperBound, within: text) {
                if link {
                    text[lowerBound..<upperBound].link = URL(string: "\(positionScheme)://\(transcriptionRange.position)")
                }
                if ranges.currentPosition == transcriptionRange.position {
                    text[lowerBound..<upperBound].swiftUI.backgroundColor = currentHighlightColor
                    if bold {
                        text[lowerBound..<upperBound].foundation.inlinePresentationIntent = .stronglyEmphasized
                    }
                } else {
                    text[lowerBound..<upperBound].swiftUI.backgroundColor = highlightColor
                }
            }
        }

        return Text(text)
    }
}

let positionScheme = "goPosition"
private let avatarSize: CGFloat = 60
