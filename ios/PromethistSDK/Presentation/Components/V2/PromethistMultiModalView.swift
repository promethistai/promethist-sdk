import Foundation
import SwiftUI
import GameController
@_implementationOnly import shared

struct PromethistMultiModalView: View {

    @EnvironmentObject private var screenInfo: ScreenInfo
    @EnvironmentObject private var textInputFocusObserver: TextInputFocusObserver
    @Environment(\.safeAreaInsets) private var safeAreaInsets
    @State private var questionnaireModalVisible: Bool = true

    private let isKeyboardConnected = GCKeyboard.coalesced != nil
    private let deviceType = UIDevice.current.userInterfaceIdiom

    // MARK: - Data
    var modalItem: Modal?
    var isHomeDisclaimerVisible: Bool

    let isTypingDisabled: Bool
    let isSendDisabled: Bool
    let showTextInput: Bool
    let isBottomOffsetEnabled: Bool
    let onPersonaSelected: (PersonaItem, PersonaType) -> Void
    let onUserInput: (String) -> Void
    let onBackToMenuClick: () -> Void
    let onHomeDisclaimerAction: (Bool) -> Void

    private var isModalActive: Bool {
        modalItem != nil || isHomeDisclaimerVisible
    }

    var body: some View {
        VStack {
            Spacer()

            content
                .background(Color(resource: \.personaSurface).opacity(isModalActive ? 1 : 0))
                .cornerRadius(.xxlarge)
        }
        .frame(maxHeight: isModalActive ? .infinity : 0)
        .animation(modalAnimation)
    }

    @ViewBuilder private var content: some View {
        VStack {
            Group {
                if isHomeDisclaimerVisible {
                    HomeDisclaimerModalView(
                        onConfirm: {
                            onHomeDisclaimerAction(true)
                        },
                        onCancel: {
                            onHomeDisclaimerAction(false)
                        }
                    )
                    .padding(.horizontal, .mediumLarge)
                } else if let item = modalItem as? SetPersonaItem {
                    SetPersonaModalView(
                        setPersonaItem: item,
                        onPersonaClick: {
                            onPersonaSelected($0, item.content.type)
                        }
                    )
                    .onAppear { textInputFocusObserver.isFocused = false }
                } else if let item = modalItem as? ActionsItem {
                    ActionsModalView(
                        actionsItem: item,
                        onItemClick: { item in
                            onUserInput(item.actionProcess)
                        }
                    )
                    .padding(.horizontal, .mediumLarge)
                    .onAppear { textInputFocusObserver.isFocused = false }
                } else if let item = modalItem as? InputItem {
                    InputModalView(
                        inputItem: item,
                        isTypingDisabled: isTypingDisabled,
                        isSendDisabled: isSendDisabled,
                        onUserInput: {
                            onUserInput($0)
                        }
                    )
                    .padding(.horizontal, .mediumLarge)
                    .padding(.top, .xsmall)
                    .onAppear { textInputFocusObserver.isFocused = false }
                } else if let item = modalItem as? QuestionnaireItem, questionnaireModalVisible {
                    QuestionnaireModalView(
                        questionnaireItem: item,
                        onUserInput: {
                            onUserInput($0)
                        }
                    )
                    .padding(.horizontal, .mediumLarge)
                    .onRotate { _ in
                        redrawQuestionnaireModal()
                    }
                    .onAppear { textInputFocusObserver.isFocused = false }
                } else if let item = modalItem as? GrowthContentItem {
                    GrowthContentView(
                        item: item,
                        onUserInput: {
                            onUserInput($0)
                        }
                    )
                    .padding(.horizontal, .mediumLarge)
                    .onAppear { textInputFocusObserver.isFocused = false }
                } else if let _ = modalItem as? SessionEndItem {
                    SessionEndModalView(onBackToMenuClick: onBackToMenuClick)
                        .padding(.horizontal, .mediumLarge)
                        .onAppear { textInputFocusObserver.isFocused = false }
                } else if let item = modalItem as? CorrectInputItem {
                    CorrectInputModalView(
                        requestText: RequestText(title: nil, text: item.correctInput.input),
                        isTypingDisabled: isTypingDisabled,
                        isSendDisabled: isSendDisabled,
                        onUserInput: {
                            onUserInput($0)
                        }
                    )
                    .padding(.horizontal, .mediumLarge)
                    .padding(.top, .xsmall)
                    .onAppear { textInputFocusObserver.isFocused = false }
                } else if let item = modalItem as? OrderSummaryItem {
                    OrderModalView(
                        orderItem: item,
                        onCancel: {
                            // TODO: Send user input
                        },
                        onSubmit: {
                            // TODO: Send user input
                        }
                    )
                    .onAppear { textInputFocusObserver.isFocused = false }
                } else if let item = modalItem as? DialPhoneItem {
                    DialPhoneModalView(
                        item: item,
                        onCancel: {
                            // TODO: Send user input
                        }
                    )
                    .padding(.horizontal, .mediumLarge)
                } else if let item = modalItem as? UserProfileItem {
                    UserProfileModalView(
                        item: item,
                        onSubmit: {
                            // TODO: Send user input
                        }
                    )
                    .padding(.horizontal, .mediumLarge)
                } else if let item = modalItem as? RatingItem {
                    
                } else if showTextInput {
                    TextInputView(
                        isTypingDisabled: isTypingDisabled,
                        isSendDisabled: isSendDisabled,
                        onSend: { text in
                            onUserInput(text)
                        }
                    )
                    .padding(.horizontal, .mediumLarge)
                    .padding(.top, .small)
                }
            }
            .padding([.top, .bottom], .medium)
            .padding(.bottom, .small)
            .frame(width: deviceType != .phone ? screenInfo.width / 1.5 : (screenInfo.width - horizontalPadding))
        }
    }

    private func redrawQuestionnaireModal() {
        Task {
            withAnimation { questionnaireModalVisible = false }
            try? await Task.sleep(nanoseconds: 0_500_000_000)
            withAnimation { questionnaireModalVisible = true }
        }
    }
}

private let horizontalPadding: CGFloat = AppTheme.Spacing.mediumLarge.rawValue * 2
fileprivate let modalAnimation: Animation = .spring(duration: 0.39, bounce: 0.28)
