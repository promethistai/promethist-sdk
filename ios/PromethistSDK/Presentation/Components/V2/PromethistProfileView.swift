import SwiftUI
@_implementationOnly import Kingfisher

struct PromethistProfileView: View {

    @ObservedObject var viewModel: ProfileViewModel

    var body: some View {
        HStack(alignment: .center, spacing: .mediumSmall) {
            avatar

            Text(viewModel.name ?? String(resource: \.auth.notLoggedIn))
                .promethistFont(size: 18, weight: .regular)
                .lineLimit(1)
                .truncationMode(.tail)
        }
    }

    @ViewBuilder private var avatar: some View {
        Group {
            if let avatar = viewModel.avatarUrl {
                KFImage(avatar)
                    .resizable()
                    .cornerRadius(avatarSize / 2)
            } else {
                Image(resource: \.personCropCircle)
                    .resizable()
                    .opacity(0.65)
            }
        }
        .aspectRatio(contentMode: .fill)
        .frame(width: avatarSize, height: avatarSize)
        .overlay(
            RoundedRectangle(cornerRadius: (avatarSize / 2))
                .stroke(Color(resource: \.personaGrey), lineWidth: 1)
        )
    }
}

private let avatarSize: CGFloat = 44
