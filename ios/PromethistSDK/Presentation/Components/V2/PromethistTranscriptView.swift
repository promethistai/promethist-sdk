import SwiftUI

struct PromethistTranscriptView: View {

    @State private var responseUUID: String? = nil
    @Environment(\.safeAreaInsets) private var safeAreaInsets

    let response: String?
    var isResponseVisible: Bool = true // TODO: Rework

    var responseOpacity: Double {
        if !isResponseVisible {
            return 0
        }
        return response != nil ? 1 : 0
    }

    var transcriptBottomOffset: CGFloat {
        safeAreaInsets.bottom > 0 ? 94 : 0
    }

    var responseMaxHeight: CGFloat {
        safeAreaInsets.bottom > 0 ? 208 : 148
    }

    var transcriptMaxHeight: CGFloat {
        safeAreaInsets.bottom > 0 ? 74 : 52
    }

    var body: some View {
        VStack {
            Spacer()

            // Persona response
            GeometryReader { proxy in
                PromethistAutoScrollText(
                    text: response ?? "",
                    startDelay: 3,
                    isResponseVisible: isResponseVisible,
                    maxHeight: responseMaxHeight
                )
                .frame(width: proxy.size.width)
                .frame(maxHeight: responseMaxHeight)
                .opacity(response != nil ? 1 : 0)
                .animation(responseAnimation, value: response)
            }
            .padding([.leading, .trailing], horizontalPadding)
            .frame(maxHeight: responseMaxHeight)
            .opacity(responseOpacity)
            .frame(maxWidth: .infinity, alignment: .center)
            .animation(.easeInOut, value: responseOpacity)
            .onChange(of: responseOpacity) { value in
                if value == 1.0 {
                    NotificationCenter.default.post(
                        name: PromethistAutoScrollText.textErasedNotification,
                        object: nil
                    )
                }
            }

            // User speech
            ZStack {}.frame(height: transcriptMaxHeight)
        }
    }
}

fileprivate let horizontalPadding: CGFloat = AppTheme.Spacing.mediumLarge.rawValue
fileprivate let responseAnimation: Animation = .easeInOut(duration: 0.35)
fileprivate let topRectangleId = "top"
fileprivate let bottomRectangleId = "bottom"
