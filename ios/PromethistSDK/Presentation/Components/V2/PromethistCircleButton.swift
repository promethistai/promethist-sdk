import SwiftUI
@_implementationOnly import shared

struct PromethistCircleButton: View {

    let icon: KeyPath<Images, shared.ImageResource>
    var isActive: Bool = false
    var isEnabled: Bool = true
    var onClick: (() -> Void)? = nil

    var body: some View {
        if let onClick = onClick {
            Button(action: onClick) {
                content
            }
            .disabled(!isEnabled)
        } else {
            content
        }
    }

    @ViewBuilder private var content: some View {
        ZStack {
            Image(resource: icon)
                .renderingMode(.template)
                .resizable()
                .scaledToFit()
                .glassyForeground(isActive: isActive)
                .frame(width: iconSize)
        }
        .frame(width: size, height: size, alignment: .center)
        .glassyBackground(isActive: isActive)
        .cornerRadius(radius)
        .animation(.easeInOut(duration: 0.6), value: isActive)
    }
}

private let size: CGFloat = 60
private let iconSize: CGFloat = 26
private let radius: CGFloat = size / 2
