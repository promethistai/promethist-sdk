import SwiftUI

struct PromethistTopFadeView: View {

    var isActive: Bool = true

    var body: some View {
        LinearGradient(
            gradient: Gradient(colors: [.black.opacity(0.8), .black.opacity(0.001)]),
            startPoint: .top,
            endPoint: .bottom
        )
        .frame(height: 243, alignment: .top)
        .opacity(isActive ? 1 : 0)
        .ignoresSafeArea(.all, edges: .all)
        .allowsHitTesting(false)
        .animation(.easeInOut(duration: 0.3), value: isActive)
    }
}

struct PromethistBottomFadeView: View {

    @Environment(\.safeAreaInsets) private var safeAreaInsets

    var isActive: Bool = true

    var body: some View {
        LinearGradient(
            gradient: Gradient(colors: [.black.opacity(0.8), .black.opacity(0.001)]),
            startPoint: .bottom,
            endPoint: .top
        )
        .frame(height: (safeAreaInsets.bottom + 60), alignment: .top)
        .opacity(isActive ? 1 : 0)
        .ignoresSafeArea(.all, edges: .all)
        .allowsHitTesting(false)
        .animation(.easeInOut(duration: 0.3), value: isActive)
    }
}
