import SwiftUI
@_implementationOnly import shared
import Kingfisher

struct PromethistToolbarView: View {

    @Binding var interactionMode: PromethistInteractionMode
    let onHomeClick: () -> Void
    let onProfileClick: () -> Void
    let onReset: () -> Void
    let onUpdateAvatarQulity: () -> Void
    let onSendTechnicalReport: () -> Void

    private let authClient = Client.shared.authClient()

    var body: some View {
        HStack(alignment: .center, spacing: .small) {
            PromethistCircleButton(
                icon: \.personaHome,
                onClick: onHomeClick
            )

            Spacer()

            PromethistInteractionToggleView(
                selection: $interactionMode
            )

            Spacer()

            NavigationLink {
                SettingsView(
                    onReset: onReset,
                    onUpdateAvatarQulity: onUpdateAvatarQulity,
                    onSendTechnicalReport: onSendTechnicalReport
                )
            } label: {
                PromethistCircleButton(
                    icon: \.personaSettings
                )
                .overlay {
                    if let avatarUrl = URL(string: "\(authClient.getStoredUserInfo()["avatarUrl"] ?? "")") {
                        KFImage(avatarUrl)
                            .resizable()
                            .clipped()
                            .clipShape(Circle())
                            .scaledToFit()
                    }
                }
            }
            .onNavigationLinkTap(onTap: onProfileClick)
        }
    }
}
