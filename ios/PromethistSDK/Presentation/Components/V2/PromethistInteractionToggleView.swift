import SwiftUI

struct PromethistInteractionToggleView: View {

    @Binding var selection: PromethistInteractionMode
    @Namespace var namespace

    var body: some View {
        HStack(alignment: .center, spacing: .zero) {
            ForEach(PromethistInteractionMode.allCases, id: \.id) { type in
                Button(action: {
                    Vibration.selection.vibrate()
                    self.selection = type
                }) {
                    item(with: type, isSelected: selection == type)
                        .animation(selectionAnimation, value: selection)
                }
            }
        }
        .padding(borderPadding)
        .glassyBackground(
            cornerRadius: itemCornerRadius,
            opacity: selection == .chat ? 1 : 0
        )
        .overlay(
            RoundedRectangle(cornerRadius: itemCornerRadius + borderPadding)
                .stroke(Color(resource: \.personaGlassyBackground), lineWidth: 1.17)
        )
    }

    @ViewBuilder private func item(with type: PromethistInteractionMode, isSelected: Bool) -> some View {
        ZStack(alignment: .center) {
            selectionIndicator(with: type, isSelected: isSelected)

            Image(resource: type.icon)
                .renderingMode(.template)
                .resizable()
                .scaledToFit()
                .glassyForeground(isActive: isSelected)
                .frame(width: itemIconSize, height: itemIconSize)
        }
    }

    @ViewBuilder private func selectionIndicator(with type: PromethistInteractionMode, isSelected: Bool) -> some View {
        Group {
            if isSelected {
                RoundedRectangle(cornerRadius: itemCornerRadius)
                    .fill(Color(resource: \.personaSurface))
                    .matchedGeometryEffect(id: matchedGeometryId, in: namespace, properties: .frame)
            } else {
                Color.clear
            }
        }
        .frame(width: itemWidth, height: itemHeight)
    }
}

private let matchedGeometryId = "selection"
private let selectionAnimation: Animation = .spring(duration: 0.35, bounce: 0.38)
private let itemHeight: CGFloat = 60
private let itemWidth: CGFloat = 88
private let itemIconSize: CGFloat = 30
private let itemCornerRadius: CGFloat = itemHeight / 2
private let borderPadding: CGFloat = 0
