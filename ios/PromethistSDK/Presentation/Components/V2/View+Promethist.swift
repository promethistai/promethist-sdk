import SwiftUI
@_implementationOnly import shared

extension View {

    func glassyBackground(
        color: SwiftUI.Color? = nil,
        cornerRadius: CGFloat = 0,
        opacity: CGFloat = 1,
        isActive: Bool = false
    ) -> some View {
        self.background {
            ZStack {
                if isActive {
                    SwiftUI.Color(resource: \.personaGlassyActiveBackground)
                } else {
                    if let color {
                        Rectangle()
                            .fill(color)

                        BackdropBlurView(radius: 35.3)
                    } else {
                        Rectangle()
                            .foregroundStyle(.ultraThinMaterial.opacity(0.65))
                    }
                }
            }
            .cornerRadius(cornerRadius)
            .opacity(opacity)
        }
    }

    func glassyForeground(
        isActive: Bool = false
    ) -> some View {
        self.foregroundStyle(
            isActive
                ? Color(resource: \.accent)
                : Color(resource: \.personaGlassyForeground)
        )
    }

    func promethistFont(
        size: CGFloat, weight: Font.Weight = .regular
    ) -> some View {
        self.font(.system(size: size, weight: weight))
    }

    func withStatusBarStyle(_ style: UIStatusBarStyle, onDisappear: UIStatusBarStyle? = nil) -> some View {
        self.onAppear {
            UIApplication.shared.statusBarStyle = style
        }.onDisappear {
            if let onDisappear = onDisappear {
                UIApplication.shared.statusBarStyle = onDisappear
            }
        }
    }
}
