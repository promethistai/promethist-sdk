import SwiftUI
@_implementationOnly import shared

struct PromethistAudioIndicatorView: View {

    let state: AudioIndicatorState

    private let converter = DecibelToPercentageConverter()
    @State private var micPercentage: CGFloat = 0

    private var backgroundColor: SwiftUI.Color {
        switch state {
        case .startspeaking, .speaking: Color(resource: \.personaGreenGlassyBackground)
        default: Color(resource: \.personaGlassyBackground)
        }
    }

    var body: some View {
        ZStack(alignment: .center) {
            switch state {
            case .none: EmptyView()
            case .playing: audioWave(isActive: false)
            case .startspeaking: text(text: String(resource: \.persona.startSpeaking))
            case .speaking: audioWave(isActive: true)
            default: EmptyView()
            }

        }
        .frame(height: height, alignment: .center)
        .glassyBackground(color: backgroundColor)
        .cornerRadius(radius)
        .animation(.easeInOut(duration: 0.3), value: state)
        .onReceive(appEvents) { event in
            switch event {
            case .micPowerUpdate(let power):
                withAnimation(.easeInOut(duration: 0.05)) {
                    micPercentage = converter.convertToPercentage(from: Double(power))
                }

            default: break
            }
        }
    }

    @ViewBuilder private func audioWave(isActive: Bool) -> some View {
        if isActive {
            Image(resource: \.personaAudioIndicatorActive)
                .resizable()
                .frame(maxWidth: .infinity, alignment: .center)
                .frame(height: 37 * micPercentage)
                .padding(.mediumSmall)
        } else {
            Image(resource: \.personaAudioIndicatorInactive)
                .resizable()
                .scaledToFit()
                .frame(maxWidth: .infinity, alignment: .center)
                .frame(height: 37)
                .padding(.mediumSmall)
        }
    }

    @ViewBuilder private func text(text: String) -> some View {
        Text(text)
            .foregroundStyle(.white)
            .promethistFont(size: 17, weight: .medium)
            .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .center)
            .padding(.mediumSmall)
            .lineLimit(1)
            .truncationMode(.tail)
    }
}

private let height: CGFloat = 60
private let radius: CGFloat = height / 2
