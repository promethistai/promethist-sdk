//
//  SimulatorDebugControlls.swift
//  Promethist
//
//  Created by Vojtěch Vošmík on 24.11.2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import SwiftUI

struct SimulatorDebugControls: View {

    let onUserInput: (String) -> Void

    var body: some View {
        ZStack {
            VStack(alignment: .leading, spacing: .mediumSmall) {
                Button(action: {
                    onUserInput("ok")
                }) {
                    Text("ok")
                }

                Button(action: {
                    onUserInput("yes")
                }) {
                    Text("yes")
                }

                Button(action: {
                    onUserInput("no")
                }) {
                    Text("no")
                }
            }
            .padding(.medium)
            .background(.black.opacity(0.3))
            .cornerRadius(.medium)
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .leading)
    }
}
