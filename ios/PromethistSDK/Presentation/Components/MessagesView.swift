import SwiftUI
@_implementationOnly import shared

struct MessagesView: View {

    @EnvironmentObject private var textInputFocusObserver: TextInputFocusObserver
    @Environment(\.safeAreaInsets) private var safeAreaInsets

    let messages: [Message]

    var body: some View {
        ScrollView(showsIndicators: false) {
            ScrollViewReader { proxy in
                LazyVStack(spacing: .zero) {
                    ForEach(messages.filter { !$0.text.isEmpty }, id: \.id) { message in
                        MessageView(message: message)
                            .padding([.leading, .trailing], messagesSpacing)
                            .padding(.bottom, .medium)
                            .id(message.id)
                    }

                    Spacer().frame(height: AppTheme.Spacing.mediumLarge.rawValue)

                    Divider()
                        .opacity(0)
                        .id(bottomId)
                }
                .frame(maxWidth: maxWidth)
                .padding(.top, topOffset)
                .onChange(of: messages) { _ in
                    scrollToBottom(proxy, delay: 0.4)
                }
                .onChange(of: textInputFocusObserver.isFocused, perform: { _ in
                    scrollToBottom(proxy, delay: 0.6)
                })
                .onAppear {
                    scrollToBottom(proxy, animate: false)
                }
            }
        }
    }

    private var topOffset: CGFloat {
        80 + safeAreaInsets.top + AppTheme.Spacing.mediumLarge.rawValue
    }

    private func scrollToBottom(_ proxy: ScrollViewProxy, animate: Bool = true, delay: CGFloat = 0) {
        DispatchQueue.main.asyncAfter(deadline: .now() + delay) {
            withAnimation(animate ? .linear : .none) {
                proxy.scrollTo(bottomId, anchor: .bottom)
            }
        }
    }
}

private let bottomId = "bottom"
private let maxWidth: CGFloat = 1200
private let messagesSpacing: CGFloat = 10
