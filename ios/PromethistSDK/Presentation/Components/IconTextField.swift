//
//  IconTextField.swift
//  Promethist
//
//  Created by Vojtěch Vošmík on 25.11.2024.
//  Copyright © 2024 orgName. All rights reserved.
//

import SwiftUI
@_implementationOnly import shared

struct IconTextField: View {

    let icon: KeyPath<Images, shared.ImageResource>
    let title: String
    let text: Binding<String>

    var body: some View {
        HStack(alignment: .center, spacing: 10) {
            Image(resource: icon)
                .renderingMode(.template)
                .resizable()
                .scaledToFit()
                .frame(width: 18, height: 18)
                .foregroundColor(Color(resource: \.primaryLabel).opacity(0.8))

            TextField(title, text: text)
                .font(AppTheme.Fonts.regular)
        }
        .padding(.horizontal, 10)
        .padding(.vertical, 15)
        .background(.regularMaterial, in: RoundedRectangle(cornerRadius: 10))
        .cornerRadius(10)
    }
}
