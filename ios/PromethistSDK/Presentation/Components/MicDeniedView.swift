//
//  MicDeniedView.swift
//  Promethist
//
//  Created by Vojtěch Vošmík on 21.02.2024.
//  Copyright © 2024 orgName. All rights reserved.
//

import SwiftUI

struct MicDeniedView: View {

    var body: some View {
        ZStack {
            VStack(alignment: .center, spacing: .large) {
                Text(resource: \.settings.micPermissionDenied)
                    .multilineTextAlignment(.center)

                Image(resource: \.icMicDenied)
                    .resizable()
                    .scaledToFit()
                    .frame(width: 80)

                IconButton(
                    text: String(resource: \.settings.openPermissionSettings),
                    onClick: openPermissionSettings
                )
                .padding(.top, .large)
            }
            .padding(.horizontal, .large)
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .background(regularMaterial)
    }
}
