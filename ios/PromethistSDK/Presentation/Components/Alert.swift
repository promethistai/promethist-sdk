//
//  Alert.swift
//  Promethist
//
//  Created by Vojtěch Vošmík on 29.10.2024.
//  Copyright © 2024 orgName. All rights reserved.
//

import SwiftUI
@_implementationOnly import shared

func buildErrorAlert(
    for errorItem: ErrorItem,
    dismissAction: @escaping () -> Void
) -> Alert {
    return Alert(
        title: Text("\(errorItem.source) \(Resources().strings.error.errorTitle.localized())"),
        message: Text(errorItem.localizedText?.localized() ?? errorItem.text),
        dismissButton: .default(Text("OK"), action: dismissAction)
    )
}

func buildSecondaryErrorAlert(
    for errorItem: ErrorItem,
    dismissAction: @escaping () -> Void,
    retryAction: @escaping () -> Void
) -> Alert {
    return Alert(
        title: Text("\(errorItem.source) \(Resources().strings.error.errorTitle.localized())"),
        message: Text(errorItem.localizedText?.localized() ?? errorItem.text),
        primaryButton: .default(Text("OK"), action: dismissAction),
        secondaryButton: .cancel(Text(resource: \.settings.restart), action: retryAction)
    )
}
