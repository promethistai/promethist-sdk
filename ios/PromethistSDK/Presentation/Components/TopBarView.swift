import SwiftUI
@_implementationOnly import shared
import Kingfisher

struct TopBarView: View {
    
    let title: String
    let subTitle: String
    let isToolbarVisible: Bool
    let isTitlesVisible: Bool
    let isHelpVisible: Bool
    let onReset: () -> Void
    let onUpdateAvatarQulity: () -> Void
    let onInfoClick: () -> Void
    let onSendTechnicalReport: () -> Void

    private let authClient = Client.shared.authClient()

    private var words: [String] {
        title.components(separatedBy: .whitespacesAndNewlines)
    }

    private var userInfo: [String : Any] {
        authClient.getStoredUserInfo()
    }

    var body: some View {
        VStack {
            if isToolbarVisible {
                HStack(alignment: .center, spacing: .zero) {
                    NavigationLink {
                        SettingsView(
                            onReset: onReset,
                            onUpdateAvatarQulity: onUpdateAvatarQulity,
                            onSendTechnicalReport: onSendTechnicalReport
                        )
                    } label: {
                        SquareButton(resource: \.iconOptions)
                    }

                    Spacer()

                    VStack(alignment: .center, spacing: .xsmall) {
                        Text(title)
                            .font(AppTheme.PrimaryFont.bold(size: words.count < 2 ? 25 : words.count < 3 ? 22.5 : 17.5))
                                .foregroundColor(.white)
                                .shadow(color: .black, radius: 1)

                        Text(subTitle)
                            .font(AppTheme.Fonts.heading3)
                            .foregroundColor(.white)
                            .shadow(color: .black, radius: 1)
                    }
                    .opacity(isTitlesVisible ? 1 : 0)

                    Spacer()

                    NavigationLink {
                        ProfileScreen()
                    } label: {
                        if let avatarUrl = URL(string: (userInfo["avatarUrl"] as? String ?? "")) {
                            KFImage(avatarUrl)
                                .resizable()
                                .clipped()
                                .cornerRadius(24)
                                .clipShape(RoundedRectangle(cornerRadius: 24))
                                .scaledToFit()
                                .frame(width: 48, height: 48, alignment: .center)
                                .padding(.small)
                        } else {
                            SquareButton(resource: \.iconProfile)
                        }
                    }
                }
                .padding([.leading, .trailing], .small)
            }

            if isHelpVisible {
                HStack {
                    Spacer()

                    Button(action: onInfoClick) {
                        SquareButton(resource: \.icQuestionMarkCircle)
                    }
                }
                .padding([.leading, .trailing], .small)
            }

            Spacer()
        }
        
        Spacer()
    }
}
