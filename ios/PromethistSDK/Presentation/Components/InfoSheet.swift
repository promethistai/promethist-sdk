//
//  InfoSheet.swift
//  Promethist
//
//  Created by Vojtěch Vošmík on 18.03.2024.
//  Copyright © 2024 orgName. All rights reserved.
//

import SwiftUI
@_implementationOnly import shared
@_implementationOnly import Kingfisher

struct InfoSheet: View {

    var appConfig: AppConfig

    @State private var selectedTopic: TutorialData.TopicItem? = nil

    private var data: TutorialData {
        appConfig.tutorialData ?? TutorialData.companion.Empty
    }

    private var topics: [TutorialData.TopicItem] {
        data.topics
    }

    var body: some View {
        ZStack {
            if let selectedTopic {
                itemDetail(selectedTopic)
            } else {
                items
            }
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .padding(.horizontal, .mediumLarge)
        .padding(.vertical, .large)
    }

    @ViewBuilder private var items: some View {
        VStack(alignment: .center, spacing: .mediumLarge) {
            DynamicText(data.title, weight: .bold)
                .padding([.top, .bottom], .xsmall)
                .padding(.bottom, .xxsmall)

            ScrollView {
                LazyVStack(spacing: .medium) {
                    ForEach(topics, id: \.self) { item in
                        Button(action: {
                            withAnimation {
                                selectedTopic = item
                            }
                        }) {
                            Text(item.title)
                                .font(AppTheme.PrimaryFont.bold(size: item.title.count > 31 ? 14 : 16))
                                .foregroundColor(.primary)
                                .frame(maxWidth: .infinity)
                                .padding(.vertical, .medium)
                                .padding(.horizontal, .mediumSmall)
                                .background(accentSecondary)
                                .cornerRadius(.xxlarge)
                                .multilineTextAlignment(.center)
                        }
                    }
                }
            }
        }
        .frame(maxWidth: .infinity)
    }

    @ViewBuilder private func itemDetail(_ item: TutorialData.TopicItem) -> some View {
        VStack(alignment: .center, spacing: .mediumLarge) {
            DynamicText(item.title, weight: .bold)
                .padding([.top, .bottom], .xsmall)
                .padding(.bottom, .xxsmall)

            ScrollView {
                VStack(alignment: .center, spacing: .mediumLarge) {
                    if let image = item.image, let url = URL(string: image) {
                        KFImage(url)
                            .resizable()
                            .scaledToFit()
                            .frame(height: 150, alignment: .center)
                    }

                    Text(item.text)
                        .font(AppTheme.PrimaryFont.regular(size: 16))
                        .foregroundColor(.primary)
                }
            }

            Button(action: {
                withAnimation {
                    selectedTopic = nil
                }
            }) {
                Text(data.returnText)
                    .font(AppTheme.PrimaryFont.bold(size: item.title.count > 31 ? 14 : 16))
                    .foregroundColor(.primary)
                    .padding(.vertical, .medium)
                    .padding(.horizontal, .mediumLarge)
                    .background(accentSecondary)
                    .cornerRadius(.xxlarge)
                    .multilineTextAlignment(.center)
            }
        }
        .frame(maxWidth: .infinity)
    }
}
