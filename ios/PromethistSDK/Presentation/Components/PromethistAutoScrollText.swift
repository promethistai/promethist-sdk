import SwiftUI
@_implementationOnly import shared

struct PromethistAutoScrollText: UIViewControllerRepresentable {

    static let textErasedNotification = NSNotification.Name("promethist.auto.scrolltext.erased")

    var text: String
    var startDelay: CGFloat
    var isResponseVisible: Bool
    var maxHeight: CGFloat

    init(text: String, startDelay: CGFloat, isResponseVisible: Bool, maxHeight: CGFloat) {
        self.text = text
        self.startDelay = startDelay
        self.isResponseVisible = isResponseVisible
        self.maxHeight = maxHeight
    }

    func makeUIViewController(context: Context) -> PromethistAutoScrollTextViewController {
        let vc = PromethistAutoScrollTextViewController()
        vc.startDelay = startDelay
        vc.text = text
        vc.maxHeight = maxHeight
        vc.setText(text)
        return vc
    }

    func updateUIViewController(_ viewController: PromethistAutoScrollTextViewController, context: Context) {
        viewController.setText(text)
        if isResponseVisible && !text.isEmpty {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                viewController.scrollToBottom()
            }
        }
    }
}

class PromethistAutoScrollTextViewController: UIViewController, UIScrollViewDelegate {

    var text: String = ""
    var startDelay: CGFloat = 0
    var maxHeight: CGFloat = 500

    private let scrollView = UIScrollView()
    private let label = UILabel()
    private var animator: UIViewPropertyAnimator?

    override func viewDidLoad() {
        super.viewDidLoad()
        setup()

        NotificationCenter.default.addObserver(
            self,
            selector: #selector(textErased),
            name: PromethistAutoScrollText.textErasedNotification,
            object: nil
        )
    }

    @objc func textErased() {
        stopAnimating()
    }

    func setText(_ text: String) {
        self.text = text
        label.text = text
        scrollView.delegate = self
    }

    func stopAnimating() {
        animator?.stopAnimation(true)
    }

    func scrollToBottom() {
        // Auto scroll
        let inset = self.scrollView.contentSize.height - self.scrollView.bounds.height
        if inset > 5 {
            DispatchQueue.main.asyncAfter(deadline: .now() + startDelay) { [weak self] in
                guard let self = self else { return }
                let duration: CGFloat = self.calculateScrollingDuration(text: self.text, scrollSpeed: 18)
                let bottomOffset = CGPoint(
                    x: 0,
                    y: inset + self.scrollView.contentInset.bottom
                )

                self.animator?.stopAnimation(true)
                self.animator = UIViewPropertyAnimator(duration: TimeInterval(duration), curve: .linear) {
                    self.scrollView.setContentOffset(bottomOffset, animated: false)
                }
                self.animator?.addCompletion { position in
                    if position == .end {}
                }
                self.animator?.startAnimation()
            }
        }
    }

    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        animator?.stopAnimation(true)
    }

    private func setup() {
        // ScrollView setup
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(scrollView)
        scrollView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0).isActive = true
        scrollView.topAnchor.constraint(equalTo: view.topAnchor, constant: 0).isActive = true
        scrollView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0).isActive = true

        // Label setup
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = text
        label.numberOfLines = 0
        label.font = .systemFont(ofSize: 24, weight: .medium)
        label.textColor = .white
        label.textAlignment = .center

        // Label shadow setup
        label.layer.shadowColor = UIColor.black.cgColor
        label.layer.shadowRadius = 1.0
        label.layer.shadowOpacity = 0.75
        label.layer.shadowOffset = CGSize(width: 1, height: 1)
        label.layer.masksToBounds = false

        // Constraints
        scrollView.addSubview(label)
        label.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: 0).isActive = true
        label.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor, constant: 0).isActive = true
        label.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: 0).isActive = true
        label.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 0).isActive = true
        label.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor, constant: 0).isActive = true
    }

    private func calculateScrollingDuration(text: String, scrollSpeed: Double) -> TimeInterval {
        let textLength = Double(text.count)
        let duration = textLength / scrollSpeed
        return duration > 5 ? duration : 5
    }
}
