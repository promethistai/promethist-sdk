import SwiftUI

struct ListeningIndicator: View {

    private let micPowerCalculator = MicPowerCalculator()

    var hideBlackGradient: Bool = false
    @Binding var isListening: Bool
    @State private var micPower: CGFloat = 0

    var body: some View {
        GeometryReader { geometry in
            VStack {
                Spacer()
                
                ZStack {
                    Rectangle()
                            .fill(EllipticalGradient(
                                    gradient: .init(colors: [ .black.opacity(0.0), .black.opacity(0.5) ]),
                                    center: .bottom,
                                    startRadiusFraction: 0.9,
                                    endRadiusFraction: 0.0
                            ))
                            .frame(width: geometry.size.width, height: geometry.size.height / 3.5)
                            .opacity(hideBlackGradient ? 0 : 1)
                    Rectangle()
                            .fill(EllipticalGradient(
                                    gradient: .init(colors: [ .green.opacity(0.0), .green.opacity(0.9) ]),
                                    center: .bottom,
                                    startRadiusFraction: 0.9,
                                    endRadiusFraction: (micPower * 0.85)
                            ))
                            .frame(width: geometry.size.width, height: geometry.size.height / 3.5)
                            .opacity(isListening ? 1.0 : 0.0)
                            //.opacity(micPower > 0 ? (Double(micPower) / 100 * 2) : 1.0)
                            .opacity(1.0 - (micPower * 1.1))
                            .animation(.easeOut(duration: 0.5), value: isListening)
                }
            }
        }
        .ignoresSafeArea(.container, edges: .all)
        .onReceive(appEvents) { event in
            switch event {
            case .micPowerUpdate(let power):
                let value = micPowerCalculator.calculatePadding(power)
                micPower = value
            default: break
            }
        }
    }
}
