import SwiftUI
@_implementationOnly import shared

struct SecondaryButton: View {
    
    let text: String
    let onClick: () -> Void
    
    var body: some View {
        Button(action: onClick) {
            ZStack(alignment: .center) {
                Text(text)
                    .font(.system(size: 13, weight: .semibold))
                    .foregroundColor(Color.accentColor)
                    .padding(.vertical, .mediumSmall)
                    .padding(.horizontal, .large)
            }
        }
    }
}
