import SwiftUI
import Combine
@_implementationOnly import shared
@_implementationOnly import Kingfisher

extension GrowthModuleView {

    var moduleBackground: SwiftUI.Color {
        switch module.state {
        case .done:
            return Color(resource: \.growthModuleDone)
        case .theNew:
            return Color(resource: \.growthModuleNew)
        case .locked:
            return Color(resource: \.growthModuleLocked)
        default:
            return Color(resource: \.growthModuleInProgress)
        }
    }

    var submoduleBackground: SwiftUI.Color {
        switch module.state {
        case .done:
            return Color(resource: \.growthSubmoduleDone)
        case .theNew:
            return Color(resource: \.growthSubmoduleNew)
        case .locked:
            return Color(resource: \.growthSubmoduleLocked)
        default:
            return Color(resource: \.growthSubmoduleInProgress)
        }
    }

    var progressBarBackground: SwiftUI.Color {
        Color(resource: \.growthProgressBackground)
    }

    var progressBarTint: SwiftUI.Color {
        switch module.state {
        case .locked:
            return Color(resource: \.growthProgressLocked)
        default:
            return Color(resource: \.growthProgress)
        }
    }

    var moduleBorder: SwiftUI.Color {
        switch module.state {
        case .done:
            return Color(resource: \.growthModuleInProgress)
        default:
            return moduleBackground.opacity(0)
        }
    }
}

struct GrowthModuleView: View {
    
    let module: GrowthModule
    let maxWidth: CGFloat
    let onUserInput: (String) -> Void
    
    @State var isExpanded: Bool

    var imageUrl: URL? {
        URL(string: module.imageURL ?? "")
    }

    var fontSize: CGFloat {
        isExpanded ? 13 : module.title.count > 22 ? 18 : 20
    }

    var body: some View {
        VStack {
            HStack(alignment: .center, spacing: .xxsmall) {
                VStack(alignment: .leading, spacing: 6) {
                    Text(module.title)
                        .font(AppTheme.PrimaryFont.bold(size: fontSize))
                        .foregroundColor(.black)
                        .lineLimit(3)
                        .truncationMode(.tail)
                        .fixedSize(horizontal: false, vertical: true)

                    if let description = module.description_, !description.isEmpty, !isExpanded {
                        Text(description)
                            .lineLimit(1)
                            .font(.system(size: 13))
                            .foregroundColor(.black)
                    }
                }
                .padding(.vertical, imageUrl != nil ? .medium : .zero)

                Spacer()

                ZStack {
                    if let imageUrl {
                        KFImage(imageUrl)
                            .fade(duration: 0.3)
                            .forceTransition()
                            .resizable()
                            .scaledToFit()
                            .frame(width: isExpanded ? 70 : 158, height: isExpanded ? 30 : 92, alignment: .center)
                    }
                }
                .frame(minWidth: 20, minHeight: isExpanded ? 25 : 60, alignment: .center)
            }
            .padding(.top, isExpanded ? 7 : 0)

            if isExpanded {
                ForEach(module.submoduleGroups.indices, id: \.self) { index in
                    let group = module.submoduleGroups[index]

                    if index > 0 {
                        Divider()
                    }
                    if group.title != nil {
                        Text(group.title!)
                            .font(AppTheme.Fonts.heading2)
                    }

                    ForEach(group.submodules, id: \.action) { submodule in
                        growthSubmoduleView(submodule)
                    }
                }
                ForEach(module.submodules.indices, id: \.self) { index in
                    if index > 0 {
                        Divider()
                    }

                    ForEach(module.submodules[index].indices, id: \.self) { index2 in
                        growthSubmoduleView(module.submodules[index][index2])
                    }
                }
            }
        }
        .padding(.leading, .medium)
        .padding(.trailing, (imageUrl == nil || isExpanded) ? .medium : .small)
        .padding(.top, imageUrl == nil ? .medium : .zero)
        .padding(.bottom, (imageUrl == nil || isExpanded) ? .medium : .zero)
        .contentShape(Rectangle())
        .onTapGesture {
            isExpanded.toggle()
        }
        .frame(maxWidth: maxWidth)
        .background(moduleBackground)
        .cornerRadius(.xlarge)
        .overlay(
            RoundedRectangle(cornerRadius: AppTheme.CornerRadius.xlarge.rawValue)
                .stroke(moduleBorder, lineWidth: 1)
        )
        .animation(.easeOut(duration: 0.2), value: module)
    }
    
    private func growthStateView(_ state: GrowthState, _ color: SwiftUI.Color, size: CGFloat = 20) -> some View {
        HStack {
            if state == .theNew || state == .locked || state == .done {
                Image(resource: state == .theNew ? \.icWandAndStars : state == .locked ? \.icLockFill : \.icCheckmark)
                    .renderingMode(.template)
                    .resizable()
                    .scaledToFit()
                    .scaleEffect(1.0)
                    .foregroundColor(color)
                    .frame(width: size, height: size)
            }
        }
    }
    
    private func growthSubmoduleView(_ submodule: GrowthSubmodule) -> some View {
        VStack {
            Button(action: {
                guard submodule.state != .locked && module.state != .locked else { return }
                onUserInput("#\(submodule.action)")
            }) {
                HStack(alignment: .center, spacing: .xsmall) {
                    VStack(alignment: .leading) {
                        Text(submodule.title)
                            .font(AppTheme.PrimaryFont.medium(size: 15.5))
                            .foregroundColor(.black)
                            .multilineTextAlignment(.leading)
                            .fixedSize(horizontal: false, vertical: true)

                        if submodule.description_ != nil {
                            Text(submodule.description_!)
                                .font(AppTheme.PrimaryFont.regular(size: 14))
                                .foregroundColor(.black)
                                .multilineTextAlignment(.leading)
                                .fixedSize(horizontal: false, vertical: true)
                                .padding(.top, .xxsmall)
                        }

                        if let progress = submodule.progress {
                            HStack(alignment: .center, spacing: .small) {
                                ProgressBarViewV2(
                                    height: 6,
                                    tint: progressBarTint,
                                    progress: .constant(Float(truncating: progress))
                                )

                                Text("\(String(format: "%.0f", (Float(truncating: progress) * 100)))%")
                                    .font(.system(size: 12))
                                    .foregroundColor(.black)
                            }
                            .padding(.top, .xxsmall)
                        }
                    }

                    Spacer()

                    if let image = URL(string: submodule.imageURL ?? "") {
                        KFImage(image)
                            .resizable()
                            .scaledToFit()
                            .frame(width: 50, height: 50)
                    }
                }
            }
        }
        .padding(.mediumSmall)
        .background(submoduleBackground)
        .opacity(submodule.state != .locked ? 1.0 : 0.5)
        .cornerRadius(.large)
    }
}
