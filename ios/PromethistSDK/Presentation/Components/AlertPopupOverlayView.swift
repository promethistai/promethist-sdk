//
//  AlertPopupOverlayView.swift
//  Promethist
//
//  Created by Vojtěch Vošmík on 23.12.2024.
//  Copyright © 2024 orgName. All rights reserved.
//

import SwiftUI

struct AlertPopupOverlayView: View {

    let text: String
    let style: AlertPopupOverlayStyle
    var onDismiss: () -> Void = {}

    private var hasDismissButton: Bool {
        style != .error
    }

    private var backgroundColor: Color {
        switch style {
        case .warning:
            Color.orange
        case .error:
            Color.red
        }
    }

    var body: some View {
        HStack(alignment: .center, spacing: .xsmall) {
            Text(text)
                .font(AppTheme.PrimaryFont.medium(size: 15))
                .foregroundStyle(.white)
                .padding(.top, 1)

            if hasDismissButton {
                Spacer()

                Button(action: onDismiss) {
                    Image(resource: \.icClose)
                        .renderingMode(.template)
                        .resizable()
                        .scaledToFit()
                        .frame(width: 24, height: 24)
                        .foregroundStyle(.white)
                }
            }
        }
        .frame(maxWidth: .infinity)
        .padding(.medium)
        .background(backgroundColor)
        .cornerRadius(.large)
        .shadow(radius: AppTheme.CornerRadius.large.rawValue)
    }
}

enum AlertPopupOverlayStyle {
    case warning, error
}
