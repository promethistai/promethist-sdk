import SwiftUI
@_implementationOnly import shared

struct IconButton: View {
    
    var text: String? = nil
    var description: String? = nil
    var resource: KeyPath<Strings, shared.ResourceStringDesc>? = nil
    var icon: String? = nil
    var color: SwiftUI.Color = Color.primary
    var background: SwiftUI.Color = Color(resource: \.accentSecondary)
    var fullSize: Bool = false
    var cornerRadius: AppTheme.CornerRadius = AppTheme.CornerRadius.large
    var onClick: () -> Void
    
    var body: some View {
        VStack {
            Button(action: onClick) {
                if fullSize {
                    Spacer()
                }
                
                VStack(alignment: .center, spacing: .xsmall) {
                    Group {
                        if let text {
                            Text(text)
                        } else if let resource {
                            Text(resource: resource)
                        }
                    }
                    .font(AppTheme.Fonts.modal)
                    .padding(.horizontal, !fullSize ? .mediumSmall : .zero)

                    if let description {
                        Text(description)
                            .font(AppTheme.PrimaryFont.regular(size: 14))
                    }
                }
                .foregroundColor(color)

                if let icon {
                    Image(systemName: icon)
                        .resizable()
                        .scaledToFit()
                        .foregroundColor(color)
                        .frame(width: iconSize, height: iconSize)
                }
                
                if fullSize {
                    Spacer()
                }
            }
        }
        .padding(.mediumSmall)
        .background(background)
        .cornerRadius(cornerRadius)
    }
}

fileprivate let iconSize: CGFloat = 20
