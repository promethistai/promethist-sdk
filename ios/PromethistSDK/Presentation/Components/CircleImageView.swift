import SwiftUI
@_implementationOnly import Kingfisher
@_implementationOnly import shared

struct CircleImageView: View {
    
    var text: String? = nil
    var url: URL? = nil
    var systemName: String = "questionmark.circle"
    var resource: KeyPath<Images, shared.ImageResource>? = nil
    var imageSize: CGFloat = 35
    var fontSize: CGFloat = 13.5
    var color: SwiftUI.Color = .white
    var shadowRadius: CGFloat = 5
    var scaleEffect: CGFloat = 1.0
    var padding: CGFloat = 10
    var cornerRadiusFactor: CGFloat = 2
    var animationDuration: CGFloat = 0.5
    var isActive: Bool = true
    
    var body: some View {
        VStack {
            if url != nil {
                KFImage(url)
                    .placeholder({ _ in
                        ProgressView()
                            .frame(width: imageSize, height: imageSize)
                            .cornerRadius(imageSize / cornerRadiusFactor)
                            .shadow(radius: shadowRadius)
                    })
                    .resizable()
                    .frame(width: imageSize, height: imageSize, alignment: .center)
                    .cornerRadius(imageSize / cornerRadiusFactor)
                    .shadow(radius: shadowRadius)
            } else if let resource {
                Image(resource: resource)
                    .renderingMode(.template)
                    .resizable()
                    .scaledToFit()
                    .scaleEffect(scaleEffect)
                    .frame(width: imageSize, height: imageSize)
                    .foregroundColor(color)
                    .opacity(isActive ? 1.0 : 0.4)
                    .shadow(radius: shadowRadius)
            } else {
                Image(systemName: systemName)
                    .resizable()
                    .scaledToFit()
                    .scaleEffect(scaleEffect)
                    .frame(width: imageSize, height: imageSize)
                    .foregroundColor(color)
                    .opacity(isActive ? 1.0 : 0.4)
                    .shadow(radius: shadowRadius)
            }
            if text != nil {
                Text("\(text!)")
                    .font(AppTheme.PrimaryFont.regular(size: fontSize))
                    .fixedSize(horizontal: false, vertical: true)
                    .foregroundColor(color)
                    .opacity(isActive ? 1.0 : 0.4)
                    .shadow(radius: shadowRadius)
            }
        }
        .padding(padding)
    }
}
