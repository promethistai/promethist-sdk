//
//  PermissionAlert.swift
//  Promethist
//
//  Created by Vojtěch Vošmík on 04.02.2025.
//  Copyright © 2025 orgName. All rights reserved.
//

import SwiftUI

struct PermissionAlert {
    
    static func alert(isPresented: Binding<Bool>) -> Alert {
        Alert(
            title: Text("Permissions Needed"),
            message: Text("This app requires audio permissions to function. Please enable them in Settings."),
            dismissButton: .default(Text("Open Settings")) {
                openSettings()
            }
        )
    }

    private static func openSettings() {
        if let url = URL(string: UIApplication.openSettingsURLString) {
            UIApplication.shared.open(url)
        }
    }
}


extension View {
    
    func withPermissionAlert(isPresented: Binding<Bool>) -> some View {
        self.alert(isPresented: isPresented) {
            PermissionAlert.alert(isPresented: isPresented)
        }
    }
}
