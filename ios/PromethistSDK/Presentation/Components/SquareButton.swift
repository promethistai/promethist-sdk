import SwiftUI
@_implementationOnly import shared
@_implementationOnly import Kingfisher

struct SquareButton: View {
    
    var url: URL? = nil
    var resource: KeyPath<Images, shared.ImageResource>? = nil
    var imageSize: CGFloat = defaultSize
    var padding: CGFloat = AppTheme.Spacing.small.rawValue
    
    var body: some View {
        ZStack {
            Group {
                if let url {
                    KFImage(url)
                        .resizable()
                } else if let resource {
                    Image(resource: resource)
                        .resizable()
                }
            }
            .scaledToFit()
            .frame(width: imageSize, height: imageSize, alignment: .center)
        }
        .padding(padding)
    }
}

fileprivate let defaultSize: CGFloat = 48
