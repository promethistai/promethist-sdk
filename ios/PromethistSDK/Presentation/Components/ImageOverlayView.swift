//
//  ImageOverlayView.swift
//  Promethist
//
//  Created by Vojtěch Vošmík on 12.03.2024.
//  Copyright © 2024 orgName. All rights reserved.
//

import SwiftUI
@_implementationOnly import Kingfisher

struct ImageOverlayView: View {

    let imageUrl: String

    @State private var alpha: CGFloat = 0

    var body: some View {
        VStack {
            Spacer()

            KFImage(URL(string: imageUrl))
                .onSuccess { _ in
                    withAnimation { alpha = 1 }
                }
                .resizable()
                .scaledToFit()
                .frame(height: 235)
                .cornerRadius(.large)
                .padding(.xsmall)
                .overlay(
                    RoundedRectangle(cornerRadius: AppTheme.CornerRadius.xlarge.rawValue)
                        .stroke(Color(resource: \.accentSecondary), lineWidth: 4)
                )
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .padding(.top, 120)
        .padding(.bottom, 80)
        .padding(.horizontal, .large)
        .opacity(alpha)
    }
}
