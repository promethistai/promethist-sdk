import SwiftUI

struct TranscriptsView: View {

    @State private var responseUUID: String? = nil
    @Environment(\.safeAreaInsets) private var safeAreaInsets

    let response: String?
    let transcript: String?
    let isListening: Bool
    let isResponseVisible: Bool
    let isTranscriptVisible: Bool
    let bottomOffset: CGFloat

    var responseOpacity: Double {
        if !isResponseVisible {
            return 0
        }
        return response != nil ? 1 : 0
    }
    
    var transcriptOpacity: Double {
        if !isTranscriptVisible {
            return 0
        }
        return isListening ? 1 : 0.5
    }

    var transcriptBottomOffset: CGFloat {
        safeAreaInsets.bottom > 0 ? 94 : 0
    }

    var responseMaxHeight: CGFloat {
        safeAreaInsets.bottom > 0 ? 208 : 128
    }

    var transcriptMaxHeight: CGFloat {
        safeAreaInsets.bottom > 0 ? 74 : 52
    }

    var body: some View {
        VStack {
            Spacer()

            // Persona response
            GeometryReader { proxy in
                PromethistAutoScrollText(
                    text: response ?? "",
                    startDelay: 3,
                    isResponseVisible: isResponseVisible,
                    maxHeight: responseMaxHeight
                )
                .frame(width: proxy.size.width)
                .frame(maxHeight: responseMaxHeight)
                .opacity(response != nil ? 1 : 0)
                .animation(responseAnimation, value: response)
            }
            .padding([.leading, .trailing], horizontalPadding)
            .frame(maxHeight: responseMaxHeight)
            .opacity(responseOpacity)
            .frame(maxWidth: .infinity, alignment: .center)
            .animation(.easeInOut, value: responseOpacity)
            .onChange(of: responseOpacity) { value in
                if value == 1.0 {
                    NotificationCenter.default.post(
                        name: PromethistAutoScrollText.textErasedNotification,
                        object: nil
                    )
                }
            }

            // User speech
            ZStack {
                if let transcript {
                    ScrollView {
                        Text(transcript)
                            .textShadowing()
                            .foregroundColor(.white)
                            .multilineTextAlignment(.center)
                            .frame(maxWidth: .infinity, alignment: .center)
                            .font(AppTheme.PrimaryFont.bold(size: 25))
                            .padding(.bottom, transcriptBottomOffset)
                            .opacity(transcriptOpacity)
                    }
                    .padding([.leading, .trailing], horizontalPadding)
                    .frame(maxHeight: transcriptMaxHeight)
                }
            }
            .frame(height: transcriptMaxHeight)
            .padding(.bottom, bottomOffset / 1.2)
        }
    }
}

fileprivate let horizontalPadding: CGFloat = AppTheme.Spacing.mediumLarge.rawValue
fileprivate let responseAnimation: Animation = .easeInOut(duration: 0.35)
fileprivate let topRectangleId = "top"
fileprivate let bottomRectangleId = "bottom"
