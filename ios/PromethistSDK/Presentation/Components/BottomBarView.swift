import Foundation
import SwiftUI
import GameController
@_implementationOnly import shared

struct BottomBarView: View {
    
    @EnvironmentObject private var screenInfo: ScreenInfo
    @EnvironmentObject private var textInputFocusObserver: TextInputFocusObserver
    @Environment(\.safeAreaInsets) private var safeAreaInsets

    @State private var isTextFieldFocused: Bool = false
    @State private var questionnaireModalVisible: Bool = true
    @State private var loadingDashPhase: CGFloat = -220

    private let isKeyboardConnected = GCKeyboard.coalesced != nil
    private let deviceType = UIDevice.current.userInterfaceIdiom
    
    var modalItem: Modal?
    var isLoading: Bool
    let isHomeButtonEnabled: Bool
    let isTypingDisabled: Bool
    let isSendDisabled: Bool
    let showTextInput: Bool
    let isBottomOffsetEnabled: Bool
    let onPersonaSelected: (PersonaItem, PersonaType) -> Void
    let onUserInput: (String) -> Void
    
    let onUndoClick: () -> Void
    let onHomeClick: () -> Void
    let onSkipClick: () -> Void
    let onBackToMenuClick: () -> Void
    
    var navigationHasBottomPadding: Bool {
        (modalItem != nil || showTextInput) && (!isTextFieldFocused || isKeyboardConnected)
    }
    
    var containerHasBottomPadding: Bool {
        modalItem != nil || showTextInput
    }

    var bottomPadding: CGFloat {
        safeAreaInsets.bottom > 0 ? 30 : 10
    }

    var body: some View {
        VStack {
            Group {
                if let item = modalItem as? SetPersonaItem {
                    SetPersonaModalView(
                        setPersonaItem: item,
                        onPersonaClick: {
                            onPersonaSelected($0, item.content.type)
                        }
                    )
                    .onAppear { textInputFocusObserver.isFocused = false }
                } else if let item = modalItem as? ActionsItem {
                    ActionsModalView(
                        actionsItem: item,
                        onItemClick: { item in
                            onUserInput(item.actionProcess)
                        }
                    )
                    .padding(.horizontal, .mediumLarge)
                    .onAppear { textInputFocusObserver.isFocused = false }
                } else if let item = modalItem as? InputItem {
                    InputModalView(
                        inputItem: item,
                        isTypingDisabled: isTypingDisabled,
                        isSendDisabled: isSendDisabled,
                        onUserInput: {
                            onUserInput($0)
                        }
                    )
                    .padding(.horizontal, .mediumLarge)
                    .padding(.top, .xsmall)
                    .onAppear { textInputFocusObserver.isFocused = false }
                } else if let item = modalItem as? QuestionnaireItem, questionnaireModalVisible {
                    QuestionnaireModalView(
                        questionnaireItem: item,
                        onUserInput: {
                            onUserInput($0)
                        }
                    )
                    .padding(.horizontal, .mediumLarge)
                    .onRotate { _ in
                        redrawQuestionnaireModal()
                    }
                    .onAppear { textInputFocusObserver.isFocused = false }
                } else if let item = modalItem as? GrowthContentItem {
                    GrowthContentView(
                        item: item,
                        onUserInput: {
                            onUserInput($0)
                        }
                    )
                    .padding(.horizontal, .mediumLarge)
                    .onAppear { textInputFocusObserver.isFocused = false }
                } else if let _ = modalItem as? SessionEndItem {
                    SessionEndModalView(onBackToMenuClick: onBackToMenuClick)
                        .padding(.horizontal, .mediumLarge)
                        .onAppear { textInputFocusObserver.isFocused = false }
                } else if let item = modalItem as? CorrectInputItem {
                    CorrectInputModalView(
                        requestText: RequestText(title: nil, text: item.correctInput.input),
                        isTypingDisabled: isTypingDisabled,
                        isSendDisabled: isSendDisabled,
                        onUserInput: {
                            onUserInput($0)
                        }
                    )
                    .padding(.horizontal, .mediumLarge)
                    .padding(.top, .xsmall)
                    .onAppear { textInputFocusObserver.isFocused = false }
                } else if showTextInput {
                    TextInputView(
                        isTypingDisabled: isTypingDisabled,
                        isSendDisabled: isSendDisabled,
                        onSend: { text in
                            onUserInput(text)
                        }
                    )
                    .padding(.horizontal, .mediumLarge)
                    .padding(.top, .small)
                }
            }
            .padding([.top, .bottom], .medium)
            .frame(width: deviceType != .phone ? screenInfo.width / 1.5 : screenInfo.width)
            
            HStack {
                Button(action: onUndoClick) {
                    CircleImageView(resource: \.icArrowUturnBackward, color: Color(resource: \.primaryLabel), shadowRadius: 0, scaleEffect: 0.8)
                        .padding(.bottom, .xxsmall)
                }
                
                Button(action: onHomeClick) {
                    CircleImageView(resource: \.icHouse, color: Color(resource: \.primaryLabel), shadowRadius: 0, scaleEffect: 0.9)
                }
                .disabled(!isHomeButtonEnabled)
                .opacity(isHomeButtonEnabled ? 1 : 0.5)
                
                Button(action: onSkipClick) {
                    CircleImageView(resource: \.icForwardEnd, color: Color(resource: \.primaryLabel), shadowRadius: 0, scaleEffect: 0.7)
                }
            }
            .padding(.bottom, navigationHasBottomPadding ? bottomPadding : 0)
        }
        .background(regularMaterial)
        .cornerRadius(radius: AppTheme.CornerRadius.xxxlarge.rawValue, corners: containerHasBottomPadding ? [.topLeft, .topRight] : .allCorners)
        .if(isBottomOffsetEnabled) { view in
            view.cornerRadius(
                radius: AppTheme.CornerRadius.xxxlarge.rawValue,
                corners: [.bottomLeft, .bottomRight]
            )
        }
        .shadow(radius: shadowRadius)
        .animation(modalAnimation/*, value: modalItem*/)
        .overlay(loadingIndicator)
        .transition(.move(edge: .bottom))
        .padding(.top, !showTextInput ? topOffset : AppTheme.Spacing.zero.rawValue)
        .padding(.bottom, containerHasBottomPadding ? 0 : bottomPadding)
        .onChange(of: textInputFocusObserver.isFocused) { isFocusd in
            withAnimation {
                isTextFieldFocused = isFocusd
            }
        }
    }
    
    private func redrawQuestionnaireModal() {
        Task {
            withAnimation { questionnaireModalVisible = false }
            try? await Task.sleep(nanoseconds: 0_500_000_000)
            withAnimation { questionnaireModalVisible = true }
        }
    }

    @ViewBuilder private var loadingIndicator: some View {
        RoundedRectangle(cornerRadius: 27)
            .strokeBorder(style: StrokeStyle(
                lineWidth: 3.5,
                lineCap: .round,
                lineJoin: .round,
                dash: [40, 400],
                dashPhase: loadingDashPhase)
            )
            .foregroundStyle(
                LinearGradient(gradient: Gradient(
                    colors: [Color(resource: \.accent), .white, Color(resource: \.accent), Color(resource: \.accent)]),
                    startPoint: .trailing,
                    endPoint: .leading
                )
            )
            .shadow(radius: 2)
            .onAppear {
                loadingDashPhase = 220
            }
            .animation(.linear(duration: 1.8).repeatForever(autoreverses: false), value: loadingDashPhase)
            .opacity((modalItem == nil && isLoading) ? 1 : 0)
    }
}

fileprivate let modalAnimation: Animation = .easeInOut(duration: 0.3)
fileprivate let shadowRadius: CGFloat = 20
fileprivate let topOffset: CGFloat = 80
