//
//  PersonaViewLayout.swift
//  Promethist
//
//  Created by Vojtěch Vošmík on 08.01.2025.
//  Copyright © 2025 orgName. All rights reserved.
//

import SwiftUI
import shared

public struct PersonaViewLayout<Content: View>: View {

    var viewModel: ClientModelImpl
    var appConfig: AppConfig
    @ObservedObject var uiStateHandler: ObservableUiStateHandler
    @ViewBuilder let content: () -> Content

    @State private var isAlertPopupDismissed: Bool = false
    @State private var isImagePickerPresented: Bool = false
    @State private var currentErrorItem: ErrorItemWrapper? = nil

    private let applyBlur: Bool

    public init(
        viewModel: ClientModelImpl,
        appConfig: AppConfig,
        uiStateHandler: ObservableUiStateHandler,
        applyBlur: Bool = false,
        content: @escaping () -> Content
    ) {
        self.viewModel = viewModel
        self.appConfig = appConfig
        self.uiStateHandler = uiStateHandler
        self.applyBlur = applyBlur
        self.content = content
    }

    var isNetworkDisconnected: Bool {
        uiStateHandler.uiState.connectionState == .disconnected
    }

    var isNetworkUnstable: Bool {
        uiStateHandler.uiState.connectionState == .unstable && !isAlertPopupDismissed
    }

    public var body: some View {
        ZStack(alignment: .bottom) {
            content()
        }
        // TODO: Rework
        /*.onChange(of: clientModel.persona.isPickingImage) { isPickingImage in
            guard isPickingImage else { return }
            isImagePickerPresented = true
        }*/
        .onChange(of: uiStateHandler.uiState.connectionState) { _ in
            isAlertPopupDismissed = false
        }
        .onChange(of: uiStateHandler.uiState.error) { errorItem in
            guard let errorItem else {
                currentErrorItem = nil
                return
            }

            if errorItem == ClientChannel.companion.sessionExpiredError {
                appEvents.send(.openAuthWebView)
            } else {
                currentErrorItem = ErrorItemWrapper(errorItem: errorItem)
            }
        }
        .alert(item: $currentErrorItem) { errorItem in
            if errorItem.errorItem.localizedText == ClientChannel.companion.buildCouldNotStartSessionError(throwable: nil).localizedText {
                if isNetworkDisconnected || ClientTarget.shared.currentTarget != .promethist {
                    buildErrorAlert(for: errorItem.errorItem) {
                        viewModel.handleEvent(event: ClientEventReset(sendAsInput: true))
                    }
                } else {
                    buildSecondaryErrorAlert(
                        for: errorItem.errorItem,
                        dismissAction: {
                            viewModel.handleEvent(event: ClientEventReset(sendAsInput: true))
                        },
                        retryAction: {
                            appEvents.send(.openAppKeyEntry)
                        }
                    )
                }
            } else {
                buildErrorAlert(for: errorItem.errorItem) {
                    viewModel.handleEvent(event: ClientEventReset(sendAsInput: true))
                }
            }
        }
        .background {
            UnityPlayerView(
                showPauseIndicator: !applyBlur,
                isPaused: uiStateHandler.uiState.isPaused
            )
            .blur(radius: applyBlur ? 25 : 0, opaque: true)
            .ignoresSafeArea(.all, edges: .all)
            .onAppear {
                startUnity()
            }
        }
        .overlay(alignment: .top) {
            Group {
                if isNetworkDisconnected {
                    AlertPopupOverlayView(
                        text: String(resource: \.network.networkError),
                        style: .error
                    )
                } else if isNetworkUnstable {
                    AlertPopupOverlayView(
                        text: String(resource: \.network.networkWarning),
                        style: .warning,
                        onDismiss: {
                            withAnimation { isAlertPopupDismissed = true }
                        }
                    )
                }
            }
            .offset(y: 80)
            .padding(.horizontal, .medium)
            .padding(.vertical, .small)
        }
        .sheet(isPresented: $isImagePickerPresented) {
            ImagePicker(sourceType: .camera) { (data) in
                print("ImagePicker: result \(data?.count ?? 0)")
                let base64 = data?.base64EncodedString()
                // TODO: Rework
                //viewModel.handleEvent(event: ClientEvent.OnImagePicked(base64Data: base64))
            }
            .onAppear {
                print("ImagePicker: onAppear")
            }
        }
    }

    func startUnity() {
        guard !UnityViewManager.shared.isUnityViewActive else { return }
        Task {
            UnityViewManager.shared.isUnityViewActive = true
            try await Task.sleep(nanoseconds: 0_350_000_000)
            UnityEmbeddedSwift.showUnity()
            let unityViewController = UnityEmbeddedSwift.getUnityView()!
            UnityViewManager.shared.delegate?.onViewUpdated(view: unityViewController)

            //if appConfig.duplexEnabled {
                DuplexSpeechRecognizerController.shared.setup()
            //} else {
            //    AudioManager.setupAudioSession()
            //}
        }
    }
}
