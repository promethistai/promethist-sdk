import SwiftUI
@_implementationOnly import shared

struct PrimaryButton: View {
    
    let text: String
    let onClick: () -> Void
    
    var body: some View {
        Button(action: onClick) {
            ZStack(alignment: .center) {
                Text(text)
                    .font(.system(size: 13, weight: .semibold))
                    .foregroundColor(.white)
                    .padding(.vertical, .mediumSmall)
                    .padding(.horizontal, .large)
            }
            .background(Color.accentColor)
            .cornerRadius(.xxlarge)
        }
    }
}
