//
//  AssetsUpdateOverlayView.swift
//  Promethist
//
//  Created by Vojtěch Vošmík on 20.03.2024.
//  Copyright © 2024 orgName. All rights reserved.
//

import SwiftUI

struct VideoAssetsUpdateOverlayView: View {

    let progress: Double

    var body: some View {
        VStack(alignment: .center, spacing: .large) {
            Spacer()

            Image(resource: \.icDownload)
                .renderingMode(.template)
                .resizable()
                .scaledToFit()
                .frame(width: 80, height: 80)
                .foregroundColor(.white)

            Text(String(resource: \.settings.downloadedNecessaryAssets))
                .font(.system(size: 20, weight: .bold))
                .padding(.bottom, .xxlarge)
                .multilineTextAlignment(.center)
                .foregroundColor(.white)

            HStack(alignment: .center, spacing: .medium) {
                ProgressView()
                    .tint(.white)

                ProgressView(value: progress)
                    .tint(Color(resource: \.accent))

                Text("\(Int(progress * 100))%")
                    .frame(width: 45)
                    .foregroundColor(.white)
            }
            .padding(.bottom, 100)

            Spacer()
        }
        .frame(maxWidth: .infinity)
        .padding(.large)
        .background(Color(resource: \.backgroundOverlay))
        .onTapGesture {}
    }
}
