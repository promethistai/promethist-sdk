import SwiftUI

@available(iOS 13.0, OSX 10.15, tvOS 13.0, watchOS 6.0, *)
public struct GridStack<Content>: View where Content: View {
    private let width: CGFloat
    private let minCellWidth: CGFloat
    private let spacing: CGFloat
    private let numItems: Int
    private let alignment: HorizontalAlignment
    private let content: (Int, CGFloat) -> Content
    private let gridCalculator = GridCalculator()
    
    public init(
        width: CGFloat,
        minCellWidth: CGFloat,
        spacing: CGFloat,
        numItems: Int,
        alignment: HorizontalAlignment = .leading,
        @ViewBuilder content: @escaping (Int, CGFloat) -> Content
    ) {
        self.width = width
        self.minCellWidth = minCellWidth
        self.spacing = spacing
        self.numItems = numItems
        self.alignment = alignment
        self.content = content
    }
    
    var items: [Int] {
        Array(0..<numItems).map { $0 }
    }
    
    public var body: some View {
        InnerGrid(
            width: width,
            spacing: spacing,
            items: items,
            alignment: alignment,
            content: content,
            gridDefinition: gridCalculator.calculate(
                availableWidth: width,
                minimumCellWidth: minCellWidth,
                cellSpacing: spacing
            )
        )
    }
}

@available(iOS 13.0, OSX 10.15, tvOS 13.0, watchOS 6.0, *)
private struct InnerGrid<Content>: View where Content: View {
    
    private let width: CGFloat
    private let spacing: CGFloat
    private let rows: [[Int]]
    private let alignment: HorizontalAlignment
    private let content: (Int, CGFloat) -> Content
    private let columnWidth: CGFloat
    
    init(
        width: CGFloat,
        spacing: CGFloat,
        items: [Int],
        alignment: HorizontalAlignment = .leading,
        @ViewBuilder content: @escaping (Int, CGFloat) -> Content,
        gridDefinition: GridCalculator.GridDefinition
    ) {
        self.width = width
        self.spacing = spacing
        self.alignment = alignment
        self.content = content
        columnWidth = gridDefinition.columnWidth
        rows = items.chunked(into: gridDefinition.columnCount)
    }
    
    var body : some View {
        //ScrollView(.vertical) {
            VStack(alignment: alignment, spacing: spacing) {
                if columnWidth > 0 {
                    ForEach(rows, id: \.self) { row in
                        HStack(spacing: spacing) {
                            ForEach(row, id: \.self) { item in
                                // Pass the index and the cell width to the content
                                self.content(item, columnWidth)
                                        .frame(width: columnWidth)
                            }
                        }.padding(.horizontal, spacing)
                    }
                }
            }
            .padding(.top, spacing)
            .frame(width: width)
        //}
    }
}
