//
//  RatingModalView.swift
//  Promethist
//
//  Created by Vojtěch Vošmík on 25.02.2025.
//  Copyright © 2025 orgName. All rights reserved.
//

import Foundation
import SwiftUI
import Kingfisher
@_implementationOnly import shared

struct RatingModalView: View {

    let item: RatingItem

    @State private var selectedOption: RatingOptionVO? = nil

    var body: some View {
        ScrollView {
            VStack(alignment: .center, spacing: .mediumLarge) {
                Text("How likely are you to recommend this app to your friends?")
                    .promethistFont(size: 30, weight: .regular)
                    .frame(maxWidth: .infinity, alignment: .leading)
                    .multilineTextAlignment(.leading)

                ForEach(RatingOptionVO.mock) { item in
                    Button(action: {
                        selectedOption = item
                    }) {
                        option(model: item, isSelected: item == selectedOption)
                    }
                }
            }
            .padding(.horizontal, .medium)
        }
    }

    @ViewBuilder private func option(model: RatingOptionVO, isSelected: Bool) -> some View {
        HStack(alignment: .center, spacing: .medium) {
            ZStack(alignment: .center) {
                Circle()
            }
            .frame(width: 32, height: 32, alignment: .center)

            KFImage(model.icon)
                .resizable()
                .scaledToFit()
                .frame(width: iconSize, height: iconSize)

            VStack(alignment: .leading, spacing: .xsmall) {
                Text(model.title)
                    .foregroundStyle(Color(resource: \.personaTextPrimary))
                    .promethistFont(size: 18, weight: .medium)
                    .frame(minWidth: .infinity, alignment: .leading)

                Text(model.text)
                    .foregroundStyle(Color(resource: \.personaTextPrimary).opacity(0.8))
                    .promethistFont(size: 12, weight: .regular)
                    .frame(minWidth: .infinity, alignment: .leading)
            }
            .frame(minWidth: .infinity, alignment: .leading)
        }
        .padding(.horizontal, .mediumSmall)
        .padding(.vertical, .mediumLarge)
        .background(isSelected ? Color(resource: \.accent) : Color(resource: \.personaGrey2))
        .cornerRadius(.large)
    }
}

private let iconSize: CGFloat = 24

private struct RatingOptionVO: Identifiable, Equatable {
    let id: String = Foundation.UUID().uuidString
    let title: String
    let text: String
    let icon: URL?

    static let mock = [
        RatingOptionVO(
            title: "Very good",
            text: "The app pleasantly surprised me, and I can recommend it with confidence.",
            icon: URL(string: "https://emojiisland.com/cdn/shop/products/17_large.png?v=1571606116")
        ),
        RatingOptionVO(
            title: "Good",
            text: "The app is useful and mostly works as I need it to. I would recommend it.",
            icon: URL(string: "https://emojiisland.com/cdn/shop/products/Smiling_Emoji_Icon_-_Blushed_large.png?v=1571606114")
        ),
        RatingOptionVO(
            title: "Average",
            text: "The app is okay, but I have some reservations about recommending it.",
            icon: URL(string: "https://emojiisland.com/cdn/shop/products/Slightly_Smiling_Emoji_Icon_34f238ed-d557-4161-b966-779d8f37b1ac_large.png?v=1571606093")
        ),
        RatingOptionVO(
            title: "Below Expectations",
            text: "Some features are nice, but the app has significant shortcomings. I’ll share my thoughts in the notes.",
            icon: URL(string: "https://emojiisland.com/cdn/shop/products/Neutral_Emoji_icon_9f1cc93a-f984-4b6c-896e-d24a643e4c28_large.png?v=1571606091")
        ),
        RatingOptionVO(
            title: "Poor",
            text: "The app does not meet my expectations, and I would not recommend it. I will share my thoughts in the notes.",
            icon: URL(string: "https://emojiisland.com/cdn/shop/products/Emoji_Icon_-_unamused_face_large.png?v=1571606093")
        ),
    ]
}
