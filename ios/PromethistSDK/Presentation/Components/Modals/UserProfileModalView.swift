//
//  UserProfileModalView.swift
//  Promethist
//
//  Created by Vojtěch Vošmík on 20.02.2025.
//  Copyright © 2025 orgName. All rights reserved.
//

import SwiftUI
@_implementationOnly import shared

struct UserProfileModalView: View {

    @FocusState private var isTextFieldFocused: Bool
    @State private var name: String = ""
    @State private var gender: Gender? = nil
    @State private var isError: Bool = false

    let item: UserProfileItem
    var showTitle: Bool = true
    let onSubmit: () -> Void

    var isValid: Bool {
        !name.isEmpty
        && (item.content.showGenderSelection ? (gender != nil) : true)
        && name.count > 2
    }

    private var borderColor: SwiftUI.Color {
        isTextFieldFocused ? Color(resource: \.accent) : Color(resource: \.personaBorderGrey)
    }

    private var borderLineWidth: CGFloat {
        isTextFieldFocused ? 2 : 1
    }

    var body: some View {
        VStack(alignment: .center, spacing: .mediumLarge) {
            if showTitle {
                Text(resource: \.modals.profileTitle)
                    .promethistFont(size: 24, weight: .medium)
                    .frame(maxWidth: .infinity, alignment: .center)
                    .multilineTextAlignment(.center)
            }

            section(title: String(resource: \.modals.profileName)) {
                TextField(String(resource: \.modals.profileNotFilled), text: $name)
                    .promethistFont(size: 18)
                    .padding(.medium)
                    .overlay(
                        RoundedRectangle(cornerRadius: 10)
                            .stroke(borderColor, lineWidth: borderLineWidth)
                    )
                    .contentShape(Rectangle())
                    .onTapGesture { isTextFieldFocused = true }
                    .focused($isTextFieldFocused)
                    .onChange(of: name) { value in
                        if name.count <= 2 {
                            withAnimation { isError = false }
                            return
                        }
                        withAnimation {
                            isError = !Validator.shared.isNameValid(entry: value)
                        }
                    }
            }

            if item.content.showGenderSelection {
                section(title: String(resource: \.modals.profileGender)) {
                    HStack(alignment: .center, spacing: 10) {
                        genderItem(
                            text: String(resource: \.modals.profileGenderMale),
                            isSelected: gender == .male,
                            onClick: {
                                gender = .male
                            }
                        )

                        genderItem(
                            text: String(resource: \.modals.profileGenderFemale),
                            isSelected: gender == .female,
                            onClick: {
                                gender = .female
                            }
                        )
                    }
                }
            }

            if isError {
                Text(resource: \.modals.profileInvalidNickname)
                    .promethistFont(size: 18, weight: .regular)
                    .foregroundStyle(.red)
                    .frame(maxWidth: .infinity, alignment: .center)
                    .padding(.horizontal, .xxsmall)
            }

            PromethistPrimaryButton(
                text: String(resource: \.modals.profileSubmit),
                isEnabled: isValid && !isError,
                onClick: {
                    guard Validator.shared.isNameValid(entry: name) else {
                        isError = true
                        return
                    }
                    // TODO

                    onSubmit()
                }
            )
            .disabled(!isValid)
            .frame(maxWidth: .infinity)
        }
        .padding(.top, .small)
    }

    @ViewBuilder private func section(
        title: String,
        @ViewBuilder content: @escaping () -> some View
    ) -> some View {
        VStack(alignment: .leading, spacing: .small) {
            Text(title)
                .promethistFont(size: 16, weight: .regular)
                .frame(maxWidth: .infinity, alignment: .leading)
                .padding(.horizontal, .xxsmall)
            
            content()
        }
    }

    @ViewBuilder private func genderItem(
        text: String,
        isSelected: Bool,
        onClick: @escaping () -> Void
    ) -> some View {
        Button(action: {
            Vibration.selection.vibrate()
            onClick()
        }) {
            HStack {
                Spacer()
                Text(text)
                    .promethistFont(size: 18, weight: isSelected ? .semibold : .regular)
                    .foregroundStyle(isSelected ? Color.white : Color(resource: \.personaTextPrimary))
                    .padding(.medium)
                Spacer()
            }
            .overlay(
                RoundedRectangle(cornerRadius: 10)
                    .stroke(Color(resource: \.personaBorderGrey), lineWidth: isSelected ? 0 : 2)
            )
            .background(Color(resource: \.accent).opacity(isSelected ? 1 : 0))
            .cornerRadius(10)
        }
    }
}

private enum Gender {
    case male, female
}
