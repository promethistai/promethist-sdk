import SwiftUI
@_implementationOnly import shared
@_implementationOnly import SwiftUIFlowLayout

struct CorrectInputModalView: View {

    let requestText: RequestText
    let isTypingDisabled: Bool
    let isSendDisabled: Bool
    let onUserInput: (String) -> Void

    var body: some View {
        HStack(spacing: .zero) {
            VStack(alignment: .leading, spacing: .mediumSmall) {
                if let title = requestText.title {
                    DynamicText(title, weight: .bold)
                        .padding([.top, .bottom], .xsmall)
                        .padding(.bottom, .xxsmall)
                }

                TextInputView(
                    text: requestText.text ?? "",
                    isTypingDisabled: isTypingDisabled,
                    isSendDisabled: isSendDisabled,
                    onSend: { text in
                        onUserInput(text)
                    }
                )
            }

            Spacer()
        }
    }
}
