import SwiftUI
@_implementationOnly import shared

struct QuestionnaireModalView: View {
    
    let questionnaireItem: QuestionnaireItem
    let onUserInput: (String) -> Void
    
    private let emotions = "😠😨😮😀😁😖😧😯😃😆😫️😟😶😲😄😣️🙁😐🙂☺️😞️😔😑😌☺️"
    
    private func emotion(_ text: String) -> String {
        if let range = text.range(of: "emotion") {
            let index = emotions.index(emotions.startIndex, offsetBy: Int(text[range.upperBound...]) ?? 0)
            return String(emotions[index])
        } else {
            return "🤔"
        }
    }
    
    @State private var contentSize: CGSize = .zero
    
    var body: some View {
        let questionnaire = questionnaireItem.content
        
        GeometryReader { geometry in
            VStack(alignment: .center, spacing: .mediumSmall) {
                if let title = questionnaire.title {
                    DynamicText(title, weight: .bold)
                        .padding([.top, .bottom], .small)
                }
                
                GridStack(
                    width: geometry.size.width,
                    minCellWidth: minCellWidth,
                    spacing: gridSpacing,
                    numItems: questionnaire.texts.count
                ) { index, cellSize in
                    
                    Button(action: {
                        onUserInput(questionnaire.texts[index])
                    }) {
                        VStack {
                            Text(questionnaire.labels.count > index ? questionnaire.labels[index] : emotion(questionnaire.texts[index]))
                                .font(AppTheme.Fonts.heading1)
                        }
                        .frame(width: cellSize, height: cellSize)
                        .background(Color.gray.opacity(0.2))
                        .cornerRadius(10)
                    }
                }
                
                HStack {
                    ForEach(0..<questionnaire.subLabels.count, id: \.self) {
                        if $0 > 0 {
                            Spacer()
                        }
                        Text(questionnaire.subLabels[$0])
                            .font(AppTheme.Fonts.heading3)
                            .foregroundColor(.primary)
                    }
                }
            }
            .overlay {
                GeometryReader { geo in
                    Color.clear.onAppear {
                        contentSize = geo.size
                    }
                }
            }
        }
        .frame(maxHeight: contentSize.height)
    }
}

fileprivate let minCellWidth: CGFloat = 60
fileprivate let gridSpacing: CGFloat = 5
