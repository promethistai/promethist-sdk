import SwiftUI
@_implementationOnly import shared

struct ActionsModalView: View {
    
    let actionsItem: ActionsItem
    let onItemClick: (ActionTile) -> Void
    
    @State private var contentSize: CGSize = .zero
    
    var body: some View {
        let content = actionsItem.content
        
        VStack(alignment: .center, spacing: .mediumSmall) {
            if !content.title.isEmpty {
                DynamicText(content.title, weight: .bold)
                    .padding([.top, .bottom], .xsmall)
            }
            
            ScrollView {
                VStack {
                    ForEach(content.tiles, id: \.self) { item in
                        ActionButtonView(
                            item: item,
                            fullScreen: content.tiles.count > 1,
                            onClick: {
                                onItemClick(item)
                            }
                        )
                    }
                }
                .overlay(
                    GeometryReader { geo in
                        Color.clear.onAppear {
                            contentSize = geo.size
                        }
                    }
                )
            }
            .frame(maxHeight: contentSize.height)
        }
    }
}

private struct ActionButtonView: View {
    
    let item: ActionTile
    let fullScreen: Bool
    let onClick: () -> Void
    
    var body: some View {
        IconButton(
            text: item.title,
            description: item.text,
            fullSize: fullScreen,
            cornerRadius: .xxlarge,
            onClick: {
                guard item.active else { return }
                onClick()
            }
        )
    }
}
