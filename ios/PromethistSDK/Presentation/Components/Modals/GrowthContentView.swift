import SwiftUI
import Combine
@_implementationOnly import shared

struct GrowthContentView: View {

    let item: GrowthContentItem
    let onUserInput: (String) -> Void

    private var globalProgressAlignment: GrowthGlobalProgressAlignment {
        return (item.content.goal ?? "").isEmpty ? .right : .left
    }

    var body: some View {
        GeometryReader { geo in
            VStack(alignment: .center, spacing: .mediumSmall) {
                DynamicText(item.content.title)
                    .padding(.top, .small)
                    .multilineTextAlignment(.center)
                    .padding(.horizontal, horizontalPadding)

                HStack {
                    if case .right = globalProgressAlignment { Spacer() }
                    Text(item.globalProgressText)
                    if case .left = globalProgressAlignment { Spacer() }
                }
                .padding(.horizontal, horizontalPadding)

                if let progress = item.content.progress {
                    ProgressBarView(
                        width: geo.size.width,
                        height: 10,
                        tint: Color(resource: \.growthTint),
                        progress: .constant(Float(truncating: progress))
                    )
                    .padding(.horizontal, horizontalPadding)
                }

                ScrollView {
                    VStack(spacing: .mediumSmall) {
                        ForEach(item.content.modules.indices, id: \.self) { index in
                            GrowthModuleView(
                                module: item.content.modules[index],
                                maxWidth: geo.size.width - AppTheme.Spacing.mediumSmall.rawValue,
                                onUserInput: onUserInput,
                                isExpanded: false //index == 0
                            )
                            .shadow(color: .black.opacity(0.1), radius: 3.5)
                        }
                    }
                    .padding(.xsmall)
                }
                .frame(maxHeight: .infinity)
            }
        }
    }
}

private let horizontalPadding: CGFloat = AppTheme.Spacing.small.rawValue

private enum GrowthGlobalProgressAlignment {
    case left, right
}

extension GrowthContentItem {

    var globalProgressText: String {
        var text = if let progress = self.content.progress {
            "\(String(format: "%.0f", (Float(truncating: progress) * 100)))%"
        } else {
            ""
        }

        if let contentText = self.content.text, !contentText.isEmpty {
            text += " \(contentText)"
        }
        if let goalText = self.content.goal?.lowercased(), !goalText.isEmpty {
            text += " \(goalText)"
        }

        return text
    }
}
