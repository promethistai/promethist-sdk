import SwiftUI

struct YearPickerView: View {
    
    private let years: [Int] = (1940...Calendar.current.component(.year, from: Date())).map { $0 }
    private let numberFormatter: NumberFormatter = {
        let nf = NumberFormatter()
        nf.usesGroupingSeparator = false
        return nf
    }()
    
    @State var selectedYearIndex: Int
    
    var onYearPicked: (Int) -> Void

    init(onYearPicked: @escaping (Int) -> Void) {
        selectedYearIndex = years.count - 1
        
        self.onYearPicked = onYearPicked
    }
    
    var body: some View {
        HStack {
            Spacer()
            
            VStack(alignment: .center, spacing: 2) {
                FixedPicker(selection: $selectedYearIndex, rowCount: years.count) { row in
                    "\(years[row])"
                }
                            
                Button(action: {
                    onYearPicked(years[selectedYearIndex])
                }) {
                    Image(resource: \.icArrowRight)
                        .renderingMode(.template)
                        .resizable()
                        .scaledToFit()
                        .scaleEffect(0.5)
                        .frame(width: 40, height: 40)
                        .foregroundColor(Color.white)
                        .background(Color.accentColor)
                        .clipShape(Circle())
                }
            }
            
            Spacer()
        }
        .padding(.bottom, 14)
    }
    
    func yearString(at index: Int) -> String {
        let selectedYear = years[index]
        return numberFormatter.string(for: selectedYear) ?? selectedYear.description
    }
}

private struct FixedPicker: UIViewRepresentable {
    
    class Coordinator : NSObject, UIPickerViewDataSource, UIPickerViewDelegate {
        @Binding var selection: Int
        
        var initialSelection: Int?
        var titleForRow: (Int) -> String
        var rowCount: Int

        func numberOfComponents(in pickerView: UIPickerView) -> Int {
            1
        }
        
        func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
            rowCount
        }
        
        func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
            titleForRow(row)
        }
        
        func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
            self.selection = row
        }
        
        init(selection: Binding<Int>, titleForRow: @escaping (Int) -> String, rowCount: Int) {
            self.titleForRow = titleForRow
            self._selection = selection
            self.rowCount = rowCount
        }
    }
    
    @Binding var selection: Int
    
    var rowCount: Int
    let titleForRow: (Int) -> String

    func makeCoordinator() -> FixedPicker.Coordinator {
        return Coordinator(selection: $selection, titleForRow: titleForRow, rowCount: rowCount)
    }

    func makeUIView(context: UIViewRepresentableContext<FixedPicker>) -> UIPickerView {
        let view = UIPickerView()
        view.delegate = context.coordinator
        view.dataSource = context.coordinator
        return view
    }
    
    func updateUIView(_ uiView: UIPickerView, context: UIViewRepresentableContext<FixedPicker>) {
        context.coordinator.titleForRow = self.titleForRow
        context.coordinator.rowCount = rowCount

        //only update selection if it has been changed
        if context.coordinator.initialSelection != selection {
            uiView.selectRow(selection, inComponent: 0, animated: true)
            context.coordinator.initialSelection = selection
        }
    }
}
