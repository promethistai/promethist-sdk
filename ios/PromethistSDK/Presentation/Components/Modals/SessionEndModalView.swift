import SwiftUI
@_implementationOnly import shared

struct SessionEndModalView: View {
    
    let onBackToMenuClick: () -> Void
    
    var body: some View {
        ZStack(alignment: .center) {
            PrimaryButton(
                text: Resources().strings.persona.backToMenu.localized(),
                onClick: onBackToMenuClick
            )
        }
        .frame(height: containerHeight)
    }
}

fileprivate let containerHeight: CGFloat = 80
