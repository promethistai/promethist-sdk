import SwiftUI
@_implementationOnly import shared
@_implementationOnly import Kingfisher

struct SetPersonaModalView: View {
    
    let setPersonaItem: SetPersonaItem
    let onPersonaClick: (PersonaItem) -> Void
    
    private var scale: Double {
        setPersonaItem.content.size == ResourceSize.l ? 1.4 : 0.6
    }
    
    @State private var contentSize: CGSize = .zero
    
    var body: some View {
        GeometryReader { geometry in
            VStack(spacing: .mediumSmall) {
                if let title = setPersonaItem.content.title {
                    DynamicText(title, weight: .bold)
                        .padding(.vertical, .small)
                        .padding(.horizontal, .mediumLarge)
                }
                
                ScrollView(.horizontal, showsIndicators: false) {
                    VStack {
                        HStack(alignment: .top) {
                            Spacer()
                            
                            ForEach(setPersonaItem.content.personas, id: \.self) { item in
                                Button(action: {
                                    onPersonaClick(item)
                                }) {
                                    PersonaItemView(
                                        item: item,
                                        size: setPersonaItem.content.size,
                                        scale: scale
                                    )
                                    .padding(.leading, -(AppTheme.Spacing.xlarge.rawValue))
                                    .padding(.trailing, -(AppTheme.Spacing.large.rawValue))
                                }
                            }
                            
                            Spacer()
                        }
                        .frame(minWidth: geometry.size.width)
                    }
                }
                .frame(width: geometry.size.width)
            }
            .overlay {
                GeometryReader { geo in
                    Color.clear.onAppear {
                        contentSize = geo.size
                    }
                }
            }
        }
        .frame(maxHeight: contentSize.height)
    }
}

private struct PersonaItemView: View {
    
    let item: PersonaItem
    let size: ResourceSize
    let scale: Double
    
    private var portrait: CGSize {
        switch size {
        case .m:
            return CGSize(width: 200, height: 200)
        case .l:
            return CGSize(width: 150, height: 200)
        case .xl:
            return CGSize(width: 100, height: 200)
        default:
            return CGSize(width: 200, height: 200)
        }
    }
    
    private var text: Double {
        switch size {
        case .m:
            return 15
        case .l:
            return 20
        case .xl:
            return 30
        default:
            return 20
        }
    }

    private var image: UIImage {
        PersonaResourcesManager().getPortrait(
            personaId: PersonaId.Companion().fromAvatarId(name: item.avatarId ?? item.id),
            size: .l
        ).toUIImage()!
    }

    private var url: URL? {
        let url = RemotePersonaResourceManager().getPortrait(
            personaId: PersonaId.Companion().fromAvatarId(name: item.avatarId ?? item.id),
            size: .l
        )
        return URL(string: url ?? "")
    }

    var body: some View {
        VStack {
            Text(item.name)
                .font(AppTheme.PrimaryFont.bold(size: text))
                .foregroundColor(.primary)
                .padding(.bottom, .xxsmall)
            
            ZStack {}
                .frame(width: portrait.width * scale, height: portrait.height * scale)
                .background(
                    Group {
                        if let remoteUrl = url {
                            KFImage(remoteUrl)
                                .resizable()
                                .scaledToFill()
                        } else {
                            Image(uiImage: image)
                                .resizable()
                                .scaledToFill()
                        }
                    }
                )
            
            if let description = item.description_ {
                Text(description)
                    .font(AppTheme.Fonts.heading3)
                    .foregroundColor(.primary)
                    .fixedSize(horizontal: false, vertical: true)
                    .frame(width: portrait.width * scale - 50)
                    .padding([.leading, .trailing], .mediumLarge)
            }
        }
    }
}
