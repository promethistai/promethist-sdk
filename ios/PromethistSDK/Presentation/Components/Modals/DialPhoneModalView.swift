//
//  DialPhoneModalView.swift
//  Promethist
//
//  Created by Vojtěch Vošmík on 20.02.2025.
//  Copyright © 2025 orgName. All rights reserved.
//

import SwiftUI
@_implementationOnly import shared

struct DialPhoneModalView: View {

    let item: DialPhoneItem
    let onCancel: () -> Void

    var body: some View {
        VStack(alignment: .center, spacing: .zero) {
            Text("\(String(resource: \.modals.callText))\(item.content.phoneNumber)")
                .promethistFont(size: 24, weight: .medium)
                .padding(.vertical, .xxlarge)
                .padding(.bottom, .small)
                .frame(maxWidth: .infinity, alignment: .center)
                .lineLimit(4)
                .truncationMode(.tail)
                .multilineTextAlignment(.center)

            HStack(spacing: .mediumSmall) {
                PromethistPrimaryButton(
                    text: String(resource: \.modals.callCancel),
                    style: AppTheme.secondaryButtonStyle,
                    onClick: onCancel
                )
                .frame(maxWidth: .infinity)

                PromethistPrimaryButton(
                    text: String(resource: \.modals.callSubmit),
                    onClick: callPhoneNumber
                )
                .frame(maxWidth: .infinity)
            }
        }
    }

    private func callPhoneNumber() {
        if let phoneURL = URL(string: "telprompt://\(item.content.phoneNumber)"),
           UIApplication.shared.canOpenURL(phoneURL) {
            UIApplication.shared.open(phoneURL, options: [:], completionHandler: nil)
        }
    }
}
