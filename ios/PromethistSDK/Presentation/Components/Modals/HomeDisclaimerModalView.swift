//
//  HomeDisclaimerModalView.swift
//  Promethist
//
//  Created by Vojtěch Vošmík on 11.02.2025.
//  Copyright © 2025 orgName. All rights reserved.
//

import SwiftUI
@_implementationOnly import shared

struct HomeDisclaimerModalView: View {

    let onConfirm: () -> Void
    let onCancel: () -> Void

    var body: some View {
        VStack(alignment: .center, spacing: .zero) {
            Text(resource: \.persona.homeDisclaimerText)
                .promethistFont(size: 24, weight: .medium)
                .padding(.vertical, .xxlarge)
                .padding(.bottom, .small)

            HStack(spacing: .mediumSmall) {
                PromethistPrimaryButton(
                    text: String(resource: \.persona.homeDisclaimerNo),
                    style: AppTheme.secondaryButtonStyle,
                    onClick: onCancel
                )
                .frame(maxWidth: .infinity)

                PromethistPrimaryButton(
                    text: String(resource: \.persona.homeDisclaimerYes),
                    onClick: onConfirm
                )
                .frame(maxWidth: .infinity)
            }
        }
    }
}
