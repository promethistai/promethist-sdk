//
//  OrderModalView.swift
//  Promethist
//
//  Created by Vojtěch Vošmík on 11.02.2025.
//  Copyright © 2025 orgName. All rights reserved.
//

import SwiftUI
@_implementationOnly import shared

struct OrderModalView: View {

    let orderItem: OrderSummaryItem
    let onCancel: () -> Void
    let onSubmit: () -> Void

    var body: some View {
        VStack(alignment: .center, spacing: .zero) {
            header
            Divider()
            items
            footer
        }
    }

    @ViewBuilder private var header: some View {
        Text(resource: \.modals.orderSummaryTitle)
            .font(AppTheme.PrimaryFont.medium(size: 22))
            .foregroundStyle(Color(resource: \.personaTextPrimary))
            .frame(maxWidth: .infinity, alignment: .center)
            .lineLimit(1)
            .truncationMode(.tail)
            .padding(.horizontal, .mediumLarge)
            .padding(.bottom, .mediumLarge)
            .padding(.top, .small)
    }

    @ViewBuilder private var footer: some View {
        VStack(alignment: .center, spacing: .zero) {
            if orderItem.content.totalAmount != nil || orderItem.content.totalAmountWithoutVat != nil {
                HStack(alignment: .center, spacing: .medium) {
                    Text(resource: \.modals.orderTotal)
                        .font(AppTheme.PrimaryFont.medium(size: 20))
                        .foregroundStyle(Color(resource: \.personaTextPrimary))
                        .frame(maxWidth: .infinity, alignment: .leading)

                    priceView(
                        price: orderItem.content.totalAmount,
                        priceWithoutVat: orderItem.content.totalAmountWithoutVat
                    )
                }
                .padding(.vertical, 14)
                .padding(.horizontal, .mediumLarge)
                .background(Color(resource: \.personaGrey2).opacity(0.25))

                Divider()
            }

            HStack(spacing: .mediumSmall) {
                PromethistPrimaryButton(
                    text: String(resource: \.modals.orderCancel),
                    style: AppTheme.secondaryButtonStyle,
                    onClick: onCancel
                )
                .frame(maxWidth: .infinity)

                PromethistPrimaryButton(
                    text: String(resource: \.modals.orderSubmit),
                    onClick: onSubmit
                )
                .frame(maxWidth: .infinity)
            }
            .padding(.top, 22)
            .padding(.horizontal, .mediumLarge)
        }
    }

    @ViewBuilder private var items: some View {
        ScrollView {
            LazyVStack(alignment: .center, spacing: .zero) {
                ForEach((orderItem.content.items ?? []).indices, id: \.self) { index in
                    let order = (orderItem.content.items ?? [])[index]
                    item(
                        title: order.name,
                        price: order.price,
                        priceWithoutVat: order.priceWithoutVat,
                        onRemoveClick: {}
                    )
                }
            }
        }
    }

    @ViewBuilder private func item(
        title: String,
        price: String?,
        priceWithoutVat: String?,
        onRemoveClick: @escaping () -> Void
    ) -> some View {
        VStack(alignment: .center, spacing: .zero) {
            HStack(alignment: .top, spacing: .medium) {
                Text(title)
                    .font(AppTheme.PrimaryFont.medium(size: 20))
                    .foregroundStyle(Color(resource: \.personaTextPrimary))
                    .frame(maxWidth: .infinity, alignment: .leading)
                    .lineLimit(3)
                    .truncationMode(.tail)

                priceView(price: price, priceWithoutVat: priceWithoutVat)
            }
            .padding(.vertical, .medium)
            .padding(.horizontal, .mediumLarge)

            Divider()
        }
    }

    @ViewBuilder private func priceView(price: String?, priceWithoutVat: String?) -> some View {
        VStack(alignment: .trailing, spacing: 6) {
            if let price {
                Text(price)
                    .font(AppTheme.PrimaryFont.medium(size: 20))
                    .foregroundStyle(Color(resource: \.personaTextPrimary))
            }

            if let priceWithoutVat {
                Text(priceWithoutVat)
                    .font(AppTheme.PrimaryFont.regular(size: 12))
                    .foregroundStyle(Color(resource: \.personaTextSecondary))
            }
        }
    }
}
