import SwiftUI
@_implementationOnly import shared
@_implementationOnly import SwiftUIFlowLayout

struct InputModalView: View {
    
    let inputItem: InputItem
    let isTypingDisabled: Bool
    let isSendDisabled: Bool
    let onUserInput: (String) -> Void

    var body: some View {
        let input = inputItem.input
        
        HStack(spacing: .zero) {
            VStack(alignment: .leading, spacing: .mediumSmall) {
                if let title = input.title {
                    DynamicText(title, weight: .bold)
                        .padding([.top, .bottom], .xsmall)
                        .padding(.bottom, .xxsmall)
                }

                if let text = input.text {
                    Text(text)
                        .font(AppTheme.PrimaryFont.regular(size: 16))
                        .fixedSize(horizontal: false, vertical: true)
                        .padding(.bottom, 6)
                }

                if input.birthYear {
                    YearPickerView {
                        onUserInput("\($0)")
                    }
                }
                
                if inputItem.textInput {
                    FlowLayout(mode: .scrollable, items: input.values, itemSpacing: AppTheme.Spacing.xsmall.rawValue) { text in
                        IconButton(
                            text: text,
                            onClick: {
                                onUserInput(text)
                            }
                        )
                    }
                    
                    TextInputView(
                        isTypingDisabled: isTypingDisabled,
                        isSendDisabled: isSendDisabled,
                        onSend: { text in
                            onUserInput(text)
                        }
                    )
                } else {
                    ForEach(input.values, id: \.self) { text in
                        IconButton(
                            text: text,
                            onClick: {
                                onUserInput(text)
                            }
                        )
                    }
                }
            }
            
            Spacer()
        }
    }
}
