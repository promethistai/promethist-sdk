//
//  ResponsiveSheet.swift
//  Promethist
//
//  Created by Vojtěch Vošmík on 04.03.2025.
//  Copyright © 2025 orgName. All rights reserved.
//

import SwiftUI

struct InnerHeightPreferenceKey: PreferenceKey {

    static let defaultValue: CGFloat = .zero

    static func reduce(value: inout CGFloat, nextValue: () -> CGFloat) {
        value = nextValue()
    }
}

extension View {

    func responsiveSheet<Content: View>(
        isPresented: Binding<Bool>,
        @ViewBuilder content: @escaping () -> Content
    ) -> some View {
        self.sheet(isPresented: isPresented) {
            ResponsiveSheetView(content: content)
        }
    }
}

private struct ResponsiveSheetView<Content: View>: View {

    let content: () -> Content
    @State private var sheetHeight: CGFloat = .zero

    var body: some View {
        VStack {
            SheetDragIndicator()
                .padding(.vertical, .mediumSmall)

            content()
        }
        .overlay {
            GeometryReader { geometry in
                Color.clear.preference(key: InnerHeightPreferenceKey.self, value: geometry.size.height)
            }
        }
        .onPreferenceChange(InnerHeightPreferenceKey.self) { newHeight in
            sheetHeight = newHeight
        }
        .presentationDetents([.height(sheetHeight)])
    }
}

struct SheetDragIndicator: View {

    var body: some View {
        RoundedRectangle(cornerRadius: 2)
            .fill(.gray.opacity(0.4))
            .frame(width: 44, height: 4)
    }
}
