import SwiftUI

struct ProgressBarView: View {

    let width: CGFloat
    let height: CGFloat
    var tint: Color = Color.accentColor
    @Binding var progress: Float
    @State private var drawingWidth = false

    var body: some View {
        ZStack(alignment: .leading) {
            RoundedRectangle(cornerRadius: height / 2)
                .fill(.primary.opacity(0.15))
            RoundedRectangle(cornerRadius: height / 2)
                .fill(tint)
                .frame(width: drawingWidth ? width * CGFloat(progress) : 0, alignment: .leading)
                .animation(.easeInOut(duration: 1), value: drawingWidth)
        }
        .frame(maxWidth: .infinity, maxHeight: height)
        .onAppear {
            drawingWidth = true
        }
    }
}

struct ProgressBarViewV2: View {

    let height: CGFloat
    var tint: Color = Color.accentColor
    @Binding var progress: Float
    @State private var drawingWidth = false

    var body: some View {
        GeometryReader { proxy in
            ZStack(alignment: .leading) {
                RoundedRectangle(cornerRadius: height / 2)
                    .fill(.primary.opacity(0.15))
                RoundedRectangle(cornerRadius: height / 2)
                    .fill(tint)
                    .frame(width: drawingWidth ? proxy.size.width * CGFloat(progress) : 0, alignment: .leading)
                    .animation(.easeInOut(duration: 1), value: drawingWidth)
            }
            .frame(maxWidth: .infinity, maxHeight: height)
            .onAppear {
                drawingWidth = true
            }
        }
        .frame(maxWidth: .infinity, maxHeight: height)
    }
}
