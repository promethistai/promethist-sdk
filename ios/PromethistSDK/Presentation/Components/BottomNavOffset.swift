import SwiftUI
@_implementationOnly import shared

struct BottomFade: View {
    
    var body: some View {
        Rectangle()
            .fill(EllipticalGradient(
                gradient: .init(colors: [ .black.opacity(0.0), .black.opacity(0.9) ]),
                center: .bottom,
                startRadiusFraction: 0.9,
                endRadiusFraction: 0.0
            ))
            .frame(maxWidth: .infinity)
    }
}
