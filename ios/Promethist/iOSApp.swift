import SwiftUI
import shared
import PromethistSDK

@main
struct iOSApp: App {

    init() {
        ClientTarget.shared.currentTarget = TargetKt.iosTarget
    }

	var body: some Scene {
		WindowGroup {
            PromethistRouter()
                .onOpenURL { url in
                    // App link
                    if let appLink = AppLinkParser.shared.parse(value: url.absoluteString) {
                        AppRouterModel.isAppLinkParsed = true
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.6) {
                            Client().appConfig().engineDomain = appLink.engineDomain
                            Client().appConfig().engineSetupName = appLink.setupName
                            Client().appConfig().preset = AppPreset.EngineV3()
                            appEvents.send(.appLinkReceived(engineDomain: appLink.engineDomain, engineSetupName: appLink.setupName))
                        }
                        return
                    }
                    
                    // Deep link
                    let appKey = url.lastPathComponent
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.6) {
                        Client().appConfig().engineAppKey = appKey
                        appEvents.send(.appKeyReceived(appKey))
                    }
                }
                .onContinueUserActivity(NSUserActivityTypeBrowsingWeb) { userActivity in
                    guard let url = userActivity.webpageURL else { return }
                    
                    // App link
                    if let appLink = AppLinkParser.shared.parse(value: url.absoluteString) {
                        AppRouterModel.isAppLinkParsed = true
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.6) {
                            Client().appConfig().engineDomain = appLink.engineDomain
                            Client().appConfig().engineSetupName = appLink.setupName
                            Client().appConfig().preset = AppPreset.EngineV3()
                            appEvents.send(.appLinkReceived(engineDomain: appLink.engineDomain, engineSetupName: appLink.setupName))
                        }
                        return
                    }
                    
                    // Deep link
                    let appKey = url.lastPathComponent
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.6) {
                        Client().appConfig().engineAppKey = appKey
                        appEvents.send(.appKeyReceived(appKey))
                    }
                }
		}
	}
}
